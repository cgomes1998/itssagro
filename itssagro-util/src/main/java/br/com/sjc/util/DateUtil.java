package br.com.sjc.util;

import org.joda.time.DateTime;
import org.joda.time.Seconds;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

public class DateUtil {

    public static final String PATTERN_DATA = "dd/MM/yyyy";

    public static final String PATTERN_DATA_SEM_CARACTERES = "ddMMyyyy";

    public static final String PATTERN_HORA = "HH:mm:ss";

    public static final String PATTERN_HORA_MINUTO = "HH:mm";

    private static final String PATTERN_HORA_SEM_CARACTERES = "HHmmss";

    public static final String MINUTOS = "60";

    public static boolean estaEntre(Date data, Date dataInicial, Date dataFinal) {

        return DateUtil.maiorQue(data, dataInicial) && DateUtil.menorQue(data, dataFinal);
    }

    public static boolean maiorQue(Date date1, Date date2) {

        LocalDateTime localDate1 = date1.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        LocalDateTime localDate2 = date2.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        return localDate1.isAfter(localDate2);
    }

    public static boolean menorQue(Date date1, Date date2) {

        LocalDateTime localDate1 = date1.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        LocalDateTime localDate2 = date2.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        return localDate1.isBefore(localDate2);
    }

    public static Date hoje() {

        return new Date();
    }

    public static String format(final String pattern, final Date date) {

        if (ObjetoUtil.isNull(date)) {

            return StringUtil.empty();
        }

        return new SimpleDateFormat(pattern).format(date);
    }

    public static String getYear(final Date data) {

        final Calendar calendar = Calendar.getInstance();

        calendar.setTime(data);

        return String.valueOf(calendar.get(Calendar.YEAR));
    }

    public static Date format(final String date, final String pattern) {

        try {

            return new SimpleDateFormat(pattern).parse(date);

        } catch (ParseException e) {

            return null;
        }
    }

    public static String formatTo(final Date date, final String pattern) {

        return new SimpleDateFormat(pattern).format(date);

    }

    public static String formatData_ddMMyyyyHHmmss(Date data) {

        if (Objects.isNull(data)) {
            return StringUtil.empty();
        }
        return DateUtil.formatData(data) + " " + DateUtil.formatHora(data);
    }

    public static String formatData(final Date data) {

        if (ObjetoUtil.isNull(data)) {

            return StringUtil.empty();
        }

        return new SimpleDateFormat(DateUtil.PATTERN_DATA).format(data);
    }

    public static String formatHora(final Date data) {

        if (ObjetoUtil.isNull(data)) {

            return StringUtil.empty();
        }

        return new SimpleDateFormat(DateUtil.PATTERN_HORA).format(data);
    }

    public static String formatHoraMinuto(final Date data) {

        if (ObjetoUtil.isNull(data)) {

            return StringUtil.empty();
        }

        return new SimpleDateFormat(DateUtil.PATTERN_HORA_MINUTO).format(data);
    }

    public static String formatHoraSemCaracteres(final Date data) {

        if (ObjetoUtil.isNull(data)) {

            return StringUtil.empty();
        }

        return new SimpleDateFormat(DateUtil.PATTERN_HORA_SEM_CARACTERES).format(data);
    }

    public static String formatData_ddMMyyyyHHmmss_semCaracteres(final Date data) {

        if (ObjetoUtil.isNull(data)) {

            return StringUtil.empty();
        }

        String date = new SimpleDateFormat(DateUtil.PATTERN_DATA_SEM_CARACTERES).format(data);
        String hours = new SimpleDateFormat(DateUtil.PATTERN_HORA_SEM_CARACTERES).format(data);

        return String.join("", date, hours);
    }

    public static String formatToSAP(final Date data) {

        if (ObjetoUtil.isNull(data)) {

            return StringUtil.empty();
        }

        return new SimpleDateFormat("yyyy/MM/dd").format(data).replaceAll("/", "");
    }

    public static void main(String[] args) {
        System.out.println(DateUtil.format("YYYYMMddHHmmss", new Date()));
    }

    public static Date dateHorarioSAPformat(final String dataText, final Date hora) {

        if (ObjetoUtil.isNull(dataText)) {

            return null;
        }

        try {

            SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");

            Date data = yyyyMMdd.parse(dataText);

            if (ObjetoUtil.isNull(hora)) {

                return data;
            }

            return DateUtil.setarHora(data, hora);

        } catch (ParseException e) {
            return null;
        }
    }

    public static Date obterDataHoraZerada(Date data) {

        try {

            final DateFormat format = new SimpleDateFormat("dd/MM/yyyy");

            data = format.parse(format.format(data));

        } catch (final ParseException e) {
            e.printStackTrace();
        }

        return data;
    }

    public static Date converterDateFimDia(Date data) {

        Calendar cal = Calendar.getInstance();

        cal.setTime(data);

        cal.set(Calendar.HOUR_OF_DAY, 23);

        cal.set(Calendar.MINUTE, 59);

        cal.set(Calendar.SECOND, 59);

        return cal.getTime();
    }

    public static Date diminuirDias(Date data, int dias) {

        Calendar cal = Calendar.getInstance();

        cal.setTime(data);

        cal.add(Calendar.DAY_OF_MONTH, -dias);

        return cal.getTime();
    }

    public static Date parseDate(String date) {

        Date data = null;

        if (!StringUtil.isEmpty(date)) {
            List<String> patterns = new ArrayList<>();
            patterns.add("yyyy-MM-dd");
            patterns.add("yyyy-MM-dd HH:mm:ss");
            patterns.add("dd/MM/yyyy");
            patterns.add("dd/MM/yyyy HH:mm:ss");

            for (String pattern : patterns) {
                try {
                    data = new SimpleDateFormat(pattern).parse(date);
                } catch (ParseException ignored) {
                }
            }
        }

        return data;
    }

    public static Date toDate(long miliseconds) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone(ZoneId.of("America/Sao_Paulo")));
        calendar.setTimeInMillis(miliseconds);

        return calendar.getTime();
    }

    public static String horasEntreDatasNoFormato_hhmmss(Date dataMenor, Date dataMaior) {

        try {

            int segundosTotal = DateUtil.segundosEntreDatas(dataMenor, dataMaior);

            double horas = Math.floor(segundosTotal / 3600);

            String horaFormatada = DateUtil.convertDoubleToDateString(horas);
            String minutosFormatado = DateUtil.convertDoubleToDateString(Math.floor((segundosTotal - (horas * 3600)) / 60));
            String segundosFormatado = DateUtil.convertDoubleToDateString(Math.floor(segundosTotal % 60));

            return horaFormatada + ":" + minutosFormatado + ":" + segundosFormatado;

        } catch (Exception e) {

            System.out.println("Erro ao formatar data, método horasEntreDatasNoFormato_hhmmss" + e.getMessage());

            return StringUtil.empty();
        }
    }

    private static String convertDoubleToDateString(Double date) {

        Integer integer = date.intValue();
        if (integer > 9) {
            return integer.toString();
        }
        return "0" + integer.toString();
    }

    public static int segundosEntreDatas(Date dataMenor, Date dataMaior) {

        DateTime dataInicial = new DateTime(dataMenor);

        DateTime dataFinal = new DateTime(dataMaior);

        return Seconds.secondsBetween(dataInicial, dataFinal).getSeconds();
    }

    public static Date criarData(int dia, int mes, int ano) {

        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.DAY_OF_MONTH, dia);

        cal.set(Calendar.MONTH, mes);

        cal.set(Calendar.YEAR, ano);

        return cal.getTime();
    }

    public static Date setarHora(Date data, Date hora) {

        Calendar cal = Calendar.getInstance();

        cal.setTime(data);

        Calendar calHora = Calendar.getInstance();

        calHora.setTime(hora);

        cal.set(Calendar.HOUR_OF_DAY, calHora.get(Calendar.HOUR_OF_DAY));

        cal.set(Calendar.MINUTE, calHora.get(Calendar.MINUTE));

        cal.set(Calendar.SECOND, calHora.get(Calendar.SECOND));

        return cal.getTime();
    }

    public static Date setarHora(Date data, int... tempo) {

        Calendar cal = Calendar.getInstance();

        cal.setTime(data);

        cal.set(Calendar.HOUR_OF_DAY, tempo[0]);

        if (tempo.length > 1) {

            cal.set(Calendar.MINUTE, tempo[1]);
        }

        if (tempo.length > 2) {

            cal.set(Calendar.SECOND, tempo[2]);
        }

        return cal.getTime();
    }
}
