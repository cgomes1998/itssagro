package br.com.sjc.util;

import java.util.*;
import java.util.function.Function;

public class ColecaoUtil {

    public static boolean isEmpty(final Set<?> list) {

        return list == null || ColecaoUtil.quantidade(list) <= 0;
    }

    public static boolean isNotEmpty(final Set<?> list) {

        return list != null && ColecaoUtil.quantidade(list) > 0;
    }

    public static boolean isEmpty(final SortedSet<?> list) {

        return list == null || ColecaoUtil.quantidade(list) <= 0;
    }

    public static boolean isNotEmpty(final SortedSet<?> list) {

        return list != null && ColecaoUtil.quantidade(list) > 0;
    }

    public static boolean isEmpty(final Collection<?> list) {

        return list == null || ColecaoUtil.quantidade(list) <= 0;
    }

    public static boolean isNotEmpty(final Collection<?> list) {

        return list != null && ColecaoUtil.quantidade(list) > 0;
    }

    public static boolean isEmpty(final List<?> list) {

        return list == null || ColecaoUtil.quantidade(list) <= 0;
    }

    public static boolean isNotEmpty(final List<?> list) {

        return list != null && ColecaoUtil.quantidade(list) > 0;
    }

    public static int quantidade(final List<?> list) {

        return list.size();
    }

    public static int quantidade(final Collection<?> list) {

        return list.size();
    }

    public static int quantidade(final Set<?> list) {

        return list.size();
    }

    public static int quantidade(final SortedSet<?> list) {

        return list.size();
    }

}
