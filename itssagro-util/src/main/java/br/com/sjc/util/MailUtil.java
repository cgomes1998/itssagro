package br.com.sjc.util;

import com.sun.mail.imap.IMAPFolder;

import javax.mail.*;
import javax.mail.internet.MimeBodyPart;
import javax.mail.search.AndTerm;
import javax.mail.search.ComparisonTerm;
import javax.mail.search.ReceivedDateTerm;
import javax.mail.search.SearchTerm;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.function.Predicate;
import java.util.logging.Logger;

public class MailUtil {

    public static final Logger LOGGER = Logger.getLogger(MailUtil.class.getName());

    private static final String MULTIPART = "multipart";

    private static final String MAIL_IMAP_HOST = "mail.imap.host";

    private static final String MAIL_IMAP_PORT = "mail.imap.port";

    private static final String MAIL_IMAP_SSL_TRUST = "mail.imap.ssl.trust";

    private static final String MAIL_IMAP_STARTTLS_ENABLE = "mail.imap.starttls.enable";

    public static void downloadDeAnexos(final Date dataFiltro, String host, String porta, String servidor, String usaurio, String senha, String diretorio) {

        try {

            Store store = MailUtil.connectServer(host, porta, servidor, usaurio, senha);

            IMAPFolder inbox = MailUtil.createObjectInbox(store);

            File file = new File(diretorio);

            Message[] messages;

            messages = MailUtil.createFileIfNotExistAndFilterMessages(inbox, file, dataFiltro);

            Arrays.asList(messages).stream().filter(MailUtil.filterAttachment()).forEach(message -> {

                try {

                    Multipart multiPart = (Multipart) message.getContent();

                    if (ObjetoUtil.isNotNull(multiPart)) {

                        for (int i = 0; i < multiPart.getCount(); i++) {

                            MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(i);

                            if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {

                                MailUtil.writeAttachamentInDirectory(file, part);
                            }
                        }
                    }

                } catch (MessagingException e) {

                    MailUtil.LOGGER.info(MessageFormat.format("\n ERRO: {0} \n MOTIVO: {1}", e.getMessage(), e.getCause()));

                } catch (IOException e) {

                    MailUtil.LOGGER.info(MessageFormat.format("\n ERRO: {0} \n MOTIVO: {1}", e.getMessage(), e.getCause()));
                }

            });

            inbox.close(false);
            store.close();

        } catch (NoSuchProviderException e) {

            MailUtil.LOGGER.info(MessageFormat.format("\n ERRO: {0} \n MOTIVO: {1}", e.getMessage(), e.getCause()));

        } catch (MessagingException e) {

            MailUtil.LOGGER.info(MessageFormat.format("\n ERRO: {0} \n MOTIVO: {1}", e.getMessage(), e.getCause()));

        } catch (Exception e) {

            MailUtil.LOGGER.info(MessageFormat.format("\n ERRO: {0} \n MOTIVO: {1}", e.getMessage(), e.getCause()));
        }
    }

    private static void writeAttachamentInDirectory(File file, MimeBodyPart part) throws MessagingException, IOException {

        String destino = file.getAbsolutePath() + File.separator + part.getFileName();

        try (FileOutputStream output = new FileOutputStream(destino)) {
            InputStream input = part.getInputStream();

            byte[] buffer = new byte[4096];

            int byteRead;

            while ((byteRead = input.read(buffer)) != -1) {
                output.write(buffer, 0, byteRead);
            }
        }
    }

    private static Message[] createFileIfNotExistAndFilterMessages(IMAPFolder inbox, File file, Date dataFiltro) throws MessagingException {

        if (!file.exists()) {

            file.mkdir();

            return inbox.getMessages();
        }

        if (ObjetoUtil.isNull(dataFiltro)) {

            return inbox.getMessages();
        }

        SearchTerm maiorIgual = new ReceivedDateTerm(ComparisonTerm.GE, DateUtil.obterDataHoraZerada(dataFiltro));

        SearchTerm menorIgual = new ReceivedDateTerm(ComparisonTerm.LE, DateUtil.converterDateFimDia(DateUtil.hoje()));

        SearchTerm andTerm = new AndTerm(menorIgual, maiorIgual);

        return inbox.search(andTerm);
    }

    private static Predicate<Message> filterAttachment() {

        return message -> {

            try {

                if (message.getContentType().contains(MULTIPART)) {

                    Multipart multiPart = (Multipart) message.getContent();

                    if (ObjetoUtil.isNotNull(multiPart)) {

                        for (int i = 0; i < multiPart.getCount(); i++) {

                            MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(i);

                            if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {

                                return true;
                            }
                        }
                    }
                }

            } catch (MessagingException ex) {

                MailUtil.LOGGER.info(MessageFormat.format("ERRO AO ITERAR O CORPO DO E-MAIL PARA IDENTIFICAR QUAL PARTE CONTÉM O ANEXO: {0}", ex.getCause()));

            } catch (IOException e) {

                MailUtil.LOGGER.info(MessageFormat.format("ERRO: {0}", e.getMessage()));
            }

            return false;

        };
    }

    private static IMAPFolder createObjectInbox(Store store) throws MessagingException {

        IMAPFolder inbox = (IMAPFolder) store.getFolder("INBOX");

        inbox.open(Folder.READ_ONLY);

        return inbox;
    }

    private static Store connectServer(String host, String porta, String servidor, String user, String password) throws MessagingException {

        Properties properties = new Properties();

        properties.put(MAIL_IMAP_HOST, host);
        properties.put(MAIL_IMAP_PORT, porta);
        properties.put(MAIL_IMAP_SSL_TRUST, "*");
        properties.put(MAIL_IMAP_STARTTLS_ENABLE, "true");

        Session emailSession = Session.getDefaultInstance(properties);

        Store store = emailSession.getStore(servidor);

        store.connect(host, user, password);

        return store;
    }
}
