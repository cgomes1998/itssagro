package br.com.sjc.util;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class RelatorioUtil {

    public static byte[] gerarPdf(final List<?> data, final InputStream templateJasper, final Map<String, Object> parametros) throws JRException {

        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(data, false);

        JasperPrint print = JasperFillManager.fillReport(templateJasper, parametros, dataSource);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        JasperExportManager.exportReportToPdfStream(print, outputStream);

        return outputStream.toByteArray();
    }
}
