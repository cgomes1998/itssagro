package br.com.sjc.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import javax.swing.text.MaskFormatter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StringUtil {

    public static String getPrintStackTrace(Exception e) {

        StringWriter stringWriter = new StringWriter();

        PrintWriter printWriter = new PrintWriter(stringWriter);

        e.printStackTrace(printWriter);

        return stringWriter.toString();
    }

    public static String toJson(Object object) {

        try {

            ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();

            return objectWriter.writeValueAsString(object);

        } catch (JsonProcessingException e) {

            e.printStackTrace();
        }

        return null;
    }

    public static String getStringPorPosicaoDeArray(String[] strings, int index) {

        try {
            return strings[index];
        } catch (ArrayIndexOutOfBoundsException e) {
            return StringUtil.empty();
        }
    }

    public static List<String> quebrarStringPorTamanho(String valor, int tamanho) {

        return Arrays.asList(valor.split("(?<=\\G.{" + tamanho + "})"));
    }

    public static boolean igual(String valor1, String valor2) {

        if (StringUtil.isNotNullEmpty(valor1) && StringUtil.isNotNullEmpty(valor2)) {
            return valor1.equals(valor2);
        }
        return false;
    }

    public static boolean igualIgnoradoCaseSensitive(String valor1, String valor2) {

        return valor1.trim().equalsIgnoreCase(valor2.trim());
    }

    public static boolean isPlaca(final String valor) {

        return !contemApenasNumeros(valor);
    }

    public static String toPlaca(final String valor) {

        return valor.substring(0, 3).toUpperCase() + "-" + valor.substring(3);
    }

    public static String toMilhar(final BigDecimal valor, final int casasDecimais) {

        Locale brasil = new Locale("pt", "BR");

        if (casasDecimais == 0) {

            DecimalFormat df = new DecimalFormat("#,###", new DecimalFormatSymbols(brasil));

            return df.format(valor);
        }

        String qntdCasas = "";

        for (int i = 0; i < casasDecimais; i++) {

            qntdCasas = qntdCasas + "#";
        }

        DecimalFormat df = new DecimalFormat("#,###." + qntdCasas, new DecimalFormatSymbols(brasil));

        return df.format(valor);
    }

    public static String toMilhar(final BigDecimal valor) {

        Locale brasil = new Locale("pt", "BR");

        DecimalFormat df = new DecimalFormat("#,###.######", new DecimalFormatSymbols(brasil));

        return df.format(valor);
    }

    public static String empty() {

        return "";
    }

    public static String apresentarValorIsNull(final String se, final String... valor) {

        if (valor != null) {

            return Arrays.asList(valor).stream().filter(i -> StringUtil.isNotNullEmpty(i)).findFirst().orElse(se);
        }

        return se;
    }

    public static String format(String value, String maskPattern) {

        try {

            MaskFormatter mask = new MaskFormatter(maskPattern);

            mask.setValueContainsLiteralCharacters(false);

            return mask.valueToString(value);

        } catch (ParseException ex) {

            Logger.getLogger(StringUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return StringUtil.empty();
    }

    public static boolean isNotNullEmpty(final String value) {

        return value != null && value.length() > 0;
    }

    public static boolean isEmpty(final String value) {

        return value == null || value.trim().length() <= 0;
    }

    public static String formatNumeric(final String value, final String pattern) {

        DecimalFormat df = new DecimalFormat();

        df.applyPattern(pattern);

        return df.format(value);
    }

    public static String removerCaracteresEspeciais(final String valor) {

        return valor.replaceAll("[./-]", "");
    }

    public static String tratarTextoComCaracterEspecial(final String value) {

        try {

            return new String(value.getBytes(), "UTF-8");

        } catch (Exception e) {

            return value;
        }
    }

    public static boolean contemApenasNumeros(final String valor) {

        return valor.matches("[0-9]+");
    }

    public static String apenasNumeros(final String valor) {

        return valor.replaceAll("[^0-9]", "");
    }

    public static Integer parseInt(final String valor) {

        try {

            return Integer.parseInt(valor);

        } catch (NumberFormatException e) {

            return null;
        }
    }

    public static String log(final String texto) {

        return MessageFormat.format("\n\n********\n\n{0}\n\n********\n", texto);
    }

    public static String completarZeroAEsquerda(int completar, String valor) {

        if (StringUtil.isEmpty(valor)) {
            return null;
        }

        int size = valor.length();

        int total = completar - size;

        String zero = "";

        for (int i = 0; i < total; i++) {

            zero = zero + 0;

        }

        return (zero + valor).toString();
    }

}
