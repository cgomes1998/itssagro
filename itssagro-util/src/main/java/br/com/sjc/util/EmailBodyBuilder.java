package br.com.sjc.util;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.Date;

public class EmailBodyBuilder {
    
    public static final String PARAMETRO_URL_SISTEMA = "${url_sistema}";
    
    private StringBuilder emailbody;
    
    public static EmailBodyBuilder build() {
        
        return new EmailBodyBuilder();
    }
    
    private EmailBodyBuilder() {
        
        emailbody = new StringBuilder();
    }
    
    public EmailBodyBuilder open_P_element() {
        
        return open_P_element("");
    }
    
    public EmailBodyBuilder open_P_element(String style) {
        
        emailbody.append("<p style=\"").append(style).append("\">");
        return this;
    }
    
    public EmailBodyBuilder close_P_element() {
        
        emailbody.append("</p>");
        return this;
    }
    
    public EmailBodyBuilder addText(String text) {
        
        emailbody.append(StringEscapeUtils.escapeHtml4(text));
        return this;
    }
    
    public EmailBodyBuilder addTextStrong(String text) {
        
        emailbody.append("<strong>").append(StringEscapeUtils.escapeHtml4(text)).append("</strong>");
        return this;
    }
    
    public EmailBodyBuilder addInformacoesDeMensagemAutomatica() {
        
        addTextStrong("Esta é uma mensagem automática. Por favor, não responda este e-mail.");
        return this;
    }
    
    public EmailBodyBuilder addLinkSistema(String label) {
        
        return addLink(PARAMETRO_URL_SISTEMA, label);
    }
    
    public EmailBodyBuilder addLink(String href, String label) {
        
        emailbody.append("<a href=\"").append(href).append("\" target=\"_blank\">").append(StringEscapeUtils.escapeHtml4(label)).append("</a>");
        return this;
    }
    
    public EmailBodyBuilder addDataHora(Date date) {
        
        emailbody.append(DateUtil.format("dd/MM/yyyy HH:mm:ss", date));
        return this;
    }
    
    public EmailBodyBuilder addLineDivider() {
        
        emailbody.append("<hr>");
        return this;
    }
    
    public EmailBodyBuilder addLineBreak() {
        
        return addLineBreak(1);
    }
    
    public EmailBodyBuilder addLineBreak(int qtde) {
        
        for (int cont = 0; cont < qtde; cont++) {
            emailbody.append("<br>");
        }
        
        return this;
    }
    
    @Override
    public String toString() {
        
        return emailbody.toString();
    }
    
}
