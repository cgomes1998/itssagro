package br.com.sjc.util;

import com.google.gson.Gson;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ObjetoUtil {

    public static <E> E jsonToObject(String conteudo, Class<E> entidadeClass) {

        Gson gson = new Gson();

        E entidade = gson.fromJson(conteudo, entidadeClass);

        return entidade;
    }

    public static boolean isNotNull(final Object... o) {

        List<Object> objects = Arrays.asList(o);

        return objects.stream().filter(ObjetoUtil::isNotNull).count() == objects.size();
    }

    public static boolean isNotNull(final Object o) {

        return o != null;
    }

    public static boolean isNull(final Object o) {

        return o == null;
    }

    /**
     * Percorre todos os fields da entidade e verifica se existe ao menus um preenchido.
     * Utilizado para resolver problema do angular enviar todos os objetos instanciados,
     * ocasionando excessão org.hibernate.TransientPropertyValueException: object references an unsaved transient instance - save the transient instance before flushing.
     *
     * @param o
     *
     * @return
     */
    public static boolean isFieldsChanged(final Object o) {

        if (isNotNull(o)) {
            return Arrays.stream(o.getClass().getDeclaredFields()).anyMatch(field -> {
                try {
                    field.setAccessible(true);
                    return !field.getName().equals("serialVersionUID") && !field.getName().equals("uuid") && !field.getName().equals("dataCadastro") && !field.getName().equals("idUsuarioAutorCadastro") && !field.getName().equals(
                            "dataAlteracao") && !field.getName().equals("idUsuarioAutorAlteracao") && !field.getName().equals("status") && isNotEmpty(o, field);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                return false;
            });
        }
        return false;
    }

    private static boolean isNotEmpty(Object o, Field field) throws IllegalAccessException {

        if (field.getType().isAssignableFrom(List.class)) {
            return isNotNull(field.get(o)) && !((List) field.get(o)).isEmpty();
        }
        return isNotNull(field.get(o)) && StringUtil.isNotNullEmpty(field.get(o).toString());
    }

}
