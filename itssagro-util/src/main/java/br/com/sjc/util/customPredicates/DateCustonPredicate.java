package br.com.sjc.util.customPredicates;

import org.hibernate.query.criteria.internal.CriteriaBuilderImpl;
import org.hibernate.query.criteria.internal.ParameterRegistry;
import org.hibernate.query.criteria.internal.Renderable;
import org.hibernate.query.criteria.internal.compile.RenderingContext;
import org.hibernate.query.criteria.internal.expression.LiteralExpression;
import org.hibernate.query.criteria.internal.predicate.AbstractSimplePredicate;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by julio.bueno on 31/05/2019.
 */
public class DateCustonPredicate extends AbstractSimplePredicate implements Serializable {

    private final Expression<String> matchExpression;

    private final Expression<Date> patternDe;

    private final Expression<Date> patternAte;

    private final Date de;

    private final Date ate;

    public static DateCustonPredicate build(CriteriaBuilder criteriaBuilder, Expression<String> matchExpression, Date de, Date ate) {

        return new DateCustonPredicate((CriteriaBuilderImpl) criteriaBuilder, matchExpression, de, ate);
    }

    private DateCustonPredicate(CriteriaBuilderImpl criteriaBuilder, Expression<String> matchExpression, Date de, Date ate) {

        super(criteriaBuilder);

        this.matchExpression = matchExpression;
        this.patternDe = new LiteralExpression<>(criteriaBuilder, de);
        this.patternAte = new LiteralExpression<>(criteriaBuilder, ate);
        this.de = de;
        this.ate = ate;
    }

    @Override
    public void registerParameters(ParameterRegistry registry) {

        Helper.possibleParameter(null, registry);
        Helper.possibleParameter(this.matchExpression, registry);
        Helper.possibleParameter(this.patternDe, registry);
        Helper.possibleParameter(this.patternAte, registry);
    }

    @Override
    public String render(boolean isNegated, RenderingContext renderingContext) {

        final String operator = isNegated ? " not between " : " between ";

        String toDateInicio = "to_date(";
        String coluna = ((Renderable) this.matchExpression).render(renderingContext) + " ";
        String toDateFim = ", 'DD-MM-YYYY') ";
        //        String dataDe = ((Renderable) this.patternDe ).render(renderingContext) + " and ";
        //        String dataAte = ((Renderable) this.patternDe ).render(renderingContext) + " ";
        String dataDe = new SimpleDateFormat("dd-MM-yyyy").format(this.de) + "' and ";
        String dataAte = new SimpleDateFormat("dd-MM-yyyy").format(this.ate) + "' ";

        return toDateInicio + coluna + toDateFim + operator + "'" + dataDe + "'" + dataAte;
    }

}
