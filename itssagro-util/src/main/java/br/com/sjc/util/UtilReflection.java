package br.com.sjc.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class UtilReflection {

    private static final String PATTERN = "\\.";

    private static final Logger LOG = Logger.getLogger(UtilReflection.class.getSimpleName());

    public static void setValor(final String campo, final Object entidade, final Object valor) {

        try {

            List<Field> fields = new ArrayList<>();

            getFields(fields, entidade.getClass());

            final Field field = fields.stream().filter(_field -> _field.getName().equals(campo)).findFirst().orElse(null);

            if (ObjetoUtil.isNotNull(field)) {

                field.setAccessible(true);

                field.set(entidade, valor);
            }

        } catch (final Exception e) {

            UtilReflection.LOG.severe(e.getMessage());
        }
    }

    public static Object getValor(final String campo, final Object entidade) {

        Object valor = null;

        try {

            List<Field> fields = new ArrayList<>();

            getFields(fields, entidade.getClass());

            final Field field = fields.stream().filter(_field -> _field.getName().equals(campo)).findFirst().orElse(null);

            if (ObjetoUtil.isNotNull(field)) {

                field.setAccessible(true);

                valor = field.get(entidade);
            }

        } catch (final Exception e) {

            UtilReflection.LOG.severe(e.getMessage());
        }

        return valor;
    }

    private static List<Field> getFields(List<Field> fields, Class<?> type) {

        if (ObjetoUtil.isNull(fields)) {

            fields = new ArrayList<>();
        }

        fields.addAll(Arrays.asList(type.getDeclaredFields()));

        if (ObjetoUtil.isNotNull(type.getSuperclass())) {

            getFields(fields, type.getSuperclass());
        }

        return fields;
    }

    public static boolean isNull(final Object entidade, final String campo) {

        final String[] dados = campo.split(PATTERN);

        Object pesquisador = entidade;

        for (final String valor : dados) {

            final Object item = UtilReflection.getValor(valor, pesquisador);

            if (ObjetoUtil.isNull(item)) {

                return true;
            }

            pesquisador = item;
        }

        return false;
    }

    public static Object getNullOrValue(final Object entidade, final String campo) {

        final String[] dados = campo.split(PATTERN);

        Object pesquisador = entidade;

        for (final String valor : dados) {

            final Object item = UtilReflection.getValor(valor, pesquisador);

            if (ObjetoUtil.isNull(item)) {

                return null;
            }

            pesquisador = item;
        }

        return pesquisador;
    }

    public static String getNullOrValueString(final Object entidade, final String campo) {

        final String[] dados = campo.split(PATTERN);

        Object pesquisador = entidade;

        for (final String valor : dados) {

            final Object item = UtilReflection.getValor(valor, pesquisador);

            if (ObjetoUtil.isNull(item)) {

                return null;
            }

            pesquisador = item;
        }

        return pesquisador.toString();
    }

}
