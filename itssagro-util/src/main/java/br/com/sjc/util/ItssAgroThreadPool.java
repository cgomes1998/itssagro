package br.com.sjc.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ItssAgroThreadPool {

    private static ItssAgroThreadPool instance;

    private static ItssAgroThreadPool getInstance(){
        if(instance == null){
            instance = new ItssAgroThreadPool();
        }
        return instance;
    }

    private ExecutorService executorServiceSingle;
    private ExecutorService executorServiceCached;
    private ExecutorService executorServiceFixed;

    private ItssAgroThreadPool(){
        executorServiceSingle = Executors.newSingleThreadExecutor();
        executorServiceCached = Executors.newCachedThreadPool();
        executorServiceFixed = Executors.newFixedThreadPool(10);
    }

    public static void enfileirarTarefa(Runnable tarefa) {
        getInstance().executorServiceSingle.execute(tarefa);
    }

    public static void executarTarefa(Runnable tarefa) {
        getInstance().executorServiceCached.execute(tarefa);
    }

    public static void enfileirarTarefaFixed(Runnable tarefa) {
        getInstance().executorServiceFixed.execute(tarefa);
    }
}
