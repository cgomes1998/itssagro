package br.com.sjc.util;

import org.apache.commons.lang3.reflect.FieldUtils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.text.NumberFormat;
import java.util.Objects;

/**
 * Created by julio.bueno on 03/07/2019.
 */
public class Util {

    public static int DEFAULT_MINIMUM_INTEGER_DIGITS = 6;

    public static Type[] getTypes(Object object) {
        ParameterizedType parameterizedType;

        try {
            parameterizedType = (ParameterizedType) object.getClass().getGenericSuperclass();

        } catch (ClassCastException e) {
            try {
                parameterizedType = (ParameterizedType) Class.forName(object.getClass().getGenericSuperclass().getTypeName()).getGenericSuperclass();

            } catch (ClassNotFoundException classNotFountexception) {
                throw new RuntimeException(classNotFountexception);
            }

        }

        return parameterizedType.getActualTypeArguments();
    }

    public static String formatarCodigo(String prefixo, long codigo) {

        return formatarCodigo(prefixo, DEFAULT_MINIMUM_INTEGER_DIGITS, codigo);
    }

    public static String formatarCodigo(String prefixo, int minimumIntegerDigits, long codigo) {

        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMaximumFractionDigits(0);
        numberFormat.setMinimumIntegerDigits(minimumIntegerDigits);
        numberFormat.setGroupingUsed(false);

        return prefixo + numberFormat.format(codigo);
    }

    public static void replaceAll(StringBuilder value, String oldValue, String newValue) {

        int indexOf = value.indexOf(oldValue);
        while (indexOf >= 0) {
            value.replace(indexOf, indexOf + oldValue.length(), newValue);
            indexOf = value.indexOf(oldValue);
        }
    }

    public static String replaceAll(String value, String oldValue, String newValue) {

        StringBuilder valueTemp = new StringBuilder(value);

        replaceAll(valueTemp, oldValue, newValue);

        return valueTemp.toString();
    }

    public static boolean isEmpty(String s) {

        return s == null || s.equals("");
    }

    public static boolean isTrue(Boolean value) {

        return !Objects.isNull(value) && value;
    }

    public static <T> T getValorFromField(Object entidade, Field field) {

        T valor = null;

        try {
            valor = (T) FieldUtils.readField(field, entidade, true);

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return valor;
    }

}
