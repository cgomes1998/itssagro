package br.com.sjc.util.anotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
public @interface ProjectionProperty {

    String[] values() default {};

    boolean sum() default false;

    boolean count() default false;

    boolean orderDesc() default false;

    boolean useForCountTotal() default false;

}
