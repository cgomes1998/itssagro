package br.com.sjc.util.encrypt;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class UtilEncoder {

    private static final String ENCRIPT_FORMATO = "UTF-8";

    private static final String ENCRIPT_TRANSFORMATION = "DESede/CBC/PKCS5Padding";

    private static final String ENCRIPT_ALGORITHM = "DESede";

    private static final String ENCRIPT_KEY_PHRASE = "9b204ac6-82ec-46af-b20b-0c914996bbc1";

    private Cipher ecipher;

    private Cipher dcipher;

    private SecretKey key;

    private IvParameterSpec iv;

    private UtilEncoder() {

        super();

        this.init();
    }

    private static final UtilEncoder encoder = new UtilEncoder();

    public static UtilEncoder get() {

        return UtilEncoder.encoder;
    }

    public void init() {

        try {

            final DESedeKeySpec keySpec = new DESedeKeySpec(UtilEncoder.ENCRIPT_KEY_PHRASE.getBytes());

            this.key = SecretKeyFactory.getInstance(UtilEncoder.ENCRIPT_ALGORITHM).generateSecret(keySpec);

            this.iv = new IvParameterSpec(new byte[8]);

            this.ecipher = Cipher.getInstance(UtilEncoder.ENCRIPT_TRANSFORMATION);

            this.dcipher = Cipher.getInstance(UtilEncoder.ENCRIPT_TRANSFORMATION);

            this.ecipher.init(Cipher.ENCRYPT_MODE, this.key, this.iv);

            this.dcipher.init(Cipher.DECRYPT_MODE, this.key, this.iv);

        } catch (final Exception e) {

        }
    }

    @SuppressWarnings("restriction")
    public String encrypt(final String str) {

        try {

            final byte[] utf8 = str.trim().getBytes(UtilEncoder.ENCRIPT_FORMATO);

            final byte[] enc = this.ecipher.doFinal(utf8);

            return new sun.misc.BASE64Encoder().encode(enc);

        } catch (final Exception e) {

            return str;
        }
    }

    public File decodeBase64ToFile(String base64, String nomeArquivo) throws IOException {

        String arquivo = System.getProperty("java.io.tmpdir") + File.separator + nomeArquivo;

        byte[] decodedImg = Base64.decodeBase64(base64.getBytes(StandardCharsets.UTF_8));

        Path destinationFile = Paths.get(arquivo);

        Files.write(destinationFile, decodedImg);

        return new File(arquivo);
    }
}