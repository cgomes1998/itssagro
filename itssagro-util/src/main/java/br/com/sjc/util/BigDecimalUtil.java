package br.com.sjc.util;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;

@Slf4j
public class BigDecimalUtil {
    
    public static BigDecimal CEM = BigDecimal.valueOf(100l);
    
    public static boolean isMaiorQueZero(final BigDecimal valor) {
        
        return valor != null && valor.intValue() > 0;
    }
    
    public static boolean isMaiorZero(final BigDecimal valor) {
        
        return ObjetoUtil.isNotNull(valor) && valor.doubleValue() > 0;
    }
    
    public static boolean isMenorIgualQue(final BigDecimal valor1, final BigDecimal valor2) {
        
        return (isMaiorZero(valor1) && isMaiorZero(valor2)) && (valor1.doubleValue() < valor2.doubleValue() || valor1.doubleValue() == valor2.doubleValue());
    }
    
    public static boolean isMaiorIgualQue(final BigDecimal valor1, final BigDecimal valor2) {
        
        return (isMaiorZero(valor1) && isMaiorZero(valor2)) && (valor1.doubleValue() > valor2.doubleValue() || valor1.doubleValue() == valor2.doubleValue());
    }
    
    public static boolean isMenorQue(final BigDecimal valor1, final BigDecimal valor2) {
        
        return (isMaiorZero(valor1) && isMaiorZero(valor2)) && valor1.doubleValue() < valor2.doubleValue();
    }
    
    public static BigDecimal subtract(BigDecimal valor1, BigDecimal valor2) {
    
        try {
    
            if (ObjetoUtil.isNull(valor1)) {
                valor1 = BigDecimal.ZERO;
            }
    
            if (ObjetoUtil.isNull(valor2)) {
                valor2 = BigDecimal.ZERO;
            }
    
            return valor1.subtract(valor2);
            
        } catch (Exception e) {
            
            log.error("Erro ao realizar subtração de valores", e);
            
            return BigDecimal.ZERO;
        }
    }
    
    public static boolean isMaiorQue(BigDecimal valor1, BigDecimal valor2) {
        
        if (ObjetoUtil.isNull(valor1)) {
            valor1 = BigDecimal.ZERO;
        }
    
        if (ObjetoUtil.isNull(valor2)) {
            valor2 = BigDecimal.ZERO;
        }
        
        return (isMaiorZero(valor1) && isMaiorZero(valor2)) && valor1.doubleValue() > valor2.doubleValue();
    }
    
    public static BigDecimal positivo(BigDecimal valor) {
        
        if (ObjetoUtil.isNull(valor)) {
            return BigDecimal.ZERO;
        }
    
        return new BigDecimal(Math.abs(valor.doubleValue()));
    }
    
    public static BigDecimal stringParaBigDecimal2Decimal(String valor) {
        
        NumberFormat format = NumberFormat.getInstance(new Locale("pt", "BR"));
        
        Number number = null;
        
        try {
            
            if (valor.contains(".")) {
                
                return new BigDecimal(valor).setScale(2, RoundingMode.UNNECESSARY);
            }
            
            number = format.parse(valor);
            
        } catch (Exception e) {
            
            return BigDecimal.ZERO;
        }
        
        return new BigDecimal(number.doubleValue()).setScale(2, RoundingMode.UNNECESSARY);
    }
    
    public static Object converterStringParaDigitoSeForNumerico(final String valor) {
        
        NumberFormat format = NumberFormat.getInstance(new Locale("pt", "BR"));
        Number number = null;
        try {
            if (valor.contains(".")) {
                return new BigDecimal(valor);
            }
            number = format.parse(valor);
        } catch (Exception e) {
            return valor;
        }
        return new BigDecimal(number.doubleValue());
    }
    
}
