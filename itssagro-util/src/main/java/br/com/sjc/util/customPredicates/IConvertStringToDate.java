package br.com.sjc.util.customPredicates;

import org.hibernate.criterion.MatchMode;
import org.hibernate.query.criteria.internal.CriteriaBuilderImpl;
import org.hibernate.query.criteria.internal.ParameterRegistry;
import org.hibernate.query.criteria.internal.Renderable;
import org.hibernate.query.criteria.internal.compile.RenderingContext;
import org.hibernate.query.criteria.internal.expression.LiteralExpression;
import org.hibernate.query.criteria.internal.predicate.AbstractSimplePredicate;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import java.io.Serializable;

/**
 * Created by bruno.andrade on 27/11/2020.
 */
public class IConvertStringToDate extends AbstractSimplePredicate implements Serializable {

    private final Expression<String> matchExpression;

    private final Expression<String> pattern;

    public static IConvertStringToDate build(CriteriaBuilder criteriaBuilder, Expression<String> matchExpression, String[] pattern) {

        return build(criteriaBuilder, matchExpression, pattern, MatchMode.EXACT);
    }

    public static IConvertStringToDate build(CriteriaBuilder criteriaBuilder, Expression<String> matchExpression, String[] pattern, MatchMode matchMode) {

        return new IConvertStringToDate((CriteriaBuilderImpl) criteriaBuilder, matchExpression, pattern, matchMode);
    }

    private IConvertStringToDate(CriteriaBuilderImpl criteriaBuilder, Expression<String> matchExpression, String[] pattern, MatchMode matchMode) {

        super(criteriaBuilder);

        this.matchExpression = matchExpression;
        this.pattern = new LiteralExpression<>(criteriaBuilder, matchMode.toMatchString(pattern[0]));
    }

    @Override
    public void registerParameters(ParameterRegistry registry) {

        Helper.possibleParameter(null, registry);
        Helper.possibleParameter(this.matchExpression, registry);
        Helper.possibleParameter(this.pattern, registry);
    }

    @Override
    public String render(boolean isNegated, RenderingContext renderingContext) {

        final String operator = isNegated ? " not between " : " between ";
        StringBuilder buider = new StringBuilder();
        buider.append(((Renderable) this.matchExpression).render(renderingContext)).append("::::date").append(operator).append(this.pattern);

        return buider.toString();
    }

}
