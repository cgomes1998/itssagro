package br.com.sjc.util.email;

/**
 * Created by julio.bueno on 06/06/2019.
 */
public enum ConfiguracaoEmailProtocolo {
    SSL, TLS
}
