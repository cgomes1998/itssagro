package br.com.sjc.util;

import lombok.Getter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Objects;

@Getter
public class RegraPersistencia {

    private String key;

    private Specification<?> specification;

    private RegraPersistencia(String key, Specification<?> specification) {

        this.key = key;
        this.specification = specification;
    }

    public static RegraPersistencia build(String key) {

        return build(key, null);
    }

    public static RegraPersistencia build(String key, Specification<?> specification) {

        return new RegraPersistencia(key, specification);
    }

    public boolean hasSpecification() {

        return !Objects.isNull(specification);
    }

    @SuppressWarnings("unchecked")
    public Predicate getPredicate(CriteriaBuilder criteriaBuilder, CriteriaQuery criteriaQuery, Root root) {

        return specification.toPredicate(root, criteriaQuery, criteriaBuilder);
    }

    public interface RegraNegocioPredicateFactory {

        Predicate createPredicate(CriteriaBuilder criteriaBuilder, CriteriaQuery criteriaQuery, Root root);

    }

}
