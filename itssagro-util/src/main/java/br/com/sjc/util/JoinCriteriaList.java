package br.com.sjc.util;

import javax.persistence.criteria.JoinType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class JoinCriteriaList implements Iterable<JoinCriteria> {

    private List<JoinCriteria> joins;

    public JoinCriteriaList() {

        joins = new ArrayList<>();
    }

    public void addJoin(JoinCriteria join) {

        if (!join.possuiJoinTipo()) {
            join.definirJoinTipoInnerJoin();
        }

        if (podeAdicionarJoin(join)) {
            joins.add(join);
        }
    }

    public void addJoins(Collection<JoinCriteria> joins) {

        joins.forEach(this::addJoin);
    }

    public void addJoin(String associationPath, String alias) {

        addJoin(associationPath, alias, JoinType.INNER);
    }

    public void addJoin(String associationPath, String alias, JoinType joinTipo) {

        addJoin(new JoinCriteria(associationPath, alias, joinTipo));
    }

    private boolean podeAdicionarJoin(JoinCriteria join) {

        return !joins.contains(join);
    }

    public boolean hasJoins() {

        return !joins.isEmpty();
    }

    @Override
    public String toString() {

        StringBuilder joinsString = new StringBuilder();

        joins.forEach(join -> joinsString.append(join).append("\n"));

        return joinsString.toString();
    }

    @Override
    public Iterator<JoinCriteria> iterator() {

        return new Iterator<JoinCriteria>() {

            int index = 0;

            @Override
            public boolean hasNext() {

                return index < joins.size();
            }

            @Override
            public JoinCriteria next() {

                return joins.get(index++);
            }
        };
    }

}
