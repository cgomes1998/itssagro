package br.com.sjc.util;

import lombok.Getter;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by julio.bueno on 09/04/2019.
 */
@Getter
public class RegraPersistenciaList implements Iterable<RegraPersistencia> {

    private List<RegraPersistencia> regrasPersistencia;

    public static RegraPersistenciaList build() {

        return new RegraPersistenciaList();
    }

    private RegraPersistenciaList() {

        regrasPersistencia = new ArrayList<>();
    }

    public RegraPersistenciaList add(String mensagem) {

        return add(RegraPersistencia.build(mensagem));
    }

    public RegraPersistenciaList add(String messageKey, Specification<?> specification) {

        return add(RegraPersistencia.build(messageKey, specification));
    }

    public RegraPersistenciaList add(RegraPersistencia regraPersistencia) {

        regrasPersistencia.add(regraPersistencia);
        return this;
    }

    @Override
    public Iterator<RegraPersistencia> iterator() {

        return new Iterator<RegraPersistencia>() {

            int index = 0;

            @Override
            public boolean hasNext() {

                return index < regrasPersistencia.size();
            }

            @Override
            public RegraPersistencia next() {

                return regrasPersistencia.get(index++);
            }
        };
    }

}
