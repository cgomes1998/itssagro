package br.com.sjc.portal;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("api")
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
    	
    	this.packages("br.com.sjc.rest");
    	
    	this.register(MultiPartFeature.class);
    	
    }
}
