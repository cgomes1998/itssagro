package br.com.sjc.portal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.quartz.QuartzAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.TimeZone;

@SpringBootApplication(scanBasePackages = ItssAuthApplication.BASE_COMPONENT)
@EnableScheduling
@EnableFeignClients("br.com.sjc.fachada.service.cargaPontual")
public class ItssAuthApplication {

    public static final String BASE_COMPONENT = "br.com.sjc";

    public static void main(String[] args) {

        SpringApplication.run(ItssAuthApplication.class, args);
    }

}
