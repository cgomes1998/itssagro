package br.com.sjc.portal;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigurationFilter {

    @Bean
    public FilterRegistrationBean<CORSFilter> registrarCORSFilter() {

        final FilterRegistrationBean<CORSFilter> registration = new FilterRegistrationBean<>();

        registration.setFilter(new CORSFilter());

        registration.addUrlPatterns("/*");

        registration.setName("CORSFilter");

        registration.setOrder(1);

        return registration;
    }
}
