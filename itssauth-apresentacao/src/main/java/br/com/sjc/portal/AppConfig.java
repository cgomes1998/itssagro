package br.com.sjc.portal;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import org.springframework.http.client.SimpleClientHttpRequestFactory;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.net.HttpURLConnection;

@Configuration
public class AppConfig {

    @Bean
    public RestTemplate restTemplate() throws IOException {
        RestTemplate restTemplate = new RestTemplate();

        // Configurar o RestTemplate para confiar em todos os certificados (apenas para fins de demonstração, não é recomendado em produção)
        HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> true);

        // Configurar o RestTemplate para usar uma fábrica de solicitação que aceita todos os certificados (apenas para fins de demonstração, não é recomendado em produção)
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory() {
            @Override
            protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
                if (connection instanceof HttpsURLConnection) {
                    ((HttpsURLConnection) connection).setHostnameVerifier((hostname, session) -> true);
                }
                super.prepareConnection(connection, httpMethod);
            }
        };
        restTemplate.setRequestFactory(factory);

        return restTemplate;
    }
}