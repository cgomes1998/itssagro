package br.com.sjc.dynamicReport.formulario;

import br.com.sjc.modelo.Formulario;
import br.com.sjc.modelo.FormularioItem;
import br.com.sjc.modelo.SenhaFormularioResposta;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.builder.component.*;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalTextAlignment;
import net.sf.dynamicreports.report.constant.VerticalTextAlignment;
import net.sf.jasperreports.engine.JREmptyDataSource;

import java.awt.*;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by Claudio Gomes
 */
@Slf4j
@RequiredArgsConstructor(staticName = "build")
public class FormularioGerarPdfComResposta {

    @NonNull
    private final Formulario formulario;

    @NonNull
    private final Romaneio romaneio;

    @NonNull
    private final List<SenhaFormularioResposta> respostas;

    private final JasperReportBuilder jasperReportBuilder = DynamicReports.report();

    public static final int WIDTH = 200;

    private static final int VERTICAL_HEIGHT = 3;

    private static final int TAMANHO_MININO_INPUT = 30;

    private final Date dataAtual = new Date();

    private final String PATTERN_RESPOSTA_DATA = "___/___/______";

    private final String PATTERN_RESPOSTA_HORA = "___:___";

    private final String IMG_CHECKBOX = "/img/checkbox.jpg";

    private final String IMG_CHECKBOX_SELECIONADO = "/img/checkbox-selecionado.jpg";

    private final String IMG_RADIO = "/img/radio.jpg";

    private final String IMG_RADIO_SELECIONADO = "/img/radio-selecionado.jpg";

    private StyleBuilder estiloSessao;

    private StyleBuilder estiloLabel;

    private StyleBuilder estiloLeftPadding5;

    private StyleBuilder estiloLinhaResposta;

    private StyleBuilder estiloObservacao;

    public void gerarPdf(OutputStream output) {

        criarEstilosDefault();

        definirTitulo();

        MultiPageListBuilder detail = DynamicReports.cmp.multiPageList();

        adicionarDadosVeiculo(detail);
        adicionarDadosMotorista(detail);
        adicionarDadosCliente(detail);
        adicionarDadosTransportadora(detail);
        adicionarItensFormulario(detail);

        jasperReportBuilder.detail(detail);

        jasperReportBuilder.setDataSource(new JREmptyDataSource(1));

        exportarPdf(output);
    }

    private void criarEstilosDefault() {

        criarEstiloLabel();
        criarEstiloSessao();
        criarEstiloLeftPadding5();
        criarEstiloLinhaResposta();
        criarEstiloObservacao();
    }

    private void criarEstiloSessao() {

        estiloSessao = DynamicReports.stl.style()
                .setTopPadding(3)
                .setBottomPadding(3);
    }

    private void criarEstiloLabel() {

        estiloLabel = DynamicReports.stl.style().setFontSize(7);
    }

    private void criarEstiloLeftPadding5() {

        estiloLeftPadding5 = DynamicReports.stl.style().setLeftPadding(5);
    }

    private void criarEstiloLinhaResposta() {

        estiloLinhaResposta = DynamicReports.stl.style().setBottomBorder(DynamicReports.stl.pen1Point()).setFontSize(7);
    }

    private void criarEstiloObservacao() {

        estiloObservacao = DynamicReports.stl.style()
                .setFontSize(7)
                .setForegroundColor(new Color(245, 123, 32));
    }

    private void definirTitulo() {

        StyleBuilder styleLabelFormulario = DynamicReports.stl.style()
                .setFontSize(7)
                .setTextAlignment(HorizontalTextAlignment.CENTER, VerticalTextAlignment.BOTTOM)
                .setLeftPadding(80);

        StyleBuilder styleDescricaoFormulario = DynamicReports.stl.style()
                .setFontSize(7)
                .setTextAlignment(HorizontalTextAlignment.CENTER, VerticalTextAlignment.TOP)
                .setLeftPadding(80);

        StyleBuilder styleTitle = DynamicReports.stl.style()
                .setFontSize(8)
                .setBottomBorder(DynamicReports.stl.pen1Point());

        jasperReportBuilder.title(
                DynamicReports.cmp.horizontalFlowList().setStyle(styleTitle).add(
                        DynamicReports.cmp.image(FormularioGerarPdfComResposta.class.getResource("/img/logo.png")).setFixedDimension(148, 36),
                        DynamicReports.cmp.verticalList().add(
                                DynamicReports.cmp.text("Formulário").setStyle(styleLabelFormulario),
                                DynamicReports.cmp.text(formulario.getDescricao()).setStyle(styleDescricaoFormulario)
                        ),
                        DynamicReports.cmp.verticalList().add(
                                DynamicReports.cmp.text("Data Emissão").setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT),
                                DynamicReports.cmp.text(DateUtil.formatTo(dataAtual, "dd/MM/YYYY")).setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT),
                                DynamicReports.cmp.text("Hora Emissão").setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT),
                                DynamicReports.cmp.text(DateUtil.formatTo(dataAtual, "HH:mm")).setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT)
                        )
                ));
    }

    private void adicionarDadosMotorista(MultiPageListBuilder detail) {

        if (!Objects.isNull(formulario.getCabecalhoDadosMotorista()) && formulario.getCabecalhoDadosMotorista()) {

            final Motorista motorista = romaneio.getItens().stream().map(ItemNFPedido::getMotorista).filter(Objects::nonNull).findFirst().orElse(null);

            MultiPageListBuilder dadosMorotista = DynamicReports.cmp.multiPageList();

            dadosMorotista.add(
                    getTitutloSessao("Dados do Motorista"),
                    DynamicReports.cmp.horizontalList(
                            getLabel("Nome"),
                            getLabel("CPF", estiloLeftPadding5),
                            getLabel("CNH", estiloLeftPadding5)
                    ),
                    DynamicReports.cmp.horizontalList(
                            getLinhaParaResposta(ObjetoUtil.isNotNull(motorista) ? motorista.getNome().toUpperCase() : StringUtil.empty()),
                            getLinhaParaResposta(estiloLeftPadding5, ObjetoUtil.isNotNull(motorista) && StringUtil.isNotNullEmpty(motorista.getCpf()) ? StringUtil.format(motorista.getCpf(), "###.###.###-##") : StringUtil.empty()),
                            getLinhaParaResposta(estiloLeftPadding5, ObjetoUtil.isNotNull(motorista) ? motorista.getCnh() : StringUtil.empty())
                    ),
                    DynamicReports.cmp.verticalGap(VERTICAL_HEIGHT),
                    DynamicReports.cmp.horizontalList(
                            getLabel("Data Validade CNH"),
                            getLabel("Data Treinamento", estiloLeftPadding5),
                            getLabel("Data Validade Treinamento", estiloLeftPadding5)
                    ),
                    DynamicReports.cmp.horizontalList(
                            ObjetoUtil.isNotNull(motorista) && ObjetoUtil.isNotNull(motorista.getDataValidadeCNH()) ? getLinhaParaResposta(DateUtil.format("dd/MM/yyyy", motorista.getDataValidadeCNH())) : getLinhaParaRespostaPattern(PATTERN_RESPOSTA_DATA),
                            ObjetoUtil.isNotNull(motorista) && ObjetoUtil.isNotNull(motorista.getDataTreinamento()) ? getLinhaParaResposta(estiloLeftPadding5, DateUtil.format("dd/MM/yyyy", motorista.getDataTreinamento())) : getLinhaParaRespostaPattern(PATTERN_RESPOSTA_DATA, estiloLeftPadding5),
                            ObjetoUtil.isNotNull(motorista) && ObjetoUtil.isNotNull(motorista.getDataValidadeTreinamento()) ? getLinhaParaResposta(estiloLeftPadding5, DateUtil.format("dd/MM/yyyy", motorista.getDataValidadeTreinamento())) : getLinhaParaRespostaPattern(PATTERN_RESPOSTA_DATA, estiloLeftPadding5)
                    ),
                    DynamicReports.cmp.verticalGap(VERTICAL_HEIGHT),
                    DynamicReports.cmp.horizontalList(
                            DynamicReports.cmp.image(FormularioGerarPdfComResposta.class.getResource(IMG_CHECKBOX)).setFixedDimension(15, 15).setWidth(15),
                            getLabel("Possui permissão para carga perigosa")
                    )
            );

            detail.add(dadosMorotista);
        }
    }

    private void adicionarDadosVeiculo(MultiPageListBuilder detail) {

        if (!Objects.isNull(formulario.getCabecalhoDadosVeiculo()) && formulario.getCabecalhoDadosVeiculo()) {

            final Transportadora transportadora = romaneio.getItens().stream().map(ItemNFPedido::getTransportadora).filter(Objects::nonNull).findFirst().orElse(null);

            final Veiculo veiculo = romaneio.getItens().stream().map(ItemNFPedido::getPlacaCavalo).filter(Objects::nonNull).findFirst().orElse(null);

            final String carretaUm = romaneio.getItens().stream().filter(i -> StringUtil.isNotNullEmpty(i.getPlacaCarretaUm())).map(i -> i.getPlacaCarretaUm()).findFirst().orElse(null);

            final String carretaDois = romaneio.getItens().stream().filter(i -> StringUtil.isNotNullEmpty(i.getPlacaCarretaDois())).map(i -> i.getPlacaCarretaDois()).findFirst().orElse(null);

            BigDecimal totalSeta = BigDecimal.ZERO;

            if (ObjetoUtil.isNotNull(veiculo) && ObjetoUtil.isNotNull(veiculo.getTipo()) && ColecaoUtil.isNotEmpty(veiculo.getTipo().getSetas())) {

                totalSeta = romaneio.getItens().stream().map(ItemNFPedido::getTipoSeta).filter(Objects::nonNull).map(TipoVeiculoSeta::getQtdValida).findFirst().orElse(BigDecimal.ZERO);
            }

            MultiPageListBuilder dadosDoVeiculo = DynamicReports.cmp.multiPageList();

            dadosDoVeiculo.add(
                    getTitutloSessao("Dados do Veículo"),
                    DynamicReports.cmp.horizontalList(
                            getLabel("Transportadora"),
                            getLabel("Seta Total (Somatório das Setas)", estiloLeftPadding5),
                            getLabel("", estiloLeftPadding5)
                    ),
                    DynamicReports.cmp.horizontalList(
                            getLinhaParaResposta(ObjetoUtil.isNotNull(transportadora) ? transportadora.getDescricao().toUpperCase() : StringUtil.empty()),
                            getLinhaParaResposta(estiloLeftPadding5, StringUtil.toMilhar(totalSeta, 0)),
                            getLabel("", estiloLeftPadding5)
                    ),
                    DynamicReports.cmp.verticalGap(VERTICAL_HEIGHT),
                    DynamicReports.cmp.horizontalList(
                            getLabel("Placa Cavalo"),
                            getLabel("Placa Carreta 1", estiloLeftPadding5),
                            getLabel("Placa Carreta 2", estiloLeftPadding5)
                    ),
                    DynamicReports.cmp.horizontalList(
                            getLinhaParaResposta(ObjetoUtil.isNotNull(veiculo) ? StringUtil.toPlaca(veiculo.getPlaca1()) : StringUtil.empty()),
                            getLinhaParaResposta(estiloLeftPadding5, carretaUm),
                            getLinhaParaResposta(estiloLeftPadding5, carretaDois)
                    ),
                    DynamicReports.cmp.verticalGap(VERTICAL_HEIGHT),
                    DynamicReports.cmp.horizontalList(
                            getLabel("Data CIV Placa Cavalo"),
                            getLabel("Data CIV Placa Carreta 1", estiloLeftPadding5),
                            getLabel("Data CIV Plava Carreta 2", estiloLeftPadding5)
                    ),
                    DynamicReports.cmp.horizontalList(
                            ObjetoUtil.isNotNull(veiculo) && ObjetoUtil.isNotNull(veiculo.getDataCivPlaca1()) ? getLinhaParaResposta(DateUtil.format("dd/MM/yyyy", veiculo.getDataCivPlaca1())) : getLinhaParaRespostaPattern(PATTERN_RESPOSTA_DATA),
                            ObjetoUtil.isNotNull(veiculo) && ObjetoUtil.isNotNull(veiculo.getDataCivPlaca2()) ? getLinhaParaResposta(estiloLeftPadding5, DateUtil.format("dd/MM/yyyy", veiculo.getDataCivPlaca2())) : getLinhaParaRespostaPattern(PATTERN_RESPOSTA_DATA, estiloLeftPadding5),
                            ObjetoUtil.isNotNull(veiculo) && ObjetoUtil.isNotNull(veiculo.getDataCivPlaca3()) ? getLinhaParaResposta(estiloLeftPadding5, DateUtil.format("dd/MM/yyyy", veiculo.getDataCivPlaca3())) : getLinhaParaRespostaPattern(PATTERN_RESPOSTA_DATA, estiloLeftPadding5)
                    ),
                    DynamicReports.cmp.verticalGap(VERTICAL_HEIGHT),
                    DynamicReports.cmp.horizontalList(
                            getLabel("Data Calibragem Placa Cavalo"),
                            getLabel("Data Calibragem Placa Carreta 1", estiloLeftPadding5),
                            getLabel("Data Calibragem Placa Carreta 2", estiloLeftPadding5)
                    ),
                    DynamicReports.cmp.horizontalList(
                            ObjetoUtil.isNotNull(veiculo) && ObjetoUtil.isNotNull(veiculo.getDataCalibragemPlaca1()) ? getLinhaParaResposta(DateUtil.format("dd/MM/yyyy", veiculo.getDataCalibragemPlaca1())) : getLinhaParaRespostaPattern(PATTERN_RESPOSTA_DATA),
                            ObjetoUtil.isNotNull(veiculo) && ObjetoUtil.isNotNull(veiculo.getDataCalibragemPlaca2()) ? getLinhaParaResposta(estiloLeftPadding5, DateUtil.format("dd/MM/yyyy", veiculo.getDataCalibragemPlaca2())) : getLinhaParaRespostaPattern(PATTERN_RESPOSTA_DATA, estiloLeftPadding5),
                            ObjetoUtil.isNotNull(veiculo) && ObjetoUtil.isNotNull(veiculo.getDataCalibragemPlaca3()) ? getLinhaParaResposta(estiloLeftPadding5, DateUtil.format("dd/MM/yyyy", veiculo.getDataCalibragemPlaca3())) : getLinhaParaRespostaPattern(PATTERN_RESPOSTA_DATA, estiloLeftPadding5)
                    ),
                    DynamicReports.cmp.verticalGap(VERTICAL_HEIGHT),
                    DynamicReports.cmp.horizontalList(
                            getLabel("Data Venc. CR"),
                            getLabel("Data Venc. AATIPP", estiloLeftPadding5),
                            getLabel("", estiloLeftPadding5)
                    ),
                    DynamicReports.cmp.horizontalList(
                            ObjetoUtil.isNotNull(veiculo) && ObjetoUtil.isNotNull(veiculo.getDataVencimentoCr()) ? getLinhaParaResposta(DateUtil.format("dd/MM/yyyy", veiculo.getDataVencimentoCr())) : getLinhaParaRespostaPattern(PATTERN_RESPOSTA_DATA),
                            ObjetoUtil.isNotNull(veiculo) && ObjetoUtil.isNotNull(veiculo.getDataVencimentoAatipp()) ? getLinhaParaResposta(estiloLeftPadding5, DateUtil.format("dd/MM/yyyy", veiculo.getDataVencimentoAatipp())) : getLinhaParaRespostaPattern(PATTERN_RESPOSTA_DATA, estiloLeftPadding5),
                            getLabel("", estiloLeftPadding5)
                    )
            );

            detail.add(dadosDoVeiculo);
        }
    }

    private void adicionarDadosCliente(MultiPageListBuilder detail) {

        if (!Objects.isNull(formulario.getCabecalhoDadosCliente()) && formulario.getCabecalhoDadosCliente()) {

            MultiPageListBuilder dadosDoCliente = DynamicReports.cmp.multiPageList();

            dadosDoCliente.add(
                    getTitutloSessao("Dados do Cliente")
            );

            romaneio.getItens().stream().map(ItemNFPedido::getCliente).filter(distinctByKey(Cliente::getId)).forEach(cliente -> {
                dadosDoCliente.add(
                        DynamicReports.cmp.horizontalList(
                                getLabel("Código", estiloLabel.setFontSize(7)),
                                getLabel("Nome", estiloLeftPadding5.setFontSize(7)),
                                getLabel("Inscrição Estadual", estiloLeftPadding5.setFontSize(7))
                        ),
                        DynamicReports.cmp.horizontalList(
                                getLinhaParaResposta(cliente.getCodigo()),
                                getLinhaParaResposta(estiloLeftPadding5, cliente.getNome().toUpperCase()),
                                getLinhaParaResposta(estiloLeftPadding5, cliente.getInscricaoEstadual())
                        ),
                        DynamicReports.cmp.verticalGap(VERTICAL_HEIGHT),
                        DynamicReports.cmp.horizontalList(
                                getLabel("Endereço", estiloLabel.setFontSize(7)),
                                getLabel("Município", estiloLeftPadding5),
                                getLabel("UF", estiloLeftPadding5)
                        ),
                        DynamicReports.cmp.horizontalList(
                                getLinhaParaResposta(StringUtil.isNotNullEmpty(cliente.getEndereco()) ? cliente.getEndereco().toUpperCase() : StringUtil.empty()),
                                getLinhaParaResposta(estiloLeftPadding5, StringUtil.isNotNullEmpty(cliente.getMunicipio()) ? cliente.getMunicipio().toUpperCase() : StringUtil.empty()),
                                getLinhaParaResposta(estiloLeftPadding5, ObjetoUtil.isNotNull(cliente.getUf()) ? cliente.getUf().getSigla() : StringUtil.empty())
                        )
                );
            });

            detail.add(dadosDoCliente);
        }
    }

    private void adicionarDadosTransportadora(MultiPageListBuilder detail) {

        if (!Objects.isNull(formulario.getCabecalhoDadosTransportadora()) && formulario.getCabecalhoDadosTransportadora()) {

            MultiPageListBuilder dadosDaTransportadora = DynamicReports.cmp.multiPageList();

            final Transportadora transportadora = romaneio.getItens().stream().map(ItemNFPedido::getTransportadora).filter(Objects::nonNull).findFirst().orElse(null);

            dadosDaTransportadora.add(
                    getTitutloSessao("Dados da Transportadora"),
                    DynamicReports.cmp.horizontalList(
                            getLabel("Código"),
                            getLabel("Nome", estiloLeftPadding5)
                    ),
                    DynamicReports.cmp.horizontalList(
                            getLinhaParaResposta(transportadora.getCodigo()),
                            getLinhaParaResposta(estiloLeftPadding5, StringUtil.isNotNullEmpty(transportadora.getDescricao()) ? transportadora.getDescricao().toUpperCase() : StringUtil.empty())
                    )
            );

            detail.add(dadosDaTransportadora);
        }
    }

    private void adicionarItensFormulario(MultiPageListBuilder detail) {

        MultiPageListBuilder itensDoFormulario = DynamicReports.cmp.multiPageList();

        Map<Integer, List<FormularioItem>> itensPorLinha = agruparItensPorLinhas(formulario);

        itensPorLinha.forEach((linha, itens) -> {
            VerticalListBuilder itensVerticalListBuilder = DynamicReports.cmp.verticalList();

            HorizontalListBuilder labels = DynamicReports.cmp.horizontalList();
            HorizontalListBuilder observacoes = DynamicReports.cmp.horizontalList();
            HorizontalListBuilder respostas = DynamicReports.cmp.horizontalList();

            onItensForeach(itens, (index, item) -> {
                StyleBuilder style = index > 0 ? estiloLeftPadding5.setFontSize(7) : estiloLabel;
                int tamanho = item.getTamanho() * TAMANHO_MININO_INPUT;
                final String resposta = getRespostaTexto(item.getId());

                switch (item.getTipo()) {
                    case SESSAO:
                        labels.add(getTitutloSessao(item.getTitulo()));
                        break;
                    case TEXT:
                        labels.add(getLabel(item.getTitulo(), style.setFontSize(7)).setWidth(WIDTH));
                        respostas.add(getLinhaParaResposta(style, resposta).setWidth(tamanho));
                        break;
                    case TEXT_AREA:
                        List<String> textoQuebrado = StringUtil.quebrarStringPorTamanho(resposta, 175);
                        VerticalListBuilder[] verticalListBuilders = textoQuebrado
                                .stream()
                                .map(r -> getLinhaParaResposta(style, r))
                                .collect(Collectors.toList())
                                .toArray(new VerticalListBuilder[textoQuebrado.size()]);
                        labels.add(getLabel(item.getTitulo(), style.setFontSize(7)).setWidth(WIDTH));
                        respostas.add(DynamicReports.cmp.verticalList().add(verticalListBuilders));
                        break;
                    case RADIO:
                        labels.add(getLabel(item.getTitulo(), style.setFontSize(7)).setWidth(WIDTH), getObservacao("Marque apenas 1 opção"), getRespostasListaOpcoesRadioComResposta(item, resposta));
                        break;
                    case CHECKBOX:
                        labels.add(getLabel(item.getTitulo(), style.setFontSize(7)).setWidth(WIDTH), getRespostasListaOpcoesCheckboxComResposta(item, resposta));
                        break;
                    case SELECT:
                        labels.add(getLabel(item.getTitulo(), style.setFontSize(7)).setWidth(WIDTH), getObservacao("Marque apenas 1 opção"), getRespostasListaOpcoesRadioComResposta(item, resposta));
                        break;
                    case DATA:
                        labels.add(getLabel(item.getTitulo(), style.setFontSize(7)).setWidth(WIDTH), getLinhaParaRespostaPattern(StringUtil.isNotNullEmpty(resposta) ? resposta : PATTERN_RESPOSTA_DATA, style));
                        break;
                    case HORA:
                        labels.add(getLabel(item.getTitulo(), style.setFontSize(7)).setWidth(WIDTH), getLinhaParaRespostaPattern(StringUtil.isNotNullEmpty(resposta) ? resposta : PATTERN_RESPOSTA_HORA, style));
                        break;
                }
            });

            if (!itens.get(0).isSessao()) {
                itensVerticalListBuilder.add(DynamicReports.cmp.verticalGap(VERTICAL_HEIGHT));
            }

            itensVerticalListBuilder.add(labels, observacoes, respostas);

            itensDoFormulario.add(itensVerticalListBuilder);
        });

        detail.add(itensDoFormulario);
    }

    private String getRespostaTexto(final Long idItem) {
        return ColecaoUtil.isNotEmpty(this.respostas)
                ? this.respostas
                .stream()
                .filter(r -> r.getItem().getId().equals(idItem))
                .map(SenhaFormularioResposta::getResposta)
                .filter(Objects::nonNull)
                .findFirst()
                .orElse(StringUtil.empty())
                : StringUtil.empty();
    }

    private void onItensForeach(
            List<FormularioItem> itens,
            BiConsumer<Integer, FormularioItem> biConsumer
    ) {

        for (int index = 0; index < itens.size(); index++) {
            biConsumer.accept(index, itens.get(index));
        }
    }

    private ComponentBuilder getTitutloSessao(String titulo) {

        StyleBuilder styleTituloSessao = DynamicReports.stl.style()
                .setFontSize(8)
                .setForegroundColor(new Color(245, 123, 32));

        StyleBuilder styleBorderSessao = DynamicReports.stl.style()
                .setTopBorder(DynamicReports.stl.pen1Point());

        VerticalListBuilder tituloSessao = DynamicReports.cmp.verticalList();
        tituloSessao.setStyle(estiloSessao).add(
                DynamicReports.cmp.text(titulo).setStyle(styleTituloSessao.setFontSize(7)),
                DynamicReports.cmp.line().setStyle(styleBorderSessao)
        );
        return tituloSessao;
    }

    private VerticalListBuilder getLinhaParaResposta(String valor) {

        return getLinhaParaResposta(null, valor);
    }

    private VerticalListBuilder getLinhaParaResposta(
            StyleBuilder style,
            String valor
    ) {

        VerticalListBuilder verticalListBuilder = DynamicReports.cmp.verticalList();
        verticalListBuilder.add(DynamicReports.cmp.text(StringUtil.isNotNullEmpty(valor) ? valor : StringUtil.empty()).setStyle(estiloLinhaResposta));

        if (!Objects.isNull(style)) {
            verticalListBuilder.setStyle(style.setFontSize(7));
        }

        return verticalListBuilder;
    }

    private VerticalListBuilder getLinhaParaRespostaPattern(String pattern) {

        return getLinhaParaRespostaPattern(pattern, estiloLabel);
    }

    private VerticalListBuilder getLinhaParaRespostaPattern(
            String pattern,
            StyleBuilder style
    ) {

        VerticalListBuilder verticalListBuilder = DynamicReports.cmp.verticalList();
        verticalListBuilder.add(DynamicReports.cmp.text(pattern));

        if (!Objects.isNull(style)) {
            verticalListBuilder.setStyle(style.setFontSize(7));
        }

        return verticalListBuilder;
    }

    private HorizontalListBuilder getRespostasListaOpcoesRadioComResposta(
            FormularioItem item,
            String resposta
    ) {

        HorizontalListBuilder horizontalList = DynamicReports.cmp.horizontalList();

        item.getOpcoes().forEach(opcao -> {
            horizontalList.add(DynamicReports.cmp.horizontalList().add(
                    DynamicReports.cmp.image(FormularioGerarPdfComResposta.class.getResource(StringUtil.igualIgnoradoCaseSensitive(opcao.getTitulo(), resposta) ? IMG_RADIO_SELECIONADO : IMG_RADIO)).setFixedDimension(10, 10).setWidth(10),
                    DynamicReports.cmp.text(opcao.getTitulo()).setStyle(estiloLabel.setFontSize(7))
            ));
        });

        return horizontalList;
    }

    private HorizontalListBuilder getRespostasListaOpcoesCheckboxComResposta(
            FormularioItem item,
            String resposta
    ) {

        HorizontalListBuilder horizontalList = DynamicReports.cmp.horizontalList();

        item.getOpcoes().forEach(opcao -> {
            horizontalList.add(DynamicReports.cmp.horizontalList().add(
                    DynamicReports.cmp.image(FormularioGerarPdfComResposta.class.getResource(respostaCheckboxSelecionada(opcao.getTitulo(), resposta) ? IMG_CHECKBOX_SELECIONADO : IMG_CHECKBOX)).setFixedDimension(10, 10).setWidth(10),
                    DynamicReports.cmp.text(opcao.getTitulo()).setStyle(estiloLabel.setFontSize(7))
            ));
        });

        return horizontalList;
    }

    private boolean respostaCheckboxSelecionada(String opcao, String resposta) {
        boolean selecionada = false;
        ObjectMapper mapper = new ObjectMapper();
        try {
            String[] opcoesSelecionadas = mapper.readValue(resposta, String[].class);
            selecionada = Arrays.stream(opcoesSelecionadas).anyMatch(o -> StringUtil.igualIgnoradoCaseSensitive(o.trim(), opcao.trim()));
        } catch (IOException e) {
            log.error("Não foi possivel ler o json das opções selecionados do formulario", e);
        }
        return selecionada;
    }

    private HorizontalListBuilder getRespostasListaOpcoes(
            FormularioItem item,
            String imgOpcao
    ) {

        HorizontalListBuilder horizontalList = DynamicReports.cmp.horizontalList();

        item.getOpcoes().forEach(opcao -> {
            horizontalList.add(DynamicReports.cmp.horizontalList().add(
                    DynamicReports.cmp.image(FormularioGerarPdfComResposta.class.getResource(imgOpcao)).setFixedDimension(10, 10).setWidth(10),
                    DynamicReports.cmp.text(opcao.getTitulo()).setStyle(estiloLabel.setFontSize(7))
            ));
        });

        return horizontalList;
    }

    private VerticalListBuilder getLabel(String label) {

        return getLabel(label, estiloLabel);
    }

    private VerticalListBuilder getLabel(
            String label,
            StyleBuilder style
    ) {

        VerticalListBuilder verticalListBuilder = DynamicReports.cmp.verticalList();
        verticalListBuilder.add(DynamicReports.cmp.text(label).setStyle(estiloLabel));

        if (!Objects.isNull(style)) {
            verticalListBuilder.setStyle(style.setFontSize(7));
        }

        return verticalListBuilder;
    }

    private TextFieldBuilder getObservacao(String observacao) {

        return DynamicReports.cmp.text(observacao).setStyle(estiloObservacao.setFontSize(7));
    }

    private void exportarPdf(OutputStream output) {

        try {
            jasperReportBuilder.toPdf(DynamicReports.export.pdfExporter(output));

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    private Map<Integer, List<FormularioItem>> agruparItensPorLinhas(Formulario formulario) {

        Map<Integer, List<FormularioItem>> itensPorLinha = new HashMap<>();

        AtomicInteger linha = new AtomicInteger(1);
        AtomicInteger colunas = new AtomicInteger(0);

        formulario.getItens()
                .forEach(item -> {
                    if (colunas.addAndGet(item.getTamanho()) > 12) {
                        colunas.set(item.getTamanho());
                        linha.incrementAndGet();
                    }

                    if (Objects.isNull(itensPorLinha.get(linha.get()))) {
                        itensPorLinha.put(linha.get(), new ArrayList<>());
                    }

                    itensPorLinha.get(linha.get()).add(item);
                });

        return itensPorLinha;
    }

    public static <T> java.util.function.Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {

        Map<Object, Boolean> map = new ConcurrentHashMap<>();

        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
}
