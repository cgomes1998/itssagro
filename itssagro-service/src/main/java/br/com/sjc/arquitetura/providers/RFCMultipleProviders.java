package br.com.sjc.arquitetura.providers;

import br.com.sjc.modelo.cfg.Ambiente;
import br.com.sjc.modelo.sap.arquitetura.jco.*;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.sync.SAPSync;
import br.com.sjc.service.SAPServiceImpl;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import com.sap.conn.jco.*;
import com.sap.conn.jco.ext.*;
import com.sap.conn.jco.server.DefaultServerHandlerFactory;
import com.sap.conn.jco.server.JCoServer;
import com.sap.conn.jco.server.JCoServerFactory;
import com.sap.conn.jco.server.JCoServerFunctionHandler;

import java.lang.reflect.Field;
import java.util.*;
import java.util.logging.Logger;

/**
 * <p>
 * <b>Title:</b> RFCMultipleProviders.java
 * </p>
 *
 * <p>
 * <b>Description:</b> Classe responsável por manter multiplos providers para conexão SAP.
 * </p>
 *
 * <p>
 * <b>Company: </b> ITSS Soluções em Tecnologia
 * </p>
 *
 * @author Bruno Zafalão
 * @version 1.0.0
 */
public class RFCMultipleProviders {

    private static final Logger LOG = Logger.getLogger(SAPServiceImpl.class.getName());

    private static final String SERVER_NAME = "server_name";

    private static final String KEY_FUNCTION_NAME = "function_name";

    private static final String RFC_FUNCTION_NAME = "ZITSSAGRO_FM_EVENT_OUT";

    private static final String[] ARRAY_RFC_FUNCTION_NAME = new String[]{"ZITSSAGRO_FM_EVENT_OUT"};

    private static final String RFC_LANGUAGE = "PT";

    public static final Map<Long, JCoCallerService> DESTINATIONS = new HashMap<Long, JCoCallerService>();

    public static final Map<String, Boolean> SERVERS = new HashMap<String, Boolean>();

    /**
     * Método responsável por iniciar os serviços de comunicação com o SAP.
     *
     * @param ambienteSAPList
     * @author Bruno Zafalão
     */
    public static <T extends JCoRequest> void start(final List<Ambiente> ambienteSAPList, final JCoServerFunctionHandler handler, final T request) {

        if (ColecaoUtil.isEmpty(ambienteSAPList)) {

            return;
        }

        JCoDestination destination = null;

        if (ObjetoUtil.isNull(destination)) {

            final MultipleDestinationDataProvider destinationProvider = new MultipleDestinationDataProvider();

            Environment.registerDestinationDataProvider(destinationProvider);

            for (final Ambiente ambiente : ambienteSAPList) {

                RFCMultipleProviders.createDestinationService(destinationProvider, ambiente);
            }

            final MultipleServerDataProvider serverProvider = new MultipleServerDataProvider();

            Environment.registerServerDataProvider(serverProvider);

            for (final Ambiente ambiente : ambienteSAPList) {

                RFCMultipleProviders.createServerService(serverProvider, ambiente);
            }
        }

        RFCMultipleProviders.startServer(ambienteSAPList, handler, request);
    }

    /**
     * Método responsável por iniciar servidores RFC.
     *
     * @param ambienteSAPList
     * @param handler
     * @author Bruno Zafalão
     */
    private static <T extends JCoRequest> void startServer(final Collection<Ambiente> ambienteSAPList, final JCoServerFunctionHandler handler, final T request) {

        final List<JCoServer> serverList = RFCMultipleProviders.getServers(ambienteSAPList);

        for (final JCoServer server : serverList) {

            try {

                String chaveDestination = "";

                for (final Ambiente ambiente : ambienteSAPList) {

                    if (ambiente.getRfcProgramID().equalsIgnoreCase(server.getProgramID())) {

                        chaveDestination = ambiente.getHost() + "-" + ambiente.getMandante();
                    }

                }

                server.setRepository(JCoDestinationManager.getDestination(chaveDestination).getRepository());

            } catch (final JCoException e1) {

                SAPSync.ONLINE = false;

                RFCMultipleProviders.LOG.info(StringUtil.log("ERRO AO REGISTRAR REPOSITORIO"));

                RFCMultipleProviders.LOG.warning(e1.getMessage());

            }

            final DefaultServerHandlerFactory.FunctionHandlerFactory factory = new DefaultServerHandlerFactory.FunctionHandlerFactory();

            for (final String rfc : RFCMultipleProviders.ARRAY_RFC_FUNCTION_NAME) {

                factory.registerHandler(rfc, handler);
            }

            server.setCallHandlerFactory(factory);

            try {

                server.start();

                RFCMultipleProviders.LOG.info(StringUtil.log("SERVER PROGRAMID " + server.getProgramID() + " REGISTRADO COM SUCESSO"));

                SAPSync.ONLINE = true;

            } catch (final Exception e1) {

                SAPSync.ONLINE = false;

                RFCMultipleProviders.LOG.severe(StringUtil.log("SERVER PROGRAMID " + server.getProgramID() + " REGISTRADO COM FALHA, ITSSAGRO OFFLINE"));

                RFCMultipleProviders.LOG.warning(e1.getMessage());

            }
        }
    }

    /**
     * Método responsável por obter lista de servidores RFC registrados no provider.
     *
     * @param ambienteSAPList
     * @return <code>List</code>
     * @author Bruno Zafalão
     */
    public static List<JCoServer> getServers(final Collection<Ambiente> ambienteSAPList) {

        final List<JCoServer> serverList = new LinkedList<JCoServer>();

        for (final Ambiente ambiente : ambienteSAPList) {

            try {

                serverList.add(JCoServerFactory.getServer(ambiente.getRfcServerName()));

            } catch (final JCoException e) {

                e.printStackTrace();
            }
        }

        return serverList;
    }

    /**
     * Método responsável por inicializar os parametros de saída da função.
     *
     * @param jcoFunction
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @author Silvânio Júnior
     */
    public static void initRequestParameters(final JCoFunction jcoFunction, final JCoRequest request) throws InstantiationException, IllegalAccessException {

        final Field[] fields = request.getClass().getDeclaredFields();

        for (int i = 0; i < fields.length; i++) {

            RFCMultipleProviders.initSimpleRequestParameter(fields[i], jcoFunction, request);

            RFCMultipleProviders.initTableResponseParameter(fields[i], jcoFunction, request);
        }
    }

    /**
     * Método responsável por inicializar os parâmetros de saída simples de uma função SAP.
     *
     * @param field
     * @param jcoFunction
     * @param response
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @author Silvânio Júnior
     */
    private static void initSimpleRequestParameter(final Field field, final JCoFunction jcoFunction, final JCoRequest response) throws InstantiationException, IllegalAccessException {

        final SAPColumn column = field.getAnnotation(SAPColumn.class);

        if (column != null) {

            field.setAccessible(true);

            final Object value = jcoFunction.getImportParameterList().getValue(column.name());

            field.set(response, value);

        } else {

            final SAPStructure structure = field.getAnnotation(SAPStructure.class);

            if (structure != null) {

                field.setAccessible(true);

                final JCoItem item = structure.item().newInstance();

                final Field[] itemfields = structure.item().getDeclaredFields();

                final JCoStructure jcoStructure = jcoFunction.getImportParameterList().getStructure(structure.name());

                for (int i = 0; i < itemfields.length; i++) {

                    final SAPColumn itemColumn = itemfields[i].getAnnotation(SAPColumn.class);

                    itemfields[i].setAccessible(true);

                    final Object value = jcoStructure.getValue(itemColumn.name());

                    if (item != null) {

                        itemfields[i].set(item, value);
                    }
                }

                field.set(response, item);
            }
        }
    }

    /**
     * Método responsável por inicializar os parâmetros de saída de uma função.
     *
     * @param field
     * @param jcoFunction
     * @param response
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @author Silvânio Júnior
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    private static void initTableResponseParameter(final Field field, final JCoFunction jcoFunction, final JCoRequest response) throws InstantiationException, IllegalAccessException {

        final SAPTable table = field.getAnnotation(SAPTable.class);

        if (table != null) {

            final JCoTable jcoTable = jcoFunction.getTableParameterList().getTable(table.name());

            field.setAccessible(true);

            final List itens = new LinkedList();

            final Field[] itemfields = table.item().getDeclaredFields();

            int linha = 0;

            for (int row = 0; row < jcoTable.getNumRows(); row++) {

                jcoTable.setRow(linha);

                final JCoItem item = table.item().newInstance();

                for (int i = 0; i < itemfields.length; i++) {

                    final SAPColumn itemColumn = itemfields[i].getAnnotation(SAPColumn.class);

                    itemfields[i].setAccessible(true);

                    final Object value = jcoTable.getValue(itemColumn.name());

                    if (item != null) {

                        itemfields[i].set(item, value);

                    }

                }

                itens.add(item);

                linha++;

            }

            field.set(response, itens);

        }
    }

    /**
     * Método responsável por criar serviço de comunicação JCoDestination.
     *
     * @param destinationProvider
     * @param ambiente
     * @author Bruno Zafalão
     */
    private static void createDestinationService(final MultipleDestinationDataProvider destinationProvider, final Ambiente ambiente) {

        try {

            Properties destinationProperties = new Properties();

            destinationProperties.setProperty("ACTION", "CREATE");

            destinationProperties.setProperty(DestinationDataProvider.JCO_DEST, "ABAP_AS_WITH_POOL");

            if (ambiente.isLoadBalancing()) {

                // "RFC"
                destinationProperties.setProperty(DestinationDataProvider.JCO_GROUP, ambiente.getGroup());

                destinationProperties.setProperty(DestinationDataProvider.JCO_MSHOST, ambiente.getHost());

                // ECP
                destinationProperties.setProperty(DestinationDataProvider.JCO_R3NAME, ambiente.getR3name());

            } else {

                destinationProperties.setProperty(DestinationDataProvider.JCO_ASHOST, ambiente.getHost());

                destinationProperties.setProperty(DestinationDataProvider.JCO_SYSNR, ambiente.getNumeroInstancia());
            }

            if (StringUtil.isNotNullEmpty(ambiente.getSapRouter())) {

                destinationProperties.setProperty(DestinationDataProvider.JCO_SAPROUTER, "/H/" + ambiente.getSapRouter() + "/H/");
            }

            destinationProperties.setProperty(DestinationDataProvider.JCO_CLIENT, ambiente.getMandante());

            destinationProperties.setProperty(DestinationDataProvider.JCO_USER, ambiente.getRfcUsuario());

            destinationProperties.setProperty(DestinationDataProvider.JCO_PASSWD, ambiente.getRfcSenha());

            destinationProperties.setProperty(DestinationDataProvider.JCO_LANG, RFCMultipleProviders.RFC_LANGUAGE);

            destinationProvider.selectAmbiente(destinationProperties, ambiente.isLoadBalancing() ? DestinationDataProvider.JCO_MSHOST : DestinationDataProvider.JCO_ASHOST);

            final JCoDestination destination = JCoDestinationManager.getDestination(ambiente.getHost() + "-" + ambiente.getMandante());

            try {

                destination.ping();

            } catch (Exception e) {

                RFCMultipleProviders.LOG.severe("ERRO AO REALIZAR O PING NO SAP, ITSSAGRO OFFLINE");
            }

            RFCMultipleProviders.DESTINATIONS.put(ambiente.getId(), JCoCallerService.getInstance(ambiente.getUuid(), JCoDestinationManager.getDestination(ambiente.getHost() + "-" + ambiente.getMandante()), ambiente.getRfcProgramID()));

        } catch (final Exception e) {

            e.printStackTrace();
        }
    }

    /**
     * Método responsável por criar serviço de comunicação JCoServer.
     *
     * @param serverProvider
     * @param ambiente
     * @author Bruno Zafalão
     */
    private static void createServerService(final MultipleServerDataProvider serverProvider, final Ambiente ambiente) {

        final Properties serverProperties = new Properties();

        serverProperties.setProperty("ACTION", "CREATE");

        serverProperties.setProperty(ServerDataProvider.JCO_CONNECTION_COUNT, "1");

        String hostESapRouter = "";

        if (StringUtil.isNotNullEmpty(ambiente.getSapRouter())) {

            hostESapRouter = "/H/" + ambiente.getSapRouter() + "/H/";

        }

        hostESapRouter = hostESapRouter + ambiente.getHost();

        serverProperties.setProperty(ServerDataProvider.JCO_GWHOST, hostESapRouter);

        serverProperties.setProperty(ServerDataProvider.JCO_PROGID, ambiente.getRfcProgramID());

        serverProperties.setProperty(ServerDataProvider.JCO_GWSERV, ambiente.getRfcPorta());

        serverProperties.setProperty(RFCMultipleProviders.KEY_FUNCTION_NAME, RFCMultipleProviders.RFC_FUNCTION_NAME);

        serverProperties.setProperty(RFCMultipleProviders.SERVER_NAME, ambiente.getRfcServerName());

        serverProvider.selectAmbiente(serverProperties);
    }

    /**
     * <p>
     * <b>Title:</b> MultipleDestinationDataProvider.java
     * </p>
     *
     * <p>
     * <b>Description:</b> Classe interna responsável por manter multiplos JCODestination.
     * </p>
     *
     * <p>
     * <b>Company: </b> ITSS Soluções em Tecnologia
     * </p>
     *
     * @author Bruno Zafalão
     * @version 1.0.0
     */
    static class MultipleDestinationDataProvider implements DestinationDataProvider {

        /**
         * Atributo destinationListener.
         */
        private DestinationDataEventListener destinationListener;

        /**
         * Atributo properties.
         */
        private final Hashtable<String, Properties> properties = new Hashtable<String, Properties>();

        /**
         * Método responsável por obter propriedades de conexão JCODestination.
         *
         * @param destinationName
         * @return <code>Properties</code>
         * @author Bruno Zafalão
         */
        @Override
        public Properties getDestinationProperties(final String destinationName) {

            if (this.properties.containsKey(destinationName)) {

                return this.properties.get(destinationName);
            }

            return null;
        }

        /**
         * Método responsável por definir evento.
         *
         * @param eventListener
         * @author Bruno Zafalão
         */
        @Override
        public void setDestinationDataEventListener(final DestinationDataEventListener eventListener) {

            this.destinationListener = eventListener;
        }

        /**
         * Método responsável por definir o controle de eventos.
         *
         * @return <code>boolean</code>
         * @author Bruno Zafalão
         */
        @Override
        public boolean supportsEvents() {

            return true;
        }

        /**
         * Método responsável por manter a fila de propriedades de conexão JCODestination.
         *
         * @param properties
         * @author Bruno Zafalão
         */
        void selectAmbiente(final Properties properties, String host) {

            final String chave = properties.getProperty(host) + "-" + properties.getProperty(DestinationDataProvider.JCO_CLIENT);

            if (properties.getProperty("ACTION").equalsIgnoreCase("CREATE")) {

                this.properties.put(chave, properties);

                this.destinationListener.updated(chave);

            } else if (properties.getProperty("ACTION").equalsIgnoreCase("DELETE")) {

                this.properties.remove(chave);

                this.destinationListener.deleted(chave);
            }
        }
    }

    /**
     * <p>
     * <b>Title:</b> MultipleServerDataProvider.java
     * </p>
     *
     * <p>
     * <b>Description:</b> Classe interna responsável por manter multiplos JCOServer.
     * </p>
     *
     * <p>
     * <b>Company: </b> ITSS Soluções em Tecnologia
     * </p>
     *
     * @author Bruno Zafalão
     * @version 1.0.0
     */
    static class MultipleServerDataProvider implements ServerDataProvider {

        /**
         * Atributo serverListener.
         */
        private ServerDataEventListener serverListener;

        /**
         * Atributo properties.
         */
        private final Hashtable<String, Properties> properties = new Hashtable<String, Properties>();

        /**
         * Método responsável por obter propriedades de conexão JCOServer.
         *
         * @param serverName
         * @return <code>Properties</code>
         * @author Bruno Zafalão
         */
        @Override
        public Properties getServerProperties(final String serverName) {

            if (this.properties.containsKey(serverName)) {

                return this.properties.get(serverName);
            }

            return null;
        }

        /**
         * Método responsável por definir evento.
         *
         * @param eventListener
         * @author Bruno Zafalão
         */
        @Override
        public void setServerDataEventListener(final ServerDataEventListener eventListener) {

            this.serverListener = eventListener;
        }

        /**
         * Método responsável por definir o controle de eventos.
         *
         * @return <code>boolean</code>
         * @author Bruno Zafalão
         */
        @Override
        public boolean supportsEvents() {

            return true;
        }

        /**
         * Método responsável por manter a fila de propriedades de conexão JCOServer.
         *
         * @param properties
         * @author Bruno Zafalão
         */
        void selectAmbiente(final Properties properties) {

            if (properties.getProperty("ACTION").equalsIgnoreCase("CREATE")) {

                this.properties.put(properties.getProperty(RFCMultipleProviders.SERVER_NAME), properties);

                this.serverListener.updated(properties.getProperty(RFCMultipleProviders.SERVER_NAME));

            } else if (properties.getProperty("ACTION").equalsIgnoreCase("DELETE")) {

                this.properties.remove(properties.getProperty(RFCMultipleProviders.SERVER_NAME));

                this.serverListener.deleted(properties.getProperty(RFCMultipleProviders.SERVER_NAME));
            }
        }
    }
}
