package br.com.sjc.service;

import br.com.sjc.fachada.service.RecebimentoNfeSapCargaPontualService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.RecebimentoNfeSapCargaPontual;
import br.com.sjc.modelo.dto.RecebimentoSapNfeCargaPontualDTO;
import br.com.sjc.persistencia.dao.RecebimentoNfeSapCargaPontualDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecebimentoNfeSapCargaPontualServiceImpl extends ServicoGenerico<Long, RecebimentoNfeSapCargaPontual> implements RecebimentoNfeSapCargaPontualService {

    @Autowired
    private RecebimentoNfeSapCargaPontualDAO dao;

    @Override
    public RecebimentoNfeSapCargaPontualDAO getDAO() {

        return this.dao;
    }

    @Override
    public Class<RecebimentoSapNfeCargaPontualDTO> getDtoListagem() {

        return RecebimentoSapNfeCargaPontualDTO.class;
    }

}
