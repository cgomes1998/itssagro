package br.com.sjc.service;

import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.service.ParametroService;
import br.com.sjc.modelo.Arquivo;
import br.com.sjc.modelo.Senha;
import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.enums.TipoParametroEnum;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.encrypt.UtilEncoder;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class AutoatendimentoMotoristaServiceImpl {

    @Autowired
    private ParametroService parametroService;

    public void imprimirSenha(Arquivo arquivos) throws ServicoException {

        List<Parametro> parametrosDaImpressora = parametroService.obterPorTipoParametro(Arrays.asList(new TipoParametroEnum[]{TipoParametroEnum.IP_IMPRESSORA_ENTRADA_MOTORISTA, TipoParametroEnum.PORTA_IMPRESSORA_ENTRADA_MOTORISTA}));

        if (ColecaoUtil.isEmpty(parametrosDaImpressora) || parametrosDaImpressora.size() < 2) {

            throw new ServicoException("Os dados de conexão da impressora não foram informados.");
        }

        Parametro parametroIp = parametrosDaImpressora.stream().filter(p -> TipoParametroEnum.IP_IMPRESSORA_ENTRADA_MOTORISTA.equals(p.getTipoParametro())).findFirst().get();

        Parametro parametroPorta = parametrosDaImpressora.stream().filter(p -> TipoParametroEnum.PORTA_IMPRESSORA_ENTRADA_MOTORISTA.equals(p.getTipoParametro())).findFirst().get();

        conectarAImpressoraEImprimeArquivos(parametroIp.getValor(), Integer.parseInt(parametroPorta.getValor()), Arrays.asList(arquivos));
    }

    public void imprimirSenhaBematech(Senha senha) throws ServicoException {
        List<Parametro> parametrosDaImpressora = parametroService.obterPorTipoParametro(Arrays.asList(new TipoParametroEnum[]{TipoParametroEnum.IP_IMPRESSORA_ENTRADA_MOTORISTA, TipoParametroEnum.PORTA_IMPRESSORA_ENTRADA_MOTORISTA}));

        Parametro parametroIp = parametrosDaImpressora.stream().filter(p -> TipoParametroEnum.IP_IMPRESSORA_ENTRADA_MOTORISTA.equals(p.getTipoParametro())).findFirst().get();
        Parametro parametroPorta = parametrosDaImpressora.stream().filter(p -> TipoParametroEnum.PORTA_IMPRESSORA_ENTRADA_MOTORISTA.equals(p.getTipoParametro())).findFirst().get();

        String quebraLinha = "" + (char) 10;
        String defineTamanhoPadraoLinha = "" + (char) 27 + (char) 50;
        String modoExpandido = "" + (char) 24 + (char) 14;
        String iniciarImpressora = ("" + (char) 27 + (char) 116 + (char) 8  + (char) 10);
        String tabImpressora = ("" + (char) 9 );
        String ativarNegrito =  "" + (char) 27 + "E ";
        String desativaNegrito =  "" + (char) 27 + "F ";
        String centralizarTexto = "" + (char)27 + (char)97 + "1 ";
        String esquerdaTexto = "" + (char)27 + (char)97 + "0 ";
        String acionarGuilhotina = ""+(char)27 + (char) 105;

        Date now = DateTime.now().toDate();
        String dataAtual = DateUtil.formatTo(now, "dd/MM/yyyy");
        String horaAtual = DateUtil.formatTo(now, "HH:mm");

        String dataAgendamento = "";
        if(senha.getCargaPontualDataAgendamentoInicial() != null){
            String dataInicio = DateUtil.formatData(senha.getCargaPontualDataAgendamentoInicial()) + " " + DateUtil.formatHoraMinuto(senha.getCargaPontualDataAgendamentoInicial());
            String horaFim = DateUtil.formatHoraMinuto(senha.getCargaPontualDataAgendamentoFinal());

            dataAgendamento = String.format("%s - %s", dataInicio, horaFim);
        }

        String senhaLayout = "";
        senhaLayout += iniciarImpressora;
        senhaLayout += defineTamanhoPadraoLinha;
        senhaLayout += centralizarTexto + "------------------------------------------------" + centralizarTexto + quebraLinha;
        senhaLayout += centralizarTexto + "SJC BIO ENERGIA " + tabImpressora + "Agendamento "   + centralizarTexto + quebraLinha;
        senhaLayout += centralizarTexto + "------------------------------------------------" + centralizarTexto + quebraLinha;
        senhaLayout += esquerdaTexto + "Data:"+ dataAtual +" " + tabImpressora + " Hora: "+ horaAtual +" " + centralizarTexto + quebraLinha;
        senhaLayout += ativarNegrito + "Senha:"+ desativaNegrito + senha.getSenha() + quebraLinha;
        senhaLayout += ativarNegrito + "Ordem:"+ desativaNegrito + senha.getCodigoOrdemVenda() + quebraLinha;
        senhaLayout += ativarNegrito + "Placa Cavalo:"+ desativaNegrito + senha.getVeiculo().getDescricaoFormatada() + quebraLinha;
        senhaLayout += ativarNegrito + "Motorista:"+ desativaNegrito + exibeConteudoLinhaBematech(senha.getMotorista().getDescricaoFormatada(), 10) + quebraLinha;
        senhaLayout += ativarNegrito + "Produto:"+ desativaNegrito + exibeConteudoLinhaBematech(senha.getMaterial().getDescricaoFormatada(), 8) + quebraLinha;
        senhaLayout += ativarNegrito + "Cliente:"+ desativaNegrito + exibeConteudoLinhaBematech(senha.getCliente().getDescricaoFormatada(), 8) + quebraLinha;
        if(StringUtil.isNotNullEmpty(senha.getCargaPontualIdAgendamento())){
            senhaLayout += ativarNegrito + " Id Agendamento:"+ desativaNegrito + senha.getCargaPontualIdAgendamento() + quebraLinha;
        }
        if(StringUtil.isNotNullEmpty(dataAgendamento)){
            senhaLayout += ativarNegrito + "Data Agendamento:"+ desativaNegrito + dataAgendamento + quebraLinha;
        }
        senhaLayout += centralizarTexto + "------------------------------------------------" + quebraLinha;
        senhaLayout += quebraLinha + quebraLinha + quebraLinha + quebraLinha + quebraLinha + quebraLinha + quebraLinha;
        senhaLayout += acionarGuilhotina;

        conectarAImpressoraBematechEImprimirConteudo(senhaLayout, parametroIp.getValor(), parametroPorta.getValor());
    }

    public void imprimirArquivos(List<Arquivo> arquivos) throws ServicoException {

        List<Parametro> parametrosDaImpressora = parametroService.obterPorTipoParametro(Arrays.asList(new TipoParametroEnum[]{TipoParametroEnum.IP_IMPRESSORA_AUTOATENDIMENTO, TipoParametroEnum.PORTA_IMPRESSORA_AUTOATENDIMENTO}));

        if (ColecaoUtil.isEmpty(parametrosDaImpressora) || parametrosDaImpressora.size() < 2) {

            throw new ServicoException("Os dados de conexão da impressora não foram informados.");
        }

        Parametro parametroIp = parametrosDaImpressora.stream().filter(p -> TipoParametroEnum.IP_IMPRESSORA_AUTOATENDIMENTO.equals(p.getTipoParametro())).findFirst().get();

        Parametro parametroPorta = parametrosDaImpressora.stream().filter(p -> TipoParametroEnum.PORTA_IMPRESSORA_AUTOATENDIMENTO.equals(p.getTipoParametro())).findFirst().get();

        conectarAImpressoraEImprimeArquivos(parametroIp.getValor(), Integer.parseInt(parametroPorta.getValor()), arquivos);
    }

    public void conectarAImpressoraBematechEImprimirConteudo(String textoImpressao, String ip, String porta){
        try {
            Socket socket = new Socket(ip, Integer.parseInt(porta));

            OutputStream outputStream = socket.getOutputStream();

            outputStream.write(textoImpressao.getBytes());

            try {

                Thread.sleep(5000);

            } catch (InterruptedException e) {
            }

            outputStream.flush();

            outputStream.close();

            socket.close();
        }catch (Exception e){
            e.printStackTrace();

            throw new ServicoException("Erro ao realizar a impressão dos arquivos, por favor contate o adminstrador.");
        }

    }

    private void conectarAImpressoraEImprimeArquivos(String ip, Integer porta, List<Arquivo> arquivos) {

        try {

            for (Arquivo arquivo : arquivos) {

                Socket socket = new Socket(ip, porta);

                File file = UtilEncoder.get().decodeBase64ToFile(arquivo.getBase64(), arquivo.getNome());

                FileInputStream fileInputStream = new FileInputStream(file);

                byte[] mybytearray = new byte[(int) file.length()];

                fileInputStream.read(mybytearray, 0, mybytearray.length);

                OutputStream outputStream = socket.getOutputStream();

                outputStream.write(mybytearray, 0, mybytearray.length);

                try {

                    Thread.sleep(5000);

                } catch (InterruptedException e) {
                }

                outputStream.flush();

                outputStream.close();

                fileInputStream.close();

                socket.close();

                file.delete();
            }

        } catch (ServicoException e) {

            e.printStackTrace();

            throw new ServicoException("Erro ao realizar a impressão dos arquivos, por favor contate o adminstrador.");

        } catch (UnknownHostException e) {

            e.printStackTrace();

            throw new ServicoException("IP (" + ip + ") e Porta (" + porta + ") da impressora não foram encontrados.");

        } catch (IOException e) {

            e.printStackTrace();

            throw new ServicoException("Erro ao realizar a impressão dos arquivos, por favor contate o adminstrador.");
        }
    }

    private String exibeConteudoLinhaBematech(String valor, int descontar){
        int caracteresLimite = 47 - descontar;

        return valor.length() <= caracteresLimite ? valor : valor.substring(0, caracteresLimite - 3) + "...";
    }
}
