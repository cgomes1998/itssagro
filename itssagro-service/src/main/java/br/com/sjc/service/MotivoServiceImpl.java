package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.MotivoService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.Motivo;
import br.com.sjc.modelo.Motivo_;
import br.com.sjc.modelo.dto.MotivoDTO;
import br.com.sjc.modelo.enums.MotivoTipoEnum;
import br.com.sjc.persistencia.dao.MotivoDAO;
import br.com.sjc.util.StringUtil;
import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by julio.bueno on 24/06/2019.
 */
@Service
public class MotivoServiceImpl extends ServicoGenerico<Long, Motivo> implements MotivoService {

    @Autowired
    private MotivoDAO motivoDAO;

    @Override
    public DAO<Long, Motivo> getDAO() {

        return motivoDAO;
    }

    @Override
    public Specification<Motivo> obterEspecification(Paginacao<Motivo> paginacao) {

        return (Specification<Motivo>) (root, query, criteriaBuilder) -> {
            Motivo motivo = paginacao.getEntidade();

            List<Predicate> predicates = new ArrayList<>();

            if (!Objects.isNull(motivo.getId())) {
                predicates.add(criteriaBuilder.equal(root.get("id"), motivo.getId()));
            }

            if (StringUtil.isNotNullEmpty(motivo.getDescricao())) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("descricao")), MatchMode.ANYWHERE.toMatchString(motivo.getDescricao().toLowerCase())));
            }

            if (!Objects.isNull(motivo.getTipo())) {
                predicates.add(criteriaBuilder.equal(root.get("tipo"), motivo.getTipo()));
            }

            if (!Objects.isNull(motivo.getStatus())) {
                predicates.add(criteriaBuilder.equal(root.get("status"), motivo.getStatus()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    @Override
    public List<Motivo> getMotivosPorTipo(MotivoTipoEnum motivoTipoEnum) {

        return motivoDAO.findAll(((root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(Motivo_.tipo), motivoTipoEnum)), Sort.by(Sort.Direction.ASC, Motivo_.TIPO));
    }

    @Override
    public Class<MotivoDTO> getDtoListagem() {

        return MotivoDTO.class;
    }

    @Override
    public Boolean getRegistrarLogAtividade() {

        return Boolean.TRUE;
    }

}
