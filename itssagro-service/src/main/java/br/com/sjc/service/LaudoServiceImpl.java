package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.LaudoService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.Laudo;
import br.com.sjc.modelo.LaudoItem;
import br.com.sjc.modelo.dto.AnaliseFiltroDTO;
import br.com.sjc.modelo.dto.LaudoListagemDTO;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.enums.StatusLaudoEnum;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.persistencia.dao.LaudoDAO;
import br.com.sjc.util.BigDecimalUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.script.*;
import java.util.*;

/**
 * Created by julio.bueno on 03/07/2019.
 */
@Service
public class LaudoServiceImpl extends ServicoGenerico<Long, Laudo> implements LaudoService {

    @Autowired
    private LaudoDAO laudoDAO;

    @Override
    public DAO<Long, Laudo> getDAO() {

        return laudoDAO;
    }

    @Override
    public Class<LaudoListagemDTO> getDtoListagem() {

        return LaudoListagemDTO.class;
    }

    @Override
    public Laudo obterCertificadoPorMaterialETanque(final AnaliseFiltroDTO filtroDTO) {

        if (StringUtil.isEmpty(filtroDTO.getTanque())) {
            return obterCertificadoPorMaterial(filtroDTO.getMaterial().getId());
        }
        return laudoDAO.findFirstByMaterialIdAndTanqueOrderByDataCadastroDesc(filtroDTO.getMaterial().getId(), filtroDTO.getTanque());
    }

    @Override
    public Laudo obterCertificadoPorMaterial(final Long idMaterial) {

        return laudoDAO.findFirstByMaterialIdOrderByDataCadastroDesc(idMaterial);
    }

    @Override
    public String obterNumeroDoCertificadoPorMaterial(final Long idMaterial, final Long tanque) {

        AnaliseFiltroDTO analiseFiltroDTO = new AnaliseFiltroDTO();

        analiseFiltroDTO.setMaterial(new Material());

        analiseFiltroDTO.getMaterial().setId(idMaterial);

        analiseFiltroDTO.setTanque(tanque + "");

        final Laudo laudo = obterCertificadoPorMaterialETanque(analiseFiltroDTO);

        if (ObjetoUtil.isNotNull(laudo)) {

            return laudo.getNumeroCertificado();
        }

        return StringUtil.empty();
    }

    @Override
    public Specification<Laudo> obterEspecification(Paginacao<Laudo> paginacao) {

        return (Specification<Laudo>) (root, query, criteriaBuilder) -> {
            Laudo laudo = paginacao.getEntidade();
            List<Predicate> predicates = new ArrayList<>();
            Join<Laudo, Material> materialJoin = root.join("material", JoinType.LEFT);
            Join<Laudo, Laudo> usuarioJoin = root.join("usuario", JoinType.LEFT);

            if (!StringUtils.isEmpty(laudo.getNumeroCertificado())) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("numeroCertificado")), MatchMode.ANYWHERE.toMatchString(laudo.getNumeroCertificado().toLowerCase())));
            }

            if (!Objects.isNull(laudo.getMaterial()) && !StringUtil.isEmpty(laudo.getMaterial().getDescricao())) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(materialJoin.get("descricao")), MatchMode.ANYWHERE.toMatchString(laudo.getMaterial().getDescricao().toLowerCase())));
            }

            if (!StringUtils.isEmpty(laudo.getTanque())) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("descricao")), MatchMode.ANYWHERE.toMatchString(laudo.getTanque().toLowerCase())));
            }

            if (!Objects.isNull(laudo.getMaterial()) && !StringUtil.isEmpty(laudo.getMaterial().getDescricao())) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(materialJoin.get("descricao")), MatchMode.ANYWHERE.toMatchString(laudo.getMaterial().getDescricao().toLowerCase())));
            }

            if (!Objects.isNull(laudo.getUsuario()) && !StringUtil.isEmpty(laudo.getUsuario().getNome())) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(usuarioJoin.get("nome")), MatchMode.ANYWHERE.toMatchString(laudo.getUsuario().getNome().toLowerCase())));
            }

            if (!Objects.isNull(laudo.getStatusLaudo())) {
                predicates.add(criteriaBuilder.equal(root.get("statusLaudo"), laudo.getStatusLaudo()));
            }

            if (ObjetoUtil.isNotNull(paginacao.getEntidade().getDataCadastro())) {
                GregorianCalendar gc = new GregorianCalendar();
                gc.setTime(paginacao.getEntidade().getDataCadastro());
                gc.set(Calendar.HOUR, 0);
                gc.set(Calendar.MINUTE, 0);
                gc.set(Calendar.SECOND, 0);
                gc.set(Calendar.MILLISECOND, 0);
                Date from = gc.getTime();
                gc.set(Calendar.HOUR, 23);
                gc.set(Calendar.MINUTE, 59);
                gc.set(Calendar.SECOND, 59);
                gc.set(Calendar.MILLISECOND, 999);
                Date to = gc.getTime();
                predicates.add(criteriaBuilder.and(criteriaBuilder.between(root.get("dataCadastro"), from, to)));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));
        };
    }

    @Override
    public void preSalvar(Laudo entidade) {

        super.preSalvar(entidade);

        entidade.setStatus(StatusEnum.ATIVO);

        entidade.getItens().forEach(item -> item.setLaudo(entidade));

        setUpNumeroCertificado(entidade);

        setUpStatusLaudo(entidade);

        if (ObjetoUtil.isNotNull(entidade.getLoteAnaliseQualidade()) && ObjetoUtil.isNull(entidade.getLoteAnaliseQualidade().getId())) {

            entidade.setLoteAnaliseQualidade(null);
        }
    }

    private void setUpNumeroCertificado(Laudo laudo) {

        if (laudo.isNew() && laudo.getMaterial().isGerarNumeroCertificado()) {

            Laudo ultimoLaudoDoMaterial = laudoDAO.findFirstByMaterialIdOrderByDataCadastroDesc(laudo.getMaterial().getId());

            Long sequenciaDoUltimoCertificado = ObjetoUtil.isNotNull(ultimoLaudoDoMaterial) && StringUtil.isNotNullEmpty(ultimoLaudoDoMaterial.getNumeroCertificado()) ?
                    new Long(ultimoLaudoDoMaterial.getNumeroCertificado().split("/")[0]) : 0L;

            laudo.setNumeroCertificado(toNumeroCertificado(sequenciaDoUltimoCertificado + 1));
        }
    }

    private String toNumeroCertificado(Long sequenciaDoCertificado) {

        return sequenciaDoCertificado.toString() + "/" + Calendar.getInstance().get(Calendar.YEAR);
    }

    @Override
    public synchronized Laudo salvar(Laudo entidade) {

        return super.salvar(entidade);
    }

    private void setUpStatusLaudo(Laudo laudo) {

        List<LaudoItem> itens = laudo.getItens();

        for (LaudoItem item : itens) {
            if (!checkItem(item)) {
                laudo.setStatusLaudo(StatusLaudoEnum.REPROVADO);
                return;
            }
        }

        laudo.setStatusLaudo(StatusLaudoEnum.APROVADO);
    }

    private boolean checkItem(LaudoItem item) {

        try {

            if (StringUtil.isNotNullEmpty(item.getResultado())) {

                String condicao = StringUtil.empty();

                if (StringUtil.isNotNullEmpty(item.getValorInicial()) && StringUtil.isNotNullEmpty(item.getValorFinal())) {
                    condicao = "RESULTADO >= CONDICAO_UM && RESULTADO <= CONDICAO_DOIS";
                } else {
                    condicao = String.format("RESULTADO %s CONDICAO_UM", item.getCondicao().getOperador());
                }

                ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
                ScriptEngine scriptEngine = scriptEngineManager.getEngineByName("javascript");

                Bindings bindings = scriptEngine.getBindings(ScriptContext.ENGINE_SCOPE);
                bindings.put("RESULTADO", BigDecimalUtil.converterStringParaDigitoSeForNumerico(item.getResultado().trim().toUpperCase()));
                bindings.put("CONDICAO_UM", BigDecimalUtil.converterStringParaDigitoSeForNumerico(item.getValorInicial().trim().toUpperCase()));
                if (StringUtil.isNotNullEmpty(item.getValorInicial()) && StringUtil.isNotNullEmpty(item.getValorFinal())) {
                    bindings.put("CONDICAO_DOIS", BigDecimalUtil.converterStringParaDigitoSeForNumerico(item.getValorFinal().trim().toUpperCase()));
                }

                return (Boolean) scriptEngine.eval(condicao);
            }

        } catch (ScriptException e) {

            e.printStackTrace();
        }

        return false;
    }

    @Override
    public Boolean getRegistrarLogAtividade() {

        return Boolean.TRUE;
    }

}
