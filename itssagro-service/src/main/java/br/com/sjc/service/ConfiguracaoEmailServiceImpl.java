package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.service.ConfiguracaoEmailService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.ConfiguracaoEmail;
import br.com.sjc.persistencia.dao.ConfiguracaoEmailDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * Created by julio.bueno on 23/07/2019.
 */
@Service
public class ConfiguracaoEmailServiceImpl extends ServicoGenerico<Long, ConfiguracaoEmail> implements ConfiguracaoEmailService {

    @Autowired
    private ConfiguracaoEmailDAO configuracaoEmailDAO;

    private ConfiguracaoEmail configuracaoEmail;

    @Override
    public DAO<Long, ConfiguracaoEmail> getDAO() {
        return configuracaoEmailDAO;
    }

    @Override
    public ConfiguracaoEmail salvar(ConfiguracaoEmail entidade) {
        configuracaoEmail = null;
        return super.salvar(entidade);
    }

    @Override
    public ConfiguracaoEmail get() {
        if(Objects.isNull(configuracaoEmail)){
            List<ConfiguracaoEmail> configuracoes = configuracaoEmailDAO.findAll();

            if(!configuracoes.isEmpty()){
                configuracaoEmail = configuracoes.get(0);

            } else {
                configuracaoEmail = new ConfiguracaoEmail();

            }
        }

        return configuracaoEmail;
    }
}
