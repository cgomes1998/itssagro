package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.TabelaValorService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.Arquivo;
import br.com.sjc.modelo.TabelaValor;
import br.com.sjc.modelo.TabelaValorItem;
import br.com.sjc.modelo.dto.TabelaValorDTO;
import br.com.sjc.modelo.sap.ConversaoLitragem;
import br.com.sjc.persistencia.dao.TabelaValorDAO;
import br.com.sjc.persistencia.dao.TabelaValorItemDAO;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.*;
import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;
import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

@Service
public class TabelaValorServiceImpl extends ServicoGenerico<Long, TabelaValor> implements TabelaValorService {

    @Autowired
    private TabelaValorDAO tabelaValorDAO;

    @Autowired
    private TabelaValorItemDAO tabelaValorItemDAO;

    @Override
    public void preSalvar(TabelaValor tabelaValor) {

        super.preSalvar(tabelaValor);

        if (tabelaValor.isNew() && tabelaValorDAO.existsByMaterial(tabelaValor.getMaterial())) {
            throw new ServicoException("Conjunto de valores já registrados para o produto selecionado.");
        }

        tabelaValor.getItens().forEach(item -> item.setTabelaValor(tabelaValor));
    }

    @Override
    public TabelaValor obterPorMaterial(Long material) {

        return getDAO().findOne((root, query, builder) ->
                builder.and(builder.equal(root.get("material"), material)))
                .orElse(null);
    }

    @Transactional
    @Override
    public TabelaValorItem obterItensParaConversao(final ConversaoLitragem item) {

        return tabelaValorItemDAO.getFirstByTemperaturaAndGrauAlcoolicoAndTabelaValorMaterialId(item.getTempTanque(), item.getGrauInpm(), item.getMaterial().getId());
    }

    @Override
    public Specification<TabelaValor> obterEspecification(Paginacao<TabelaValor> paginacao) {

        return (Specification<TabelaValor>) (root, query, criteriaBuilder) -> {
            TabelaValor tabelaValor = paginacao.getEntidade();

            List<Predicate> predicates = new ArrayList<>();

            if (!StringUtils.isEmpty(tabelaValor.getDescricao())) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("descricao")), MatchMode.ANYWHERE.toMatchString(tabelaValor.getDescricao())));
            }

            if (ObjetoUtil.isNotNull(tabelaValor.getMaterial())) {
                predicates.add(criteriaBuilder.equal(root.get("material"), tabelaValor.getMaterial()));
            }

            if (!Objects.isNull(tabelaValor.getStatus())) {
                predicates.add(criteriaBuilder.equal(root.get("status"), tabelaValor.getStatus()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    @Override
    public List<TabelaValorItem> importarArquivo(Arquivo arquivo) {

        List<TabelaValorItem> itens = new ArrayList<>();
        if (ObjetoUtil.isNotNull(arquivo)) {
            try {
                File file = new File(arquivo.getNome());
                byte[] bytes = Base64.decodeBase64(arquivo.getBase64());
                FileUtils.writeByteArrayToFile(file, bytes);

                try (Workbook workbook = WorkbookFactory.create(file)) {
                    Sheet sheet = workbook.getSheetAt(0);

                    Iterator<Row> rowIterator = sheet.iterator();
                    if (sheet.getPhysicalNumberOfRows() == 0) {
                        throw new ServicoException("Nenhum registro de valores encontrado no arquivo.");
                    }

                    while (rowIterator.hasNext()) {
                        Row row = rowIterator.next();
                        boolean existeCelulaVazia = false;
                        for (Cell cell : row) {
                            if (ObjetoUtil.isNotNull(cell)) {
                                if (CellType.NUMERIC.equals(cell.getCellType())) {
                                    existeCelulaVazia = ObjetoUtil.isNull(cell.getNumericCellValue());
                                } else if (CellType.STRING.equals(cell.getCellType())) {
                                    existeCelulaVazia = StringUtil.isEmpty(cell.getStringCellValue());
                                }
                            }
                            if (existeCelulaVazia) {
                                break;
                            }
                        }
                        if (!existeCelulaVazia)
                            itens.add(new TabelaValorItem(null, obterValorCell(row, 0), obterValorCell(row, 1), obterValorCell(row, 2), obterValorCell(row, 3), obterValorCell(row, 4), obterValorCell(row, 5)));
                    }
                } catch (ServicoException e) {
                    throw e;
                }
            } catch (Exception e) {
                throw new ServicoException("Erro ao importar o arquivo " + arquivo.getNome());
            } finally {
                FileUtils.deleteQuietly(new File(arquivo.getNome()));
            }
        }
        return itens;
    }

    BigDecimal obterValorCell(Row row, int index) {

        Cell cell = row.getCell(index);

        if (ObjetoUtil.isNotNull(cell) && ObjetoUtil.isNotNull(cell.getCellType()) && !CellType.NUMERIC.equals(cell.getCellType())) {

            throw new ServicoException(MessageFormat.format("Arquivo inconsistente ou com valores inválidos, linha {0}", row.getRowNum() + 1));
        }

        return ObjetoUtil.isNotNull(cell) && ObjetoUtil.isNotNull(cell.getNumericCellValue()) ? BigDecimal.valueOf(cell.getNumericCellValue()).setScale(4, RoundingMode.HALF_EVEN) : BigDecimal.ZERO;
    }

    @Override
    public DAO<Long, TabelaValor> getDAO() {

        return this.tabelaValorDAO;
    }

    @Override
    public Class<TabelaValorDTO> getDtoListagem() {

        return TabelaValorDTO.class;
    }

    @Override
    public Boolean getRegistrarLogAtividade() {

        return Boolean.TRUE;
    }

}
