package br.com.sjc.service;

import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.ParametroService;
import br.com.sjc.fachada.service.SafraService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.dto.SafraDTO;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.sap.Safra;
import br.com.sjc.modelo.sap.request.EventOutRequest;
import br.com.sjc.modelo.sap.sync.SAPSafra;
import br.com.sjc.persistencia.dao.SafraDAO;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;
import java.util.logging.Logger;

@Service
public class SafraServiceImpl extends ServicoGenerico<Long, Safra> implements SafraService {

    private static final Logger LOG = Logger.getLogger(SafraServiceImpl.class.getName());

    @Autowired
    private SafraDAO dao;

    @Autowired
    private ParametroService parametroService;

    @Override
    public Specification<Safra> obterEspecification(final Paginacao<Safra> paginacao) {

        final Safra entidade = paginacao.getEntidade();

        final Specification<Safra> specification = new Specification<Safra>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<Safra> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                if (StringUtil.isNotNullEmpty(entidade.getCodigo())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("codigo")), "%" + entidade.getCodigo().toUpperCase() + "%")));
                }

                if (StringUtil.isNotNullEmpty(entidade.getDescricao())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("descricao")), "%" + entidade.getDescricao().toUpperCase() + "%")));
                }

                if (ObjetoUtil.isNotNull(entidade.getStatus())) {

                    predicates.add(builder.equal(root.get("status"), entidade.getStatus()));
                }

                if (ObjetoUtil.isNotNull(entidade.getStatusCadastroSap())) {

                    predicates.add(builder.equal(root.get("statusCadastroSap"), entidade.getStatusCadastroSap()));
                }

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Override
    public void salvarSAP(final EventOutRequest request, final String host, String mandante) {

        if (ColecaoUtil.isNotEmpty(request.getData())) {

            final Map<String, SAPSafra> groupSafra = this.groupSafra(request);

            groupSafra.keySet().forEach(itemGrupo -> {

                final SAPSafra safra = groupSafra.get(itemGrupo);

                Safra entidadeSaved = this.getDAO().findByCodigo(safra.getCodigo());

                entidadeSaved = safra.copy(entidadeSaved);

                if (StringUtil.isNotNullEmpty(entidadeSaved.getCodigo())) {

                    this.salvar(entidadeSaved);
                }
            });
        }
    }

    private Map<String, SAPSafra> groupSafra(final EventOutRequest request) {

        final Map<String, SAPSafra> groupSafra = new HashMap<String, SAPSafra>();

        request.getData().forEach(registro -> {

            SAPSafra entidade = groupSafra.get(registro.getItemDocumento().trim());

            if (ObjetoUtil.isNull(entidade)) {

                entidade = SAPSafra.newInstance();
            }

            try {

                entidade.update(registro.getCampo().trim(), registro.getValor().toString().trim());

                groupSafra.put(registro.getItemDocumento().trim(), entidade);

            } catch (final Exception e) {

                SafraServiceImpl.LOG.info("ERRO: " + e.getMessage());
            }
        });

        return groupSafra;
    }

    @Override
    public List<Safra> listarSincronizados(final Date data) {

        return ObjetoUtil.isNull(data) ? this.dao.findByStatusCadastroSapIn(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO})) : this.dao.findByStatusCadastroSapInAndDataCadastroGreaterThanEqual(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO}), data);
    }

    @Override
    public void sincronizar(final List<Safra> entidades) {

        if (ColecaoUtil.isNotEmpty(entidades)) {

            entidades.forEach(item -> {

                Safra entidadeSaved = this.getDAO().findByCodigo(item.getCodigo());

                item.setId(null);

                if (ObjetoUtil.isNotNull(entidadeSaved)) {

                    item.setId(entidadeSaved.getId());
                }
            });

            this.salvar(entidades);
        }
    }

    @Override
    public List<Safra> buscarSafrasAutoComplete(final String value) {

        if (StringUtil.contemApenasNumeros(value.replaceAll("-", "/"))) {

            return this.dao.findTop10ByStatusAndCodigoIgnoreCaseContainingOrderByCodigoAsc(StatusEnum.ATIVO, value);
        }

        return this.getDAO().findTop10ByStatusAndDescricaoIgnoreCaseContainingOrderByCodigoAsc(StatusEnum.ATIVO, value.replaceAll("-", "/"));
    }

    @Override
    public Safra obterPorCodigo(final String codigo) {
        return this.getDAO().findByCodigo(codigo);
    }

    @Override
    public List<Safra> listar() {
        return this.getDAO().findAll();
    }

    @Override
    public SafraDAO getDAO() {
        return this.dao;
    }

    @Override
    public Class<? extends DataDTO> getDtoListagem() {
        return SafraDTO.class;
    }
}
