package br.com.sjc.service.romaneio.filters;

import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.sap.ItemNFPedido;
import br.com.sjc.modelo.sap.ItensClassificacao;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.request.EventInRequest;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class TransDePropDeMercDepositadaFilters extends RomaneioFilters {

    @Override
    public JCoRequest instace(final Romaneio romaneio, final String centro, final String requestKey, final DadosSincronizacao dadosSincronizacao, final String rfcProgramID) {

        return EventInRequest.instance(dadosSincronizacao, centro, rfcProgramID, this.getFilters(romaneio, requestKey), false, null);
    }

    private List<EventInRequestItem> getFilters(final Romaneio romaneio, final String requestKey) {

        final List<EventInRequestItem> filters = new LinkedList<EventInRequestItem>();

        int seq = 1;

        int codItem = 0;

        seq = this.montarBridgeParaUsuarioCriadorRomaneio(romaneio, requestKey, filters, seq);

        final ItemNFPedido cabecalho = romaneio.getItens().stream().findFirst().get();

        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_CENTRO.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getCentro()) ? romaneio.getCentro().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_MATERIAL.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(cabecalho.getMaterial()) ? cabecalho.getMaterial().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_DEPOS.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(cabecalho.getDeposito()) ? cabecalho.getDeposito().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_SAFRA.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(cabecalho.getSafra()) ? cabecalho.getSafra().getCodigo() : StringUtil.empty()));

        BigDecimal descontos = BigDecimal.ZERO;

        if (ObjetoUtil.isNotNull(romaneio.getClassificacao()) && ColecaoUtil.isNotEmpty(romaneio.getClassificacao().getItens())) {

            descontos = romaneio.getClassificacao().getItens().stream().map(ItensClassificacao::getDescontoValido).reduce(BigDecimal.ZERO, BigDecimal::add);
        }

        for (ItemNFPedido item : romaneio.getItens()) {

            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_ROMAN.name(), String.valueOf(seq++), String.valueOf(codItem), romaneio.getNumeroRomaneio()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_DATA.name(), String.valueOf(seq++), String.valueOf(codItem), DateUtil.formatToSAP(DateUtil.hoje())));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_OPERA.name(), String.valueOf(seq++), String.valueOf(codItem), romaneio.getOperacao().getOperacao()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_PEDIDO.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(item.getNumeroPedido()) ? item.getNumeroPedido() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_ITEMP.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(item.getItemPedido()) ? item.getItemPedido() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_PRODUT.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getProdutor()) ? item.getProdutor().getCodigo() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_ARMT.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(romaneio.getArmazemTerceiros()) ? romaneio.getArmazemTerceiros().getCodigo() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_VALORP.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getValorUnitarioPedido()) ? item.getValorUnitarioPedido().toString() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_SALDO.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getSaldoPedido()) ? item.getSaldoPedido().toString() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_TRANSP.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getTransportadora()) ? item.getTransportadora().getCodigo() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_MOTORIST.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getMotorista()) ? item.getMotorista().getCodigo() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_PLACAC.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getPlacaCavalo()) ? StringUtil.toPlaca(item.getPlacaCavalo().getPlaca1()) : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_UF.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getPlacaCavalo()) && ObjetoUtil.isNotNull(item.getPlacaCavalo().getUfPlaca1()) ? item.getPlacaCavalo().getUfPlaca1().getSigla() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_PLACA1.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getPlacaCavaloUm()) ? StringUtil.removerCaracteresEspeciais(item.getPlacaCavaloUm().getPlaca1()) : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_PLACA2.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getPlacaCavaloDois()) ? StringUtil.removerCaracteresEspeciais(item.getPlacaCavaloDois().getPlaca1()) : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_TIPOCAP.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getTipoVeiculo()) ? item.getTipoVeiculo().getCodigo() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_TIPOFRE.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getTipoFrete()) ? item.getTipoFrete().name() : StringUtil.empty()));

            seq = this.mapeamentoBridgeParaRateio(romaneio, filters, seq, requestKey, codItem, item, descontos);
        }

        seq = this.montarPesagemDestino(romaneio, descontos, requestKey, filters, seq);

        if (romaneio.isPossuiDadosOrigem()) {

            seq = this.montarPesagemOrigem(romaneio, requestKey, filters, seq);

            seq = this.montarClassificacaoOrigem(romaneio.getItensClassificacaoOrigem(), requestKey, filters, seq);
        }

        seq = this.montarClassificacaoDestino(romaneio.getClassificacao(), requestKey, filters, seq);

        int grupo = 1;

        seq = this.montarObservacao(romaneio, requestKey, filters, seq, grupo);

        grupo++;

        this.montarObservacaoNota(romaneio, requestKey, filters, seq, grupo);

        adicionarRequestRomaneioSemDivisao(romaneio, requestKey);

        return filters;
    }
}
