package br.com.sjc.service;

import br.com.sjc.fachada.service.PortariaItemService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.cfg.PortariaItem;
import br.com.sjc.modelo.dto.PortariaItemDTO;
import br.com.sjc.persistencia.dao.PortariaItemDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PortariaItemServiceImpl extends ServicoGenerico<Long, PortariaItem> implements PortariaItemService {
    @Autowired
    private PortariaItemDAO dao;

    @Override
    public Class<PortariaItemDTO> getDtoListagem() {
        return PortariaItemDTO.class;
    }

    @Override
    public PortariaItemDAO getDAO() {
        return this.dao;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PortariaItemDTO> listarItensAtivos() {
        List<PortariaItemDTO> itens = dao.listarAtivos().stream().map(PortariaItemDTO::new).collect(Collectors.toList());
        return itens;
    }
}
