package br.com.sjc.service;

import br.com.sjc.fachada.service.RetornoNFeService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.dto.NfeDepositoDTO;
import br.com.sjc.modelo.dto.RetornoNFEListagemDTO;
import br.com.sjc.modelo.dto.RetornoNfeDTO;
import br.com.sjc.modelo.dto.RomaneioDepositoDTO;
import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.sap.RetornoNFe;
import br.com.sjc.persistencia.dao.ItemNFPedidoDAO;
import br.com.sjc.persistencia.dao.RetornoNFeDAO;
import br.com.sjc.persistencia.dao.fachada.RetornoNfeCustomRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.sql.DataSource;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Service
@Slf4j
public class RetornoNFeServiceImpl extends ServicoGenerico<Long, RetornoNFe> implements RetornoNFeService {

    @Autowired
    private RetornoNFeDAO dao;

    @Autowired
    private ItemNFPedidoDAO itemNFPedidoDAO;

    @Autowired
    private RetornoNfeCustomRepository retornoNfeCustomRepository;
    
    @Autowired
    private DataSource dataSource;
    
    @Override
    public void salvarListaRetornoNfe(List<RetornoNFe> retornoNFes) {
        
        retornoNFes.stream().forEach(retornoNFe -> {
            try {
                boolean notaExiste = Objects.nonNull(retornoNFe.getId());
                executeQuery(dataSource, notaExiste ? retornoNFe.queryUpdate() : retornoNFe.queryInsert());
            } catch (Exception exception) {
                log.error(String.format("Erro ao inserir nota fiscal retornada pelo SAP: \n %s", retornoNFe.toString()), exception);
            }
        });
    }

    @Override
    public List<RetornoNFe> obterEntidadeProjetantoApenasId(final Set<String> notas) {

        return retornoNfeCustomRepository.obterIdRetornoNFePorNfe(notas);
    }

    @Override
    public List<RetornoNFe> listarPorNfeTransporte(final Set<String> nfesTranporte) {

        return this.getDAO().findByNfeTransporteIn(nfesTranporte);
    }

    @Override
    public RetornoNFe obterNotaPorNumero(final String numero) {

        return this.getDAO().findFirstByNfeTransporteOrNfe(numero, numero);
    }

    @Override
    public boolean notaEstaSendoUtilizada(final String numeroNota, final Long idRomaneio) {

        return this.itemNFPedidoDAO.existsByNumeroNotaFiscalTransporteAndRomaneioStatusIntegracaoSAPInOrRomaneioStatusRomaneioInAndRomaneioIdNe(numeroNota, StatusIntegracaoSAPEnum.ESTORNADO, StatusRomaneioEnum.ESTORNADO, idRomaneio);
    }

    @Override
    public List<RetornoNFe> listarNfePorRomaneio(final Long idRomaneio) {

        return dao.findByRomaneioId(idRomaneio);
    }

    @Override
    public List<NfeDepositoDTO> obterDadosDeNotaParaRelatorioDeDeposito(List<RomaneioDepositoDTO> romaneioDepositoDTOS) {

        return retornoNfeCustomRepository.obterDadosDeNotaParaRelatorioDeDeposito(romaneioDepositoDTOS);
    }

    @Override
    public RetornoNFeDAO getDAO() {

        return this.dao;
    }

    @Override
    public Class<RetornoNFEListagemDTO> getDtoListagem() {

        return RetornoNFEListagemDTO.class;
    }
    
    @Override
    public List<RetornoNfeDTO> obterRetornoNFePorRomaneios(final List<String> numRomaneios) {
    	return retornoNfeCustomRepository.obterRetornoNFePorRomaneios(numRomaneios);
    }
    
    
}
