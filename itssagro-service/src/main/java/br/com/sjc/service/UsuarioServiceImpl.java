package br.com.sjc.service;

import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.UsuarioService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.Perfil;
import br.com.sjc.modelo.Perfil_;
import br.com.sjc.modelo.Usuario;
import br.com.sjc.modelo.Usuario_;
import br.com.sjc.modelo.dto.UsuarioDTO;
import br.com.sjc.modelo.dto.aprovacaoDocumento.UsuarioAprovacaoDocumento;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.persistencia.dao.UsuarioDAO;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.encrypt.MD5Util;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
@Log4j2
public class UsuarioServiceImpl extends ServicoGenerico<Long, Usuario> implements UsuarioService {

    @Autowired
    private UsuarioDAO dao;

    @Override
    public Collection<UsuarioAprovacaoDocumento> listarUsuariosComPermissaoParaAprovarIndices() {

        Paginacao paginacao = new Paginacao();

        paginacao.setListarTodos(true);

        paginacao.setSpecification(((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.isTrue(root.join(Usuario_.perfis).get(Perfil_.aplicarIndiceRestrito))));

        return dtoListar(paginacao, UsuarioAprovacaoDocumento.class).getContent();
    }

    @Override
    public Specification<Usuario> obterEspecification(final Paginacao<Usuario> paginacao) {

        final Usuario entidade = paginacao.getEntidade();

        final Specification<Usuario> specification = new Specification<Usuario>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<Usuario> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                if (StringUtil.isNotNullEmpty(entidade.getNome())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("nome")), "%" + entidade.getNome().toUpperCase() + "%")));
                }

                if (StringUtil.isNotNullEmpty(entidade.getLogin())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("login")), "%" + entidade.getLogin().toUpperCase() + "%")));
                }

                if (StringUtil.isNotNullEmpty(entidade.getCpf())) {

                    predicates.add(builder.equal(root.get("cpf"), StringUtil.removerCaracteresEspeciais(entidade.getCpf())));
                }

                if (ObjetoUtil.isNotNull(entidade.getPerfil())) {

                    final Path<Object> pathPerfil = root.join("perfis", JoinType.LEFT);

                    predicates.add(builder.equal(pathPerfil.get("id"), entidade.getPerfil().getId()));
                }

                predicates.add(builder.and(entidade.getStatus() == null ? root.get("status").in(StatusEnum.ATIVO, StatusEnum.INATIVO) : root.get("status").in(entidade.getStatus())));

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Override
    public Usuario autenticarUsuario(final String login, final String senha, final boolean senhaJaCriptografada) {

        try {

            final String senhaEcriptada = MD5Util.cript(senha);

            Usuario u = dao.findByLoginAndSenha(login, senhaJaCriptografada ? senha : senhaEcriptada);

            return u;

        } catch (Exception e) {
            log.error("", e);
            return null;
        }
    }

    @Override
    public String obterLoginPorId(final Long id) {

        return this.getDAO().findLoginById(id);
    }

    @Override
    public Usuario obterPorLogin(final String login) {

        return this.getDAO().findTop1ByLogin(login);
    }

    @Override
    public boolean existeUsuarioComLoginESenha(final String login, final String senha) {

        return this.getDAO().existsByLoginAndSenha(login, senha);
    }

    @Override
    public void preSalvar(Usuario entidade) {

        this.usuarioJaExiste(entidade);

        if (StringUtil.isNotNullEmpty(entidade.getCpf())) {

            entidade.setCpf(StringUtil.removerCaracteresEspeciais(entidade.getCpf()));
        }
    }

    private void usuarioJaExiste(final Usuario entidade) {

        final Boolean isCadastro = entidade.getId() == null;

        if ((isCadastro && dao.existsByLogin(entidade.getLogin())) || (!isCadastro && dao.existsByLoginAndNeId(entidade.getLogin(), entidade.getId()))) {

            throw new ServicoException("Já existe um usuário com o login: " + entidade.getLogin());
        }

        if (isCadastro && StringUtil.isNotNullEmpty(entidade.getCpf()) && dao.existsByCpf(StringUtil.removerCaracteresEspeciais(entidade.getCpf()))) {
            throw new ServicoException("Já existe um usuário com o cpf: " + entidade.getCpf());
        }
    }

    @Override
    public void atualizarDataUltimoAcesso(final Long id, final Date data) {

        this.getDAO().atualizarDataUltimoAcesso(id, data);
    }

    @Override
    @Transactional(readOnly = true)
    public String obterNomePorId(final Long id) {

        return dao.findNomeById(id);
    }

    @Override
    public Class<UsuarioDTO> getDtoListagem() {

        return UsuarioDTO.class;
    }

    @Override
    public UsuarioDAO getDAO() {

        return dao;
    }

}
