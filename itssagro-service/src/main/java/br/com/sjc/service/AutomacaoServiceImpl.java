package br.com.sjc.service;

import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.service.AutomacaoService;
import br.com.sjc.modelo.LogAutomacao;
import br.com.sjc.modelo.LogAutomacao_;
import br.com.sjc.modelo.dto.NrCartaoStatusDTO;
import br.com.sjc.modelo.dto.PreCadastroVeiculoDTO;
import br.com.sjc.modelo.dto.VeiculoBalancaDTO;
import br.com.sjc.modelo.soap.automacao.UrnServidorBindingStub;
import br.com.sjc.persistencia.dao.LogAutomacaoDAO;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.StringUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
@Service
public class AutomacaoServiceImpl implements AutomacaoService {

    private final URL url;

    @Autowired
    private LogAutomacaoDAO logAutomacaoDAO;

    public AutomacaoServiceImpl(Environment environment) throws MalformedURLException {
        this.url = new URL(environment.getRequiredProperty("conceitto.automacao.endpoint"));
    }

    @Scheduled(cron = "0 0 * * * *")
    public void deletarLogsDeTresDiasAtras() {

        log.info("JOB para deletar logs executado");
        List<LogAutomacao> logs = logAutomacaoDAO.findAll(((root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get(LogAutomacao_.dataCadastro), DateUtil.converterDateFimDia(DateUtil.diminuirDias(DateUtil.hoje(), 3)))));
        if (ColecaoUtil.isNotEmpty(logs)) {
            log.info(String.format("%d logs serão deletados da base.", logs.size()));
            logAutomacaoDAO.deleteAll(logs);
            log.info("Logs deletados com sucesso!");
        } else {
            log.info("Nenhum log encontrado registrado para a condição passada de três anteriores a data de hoje");
        }
    }

    /**
     * Retorna balanca, numero do tag, placa veiculo, peso 1, peso 2 e status.
     *
     * @param bal
     * @return
     */
    @Override
    public VeiculoBalancaDTO obterPesoBalanca(int bal) {
        LogAutomacao logAutomacao = new LogAutomacao();
        try {
            Map<String, Object> corpoDoEnvio = new HashMap<>();
            corpoDoEnvio.put("bal", bal);
            logAutomacao = salvarLogDeEnvio(logAutomacao, corpoDoEnvio,"obterPesoBalanca");
            String retorno = new UrnServidorBindingStub(url, null).consulta_automatiza(bal);
            salvarLogDeRetorno(logAutomacao, retorno);
            return new VeiculoBalancaDTO(retorno);
        } catch (Exception e) {
            salvarLogDeErro(logAutomacao, e);
            throw new ServicoException(e.getMessage());
        }
    }

    /**
     * Altera o status de pesagem de um veiculo usando como referencia o numero do TAG.
     *
     * @param tag
     * @param status
     * @param msg1
     * @param msg2
     * @param msg3
     * @param peso_ordem
     * @param obs
     * @return
     */
    @Override
    public NrCartaoStatusDTO alterarStatusPesagem(int tag, int status, String msg1, String msg2, String msg3, int peso_ordem, String obs) {
        LogAutomacao logAutomacao = new LogAutomacao();
        try {
            if (StringUtil.isEmpty(msg1)) {
                msg1 = StringUtil.empty();
            }
            if (StringUtil.isEmpty(msg2)) {
                msg2 = StringUtil.empty();
            }
            if (StringUtil.isEmpty(msg3)) {
                msg3 = StringUtil.empty();
            }
            Map<String, Object> corpoDoEnvio = new HashMap<>();
            corpoDoEnvio.put("tag", tag);
            corpoDoEnvio.put("status", status);
            corpoDoEnvio.put("msg1", msg1);
            corpoDoEnvio.put("msg2", msg2);
            corpoDoEnvio.put("msg3", msg3);
            corpoDoEnvio.put("peso_ordem", peso_ordem);
            corpoDoEnvio.put("obs", obs);
            logAutomacao = salvarLogDeEnvio(logAutomacao, corpoDoEnvio,"alterarStatusPesagem");
            String retorno = new UrnServidorBindingStub(url, null).trocastatus_automatiza(tag, status, msg1, msg2, msg3, 0, null);
            salvarLogDeRetorno(logAutomacao, retorno);
            return new NrCartaoStatusDTO(retorno);
        } catch (Exception e) {
            salvarLogDeErro(logAutomacao, e);
            throw new ServicoException(e.getMessage());
        }
    }

    /**
     * Faz o pre-cadastro de um veiculo usando como referencia o numero da PLACA e a BALANCA.
     *
     * @param tag
     * @param placa_cavalo
     * @param placa_carreta
     * @param transportador
     * @param motorista
     * @param razaosocial
     * @param endereco
     * @param municipio
     * @param produto
     * @param data_ordem
     * @param nr_ordem
     * @return
     */
    @Override
    public PreCadastroVeiculoDTO realizarPreCadastroVeiculo(int tag, String placa_cavalo, String placa_carreta, String transportador, String motorista, String razaosocial, String endereco, String municipio, String produto, String data_ordem, String nr_ordem) {
        LogAutomacao logAutomacao = new LogAutomacao();
        try {
            Map<String, Object> corpoDoEnvio = new HashMap<>();
            corpoDoEnvio.put("tag", tag);
            corpoDoEnvio.put("placa_cavalo", placa_cavalo);
            corpoDoEnvio.put("placa_carreta", placa_carreta);
            corpoDoEnvio.put("transportador", transportador);
            corpoDoEnvio.put("motorista", motorista);
            corpoDoEnvio.put("razaosocial", razaosocial);
            corpoDoEnvio.put("endereco", endereco);
            corpoDoEnvio.put("municipio", municipio);
            corpoDoEnvio.put("produto", produto);
            corpoDoEnvio.put("data_ordem", data_ordem);
            corpoDoEnvio.put("nr_ordem", nr_ordem);
            logAutomacao = salvarLogDeEnvio(logAutomacao, corpoDoEnvio,"realizarPreCadastroVeiculo");
            String retorno = new UrnServidorBindingStub(url, null).precad_automatiza(tag, placa_cavalo, placa_carreta, transportador, motorista, razaosocial, endereco, municipio, produto, data_ordem, nr_ordem);
            salvarLogDeRetorno(logAutomacao, retorno);
            return new PreCadastroVeiculoDTO(retorno);
        } catch (Exception e) {
            salvarLogDeErro(logAutomacao, e);
            throw new ServicoException(e.getMessage());
        }
    }

    private void salvarLogDeRetorno(LogAutomacao logAutomacao, String corpoDoRetorno) {
        logAutomacao.setConteudoRetornado(corpoDoRetorno);
        logAutomacaoDAO.save(logAutomacao);
    }

    private LogAutomacao salvarLogDeEnvio(LogAutomacao logAutomacao, Map<String, Object> corpoDoEnvio, String acao) {
        logAutomacao.setAcao(acao);
        logAutomacao.setConteudoEnviado(corpoDoEnvio.toString());
        logAutomacao = logAutomacaoDAO.save(logAutomacao);
        return logAutomacao;
    }

    private void salvarLogDeErro(LogAutomacao logAutomacao, Exception e) {
        logAutomacao.setExcecao(e.getMessage());
        logAutomacaoDAO.save(logAutomacao);
    }


}
