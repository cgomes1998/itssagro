package br.com.sjc.service;

import br.com.sjc.fachada.modelo.EmailSenderConfiguration;
import br.com.sjc.fachada.service.ConfiguracaoEmailService;
import br.com.sjc.fachada.service.EmailSenderService;
import br.com.sjc.modelo.ConfiguracaoEmail;
import br.com.sjc.util.email.ConfiguracaoEmailProtocolo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * Created by julio.bueno on 06/06/2019.
 */
@Service
public class EmailSenderServiceImpl extends EmailSenderService {

    @Autowired
    private ConfiguracaoEmailService configuracaoEmailService;

    @Value("${itssagro.ambiente.dev}")
    private Boolean ambienteDev;

    @Override
    public EmailSenderConfiguration getConfiguracao() {
        ConfiguracaoEmail configuracaoEmail = configuracaoEmailService.get();

        return new EmailSenderConfiguration() {
            @Override
            public String getHost() {
                return configuracaoEmail.getServer();
            }

            @Override
            public String getPorta() {
                return configuracaoEmail.getPorta();
            }

            @Override
            public String getRemetente() {
                return "no-reply@sjcbioenergia.com.br";
            }

            @Override
            public String getUsuario() {
                return configuracaoEmail.getUsuario();
            }

            @Override
            public String getSenha() {
                return configuracaoEmail.getSenha();
            }

            @Override
            public Boolean getAmbienteDev() {
                return ambienteDev;
            }

            @Override
            public ConfiguracaoEmailProtocolo getProtocolo() {
                return Objects.isNull(configuracaoEmail.getProtocolo()) ? ConfiguracaoEmailProtocolo.SSL : configuracaoEmail.getProtocolo();
            }

            @Override
            public String getUrlSistema() {
                return "http://localhost:8080/";
            }
        };
    }
}
