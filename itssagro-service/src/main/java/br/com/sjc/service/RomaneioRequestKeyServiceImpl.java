package br.com.sjc.service;

import br.com.sjc.fachada.service.RomaneioRequestKeyService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.dto.RomaneioRequestKeyDTO;
import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.sap.RomaneioRequestKey;
import br.com.sjc.persistencia.dao.RomaneioRequestKeyDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

@Service
public class RomaneioRequestKeyServiceImpl extends ServicoGenerico<Long, RomaneioRequestKey> implements RomaneioRequestKeyService {

    private static final Logger LOGGER = Logger.getLogger(RomaneioRequestKeyServiceImpl.class.getName());

    @Autowired
    private RomaneioRequestKeyDAO dao;

    @Override
    public RomaneioRequestKey obterPorRequestEOperacao(
            final String request,
            final String operacao
    ) {

        LOGGER.info("Request: " + request + "\n" + "Operação: " + operacao);

        return this.getDAO().findByRequestKeyAndRomaneioOperacaoOperacao(request, operacao);
    }

    @Override
    public List<RomaneioRequestKeyDTO> listarRomaneiosRequestsPorId(final List<Long> idsRomaneio) {

        return this.getDAO().findByRomaneioIdIn(idsRomaneio);
    }

    @Override
    public void atualizarStatusSAPRomaneioPorOperacaoERequest(
            final String chave,
            final String operacao,
            final StatusIntegracaoSAPEnum statusIntegracaoSAP
    ) {

        this.getDAO().updateStatusSAPRomaneioPorChaveEOperacao(chave, operacao, statusIntegracaoSAP);
    }

    @Override
    public RomaneioRequestKeyDAO getDAO() {

        return this.dao;
    }

    @Override
    public Class<? extends DataDTO> getDtoListagem() {

        return null;
    }

}
