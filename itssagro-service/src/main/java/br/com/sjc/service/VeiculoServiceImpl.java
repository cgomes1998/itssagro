package br.com.sjc.service;

import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.SAPService;
import br.com.sjc.fachada.service.VeiculoService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.dto.VeiculoDTO;
import br.com.sjc.modelo.endpoint.request.item.VeiculoRequest;
import br.com.sjc.modelo.endpoint.response.item.VeiculoResponse;
import br.com.sjc.modelo.enums.*;
import br.com.sjc.modelo.sap.TipoVeiculo;
import br.com.sjc.modelo.sap.Veiculo;
import br.com.sjc.modelo.sap.Veiculo_;
import br.com.sjc.modelo.sap.request.EventOutRequest;
import br.com.sjc.modelo.sap.response.item.RfcVeiculoResponseItem;
import br.com.sjc.modelo.sap.sync.SAPVeiculo;
import br.com.sjc.persistencia.dao.TipoVeiculoDAO;
import br.com.sjc.persistencia.dao.VeiculoDAO;
import br.com.sjc.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;

@Service
public class VeiculoServiceImpl extends ServicoGenerico<Long, Veiculo> implements VeiculoService {

    private static final Logger LOG = Logger.getLogger(VeiculoServiceImpl.class.getName());

    @Autowired
    private VeiculoDAO dao;

    @Autowired
    private TipoVeiculoDAO tipoVeiculoDAO;

    @Autowired
    private SAPService sapService;

    @Override
    public Specification<Veiculo> obterEspecification(final Paginacao<Veiculo> paginacao) {

        final Veiculo entidade = paginacao.getEntidade();

        final Specification<Veiculo> specification = new Specification<Veiculo>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<Veiculo> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                if (StringUtil.isNotNullEmpty(entidade.getChassi())) {

                    predicates.add(builder.equal(root.get("chassi"), entidade.getChassi()));
                }

                if (StringUtil.isNotNullEmpty(entidade.getRenavam())) {

                    predicates.add(builder.equal(root.get("renavam"), entidade.getRenavam()));
                }

                if (StringUtil.isNotNullEmpty(entidade.getPlaca1())) {
                    String placa = StringUtil.removerCaracteresEspeciais(entidade.getPlaca1()).toUpperCase();

                    predicates.add(builder.or(builder.equal(root.get("placa1"), placa), builder.equal(root.get("placa2"), placa), builder.equal(root.get("placa3"), placa)));
                }

                if (ObjetoUtil.isNotNull(entidade.getStatus())) {

                    predicates.add(builder.equal(root.get("status"), entidade.getStatus()));

                }

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Override
    public List<VeiculoResponse> listarParaArmazemTerceiros(final List<VeiculoRequest> requests) {

        if (ColecaoUtil.isNotEmpty(requests)) {

            AtomicBoolean teveNovosCadastros = new AtomicBoolean(false);

            List<VeiculoResponse> response = new ArrayList<>();

            requests.stream().forEach(item -> {

                Veiculo veiculo = this.getDAO().findTop1ByPlaca1(item.getPlaca());

                if (ObjetoUtil.isNotNull(veiculo)) {

                    response.add(veiculo.toResponse());

                } else {

                    veiculo = new Veiculo();

                    veiculo.setPlaca1(item.getPlaca());

                    veiculo.setUfPlaca1(StringUtil.isNotNullEmpty(item.getUf()) ? UFEnum.get(item.getUf()) : null);

                    this.getDAO().save(veiculo);

                    teveNovosCadastros.set(true);
                }
            });

            if (teveNovosCadastros.get()) {

                new Thread() {

                    @Override
                    public void run() {

                        sapService.enviarCadastro(DadosSincronizacaoEnum.VEICULO, null);
                    }

                }.start();
            }

            return response;
        }

        return null;
    }

    @Override
    public Veiculo salvar(Veiculo entidade) {

        Veiculo entidadeSaved = super.salvar(entidade);

        ItssAgroThreadPool.executarTarefa(() -> this.sapService.enviarCadastro(DadosSincronizacaoEnum.VEICULO, null));

        return entidadeSaved;
    }

    @Override
    public void salvarSAP(final List<RfcVeiculoResponseItem> retorno) {

        if (ColecaoUtil.isNotEmpty(retorno)) {

            retorno.stream().forEach(item -> {

                try {

                    if (StringUtil.isNotNullEmpty(item.getPlaca())) {

                        Veiculo entidadeSaved = this.getDAO().findByPlaca1(StringUtil.removerCaracteresEspeciais(item.getPlaca()));

                        if (ObjetoUtil.isNull(entidadeSaved)) {

                            entidadeSaved = new Veiculo();

                            entidadeSaved.setPlaca1(StringUtil.removerCaracteresEspeciais(item.getPlaca()).toUpperCase());

                            entidadeSaved.setDataCadastro(DateUtil.hoje());

                            entidadeSaved.setStatus(StatusEnum.ATIVO);

                        } else {

                            entidadeSaved.setDataAlteracao(DateUtil.hoje());
                        }

                        if (StringUtil.isNotNullEmpty(item.getPlaca())) {

                            entidadeSaved.setCodigo(StringUtil.removerCaracteresEspeciais(item.getPlaca()).toUpperCase());

                            entidadeSaved.setStatusCadastroSap(StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO);
                        }

                        if (StringUtil.isNotNullEmpty(item.getCodigo()) || StringUtil.isNotNullEmpty(item.getTipoVeiculo())) {

                            final TipoVeiculo tipoVeiculo = this.tipoVeiculoDAO.findFirstByCodigoOrTipo(item.getCodigo(), item.getTipoVeiculo());

                            if (ObjetoUtil.isNotNull(tipoVeiculo))
                                entidadeSaved.setTipo(tipoVeiculo);
                        }

                        entidadeSaved.setLogSAP("VEÍCULO SINCRONIZADO COM O SAP EM " + DateUtil.format("dd/MM/yyyy HH:mm:ss", DateUtil.hoje()));

                        if (StringUtil.isNotNullEmpty(entidadeSaved.getCodigo())) {

                            entidadeSaved.setLogSAP(entidadeSaved.getLogSAP() + "\nCÓDIGO: " + entidadeSaved.getCodigo());
                        }

                        entidadeSaved.setStatus(StringUtil.isNotNullEmpty(item.getStatus()) ? StatusEnum.getCodigoSAP(item.getStatus()) : null);

                        this.getDAO().save(entidadeSaved);
                    }

                } catch (Exception e) {

                    VeiculoServiceImpl.LOG.info(MessageFormat.format("ERRO AO PERSISTIR VEICULO: {0}, ERRO: {1}", item.getPlaca(), e.getMessage()));
                }

            });
        }
    }

    @Override
    public void salvarSAP(final EventOutRequest request, final String host, final String mandante) {

        if (ColecaoUtil.isNotEmpty(request.getData())) {

            final Map<String, SAPVeiculo> groupVeiculo = this.groupVeiculo(request);

            groupVeiculo.keySet().stream().forEach(itemGrupo -> {

                final SAPVeiculo veiculo = groupVeiculo.get(itemGrupo);

                Veiculo entidadeSaved = this.getDAO().findByPlaca1(veiculo.getPlaca1());

                entidadeSaved = veiculo.copy(entidadeSaved);

                if (StringUtil.isNotNullEmpty(entidadeSaved.getCodigoTipoVeiculo())) {
                    final TipoVeiculo tipoVeiculo = this.tipoVeiculoDAO.findFirstByCodigoOrTipo(entidadeSaved.getCodigoTipoVeiculo(), entidadeSaved.getCodigoTipoVeiculo());
                    if (ObjetoUtil.isNotNull(tipoVeiculo))
                        entidadeSaved.setTipo(tipoVeiculo);
                }

                entidadeSaved.setStatusRegistro(veiculo.getStatusRegistroSAP());

                entidadeSaved.setCodigo(entidadeSaved.getCodigo());

                if (ObjetoUtil.isNotNull(entidadeSaved.getUfPlaca1())) {

                    entidadeSaved.setCodigo(entidadeSaved.getUfPlaca1().name() + entidadeSaved.getPlaca1());

                    LOG.info(StringUtil.log("OK - SINCRONIZACAO DE VEICULO COM CODIGO - " + entidadeSaved.getCodigo()));

                } else {

                    LOG.info(StringUtil.log("SINCRONIZACAO DE VEICULO NAO VEIO CODIGO"));
                }

                if (ColecaoUtil.isNotEmpty(request.getLogRetorno())) {

                    AtomicBoolean success = new AtomicBoolean(true);

                    AtomicBoolean validar = new AtomicBoolean(true);

                    final StringBuilder sb = new StringBuilder();

                    request.getLogRetorno().forEach(registro -> {

                        sb.append(registro.getMessage());

                        sb.append("\n");

                        if (registro.getCodMessage().equals("E") || registro.getCodMessage().equals("A") && validar.get()) {

                            success.set(false);

                            validar.set(false);
                        }
                    });

                    if (ObjetoUtil.isNull(entidadeSaved.getCodigo()) || StringUtil.isEmpty(entidadeSaved.getCodigo())) {

                        success.set(false);

                        sb.append(" O VEICULO CHEGOU SEM O CODIGO");
                    }

                    if (success.get()) {

                        entidadeSaved.setStatusCadastroSap(StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO);

                    } else {

                        entidadeSaved.setStatusCadastroSap(StatusCadastroSAPEnum.RETORNO_COM_ERRO);
                    }

                    entidadeSaved.setLogSAP(StringUtil.isEmpty(sb.toString()) ? null : sb.toString());
                }

                if (StringUtil.isNotNullEmpty(entidadeSaved.getCodigo())) {

                    this.dao.save(entidadeSaved);
                }
            });

        } else if (ObjetoUtil.isNotNull(request.getStatusSAP()) && !StatusIntegracaoSAPEnum.COMPLETO.getCodigo().toString().equals(request.getStatusSAP())) {

            Veiculo veiculo = this.dao.findByRequestKey(request.getRequestKey());

            if (ObjetoUtil.isNotNull(veiculo) && !veiculo.getStatus().equals(StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO)) {

                veiculo.setStatusCadastroSap(StatusCadastroSAPEnum.RETORNO_COM_ERRO);

                if (ColecaoUtil.isNotEmpty(request.getLogRetorno())) {

                    request.getLogRetorno().forEach(registro -> {

                        veiculo.setLogSAP(registro.getCodMessage() + " - " + registro.getMessage());
                    });
                }

                this.dao.save(veiculo);
            }
        }
    }

    private Map<String, SAPVeiculo> groupVeiculo(final EventOutRequest request) {

        final Map<String, SAPVeiculo> groupVeiculo = new HashMap<String, SAPVeiculo>();

        request.getData().forEach(registro -> {

            try {

                SAPVeiculo entidade = groupVeiculo.get(registro.getItemDocumento().trim());

                if (ObjetoUtil.isNull(entidade)) {

                    entidade = SAPVeiculo.newInstance();
                }

                entidade.update(registro.getCampo().trim(), registro.getValor().trim());

                groupVeiculo.put(registro.getItemDocumento().trim(), entidade);

            } catch (final Exception e) {

                VeiculoServiceImpl.LOG.info("ERRO: " + e.getMessage());
            }
        });

        return groupVeiculo;
    }

    @Override
    public void sincronizar(final List<Veiculo> entidades) {

        if (ColecaoUtil.isNotEmpty(entidades)) {

            entidades.forEach(item -> {

                Veiculo entidadeSaved = this.getDAO().findByCodigo(item.getCodigo());

                item.setId(null);

                if (ObjetoUtil.isNotNull(entidadeSaved)) {

                    item.setId(entidadeSaved.getId());
                }

                if (ObjetoUtil.isNotNull(item.getTipo())) {

                    item.setTipo(this.tipoVeiculoDAO.findByCodigo(item.getTipo().getCodigo()));
                }
            });

            entidades.stream().forEach(i -> {
                this.dao.save(i);
            });
        }
    }

    @Override
    public List<Veiculo> listarPreCadastro() {

        Paginacao paginacao = new Paginacao();

        paginacao.setListarTodos(true);

        paginacao.setSpecification(((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(root.get(Veiculo_.statusCadastroSap).in(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.NAO_ENVIADO,
                StatusCadastroSAPEnum.RETORNO_COM_ERRO})), criteriaBuilder.isNotNull(root.get(Veiculo_.ufPlaca1)))));

        Collection<Veiculo> content = listar(paginacao).getContent();

        return ObjetoUtil.isNotNull(content) ? new ArrayList(content) : new ArrayList<>();
    }

    @Override
    public List<Veiculo> listarSincronizados(final Date data) {

        return ObjetoUtil.isNull(data) ? this.dao.findByStatusCadastroSapIn(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO})) :
                this.dao.findByStatusCadastroSapInAndDataCadastroGreaterThanEqual(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO}), data);
    }

    @Override
    public Veiculo obterPorCodigo(final String codigo) {

        return this.getDAO().findByCodigo(codigo);
    }

    @Override
    public void preSalvar(Veiculo entidade) {

        if (StringUtil.isNotNullEmpty(entidade.getPlaca1())) {

            entidade.setPlaca1(StringUtil.removerCaracteresEspeciais(entidade.getPlaca1()).toUpperCase());

            this.veiculoJaExiste(entidade);
        }

        if (StringUtil.isNotNullEmpty(entidade.getPlaca2())) {

            entidade.setPlaca2(StringUtil.removerCaracteresEspeciais(entidade.getPlaca2()).toUpperCase());
        }

        if (StringUtil.isNotNullEmpty(entidade.getPlaca3())) {

            entidade.setPlaca3(StringUtil.removerCaracteresEspeciais(entidade.getPlaca3()).toUpperCase());
        }

        entidade.setStatusCadastroSap(StatusCadastroSAPEnum.NAO_ENVIADO);
    }

    private void veiculoJaExiste(final Veiculo entidade) {

        final Boolean isCadastro = entidade.getId() == null;

        if ((isCadastro && dao.existsByPlaca1(entidade.getPlaca1())) || (!isCadastro && dao.existsByPlaca1AndNeId(entidade.getPlaca1(), entidade.getId()))) {

            throw new ServicoException("Já existe um veículo com a placa: " + entidade.getPlaca1());
        }
    }

    @Override
    public List<Veiculo> buscarVeiculosAutoComplete(String value) {

        if (StringUtil.isNotNullEmpty(value)) {
            value = value.replace("-", StringUtil.empty());
        }
        List<Veiculo> veiculos = this.dao.findTop10ByPlaca1IgnoreCaseContainingOrderByPlaca1Asc(value);
        veiculos.removeIf(veiculo -> StatusEnum.INATIVO.equals(veiculo.getStatus()));
        return veiculos;
    }

    @Override
    public List<Veiculo> buscarVeiculosAtivosAutoComplete(String value) {

        return this.dao.findTop10ByStatusAndPlaca1IgnoreCaseContainingOrderByPlaca1Asc(StatusEnum.ATIVO, value);
    }

    @Override
    @Transactional
    public void alterarStatus(Veiculo entidade) {

        super.alterarStatus(entidade);

        registrarLogAtividade(entidade, StatusEnum.ATIVO.equals(entidade.getStatus()) ? LogAtividadeAcao.ATIVAR : LogAtividadeAcao.INATIVAR);
    }

    @Override
    public Class<VeiculoDTO> getDtoListagem() {

        return VeiculoDTO.class;
    }

    @Override
    public VeiculoDAO getDAO() {

        return this.dao;
    }

    @Override
    public Boolean getRegistrarLogAtividade() {

        return Boolean.TRUE;
    }

}
