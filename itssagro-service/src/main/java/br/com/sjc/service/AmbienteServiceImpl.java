package br.com.sjc.service;

import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.AmbienteService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.cfg.Ambiente;
import br.com.sjc.modelo.dto.AmbienteDTO;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.persistencia.dao.AmbienteDAO;
import br.com.sjc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class AmbienteServiceImpl extends ServicoGenerico<Long, Ambiente> implements AmbienteService {

    @Autowired
    private AmbienteDAO dao;
    
    @Override
    public Specification<Ambiente> obterEspecification(final Paginacao<Ambiente> paginacao) {

        final Ambiente entidade = paginacao.getEntidade();

        final Specification<Ambiente> specification = new Specification<Ambiente>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<Ambiente> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                if (StringUtil.isNotNullEmpty(entidade.getDescricao())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("descricao")), "%" + entidade.getDescricao().toUpperCase() + "%")));
                }

                if (StringUtil.isNotNullEmpty(entidade.getHost())) {

                    predicates.add(builder.equal(root.get("host"), entidade.getHost()));
                }

                if (StringUtil.isNotNullEmpty(entidade.getRfcPorta())) {

                    predicates.add(builder.equal(root.get("rfcPorta"), entidade.getRfcPorta()));
                }

                if (StringUtil.isNotNullEmpty(entidade.getMandante())) {

                    predicates.add(builder.equal(root.get("mandante"), entidade.getMandante()));
                }

                if (StringUtil.isNotNullEmpty(entidade.getNumeroInstancia())) {

                    predicates.add(builder.equal(root.get("numeroInstancia"), entidade.getNumeroInstancia()));
                }

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Override
    public Ambiente obterPorHostEMandante(final String host, final String mandante) {

        return this.dao.findByHostAndMandante(host, mandante);
    }

    @Override
    public Ambiente obterAmbienteLogado() {

        return this.getDAO().findAll().get(0);
    }

    @Override
    public AmbienteDAO getDAO() {

        return this.dao;
    }

    @Override
    public Class<AmbienteDTO> getDtoListagem() {

        return AmbienteDTO.class;
    }

}
