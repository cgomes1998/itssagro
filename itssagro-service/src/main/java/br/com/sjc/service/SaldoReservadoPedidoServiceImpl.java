package br.com.sjc.service;

import br.com.sjc.fachada.service.SaldoReservadoPedidoService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.SaldoReservadoPedido;
import br.com.sjc.modelo.SaldoReservadoPedidoDTO;
import br.com.sjc.modelo.request.SaldoPedidoReservadoRequest;
import br.com.sjc.modelo.response.SaldoPedidoReservadoResponse;
import br.com.sjc.persistencia.dao.SaldoReservadoPedidoDAO;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.logging.Logger;

@Service
public class SaldoReservadoPedidoServiceImpl extends ServicoGenerico<Long, SaldoReservadoPedido> implements SaldoReservadoPedidoService {

    private static final Logger LOG = Logger.getLogger(SaldoReservadoPedido.class.getName());

    @Autowired
    private SaldoReservadoPedidoDAO dao;

    @Override
    public void reservarSaldoPedido(String numeroRomaneioConsumidor, String numeroPedido, String itemPedido, BigDecimal saldoReservado) {
        SaldoReservadoPedido saldoReservadoPedido = this.dao.findFirstByNumeroRomaneioConsumidorAndNumeroPedidoAndItemPedido(numeroRomaneioConsumidor,
                new Long(numeroPedido).toString(), new Long(itemPedido).toString());

        if (ObjetoUtil.isNotNull(saldoReservadoPedido)) {
            saldoReservadoPedido.setSaldoReservado(saldoReservado);
            LOG.fine("(SaldoReservadoPedidoService.reservarSaldoPedido) SALDO DE PEDIDO ATUALIZADO -> " + saldoReservadoPedido);
        } else {
            saldoReservadoPedido = new SaldoReservadoPedido();
            saldoReservadoPedido.setNumeroRomaneioConsumidor(numeroRomaneioConsumidor);
            saldoReservadoPedido.setNumeroPedido(new Long(numeroPedido).toString());
            saldoReservadoPedido.setItemPedido(itemPedido);
            saldoReservadoPedido.setSaldoReservado(saldoReservado);
            LOG.fine("(SaldoReservadoPedidoService.reservarSaldoPedido) SALDO DE PEDIDO RESERVADO -> " + saldoReservadoPedido);
        }

        this.dao.save(saldoReservadoPedido);
    }

    @Override
    public void liberarSaldoPedido(String numeroRomaneioConsumidor, String numeroPedido, String itemPedido) {
        SaldoReservadoPedido saldoReservadoPedido = this.dao.findFirstByNumeroRomaneioConsumidorAndNumeroPedidoAndItemPedido(numeroRomaneioConsumidor,
                new Long(numeroPedido).toString(), new Long(itemPedido).toString());
        if (ObjetoUtil.isNotNull(saldoReservadoPedido)) {
            LOG.fine("(SaldoReservadoPedidoService.liberarSaldoPedido) SALDO DE PEDIDO LIBERADO -> " + saldoReservadoPedido);
            this.dao.delete(saldoReservadoPedido);
        }
    }

    @Override
    public SaldoPedidoReservadoResponse obterSaldoReservadoPedido(SaldoPedidoReservadoRequest request) {
        BigDecimal saldoReservado;

        if (StringUtil.isNotNullEmpty(request.getNumeroRomaneioConsumidor())) {
            saldoReservado = this.dao.obterSaldoReservado(request.getNumeroRomaneioConsumidor(), new Long(request.getNumeroPedido()).toString(),
                    new Long(request.getItemPedido()).toString());
        } else {
            saldoReservado = this.dao.obterSaldoReservado(new Long(request.getNumeroPedido()).toString(),
                    new Long(request.getItemPedido()).toString());
        }

        return SaldoPedidoReservadoResponse.builder().saldoReservado(saldoReservado).build();
    }

    @Override
    public SaldoReservadoPedidoDAO getDAO() {

        return this.dao;
    }

    @Override
    public Class<SaldoReservadoPedidoDTO> getDtoListagem() {

        return SaldoReservadoPedidoDTO.class;
    }

}
