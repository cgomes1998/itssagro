package br.com.sjc.service;

import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.TransportadoraService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.dto.TransportadoraDTO;
import br.com.sjc.modelo.endpoint.request.item.TransportadoraRequest;
import br.com.sjc.modelo.endpoint.response.item.TransportadoraResponse;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import br.com.sjc.modelo.sap.Transportadora;
import br.com.sjc.modelo.sap.request.EventOutRequest;
import br.com.sjc.modelo.sap.sync.SAPTransportadora;
import br.com.sjc.persistencia.dao.TransportadoraDAO;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TransportadoraServiceImpl extends ServicoGenerico<Long, Transportadora> implements TransportadoraService {

    @Autowired
    private TransportadoraDAO dao;

    @Override
    public Specification<Transportadora> obterEspecification(final Paginacao<Transportadora> paginacao) {

        final Transportadora entidade = paginacao.getEntidade();

        final Specification<Transportadora> specification = new Specification<Transportadora>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<Transportadora> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                if (StringUtil.isNotNullEmpty(entidade.getCodigo())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("codigo")), "%" + entidade.getCodigo().toUpperCase() + "%")));
                }

                if (StringUtil.isNotNullEmpty(entidade.getDescricao())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("descricao")), "%" + entidade.getDescricao().toUpperCase() + "%")));
                }

                if (StringUtil.isNotNullEmpty(entidade.getCnpj())) {

                    predicates.add(builder.equal(root.get("cnpj"), StringUtil.removerCaracteresEspeciais(entidade.getCnpj())));
                }

                if (StringUtil.isNotNullEmpty(entidade.getCpf())) {

                    predicates.add(builder.equal(root.get("cpf"), StringUtil.removerCaracteresEspeciais(entidade.getCpf())));
                }

                if (ObjetoUtil.isNotNull(entidade.getStatusRegistro())) {

                    predicates.add(builder.equal(root.get("statusRegistro"), entidade.getStatusRegistro()));
                }

                if (ObjetoUtil.isNotNull(entidade.getStatusCadastroSap())) {

                    predicates.add(builder.equal(root.get("statusCadastroSap"), entidade.getStatusCadastroSap()));
                }

                query.orderBy(builder.asc(root.get("codigo")));

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Override
    public synchronized void salvarSAP(final EventOutRequest request, final String host, String mandante) {

        if (ColecaoUtil.isNotEmpty(request.getData())) {

            final Map<String, SAPTransportadora> groupTransportadora = this.groupTransportadora(request);

            groupTransportadora.keySet().forEach(itemGrupo -> {

                final SAPTransportadora transportadora = groupTransportadora.get(itemGrupo);

                Transportadora entidadeSaved = this.dao.findFirstByCodigo(transportadora.getCodigo());

                entidadeSaved = transportadora.copy(entidadeSaved);

                if (StringUtil.isNotNullEmpty(entidadeSaved.getCodigo())) {

                    this.salvar(entidadeSaved);
                }
            });
        }
    }

    private Map<String, SAPTransportadora> groupTransportadora(final EventOutRequest request) {

        final Map<String, SAPTransportadora> groupTransportadora = new HashMap<String, SAPTransportadora>();

        request.getData().forEach(registro -> {

            try {

                SAPTransportadora entidade = groupTransportadora.get(registro.getItemDocumento().trim());

                if (ObjetoUtil.isNull(entidade)) {

                    entidade = SAPTransportadora.newInstance();
                }

                entidade.update(registro.getCampo().trim(), registro.getValor().trim());

                groupTransportadora.put(registro.getItemDocumento().trim(), entidade);

            } catch (final Exception e) {

                e.printStackTrace();
            }
        });

        return groupTransportadora;
    }

    @Override
    public List<Transportadora> listarSincronizados(final Date data) {

        return ObjetoUtil.isNull(data) ? this.dao.findByStatusCadastroSapIn(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO})) : this.dao.findByStatusCadastroSapInAndDataCadastroGreaterThanEqual(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO}), data);
    }

    @Override
    public void sincronizar(final List<Transportadora> entidades) {

        if (ColecaoUtil.isNotEmpty(entidades)) {

            entidades.forEach(item -> {

                Transportadora entidadeSaved = this.getDAO().findByCodigo(item.getCodigo());

                item.setId(null);

                if (ObjetoUtil.isNotNull(entidadeSaved)) {

                    item.setId(entidadeSaved.getId());
                }
            });

            this.salvar(entidades);
        }
    }

    @Override
    public List<Transportadora> buscarTransportadorasAutoComplete(final String value) {
        return this.dao.findTop10ByStatusRegistroAndCodigoIgnoreCaseContainingOrCnpjIgnoreCaseContainingOrCpfIgnoreCaseContainingOrDescricaoIgnoreCaseContainingOrderByCodigoAsc(StatusRegistroEnum.ATIVO, value, value, value, value);
    }

    @Override
    public List<TransportadoraResponse> listarParaArmazemDeTerceiros(final List<TransportadoraRequest> requests) {

        if (ColecaoUtil.isNotEmpty(requests)) {

            final List<String> cpfs = requests.stream().filter(i -> StringUtil.isNotNullEmpty(i.getCnpj()) && i.getCnpj().length() == 11).map(TransportadoraRequest::getCnpj).collect(Collectors.toList());

            final List<String> cnpjs = requests.stream().filter(i -> StringUtil.isNotNullEmpty(i.getCnpj()) && i.getCnpj().length() == 14).map(TransportadoraRequest::getCnpj).collect(Collectors.toList());

            final List<Transportadora> transportadoras = this.getDAO().findByCnpjInOrCpfIn(cnpjs, cpfs);

            if (ColecaoUtil.isNotEmpty(transportadoras)) {

                return transportadoras.stream().map(Transportadora::toResponse).collect(Collectors.toList());
            }
        }

        return null;
    }

    @Override
    public Transportadora obterPorCodigo(final String codigo) {
        return this.getDAO().findByCodigo(codigo);
    }

    @Override
    public TransportadoraDAO getDAO() {
        return this.dao;
    }

    @Override
    public Class<? extends DataDTO> getDtoListagem() {
        return TransportadoraDTO.class;
    }
}
