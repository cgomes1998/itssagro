package br.com.sjc.service;

import br.com.sjc.dynamicReport.formulario.FormularioGerarPdfComResposta;
import br.com.sjc.dynamicReport.formulario.FormularioGerarPdfSemResposta;
import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.FormularioService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.*;
import br.com.sjc.modelo.dto.FormularioDTO;
import br.com.sjc.modelo.enums.FormularioStatus;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.persistencia.dao.FormularioDAO;
import br.com.sjc.persistencia.dao.RomaneioDAO;
import br.com.sjc.persistencia.dao.SenhaFormularioRespostaDAO;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Predicate;
import java.io.OutputStream;
import java.util.*;

/**
 * Created by julio.bueno on 04/07/2019.
 */
@Service
public class FormularioServiceImpl extends ServicoGenerico<Long, Formulario> implements FormularioService {

    @Autowired
    private FormularioDAO formularioDAO;

    @Autowired
    private RomaneioDAO romaneioDAO;

    @Override
    public DAO<Long, Formulario> getDAO() {

        return formularioDAO;
    }

    @Override
    public Class<FormularioDTO> getDtoListagem() {

        return FormularioDTO.class;
    }

    @Override
    public Specification<Formulario> obterEspecification(Paginacao<Formulario> paginacao) {

        return (Specification<Formulario>) (root, query, criteriaBuilder) -> {
            Formulario formulario = paginacao.getEntidade();

            List<Predicate> predicates = new ArrayList<>();

            if (!Objects.isNull(formulario.getId())) {
                predicates.add(criteriaBuilder.equal(root.get("id"), formulario.getId()));
            }

            if (!StringUtils.isEmpty(formulario.getDescricao())) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("descricao")), MatchMode.ANYWHERE.toMatchString(formulario.getDescricao())));
            }

            if (!Objects.isNull(formulario.getFormularioStatus())) {
                predicates.add(criteriaBuilder.equal(root.get("formularioStatus"), formulario.getFormularioStatus()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    @Override
    public void preSalvar(Formulario formulario) {

        super.preSalvar(formulario);

        formulario.getItens().forEach(item -> {
            item.setFormulario(formulario);
            item.getOpcoes().forEach(opcao -> opcao.setFormularioItem(item));
        });
    }

    @Override
    public Boolean verificarSeFormularioEstaEmUso(Long id) {

        return false;
    }

    @Override
    public void publicar(Formulario formulario) {

        formulario.setFormularioStatus(FormularioStatus.PUBLICADO);
        salvar(formulario);
    }

    @Override
    public void descontinuar(Formulario formulario) {

        formulario.setFormularioStatus(FormularioStatus.DESCONTINUADO);
        salvar(formulario);
    }

    @Override
    public void clonar(Long id) {

        Formulario formularioTemp = get(id);

        Formulario clone = new Formulario();
        clone.setStatus(StatusEnum.ATIVO);
        clone.setFormularioStatus(FormularioStatus.EM_CRIACAO);
        clone.setDescricao(formularioTemp.getDescricao());
        clone.setCabecalhoDadosCliente(formularioTemp.getCabecalhoDadosCliente());
        clone.setCabecalhoDadosMotorista(formularioTemp.getCabecalhoDadosMotorista());
        clone.setCabecalhoDadosTransportadora(formularioTemp.getCabecalhoDadosTransportadora());
        clone.setCabecalhoDadosVeiculo(formularioTemp.getCabecalhoDadosVeiculo());

        formularioTemp.getItens().forEach(item -> {
            FormularioItem itemClone = new FormularioItem();
            itemClone.setTitulo(item.getTitulo());
            itemClone.setRequired(item.getRequired());
            itemClone.setOrdem(item.getOrdem());
            itemClone.setTamanho(item.getTamanho());
            itemClone.setValorEsperado(item.getValorEsperado());
            itemClone.setTipo(item.getTipo());

            item.getOpcoes().forEach(opcao -> {
                FormularioItemOpcao opcaoClone = new FormularioItemOpcao();
                opcaoClone.setTitulo(opcao.getTitulo());
                opcaoClone.setOrdem(opcao.getOrdem());

                itemClone.addOpcao(opcaoClone);
            });

            clone.addItem(itemClone);
        });

        salvar(clone);
    }

    @Override
    public List<FormularioItem> listarItens(Long formularioId) {

        return get(formularioId).getItens();
    }

    @Override
    public void gerarPdfFormulario(Long id, OutputStream output) {

        FormularioGerarPdfSemResposta.build(get(id)).gerarPdf(output);
    }

    @Override
    public void gerarPdfFormularioRomaneio(final Long idFormulario, final Long idRomaneio, OutputStream outputStream) {

        final Optional<Formulario> formulario = formularioDAO.findById(idFormulario);

        final Optional<Romaneio> romaneio = romaneioDAO.findById(idRomaneio);

        List<SenhaFormularioResposta> respostas = new ArrayList<>();

        Senha senha = romaneio.get().getSenha();

        if (romaneio.isPresent() && ObjetoUtil.isNotNull(senha)) {

            SenhaFormulario formularioAtual =
                    senha.getFormularios().stream().filter(_formulario -> _formulario.getFormulario().getId().equals(idFormulario)).sorted(Comparator.comparing(SenhaFormulario::getDataCadastro).reversed()).findFirst().orElse(senha.getFormularioAtual());

            if (ObjetoUtil.isNotNull(formularioAtual) && ColecaoUtil.isNotEmpty(formularioAtual.getRespostas())) {

                respostas.addAll(formularioAtual.getRespostas());
            }
        }

        FormularioGerarPdfComResposta.build(formulario.orElse(null), romaneio.orElse(null), respostas).gerarPdf(outputStream);
    }

    @Override
    public List<Formulario> listarPublicados() {

        return formularioDAO.findByFormularioStatus(FormularioStatus.PUBLICADO);
    }

    @Override
    public Boolean getRegistrarLogAtividade() {

        return Boolean.TRUE;
    }

}
