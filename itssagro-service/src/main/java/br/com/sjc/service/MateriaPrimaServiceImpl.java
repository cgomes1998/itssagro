package br.com.sjc.service;

import br.com.sjc.fachada.service.MateriaPrimaService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.MateriaPrima;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.dto.MateriaPrimaDTO;
import br.com.sjc.persistencia.dao.MateriaPrimaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MateriaPrimaServiceImpl extends ServicoGenerico<Long, MateriaPrima> implements MateriaPrimaService {
    @Autowired
    private MateriaPrimaDAO dao;

    @Override
    public MateriaPrimaDAO getDAO() {
        return this.dao;
    }

    @Override
    public Class<? extends DataDTO> getDtoListagem() {

        return MateriaPrimaDTO.class;
    }
}
