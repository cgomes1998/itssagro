package br.com.sjc.service;

import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.AnaliseService;
import br.com.sjc.fachada.service.LaudoService;
import br.com.sjc.fachada.service.LogAtividadeService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.*;
import br.com.sjc.modelo.dto.AnaliseDTO;
import br.com.sjc.modelo.dto.AnaliseFiltroDTO;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.persistencia.dao.AnaliseDAO;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.bundle.MessageSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AnaliseServiceImpl extends ServicoGenerico<Long, Analise> implements AnaliseService {

    @Autowired
    private AnaliseDAO dao;

    @Autowired
    private LaudoService laudoService;

    @Override
    public Analise obterAnalisePorMaterial(AnaliseFiltroDTO filtroDTO) {

        if (ObjetoUtil.isNull(filtroDTO) || ObjetoUtil.isNull(filtroDTO.getMaterial())) {
            return null;
        }

        final Analise analise = dao.findByMaterialId(filtroDTO.getMaterial().getId());

        List<AnaliseItem> analiseItens = analise.getItens();

        if (ObjetoUtil.isNull(analise) || ColecaoUtil.isEmpty(analiseItens)) {
            return null;
        }

        analiseItens = analiseItens.stream().filter(i -> StatusEnum.ATIVO.equals(i.getStatus())).collect(Collectors.toList());

        if (ObjetoUtil.isNotNull(analise) && filtroDTO.getMaterial().isBuscarCertificadoQualidade()) {

            final Laudo laudo = laudoService.obterCertificadoPorMaterialETanque(filtroDTO);

            if (ObjetoUtil.isNotNull(laudo) && ColecaoUtil.isNotEmpty(laudo.getItens())) {

                analise.setIdLaudo(laudo.getId());

                analise.setAssinatura(laudo.getAssinatura());

                analise.setLacreAmostra(laudo.getLacreAmostra());

                analiseItens.stream().forEach(i -> {

                    LaudoItem laudoItem =
                            laudo.getItens().stream().filter(li -> li.getCaracteristica().concat(":").concat(li.getSubCaracteristica()).equalsIgnoreCase(i.getCaracteristica().concat(":").concat(i.getSubCaracteristica()))).findFirst().orElse(null);

                    if (ObjetoUtil.isNotNull(laudoItem)) {

                        i.setResultado(laudoItem.getResultado());
                    }
                });
            }
        }

        analise.setItens(analiseItens);

        return analise;
    }

    @Override
    public Specification<Analise> obterEspecification(final Paginacao<Analise> paginacao) {

        return (Root<Analise> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) -> {
            final List<Predicate> predicates = new ArrayList<>();

            if (paginacao.getEntidade().getMaterial() != null) {
                predicates.add(builder.and(builder.equal(root.get("material"), paginacao.getEntidade().getMaterial())));
            }
            if (paginacao.getEntidade().getStatus() != null) {
                predicates.add(builder.and(builder.equal(root.get("status"), paginacao.getEntidade().getStatus())));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }

    private Specification<Analise> obterEspecificacaoMaterial(final Long materialId) {

        return (Root<Analise> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) -> builder.equal(root.join("material").get("id"), materialId);
    }

    @Override
    public AnaliseDAO getDAO() {

        return this.dao;
    }

    @Override
    public Class<AnaliseDTO> getDtoListagem() {

        return AnaliseDTO.class;
    }

    @Override
    public List<Analise> listarPorMaterial(Long id) {

        return this.getDAO().findAll(obterEspecificacaoMaterial(id));
    }

    @Override
    public void validar(Analise entidade) throws Exception {

        if (this.getDAO().existsByMaterial_Id(entidade.getMaterial().getId())) {

            throw new Exception(MessageSupport.getMessage("MSGE021"));
        }
    }

    @Override
    public void preSalvar(Analise analise) {

        super.preSalvar(analise);

        analise.getItens().forEach(item -> item.setAnalise(analise));
    }


    @Override
    public Boolean getRegistrarLogAtividade() {

        return Boolean.TRUE;
    }

}
