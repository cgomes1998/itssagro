package br.com.sjc.service;

import br.com.sjc.fachada.service.BridgeService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.sap.Bridge;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.RomaneioRequestKey;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;
import br.com.sjc.persistencia.dao.BridgeDAO;
import br.com.sjc.persistencia.dao.RomaneioRequestKeyDAO;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class BridgeServiceImpl extends ServicoGenerico<Long, Bridge> implements BridgeService {

    @Autowired
    private BridgeDAO dao;

    @Autowired
    private RomaneioRequestKeyDAO romaneioRequestKeyDAO;

    @Override
    public void salvar(final List<EventInRequestItem> itens, final Romaneio romaneio) {

        if (!this.existeBridgeParaRomaneio(romaneio)) {

            if (ColecaoUtil.isNotEmpty(itens)) {

                itens.stream().forEach(i -> {

                    this.getDAO().save(EventInRequestItem.toBridge(i, romaneio));
                });
            }

            vincularRequestKeyPorItem(romaneio, itens);

            vincularRequestKeyCabecalho(romaneio, itens);
        }
    }

    private void vincularRequestKeyCabecalho(Romaneio romaneio, final List<EventInRequestItem> itens) {

        if (!romaneio.getOperacao().isPossuiDivisaoCarga()) {

            Map<String, List<EventInRequestItem>> eventsMapRequest = itens.stream().collect(Collectors.groupingBy(EventInRequestItem::getRequestKey));

            romaneio.setRequestKey(StringUtil.completarZeroAEsquerda(20, eventsMapRequest.entrySet().stream().map(i -> i.getKey()).findFirst().get()));
        }
    }

    private void vincularRequestKeyPorItem(Romaneio romaneio, final List<EventInRequestItem> itens) {

        if (romaneio.getOperacao().isPossuiDivisaoCarga()) {

            AtomicInteger index = new AtomicInteger(0);

            Map<String, List<EventInRequestItem>> eventsMapRequest = itens.stream().collect(Collectors.groupingBy(EventInRequestItem::getRequestKey));

            List<String> requests = eventsMapRequest.entrySet().stream().map(i -> i.getKey()).collect(Collectors.toList());

            romaneio.getItens().forEach(item -> {

                if (requests.size() > 1) {

                    index.addAndGet(10);

                    String _requestKey = requests.stream().filter(i -> {

                        String[] pattern = i.split("ERM");

                        String requestPrimeiraParte = pattern[0];

                        String count = requestPrimeiraParte.substring(requestPrimeiraParte.length() - 2);

                        return count.equals(index.toString());
                    }).findFirst().orElse(null);

                    item.setRequestKey(StringUtil.completarZeroAEsquerda(20, _requestKey));

                } else {

                    item.setRequestKey(StringUtil.completarZeroAEsquerda(20, eventsMapRequest.entrySet().stream().map(i -> i.getKey()).findFirst().get()));
                }

                romaneioRequestKeyDAO.save(new RomaneioRequestKey(romaneio, item.getRequestKey()));
            });
        }
    }

    @Override
    public boolean existeBridgeParaRomaneio(final Romaneio romaneio) {

        return this.getDAO().existsByRomaneioId(romaneio.getId());
    }

    @Override
    @Transactional(readOnly = true)
    public List<Bridge> listarBridges(final Long idRomaneio) {

        return this.getDAO().findByRomaneioId(idRomaneio);
    }

    @Override
    public BridgeDAO getDAO() {

        return this.dao;
    }

}
