package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.rest.paginacao.Pagina;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.LoteService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.Lacre;
import br.com.sjc.modelo.Lacre_;
import br.com.sjc.modelo.Lote;
import br.com.sjc.modelo.Lote_;
import br.com.sjc.modelo.dto.LoteDTO;
import br.com.sjc.modelo.enums.StatusLacreEnum;
import br.com.sjc.persistencia.dao.LacreDAO;
import br.com.sjc.persistencia.dao.LoteDAO;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class LoteServiceImpl extends ServicoGenerico<Long, Lote> implements LoteService {

    @Autowired
    private LoteDAO loteDAO;

    @Autowired
    private LacreDAO lacreDAO;

    @Override
    public DAO<Long, Lote> getDAO() {

        return loteDAO;
    }

    @Override
    public Class<LoteDTO> getDtoListagem() {

        return LoteDTO.class;
    }

    @Override
    public void preSalvar(Lote lote) {

        super.preSalvar(lote);
        lote.getLacres().forEach(lacre -> {
            if (lote.isNew() && lacreDAO.existsByCodigo(lacre.getCodigo())) {
                throw new ServicoException("Já existe lacres cadastradas no sistema dentro do intervalo informado.");
            }
            lacre.setLote(lote);
        });
    }

    @Override
    public Specification<Lote> obterEspecification(Paginacao<Lote> paginacao) {

        return (Specification<Lote>) (root, query, criteriaBuilder) -> {
            Lote lote = paginacao.getEntidade();
            List<Predicate> predicates = new ArrayList<>();
            Join<Lote, Lacre> lacreJoin = root.join(Lote_.lacres, JoinType.LEFT);
            if (Objects.nonNull(lote.getCodigo())) {
                predicates.add(criteriaBuilder.like(lacreJoin.get(Lacre_.codigo), MatchMode.ANYWHERE.toMatchString(lote.getCodigo())));
            }
            if (ObjetoUtil.isNotNull(lote.getDataCadastroDe())) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(lacreJoin.get(Lacre_.dataCadastro), DateUtil.obterDataHoraZerada(lote.getDataCadastroDe())));
            }
            if (ObjetoUtil.isNotNull(lote.getDataCadastroAte())) {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(lacreJoin.get(Lacre_.dataCadastro), DateUtil.converterDateFimDia(lote.getDataCadastroAte())));
            }
            if (Objects.nonNull(lote.getStatusLacre())) {
                predicates.add(criteriaBuilder.equal(lacreJoin.get(Lacre_.statusLacre), lote.getStatusLacre()));
            }
            query.distinct(true);
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    @Override
    public Pagina<LoteDTO> dtoListar(Paginacao paginacao) {

        Pagina<LoteDTO> loteDTOPagina = super.dtoListar(paginacao, LoteDTO.class);

        if (loteDTOPagina.getTotalElements() > 0l) {

            List<Long> lotesId = loteDTOPagina.getContent().stream().map(LoteDTO::getId).collect(Collectors.toList());

            List<LoteDTO> lotesComTodosLacresDisponiveis = loteDAO.getLotesComTodosLacresStatusIgualA(StatusLacreEnum.DISPONIVEL, lotesId);

            if (ColecaoUtil.isNotEmpty(lotesComTodosLacresDisponiveis)) {

                loteDTOPagina.getContent().stream().forEach(loteDTO -> loteDTO.setTodosLacresDisponiveis(lotesComTodosLacresDisponiveis.stream().anyMatch((loteDTO1 -> loteDTO1.getId().equals(loteDTO.getId())))));
            }
        }

        return loteDTOPagina;
    }

    @Override
    public Boolean getRegistrarLogAtividade() {

        return Boolean.TRUE;
    }

}
