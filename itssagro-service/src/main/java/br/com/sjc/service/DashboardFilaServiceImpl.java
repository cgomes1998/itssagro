package br.com.sjc.service;

import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.service.*;
import br.com.sjc.modelo.TipoFluxoEnum;
import br.com.sjc.modelo.*;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.cfg.DadosSincronizacao_;
import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.dto.*;
import br.com.sjc.modelo.enums.*;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.modelo.util.RomaneioUtil;
import br.com.sjc.persistencia.dao.RomaneioDAO;
import br.com.sjc.persistencia.dao.fachada.EtapaCriterioCustomRepository;
import br.com.sjc.persistencia.dao.fachada.RomaneioCustomRepository;
import br.com.sjc.util.*;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.hibernate.collection.internal.PersistentBag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import javax.persistence.NonUniqueResultException;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.script.*;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
@Service
public class DashboardFilaServiceImpl implements DashboardFilaService {

    private static final String BTN_CONFIRMACAO_NAO = "NAO";

    private static final String BTN_CONFIRMACAO_SIM = "SIM";

    @Autowired
    private RomaneioDAO romaneioDAO;

    @Autowired
    private RomaneioCustomRepository romaneioCustomRepository;

    @Autowired
    private SenhaService senhaService;

    @Autowired
    private SenhaEtapaService senhaEtapaService;

    @Autowired
    private FluxoEtapaService fluxoEtapaService;

    @Autowired
    private RomaneioService romaneioService;

    @Autowired
    private AutomacaoService automacaoService;

    @Autowired
    private ParametroService parametroService;

    @Autowired
    private ConversaoLitragemService conversaoLitragemService;

    @Autowired
    private PesagemService pesagemService;

    @Autowired
    private ClassificacaoService classificacaoService;

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private SAPService sapService;

    @Autowired
    private EventoAutomacaoGraosService eventoAutomacaoGraosService;

    @Autowired
    private EtapaCriterioCustomRepository etapaCriterioCustomRepository;

    @Autowired
    private AgendamentoCargaPontualService agendamentoCargaPontualService;

    @Autowired
    private LogAtividadeService logAtividadeService;

    @Autowired
    private CentroService centroService;
    
    @Value("${spring.profiles.active}")
    private String profileAtivo;

    /**
     * Obtém a filas de senhas com base nos filtros selecionados.
     *
     * @param filtro Filtro de consulta
     *
     * @return EtapaFilaDTO
     */
    @Override
    public List<EtapaFilaDTO> obterFilas(DashboardFilaFiltroDTO filtro) {

        int horasValidadeSenha = Integer.parseInt(this.parametroService.obterPorTipoParametro(TipoParametroEnum.QUANTIDADE_HORAS_VALIDADE_SENHA).getValor());

        Date data = Date.from(ZonedDateTime.now().minusHours(horasValidadeSenha).toInstant());

        Comparator<SenhaDTO> senhaComparator = this.obterSenhaComparator();

        List<FluxoEtapaDTO> fluxoEtapas = this.fluxoEtapaService.listarDtoPorFluxo(Collections.singletonList(filtro.getFluxo().getId()));

        List<SenhaDTO> senhas = this.senhaService.listarPorMaterialEPlacaENumeroRomaneioENumeroCartaoAcesso(ObjetoUtil.isNotNull(filtro.getMaterial()) ? Arrays.asList(filtro.getMaterial()) : null, filtro.getPlaca(),
                filtro.getNumeroRomaneio(), filtro.getNumeroCartaoAcesso(), data, fluxoEtapas, Arrays.asList(StatusSenhaEnum.CANCELADA, StatusSenhaEnum.PENDENTE));

        List<EtapaCriterioDTO> etapasFalha = this.etapaCriterioCustomRepository.obterQuantidadeRepeticoesDaEtapa(fluxoEtapas.stream().map(FluxoEtapaDTO::getId).collect(Collectors.toSet()));

        return fluxoEtapas.stream().map(fluxoEtapa -> this.obterEtapaFilaDTO(fluxoEtapa, senhas, senhaComparator, etapasFalha)).collect(Collectors.toList());
    }

    /**
     * Obtém a filas de senhas com base nos filtros selecionados para o relatório.
     *
     * @param filtro Filtro de consulta
     *
     * @return List<SenhaEtapaRelatorioDTO></SenhaEtapaRelatorioDTO>
     */
    @Override
    public List<SenhaEtapaRelatorioDTO> obterFilasRelatorio(RelatorioFilaFiltroDTO filtro) {

        Date data = filtro.getDataInicio();
        Date dataFim = filtro.getDataFim();

        List<FluxoEtapaDTO> fluxoEtapas = this.fluxoEtapaService.listarDtoPorFluxo(Collections.singletonList(filtro.getFluxo().getId()));

        List<SenhaEtapaRelatorioDTO> etapaSenhas = this.senhaEtapaService.obterSenhasEtapasPorFluxoEtapas(data, dataFim, fluxoEtapas);

        return etapaSenhas.stream().map(etapaSenha -> this.obterRomaneioEtapa(etapaSenha)).collect(Collectors.toList());
    }

    /**
     * Utilizado para obter a pesagem das balanças configuradas nos fluxos por meio de uma JOB que é executada a cada 10 segundos.
     */
    @Override
    public void executarEventosAutomacao() {

        VeiculoBalancaDTO pesoBalanca = this.automacaoService.obterPesoBalanca(1);

        if (ObjetoUtil.isNotNull(pesoBalanca) && (ObjetoUtil.isNotNull(pesoBalanca.getPeso1()) || ObjetoUtil.isNotNull(pesoBalanca.getPeso2())) && (StatusAutomacaoEnum.REALIZADO_PRIMEIRA_PESAGEM.equals(pesoBalanca.getStatus()) || StatusAutomacaoEnum.REALIZADO_SEGUNDA_PESAGEM.equals(pesoBalanca.getStatus()))) {

            executarRomaneioDoGestaoDePatio(pesoBalanca);

            executarRomaneiosDoItssAgro(pesoBalanca);
        }
    }

    private SenhaEtapaRelatorioDTO obterRomaneioEtapa(SenhaEtapaRelatorioDTO senhaEtapa){
        RomaneioDTO romaneioDTO = ObjetoUtil.isNotNull(senhaEtapa.getSenhaId()) ? this.romaneioDAO.getBySenhaId(senhaEtapa.getSenhaId()) : null;

        if (ObjetoUtil.isNotNull(romaneioDTO)) {
            senhaEtapa.setNumeroRomaneio(romaneioDTO.getNumeroRomaneio());

            if(StringUtil.isEmpty(senhaEtapa.getNomeCliente())){
                senhaEtapa.setNomeCliente(romaneioDTO.getCliente());
            }
        }

        return senhaEtapa;
    }

    private void executarRomaneiosDoItssAgro(VeiculoBalancaDTO pesoBalanca) {

        this.obterRomaneioPorNumeroCartaoAcessoEPlacaDoVeiculo(pesoBalanca.getNrCartao(), pesoBalanca.getPlacaVeiculo()).ifPresent(romaneio -> {

            if (!romaneio.getOperacao().isGestaoPatio()) {

                this.atribuirPesagemItssAgro(romaneio, pesoBalanca);
            }
        });
    }

    private Optional<Romaneio> obterRomaneioPorNumeroCartaoAcessoEPlacaDoVeiculo(Integer numeroCartaoDeAcesso, String placa1) {

        String placa = placa1.toUpperCase().replace("-", "");

        return this.romaneioDAO.findOne(((root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            Join<Romaneio, DadosSincronizacao> dadosSincronizacaoJoin = root.join(Romaneio_.operacao);

            Join<Romaneio, ItemNFPedido> itemNFPedidoJoin = root.join(Romaneio_.itens, JoinType.LEFT);

            Join<Romaneio, Veiculo> veiculoJoin = root.join(Romaneio_.placaCavalo, JoinType.LEFT);

            Join<ItemNFPedido, Veiculo> itemNFPedidoVeiculoJoin = itemNFPedidoJoin.join(ItemNFPedido_.placaCavalo, JoinType.LEFT);

            predicates.add(criteriaBuilder.or(criteriaBuilder.isFalse(dadosSincronizacaoJoin.get(DadosSincronizacao_.gestaoPatio)), criteriaBuilder.isNull(dadosSincronizacaoJoin.get(DadosSincronizacao_.gestaoPatio))));

            predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get(Romaneio_.statusRomaneio), StatusRomaneioEnum.EM_PROCESSAMENTO), criteriaBuilder.equal(root.get(Romaneio_.numeroCartaoAcesso), numeroCartaoDeAcesso),
                    criteriaBuilder.or(criteriaBuilder.equal(veiculoJoin.get(Veiculo_.placa1), placa), criteriaBuilder.equal(itemNFPedidoVeiculoJoin.get(Veiculo_.placa1), placa))));

            return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));
        }));
    }

    private void executarRomaneioDoGestaoDePatio(VeiculoBalancaDTO pesoBalanca) {

        this.senhaService.obterPorPlacaENumeroCartaoAcessoEEtapaContem(pesoBalanca.getPlacaVeiculo(), pesoBalanca.getNrCartao(), EtapaEnum.PESAGEM_TARA, EtapaEnum.PESAGEM_BRUTO).ifPresent(senha -> {

            Romaneio romaneioSenha = this.romaneioDAO.findFirstBySenhaId(senha.getId());

            if (romaneioSenha.getOperacao().isGestaoPatio()) {

                EtapaSenhaFilaDTO etapaSenhaFila = this.obterEtapaSenhaFilaAtual(new SenhaDTO(senha), romaneioSenha);

                this.atribuirPesagem(romaneioSenha, pesoBalanca);

                this.validarCondicaoSaidaEtapa(etapaSenhaFila);
            }
        });
    }

    /**
     * Método responsável por validar o evento realizado para o romaneio registrado no itss agro e executar a integração com a conceitto de acordo
     * com o que foi configurado no fluxo para a etapa atual
     *
     * @param romaneio
     */
    @Override
    public void validarEventosDaAutomacao(Romaneio romaneio, EtapaEnum etapaAtualDoRomaneioEnum, CriterioTipoEnum criterioTipoEnum) {

        if (this.profileAtivo.equalsIgnoreCase("prd")) {
    
            if (ObjetoUtil.isNotNull(romaneio) && !romaneio.getOperacao().isGestaoPatio() && ObjetoUtil.isNotNull(romaneio.getId()) && ObjetoUtil.isNotNull(romaneio.getNumeroCartaoAcesso())) {
        
                eventoAutomacaoGraosService.obterEventoASerExecutado(romaneio, etapaAtualDoRomaneioEnum, criterioTipoEnum).ifPresent(eventoAutomacaoGraos -> {
            
                    EventoAutomacaoGraosEtapa etapaAtual = eventoAutomacaoGraos.getEtapas().stream().filter(etapa -> etapa.getEtapa().equals(etapaAtualDoRomaneioEnum)).findFirst().orElse(null);
            
                    if (ObjetoUtil.isNotNull(etapaAtual)) {
                
                        this.executarEventosAutomacao(etapaAtual, romaneio, criterioTipoEnum);
                    }
                });
            }
        }
    }

    /**
     * Caso teha atendido a condição, enviar a senha para a próxima etapa e executa os eventos de automação caso exista.
     *
     * @param etapaSenhaFila Etapa fila atual da senha
     */
    @Override
    public void validarCondicaoSaidaEtapa(EtapaSenhaFilaDTO etapaSenhaFila) {

        this.verificarFluxoECriterios(etapaSenhaFila);

        FluxoEtapa fluxoEtapa = this.fluxoEtapaService.get(etapaSenhaFila.getFluxoEtapa().getId());

        Senha senha = this.senhaService.get(etapaSenhaFila.getSenha().getId());

        Romaneio romaneio = null;

        if (ObjetoUtil.isNotNull(etapaSenhaFila.getSenha())) {

            senha.setBotaoAcionadoAguardandoChamadaCarregamento(etapaSenhaFila.getSenha().getBotaoAcionadoAguardandoChamadaCarregamento());
        }

        if (ObjetoUtil.isNotNull(etapaSenhaFila.getRomaneio())) {

            romaneio = this.romaneioCustomRepository.get(etapaSenhaFila.getRomaneio().getId());

            this.montarDadosParaEtapaLocalCarregamento(etapaSenhaFila, romaneio);

            this.montarDadosParaEtapaConfirmacaoDePeso(etapaSenhaFila, romaneio);
        }

        FluxoEtapa proximaEtapaSucesso = this.obterProximaEtapaPorTipoCriterio(fluxoEtapa, senha, romaneio, CriterioTipoEnum.SUCESSO);

        FluxoEtapa proximaEtapaFalha = this.obterProximaEtapaPorTipoCriterio(fluxoEtapa, senha, romaneio, CriterioTipoEnum.FALHA);

        if (ObjetoUtil.isNotNull(proximaEtapaFalha)) {

            if (senha.getEtapaAtual().getQtdRepeticao() < etapaSenhaFila.getRepetirEtapa()) {

                // Caso não tenha atingido o limite de tentativas, realiza o incremento da mesma e permanece na mesma etapa.
                senha.getEtapas().stream().max(Comparator.comparing(SenhaEtapa::getHoraEntrada)).ifPresent(ultimaEtapa -> ultimaEtapa.setQtdRepeticao(ultimaEtapa.getQtdRepeticao() + 1));

                this.senhaService.salvar(senha);

            } else {

                // Caso tenha atingido o limite de tentativas, realiza o envio para a etapa de falha.
                this.senhaService.adicionarEtapa(senha, proximaEtapaFalha);

                this.executarAcoesEtapa(fluxoEtapa, senha, romaneio, CriterioTipoEnum.FALHA);

                this.executarEventosAutomacao(fluxoEtapa, senha, romaneio, CriterioTipoEnum.FALHA);
            }

        } else if (ObjetoUtil.isNotNull(proximaEtapaSucesso)) {

            this.senhaService.adicionarEtapa(senha, proximaEtapaSucesso);

            this.executarAcoesEtapa(fluxoEtapa, senha, romaneio, CriterioTipoEnum.SUCESSO);

            this.executarEventosAutomacao(fluxoEtapa, senha, romaneio, CriterioTipoEnum.SUCESSO);

            trocarStatusAgendamentoCargaPontual(fluxoEtapa, senha, false);

            if (ObjetoUtil.isNotNull(romaneio)) {

                romaneio.setPassouEtapaAguardandoChamada(EtapaEnum.AGUARDANDO_CHAMADA_CARREGAMENTO.equals(etapaSenhaFila.getFluxoEtapa().getEtapa()));

                if (romaneio.getPassouEtapaAguardandoChamada()) {

                    romaneio.setDataSaidaEtapaAguardandoChamada(DateUtil.hoje());

                    this.romaneioCustomRepository.alterarAtributos(romaneio, "passouEtapaAguardandoChamada", "dataSaidaEtapaAguardandoChamada");

                } else {

                    this.romaneioCustomRepository.alterarAtributos(romaneio, "passouEtapaAguardandoChamada");
                }
            }
        }
    }

    private void trocarStatusAgendamentoCargaPontual(FluxoEtapa fluxoEtapa, Senha senha, Boolean estorno) {

        if (ObjetoUtil.isNotNull(senha.getCargaPontualIdAgendamento())) {

            ItssAgroThreadPool.executarTarefa(() -> {

                try {

                    agendamentoCargaPontualService.trocaStatus(senha, fluxoEtapa, estorno);

                } catch (Exception e) {

                    e.printStackTrace();
                }
            });
        }
    }

    private void montarDadosParaEtapaLocalCarregamento(EtapaSenhaFilaDTO etapaSenhaFila, Romaneio romaneio) {

        if (EtapaEnum.EM_FILA_CARGA_DESCARGA.equals(etapaSenhaFila.getFluxoEtapa().getEtapa()) || EtapaEnum.AGUARDANDO_CHEGADA_AMOSTRA.equals(etapaSenhaFila.getFluxoEtapa().getEtapa()) || EtapaEnum.CARREGAMENTO.equals(etapaSenhaFila.getFluxoEtapa().getEtapa())) {

            romaneio.setLocalCarregamento(etapaSenhaFila.getRomaneio().getLocalCarregamento());

            romaneio.setAnalises(etapaSenhaFila.getRomaneio().getAnalises());
        }
    }

    private void montarDadosParaEtapaConfirmacaoDePeso(EtapaSenhaFilaDTO etapaSenhaFila, Romaneio romaneio) {

        if (EtapaEnum.CONFIRMACAO_PESO.equals(etapaSenhaFila.getFluxoEtapa().getEtapa()) && StringUtil.isNotNullEmpty(etapaSenhaFila.getRomaneio().getBotaoAcionadoConfirmacaoPeso())) {

            romaneio.setBotaoAcionadoConfirmacaoPeso(StringUtil.removerCaracteresEspeciais(etapaSenhaFila.getRomaneio().getBotaoAcionadoConfirmacaoPeso().toUpperCase()));

            this.setarRateioRealizadoNaConfirmacaoDePeso(etapaSenhaFila, romaneio);
        }
    }

    private void setarRateioRealizadoNaConfirmacaoDePeso(EtapaSenhaFilaDTO etapaSenhaFila, Romaneio romaneio) {

        if (EtapaEnum.CONFIRMACAO_PESO.equals(etapaSenhaFila.getFluxoEtapa().getEtapa()) && ColecaoUtil.isNotEmpty(romaneio.getItens()) && romaneio.getItens().size() > 0 && ObjetoUtil.isNotNull(etapaSenhaFila.getRomaneio())) {

            romaneio.getItens().forEach(itemNFPedido -> {

                ItemNFPedidoDTO itemNFPedidoDTO = etapaSenhaFila.getRomaneio().getItens().stream().filter(i -> i.getId().equals(itemNFPedido.getId())).findFirst().orElse(null);

                if (ObjetoUtil.isNotNull(itemNFPedidoDTO)) {

                    itemNFPedido.setRateio(itemNFPedidoDTO.getRateio());
                }
            });
        }
    }

    @Override
    public void estornar(EtapaSenhaFilaDTO etapaSenhaFila) {

        List<FluxoEtapa> fluxoEtapas = this.fluxoEtapaService.listarPorFluxo(etapaSenhaFila.getFluxoEtapa().getFluxo().getId());

        fluxoEtapas.sort(Comparator.comparing(FluxoEtapa::getSequencia));

        fluxoEtapas.stream().filter(fluxoEtapa -> fluxoEtapa.getEtapa().equals(etapaSenhaFila.getFluxoEtapa().getEtapa()) && fluxoEtapa.getSequencia().equals(etapaSenhaFila.getFluxoEtapa().getSequencia())).findFirst().ifPresent(fluxoEtapa -> {

            int index = fluxoEtapas.indexOf(fluxoEtapa);

            if (index > 0) {

                Senha senha = this.senhaService.get(etapaSenhaFila.getSenha().getId());

                this.senhaService.adicionarEtapa(senha, fluxoEtapas.get(index - 1));

                trocarStatusAgendamentoCargaPontual(fluxoEtapa, senha, true);
            }
        });
    }

    @Override
    public FluxoEtapa obterFluxoEtapaPorMaterialEEtapa(TipoFluxoEnum tipoFluxo, Material material, EtapaEnum etapaEnum) {

        return fluxoEtapaService.obterFluxoEtapaPorMaterialEEtapa(tipoFluxo, material, etapaEnum);
    }

    @Override
    public void atualizarDashboard() {

        ItssAgroThreadPool.executarTarefa(() -> template.convertAndSend("/senha/dashboard-filas", Boolean.TRUE));
    }

    private EtapaFilaDTO obterEtapaFilaDTO(FluxoEtapaDTO fluxoEtapa, List<SenhaDTO> senhas, Comparator<SenhaDTO> senhaComparator, List<EtapaCriterioDTO> etapasFalha) {

        EtapaFilaDTO etapaFila = new EtapaFilaDTO();

        etapaFila.setSequencia(fluxoEtapa.getSequencia());

        etapaFila.setEtapa(fluxoEtapa.getEtapa());

        if (!fluxoEtapa.isEtapaFinal()) {

            etapaFila.setSenhas(senhas.stream().sorted(senhaComparator).filter(senha -> fluxoEtapa.getEtapa().equals(senha.getEtapaAtual().getEtapa().getEtapa()) && fluxoEtapa.getSequencia().equals(senha.getEtapaAtual().getEtapa().getSequencia())).map(senha -> this.obterEtapaSenhaFila(senha, null, fluxoEtapa, etapasFalha)).filter(Objects::nonNull).collect(Collectors.toList()));
        }

        return etapaFila;
    }

    private Comparator<SenhaDTO> obterSenhaComparator() {

        Integer qdtMaximaChamada = Integer.parseInt(this.parametroService.obterPorTipoParametro(TipoParametroEnum.QUANTIDADE_DE_CHAMADAS_DE_SENHA).getValor());

        return Comparator.comparing((SenhaDTO s) -> s.getEtapaAtual().getQtdChamada() != null && s.getEtapaAtual().getQtdChamada().compareTo(qdtMaximaChamada) >= 0).thenComparing((SenhaDTO s) -> s.getEtapaAtual().getHoraEntrada());
    }

    private EtapaSenhaFilaDTO obterEtapaSenhaFila(SenhaDTO senha, Romaneio romaneio, FluxoEtapaDTO fluxoEtapa, List<EtapaCriterioDTO> etapasFalha) {

        RomaneioDTO romaneioDTO = ObjetoUtil.isNull(romaneio) && ObjetoUtil.isNotNull(senha) ? this.romaneioDAO.getBySenhaId(senha.getId()) : new RomaneioDTO(romaneio);

        if ((ObjetoUtil.isNotNull(romaneioDTO) && !Arrays.asList(StatusRomaneioEnum.ESTORNADO, StatusRomaneioEnum.PENDENTE).contains(romaneioDTO.getStatusRomaneio())) || Arrays.asList(EtapaEnum.DISTRIBUICAO_SENHA,
                EtapaEnum.CRIAR_ROMANEIO).contains(fluxoEtapa.getEtapa())) {

            EtapaSenhaFilaDTO etapaSenhaFila = new EtapaSenhaFilaDTO();

            etapaSenhaFila.setSenha(senha);

            etapaSenhaFila.setRomaneio(romaneioDTO);

            etapaSenhaFila.setFluxoEtapa(fluxoEtapa);

            if (ColecaoUtil.isNotEmpty(etapasFalha)) {

                etapaSenhaFila.setRepetirEtapa(etapasFalha.stream().filter(e -> e.getIdFluxoEtapa().equals(fluxoEtapa.getId())).mapToInt(EtapaCriterioDTO::getRepetirEtapa).findFirst().orElse(0));
            }

            return etapaSenhaFila;
        }
        return null;
    }

    /**
     * Gera a etapa fila atual da senha com base na etapa fila atual.
     *
     * @param senha    Senha
     * @param romaneio Romaneio vinculado a senha
     *
     * @return EtapaSenhaFilaDTO
     */
    private EtapaSenhaFilaDTO obterEtapaSenhaFilaAtual(SenhaDTO senha, Romaneio romaneio) {

        return this.obterEtapaSenhaFila(senha, romaneio, senha.getEtapaAtual().getEtapa(), null);
    }

    /**
     * Verifica se as informações para validação de condição de saída estão preenchidos.
     *
     * @param etapaSenhaFila Etapa fila atual da senha
     */
    private void verificarFluxoECriterios(EtapaSenhaFilaDTO etapaSenhaFila) {

        if (ObjetoUtil.isNull(etapaSenhaFila.getFluxoEtapa())) {
            throw new ServicoException("Nenhum fluxo configurado para o material e/ou etapa selecionada");
        }
    }

    /**
     * Percorre todos os criterios, verifica se atendeu as condições e retorna a etapa.
     *
     * @param fluxoEtapa Etapa fila atual da senha
     * @param senha      Etapa fila atual da senha
     * @param romaneio   Etapa fila atual da senha
     * @param tipo       Tipo do criterio
     *
     * @return EtapaEnum
     */
    private FluxoEtapa obterProximaEtapaPorTipoCriterio(FluxoEtapa fluxoEtapa, Senha senha, Romaneio romaneio, CriterioTipoEnum tipo) {

        return fluxoEtapa.getCriterios().stream().filter(criterio -> tipo.equals(criterio.getTipo())).sorted(Comparator.comparing(EtapaCriterio::getNumeroCriterio)).map(criterio -> {
            StringBuilder script = new StringBuilder();
            StringBuilder scriptDivisao = new StringBuilder();
            Bindings atributos = new SimpleBindings();
            criterio.getCondicoes().stream().sorted(Comparator.comparing(EtapaCriterioCondicao::getNumeroCondicao)).forEach(condicao -> this.montarCondicao(senha, romaneio, condicao, script, scriptDivisao, atributos));
            return ColecaoUtil.isEmpty(criterio.getCondicoes()) || this.atendeuAsCondicoes(script, scriptDivisao, atributos) ? criterio.getProximaEtapa() : null;
        }).filter(ObjetoUtil::isNotNull).findFirst().orElse(null);
    }

    /**
     * Monta o script com base no campo, na condição e no valor.
     * Adiciona o nome do campo no script.
     * Adiciona o valor do campo recebido via reflexão na lista de atributos.
     * As informações do campo e do valor são tratadas utilizando reflexão para serem comparadas conforme o tipo.
     *
     * @param senha         Etapa fila atual da senha
     * @param romaneio      Etapa fila atual da senha
     * @param condicao      Condição de saída da etapa
     * @param script        Expressão booleana
     * @param scriptDivisao Expressão booleana apenas para validações por divisão de carga
     * @param atributos     Atributos a serem substituídos na expressão booleana
     */
    private void montarCondicao(Senha senha, Romaneio romaneio, EtapaCriterioCondicao condicao, StringBuilder script, StringBuilder scriptDivisao, Bindings atributos) {

        try {
            Stream.of(this.obterObjeto(senha, romaneio, condicao.getEtapa())).filter(Objects::nonNull).map(objeto -> {
                if (objeto.getClass().isAssignableFrom(ArrayList.class)) {
                    return (List<?>) objeto;
                } else if (objeto.getClass().isAssignableFrom(PersistentBag.class)) {
                    return (PersistentBag) objeto;
                }
                return Collections.singletonList(objeto);
            }).forEach(objetos -> objetos.forEach(o -> this.montarCondicao(romaneio, condicao, script, scriptDivisao, atributos, o, objetos.indexOf(o) + 1 < objetos.size())));
        } catch (ServicoException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServicoException("Houve um problema ao validar as condições de saída da etapa");
        }
    }

    /**
     * Monta o script com base no campo, na condição e no valor.
     * Adiciona o nome do campo no script.
     * Adiciona o valor do campo recebido via reflexão na lista de atributos.
     * As informações do campo e do valor são tratadas utilizando reflexão para serem comparadas conforme o tipo.
     *
     * @param romaneio       Etapa fila atual da senha
     * @param condicao       Condição de saída da etapa
     * @param script         Expressão booleana
     * @param scriptDivisao  Expressão booleana apenas para validações por divisão de carga
     * @param atributos      Atributos a serem substituídos na expressão booleana
     * @param objeto         Objeto a ser utilizando para obter os atributos
     * @param maisDeUmObjeto Sinaliza que está percorrendo uma lista de objetos
     */
    private void montarCondicao(Romaneio romaneio, EtapaCriterioCondicao condicao, StringBuilder script, StringBuilder scriptDivisao, Bindings atributos, Object objeto, boolean maisDeUmObjeto) {

        try {
            if (ObjetoUtil.isNotNull(condicao.getOperadorEntrada())) {
                script.append(condicao.getOperadorEntrada().getOperador());
            }
            Field campo = FieldUtils.getDeclaredField(objeto.getClass(), condicao.getCampo().getCampo(), true);
            if (!Arrays.asList(EtapaCampoEnum.PESAGEM_PESO_LIQUIDO, EtapaCampoEnum.CONFIRMACAO_PESO_PESO_LIQUIDO).contains(condicao.getCampo())) {
                script.append(condicao.getCampo().name());
                script.append(condicao.getCondicao().getOperador());
                script.append(condicao.getCampo().name()).append("_VALOR");
                if (ObjetoUtil.isNotNull(condicao.getOperadorSaida())) {
                    script.append(condicao.getOperadorSaida().getOperador());
                }
            }
            if (EtapaCampoEnum.PESAGEM_BRUTO_PESO_TIPO_VEICULO.equals(condicao.getCampo())) {
                atributos.put(condicao.getCampo().name(), romaneio.getPesagem().getPesoInicial());
                atributos.put(condicao.getCampo().name() + "_VALOR", romaneio.getPesagem().getPlacaCavalo().getTipo().getPeso());
            } else if (EtapaCampoEnum.PESAGEM_BRUTO_CONVERSAO.equals(condicao.getCampo())) {
                Parametro parametro = this.parametroService.obterPorTipoParametro(TipoParametroEnum.TOLERANCIA_NA_PESAGEM_BRUTO_ETANOL);
                atributos.put(condicao.getCampo().name(), this.conversaoLitragemService.obterDiferencaLitros(romaneio));
                atributos.put(condicao.getCampo().name() + "_VALOR", parametro == null ? BigDecimal.ZERO : new BigDecimal(parametro.getValor()).abs());
            } else if (Arrays.asList(EtapaCampoEnum.PESAGEM_PESO_LIQUIDO, EtapaCampoEnum.CONFIRMACAO_PESO_PESO_LIQUIDO).contains(condicao.getCampo())) {
                condicaoParaValidarSaldoDaOrdemDeVenda(romaneio, condicao, scriptDivisao);
            } else if (ObjetoUtil.isNotNull(campo)) {
                atributos.put(condicao.getCampo().name(), this.converterValor(campo.getType(), campo.get(objeto)));
                atributos.put(condicao.getCampo().name() + "_VALOR", this.converterValor(campo.getType(), condicao.getValor()));
            }
            if (maisDeUmObjeto && !Arrays.asList(EtapaCampoEnum.PESAGEM_PESO_LIQUIDO, EtapaCampoEnum.CONFIRMACAO_PESO_PESO_LIQUIDO).contains(condicao.getCampo())) {
                script.append(" && ");
            }
        } catch (Exception e) {

            e.printStackTrace();

            throw new ServicoException("Houve um problema ao validar as condições de saída da etapa: " + e.getMessage());
        }
    }

    private void condicaoParaValidarSaldoDaOrdemDeVenda(Romaneio romaneio, EtapaCriterioCondicao condicao, StringBuilder script) {

        Material material = romaneio.getItens().stream().map(ItemNFPedido::getMaterial).filter(Objects::nonNull).findFirst().get();
        String unidadeOrdem = romaneio.getItens().stream().map(ItemNFPedido::getUnidadeMedidaOrdem).filter(Objects::nonNull).findFirst().orElse(null);
        List<ConfiguracaoUnidadeMedidaMaterial> unidades = sapService.obterMedidasParaConversao(material);
        boolean possuiDivisao = romaneio.getItens().size() > 1;
        HashMap<String, AtomicLong> stringAtomicLongHashMap = possuiDivisao ? new HashMap<>() : RomaneioUtil.converterPesoLiquidoParaUnidadeMedidaDaOrdem(romaneio);
        BigDecimal pesoLiquidoConvertidoPelaUnidadeMaterial = possuiDivisao ? BigDecimal.ZERO : sapService.converterQuantidade(unidades, stringAtomicLongHashMap.keySet().stream().findFirst().get(),
                stringAtomicLongHashMap.get(stringAtomicLongHashMap.keySet().stream().findFirst().get()).doubleValue());
        Long indexItens = 0L;
        if (possuiDivisao) {
            script.append(" ( ");
        }
        for (ItemNFPedido itemNFPedido : romaneio.getItens()) {
            indexItens++;
            boolean ultimoItem = indexItens.intValue() == romaneio.getItens().size();
            if (possuiDivisao) {
                pesoLiquidoConvertidoPelaUnidadeMaterial = sapService.converterQuantidade(unidades, material.isRealizarConversaoLitragem() ? "L" : "KG", itemNFPedido.getRateio().doubleValue());
            }
            script.append(pesoLiquidoConvertidoPelaUnidadeMaterial);
            script.append(" " + condicao.getCondicao().getOperador() + " ");
            if (StringUtil.isNotNullEmpty(unidadeOrdem)) {
                BigDecimal totalSaldo = sapService.converterQuantidade(unidades, unidadeOrdem, ObjetoUtil.isNotNull(itemNFPedido.getSaldoOrdemVenda()) ? itemNFPedido.getSaldoOrdemVenda().doubleValue() : 0);
                script.append(totalSaldo);
            } else {
                script.append(pesoLiquidoConvertidoPelaUnidadeMaterial);
            }
            if (!ultimoItem) {
                script.append(" && ");
            }
        }
        if (possuiDivisao) {
            script.append(" ) ");
        }
    }

    /**
     * Obtém o objeto que deve ser instanciado via reflexão com base na etapa.
     *
     * @param senha    Etapa fila atual da senha
     * @param romaneio Etapa fila atual da senha
     * @param etapa    Etapa fila atual da senha
     *
     * @return Object
     */
    private Object obterObjeto(Senha senha, Romaneio romaneio, EtapaEnum etapa) {

        switch (etapa) {
            case CONFIRMACAO_PESO:
            case CRIAR_ROMANEIO:
            case CANCELAMENTO_ORDEM:
                return romaneio;
            case ANALISE_QUALIDADE:
                return romaneio.getAnalises();
            case PESAGEM_TARA:
            case PESAGEM_BRUTO:
                return romaneio.getPesagem();
            case LACRAGEM:
                return romaneio.getLacragem();
            case DESCARGA:
            case CONVERSAO_LITRAGEM:
                return romaneio.getConversaoLitragem();
            case CHECKLIST:
                return senha.getFormularioAtual();
            case EMITIR_DOCUMENTO:
                return romaneio.getEmissaoDocumento();
            case CONFERENCIA_GERAIS:
                return romaneio.getConferencia();
            case PENDENCIA_COMERCIAL:
                return romaneio.getPendencia();
            case CARREGAMENTO:
            case AGUARDANDO_CHEGADA_AMOSTRA:
            case EM_FILA_CARGA_DESCARGA:
                return romaneio.getLocalCarregamento();
            default:
                return senha;
        }
    }

    /**
     * Converte o valor da condição com base no tipo do campo.
     *
     * @param tipo   Tipo do campo
     * @param objeto Valor da condição
     *
     * @return Object
     *
     * @throws ParseException Excessão ao tentar converter o valor em um tipo desconhecido
     */
    private Object converterValor(Class<?> tipo, Object objeto) throws ParseException {

        if (objeto == null || objeto.toString().trim().equalsIgnoreCase("null")) {
            return null;
        } else if (Integer.class.isAssignableFrom(tipo)) {
            return Integer.valueOf(objeto.toString());
        } else if (Float.class.isAssignableFrom(tipo)) {
            return Float.valueOf(objeto.toString());
        } else if (Double.class.isAssignableFrom(tipo)) {
            return Double.valueOf(objeto.toString());
        } else if (BigDecimal.class.isAssignableFrom(tipo)) {
            return new BigDecimal(objeto.toString());
        } else if (Boolean.class.isAssignableFrom(tipo)) {
            return Boolean.valueOf(objeto.toString());
        } else if (Date.class.isAssignableFrom(tipo)) {
            return new SimpleDateFormat("dd/MM/YYYY").parse(objeto.toString());
        } else if (StringUtil.isNotNullEmpty(objeto.toString())) {
            return objeto.toString().trim().replaceAll(" ", "_").toUpperCase();
        }
        return objeto.toString();
    }

    /**
     * Compila as condições baseado no script e nos atributos e verifica se foi atendida.
     *
     * @param script        Expressão booleana
     * @param scriptDivisao Expressão booleana apenas para validações por divisão de carga
     * @param atributos     Atributos a serem substituídos na expressão booleana
     *
     * @return Boolean
     */
    private Boolean atendeuAsCondicoes(StringBuilder script, StringBuilder scriptDivisao, Bindings atributos) {

        try {
            if (atributos.isEmpty() && StringUtil.isEmpty(scriptDivisao.toString())) {
                return true;
            }
            if (StringUtil.isNotNullEmpty(scriptDivisao.toString())) {
                if (StringUtil.isNotNullEmpty(script.toString())) {
                    String operador = script.substring(script.toString().length() - 2, script.toString().length());
                    boolean existeOperador = Arrays.asList("&&", "||").contains(operador);
                    if (!existeOperador) {
                        script.append(" && ");
                    }
                }
                script.append(" " + scriptDivisao.toString());
            }
            ScriptEngineManager manager = new ScriptEngineManager();
            ScriptEngine engine = manager.getEngineByName("JavaScript");
            Object resultado = engine.eval(script.toString(), atributos);
            if (ObjetoUtil.isNotNull(resultado)) {
                return (Boolean) resultado;
            }
            return false;
        } catch (ScriptException e) {
            log.error(e);
            throw new ServicoException("Houve um problema ao validar as condições de saída da etapa");
        }
    }

    /**
     * Executa os eventos de automação da etapa para operações do itss agro
     *
     * @param eventoAutomacaoGraosEtapa Etapa atual
     * @param romaneio                  Romaneio a ser processado
     * @param tipo                      Tipo do criterio
     */
    private void executarEventosAutomacao(EventoAutomacaoGraosEtapa eventoAutomacaoGraosEtapa, Romaneio romaneio, CriterioTipoEnum tipo) {

        try {

            this.obterEventosPorTipoCriterio(eventoAutomacaoGraosEtapa, tipo).forEach(etapaCriterioEvento -> {

                switch (etapaCriterioEvento.getEvento()) {

                    case CONSULTA_AUTOMATIZA:

                        this.obterPesoBalanca(romaneio, etapaCriterioEvento);
                        break;

                    case PRECAD_AUTOMATIZA:

                        this.realizarPreCadastroVeiculo(romaneio);
                        break;

                    case TROCASTATUS_AUTOMATIZA:

                        this.alterarStatusPesagem(romaneio, etapaCriterioEvento);
                        break;
                }
            });

        } catch (ServicoException e) {

            throw e;

        } catch (Exception e) {

            log.error(e);

            throw new ServicoException("Houve um problema ao executar os eventos de automação");
        }
    }

    /**
     * Executa os eventos de automação da próxima etapa para operações do gestão de pátio
     *
     * @param fluxoEtapa Próxima etapa fila da senha
     * @param senha      Próxima etapa fila da senha
     * @param romaneio   Próxima etapa fila da senha
     * @param tipo       Tipo do criterio
     */
    private void executarEventosAutomacao(FluxoEtapa fluxoEtapa, Senha senha, Romaneio romaneio, CriterioTipoEnum tipo) {

        try {

            this.obterEventosPorTipoCriterio(fluxoEtapa, tipo).forEach(etapaCriterioEvento -> {

                switch (etapaCriterioEvento.getEvento()) {

                    case CONSULTA_AUTOMATIZA:

                        this.obterPesoBalanca(romaneio, etapaCriterioEvento);
                        break;

                    case PRECAD_AUTOMATIZA:

                        this.realizarPreCadastroVeiculo(senha, romaneio);
                        break;

                    case TROCASTATUS_AUTOMATIZA:

                        this.alterarStatusPesagem(senha, etapaCriterioEvento);
                        break;
                }
            });

        } catch (ServicoException e) {

            throw e;

        } catch (Exception e) {

            log.error(e);

            throw new ServicoException("Houve um problema ao executar os eventos de automação");
        }
    }

    /**
     * Obtém os eventos de automação por tipo de critério baseado na próxima etapa fila da senha.
     *
     * @param fluxoEtapa Fluxo atual
     * @param tipo       Tipo do criterio
     *
     * @return List
     */
    private List<EtapaCriterioEvento> obterEventosPorTipoCriterio(FluxoEtapa fluxoEtapa, CriterioTipoEnum tipo) {

        return fluxoEtapa.getCriterios().stream().filter(criterio -> tipo.equals(criterio.getTipo())).flatMap(etapaCriterio -> etapaCriterio.getEventos().stream()).sorted(Comparator.comparing(EtapaCriterioEvento::getNumeroEvento)).collect(Collectors.toList());
    }

    /**
     * Obtém os eventos de automação por tipo de critério baseado na etapa atual do romaneio processado no itss agro
     *
     * @param eventoAutomacaoGraosEtapa Fluxo atual
     * @param tipo                      Tipo do criterio
     *
     * @return List
     */
    private List<EtapaCriterioEvento> obterEventosPorTipoCriterio(EventoAutomacaoGraosEtapa eventoAutomacaoGraosEtapa, CriterioTipoEnum tipo) {

        return eventoAutomacaoGraosEtapa.getEventos().stream().filter(evento -> evento.getCriterioTipo().equals(tipo.name())).sorted(Comparator.comparing(EtapaCriterioEvento::getNumeroEvento)).collect(Collectors.toList());
    }

    private void obterPesoBalanca(Romaneio romaneio, EtapaCriterioEvento evento) {

        VeiculoBalancaDTO pesoBalanca = this.automacaoService.obterPesoBalanca(Integer.parseInt(evento.getHardware().getIdentificacao()));

        if (ObjetoUtil.isNotNull(pesoBalanca) && (ObjetoUtil.isNotNull(pesoBalanca.getPeso1()) || ObjetoUtil.isNotNull(pesoBalanca.getPeso2())) && (StatusAutomacaoEnum.REALIZADO_PRIMEIRA_PESAGEM.equals(pesoBalanca.getStatus()) || StatusAutomacaoEnum.REALIZADO_SEGUNDA_PESAGEM.equals(pesoBalanca.getStatus())) && ObjetoUtil.isNotNull(romaneio)) {

            this.atribuirPesagemItssAgro(romaneio, pesoBalanca);
        }
    }

    /**
     * Realizar pré-cadastro do veiculo na conceitto para processos do gestão de pátio
     *
     * @param senha
     * @param romaneio
     */
    private void realizarPreCadastroVeiculo(Senha senha, Romaneio romaneio) {

        if (ObjetoUtil.isNotNull(senha) && ObjetoUtil.isNotNull(romaneio)) {

            this.automacaoService.realizarPreCadastroVeiculo(senha.getNumeroCartaoAcesso(), senha.getVeiculo().getPlaca1(), senha.getVeiculo().getPlaca2(), senha.getTransportadora().getDescricao(), senha.getMotorista().getNome(), null,
                    null, null, senha.getMaterial().getDescricao().substring(0, 1), null, romaneio.getNumeroRomaneio());
        }
    }

    /**
     * Realizar pré-cadastro do veiculo na conceitto para processos do itss agro
     *
     * @param romaneio
     */
    private void realizarPreCadastroVeiculo(Romaneio romaneio) {

        if (ObjetoUtil.isNotNull(romaneio)) {

            Veiculo veiculo = romaneio.getVeiculoDoRomaneio();

            Transportadora transportadora = romaneio.getTransportadoraDoRomaneio();

            Material material = romaneio.getMaterialDoRomaneio();

            Motorista motorista = romaneio.getMotoristaDoRomaneio();

            this.automacaoService.realizarPreCadastroVeiculo(romaneio.getNumeroCartaoAcesso(), veiculo.getPlaca1(), veiculo.getPlaca2(), transportadora.getDescricao(), motorista.getNome(), null, null, null,
                    material.getDescricao().substring(0, 1), null, romaneio.getNumeroRomaneio());
        }
    }

    /**
     * Método responsável pora alterar o status da pesagem inserida na conceitto para processos do itss agro
     *
     * @param romaneio
     * @param evento
     */
    private void alterarStatusPesagem(Romaneio romaneio, EtapaCriterioEvento evento) {

        if (ObjetoUtil.isNotNull(romaneio) && ObjetoUtil.isNotNull(romaneio.getNumeroCartaoAcesso())) {

            this.automacaoService.alterarStatusPesagem(romaneio.getNumeroCartaoAcesso(), evento.getStatusAutomacao().getCodigo(), evento.getMensagemUm(), evento.getMensagemDois(), evento.getMensagemTres(), 0, null);
        }
    }

    /**
     * Método responsável pora alterar o status da pesagem inserida na conceitto para processos do gestão de pátio
     *
     * @param senha
     * @param evento
     */
    private void alterarStatusPesagem(Senha senha, EtapaCriterioEvento evento) {

        if (ObjetoUtil.isNotNull(senha) && ObjetoUtil.isNotNull(senha.getNumeroCartaoAcesso())) {

            this.automacaoService.alterarStatusPesagem(senha.getNumeroCartaoAcesso(), evento.getStatusAutomacao().getCodigo(), evento.getMensagemUm(), evento.getMensagemDois(), evento.getMensagemTres(), 0, null);
        }
    }

    @Override
    public void alterarStatusPesagem(Senha senha, FluxoEtapa fluxoEtapa) {

        if (ObjetoUtil.isNotNull(senha.getNumeroCartaoAcesso())) {

            this.automacaoService.alterarStatusPesagem(senha.getNumeroCartaoAcesso(), fluxoEtapa.getStatusEstornoAutomacao(), this.obterMensagem(fluxoEtapa.getMensagemEstornoAutomacao(), 0, 12),
                    this.obterMensagem(fluxoEtapa.getMensagemEstornoAutomacao(), 12, 24), this.obterMensagem(fluxoEtapa.getMensagemEstornoAutomacao(), 24, 36), 0, null);
        }
    }

    private String obterMensagem(String mensagem, int begin, int end) {

        return StringUtil.isNotNullEmpty(mensagem) && mensagem.length() > begin ? mensagem.substring(begin, Math.min(mensagem.length(), end)) : null;
    }

    private void atribuirPesagemItssAgro(Romaneio romaneio, VeiculoBalancaDTO pesoBalanca) {

        obterPesagemDoRomaneioParaEventosDaAutomacao(romaneio);

        boolean statusEDaPesagem = false;

        EtapaEnum etapaRomaneioItssAgro = null;

        switch (pesoBalanca.getStatus()) {

            case REALIZADO_PRIMEIRA_PESAGEM: {

                statusEDaPesagem = true;

                etapaRomaneioItssAgro = EtapaEnum.PESAGEM_BRUTO;

                romaneio.getPesagem().setPesoInicial(pesoBalanca.getPeso1());

                break;
            }

            case REALIZADO_SEGUNDA_PESAGEM: {

                statusEDaPesagem = true;

                etapaRomaneioItssAgro = EtapaEnum.PESAGEM_TARA;

                romaneio.getPesagem().setPesoFinal(pesoBalanca.getPeso1());

                break;
            }

            default:

                statusEDaPesagem = false;

                break;
        }

        if (statusEDaPesagem) {

            if (BigDecimalUtil.isMaiorQueZero(romaneio.getPesagem().getPesoInicial()) && BigDecimalUtil.isMaiorQueZero(romaneio.getPesagem().getPesoFinal())) {

                calcularDescontosDeClassificacao(romaneio);

                romaneio.setClassificacao(this.classificacaoService.salvar(romaneio.getClassificacao()));
            }

            romaneio.setPesagem(this.pesagemService.salvar(romaneio.getPesagem()));

            salvarRomaneio(romaneio);

            this.validarEventosDaAutomacao(romaneio, etapaRomaneioItssAgro, CriterioTipoEnum.SUCESSO);
        }
    }

    private void calcularDescontosDeClassificacao(Romaneio romaneio) {

        if (ObjetoUtil.isNotNull(romaneio.getClassificacao())) {

            if (ColecaoUtil.isNotEmpty(romaneio.getClassificacao().getItens())) {

                BigDecimal pesoLiquidoUmido = romaneio.getPesagem().getPesoInicial().subtract(romaneio.getPesagem().getPesoFinal());

                AtomicInteger index = new AtomicInteger(0);

                romaneio.getClassificacao().getItens().forEach(itemClassificacao -> {

                    if (index.get() == 0) {

                        itemClassificacao.setDesconto((itemClassificacao.getPercentualDescontoValido().multiply(pesoLiquidoUmido)).divide(BigDecimalUtil.CEM, 2, RoundingMode.HALF_UP));

                        itemClassificacao.setDescontoClassificacao(pesoLiquidoUmido.subtract(itemClassificacao.getDescontoValido()));

                    } else {

                        ItensClassificacao primeiroItem = romaneio.getClassificacao().getItens().stream().findFirst().get();

                        itemClassificacao.setDesconto(itemClassificacao.getPercentualDescontoValido().multiply(primeiroItem.getDescontoClassificacaoValido()).divide(BigDecimalUtil.CEM, 2, RoundingMode.HALF_UP));

                        itemClassificacao.setDescontoClassificacao(primeiroItem.getDescontoClassificacaoValido().subtract(itemClassificacao.getDescontoValido()));
                    }

                    index.incrementAndGet();
                });
            }
        }
    }

    private void atribuirPesagem(Romaneio romaneio, VeiculoBalancaDTO pesoBalanca) {

        obterPesagemDoRomaneioParaEventosDaAutomacao(romaneio);

        if (EtapaEnum.PESAGEM_TARA.equals(romaneio.getSenha().getEtapaAtual().getEtapa().getEtapa())) {

            romaneio.getPesagem().setPesoFinal(pesoBalanca.getPeso1());

        } else if (EtapaEnum.PESAGEM_BRUTO.equals(romaneio.getSenha().getEtapaAtual().getEtapa().getEtapa())) {

            romaneio.getPesagem().setPesoInicial(pesoBalanca.getPeso1());
        }

        romaneio.setPesagem(this.pesagemService.salvar(romaneio.getPesagem()));

        salvarRomaneio(romaneio);
    }

    private void obterPesagemDoRomaneioParaEventosDaAutomacao(Romaneio romaneio) {

        Long idDaPesagemPorRomaneio = this.romaneioCustomRepository.obterIdDaPesagemPorRomaneio(romaneio.getId());

        if (ObjetoUtil.isNotNull(idDaPesagemPorRomaneio)) {

            romaneio.setPesagem(this.pesagemService.get(idDaPesagemPorRomaneio));
        }

        if (ObjetoUtil.isNull(romaneio.getPesagem())) {
            romaneio.setPesagem(new Pesagem());
        }

        romaneio.getPesagem().setPlacaCavalo(romaneio.getPlacaCavalo());

        romaneio.getPesagem().setSafra(romaneio.getSafra());
    }

    private void executarAcoesEtapa(FluxoEtapa fluxoEtapa, Senha senha, Romaneio romaneio, CriterioTipoEnum tipo) {

        try {
            AtomicBoolean executado = new AtomicBoolean(false);
            fluxoEtapa.getCriterios().stream().filter(criterio -> tipo.equals(criterio.getTipo())).flatMap(etapaCriterio -> etapaCriterio.getAcoes().stream()).forEach(acao -> Stream.of(this.obterObjeto(senha, romaneio, acao.getEtapa())).filter(Objects::nonNull).map(objeto -> {
                if (objeto.getClass().isAssignableFrom(ArrayList.class)) {
                    return (List<?>) objeto;
                } else if (objeto.getClass().isAssignableFrom(PersistentBag.class)) {
                    return (PersistentBag) objeto;
                }
                return Collections.singletonList(objeto);
            }).forEach(objetos -> objetos.forEach(o -> this.alterarValorCampo(o, acao))));
            if (executado.get() || (ObjetoUtil.isNotNull(senha) && !senha.isNew())) {
                this.senhaService.salvar(senha);
            }
            if (executado.get() || EtapaEnum.AGUARDANDO_CHEGADA_AMOSTRA.equals(fluxoEtapa.getEtapa()) || EtapaEnum.EM_FILA_CARGA_DESCARGA.equals(fluxoEtapa.getEtapa()) || EtapaEnum.CARREGAMENTO.equals(fluxoEtapa.getEtapa()) || (EtapaEnum.CONFIRMACAO_PESO.equals(fluxoEtapa.getEtapa()) && CriterioTipoEnum.SUCESSO.equals(tipo) && BTN_CONFIRMACAO_SIM.equals(romaneio.getBotaoAcionadoConfirmacaoPeso())) && ObjetoUtil.isNotNull(romaneio) && !romaneio.isNew()) {
                salvarNovoRomaneio(romaneio);
                if (ObjetoUtil.isNotNull(romaneio.getPesagem()) && !romaneio.getPesagem().isNew()) {
                    romaneio.getPesagem().setNumeroRomaneio(romaneio.getNumeroRomaneio());
                    this.pesagemService.salvar(romaneio.getPesagem());
                }
                if (EtapaEnum.CONFIRMACAO_PESO.equals(fluxoEtapa.getEtapa()) && CriterioTipoEnum.SUCESSO.equals(tipo) && BTN_CONFIRMACAO_SIM.equals(romaneio.getBotaoAcionadoConfirmacaoPeso())) {
                    this.sapService.enviarRomaneio(romaneio, false);
                }
            }
        } catch (Exception e) {
            log.error(e);
        }
    }

    private void alterarValorCampo(Object objeto, EtapaCriterioAcao acao) {

        try {
            Field campo = FieldUtils.getDeclaredField(objeto.getClass(), acao.getCampo().getCampo(), true);
            Object valor = Enum.class.isAssignableFrom(campo.getType()) ? Enum.valueOf((Class<Enum>) campo.getType(), acao.getValor().toUpperCase()) : this.converterValor(campo.getType(), acao.getValor());
            FieldUtils.writeField(campo, objeto, valor, true);
        } catch (Exception e) {
            log.error(e);
        }
    }

    private void salvarNovoRomaneio(Romaneio entidade) {

        try {

            if (ObjetoUtil.isNull(entidade.getId())) {

                entidade.setNumeroRomaneio(this.gerarNumeroRomaneio());

                Parametro parametro = this.parametroService.obterPorTipoParametro(TipoParametroEnum.CENTRO_SAP);

                if (ObjetoUtil.isNotNull(parametro)) {

                    entidade.setCentro(this.centroService.obterPorCodigo(parametro.getValor()));
                }
            }

            salvarRomaneio(entidade);

        } catch (NonUniqueResultException nonUniqueResultException) {

            throw new ServicoException("O romaneio já foi criado e está em processamento, aguarde um momento.");
        }
    }

    private String gerarNumeroRomaneio() {

        final Parametro parametro = this.parametroService.obterPorTipoParametro(TipoParametroEnum.CENTRO_SAP);

        String codigo = this.obterUltimoNumeroRomaneio();

        final String centro = parametro.getValor();

        final Integer numeroSequencia = Integer.parseInt(codigo) + 1;

        final int totalCaracter = centro.length() + String.valueOf(numeroSequencia).length();

        final int totalFaltando = 10 - totalCaracter;

        return centro + StringUtil.completarZeroAEsquerda(totalFaltando + String.valueOf(numeroSequencia).length(), String.valueOf(numeroSequencia));
    }

    private String obterUltimoNumeroRomaneio() {

        return this.romaneioDAO.getMaxId().toString();
    }

    private void salvarRomaneio(Romaneio entidade) {

        Boolean cadastro = Boolean.FALSE;

        Usuario usuarioLogado = entidade.getUsuarioLogado();

        if (entidade.getId() == null) {

            cadastro = Boolean.TRUE;

            entidade.setDataCadastro(DateUtil.hoje());

        } else {

            entidade.setDataAlteracao(DateUtil.hoje());
        }

        entidade = this.romaneioDAO.save(entidade);

        if (ObjetoUtil.isNotNull(usuarioLogado)) {

            entidade.setUsuarioLogado(usuarioLogado);
        }

        registrarLogAtividadeRomaneio(entidade, cadastro);
    }

    private <E> void registrarLogAtividadeRomaneio(E entidade, Boolean... cadastro) {

        EntidadeGenerica entidadeGenerica = (EntidadeGenerica) entidade;

        registrarLogAtividadeRomaneio(entidade, ObjetoUtil.isNotNull(cadastro) && cadastro[0] ? LogAtividadeAcao.CADASTRO : LogAtividadeAcao.getLogAtividadeAcao(entidadeGenerica));
    }

    private <E> void registrarLogAtividadeRomaneio(E entidade, LogAtividadeAcao logAtividadeAcao) {

        EntidadeGenerica entidadeGenerica = (EntidadeGenerica) entidade;

        if (ObjetoUtil.isNotNull(entidadeGenerica.getUsuarioLogado())) {

            LogAtividade logAtividade =
                    LogAtividade.logAtividadeBuilder().entidadeTipo(EntidadeTipo.getEntidadeTipo(entidade)).logAtividadeAcao(logAtividadeAcao).usuario(entidadeGenerica.getUsuarioLogado()).entidadeGenerica(entidadeGenerica).build();

            if (ObjetoUtil.isNotNull(logAtividade.getEntidadeTipo()) && ObjetoUtil.isNotNull(logAtividade.getEntidadeId())) {

                logAtividadeService.getDAO().save(logAtividade);
            }
        }
    }
}
