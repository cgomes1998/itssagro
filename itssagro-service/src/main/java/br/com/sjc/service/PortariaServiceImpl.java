package br.com.sjc.service;

import br.com.sjc.fachada.service.PortariaService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.cfg.Portaria;
import br.com.sjc.modelo.dto.PortariaDTO;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import br.com.sjc.persistencia.dao.PortariaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PortariaServiceImpl extends ServicoGenerico<Long, Portaria> implements PortariaService {
    @Autowired
    private PortariaDAO dao;

    @Override
    public Class<PortariaDTO> getDtoListagem() {
        return PortariaDTO.class;
    }

    @Override
    public PortariaDAO getDAO() {
        return this.dao;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PortariaDTO> listarPortariasAtivas() {
        List<PortariaDTO> portarias = dao.listarAtivas().stream().map(PortariaDTO::new).collect(Collectors.toList());
        return portarias;
    }

}
