package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.service.RomaneioLogEnvioService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.RomaneioLogEnvio;
import br.com.sjc.persistencia.dao.RomaneioLogEnvioDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class RomaneioLogEnvioServiceImpl extends ServicoGenerico<Long, RomaneioLogEnvio> implements RomaneioLogEnvioService {

    @Autowired
    private RomaneioLogEnvioDAO dao;

    @Override
    @Transactional
    public RomaneioLogEnvio obterPorRomaneio(final Romaneio romaneio) {

        return this.dao.findByRomaneioId(romaneio.getId());
    }

    @Override
    @Transactional
    public RomaneioLogEnvio obterPorRomaneioId(final Long id) {

        return this.dao.findByRomaneioId(id);
    }

    @Override
    public DAO<Long, RomaneioLogEnvio> getDAO() {

        return this.dao;
    }

    @Override
    public Class<? extends DataDTO> getDtoListagem() {

        return null;
    }
}
