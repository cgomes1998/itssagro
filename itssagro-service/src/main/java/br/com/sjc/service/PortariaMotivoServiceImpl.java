package br.com.sjc.service;

import br.com.sjc.fachada.service.PortariaMotivoService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.cfg.PortariaMotivo;
import br.com.sjc.modelo.dto.PortariaDTO;
import br.com.sjc.modelo.dto.PortariaMotivoDTO;
import br.com.sjc.persistencia.dao.PortariaMotivoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PortariaMotivoServiceImpl extends ServicoGenerico<Long, PortariaMotivo> implements PortariaMotivoService {
    @Autowired
    private PortariaMotivoDAO dao;

    @Override
    public Class<PortariaMotivoDTO> getDtoListagem() {
        return PortariaMotivoDTO.class;
    }

    @Override
    public PortariaMotivoDAO getDAO() {
        return this.dao;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PortariaMotivoDTO> listarMotivosAtivos() {
        List<PortariaMotivoDTO> motivos = dao.listarAtivos().stream().map(PortariaMotivoDTO::new).collect(Collectors.toList());
        return motivos;
    }
}
