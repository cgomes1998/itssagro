package br.com.sjc.service;

import br.com.sjc.fachada.service.ContratoService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.sap.CondicaoPagamento;
import br.com.sjc.modelo.sap.GrupoCompradores;
import br.com.sjc.modelo.sap.OrganizacaoCompra;
import br.com.sjc.modelo.sap.TipoContrato;
import br.com.sjc.persistencia.dao.CondicaoPagamentoDAO;
import br.com.sjc.persistencia.dao.GrupoCompradoresDAO;
import br.com.sjc.persistencia.dao.OrganizacaoCompraDAO;
import br.com.sjc.persistencia.dao.TipoContratoDAO;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContratoServiceImpl extends ServicoGenerico<Long, TipoContrato> implements ContratoService {

    @Autowired
    private TipoContratoDAO dao;

    @Autowired
    private CondicaoPagamentoDAO condicaoPagamentoDAO;

    @Autowired
    private GrupoCompradoresDAO grupoCompradoresDAO;

    @Autowired
    private OrganizacaoCompraDAO organizacaoCompraDAO;

    @Override
    public List<TipoContrato> listarTipoContratos() {
        return this.getDAO().findByOrderByCodigoAsc();
    }

    @Override
    public List<CondicaoPagamento> listarCondicoesPagamento() {
        return this.getCondicaoPagamentoDAO().findByOrderByCodigoAsc();
    }

    @Override
    public List<GrupoCompradores> listarGrupoCompradores() {
        return this.getGrupoCompradoresDAO().findByOrderByCodigoAsc();
    }

    @Override
    public List<OrganizacaoCompra> listarOrganizacaoCompras() {
        return this.getOrganizacaoCompraDAO().findByOrderByCodigoAsc();
    }

    @Override
    public void salvarTipoContrato(final List<TipoContrato> entidades) {

        if (ColecaoUtil.isNotEmpty(entidades)) {

            entidades.stream().forEach(entidade -> {

                if (StringUtil.isNotNullEmpty(entidade.getCodigo())) {

                    entidade.setId(this.getDAO().findIdByCodigo(entidade.getCodigo()));

                    this.getDAO().save(entidade);
                }
            });
        }
    }

    @Override
    public void salvarCondicaoPagamento(final List<CondicaoPagamento> entidades) {

        if (ColecaoUtil.isNotEmpty(entidades)) {

            entidades.stream().forEach(entidade -> {

                if (StringUtil.isNotNullEmpty(entidade.getCodigo())) {

                    entidade.setId(this.getCondicaoPagamentoDAO().findIdByCodigo(entidade.getCodigo()));

                    this.getCondicaoPagamentoDAO().save(entidade);
                }
            });
        }
    }

    @Override
    public void salvarGrupoCompradores(final List<GrupoCompradores> entidades) {

        if (ColecaoUtil.isNotEmpty(entidades)) {

            entidades.stream().forEach(entidade -> {

                if (StringUtil.isNotNullEmpty(entidade.getCodigo())) {

                    entidade.setId(this.getGrupoCompradoresDAO().findIdByCodigo(entidade.getCodigo()));

                    this.getGrupoCompradoresDAO().save(entidade);
                }
            });
        }
    }

    @Override
    public void salvarOrganizacaoCompras(final List<OrganizacaoCompra> entidades) {

        if (ColecaoUtil.isNotEmpty(entidades)) {

            entidades.stream().forEach(entidade -> {

                if (StringUtil.isNotNullEmpty(entidade.getCodigo())) {

                    entidade.setId(this.getOrganizacaoCompraDAO().findIdByCodigo(entidade.getCodigo()));

                    this.getOrganizacaoCompraDAO().save(entidade);
                }
            });
        }
    }

    @Override
    public TipoContratoDAO getDAO() {
        return this.dao;
    }

    public CondicaoPagamentoDAO getCondicaoPagamentoDAO() {
        return this.condicaoPagamentoDAO;
    }

    public GrupoCompradoresDAO getGrupoCompradoresDAO() {
        return this.grupoCompradoresDAO;
    }

    public OrganizacaoCompraDAO getOrganizacaoCompraDAO() {
        return this.organizacaoCompraDAO;
    }
}
