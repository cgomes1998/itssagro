package br.com.sjc.service;

import br.com.sjc.fachada.service.*;
import br.com.sjc.fachada.service.cargaPontual.AgendamentoService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.*;
import br.com.sjc.modelo.TipoFluxoEnum;
import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.dto.AgendamentoCargaPontualDTO;
import br.com.sjc.modelo.dto.OrdemVendaDTO;
import br.com.sjc.modelo.dto.RetornoNFERomaneioSenhaListagemDTO;
import br.com.sjc.modelo.enums.*;
import br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento.*;
import br.com.sjc.modelo.pojo.cargaPontual.agendamento.trocaStatus.Body;
import br.com.sjc.modelo.pojo.cargaPontual.autenticacao.RetornoGerarToken;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.modelo.sap.Motorista;
import br.com.sjc.persistencia.dao.AgendamentoCargaPontualDAO;
import br.com.sjc.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.*;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class AgendamentoCargaPontualServiceImpl extends ServicoGenerico<Long, AgendamentoCargaPontual> implements AgendamentoCargaPontualService {

    @Autowired
    private AgendamentoCargaPontualDAO dao;

    @Autowired
    private AgendamentoService agendamentoService;

    @Autowired
    private AutenticacaoCargaPontualService autenticacaoCargaPontualService;

    @Autowired
    private SenhaService senhaService;

    @Autowired
    private SenhaEtapaService senhaEtapaService;

    @Autowired
    private MotoristaService motoristaService;

    @Autowired
    private TransportadoraService transportadoraService;

    @Autowired
    private VeiculoService veiculoService;

    @Autowired
    private TipoVeiculoService tipoVeiculoService;

    @Autowired
    private ParametroService parametroService;

    @Autowired
    private SAPService sapService;

    @Autowired
    private RetornoNFeService retornoNFeService;

    @Autowired
    private RecebimentoNfeSapCargaPontualService recebimentoNfeSapCargaPontualService;

    @Autowired
    private Environment environment;

    @Override
    public AgendamentoCargaPontualDAO getDAO() {

        return this.dao;
    }

    @Override
    public Class<AgendamentoCargaPontualDTO> getDtoListagem() {

        return AgendamentoCargaPontualDTO.class;
    }

//    @Scheduled(cron = "0/15 * * * * *") // 15 segundos
    @Scheduled(cron = "0 0/5 * * * *") // 5 minutos
    @Transactional
    public void trocaStatusRecebimentoNfe() {

        Collection<RetornoNFERomaneioSenhaListagemDTO> notas = retornoNFeService.dtoListar(((root, criteriaQuery, criteriaBuilder) -> {

            Join<RetornoNFe, Romaneio> romaneioJoin = root.join(RetornoNFe_.romaneio);

            Join<Romaneio, Senha> senhaJoin = romaneioJoin.join(Romaneio_.senha);

            Date hojeInicioDia = DateUtil.obterDataHoraZerada(new Date());

            Date hojeFimDia = DateUtil.converterDateFimDia(new Date());

            Subquery<Long> subQuery = criteriaQuery.subquery(Long.class);

            Root<RecebimentoNfeSapCargaPontual> recebimentoNfeSapCargaPontualRoot = subQuery.from(RecebimentoNfeSapCargaPontual.class);

            subQuery.where(criteriaBuilder.and(
                    criteriaBuilder.isNull(recebimentoNfeSapCargaPontualRoot.get(RecebimentoNfeSapCargaPontual_.exception)),
                    criteriaBuilder.between(recebimentoNfeSapCargaPontualRoot.get(RecebimentoNfeSapCargaPontual_.dataCadastro), hojeInicioDia, hojeFimDia)
            ));

            subQuery.select(recebimentoNfeSapCargaPontualRoot.join(RecebimentoNfeSapCargaPontual_.senha).get(Senha_.id));

            List<Predicate> predicates = new ArrayList<>();

            predicates.add(criteriaBuilder.between(root.get(RetornoNFe_.dataCadastro), hojeInicioDia, hojeFimDia));

            predicates.add(criteriaBuilder.isNotNull(senhaJoin.get(Senha_.cargaPontualIdAgendamento)));

            predicates.add(criteriaBuilder.not(senhaJoin.get(Senha_.id).in(subQuery)));

            return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));

        }), RetornoNFERomaneioSenhaListagemDTO.class);

        if (ColecaoUtil.isNotEmpty(notas)) {

            Parametro parametro = parametroService.obterPorTipoParametro(TipoParametroEnum.STATUS_AGENDAMENTO_RECEBIMENTO_NFE);

            FluxoEtapa fluxoEtapa = new FluxoEtapa();

            fluxoEtapa.setStatusCargaPontualDestinoSucesso(Integer.valueOf(parametro.getValor()));

            notas.parallelStream().forEach(nota -> {

                RecebimentoNfeSapCargaPontual recebimentoNfeSapCargaPontual = null;

                try {

                    recebimentoNfeSapCargaPontual = recebimentoNfeSapCargaPontualService.salvar(RecebimentoNfeSapCargaPontual.builder().senha(nota.getRomaneio().getSenha()).build());

                    trocaStatus(nota.getRomaneio().getSenha(), fluxoEtapa, false);

                } catch (Exception e) {

                    recebimentoNfeSapCargaPontual.setException(StringUtil.getPrintStackTrace(e));

                    recebimentoNfeSapCargaPontualService.alterarAtributos(recebimentoNfeSapCargaPontual, RecebimentoNfeSapCargaPontual_.EXCEPTION);
                }
            });
        }
    }

    @Transactional
    @Override
    public void trocaStatusEntradaMotorista(Senha senha) throws Exception {

        Parametro parametro = parametroService.obterPorTipoParametro(TipoParametroEnum.STATUS_AGENDAMENTO_ENTRADA_MOTORISTA);

        FluxoEtapa fluxoEtapa = new FluxoEtapa();

        fluxoEtapa.setStatusCargaPontualDestinoSucesso(Integer.valueOf(parametro.getValor()));

        trocaStatus(senha, fluxoEtapa, false);

        senha.setStatusSenha(StatusSenhaEnum.AGUARDANDO_CHAMADA);
        senha.setDataChegadaMotorista(DateUtil.hoje());
        senhaService.alterarAtributos(senha, Senha_.STATUS_SENHA, Senha_.DATA_CHEGADA_MOTORISTA);

        SenhaEtapa etapaAtual = senha.getEtapaAtual();
        etapaAtual.setStatusEtapa(senha.getStatusSenha().toString());
        senhaEtapaService.alterarAtributos(etapaAtual, SenhaEtapa_.STATUS_ETAPA);
    }

    @Override
    public void trocaStatus(Senha senha, FluxoEtapa fluxoEtapa, Boolean estorno) throws Exception {

        autenticacaoCargaPontualService.gerarTokenAutenticacao();

        RetornoGerarToken retornoGerarToken = autenticacaoCargaPontualService.getRetornoGerarToken();

        br.com.sjc.modelo.pojo.cargaPontual.agendamento.trocaStatus.Retorno retorno = agendamentoService.trocaStatus(retornoGerarToken.getRetorno().getConteudo().getToken(),
                Body.builder().senha(senha).fluxoEtapa(fluxoEtapa).estorno(estorno).build());

        lancarExceptionErroIntegracao(retorno.getRetorno().getStatus(), "Erro ao trocar status do agendamento %s", senha.getCargaPontualIdAgendamento());
    }

    @Override
    @Transactional
    public void gerarSenhaCargaPontual(Date... dataFiltro) {

        try {

            autenticacaoCargaPontualService.gerarTokenAutenticacao();

            RetornoGerarToken retornoGerarToken = autenticacaoCargaPontualService.getRetornoGerarToken();

            StringBuilder stringBuilderFiltros = getFiltros(dataFiltro);

            Retorno retornoConsultaAgendamento = agendamentoService.consultarAgendamento(retornoGerarToken.getRetorno().getConteudo().getToken(), stringBuilderFiltros.toString());

            lancarExceptionErroIntegracao(retornoConsultaAgendamento.getRetorno().getStatus(), "Erro ao consultar agendamento no carga pontual\nFiltros: %s\nStatus: %s\nMensagem: %s", stringBuilderFiltros.toString(), String.valueOf(retornoConsultaAgendamento.getRetorno().getStatus()), retornoConsultaAgendamento.getRetorno().getMensagem());

            if (ObjetoUtil.isNotNull(retornoConsultaAgendamento.getRetorno().getConteudo()) && ColecaoUtil.isNotEmpty(retornoConsultaAgendamento.getRetorno().getConteudo().getConteudo())) {

                Collection<AgendamentoCargaPontualDTO> agendamentosSalvos = dtoListar(((root, criteriaQuery, criteriaBuilder) -> {

                    List<Integer> idAgendamentos = retornoConsultaAgendamento.getRetorno().getConteudo().getConteudo().stream().map(Conteudo2::getIdAgendamento).collect(Collectors.toList());

                    return criteriaBuilder.and(
                            root.get(AgendamentoCargaPontual_.idAgendamento).in(idAgendamentos),
                            criteriaBuilder.isNull(root.get(AgendamentoCargaPontual_.exception))
                    );

                }), AgendamentoCargaPontualDTO.class);

                if (ColecaoUtil.isNotEmpty(agendamentosSalvos)) {

                    retornoConsultaAgendamento.getRetorno().getConteudo().getConteudo().removeIf(conteudo2 -> {

                        Optional<AgendamentoCargaPontualDTO> optional = agendamentosSalvos.stream().filter(agendamentoSalvo -> agendamentoSalvo.getIdAgendamento() == conteudo2.getIdAgendamento()).findFirst();

                        if (optional.isPresent()) {

                            boolean existeSenhaVinculada = senhaService.existe(((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(
                                    criteriaBuilder.equal(root.get(Senha_.cargaPontualIdAgendamento), conteudo2.getIdAgendamento() + ""),
                                    root.get(Senha_.statusSenha).in(Arrays.asList(StatusSenhaEnum.PENDENTE, StatusSenhaEnum.AGUARDANDO_CHAMADA, StatusSenhaEnum.CHAMADA, StatusSenhaEnum.COMPLETA, StatusSenhaEnum.AGUARDANDO_ENTRADA))
                            )));

                            return existeSenhaVinculada;
                        }

                        return false;
                    });
                }

                if (ColecaoUtil.isNotEmpty(retornoConsultaAgendamento.getRetorno().getConteudo().getConteudo())) {

                    retornoConsultaAgendamento.getRetorno().getConteudo().getConteudo().stream().sorted(Comparator.comparing(Conteudo2::getDataAgendamentoInicialDate)).forEach(conteudo2 -> {

                        AgendamentoCargaPontual agendamentoCargaPontual = null;

                        try {

                            agendamentoCargaPontual = salvar(AgendamentoCargaPontual.builder().conteudo(conteudo2).build());

                            Senha senha = new Senha();

                            senha.setStatusSenha(StatusSenhaEnum.AGUARDANDO_ENTRADA);

                            senha.setTipoFluxo(conteudo2.getTipoOperacaoAgendamento().toUpperCase().trim().equals("CARGA") ? TipoFluxoEnum.EXPEDICAO : TipoFluxoEnum.RECEBIMENTO);

                            senha.setCargaPontualIdAgendamento(String.valueOf(conteudo2.getIdAgendamento()));

                            inicializarSenhaPeriodoAgendamentoCargaPontual(conteudo2, senha);

                            inicializarMotorista(conteudo2, senha);

                            inicializarTransportadora(conteudo2, senha);

                            inicializarVeiculo(conteudo2, senha);

                            inicializarDadosOrdemVenda(conteudo2, senha);

                            senhaService.gerar(senha);

                        } catch (Exception e) {

                            agendamentoCargaPontual.setException(StringUtil.getPrintStackTrace(e));

                            alterarAtributos(agendamentoCargaPontual, AgendamentoCargaPontual_.EXCEPTION);
                        }
                    });
                }

            } else {

                lancarExceptionErroIntegracao(404, "Não foi encontrado agendamentos para os filtros informados: %s", stringBuilderFiltros.toString());
            }

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    private void inicializarDadosOrdemVenda(Conteudo2 conteudo2, Senha senha) {

        conteudo2.getOrdens().stream().findFirst().ifPresent(orden -> {

            try {

                OrdemVendaDTO ordemVendaDTO = this.sapService.buscarOrdemVenda(orden.getNumeroOrigem());

                senha.setCodigoOrdemVenda(orden.getNumeroOrigem());

                senha.setSaldoOrdemVenda(ordemVendaDTO.getSaldo());

                senha.setOperadorLogistico(ordemVendaDTO.getOperadorLogistica());

                senha.setMaterial(ordemVendaDTO.getMaterial());

                senha.setCliente(ordemVendaDTO.getCliente());

            } catch (Exception e) {

                e.printStackTrace();
            }
        });

        inicializarSenhaQuantidadeCarregar(conteudo2, senha);
    }

    private void inicializarSenhaQuantidadeCarregar(Conteudo2 conteudo2, Senha senha) {

        if (ColecaoUtil.isNotEmpty(conteudo2.getOrdens())) {

            AtomicReference<BigDecimal> bigDecimalAtomicReference = new AtomicReference<>(BigDecimal.ZERO);

            conteudo2.getOrdens().stream().findFirst().ifPresent(orden -> {

                if (ObjetoUtil.isNotNull(orden.getProdutos()) && StringUtil.isNotNullEmpty(orden.getProdutos().getQuantidadeAgendada())) {

                    orden.getProdutos().setQuantidadeAgendada(orden.getProdutos().getQuantidadeAgendada().replace(".", ""));

                    bigDecimalAtomicReference.accumulateAndGet(BigDecimalUtil.stringParaBigDecimal2Decimal(orden.getProdutos().getQuantidadeAgendada()), BigDecimal::add);
                }
            });

            senha.setQuantidadeCarregar(bigDecimalAtomicReference.get());
        }
    }

    private StringBuilder getFiltros(Date... dataFiltro) {

        StringBuilder stringBuilderFiltros = new StringBuilder();

        stringBuilderFiltros.append("age_status = 1"); // Todos agendamentos ativos

        stringBuilderFiltros.append(" and "); // Operador de condição

        if (ObjetoUtil.isNotNull(dataFiltro) && dataFiltro.length > 0) {

            stringBuilderFiltros.append(String.format("age_data = '%s'", DateUtil.format("yyyyMMdd", dataFiltro[0]))); // Data do agendamento

        } else {

            stringBuilderFiltros.append(String.format("age_data = '%s'", DateUtil.format("yyyyMMdd", new Date()))); // Data do agendamento
        }

        stringBuilderFiltros.append(" and "); // Operador de condição

        Parametro parametroCentroLogado = parametroService.obterPorTipoParametro(TipoParametroEnum.CENTRO_SAP);

        stringBuilderFiltros.append("pla_id = " + (parametroCentroLogado.getValor().equals("1302") ? environment.getProperty("cargaPontual.servico.plantaCodigoUsf") : environment.getProperty("cargaPontual.servico.plantaCodigoUrd")));

        return stringBuilderFiltros;
    }

    private void inicializarVeiculo(Conteudo2 conteudo2, Senha senha) {

        if (ObjetoUtil.isNotNull(conteudo2.getVeiculos())) {

            Veiculo veiculo = veiculoService.get(((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(Veiculo_.placa1), conteudo2.getVeiculos().getPlacaCavalo().getPlaca().replace("-", ""))));

            if (ObjetoUtil.isNotNull(veiculo)) {

                TipoVeiculo tipoVeiculo = tipoVeiculoService.get(TipoVeiculo_.CODIGO, conteudo2.getVeiculos().getCodsistemaexternotipoveiculo());

                veiculo.setarValoresCargaPontual(conteudo2.getVeiculos(), tipoVeiculo);

                senha.setVeiculo(veiculoService.salvar(veiculo));

            } else {

                TipoVeiculo tipoVeiculo = tipoVeiculoService.get(TipoVeiculo_.CODIGO, conteudo2.getVeiculos().getCodsistemaexternotipoveiculo());

                senha.setVeiculo(veiculoService.salvar(Veiculo.builderCargaPontual().veiculos(conteudo2.getVeiculos()).tipoVeiculo(tipoVeiculo).build()));
            }
        }
    }

    private void inicializarTransportadora(Conteudo2 conteudo2, Senha senha) {

        if (StringUtil.isNotNullEmpty(conteudo2.getCnpjCpfTransportador())) {

            Transportadora transportadora = transportadoraService.get(((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.or(
                    criteriaBuilder.equal(root.get(Transportadora_.cnpj), conteudo2.getCnpjCpfTransportador()),
                    criteriaBuilder.equal(root.get(Transportadora_.cpf), conteudo2.getCnpjCpfTransportador())
            )));

            senha.setTransportadora(transportadora);
        }
    }

    private void inicializarMotorista(Conteudo2 conteudo2, Senha senha) {

        if (ObjetoUtil.isNotNull(conteudo2.getMotorista()) && StringUtil.isNotNullEmpty(conteudo2.getMotorista().getCpf())) {

            Motorista motorista = motoristaService.get(((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(Motorista_.cpf), conteudo2.getMotorista().getCpf())));

            if (ObjetoUtil.isNotNull(motorista)) {

                motorista.setarValoresCargaPontual(conteudo2.getMotorista());

                senha.setMotorista(motoristaService.salvar(motorista));

            } else {

                senha.setMotorista(motoristaService.salvar(Motorista.builderCargaPontual().motorista(conteudo2.getMotorista()).build()));
            }
        }
    }

    private void inicializarSenhaPeriodoAgendamentoCargaPontual(Conteudo2 conteudo2, Senha senha) {

        String[] splitPeriodoInicialFinal = conteudo2.getPeriodoInicialFinal().split(" - ");

        String[] splitPeriodoInicial = splitPeriodoInicialFinal[0].split(":");

        String[] splitPeriodoFinal = splitPeriodoInicialFinal[1].split(":");

        Date date = DateUtil.format(conteudo2.getDataAgendamento(), "yyyy-MM-dd");

        senha.setCargaPontualDataAgendamentoInicial(DateUtil.setarHora(date, Integer.valueOf(splitPeriodoInicial[0].trim()), Integer.valueOf(splitPeriodoInicial[1].trim())));

        senha.setCargaPontualDataAgendamentoFinal(DateUtil.setarHora(date, Integer.valueOf(splitPeriodoFinal[0].trim()), Integer.valueOf(splitPeriodoFinal[1].trim())));
    }

    private void lancarExceptionErroIntegracao(int status, String mensagem, String... params) throws Exception {

        if (status != 200) {

            throw new Exception(String.format(mensagem, params));
        }
    }
}
