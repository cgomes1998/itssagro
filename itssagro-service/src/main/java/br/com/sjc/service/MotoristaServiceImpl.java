package br.com.sjc.service;

import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.MotoristaService;
import br.com.sjc.fachada.service.SAPService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.dto.MotoristaDTO;
import br.com.sjc.modelo.endpoint.request.item.MotoristaRequest;
import br.com.sjc.modelo.endpoint.response.item.MotoristaResponse;
import br.com.sjc.modelo.enums.*;
import br.com.sjc.modelo.sap.Motorista;
import br.com.sjc.modelo.sap.request.EventOutRequest;
import br.com.sjc.modelo.sap.response.item.RfcMotoristaResponseItem;
import br.com.sjc.modelo.sap.sync.SAPMotorista;
import br.com.sjc.persistencia.dao.MotoristaDAO;
import br.com.sjc.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;

@Service
public class MotoristaServiceImpl extends ServicoGenerico<Long, Motorista> implements MotoristaService {

    private static final Logger LOG = Logger.getLogger(MotoristaServiceImpl.class.getName());

    @Autowired
    private MotoristaDAO dao;

    @Autowired
    private SAPService sapService;

    @Override
    public Specification<Motorista> obterEspecification(final Paginacao<Motorista> paginacao) {

        final Motorista entidade = paginacao.getEntidade();

        final Specification<Motorista> specification = new Specification<Motorista>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<Motorista> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                if (StringUtil.isNotNullEmpty(entidade.getNome())) {
                    predicates.add(builder.and(builder.like(builder.upper(root.get("nome")), "%" + entidade.getNome().toUpperCase() + "%")));
                }

                if (StringUtil.isNotNullEmpty(entidade.getCpf())) {
                    predicates.add(builder.equal(root.get("cpf"), StringUtil.removerCaracteresEspeciais(entidade.getCpf())));
                }

                if (ObjetoUtil.isNotNull(entidade.getStatus())) {
                    predicates.add(builder.equal(root.get("status"), entidade.getStatus()));
                }

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Override
    public List<MotoristaResponse> listarParaArmazemTerceiros(final List<MotoristaRequest> requests) {

        if (ColecaoUtil.isNotEmpty(requests)) {

            AtomicBoolean teveNovosCadastros = new AtomicBoolean(false);

            List<MotoristaResponse> response = new ArrayList<>();

            requests.stream().forEach(item -> {

                Motorista motorista = this.getDAO().findTop1ByCpfOrCnh(item.getCpf(), item.getCnh());

                if (ObjetoUtil.isNotNull(motorista)) {

                    response.add(motorista.toResponse());

                } else {

                    motorista = new Motorista();

                    motorista.setNome(item.getNome());

                    motorista.setCnh(item.getCnh());

                    motorista.setCpf(item.getCpf());

                    this.getDAO().save(motorista);

                    teveNovosCadastros.set(true);
                }
            });

            if (teveNovosCadastros.get()) {

                new Thread() {

                    @Override
                    public void run() {

                        sapService.enviarCadastro(DadosSincronizacaoEnum.MOTORISTA, null);
                    }

                }.start();
            }

            return response;
        }

        return null;
    }

    @Override
    public Motorista salvar(Motorista entidade) {

        Motorista entidadeSaved = super.salvar(entidade);

        ItssAgroThreadPool.executarTarefa(() -> this.sapService.enviarCadastro(DadosSincronizacaoEnum.MOTORISTA, null));

        return entidadeSaved;
    }

    @Override
    public void salvarSAP(final EventOutRequest request, final String host, String mandante) {

        if (ColecaoUtil.isNotEmpty(request.getData())) {

            final Map<String, SAPMotorista> groupMotorista = this.groupMotorista(request);

            for (final String itemGrupo : groupMotorista.keySet()) {

                final SAPMotorista motorista = groupMotorista.get(itemGrupo);

                Motorista entidadeSaved = this.dao.findByIdOrCpf(motorista.getIdMotorista(), motorista.getCpf());

                entidadeSaved = motorista.copy(entidadeSaved);

                entidadeSaved.setStatusRegistro(motorista.getStatusRegistroSAP());

                LOG.info(StringUtil.log("MOTORISTA - " + entidadeSaved.getCpf()));

                LOG.info(StringUtil.isNotNullEmpty(entidadeSaved.getCodigo()) ? StringUtil.log("OK - SINCRONIZACAO DE MOTORISTA COM CODIGO") : StringUtil.log("SINCRONIZACAO DE MOTORISTA NAO VEIO CODIGO"));

                if (ColecaoUtil.isNotEmpty(request.getLogRetorno())) {

                    AtomicBoolean success = new AtomicBoolean(true);

                    AtomicBoolean validar = new AtomicBoolean(true);

                    final StringBuilder sb = new StringBuilder();

                    request.getLogRetorno().forEach(registro -> {

                        sb.append(registro.getMessage());

                        sb.append("\n");

                        if (registro.getCodMessage().equals("E") || registro.getCodMessage().equals("A") && validar.get()) {

                            success.set(false);

                            validar.set(false);
                        }
                    });

                    if (success.get()) {

                        entidadeSaved.setStatusCadastroSap(StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO);

                    } else {

                        entidadeSaved.setStatusCadastroSap(StatusCadastroSAPEnum.RETORNO_COM_ERRO);
                    }

                    entidadeSaved.setLogSAP(StringUtil.isEmpty(sb.toString()) ? null : sb.toString());
                }

                if (StringUtil.isNotNullEmpty(entidadeSaved.getCodigo())) {

                    this.getDAO().save(entidadeSaved);
                }
            }

        } else if (ObjetoUtil.isNotNull(request.getStatusSAP()) && !StatusIntegracaoSAPEnum.COMPLETO.getCodigo().toString().equals(request.getStatusSAP())) {

            Motorista motorista = this.dao.findByRequestKey(request.getRequestKey());

            if (ObjetoUtil.isNotNull(motorista) && !motorista.getStatus().equals(StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO)) {

                motorista.setStatusCadastroSap(StatusCadastroSAPEnum.RETORNO_COM_ERRO);

                if (ColecaoUtil.isNotEmpty(request.getLogRetorno())) {

                    request.getLogRetorno().forEach(registro -> {

                        motorista.setLogSAP(registro.getCodMessage() + " - " + registro.getMessage());
                    });
                }

                this.getDAO().save(motorista);
            }
        }
    }

    private Map<String, SAPMotorista> groupMotorista(final EventOutRequest request) {

        final Map<String, SAPMotorista> groupMotorista = new HashMap<String, SAPMotorista>();

        request.getData().forEach(registro -> {

            try {

                SAPMotorista entidade = groupMotorista.get(registro.getItemDocumento().trim());

                if (ObjetoUtil.isNull(entidade)) {

                    entidade = SAPMotorista.newInstance();
                }

                entidade.update(registro.getCampo().trim(), registro.getValor().trim());

                groupMotorista.put(registro.getItemDocumento().trim(), entidade);

            } catch (final Exception e) {

                MotoristaServiceImpl.LOG.info("ERRO: " + e.getMessage());
            }
        });

        return groupMotorista;
    }

    @Override
    public void salvarSAP(final List<RfcMotoristaResponseItem> retorno) {

        if (ColecaoUtil.isNotEmpty(retorno)) {

            retorno.stream().forEach(item -> {

                try {

                    Motorista entidadeSaved = this.getDAO().findByCpfOrCodigo(item.getCpf(), item.getCodigo());

                    entidadeSaved = item.copy(entidadeSaved);

                    entidadeSaved.setStatusRegistro(item.getStatusRegistroSAP());

                    AtomicBoolean success = new AtomicBoolean(true);

                    final StringBuilder sb = new StringBuilder();

                    if (success.get()) {

                        entidadeSaved.setStatusCadastroSap(StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO);

                    } else {

                        entidadeSaved.setStatusCadastroSap(StatusCadastroSAPEnum.RETORNO_COM_ERRO);
                    }

                    entidadeSaved.setLogSAP("MOTORISTA SINCRONIZADO COM O SAP EM " + DateUtil.format("dd/MM/yyyy HH:mm:ss", DateUtil.hoje()));

                    if (ObjetoUtil.isNull(entidadeSaved.getCodigo()) || StringUtil.isEmpty(entidadeSaved.getCodigo())) {

                        success.set(false);

                        sb.append("\nO MOTORISTA CHEGOU SEM O CODIGO");
                    }

                    if (StringUtil.isNotNullEmpty(entidadeSaved.getCodigo())) {

                        entidadeSaved.setLogSAP(entidadeSaved.getLogSAP() + "\nCÓDIGO: " + entidadeSaved.getCodigo());
                    }

                    entidadeSaved.setLogSAP(StringUtil.isEmpty(sb.toString()) ? null : sb.toString());

                    this.getDAO().save(entidadeSaved);

                } catch (Exception e) {

                    MotoristaServiceImpl.LOG.info(MessageFormat.format("ERRO AO PERSISTIR MOTORISTA {0}, ERRO {1}", item.getCpf().concat(" - ").concat(item.getNome()), e.getMessage()));
                }
            });
        }
    }

    @Override
    public List<Motorista> listarPreCadastro() {

        return this.getDAO().findByStatusCadastroSapIn(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.NAO_ENVIADO, StatusCadastroSAPEnum.RETORNO_COM_ERRO}));
    }

    @Override
    public List<Motorista> listarSincronizados(Date data) {

        return ObjetoUtil.isNull(data) ? this.getDAO().findByStatusCadastroSapIn(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO})) :
                this.getDAO().findByStatusCadastroSapInAndDataCadastroGreaterThanEqual(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO}), data);
    }

    @Override
    public Motorista obterPorCodigo(final String codigo) {

        return this.getDAO().findByCodigo(codigo);
    }

    @Override
    public void preSalvar(Motorista entidade) {

        entidade.setCpf(StringUtil.removerCaracteresEspeciais(entidade.getCpf()));

        if (StringUtil.isNotNullEmpty(entidade.getCelular())) {

            entidade.setCelular(StringUtil.removerCaracteresEspeciais(entidade.getCelular()));
        }

        if (StringUtil.isNotNullEmpty(entidade.getTelefone())) {

            entidade.setTelefone(StringUtil.removerCaracteresEspeciais(entidade.getTelefone()));
        }

        this.motoristaJaExiste(entidade);

        this.transformarToUpperCase(entidade);

        entidade.setStatusCadastroSap(StatusCadastroSAPEnum.NAO_ENVIADO);

        removerVeiculoDeOutrosMotoristas(entidade);
    }

    private void removerVeiculoDeOutrosMotoristas(Motorista motorista) {

        if (!Objects.isNull(motorista.getVeiculo())) {
            dao.removerVeiculoDeOutrosMotoristas(motorista.getVeiculo().getId(), motorista.getId());
        }
    }

    @Override
    public void transformarToUpperCase(Motorista entidade) {

        entidade.setNome(StringUtil.isNotNullEmpty(entidade.getNome()) ? entidade.getNome().toUpperCase() : null);

        entidade.setRua(StringUtil.isNotNullEmpty(entidade.getRua()) ? entidade.getRua().toUpperCase() : null);

        entidade.setBairro(StringUtil.isNotNullEmpty(entidade.getBairro()) ? entidade.getBairro().toUpperCase() : null);
    }

    private void motoristaJaExiste(final Motorista entidade) {

        final Boolean isCadastro = entidade.getId() == null;

        if ((isCadastro && dao.existsByCpf(entidade.getCpf())) || (!isCadastro && dao.existsByCpfAndNeId(entidade.getCpf(), entidade.getId()))) {

            throw new ServicoException(MessageFormat.format("Já existe um motorista com o cpf {0}", entidade.getCpf()));
        }
    }

    @Override
    public List<Motorista> buscarMotoristasAutoComplete(final String value) {

        List<Motorista> motoristas = this.dao.findTop10ByNomeIgnoreCaseContainingOrCodigoIgnoreCaseContainingOrCpfIgnoreCaseContainingOrCnhIgnoreCaseContainingOrderByCodigoAsc(value, value, value, value);
        motoristas.removeIf(motorista -> StatusEnum.INATIVO.equals(motorista.getStatus()));
        return motoristas;
    }

    @Override
    public Motorista getMorotistaPorVeiculo(Long veiculoId) {

        return dao.findFirstByVeiculoIdAndStatus(veiculoId, StatusEnum.ATIVO);
    }

    @Override
    @Transactional
    public void alterarStatus(Motorista entidade) {

        super.alterarStatus(entidade);

        registrarLogAtividade(entidade, StatusEnum.ATIVO.equals(entidade.getStatus()) ? LogAtividadeAcao.ATIVAR : LogAtividadeAcao.INATIVAR);
    }

    @Override
    public Class<MotoristaDTO> getDtoListagem() {

        return MotoristaDTO.class;
    }

    @Override
    public MotoristaDAO getDAO() {

        return dao;
    }

    @Override
    public Boolean getRegistrarLogAtividade() {

        return Boolean.TRUE;
    }

}
