package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.sap.TabelaClassificacaoHeader;
import br.com.sjc.persistencia.dao.fachada.TabelaClassificacaoCustomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class TabelaClassificacaoHeaderService extends ServicoGenerico<String, TabelaClassificacaoHeader> {

    @Override
    public DAO<String, TabelaClassificacaoHeader> getDAO() {

        return null;
    }

}
