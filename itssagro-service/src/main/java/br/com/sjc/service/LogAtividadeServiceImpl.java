package br.com.sjc.service;

import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.LogAtividadeService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.LogAtividade;
import br.com.sjc.modelo.LogAtividade_;
import br.com.sjc.modelo.Usuario;
import br.com.sjc.modelo.dto.AmbienteDTO;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.dto.LogAtividadeDTO;
import br.com.sjc.modelo.enums.EntidadeTipo;
import br.com.sjc.persistencia.dao.LogAtividadeDAO;
import br.com.sjc.persistencia.dao.UsuarioDAO;
import br.com.sjc.util.ColecaoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class LogAtividadeServiceImpl extends ServicoGenerico<Long, LogAtividade> implements LogAtividadeService {

    @Autowired
    private LogAtividadeDAO dao;

    @Autowired
    private UsuarioDAO usuarioDAO;

    @Override
    public LogAtividadeDAO getDAO() {

        return this.dao;
    }

    @Override
    public Class<? extends DataDTO> getDtoListagem() {

        return null;
    }

    @Override
    public List<LogAtividadeDTO> getLogAtividades(EntidadeTipo entidadeTipo, Long entidadeId) {

        List<LogAtividade> logAtividades = dao.findAll(((root, criteriaQuery, criteriaBuilder) ->{

            criteriaQuery.orderBy(criteriaBuilder.desc(root.get(LogAtividade_.dataCadastro)));

            return  criteriaBuilder.and(
                    criteriaBuilder.equal(root.get(LogAtividade_.entidadeTipo), entidadeTipo),
                    criteriaBuilder.equal(root.get(LogAtividade_.entidadeId), entidadeId)
            );
        }
       ));

        List<LogAtividadeDTO> logAtividadesDto = new ArrayList<>();

        if (ColecaoUtil.isNotEmpty(logAtividades)) {

            logAtividadesDto = logAtividades.stream().map(logAtividade -> {

                String usuarioAutor = usuarioDAO.findNomeById(logAtividade.getUsuarioAutor());

                return LogAtividadeDTO.builder()
                        .dataCadastro(logAtividade.getDataCadastro())
                        .usuarioAutor(usuarioAutor)
                        .logAtividadeAcao(logAtividade.getLogAtividadeAcao())
                        .build();

            }).collect(Collectors.toList());
        }

        return logAtividadesDto;
    }

}
