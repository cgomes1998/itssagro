package br.com.sjc.service;

import br.com.sjc.fachada.service.EmpresaService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.cfg.Empresa;
import br.com.sjc.modelo.dto.BalancaDTO;
import br.com.sjc.modelo.dto.EmpresaDTO;
import br.com.sjc.persistencia.dao.EmpresaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmpresaServiceImpl extends ServicoGenerico<Long, Empresa> implements EmpresaService {
    @Autowired
    private EmpresaDAO dao;

    @Override
    public Class<EmpresaDTO> getDtoListagem() {
        return EmpresaDTO.class;
    }

    @Override
    public EmpresaDAO getDAO() {
        return this.dao;
    }
}
