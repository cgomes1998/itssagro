package br.com.sjc.service;

import br.com.sjc.fachada.service.LogRomaneioService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.LogRomaneio;
import br.com.sjc.modelo.LogRomaneioAcoes;
import br.com.sjc.modelo.Usuario;
import br.com.sjc.modelo.dto.LogRomaneioDTO;
import br.com.sjc.modelo.sap.LogRestricaoOrdem;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.persistencia.dao.LogRomaneioDAO;
import br.com.sjc.persistencia.dao.fachada.LogRomaneioCustomRepository;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LogRomaneioServiceImpl extends ServicoGenerico<Long, LogRomaneio> implements LogRomaneioService {

    @Autowired
    private LogRomaneioDAO dao;

    @Autowired
    private LogRomaneioCustomRepository logRomaneioCustomRepository;

    @Override
    public void salvar(Romaneio romaneio, Usuario usuarioToken) {

        LogRomaneio logRomaneio = dao.findByRomaneioId(romaneio.getId());

        if (ObjetoUtil.isNull(logRomaneio)) {

            logRomaneio = new LogRomaneio();

            logRomaneio.setRomaneio(romaneio);
        }

        LogRomaneioAcoes logRomaneioAcoes = new LogRomaneioAcoes();

        logRomaneioAcoes.setLogRomaneio(logRomaneio);

        logRomaneioAcoes.setResponsavel(usuarioToken);

        logRomaneioAcoes.setMotivo(romaneio.getMotivoLiberacao());

        logRomaneioAcoes.setRestricoes(new ArrayList<>());

        if (ColecaoUtil.isNotEmpty(romaneio.getRestricoes())) {

            logRomaneioAcoes.getRestricoes().addAll(romaneio.getRestricoes().stream().map(restricao -> new LogRestricaoOrdem(restricao.getMensagem(), restricao.getCodigoOrdemVenda(), restricao.getTipoRestricao(), logRomaneioAcoes)).collect(Collectors.toList()));
        }

        if (ColecaoUtil.isEmpty(logRomaneio.getLogs())) {

            logRomaneio.setLogs(new ArrayList<>());
        }

        logRomaneio.getLogs().add(logRomaneioAcoes);

        salvar(logRomaneio);
    }

    @Override
    public List<LogRomaneioDTO> obterLogPorRomaneio(final Long idRomaneio) {

        return logRomaneioCustomRepository.obterLogsDeLiberacaoPorRomaneio(idRomaneio);
    }

    @Override
    public Class<LogRomaneioDTO> getDtoListagem() {

        return LogRomaneioDTO.class;
    }

    @Override
    public LogRomaneioDAO getDAO() {

        return dao;
    }

}
