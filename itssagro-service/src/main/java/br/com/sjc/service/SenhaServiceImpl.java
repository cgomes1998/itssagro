package br.com.sjc.service;

import br.com.sjc.fachada.rest.paginacao.Pagina;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.*;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.*;
import br.com.sjc.modelo.cfg.Campo;
import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.dto.*;
import br.com.sjc.modelo.enums.*;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.modelo.sap.email.SolicitarLiberacaoSenhaEmail;
import br.com.sjc.persistencia.dao.SenhaDAO;
import br.com.sjc.persistencia.dao.fachada.SenhaCustomRepository;
import br.com.sjc.util.*;
import br.com.sjc.util.bundle.MessageSupport;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JRException;
import org.hibernate.criterion.MatchMode;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.*;
import java.io.InputStream;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Log4j2
@Service
public class SenhaServiceImpl extends ServicoGenerico<Long, Senha> implements SenhaService {

    private static final String REL_SENHA_JASPER = "jasper/rel_senha.jasper";

    private static final String LOGO_PNG = "img/logo.png";

    @Autowired
    private SenhaDAO dao;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private EmailSenderService emailSenderService;

    @Autowired
    private ParametroService parametroService;

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private VeiculoService veiculoService;

    @Autowired
    private CampoOperacaoService campoOperacaoService;

    @Autowired
    private SenhaCustomRepository senhaCustomRepository;

    @Autowired
    private DashboardFilaService dashboardFilaService;

    @Autowired
    private DashboardGerencialService dashboardGerencialService;

    @Autowired
    private FluxoEtapaService fluxoEtapaService;

    @Override
    public void gerar(Senha senha) {

        formatarSenha(senha);
        //Toda nova senha é criada com o status AGUARDANDO_CHAMADA como padrão
        if (ObjetoUtil.isNull(senha.getStatusSenha())) {
            senha.setStatusSenha(StatusSenhaEnum.AGUARDANDO_CHAMADA);
        }
        //É realizado a persistência do veícula caso seja um novo registro e tenha a placa informada
        if (ObjetoUtil.isNotNull(senha.getVeiculo()) && senha.getVeiculo().isNew() && StringUtil.isNotNullEmpty(senha.getVeiculo().getPlaca1())) {

            if (StringUtil.isNotNullEmpty(senha.getVeiculo().getPlaca1())) {

                senha.getVeiculo().setPlaca1(StringUtil.removerCaracteresEspeciais(senha.getVeiculo().getPlaca1()).toUpperCase());
            }

            if (StringUtil.isNotNullEmpty(senha.getVeiculo().getPlaca2())) {

                senha.getVeiculo().setPlaca2(StringUtil.removerCaracteresEspeciais(senha.getVeiculo().getPlaca2()).toUpperCase());
            }

            if (StringUtil.isNotNullEmpty(senha.getVeiculo().getPlaca3())) {

                senha.getVeiculo().setPlaca3(StringUtil.removerCaracteresEspeciais(senha.getVeiculo().getPlaca3()).toUpperCase());
            }

            boolean veiculoInativo = veiculoService.existe(((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(
                    criteriaBuilder.equal(root.get(Veiculo_.placa1), senha.getVeiculo().getPlaca1()),
                    criteriaBuilder.equal(root.get(Veiculo_.status), StatusEnum.INATIVO)
            )));

            if (veiculoInativo) {

                throw new ServiceException("Veiculo com casdastro inativo. Favor verificar!");
            }

            senha.getVeiculo().setStatus(StatusEnum.ATIVO);

            senha.getVeiculo().setUsuarioLogado(senha.getUsuarioLogado());

            ItssAgroThreadPool.executarTarefa(() -> veiculoService.salvar(senha.getVeiculo()));
        }

        this.salvar(senha);
    }

    @Override
    public void preSalvar(Senha senha) {

        super.preSalvar(senha);

        if (!ObjetoUtil.isFieldsChanged(senha.getMotorista())) {
            senha.setMotorista(null);
        }
        if (!ObjetoUtil.isFieldsChanged(senha.getVeiculo())) {
            senha.setVeiculo(null);
        }
        if (!ObjetoUtil.isFieldsChanged(senha.getCliente())) {
            senha.setCliente(null);
        }
        if (!ObjetoUtil.isFieldsChanged(senha.getTransportadora())) {
            senha.setTransportadora(null);
        }
        if (!ObjetoUtil.isFieldsChanged(senha.getSeta())) {
            senha.setSeta(null);
        }

        this.validarDadosObrigatorios(senha);

        this.adicionarNovaEtapa(senha);

        if (ColecaoUtil.isNotEmpty(senha.getEtapas())) {

            senha.getEtapas().forEach(senhaEtapa -> senhaEtapa.setSenha(senha));
        }

        if (ObjetoUtil.isNotNull(senha.getFormularios())) {

            senha.getFormularios().forEach(senhaFormulario -> senhaFormulario.setSenha(senha));
        }
    }

    private void validarDadosObrigatorios(Senha entidade) throws ServiceException {

        if (Arrays.asList(new StatusSenhaEnum[]{StatusSenhaEnum.AGUARDANDO_ENTRADA, StatusSenhaEnum.AGUARDANDO_CHAMADA, StatusSenhaEnum.PENDENTE, StatusSenhaEnum.CANCELADA}).contains(entidade.getStatusSenha())) {
            return;
        }

        final List<CampoOperacaoDTO> campoOperacaos = campoOperacaoService.obterCamposPorOperacao(DadosSincronizacaoEnum.SENHA);

        if (ColecaoUtil.isNotEmpty(campoOperacaos) && campoOperacaos.stream().anyMatch(CampoOperacaoDTO::isObrigatorio)) {

            List<String> variaveis = campoOperacaos.stream().filter(CampoOperacaoDTO::isObrigatorio).map(CampoOperacaoDTO::getCampo).map(Campo::getDescricao).collect(Collectors.toList());

            for (String variavel : variaveis) {

                Object valor = UtilReflection.getValor(variavel, entidade);

                if (ObjetoUtil.isNull(valor)) {

                    final String label = campoOperacaos.stream().filter(i -> i.getCampo().getDescricao().equals(variavel)).map(CampoOperacaoDTO::getDisplay).findFirst().orElse(StringUtil.empty());

                    throw new ServiceException(MessageSupport.getMessage("MSGE026", label));
                }
            }
        }
    }

    private void adicionarNovaEtapa(Senha senha) {

        if (ColecaoUtil.isEmpty(senha.getEtapas()) && (ObjetoUtil.isNull(senha.getCargaPontualIdAgendamento()) || (ObjetoUtil.isNotNull(senha.getCargaPontualIdAgendamento()) && StatusSenhaEnum.AGUARDANDO_ENTRADA.equals(senha.getStatusSenha())))) {

            FluxoEtapa fluxoEtapa = this.fluxoEtapaService.obterFluxoEtapaPorMaterialEEtapa(senha.getTipoFluxo(), senha.getMaterial(), EtapaEnum.DISTRIBUICAO_SENHA);

            if (ObjetoUtil.isNull(fluxoEtapa)) {

                throw new ServiceException(MessageFormat.format("Não foi encontrado fluxo do tipo {0} para o material {1}", senha.getTipoFluxo().getDescricao(), senha.getMaterial().toString()));
            }

            //Cria e adiciona a nova etapa
            SenhaEtapa senhaEtapa = new SenhaEtapa(fluxoEtapa, new Date(), senha.getStatusSenha().name());

            senhaEtapa.setQtdRepeticao(0);

            senha.setEtapas(Collections.singletonList(senhaEtapa));

            senha.setEtapaAtual(senhaEtapa);
        }
    }

    @Override
    public void posSalvar(Senha entidade) {

        super.posSalvar(entidade);

        this.dashboardGerencialService.atualizarDashboard();

        this.dashboardFilaService.atualizarDashboard();

        this.dashboardGerencialService.atualizarDashboard();
    }

    @Override
    @Transactional
    public void adicionarEtapa(Senha senha, FluxoEtapa etapa) {

        //Só faz a inclusão da nova etapa caso seja diferente da atual

        if (!etapa.getEtapa().equals(senha.getEtapaAtual().getEtapa().getEtapa())) {

            //Atribui o status e a hora de saída das etapas
            senha.getEtapas().stream().filter(senhaEtapa -> ObjetoUtil.isNull(senhaEtapa.getHoraSaida()) && ObjetoUtil.isNotNull(senhaEtapa.getEtapa())).forEach(senhaEtapa -> {

                senhaEtapa.setStatusEtapa(this.obterStatusUltimaEtapa(senhaEtapa.getEtapa().getEtapa()));

                senhaEtapa.setHoraSaida(new Date());
            });

            //Cria e adiciona a nova etapa
            SenhaEtapa senhaEtapa = new SenhaEtapa(etapa, new Date(), this.obterStatusEtapa(etapa.getEtapa()));

            senhaEtapa.setQtdRepeticao(Long.valueOf(senha.getEtapas().stream().filter(t -> ObjetoUtil.isNotNull(t.getEtapa()) && t.getEtapa().getEtapa().equals(etapa.getEtapa())).count()).intValue());

            senha.setEtapaAtual(senhaEtapa);

            senha.getEtapas().add(senha.getEtapaAtual());

            senha.getEtapas().sort(Comparator.comparing(SenhaEtapa::getHoraEntrada).reversed());
        }

        if (ColecaoUtil.isNotEmpty(senha.getEtapas())) {

            senha.getEtapas().forEach(senhaEtapa -> senhaEtapa.setSenha(senha));
        }

        Senha senhaSalva = this.dao.save(senha);

        senha.setEtapaAtual(senhaSalva.getEtapaAtual());

        senha.setEtapas(senhaSalva.getEtapas());

        this.dashboardFilaService.atualizarDashboard();

        this.dashboardGerencialService.atualizarDashboard();
    }

    private String obterStatusUltimaEtapa(EtapaEnum etapa) {

        switch (etapa) {
            case EM_FILA_CARGA_DESCARGA:
            case AGUARDANDO_CHEGADA_AMOSTRA:
            case AGUARDANDO_CHAMADA_CARREGAMENTO:
                return StatusSenhaEnum.CHAMADA.name();
            case DISTRIBUICAO_SENHA:
                return StatusSenhaEnum.COMPLETA.name();
            case CHECKLIST:
                return StatusChecklistEnum.APROVADO.name();
            case PESAGEM_TARA:
            case PESAGEM_BRUTO:
                return StatusPesagemQualidadeEnum.CAPTURADO.name();
            case LACRAGEM:
                return StatusLacragemEnum.LACRADO.name();
            case CONVERSAO_LITRAGEM:
                return StatusConversaoLitragemEnum.APROVADO.name();
            case ANALISE_QUALIDADE:
                return StatusAnaliseQualidadeEnum.APROVADO.name();
            case CRIAR_ROMANEIO:
            case CANCELAMENTO_ORDEM:
                return StatusRomaneioEnum.CONCLUIDO.name();
            case EMITIR_DOCUMENTO:
                return StatusEmissaoDocumentoEnum.EMITIDOS.name();
            case DESCARGA:
                return StatusDescarregamentoEnum.DESCARREGADO.name();
            case PENDENCIA_COMERCIAL:
                return StatusPendenciaEnum.LIBERADO.name();
            case CONFERENCIA_GERAIS:
                return StatusConferenciaEnum.LIBERADO.name();
            case CONFIRMACAO_PESO: {
                return StatusConfirmacaoPesoEnum.CONFIRMADO.name();
            }
            case CARREGAMENTO: {
                return StatusLocalCarregamentoPesoEnum.CARREGADO.name();
            }
            case SAIDA:
                return "";
        }
        return null;
    }

    private String obterStatusEtapa(EtapaEnum etapa) {

        switch (etapa) {
            case EM_FILA_CARGA_DESCARGA:
            case AGUARDANDO_CHEGADA_AMOSTRA:
            case DISTRIBUICAO_SENHA:
            case AGUARDANDO_CHAMADA_CARREGAMENTO:
                return StatusSenhaEnum.AGUARDANDO_CHAMADA.name();
            case CHECKLIST:
                return StatusChecklistEnum.AGUARDANDO.name();
            case PESAGEM_TARA:
            case PESAGEM_BRUTO:
                return StatusPesagemQualidadeEnum.AGUARDANDO.name();
            case LACRAGEM:
                return StatusLacragemEnum.AGUARDANDO.name();
            case CONVERSAO_LITRAGEM:
                return StatusConversaoLitragemEnum.AGUARDANDO.name();
            case ANALISE_QUALIDADE:
                return StatusAnaliseQualidadeEnum.AGUARDANDO.name();
            case CRIAR_ROMANEIO:
            case CANCELAMENTO_ORDEM:
                return StatusRomaneioEnum.AGUARDANDO.name();
            case EMITIR_DOCUMENTO:
                return StatusEmissaoDocumentoEnum.AGUARDANDO.name();
            case DESCARGA:
                return StatusDescarregamentoEnum.AGUARDANDO.name();
            case PENDENCIA_COMERCIAL:
                return StatusPendenciaEnum.AGUARDANDO.name();
            case CONFERENCIA_GERAIS:
                return StatusConferenciaEnum.AGUARDANDO.name();
            case CONFIRMACAO_PESO:
                return StatusConfirmacaoPesoEnum.AGUARDANDO.name();
            case CARREGAMENTO:
                return StatusLocalCarregamentoPesoEnum.AGUARDANDO.name();

            case SAIDA:
                return "";
        }
        return null;
    }

    private void formatarSenha(Senha senha) {

        LocalDate hoje = LocalDate.now();
        senha.setSenha("#" + getAbreviacaoMaterial(senha).toUpperCase() + "-" + String.format("%02d", hoje.getYear() % 100) + String.format("%02d", hoje.getMonthValue()) + String.format("%02d", hoje.getDayOfMonth()) + String.format("%03d"
                , getDAO().count() + 1));
    }

    private String getAbreviacaoMaterial(Senha senha) {

        String[] nomes = senha.getMaterial().getDescricao().split(" ");
        if (nomes.length > 1) {
            return nomes[0].substring(0, 2) + nomes[1].substring(0, 1);
        }
        return nomes[0].substring(0, 3);
    }

    @Override
    public byte[] imprimirPdf(Long idSenha) throws JRException {
        Senha senha = get(idSenha);
        senha.setUsuarioCadastro(usuarioService.get(senha.getIdUsuarioAutorCadastro()));

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(REL_SENHA_JASPER);
        Map<String, Object> parametros = montarParametrosRelatorio(senha);

        return RelatorioUtil.gerarPdf(Collections.singletonList(senha), inputStream, parametros);
    }

    @Override
    public void alterarStatusSenha(Senha senha) {

        Senha senhaSalva = get(senha.getId());

        senhaSalva.setStatusSenha(senha.getStatusSenha());

        senhaSalva.setIdUsuarioAutorAlteracao(senha.getIdUsuarioAutorAlteracao());

        senhaSalva.setDataAlteracao(new Date());

        if (StatusSenhaEnum.CHAMADA.equals(senha.getStatusSenha())) {

            CompletableFuture.runAsync(() -> this.chamar(senhaSalva));

            if (ColecaoUtil.isNotEmpty(senhaSalva.getEtapas())) {

                SenhaEtapa ultimaEtapa = senhaSalva.getEtapas().stream().max(Comparator.comparing(SenhaEtapa::getHoraEntrada)).get();

                ultimaEtapa.setQtdChamada(ObjetoUtil.isNull(ultimaEtapa.getQtdChamada()) ? 1 : ultimaEtapa.getQtdChamada() + 1);

                if (EtapaEnum.DISTRIBUICAO_SENHA.equals(ultimaEtapa.getEtapa())) {

                    //Caso ainda esteja na etapa distribuição de senha, é alterado o status para chamada
                    ultimaEtapa.setStatusEtapa(senha.getStatusSenha().name());
                }
            }
        }

        if (ColecaoUtil.isNotEmpty(senhaSalva.getEtapas())) {

            senhaSalva.getEtapas().forEach(senhaEtapa -> senhaEtapa.setSenha(senhaSalva));
        }

        getDAO().save(senhaSalva);

        this.dashboardGerencialService.atualizarDashboard();
    }

    private void chamar(Senha senha) {

        senha.setUsuarioCadastro(this.usuarioService.get(senha.getIdUsuarioAutorAlteracao()));
        this.template.convertAndSend("/senha/chamada/ultima", senha);
    }

    @Override
    public void enviarEmailLiberacao(Senha senha) {

        Usuario usuario = this.usuarioService.get(senha.getIdUsuarioAutorAlteracao());

        Parametro parametro = parametroService.obterPorTipoParametro(TipoParametroEnum.EMAIL_RESPONSAVEL_PELA_LIBERACAO_SENHA);

        emailSenderService.sendEmail(new SolicitarLiberacaoSenhaEmail(senha, usuario, ObjetoUtil.isNotNull(parametro) ? parametro.getValor() : ""));

        registrarLogAtividade(senha);
    }

    @Override
    public Senha obterUltimaSenhaChamada() {

        Senha senha = dao.findTop1ByStatusSenhaOrderByDataAlteracaoDesc(StatusSenhaEnum.CHAMADA);
        if (ObjetoUtil.isNotNull(senha) && ObjetoUtil.isNotNull(senha.getIdUsuarioAutorAlteracao())) {
            Usuario usuario = this.usuarioService.get(senha.getIdUsuarioAutorAlteracao());
            senha.setUsuarioCadastro(usuario);
        }
        return senha;
    }

    @Override
    public List<PainelSenhaDTO> obterUltimasSenhasChamadas(Long idSenha) {

        List<Senha> senhaList = dao.findTop3ByStatusSenhaAndIdNotOrderByDataAlteracaoDesc(StatusSenhaEnum.CHAMADA, idSenha);
        return senhaList.stream().map(Senha::getMaterial).map(material -> new PainelSenhaDTO(new MaterialDTO(material),
                senhaList.stream().filter(senha -> senha.getMaterial().getId().equals(material.getId())).sorted(Comparator.comparing(Senha::getDataAlteracao).reversed()).limit(3).map(SenhaDTO::new).collect(Collectors.toList()))).collect(Collectors.toList());
    }

    @Override
    public Senha salvarFormulario(SenhaFormulario entidade) {

        removerRespostaOpcaoIncorreta(entidade);
        if (entidade.isNew()) {
            //Preenchendo o relacionamento bidirecional
            entidade.getRespostas().forEach(resposta -> {
                resposta.setSenhaFormulario(entidade);
                resposta.getOpcoes().forEach(opcao -> opcao.setSenhaFormularioResposta(resposta));
            });
            entidade.getSenha().getFormularios().add(entidade);
            entidade.getSenha().setFormularioAtual(entidade);
        }
        return super.salvar(entidade.getSenha());
    }

    @Override
    public void executarCancelamentoAutomatico() {

        Parametro parametro = parametroService.obterPorTipoParametro(TipoParametroEnum.QUANTIDADE_HORAS_VALIDADE_SENHA);
        ZonedDateTime dataValidade = ZonedDateTime.now().minusHours(Long.parseLong(parametro.getValor()));
        List<Senha> senhas = this.senhaCustomRepository.obterPorDataAlteracaoBeforeEStatusSenhaIn(Date.from(dataValidade.toInstant()), StatusSenhaEnum.AGUARDANDO_CHAMADA, StatusSenhaEnum.CHAMADA, StatusSenhaEnum.PENDENTE);
        if (ColecaoUtil.isNotEmpty(senhas)) {
            this.dao.atualizarStatusDeSenhas(StatusSenhaEnum.CANCELADA, senhas.stream().map(Senha::getId).collect(Collectors.toList()));
        }
    }

    public void removerRespostaOpcaoIncorreta(SenhaFormulario entidade) {

        Iterator<SenhaFormularioResposta> respostas = entidade.getRespostas().iterator();
        while (respostas.hasNext()) {

            SenhaFormularioResposta resposta = respostas.next();

            if (FormularioItemTipo.SESSAO.equals(resposta.getItem().getTipo())) {
                respostas.remove();
                continue;
            }

            Iterator<SenhaFormularioRespostaOpcao> opcoes = resposta.getOpcoes().iterator();
            while (opcoes.hasNext()) {
                SenhaFormularioRespostaOpcao opcao = opcoes.next();
                if (FormularioItemTipo.CHECKBOX.equals(resposta.getItem().getTipo())) {
                    if (!opcao.isMarcado()) {
                        opcoes.remove();
                    }
                } else if (StringUtil.isNotNullEmpty(resposta.getResposta()) && !opcao.getFormularioItemOpcao().getTitulo().equals(resposta.getResposta())) {
                    opcoes.remove();
                }
            }
        }
    }

    @Override
    public Senha obterPorSenha(String senha) {

        return getDAO().findOneBySenha(senha);
    }

    @Override
    public Senha obterPorIdAgendamentoOuPlaca(ConsultaSenhaMotoristaEntrada consultaSenhaMotoristaEntrada) {

        try {

            if (StringUtil.isPlaca(consultaSenhaMotoristaEntrada.getFiltro())) {

                return getDAO().findOne(((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(
                        criteriaBuilder.not(root.get(Senha_.statusSenha).in(Arrays.asList(StatusSenhaEnum.CANCELADA))),
                        criteriaBuilder.equal(root.join(Senha_.veiculo).get(Veiculo_.placa1), consultaSenhaMotoristaEntrada.getFiltro().replace("-", "").toUpperCase().trim())
                ))).get();

            } else {

                return getDAO().findOne(((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(
                        criteriaBuilder.not(root.get(Senha_.statusSenha).in(Arrays.asList(StatusSenhaEnum.CANCELADA))),
                        criteriaBuilder.equal(root.get(Senha_.cargaPontualIdAgendamento), consultaSenhaMotoristaEntrada.getFiltro())
                ))).get();
            }

        } catch (NoSuchElementException e) {

            return null;
        }
    }

    @Override
    public List<Senha> buscarSenhasAutoComplete(final String value) {

        return this.dao.findTop10ByStatusSenhaAndSenhaContainingIgnoreCaseOrMaterialDescricaoContainingIgnoreCaseOrMotoristaNomeContainingIgnoreCaseOrderBySenhaAsc(StatusSenhaEnum.COMPLETA, value, value, value);
    }

    @Override
    public List<SenhaDTO> listarPorMaterialEPlacaENumeroRomaneioENumeroCartaoAcesso(List<Material> materiais, String placa, String numeroRomaneio, String numeroCartaoAcesso, Date data, List<FluxoEtapaDTO> fluxoEtapas,
                                                                                    List<StatusSenhaEnum> statusSenha) {

        return senhaCustomRepository.listarPorMaterialEPlacaENumeroRomaneioENumeroCartaoAcesso(materiais, placa, numeroRomaneio, numeroCartaoAcesso, data, fluxoEtapas, statusSenha);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Motivo> obterMotivoPorId(Long id) {

        return dao.findMotivosById(id);
    }

    @Override
    public Optional<Senha> obterPorPlacaENumeroCartaoAcessoEEtapaContem(String placa, Integer nrCartao, EtapaEnum... etapas) {

        return getDAO().findOne((root, query, builder) -> {
            query.distinct(true);
            Root<Romaneio> romaneioRoot = query.from(Romaneio.class);
            Join<Senha, Veiculo> veiculoJoin = root.join(Senha_.veiculo);
            Join<Senha, SenhaEtapa> senhaEtapaJoin = root.join(Senha_.etapaAtual);

            return builder.and(builder.equal(builder.upper(veiculoJoin.get(Veiculo_.placa1)), placa.toUpperCase().replace("-", "")), builder.equal(root.get(Senha_.numeroCartaoAcesso), nrCartao),
                    senhaEtapaJoin.join(SenhaEtapa_.etapa).get(FluxoEtapa_.etapa.getName()).in(Arrays.asList(etapas)), builder.not(root.get(Senha_.statusSenha).in(Arrays.asList(StatusSenhaEnum.CANCELADA, StatusSenhaEnum.PENDENTE))),
                    builder.and(builder.equal(root, romaneioRoot.get(Romaneio_.senha))), builder.not(romaneioRoot.get(Romaneio_.statusRomaneio).in(Arrays.asList(StatusRomaneioEnum.PENDENTE, StatusRomaneioEnum.ESTORNADO))));
        });
    }

    @Override
    public boolean existsByVeiculoIdAndStatusSenhaNotInAndIdNot(Long id, List<StatusSenhaEnum> asList, Long idSenha) {

        return getDAO().existsByVeiculoIdAndStatusSenhaNotInAndIdNot(id, asList, idSenha);
    }

    @Override
    public boolean existsByVeiculoIdAndStatusSenhaNotIn(Long id, List<StatusSenhaEnum> asList) {

        return getDAO().existsByVeiculoIdAndStatusSenhaNotIn(id, asList);
    }

    @Override
    public void estornarRomaneioNaAutomacao(Romaneio romaneio) {

        Material material = romaneio.getItens().stream().map(ItemNFPedido::getMaterial).filter(Objects::nonNull).findFirst().get();

        FluxoEtapa fluxoEtapa = dashboardFilaService.obterFluxoEtapaPorMaterialEEtapa(romaneio.getSenha().getTipoFluxo(), material, EtapaEnum.DISTRIBUICAO_SENHA);

        if (ObjetoUtil.isNotNull(fluxoEtapa.getStatusEstornoAutomacao())) {

            this.dashboardFilaService.alterarStatusPesagem(romaneio.getSenha(), fluxoEtapa);
        }
    }

    private Map<String, Object> montarParametrosRelatorio(Senha senha) {

        Map<String, Object> parametros = new HashMap<>();
        parametros.put("LOGO", getClass().getClassLoader().getResourceAsStream(LOGO_PNG));
        if(senha.getCargaPontualDataAgendamentoInicial() != null){
            String dataInicio = DateUtil.formatData_ddMMyyyyHHmmss(senha.getCargaPontualDataAgendamentoInicial());
            String horaFim = DateUtil.formatHora(senha.getCargaPontualDataAgendamentoFinal());

            parametros.put("CARGA_PONTUAL_DATA_AGENDAMENTO", String.format("%s - %s", dataInicio, horaFim));
        }

        return parametros;
    }

    @Override
    public Specification<Senha> obterEspecification(Paginacao<Senha> paginacao) {

        return (Root<Senha> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {
            Join<Senha, Motorista> motorista = root.join("motorista", JoinType.LEFT);
            Join<Senha, Veiculo> veiculo = root.join("veiculo", JoinType.LEFT);
            Join<Senha, Cliente> cliente = root.join("cliente", JoinType.LEFT);
            List<Predicate> predicados = new ArrayList<>();

            if (ObjetoUtil.isNotNull(paginacao.getEntidade().getMaterial())) {
                predicados.add(builder.and(builder.equal(root.get("material"), paginacao.getEntidade().getMaterial())));
            }

            if (ObjetoUtil.isNotNull(paginacao.getEntidade().getMotorista()) && StringUtil.isNotNullEmpty(paginacao.getEntidade().getMotorista().getNome())) {
                predicados.add(builder.and(builder.like(builder.upper(motorista.get("nome")), MatchMode.ANYWHERE.toMatchString(paginacao.getEntidade().getMotorista().getNome().toUpperCase()))));
            }

            if (ObjetoUtil.isNotNull(paginacao.getEntidade().getVeiculo()) && StringUtil.isNotNullEmpty(paginacao.getEntidade().getVeiculo().getPlaca1())) {
                predicados.add(builder.and(builder.like(builder.upper(veiculo.get("placa1")), MatchMode.ANYWHERE.toMatchString(paginacao.getEntidade().getVeiculo().getPlaca1().toUpperCase().replace("-", "")))));
            }

            if (StringUtil.isNotNullEmpty(paginacao.getEntidade().getCodigoOrdemVenda())) {
                predicados.add(builder.and(builder.like(builder.upper(root.get("codigoOrdemVenda")), MatchMode.ANYWHERE.toMatchString(paginacao.getEntidade().getCodigoOrdemVenda()))));
            }

            if (ObjetoUtil.isNotNull(paginacao.getEntidade().getCliente()) && StringUtil.isNotNullEmpty(paginacao.getEntidade().getCliente().getCpfCnpj())) {
                predicados.add(builder.and(builder.like(cliente.get("cpfCnpj"), MatchMode.START.toMatchString(paginacao.getEntidade().getCliente().getCpfCnpj().replace(".", "").replace("-", "")))));
            }

            if (ObjetoUtil.isNotNull(paginacao.getEntidade().getCliente()) && StringUtil.isNotNullEmpty(paginacao.getEntidade().getCliente().getNome())) {
                predicados.add(builder.and(builder.like(builder.upper(cliente.get("nome")), MatchMode.ANYWHERE.toMatchString(paginacao.getEntidade().getCliente().getNome().toUpperCase()))));
            }

            if (ColecaoUtil.isNotEmpty(paginacao.getEntidade().getStatusSenhaList())) {
                predicados.add(builder.and(builder.in(root.get("statusSenha")).value(paginacao.getEntidade().getStatusSenhaList())));
            }

            if (StringUtil.isNotNullEmpty(paginacao.getEntidade().getSenha())) {
                predicados.add(builder.and(builder.like(builder.upper(root.get("senha")), MatchMode.ANYWHERE.toMatchString(paginacao.getEntidade().getSenha().toUpperCase()))));
            }

            if (ObjetoUtil.isNotNull(paginacao.getEntidade().getDataCadastro())) {
                GregorianCalendar gc = new GregorianCalendar();
                gc.setTime(paginacao.getEntidade().getDataCadastro());
                gc.set(Calendar.HOUR, 0);
                gc.set(Calendar.MINUTE, 0);
                gc.set(Calendar.SECOND, 0);
                gc.set(Calendar.MILLISECOND, 0);
                Date from = gc.getTime();
                gc.set(Calendar.HOUR, 23);
                gc.set(Calendar.MINUTE, 59);
                gc.set(Calendar.SECOND, 59);
                gc.set(Calendar.MILLISECOND, 999);
                Date to = gc.getTime();
                predicados.add(builder.and(builder.between(root.get("dataCadastro"), from, to)));
            }

            return builder.and(predicados.toArray(new Predicate[]{}));
        };
    }

    @Override
    public List<TempoPorEtapaDTO> obterTempoMedioPorEtapaESenhas(List<Long> ids, List<FluxoEtapa> etapas) {

        return senhaCustomRepository.obterTempoMedioPorEtapaESenhas(ids, etapas);
    }

    @Override
    public List<TempoPorEtapaDTO> obterTempoMedioPorEtapaEnumESenhas(List<Long> ids, List<EtapaEnum> etapas) {

        return senhaCustomRepository.obterTempoMedioPorEtapaEnumESenhas(ids, etapas);
    }

    @Override
    public Class<SenhaListagemDTO> getDtoListagem() {

        return SenhaListagemDTO.class;
    }

    @Override
    public SenhaDAO getDAO() {

        return dao;
    }

    @Override
    public boolean existeSenhaPorTipoVeiculoSeta(Long tipoVeiculoSetaId) {

        return getDAO().existsSenhaByTipoVeiculoSeta(tipoVeiculoSetaId);
    }

    @Override
    public Boolean getRegistrarLogAtividade() {

        return Boolean.TRUE;
    }

    @Override
    public Pagina<SenhaListagemDTO> dtoListar(Paginacao paginacao) {

        Pagina<SenhaListagemDTO> paginaResultado = super.dtoListar(paginacao, SenhaListagemDTO.class);

        if (paginaResultado.getTotalElements() > 0l) {

            List<Long> senhasId = paginaResultado.getContent().stream().map(SenhaListagemDTO::getId).collect(Collectors.toList());

            Collection<SenhaListagemDTO> senhasEmRomaneiosEmProcessamento = dtoListar(((root, criteriaQuery, criteriaBuilder) -> {

                Subquery<Long> subQuery   = criteriaQuery.subquery(Long.class);
                Root<Romaneio>   romaneioRoot = subQuery.from(Romaneio.class);

                subQuery.where(
                        criteriaBuilder.and(
                                romaneioRoot.join(Romaneio_.senha).get(Senha_.id).in(senhasId),
                                criteriaBuilder.equal(romaneioRoot.get(Romaneio_.statusRomaneio), StatusRomaneioEnum.EM_PROCESSAMENTO)
                        )
                );

                subQuery.select(romaneioRoot.join(Romaneio_.senha).get(Senha_.id));

                return root.get(Senha_.id).in(subQuery);
            }), SenhaListagemDTO.class);

            if (ColecaoUtil.isNotEmpty(senhasEmRomaneiosEmProcessamento)) {

                paginaResultado.getContent().forEach(senha -> {

                    senha.setPodeAlterar(senhasEmRomaneiosEmProcessamento.stream().filter(s -> s.getId().equals(senha.getId())).count() > 0);
                });
            }
        }

        return paginaResultado;
    }

}
