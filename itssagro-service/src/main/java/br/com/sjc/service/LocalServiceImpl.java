package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.LocalService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.Local;
import br.com.sjc.modelo.dto.LocalDTO;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.persistencia.dao.LocalDAO;
import br.com.sjc.util.Util;
import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by julio.bueno on 03/07/2019.
 */
@Service
public class LocalServiceImpl extends ServicoGenerico<Long, Local> implements LocalService {

    @Autowired
    private LocalDAO localDAO;

    @Override
    public DAO<Long, Local> getDAO() {

        return localDAO;
    }

    @Override
    public Class<LocalDTO> getDtoListagem() {

        return LocalDTO.class;
    }

    @Override
    public Specification<Local> obterEspecification(Paginacao<Local> paginacao) {

        return (Specification<Local>) (root, query, criteriaBuilder) -> {
            Local local = paginacao.getEntidade();

            List<Predicate> predicates = new ArrayList<>();

            if (!Objects.isNull(local.getCodigoFormatado())) {
                predicates.add(criteriaBuilder.like(root.get("codigoFormatado"), MatchMode.ANYWHERE.toMatchString(local.getCodigoFormatado())));
            }

            if (!StringUtils.isEmpty(local.getDescricao())) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("descricao")), MatchMode.ANYWHERE.toMatchString(local.getDescricao().toLowerCase())));
            }

            if (!Objects.isNull(local.getStatus())) {
                predicates.add(criteriaBuilder.equal(root.get("status"), local.getStatus()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    @Override
    public void preSalvar(Local entidade) {

        super.preSalvar(entidade);
        gerarCodigo(entidade);
    }

    private void gerarCodigo(Local local) {

        if (local.isNew()) {
            Long ultimoCodigo = localDAO.getMaxCodigo();
            Long proximoCodigo = Objects.isNull(ultimoCodigo) ? 1L : ultimoCodigo + 1;

            local.setCodigo(proximoCodigo);
            local.setCodigoFormatado(Util.formatarCodigo(Local.PREFIXO, proximoCodigo));
        }
    }

    @Override
    public synchronized Local salvar(Local entidade) {

        return super.salvar(entidade);
    }

    @Override
    public List<Local> listarLocaisAtivos() {

        return localDAO.findByStatus(StatusEnum.ATIVO);
    }

    @Override
    public Boolean getRegistrarLogAtividade() {

        return Boolean.TRUE;
    }

}
