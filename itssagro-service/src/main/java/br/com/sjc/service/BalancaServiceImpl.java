package br.com.sjc.service;

import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.BalancaService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.Balanca;
import br.com.sjc.modelo.dto.BalancaDTO;
import br.com.sjc.modelo.dto.CapturaBalanca;
import br.com.sjc.persistencia.dao.BalancaDAO;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class BalancaServiceImpl extends ServicoGenerico<Long, Balanca> implements BalancaService {

    @Autowired
    private BalancaDAO dao;

    @Override
    public List<Balanca> obterBalancasPorDirecaoETipoPeso(final CapturaBalanca entidade) {

        return this.getDAO().findByDirecaoInAndFluxoPesagensTipoFluxo(entidade.getDirecoes(), entidade.getTipoFluxo());
    }

    @Override
    public Specification<Balanca> obterEspecification(final Paginacao<Balanca> paginacao) {

        final Balanca entidade = paginacao.getEntidade();

        final Specification<Balanca> specification = new Specification<Balanca>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<Balanca> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                if (StringUtil.isNotNullEmpty(entidade.getNome())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("nome")), "%" + entidade.getNome().toUpperCase() + "%")));
                }

                if (StringUtil.isNotNullEmpty(entidade.getIp())) {

                    predicates.add(builder.equal(root.get("ip"), entidade.getIp()));
                }

                if (ObjetoUtil.isNotNull(entidade.getDirecao())) {

                    predicates.add(builder.equal(root.get("direcao"), entidade.getDirecao()));
                }

                if (ObjetoUtil.isNotNull(entidade.getStatus())) {

                    predicates.add(builder.equal(root.get("status"), entidade.getStatus()));

                }

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Override
    public void preSalvar(Balanca entidade) {

        this.balancaJaExiste(entidade);

        entidade.getFluxoPesagens().forEach(i -> i.setBalanca(entidade));
    }

    private void balancaJaExiste(final Balanca entidade) {

        final Boolean isCadastro = entidade.getId() == null;

        if ((isCadastro && dao.existsByNome(entidade.getNome()))
                || (!isCadastro && dao.existsByNomeAndNeId(entidade.getNome(), entidade.getId()))) {

            throw new ServicoException("Já existe uma balança com o nome: " + entidade.getNome());
        }
    }

    @Override
    public Class<BalancaDTO> getDtoListagem() {
        return BalancaDTO.class;
    }


    @Override
    public BalancaDAO getDAO() {
        return dao;
    }
}
