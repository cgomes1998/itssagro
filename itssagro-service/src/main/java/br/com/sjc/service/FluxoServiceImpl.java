package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.FluxoService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.*;
import br.com.sjc.modelo.dto.FluxoDTO;
import br.com.sjc.modelo.dto.MaterialDTO;
import br.com.sjc.modelo.enums.LogAtividadeAcao;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.sap.Centro;
import br.com.sjc.persistencia.dao.FluxoDAO;
import br.com.sjc.persistencia.dao.fachada.FluxoCustomRepository;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class FluxoServiceImpl extends ServicoGenerico<Long, Fluxo> implements FluxoService {

    @Autowired
    private FluxoDAO fluxoDAO;

    @Autowired
    private FluxoCustomRepository fluxoCustomRepository;

    @Override
    public List<FluxoDTO> listarOrdernandoDeFormaCrescentePorCodigo() {

        return fluxoCustomRepository.listarOrdernandoDeFormaCrescentePorCodigo();
    }

    @Override
    public List<MaterialDTO> listarMateriaisDoFluxo(List<Long> idFluxos) {

        return fluxoCustomRepository.listarMateriaisDoFluxo(idFluxos);
    }

    @Override
    public void preSalvar(Fluxo fluxo) {

        super.preSalvar(fluxo);

        fluxo.setStatus(StatusEnum.ATIVO);

        if (fluxo.isNew()) {

            fluxo.setCodigo(this.fluxoDAO.count() + 1);
        }

        if (fluxo.getEtapas().stream().filter(FluxoEtapa::isEtapaFinal).count() > 1) {

            throw new ServicoException("Só é permitido uma Etapa Final para o fluxo selecionado");
        }

        fluxo.getCentros().forEach(centro -> centro.setFluxo(fluxo));

        fluxo.getMateriais().forEach(material -> material.setFluxo(fluxo));

        fluxo.getEtapas().stream().forEach(fluxoEtapa -> {

            fluxoEtapa.setFluxo(fluxo);

            fluxoEtapa.getCriterios().stream().forEach(etapaCriterio -> {

                etapaCriterio.setFluxoEtapa(fluxoEtapa);

                etapaCriterio.getCondicoes().stream().forEach(etapaCriterioCondicao -> etapaCriterioCondicao.setEtapaCriterio(etapaCriterio));

                etapaCriterio.getAcoes().stream().forEach(etapaCriterioAcao -> etapaCriterioAcao.setEtapaCriterio(etapaCriterio));

                etapaCriterio.getEventos().stream().forEach(etapaCriterioEvento -> etapaCriterioEvento.setEtapaCriterio(etapaCriterio));
            });
        });
    }

    @Override
    public Specification<Fluxo> obterEspecification(Paginacao<Fluxo> paginacao) {

        return (Specification<Fluxo>) (root, query, criteriaBuilder) -> {
            Fluxo fluxo = paginacao.getEntidade();

            List<Predicate> predicates = new ArrayList<>();

            if (!Objects.isNull(fluxo.getCodigo())) {
                predicates.add(criteriaBuilder.equal(root.get("codigo"), fluxo.getCodigo()));
            }

            if (!Objects.isNull(fluxo.getDescricao())) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("descricao")), MatchMode.ANYWHERE.toMatchString(fluxo.getDescricao().toLowerCase())));
            }

            if (!Objects.isNull(fluxo.getMaterial())) {
                Join<Fluxo, FluxoMaterial> materiais = root.join("materiais", JoinType.LEFT);
                predicates.add(criteriaBuilder.equal(materiais.get("material"), fluxo.getMaterial()));
            }

            if (!Objects.isNull(fluxo.getCentro())) {
                Join<Fluxo, FluxoCentro> centros = root.join("centros", JoinType.LEFT);
                predicates.add(criteriaBuilder.equal(centros.get("centro"), fluxo.getCentro()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    @Override
    public DAO<Long, Fluxo> getDAO() {

        return fluxoDAO;
    }

    @Override
    public Class<FluxoDTO> getDtoListagem() {

        return FluxoDTO.class;
    }

    @Override
    public Fluxo obterPorCentroMaterial(Centro centro, Senha senha) {

        List<Fluxo> fluxos = getDAO().findAll(obterEspecificationFluxoCentro(centro, senha));

        if (ColecaoUtil.isNotEmpty(fluxos) && ColecaoUtil.quantidade(fluxos) > 1) {

            throw new ServicoException("Mais de um fluxo configurado para o centro e material selecionados");

        } else if (ColecaoUtil.isNotEmpty(fluxos)) {

            return fluxos.get(0);
        }

        return null;
    }

    private Specification<Fluxo> obterEspecificationFluxoCentro(Centro centro, Senha senha) {

        return (Root<Fluxo> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {

            List<Predicate> predicados = new ArrayList<>();

            if (ObjetoUtil.isNotNull(senha)) {

                if (ObjetoUtil.isNotNull(senha.getMaterial())) {

                    Join<Fluxo, FluxoMaterial> materiais = root.join("materiais", JoinType.LEFT);

                    predicados.add(builder.equal(materiais.get("material"), senha.getMaterial().getId()));
                }

                predicados.add(builder.equal(root.get(Fluxo_.tipoFluxo), senha.getTipoFluxo()));
            }

            if (!Objects.isNull(centro)) {

                Join<Fluxo, FluxoCentro> centros = root.join("centros", JoinType.LEFT);

                predicados.add(builder.equal(centros.get("centro"), centro));
            }

            predicados.add(builder.equal(root.get("status"), StatusEnum.ATIVO.ordinal()));

            query.distinct(true);

            return builder.and(predicados.toArray(new Predicate[]{}));
        };
    }

    @Override
    @Transactional
    public void alterarStatus(Fluxo entidade) {

        super.alterarStatus(entidade);

        registrarLogAtividade(entidade, StatusEnum.ATIVO.equals(entidade.getStatus()) ? LogAtividadeAcao.ATIVAR : LogAtividadeAcao.INATIVAR);
    }

    @Override
    public Boolean getRegistrarLogAtividade() {

        return Boolean.TRUE;
    }

}
