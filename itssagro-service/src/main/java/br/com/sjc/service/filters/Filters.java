package br.com.sjc.service.filters;

import br.com.sjc.fachada.service.AmbienteService;
import br.com.sjc.fachada.service.AmbienteSincronizacaoService;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;
import org.springframework.beans.factory.annotation.Autowired;

import javax.naming.NamingException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class Filters {

    @Autowired
    private AmbienteService ambienteService;

    @Autowired
    private AmbienteSincronizacaoService ambienteSincronizacaoService;

    protected AmbienteService getAmbienteService() throws NamingException {

        return this.ambienteService;
    }

    protected AmbienteSincronizacaoService getAmbienteSincronizadoService() throws NamingException {

        return this.ambienteSincronizacaoService;
    }

    public abstract JCoRequest instace(String centro, String requestKey, DadosSincronizacaoEnum entidade, DadosSincronizacao dadosSincronizacao, String rfcProgramID, String codigo, Date ultimaSincronizacao);

    public List<EventInRequestItem> addFilters(final String requestKey, final Map<String, String> fields, final String itemDocumento) {

        final List<EventInRequestItem> filters = new LinkedList<EventInRequestItem>();

        int sequencia = 1;

        for (final String field : fields.keySet()) {

            filters.add(EventInRequestItem.instance(requestKey, field, String.valueOf(sequencia++), itemDocumento, fields.get(field)));
        }

        if (fields.isEmpty()) {

            filters.add(EventInRequestItem.instance(requestKey, null, null, null, null));
        }

        return filters;
    }
}
