package br.com.sjc.service;

import br.com.sjc.fachada.service.ConfiguracaoUnidadeMedidaMaterialService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.sap.ConfiguracaoUnidadeMedidaMaterial;
import br.com.sjc.persistencia.dao.ConfiguracaoUnidadeMedidaMaterialDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.logging.Logger;

@Service
public class ConfiguracaoUnidadeMedidaMaterialServiceImpl extends ServicoGenerico<Long, ConfiguracaoUnidadeMedidaMaterial> implements ConfiguracaoUnidadeMedidaMaterialService {

    private static final Logger LOGGER = Logger.getLogger(ConfiguracaoUnidadeMedidaMaterialServiceImpl.class.getName());

    @Autowired
    private ConfiguracaoUnidadeMedidaMaterialDAO dao;

    @Transactional(readOnly = true)
    @Override
    public List<ConfiguracaoUnidadeMedidaMaterial> obterConfiguracoesUnidadesMedidas(
            String codigoMaterial
    ) {
        return dao.findByMaterialCodigo(codigoMaterial);
    }

    @Override
    public ConfiguracaoUnidadeMedidaMaterialDAO getDAO() {

        return this.dao;
    }

    @Override
    public Class<? extends DataDTO> getDtoListagem() {

        return null;
    }
}
