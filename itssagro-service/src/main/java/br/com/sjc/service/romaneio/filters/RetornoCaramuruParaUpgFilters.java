package br.com.sjc.service.romaneio.filters;

import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.sap.ItemNFPedido;
import br.com.sjc.modelo.sap.ItensClassificacao;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.request.EventInRequest;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class RetornoCaramuruParaUpgFilters extends RomaneioFilters {

    @Override
    public JCoRequest instace(
            final Romaneio romaneio,
            final String centro,
            final String requestKey,
            final DadosSincronizacao dadosSincronizacao,
            final String rfcProgramID
    ) {

        return EventInRequest.instance(dadosSincronizacao, centro, rfcProgramID, this.getFilters(romaneio, requestKey), false, null);
    }

    private List<EventInRequestItem> getFilters(
            final Romaneio romaneio,
            final String requestKey
    ) {

        final List<EventInRequestItem> filters = new LinkedList<EventInRequestItem>();

        int seq = 1;

        String pattern = ERM;

        String requests[] = requestKey.split(pattern);

        String request = requests[0];

        int countRequest = 0;

        int codItem = 0;

        for (ItemNFPedido item : romaneio.getItens()) {

            countRequest = countRequest + 10;

            codItem = codItem + 1;

            String novaRequest = StringUtil.empty();

            Integer requestKeyDivisao = Integer.parseInt(request + countRequest);

            if (romaneio.getItens().size() == 1) {

                novaRequest = requestKey;

            } else {

                novaRequest = String.valueOf(requestKeyDivisao) + pattern + requests[1];
            }

            seq = this.montarBridgeParaUsuarioCriadorRomaneio(romaneio, novaRequest, filters, seq);
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_ITEM.name(), String.valueOf(seq++), String.valueOf(codItem), String.valueOf(codItem)));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_ROMAN.name(), String.valueOf(seq++), String.valueOf(codItem), romaneio.getNumeroRomaneio()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_DATA.name(), String.valueOf(seq++), String.valueOf(codItem), DateUtil.formatToSAP(DateUtil.hoje())));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_CENTRO.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(romaneio.getCentro()) ? romaneio.getCentro().getCodigo() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_OPERA.name(), String.valueOf(seq++), String.valueOf(codItem), romaneio.getOperacao().getOperacao()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_ARMT.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getProdutor()) ? item.getProdutor().getCodigo() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_SAFRA.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getSafra()) ? item.getSafra().getCodigo() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_MATERIAL.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getMaterial()) ? item.getMaterial().getCodigo() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_TRANSP.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getTransportadora()) ? item.getTransportadora().getCodigo() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_MOTORIST.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getMotorista()) ? item.getMotorista().getCodigo() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PLACAC.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getPlacaCavalo()) ? StringUtil.toPlaca(item.getPlacaCavalo().getPlaca1()) : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_UF.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getPlacaCavalo()) && ObjetoUtil.isNotNull(item.getPlacaCavalo().getUfPlaca1()) ? item.getPlacaCavalo().getUfPlaca1().getSigla() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PLACA1.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getPlacaCavaloUm()) ? StringUtil.removerCaracteresEspeciais(item.getPlacaCavaloUm().getPlaca1()) : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PLACA2.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getPlacaCavaloDois()) ? StringUtil.removerCaracteresEspeciais(item.getPlacaCavaloDois().getPlaca1()) : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_TIPOCAP.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getTipoVeiculo()) ? item.getTipoVeiculo().getCodigo() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_TIPOFRE.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getTipoFrete()) ? item.getTipoFrete().name() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_NUMNFE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(item.getNumeroNfe()) ? item.getNumeroNfe() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_SERIENFE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(item.getSerieNfe()) ? item.getSerieNfe() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_DNFE.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getDataNfe()) ? DateUtil.formatToSAP(item.getDataNfe()) : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_HNFE.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getDataNfe()) ? DateUtil.formatHoraSemCaracteres(item.getDataNfe()) : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_VNFE.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getValorUnitarioNfe()) ? item.getValorUnitarioNfe().toString() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PNFE.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getPesoTotalNfe()) ? item.getPesoTotalNfe().toString() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_LOGNFE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(item.getNumeroLog()) ? item.getNumeroLog() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_NANFE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(item.getNumeroAleatorioChaveAcesso()) ? item.getNumeroAleatorioChaveAcesso() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_DVNFE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(item.getDigitoVerificadorNfe()) ? item.getDigitoVerificadorNfe() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_VTNFE.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getValorTotalNfe()) ? item.getValorTotalNfe().toString() : StringUtil.empty()));

            // Informações do CTE
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_VALOCTE.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(romaneio.getValorTotalCte()) ? romaneio.getValorTotalCte().toString() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_UFEMIT.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(romaneio.getUfEmissoraCte()) ? romaneio.getUfEmissoraCte().toString() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_UFTOMA.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(romaneio.getUfTomadoraCte()) ? romaneio.getUfTomadoraCte().toString() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_POCTE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(romaneio.getPedidoCte()) ? romaneio.getPedidoCte() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_ITEMCTE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(romaneio.getItemPedidoCte()) ? romaneio.getItemPedidoCte() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_IVACTE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(romaneio.getIvaCte()) ? romaneio.getIvaCte() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PARCCTE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(romaneio.getParceiroCte()) ? romaneio.getParceiroCte() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_CHAVECTE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(romaneio.getChaveAcessoCte()) ? romaneio.getChaveAcessoCte() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_NUNCTE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(romaneio.getNumeroCte()) ? romaneio.getNumeroCte() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_SERIECTE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(romaneio.getSerieCte()) ? romaneio.getSerieCte() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_LOGCTE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(romaneio.getLogCte()) ? romaneio.getLogCte() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_NUMALE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(romaneio.getNumeroAleatorioCte()) ? romaneio.getNumeroAleatorioCte() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_DIGCTE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(romaneio.getDigitoVerificadorCte()) ? romaneio.getDigitoVerificadorCte() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_DATACTE.name(), String.valueOf(seq++), String.valueOf(codItem), DateUtil.formatToSAP(romaneio.getDataCte())));

            BigDecimal descontos = BigDecimal.ZERO;

            if (ObjetoUtil.isNotNull(romaneio.getClassificacao()) && ColecaoUtil.isNotEmpty(romaneio.getClassificacao().getItens())) {

                descontos = romaneio.getClassificacao().getItens().stream().map(ItensClassificacao::getDescontoValido).reduce(BigDecimal.ZERO, BigDecimal::add);
            }

            seq = this.mapeamentoBridgeParaRateio(romaneio, filters, seq, novaRequest, codItem, item, descontos);

            seq = this.montarPesagemDestino(romaneio, descontos, novaRequest, filters, seq);

            if (romaneio.isPossuiDadosOrigem()) {

                seq = this.montarPesagemOrigem(romaneio, novaRequest, filters, seq);

                seq = this.montarClassificacaoOrigem(romaneio.getItensClassificacaoOrigem(), novaRequest, filters, seq);
            }

            seq = this.montarClassificacaoDestino(romaneio.getClassificacao(), novaRequest, filters, seq);

            int grupo = 1;

            seq = this.montarObservacao(romaneio, novaRequest, filters, seq, grupo);

            grupo++;

            this.montarObservacaoNota(romaneio, novaRequest, filters, seq, grupo);

            adicionarRequestParaRomaneioComDivisao(romaneio, countRequest, novaRequest);
        }


        return filters;
    }


}
