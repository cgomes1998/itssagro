package br.com.sjc.service.filters;

import br.com.sjc.fachada.service.VeiculoService;
import br.com.sjc.fachada.servico.impl.ServiceInject;
import br.com.sjc.modelo.cfg.AmbienteSincronizacao;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.sap.Veiculo;
import br.com.sjc.modelo.sap.Veiculo_;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.request.EventInRequest;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;
import br.com.sjc.modelo.sap.sync.SAPVeiculo;
import br.com.sjc.service.SAPServiceImpl;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.quartz.JobDataMap;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

public class PreCadastroVeiculoFilters extends PreCadastroFilters {

    private static final Logger LOGGER = Logger.getLogger(PreCadastroVeiculoFilters.class.getName());

    @Override
    public JCoRequest instace(JobDataMap jobDataMap, final String centro, final String requestKey, final DadosSincronizacaoEnum entidade, final DadosSincronizacao dadosSincronizacao, final String rfcProgramID, final String codigo,
                              final AmbienteSincronizacao sincronizacao) {

        try {

            final List<Veiculo> entidades = ObjetoUtil.isNotNull(ServiceInject.veiculoService) ? ServiceInject.veiculoService.listarPreCadastro() : this.getVeiculoService(jobDataMap).listarPreCadastro();

            return ColecaoUtil.isNotEmpty(entidades) ? EventInRequest.instance(dadosSincronizacao, centro, rfcProgramID, this.getVeiculoFilters(jobDataMap, requestKey, entidades, sincronizacao, centro), false, entidades) : null;

        } catch (final Exception e) {

            PreCadastroVeiculoFilters.LOGGER.info("INICIO ERRO NA CRIACAO DE REQUEST PARA PRE-CADASTRO");

            e.printStackTrace();

            PreCadastroVeiculoFilters.LOGGER.info("FIM ERRO NA CRIACAO DE REQUEST PARA PRE-CADASTRO");

        }

        return null;
    }

    private List<EventInRequestItem> getVeiculoFilters(JobDataMap jobDataMap, String requestKey, final List<Veiculo> entidades, AmbienteSincronizacao sincronizacao, String centro) throws Exception {

        final List<EventInRequestItem> filters = new LinkedList<EventInRequestItem>();

        if (ColecaoUtil.isNotEmpty(entidades)) {

            for (final Veiculo veiculo : entidades) {

                veiculo.setRequestKey(requestKey);

                filters.add(EventInRequestItem.instance(requestKey, SAPVeiculo.COLUMN_INPUT.C_PRE_CAD.name(), "1", SAPServiceImpl.FLAG_SAP));
                filters.add(EventInRequestItem.instance(requestKey, SAPVeiculo.COLUMN_INPUT.C_PLACA1.name(), "2", veiculo.getPlaca1()));
                filters.add(EventInRequestItem.instance(requestKey, SAPVeiculo.COLUMN_INPUT.C_UF1.name(), "3", ObjetoUtil.isNotNull(veiculo.getUfPlaca1()) ? veiculo.getUfPlaca1().name() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(requestKey, SAPVeiculo.COLUMN_INPUT.C_PLACA2.name(), "4", veiculo.getPlaca2()));
                filters.add(EventInRequestItem.instance(requestKey, SAPVeiculo.COLUMN_INPUT.C_UF2.name(), "5", ObjetoUtil.isNotNull(veiculo.getUfPlaca2()) ? veiculo.getUfPlaca2().name() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(requestKey, SAPVeiculo.COLUMN_INPUT.C_PLACA3.name(), "6", veiculo.getPlaca3()));
                filters.add(EventInRequestItem.instance(requestKey, SAPVeiculo.COLUMN_INPUT.C_UF3.name(), "7", ObjetoUtil.isNotNull(veiculo.getUfPlaca3()) ? veiculo.getUfPlaca3().name() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(requestKey, SAPVeiculo.COLUMN_INPUT.C_MUNICIPIO.name(), "8", veiculo.getMunicipio()));
                filters.add(EventInRequestItem.instance(requestKey, SAPVeiculo.COLUMN_INPUT.C_RENAVAM.name(), "9", veiculo.getRenavam()));
                filters.add(EventInRequestItem.instance(requestKey, SAPVeiculo.COLUMN_INPUT.C_CHASSI.name(), "10", veiculo.getChassi()));
                filters.add(EventInRequestItem.instance(requestKey, SAPVeiculo.COLUMN_INPUT.C_MARCA.name(), "11", veiculo.getMarca()));
                filters.add(EventInRequestItem.instance(requestKey, SAPVeiculo.COLUMN_INPUT.C_TIPO.name(), "12", ObjetoUtil.isNotNull(veiculo.getTipo()) ? veiculo.getTipo().getCodigo() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(requestKey, SAPVeiculo.COLUMN_INPUT.C_COR.name(), "13", veiculo.getCor()));
                filters.add(EventInRequestItem.instance(requestKey, SAPVeiculo.COLUMN_INPUT.C_EIXOS.name(), "14", String.valueOf(ObjetoUtil.isNotNull(veiculo.getEixos()) ? veiculo.getEixos() : "0")));
                filters.add(EventInRequestItem.instance(requestKey, SAPVeiculo.COLUMN_INPUT.ID_VEICULO.name(), "15", veiculo.getId().toString()));
                filters.add(EventInRequestItem.instance(requestKey, SAPVeiculo.COLUMN_INPUT.C_TIPO1.name(), "16", ObjetoUtil.isNotNull(veiculo.getTipo()) ? veiculo.getTipo().getTipo() : StringUtil.empty()));

                if (ObjetoUtil.isNotNull(ServiceInject.veiculoService)) {

                    ServiceInject.veiculoService.alterarAtributos(veiculo, Veiculo_.REQUEST_KEY);

                } else {

                    this.getVeiculoService(jobDataMap).alterarAtributos(veiculo, Veiculo_.REQUEST_KEY);
                }

                getAmbienteSincronizadoService(jobDataMap).updateRequestCount(sincronizacao);

                sincronizacao = this.obterAmbienteSincronizacao(jobDataMap, sincronizacao.getDadosSincronizacao(), sincronizacao.getAmbiente());

                requestKey = EventInRequestItem.generateKey(sincronizacao.getRequestCount(), SAPServiceImpl.CHAMADA_EXTERNA_SAP, sincronizacao.getDadosSincronizacao().getModulo().getCodigo(), centro, DateUtil.getYear(DateUtil.hoje()));

            }
        }

        return filters;
    }

    public VeiculoService getVeiculoService(JobDataMap jobDataMap) {

        return (VeiculoService) jobDataMap.get("veiculoService");
    }

}
