package br.com.sjc.service;

import br.com.sjc.fachada.service.ConfiguracaoEmailService;
import br.com.sjc.fachada.service.DadosSincronizacaoService;
import br.com.sjc.fachada.service.PesagemService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.Pesagem;
import br.com.sjc.modelo.PesagemHistorico;
import br.com.sjc.modelo.Usuario;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.dto.PesagemHistoricoDTO;
import br.com.sjc.modelo.dto.PesagemManualDTO;
import br.com.sjc.modelo.dto.RelatorioFluxoDeCarregamentoDTO;
import br.com.sjc.modelo.sap.ItemNFPedido;
import br.com.sjc.modelo.sap.email.EmailAlteracaoPesagem;
import br.com.sjc.persistencia.dao.PesagemDAO;
import br.com.sjc.persistencia.dao.fachada.PesagemCustomRepository;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import java.math.BigDecimal;
import java.util.*;

import static br.com.sjc.fachada.rest.BaseEndpoint.TOKEN_USER;

@Service
public class PesagemServiceImpl extends ServicoGenerico<Long, Pesagem> implements PesagemService {

    public static final String DATA_PRIMEIRA_PESAGEM = "data_primeira_pesagem";

    public static final String DATA_SEGUNDA_PESAGEM = "data_segunda_pesagem";

    @Autowired
    private PesagemDAO dao;

    @Autowired
    private EmailSenderServiceImpl emailSenderService;

    @Autowired
    private ConfiguracaoEmailService configuracaoEmailService;

    @Autowired
    private DadosSincronizacaoService dadosSincronizacaoService;

    @Autowired
    private PesagemCustomRepository pesagemCustomRepository;

    @Override
    public PesagemDAO getDAO() {
        return this.dao;
    }

    @Override
    public Class<? extends DataDTO> getDtoListagem() {
        return null;
    }

    @Override
    public Pesagem salvar(Pesagem pesagem) {

        this.gerarHistorico(pesagem);

        return super.salvar(pesagem);
    }

    private void enviarEmailDeAlteracaoDePesagem(Pesagem pesagem) {

        if (!Objects.isNull(pesagem.getDadosSincronizacaoEnum())) {
            DadosSincronizacao dadosSincronizacao = dadosSincronizacaoService.obterPorEntidade(pesagem.getDadosSincronizacaoEnum());

            if (!Objects.isNull(dadosSincronizacao) && dadosSincronizacao.isEnviarEmailPesagemManual()) {
                pesagem.setPesoInicialAntigo(dao.findPesoInicialById(pesagem.getId()));
                pesagem.setPesoFinalAntigo(dao.findPesoFinalById(pesagem.getId()));

                if(pesagem.getPesoInicial() == null){
                    pesagem.setPesoInicial(BigDecimal.ZERO);
                }

                if(pesagem.getPesoInicialAntigo() == null){
                    pesagem.setPesoInicialAntigo(BigDecimal.ZERO);
                }

                if(pesagem.getPesoFinal() == null){
                    pesagem.setPesoFinal(BigDecimal.ZERO);
                }

                if(pesagem.getPesoFinalAntigo() == null){
                    pesagem.setPesoFinalAntigo(BigDecimal.ZERO);
                }

                if((pesagem.isPesagemBrutaManual() && pesagem.getPesoInicial().compareTo(pesagem.getPesoInicialAntigo()) != 0) || (pesagem.isPesagemFinalManual() && pesagem.getPesoFinal().compareTo(pesagem.getPesoFinalAntigo()) != 0)){
                    pesagem.setEmailsAlteracao(configuracaoEmailService.get().getEmailsPesagem());
                    this.emailSenderService.sendEmail(new EmailAlteracaoPesagem(pesagem));
                }
            }
        }
    }

    private void gerarHistorico(Pesagem pesagem) {

        if (ColecaoUtil.isEmpty(pesagem.getHistoricos())) {

            adicionarHistoricoEEnviarEmail(pesagem);

        } else {

            Optional<PesagemHistorico> ultimaPesagem = pesagem.getHistoricos().stream().sorted(Comparator.comparing(PesagemHistorico::getDataCadastro).reversed()).findFirst();

            ultimaPesagem.ifPresent(anterior -> {

                if ((ObjetoUtil.isNotNull(pesagem.getPesoFinal()) && !pesagem.getPesoFinal().equals(anterior.getPesoFinal())) || (ObjetoUtil.isNotNull(pesagem.getPesoInicial()) && !pesagem.getPesoInicial().equals(anterior.getPesoInicial()))) {

                    adicionarHistoricoEEnviarEmail(pesagem);
                }
            });
        }

        if (ColecaoUtil.isNotEmpty(pesagem.getHistoricos())) {

            pesagem.getHistoricos().forEach(pesagemHistorico -> pesagemHistorico.setPesagem(pesagem));
        }
    }

    private void adicionarHistoricoEEnviarEmail(Pesagem pesagem) {

        PesagemHistorico historico = new PesagemHistorico();

        historico.setPesagem(pesagem);

        historico.setPesoInicial(pesagem.getPesoInicial());

        historico.setPesoFinal(pesagem.getPesoFinal());

        historico.setDataCadastro(new Date());

        if (ObjetoUtil.isNotNull(pesagem.getUsuarioPesoInicial()) && ObjetoUtil.isNull(pesagem.getUsuarioPesoFinal())) {

            historico.setUsuario(pesagem.getUsuarioPesoInicial());

        } else if (ObjetoUtil.isNotNull(pesagem.getUsuarioPesoFinal())) {

            historico.setUsuario(pesagem.getUsuarioPesoFinal());
        }

        pesagem.getHistoricos().add(historico);

        this.enviarEmailDeAlteracaoDePesagem(pesagem);
    }

    @Override
    public PesagemHistoricoDTO obterSegundaPesagemDoRomaneio(Long idPesagem) {

        List<PesagemHistoricoDTO> pesagensHistorico = obterSegundaPesagemDeRomaneios(Collections.singletonList(idPesagem));

        if (ColecaoUtil.isNotEmpty(pesagensHistorico)) {

            return pesagensHistorico.get(0);
        }

        return null;
    }

    @Override
    public PesagemHistoricoDTO obterPrimeiraPesagemDoRomaneio(Long idPesagem) {

        List<PesagemHistoricoDTO> pesagensHistorico = obterPrimeiraPesagemDeRomaneios(Collections.singletonList(idPesagem));

        if (ColecaoUtil.isNotEmpty(pesagensHistorico)) {

            return pesagensHistorico.get(0);
        }

        return null;
    }

    @Override
    public PesagemHistoricoDTO obterSegundaPesagemDoRomaneio(List<PesagemHistoricoDTO> pesagensHistorico, Long idPesagem) {

        if (ColecaoUtil.isNotEmpty(pesagensHistorico)) {

            return pesagensHistorico.stream().filter(pesagemHistorico -> pesagemHistorico.getIdPesagem().equals(idPesagem)).findFirst().orElse(null);
        }

        return null;
    }

    @Override
    public PesagemHistoricoDTO obterPrimeiraPesagemDoRomaneio(List<PesagemHistoricoDTO> pesagensHistorico, Long idPesagem) {

        if (ColecaoUtil.isNotEmpty(pesagensHistorico)) {

            return pesagensHistorico.stream().filter(pesagemHistorico -> pesagemHistorico.getIdPesagem().equals(idPesagem)).findFirst().orElse(null);
        }

        return null;
    }

    @Override
    public List<PesagemHistoricoDTO> obterSegundaPesagemDeRomaneios(List<Long> idPesagens) {
        return pesagemCustomRepository.obterSegundaPesagemDeRomaneios(idPesagens);
    }

    @Override
    public List<PesagemHistoricoDTO> obterPrimeiraPesagemDeRomaneios(List<Long> idPesagens) {
        return pesagemCustomRepository.obterPrimeiraPesagemDeRomaneios(idPesagens);
    }

    @Override
    public List<PesagemManualDTO> obterPesagensManuais(final ItemNFPedido filtro) {
        return pesagemCustomRepository.obterPesagensManuais(filtro);
    }

    @Override
    public Map<String, Date> getDatasDePesagem(
            List<PesagemHistoricoDTO> historicosPrimeiraPesagem,
            List<PesagemHistoricoDTO> historicosSegundaPesagem,
            Long idPesagem
    ) {

        Map<String, Date> mapDatasPesagem = new HashMap<>();

        PesagemHistoricoDTO primeiraPesagem = null;

        if (ColecaoUtil.isNotEmpty(historicosPrimeiraPesagem)) {

            primeiraPesagem = obterPrimeiraPesagemDoRomaneio(historicosPrimeiraPesagem, idPesagem);

            if (ObjetoUtil.isNotNull(primeiraPesagem)) {

                mapDatasPesagem.put(PesagemServiceImpl.DATA_PRIMEIRA_PESAGEM, primeiraPesagem.getDataCadastro());
            }
        }

        if (ColecaoUtil.isNotEmpty(historicosSegundaPesagem)) {

            PesagemHistoricoDTO segundaPesagem = obterSegundaPesagemDoRomaneio(historicosSegundaPesagem, idPesagem);

            if (ObjetoUtil.isNotNull(segundaPesagem)) {

                if (ObjetoUtil.isNull(primeiraPesagem)) {

                    mapDatasPesagem.put(PesagemServiceImpl.DATA_PRIMEIRA_PESAGEM, segundaPesagem.getDataCadastro());
                }

                mapDatasPesagem.put(PesagemServiceImpl.DATA_SEGUNDA_PESAGEM, segundaPesagem.getDataCadastro());
            }
        }

        return mapDatasPesagem;
    }

    @Override
    public Boolean getRegistrarLogAtividade() {

        return Boolean.TRUE;
    }

}
