package br.com.sjc.service.filters;

import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.cfg.AmbienteSincronizacao;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.service.SAPServiceImpl;
import br.com.sjc.util.ObjetoUtil;
import org.quartz.JobDataMap;

import java.util.Date;
import java.util.logging.Logger;

public class FiltersFactory {

    private static final Logger logger = Logger.getLogger(SAPServiceImpl.class.getName());

    public static JCoRequest createPreCadastro(JobDataMap jobDataMap, final String centro, final String requestKey, final DadosSincronizacaoEnum entidade, final DadosSincronizacao dadosSincronizacao, final String rfcProgramID, final String codigo, AmbienteSincronizacao sincronizacao) {

        switch (entidade) {

            case MOTORISTA: {

                return new PreCadastroMotoristaFilters().instace(jobDataMap, centro, requestKey, entidade, dadosSincronizacao, rfcProgramID, codigo, sincronizacao);
            }

            case VEICULO: {

                return new PreCadastroVeiculoFilters().instace(jobDataMap, centro, requestKey, entidade, dadosSincronizacao, rfcProgramID, codigo, sincronizacao);
            }

            default:

                break;
        }

        return null;
    }

    public static JCoRequest create(final String centro, final String requestKey, final DadosSincronizacaoEnum entidade, final DadosSincronizacao dadosSincronizacao, final String rfcProgramID, final String codigo, Date ultimaSincronizacao) {

        if (ObjetoUtil.isNotNull(dadosSincronizacao)) {

            switch (entidade) {

                case PRODUTOR: {

                    return new ProdutorFilters().instace(centro, requestKey, entidade, dadosSincronizacao, rfcProgramID, codigo, ultimaSincronizacao);
                }

                case MATERIAL: {

                    return new MaterialFilters().instace(centro, requestKey, entidade, dadosSincronizacao, rfcProgramID, codigo, ultimaSincronizacao);
                }

                case CENTRO: {

                    return new CentroFilters().instace(centro, requestKey, entidade, dadosSincronizacao, rfcProgramID, codigo, ultimaSincronizacao);
                }

                case MOTORISTA: {

                    return new MotoristaFilters().instace(centro, requestKey, entidade, dadosSincronizacao, rfcProgramID, codigo, ultimaSincronizacao);
                }

                case VEICULO: {

                    return new VeiculoFilters().instace(centro, requestKey, entidade, dadosSincronizacao, rfcProgramID, codigo, ultimaSincronizacao);
                }

                case SAFRA: {

                    return new SafraFilters().instace(centro, requestKey, entidade, dadosSincronizacao, rfcProgramID, codigo, ultimaSincronizacao);
                }

                case TRANSPORTADORA: {

                    return new TransportadoraFilters().instace(centro, requestKey, entidade, dadosSincronizacao, rfcProgramID, codigo, ultimaSincronizacao);
                }

                case TIPO_VEICULO: {

                    return new TipoVeiculoFilters().instace(centro, requestKey, entidade, dadosSincronizacao, rfcProgramID, codigo, ultimaSincronizacao);
                }

                case TABELA_CLASSIFICACAO: {

                    return new TabelaClassificacaoFilters().instace(centro, requestKey, entidade, dadosSincronizacao, rfcProgramID, codigo, ultimaSincronizacao);
                }

                default:

                    FiltersFactory.logger.info("ENTIDADE " + entidade.name() + " NÃO ENCONTRADA");

                    return null;
            }
        }

        FiltersFactory.logger.info("ENTIDADE " + entidade.name() + " NÃO MAPEADA");

        return null;
    }

}
