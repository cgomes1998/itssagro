package br.com.sjc.service.filters;

import br.com.sjc.fachada.service.AmbienteSincronizacaoService;
import br.com.sjc.fachada.servico.impl.ServiceInject;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.cfg.Ambiente;
import br.com.sjc.modelo.cfg.AmbienteSincronizacao;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;
import br.com.sjc.util.ObjetoUtil;
import org.quartz.JobDataMap;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class PreCadastroFilters {

    public abstract JCoRequest instace(JobDataMap jobDataMap, String centro, String requestKey, DadosSincronizacaoEnum entidade, DadosSincronizacao dadosSincronizacao, String rfcProgramID, String codigo, AmbienteSincronizacao sincronizacao);

    public List<EventInRequestItem> addFilters(final String requestKey, final Map<String, String> fields, final String itemDocumento) {

        final List<EventInRequestItem> filters = new LinkedList<EventInRequestItem>();

        int sequencia = 1;

        for (final String field : fields.keySet()) {

            filters.add(EventInRequestItem.instance(requestKey, field, String.valueOf(sequencia++), itemDocumento, fields.get(field)));
        }

        if (fields.isEmpty()) {

            filters.add(EventInRequestItem.instance(requestKey, null, null, null, null));
        }

        return filters;
    }

    protected AmbienteSincronizacao obterAmbienteSincronizacao(JobDataMap jobDataMap, final DadosSincronizacao dadosSincronizacao, final Ambiente ambiente) throws Exception {

        AmbienteSincronizacao sincronizacao = ObjetoUtil.isNotNull(ServiceInject.ambienteSincronizacaoService) ? ServiceInject.ambienteSincronizacaoService.obterPorAmbienteESincronizacao(dadosSincronizacao, ambiente) : this.getAmbienteSincronizadoService(jobDataMap).obterPorAmbienteESincronizacao(dadosSincronizacao, ambiente);

        if (ObjetoUtil.isNull(sincronizacao)) {

            sincronizacao = new AmbienteSincronizacao();

            sincronizacao.setAmbiente(ambiente);

            sincronizacao.setDadosSincronizacao(dadosSincronizacao);

            sincronizacao.setRequestCount(1l);

            sincronizacao.setUltimaSincronizacao(new Date());
        }

        return sincronizacao;
    }

    protected AmbienteSincronizacaoService getAmbienteSincronizadoService(JobDataMap jobDataMap) {

        return ObjetoUtil.isNotNull(ServiceInject.ambienteSincronizacaoService) ? ServiceInject.ambienteSincronizacaoService : (AmbienteSincronizacaoService) jobDataMap.get("ambienteSincronizacaoService");
    }

}
