package br.com.sjc.service.romaneio.filters;

import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.request.EventInRequest;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;

import java.util.LinkedList;
import java.util.List;

public class EntradaFisicaTransferenciaFilters extends RomaneioFilters {

    @Override
    public JCoRequest instace(final Romaneio romaneio, final String centro, final String requestKey, final DadosSincronizacao dadosSincronizacao, final String rfcProgramID) {

        return EventInRequest.instance(dadosSincronizacao, centro, rfcProgramID, this.getFilters(romaneio, requestKey, centro, dadosSincronizacao), false, null);
    }

    private List<EventInRequestItem> getFilters(final Romaneio romaneio, final String requestKey, final String centro, DadosSincronizacao dadosSincronizacao) {

        final List<EventInRequestItem> filters = new LinkedList<EventInRequestItem>();

        int seq = this.montarBridgeParaUsuarioCriadorRomaneio(romaneio, requestKey, filters, 1);

        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_CENTRO.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getCentroDestino()) ? romaneio.getCentroDestino().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_NAME1.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getCentroDestino()) ? romaneio.getCentroDestino().getDescricao() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_MATERIAL.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getMaterial()) ? romaneio.getMaterial().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_DEPOS.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getDeposito()) ? romaneio.getDeposito().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_SAFRA.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getSafra()) ? romaneio.getSafra().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_ROMAN.name(), String.valueOf(seq++), romaneio.getNumeroRomaneio()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_DATA.name(), String.valueOf(seq++), DateUtil.formatToSAP(DateUtil.hoje())));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_OPERA.name(), String.valueOf(seq++), romaneio.getOperacao().getOperacao()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_REMESSA.name(), String.valueOf(seq++), StringUtil.isNotNullEmpty(romaneio.getRemessaNfe()) ? romaneio.getRemessaNfe() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_NUMNFE.name(), String.valueOf(seq++), StringUtil.isNotNullEmpty(romaneio.getNumeroNfe()) ? romaneio.getNumeroNfe() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_SERIENFE.name(), String.valueOf(seq++), StringUtil.isNotNullEmpty(romaneio.getSerieNfe()) ? romaneio.getSerieNfe() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_DNFE.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getDataNfe()) ? DateUtil.formatToSAP(romaneio.getDataNfe()) : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_HNFE.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getDataNfe()) ? DateUtil.formatHoraSemCaracteres(romaneio.getDataNfe()) : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_LOGNFE.name(), String.valueOf(seq++), StringUtil.isNotNullEmpty(romaneio.getNumeroLog()) ? romaneio.getNumeroLog() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_NANFE.name(), String.valueOf(seq++), StringUtil.isNotNullEmpty(romaneio.getNumeroAleatorioChaveAcesso()) ? romaneio.getNumeroAleatorioChaveAcesso() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_DVNFE.name(), String.valueOf(seq++), StringUtil.isNotNullEmpty(romaneio.getDigitoVerificadorNfe()) ? romaneio.getDigitoVerificadorNfe() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_VTNFE.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getValorTotalNfe()) ? romaneio.getValorTotalNfe().toString() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_PNFE.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getPesoTotalNfe()) ? romaneio.getPesoTotalNfe().toString() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_BSMNG.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getMedidaNfe()) ? romaneio.getMedidaNfe() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_CHNFE.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getChaveAcessoNfe()) ? romaneio.getChaveAcessoNfe() : StringUtil.empty()));

        seq = this.montarPesagemOrigem(romaneio, requestKey, filters, seq);

        if (romaneio.isPossuiDadosOrigem()) {
            seq = this.montarClassificacaoOrigem(romaneio.getItensClassificacaoOrigem(), requestKey, filters, seq);
        }

        int grupo = 1;

        seq = this.montarObservacao(romaneio, requestKey, filters, seq, grupo);

        this.montarObservacaoNota(romaneio, requestKey, filters, seq, grupo);

        return filters;
    }
}
