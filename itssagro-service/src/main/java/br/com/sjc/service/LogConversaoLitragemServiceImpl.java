package br.com.sjc.service;

import br.com.sjc.fachada.service.LogConversaoLitragemService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.LogConversaoLitragem;
import br.com.sjc.modelo.dto.aprovacaoDocumento.LogConversaoLitragemListagemDTO;
import br.com.sjc.persistencia.dao.LogConversaoLitragemDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogConversaoLitragemServiceImpl extends ServicoGenerico<Long, LogConversaoLitragem> implements LogConversaoLitragemService {

    @Autowired
    private LogConversaoLitragemDAO dao;

    @Override
    public LogConversaoLitragemDAO getDAO() {

        return this.dao;
    }

    @Override
    public Class<LogConversaoLitragemListagemDTO> getDtoListagem() {

        return LogConversaoLitragemListagemDTO.class;
    }

}
