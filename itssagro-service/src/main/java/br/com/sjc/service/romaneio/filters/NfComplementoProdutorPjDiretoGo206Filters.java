package br.com.sjc.service.romaneio.filters;

import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.sap.ItemNFPedido;
import br.com.sjc.modelo.sap.ItensClassificacao;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.request.EventInRequest;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class NfComplementoProdutorPjDiretoGo206Filters extends RomaneioFilters {

    @Override
    public JCoRequest instace(final Romaneio romaneio, final String centro, final String requestKey, final DadosSincronizacao dadosSincronizacao, final String rfcProgramID) {

        return EventInRequest.instance(dadosSincronizacao, centro, rfcProgramID, this.getFilters(romaneio, requestKey, centro, dadosSincronizacao), false, null);
    }

    private List<EventInRequestItem> getFilters(final Romaneio romaneio, final String requestKey, final String centro, DadosSincronizacao dadosSincronizacao) {

        final List<EventInRequestItem> filters = new LinkedList<EventInRequestItem>();

        int seq = 1;

        String pattern = ERM;

        String requests[] = requestKey.split(pattern);

        String request = requests[0];

        int countRequest = 0;

        Map<String, List<ItemNFPedido>> itensPorChaveMap = romaneio.getItens().stream().collect(Collectors.groupingBy(ItemNFPedido::getChaveAcessoNfe));

        for (String chave : itensPorChaveMap.keySet()) {

            countRequest = countRequest + DEZ;

            String novaRequest = montarRequestKeyPorMap(requestKey, pattern, requests, request, countRequest, itensPorChaveMap);

            final List<ItemNFPedido> itensChave = itensPorChaveMap.get(chave);

            int codItem = 0;

            seq = this.montarBridgeParaUsuarioCriadorRomaneio(romaneio, novaRequest, filters, seq);

            final ItemNFPedido cabecalho = itensChave.get(0);

            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_CENTRO.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getCentro()) ? romaneio.getCentro().getCodigo() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_MATERIAL.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(cabecalho.getMaterial()) ? cabecalho.getMaterial().getCodigo() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_DEPOS.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(cabecalho.getDeposito()) ? cabecalho.getDeposito().getCodigo() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_SAFRA.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(cabecalho.getSafra()) ? cabecalho.getSafra().getCodigo() : StringUtil.empty()));

            BigDecimal descontos = BigDecimal.ZERO;

            if (ObjetoUtil.isNotNull(romaneio.getClassificacao()) && ColecaoUtil.isNotEmpty(romaneio.getClassificacao().getItens())) {

                descontos = romaneio.getClassificacao().getItens().stream().map(ItensClassificacao::getDescontoValido).reduce(BigDecimal.ZERO, BigDecimal::add);
            }

            for (ItemNFPedido item : itensChave) {

                codItem = codItem + 1;

                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_ITEM.name(), String.valueOf(seq++), String.valueOf(codItem), String.valueOf(countRequest / DEZ)));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_ROMAN.name(), String.valueOf(seq++), String.valueOf(codItem), romaneio.getNumeroRomaneio()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_DATA.name(), String.valueOf(seq++), String.valueOf(codItem), DateUtil.formatToSAP(DateUtil.hoje())));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_CENTRO1.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(romaneio.getCentroDestino()) ? romaneio.getCentroDestino().getCodigo() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_OPERA.name(), String.valueOf(seq++), String.valueOf(codItem), romaneio.getOperacao().getOperacao()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PEDIDO.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(item.getNumeroPedido()) ? item.getNumeroPedido() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_ITEMP.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(item.getItemPedido()) ? item.getItemPedido() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PRODUT.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getProdutor()) ? item.getProdutor().getCodigo() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_VALORP.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getValorUnitarioPedido()) ? item.getValorUnitarioPedido().toString() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_SALDO.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getSaldoPedido()) ? item.getSaldoPedido().toString() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_TRANSP.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getTransportadora()) ? item.getTransportadora().getCodigo() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_MOTORIST.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getMotorista()) ? item.getMotorista().getCodigo() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PLACAC.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getPlacaCavalo()) ? StringUtil.toPlaca(item.getPlacaCavalo().getPlaca1()) : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_UF.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getPlacaCavalo()) && ObjetoUtil.isNotNull(item.getPlacaCavalo().getUfPlaca1()) ? item.getPlacaCavalo().getUfPlaca1().getSigla() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PLACA1.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getPlacaCavaloUm()) ? StringUtil.removerCaracteresEspeciais(item.getPlacaCavaloUm().getPlaca1()) : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PLACA2.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getPlacaCavaloDois()) ? StringUtil.removerCaracteresEspeciais(item.getPlacaCavaloDois().getPlaca1()) : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_TIPOCAP.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getTipoVeiculo()) ? item.getTipoVeiculo().getCodigo() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_TIPOFRE.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getTipoFrete()) ? item.getTipoFrete().name() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_NUMNFE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(item.getNumeroNfe()) ? item.getNumeroNfe() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_SERIENFE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(item.getSerieNfe()) ? item.getSerieNfe() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_DNFE.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getDataNfe()) ? DateUtil.formatToSAP(item.getDataNfe()) : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_HNFE.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getDataNfe()) ? DateUtil.formatHoraSemCaracteres(item.getDataNfe()) : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_VNFE.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getValorUnitarioNfe()) ? item.getValorUnitarioNfe().toString() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PNFE.name(), String.valueOf(seq++), String.valueOf(codItem), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getPesoTotalNfe()) ? item.getPesoTotalNfe().toString() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_LOGNFE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(item.getNumeroLog()) ? item.getNumeroLog() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_NANFE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(item.getNumeroAleatorioChaveAcesso()) ? item.getNumeroAleatorioChaveAcesso() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_DVNFE.name(), String.valueOf(seq++), String.valueOf(codItem), StringUtil.isNotNullEmpty(item.getDigitoVerificadorNfe()) ? item.getDigitoVerificadorNfe() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_VTNFE.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getValorTotalNfe()) ? item.getValorTotalNfe().toString() : StringUtil.empty()));

                if (romaneio.getOperacao().isPossuiPesagem()) {

                    seq = this.mapeamentoBridgeParaRateio(romaneio, filters, seq, novaRequest, codItem, item, descontos);
                }
            }

            if (romaneio.isPossuiDadosOrigem()) {

                seq = this.montarPesagemOrigem(romaneio, novaRequest, filters, seq);

                seq = this.montarClassificacaoOrigem(romaneio.getItensClassificacaoOrigem(), novaRequest, filters, seq);
            }

            int grupo = 1;

            seq = this.montarObservacao(romaneio, novaRequest, filters, seq, grupo);

            grupo++;

            this.montarObservacaoNota(romaneio, novaRequest, filters, seq, grupo);

            adicionarRequestParaRomaneioComDivisao(romaneio, countRequest, novaRequest);
        }

        return filters;
    }
}
