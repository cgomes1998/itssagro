package br.com.sjc.service;

import br.com.sjc.fachada.rest.paginacao.Pagina;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.CampoOperacaoMaterialService;
import br.com.sjc.fachada.service.ConfiguracaoUnidadeMedidaMaterialService;
import br.com.sjc.fachada.service.MaterialService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.AnaliseQualidade_;
import br.com.sjc.modelo.cfg.CampoOperacaoMaterial;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.dto.MaterialDTO;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import br.com.sjc.modelo.sap.ConfiguracaoUnidadeMedidaMaterial;
import br.com.sjc.modelo.sap.ConversaoLitragem_;
import br.com.sjc.modelo.sap.LocalCarregamento_;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.modelo.sap.request.EventOutRequest;
import br.com.sjc.modelo.sap.request.item.EventOutRequestItem;
import br.com.sjc.modelo.sap.sync.SAPMaterial;
import br.com.sjc.persistencia.dao.MaterialDAO;
import br.com.sjc.persistencia.dao.fachada.MaterialCustomRepository;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;
import java.util.logging.Logger;

@Service
public class MaterialServiceImpl extends ServicoGenerico<Long, Material> implements MaterialService {

    private static final Logger LOGGER = Logger.getLogger(MaterialServiceImpl.class.getName());

    @Autowired
    private MaterialDAO dao;

    @Autowired
    private MaterialCustomRepository materialCustomRepository;

    @Autowired
    private CampoOperacaoMaterialService campoOperacaoMaterialService;

    @Autowired
    private ConfiguracaoUnidadeMedidaMaterialService configuracaoUnidadeMedidaMaterialService;

    @Transactional(readOnly = true)
    @Override
    public List<ConfiguracaoUnidadeMedidaMaterial> obterConfiguracoesUnidadesMedidas(String codigoMaterial) {

        return configuracaoUnidadeMedidaMaterialService.obterConfiguracoesUnidadesMedidas(codigoMaterial);
    }

    private void montarConfiguracaoDeCampos(Material material) {

        List<CampoOperacaoMaterial> campos = new ArrayList<>();

        campos.add(new CampoOperacaoMaterial(DadosSincronizacaoEnum.ANALISE, false, "Lacre de amostra", campoOperacaoMaterialService.obterCampo(AnaliseQualidade_.lacreAmostra.getName()), material));

        campos.add(new CampoOperacaoMaterial(DadosSincronizacaoEnum.ANALISE, false, "Lote", campoOperacaoMaterialService.obterCampo("loteAnaliseQualidade.codigo"), material));

        campos.add(new CampoOperacaoMaterial(DadosSincronizacaoEnum.ANALISE, false, "Dt. Fabricação de Lote", campoOperacaoMaterialService.obterCampo("loteAnaliseQualidade.dataFabricacao"), material));

        campos.add(new CampoOperacaoMaterial(DadosSincronizacaoEnum.ANALISE, false, "Dt. Vencimento de Lote", campoOperacaoMaterialService.obterCampo("loteAnaliseQualidade.dataVencimento"), material));

        campos.add(new CampoOperacaoMaterial(DadosSincronizacaoEnum.ANALISE, false, "Assinatura", campoOperacaoMaterialService.obterCampo(AnaliseQualidade_.assinatura.getName()), material));

        campos.add(new CampoOperacaoMaterial(DadosSincronizacaoEnum.ANALISE, false, "Local de carregamento", campoOperacaoMaterialService.obterCampo(LocalCarregamento_.identificador.getName()), material));

        campos.add(new CampoOperacaoMaterial(DadosSincronizacaoEnum.ANALISE, false, "Dt. início do carregamento", campoOperacaoMaterialService.obterCampo(LocalCarregamento_.dataInicioDoCarregamento.getName()), material));

        campos.add(new CampoOperacaoMaterial(DadosSincronizacaoEnum.ANALISE, false, "Dt. fim do carregamento", campoOperacaoMaterialService.obterCampo(LocalCarregamento_.dataFimDoCarregamento.getName()), material));

        campos.add(new CampoOperacaoMaterial(DadosSincronizacaoEnum.ANALISE, true, false, "Grau I.N.P.M", campoOperacaoMaterialService.obterCampo(ConversaoLitragem_.grauInpm.getName()), material));

        material.setCampos(new ArrayList<>());

        material.getCampos().addAll(campos);
    }

    @Override
    public Specification<Material> obterEspecification(final Paginacao<Material> paginacao) {

        final Material entidade = paginacao.getEntidade();

        final Specification<Material> specification = new Specification<Material>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<Material> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                if (StringUtil.isNotNullEmpty(entidade.getCodigo())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("codigo")), "%" + entidade.getCodigo().toUpperCase() + "%")));
                }

                if (StringUtil.isNotNullEmpty(entidade.getDescricao())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("descricao")), "%" + entidade.getDescricao().toUpperCase() + "%")));
                }

                if (ObjetoUtil.isNotNull(entidade.getStatusRegistro())) {

                    predicates.add(builder.equal(root.get("statusRegistro"), entidade.getStatusRegistro()));
                }

                if (ObjetoUtil.isNotNull(entidade.getStatusCadastroSap())) {

                    predicates.add(builder.equal(root.get("statusCadastroSap"), entidade.getStatusCadastroSap()));
                }

                query.orderBy(builder.asc(root.get("codigo")));

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Override
    public void salvarSAP(final EventOutRequest request, final String host, final String mandante) {

        if (ColecaoUtil.isNotEmpty(request.getData())) {

            final Map<String, SAPMaterial> groupMaterial = this.groupMaterial(request);

            groupMaterial.keySet().forEach(grupo -> {

                try {

                    final SAPMaterial material = groupMaterial.get(grupo);

                    Material entidadeSaved = this.dao.findFirstByCodigo(material.getCodigo());

                    entidadeSaved = material.copy(entidadeSaved);

                    if (StringUtil.isNotNullEmpty(entidadeSaved.getCodigo())) {

                        this.salvar(entidadeSaved);
                    }

                } catch (Exception e) {

                    e.printStackTrace();
                }
            });
        }
    }

    private Map<String, SAPMaterial> groupMaterial(final EventOutRequest request) {

        final Map<String, SAPMaterial> groupMaterial = new HashMap<>();

        for (final EventOutRequestItem registro : request.getData()) {

            SAPMaterial entidade = groupMaterial.get(registro.getItemDocumento().trim());

            if (ObjetoUtil.isNull(entidade)) {

                entidade = SAPMaterial.newInstance();
            }

            try {

                entidade.update(registro.getCampo().trim(), registro.getValor().trim());

                groupMaterial.put(registro.getItemDocumento().trim(), entidade);

            } catch (final Exception e) {

                e.printStackTrace();
            }
        }

        return groupMaterial;
    }

    @Override
    public List<Material> listarSincronizados(final Date data) {

        return ObjetoUtil.isNull(data) ? this.dao.findByStatusCadastroSapIn(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO})) :
                this.dao.findByStatusCadastroSapInAndDataCadastroGreaterThanEqual(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO}), data);
    }

    @Override
    public void sincronizar(final List<Material> entidades) {

        if (ColecaoUtil.isNotEmpty(entidades)) {

            entidades.forEach(item -> {

                Material entidadeSaved = this.getDAO().findFirstByCodigo(item.getCodigo());

                item.setId(null);

                if (ObjetoUtil.isNotNull(entidadeSaved)) {

                    item.setId(entidadeSaved.getId());
                }
            });

            this.salvar(entidades);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<MaterialDTO> listarMateriaisAtivos() {

        return materialCustomRepository.listarPorStatus(StatusRegistroEnum.ATIVO);
    }

    @Override
    public Material obterPorCodigo(final String codigo) {

        return this.getDAO().findFirstByCodigo(codigo);
    }

    @Override
    public Pagina<?> listarComPaginacao(final Paginacao<MaterialDTO> paginacao) {

        return this.dtoListar(paginacao);
    }

    private Specification<Material> obterEspecificacaoAnalise(final Paginacao<MaterialDTO> paginacao) {

        return (Root<Material> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) -> {
            final List<Predicate> predicates = new ArrayList<>();
            if (paginacao.getEntidade().getId() != null) {
                predicates.add(builder.and(builder.equal(root.get("id"), paginacao.getEntidade().getId())));
            }

            query.distinct(true);
            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }

    @Override
    public void preSalvar(Material entidade) {

        super.preSalvar(entidade);

        if (ColecaoUtil.isEmpty(entidade.getCampos())) {

            montarConfiguracaoDeCampos(entidade);
        }

        if (ColecaoUtil.isNotEmpty(entidade.getCampos())) {

            entidade.getCampos().forEach(i -> i.setMaterial(entidade));
        }

        if (ColecaoUtil.isNotEmpty(entidade.getConversoes())) {

            entidade.getConversoes().forEach(c -> c.setMaterial(entidade));
        }
    }

    @Override
    public MaterialDAO getDAO() {

        return this.dao;
    }

    @Override
    public Class<? extends DataDTO> getDtoListagem() {

        return MaterialDTO.class;
    }

    @Override
    public Boolean getRegistrarLogAtividade() {

        return Boolean.TRUE;
    }

}
