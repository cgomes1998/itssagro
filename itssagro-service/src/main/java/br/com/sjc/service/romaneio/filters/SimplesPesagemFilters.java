package br.com.sjc.service.romaneio.filters;

import br.com.sjc.fachada.servico.impl.ServiceInject;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.dto.PesagemHistoricoDTO;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.request.EventInRequest;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class SimplesPesagemFilters extends RomaneioFilters {

	@Override
	public JCoRequest instace(
		   final Romaneio romaneio,
		   final String centro,
		   final String requestKey,
		   final DadosSincronizacao dadosSincronizacao,
		   final String rfcProgramID
	) {

		return EventInRequest.instance(
			   dadosSincronizacao,
			   centro,
			   rfcProgramID,
			   this.getFilters(romaneio,
					requestKey,
					centro
			   ),
			   false,
			   null
		);
	}

	private List<EventInRequestItem> getFilters(
		   final Romaneio romaneio,
		   final String requestKey,
		   final String centro
	) {

		final List<EventInRequestItem> filters = new LinkedList<EventInRequestItem>();

		Integer seq = 0;

		seq = this.montarBridgeParaUsuarioCriadorRomaneio(
			   romaneio,
			   requestKey,
			   filters,
			   seq
		);

		filters.add(EventInRequestItem.instance(
			   requestKey,
			   SAPRomaneio.BRIDGES.C_ROMAN.name(),
			   String.valueOf(seq++),
			   romaneio.getNumeroRomaneio()
		));

		filters.add(EventInRequestItem.instance(
			   requestKey,
			   SAPRomaneio.BRIDGES.C_OPERA.name(),
			   String.valueOf(seq++),
			   romaneio.getOperacao().getOperacao()
		));

		filters.add(EventInRequestItem.instance(
			   requestKey,
			   SAPRomaneio.BRIDGES.C_SAFRA.name(),
			   String.valueOf(seq++),
			   ObjetoUtil.isNotNull(romaneio.getSafra()) ? romaneio.getSafra().getCodigo() : StringUtil.empty()
		));

		filters.add(EventInRequestItem.instance(
			   requestKey,
			   SAPRomaneio.BRIDGES.C_DATUM.name(),
			   String.valueOf(seq++),
			   DateUtil.formatToSAP(DateUtil.hoje())
		));

		filters.add(EventInRequestItem.instance(
			   requestKey,
			   SAPRomaneio.BRIDGES.C_OPERA.name(),
			   String.valueOf(seq++),
			   ObjetoUtil.isNotNull(romaneio.getOperacao()) ? romaneio.getOperacao().getOperacao() : StringUtil.empty()
		));

		filters.add(EventInRequestItem.instance(
			   requestKey,
			   SAPRomaneio.BRIDGES.C_WERKS.name(),
			   String.valueOf(seq++),
			   centro
		));

		filters.add(EventInRequestItem.instance(
			   requestKey,
			   SAPRomaneio.BRIDGES.C_PL_CAV.name(),
			   String.valueOf(seq++),
			   ObjetoUtil.isNotNull(romaneio.getPlacaCavalo()) ? StringUtil.toPlaca(romaneio.getPlacaCavalo().getPlaca1()) : StringUtil.empty()
		));

		filters.add(EventInRequestItem.instance(
			   requestKey,
			   SAPRomaneio.BRIDGES.C_UF.name(),
			   String.valueOf(seq++),
			   ObjetoUtil.isNotNull(romaneio.getPlacaCavalo()) && ObjetoUtil.isNotNull(romaneio.getPlacaCavalo().getUfPlaca1()) ? romaneio.getPlacaCavalo().getUfPlaca1().getSigla() : StringUtil.empty()
		));

		filters.add(EventInRequestItem.instance(
			   requestKey,
			   SAPRomaneio.BRIDGES.C_REGIO.name(),
			   String.valueOf(seq++),
			   ObjetoUtil.isNotNull(romaneio.getPlacaCavalo()) && ObjetoUtil.isNotNull(romaneio.getPlacaCavalo().getUfPlaca1()) ? romaneio.getPlacaCavalo().getUfPlaca1().getSigla() : StringUtil.empty()
	      ));

		filters.add(EventInRequestItem.instance(
			   requestKey,
			   SAPRomaneio.BRIDGES.C_PL_CAR1.name(),
			   String.valueOf(seq++),
			   ObjetoUtil.isNotNull(romaneio.getPlacaCavaloUm()) ? romaneio.getPlacaCavaloUm().getPlaca1() : StringUtil.empty()
		));

		filters.add(EventInRequestItem.instance(
			   requestKey,
			   SAPRomaneio.BRIDGES.C_PL_CAR2.name(),
			   String.valueOf(seq++),
			   ObjetoUtil.isNotNull(romaneio.getPlacaCavaloDois()) ? romaneio.getPlacaCavaloDois().getPlaca1() : StringUtil.empty()
		));

		filters.add(EventInRequestItem.instance(
			   requestKey,
			   SAPRomaneio.BRIDGES.C_MOTORIST.name(),
			   String.valueOf(seq++),
			   ObjetoUtil.isNotNull(romaneio.getMotorista()) ? romaneio.getMotorista().getCodigo() : StringUtil.empty()
		));

		filters.add(EventInRequestItem.instance(
			   requestKey,
			   SAPRomaneio.BRIDGES.C_TEXT3.name(),
			   String.valueOf(seq++),
			   String.valueOf(seq),
			   ObjetoUtil.isNotNull(romaneio.getMotorista()) ? romaneio.getMotorista().getNome() : StringUtil.empty()
		));

		filters.add(EventInRequestItem.instance(
			   requestKey,
			   SAPRomaneio.BRIDGES.C_NOME.name(),
			   String.valueOf(seq++),
			   StringUtil.isNotNullEmpty(romaneio.getNomeSimplesPesagem()) ? romaneio.getNomeSimplesPesagem() : StringUtil.empty()
		));

		filters.add(EventInRequestItem.instance(
			   requestKey,
			   SAPRomaneio.BRIDGES.C_PRODUT.name(),
			   String.valueOf(seq++),
			   StringUtil.isNotNullEmpty(romaneio.getMaterialSimplesPesagem()) ? romaneio.getMaterialSimplesPesagem() : StringUtil.empty()
		));

		filters.add(EventInRequestItem.instance(
			   requestKey,
			   SAPRomaneio.BRIDGES.C_TRANSP_C.name(),
			   String.valueOf(seq++),
			   ObjetoUtil.isNotNull(romaneio.getTransportadora()) ? romaneio.getTransportadora()
			                                                                .getCodigo() : StringUtil.empty()
		));

		filters.add(EventInRequestItem.instance(
			   requestKey,
			   SAPRomaneio.BRIDGES.C_TRANSP_N.name(),
			   String.valueOf(seq++),
			   ObjetoUtil.isNotNull(romaneio.getTransportadora()) ? romaneio.getTransportadora()
			                                                                .getDescricao() : StringUtil.empty()
		));

		BigDecimal liquidoSeco = BigDecimal.valueOf(0);

		if (ObjetoUtil.isNotNull(romaneio.getPesagem())) {

			if (romaneio.getPesagem().getPesoInicial().compareTo(romaneio.getPesagem().getPesoFinal()) > 0) {

				filters.add(EventInRequestItem.instance(
					   requestKey,
					   SAPRomaneio.SIMPLES_PESAGEM.C_PESO_PSB.name(),
					   String.valueOf(seq++),
					   romaneio.getPesagem().getPesoInicial().toString()
				));

				filters.add(EventInRequestItem.instance(
					   requestKey,
					   SAPRomaneio.SIMPLES_PESAGEM.C_PESO_TAR.name(),
					   String.valueOf(seq++),
					   romaneio.getPesagem().getPesoFinal().toString()
				));

			} else {

				filters.add(EventInRequestItem.instance(
					   requestKey,
					   SAPRomaneio.SIMPLES_PESAGEM.C_PESO_PSB.name(),
					   String.valueOf(seq++),
					   romaneio.getPesagem().getPesoFinal().toString()
				));

				filters.add(EventInRequestItem.instance(
					   requestKey,
					   SAPRomaneio.SIMPLES_PESAGEM.C_PESO_TAR.name(),
					   String.valueOf(seq++),
					   romaneio.getPesagem().getPesoInicial().toString()
				));
			}

			PesagemHistoricoDTO historicoPrimeiraPesagem = ServiceInject.pesagemService.obterPrimeiraPesagemDoRomaneio(romaneio.getPesagem().getId());

	            filters.add(EventInRequestItem.instance(
				requestKey,
				SAPRomaneio.SIMPLES_PESAGEM.C_DATA_PSB.name(),
				String.valueOf(seq++),
				ObjetoUtil.isNotNull(historicoPrimeiraPesagem) ? DateUtil.formatToSAP(historicoPrimeiraPesagem.getDataCadastro()) : StringUtil.empty()
	            ));

			filters.add(EventInRequestItem.instance(
				   requestKey,
				   SAPRomaneio.SIMPLES_PESAGEM.C_HORA_PSB.name(),
				   String.valueOf(seq++),
				   ObjetoUtil.isNotNull(historicoPrimeiraPesagem) ? DateUtil.formatHoraSemCaracteres(historicoPrimeiraPesagem.getDataCadastro()) : StringUtil.empty()
			));

	            if (ObjetoUtil.isNotNull(romaneio.getPesagem().getUsuarioPesoFinal())) {

	                filters.add(EventInRequestItem.instance(
	                	   requestKey,
		                   SAPRomaneio.SIMPLES_PESAGEM.C_USUA_PSB.name(),
		                   String.valueOf(seq++),
		                   romaneio.getPesagem().getUsuarioPesoFinal().getLogin().concat("-").concat(romaneio.getPesagem().getUsuarioPesoFinal().getNome())
	                ));
	            }

			PesagemHistoricoDTO historicoSegundaPesagem = ServiceInject.pesagemService.obterSegundaPesagemDoRomaneio(romaneio.getPesagem().getId());

	            filters.add(EventInRequestItem.instance(
	            	   requestKey,
		               SAPRomaneio.SIMPLES_PESAGEM.C_DATA_PST.name(),
		               String.valueOf(seq++),
		               ObjetoUtil.isNotNull(historicoSegundaPesagem) ? DateUtil.formatToSAP(historicoSegundaPesagem.getDataCadastro()) : StringUtil.empty()
	            ));

	            filters.add(EventInRequestItem.instance(
	            	   requestKey,
		               SAPRomaneio.SIMPLES_PESAGEM.C_HORA_PST.name(),
		               String.valueOf(seq++),
		               ObjetoUtil.isNotNull(historicoSegundaPesagem) ? DateUtil.formatHoraSemCaracteres(historicoSegundaPesagem.getDataCadastro()) : StringUtil.empty()
	            ));

	            if (ObjetoUtil.isNotNull(romaneio.getPesagem().getUsuarioPesoInicial())) {

	                filters.add(EventInRequestItem.instance(
				requestKey,
				SAPRomaneio.SIMPLES_PESAGEM.C_USUA_PST.name(),
				String.valueOf(seq++),
				romaneio.getPesagem().getUsuarioPesoInicial().getLogin().concat("-").concat(romaneio.getPesagem().getUsuarioPesoInicial().getNome())
	                ));
	            }

			liquidoSeco = (romaneio.getPesagem().getPesoInicial().subtract(romaneio.getPesagem().getPesoFinal()));
		}

		filters.add(EventInRequestItem.instance(
			   requestKey,
			   SAPRomaneio.SIMPLES_PESAGEM.C_PESOLS.name(),
			   String.valueOf(seq++),
			   liquidoSeco.toString()
		));

		filters.add(EventInRequestItem.instance(
			   requestKey,
			   SAPRomaneio.SIMPLES_PESAGEM.C_QUANT.name(),
			   String.valueOf(seq++),
			   liquidoSeco.toString()
		));

		List<String> listaObs = converterStringFormatoSAP(romaneio.getObservacao());

		int contadorItensObs = 1;

		int grupo = 1;

		for (String obs : listaObs) {
			filters.add(EventInRequestItem.instance(
				   requestKey,
				   SAPRomaneio.BRIDGES.C_TEXTOB.name(),
				   String.valueOf(seq++),
				   String.valueOf(contadorItensObs++),
				   String.valueOf(grupo),
				   obs
			));
		}

		adicionarRequestRomaneioSemDivisao(
			   romaneio,
			   requestKey
		);

		return filters;
	}

}
