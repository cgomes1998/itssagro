package br.com.sjc.service.romaneio.filters;

import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.enums.OrigemClassificacaoEnum;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.request.EventInRequest;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;

import java.util.LinkedList;
import java.util.List;

public class NotaFiscalTransporteFilters extends RomaneioFilters {

    @Override
    public JCoRequest instace(final Romaneio romaneio, final String centro, final String requestKey, final DadosSincronizacao dadosSincronizacao, final String rfcProgramID) {

        return EventInRequest.instance(dadosSincronizacao, centro, rfcProgramID, this.getFilters(romaneio, requestKey, centro), false, null);
    }

    private List<EventInRequestItem> getFilters(final Romaneio romaneio, final String requestKey, final String centro) {

        final List<EventInRequestItem> filters = new LinkedList<EventInRequestItem>();

        Integer seq = 0;

        seq = this.montarBridgeParaUsuarioCriadorRomaneio(romaneio, requestKey, filters, seq);
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_BRANCH.name(), String.valueOf(seq++), centro));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_SHPMRK.name(), String.valueOf(seq++), romaneio.getNumeroRomaneio()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_OPERA.name(), String.valueOf(seq++), romaneio.getOperacao().getOperacao()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_DATA.name(), String.valueOf(seq++), DateUtil.formatToSAP(DateUtil.hoje())));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_DOCDAT.name(), String.valueOf(seq++), DateUtil.formatToSAP(romaneio.getDataCadastro())));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_PARID.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getProdutor()) ? romaneio.getProdutor().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_INCO1.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getTipoFrete()) ? romaneio.getTipoFrete().name() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_SAFRA.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getSafra()) ? romaneio.getSafra().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_MATNR.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getMaterial()) ? romaneio.getMaterial().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_PLACA.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getPlacaCavalo()) ? StringUtil.toPlaca(romaneio.getPlacaCavalo().getPlaca1()) : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_UF.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getPlacaCavalo()) && ObjetoUtil.isNotNull(romaneio.getPlacaCavalo().getUfPlaca1()) ? romaneio.getPlacaCavalo().getUfPlaca1().getSigla() : StringUtil.empty()));

        if (OrigemClassificacaoEnum.ORIGEM.equals(romaneio.getLocalPesagem())) {

            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_MENGE.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getQuantidadePesoOrigem()) ? romaneio.getQuantidadePesoOrigem().toString() : StringUtil.empty()));

        } else {

            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_MENGE.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getTipoVeiculo()) ? romaneio.getTipoVeiculo().getPeso().toString() : StringUtil.empty()));
        }

        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_MOTORIST.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getMotorista()) ? romaneio.getMotorista().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_TRANSP.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getTransportadora()) ? romaneio.getTransportadora().getCodigo() : StringUtil.empty()));

        int grupo = 1;

        seq = this.montarObservacao(romaneio, requestKey, filters, seq, grupo);

        grupo++;

        this.montarObservacaoNota(romaneio, requestKey, filters, seq, grupo);

        adicionarRequestRomaneioSemDivisao(romaneio, requestKey);

        return filters;
    }

}
