package br.com.sjc.service;

import br.com.sjc.arquitetura.providers.RFCMultipleProviders;
import br.com.sjc.fachada.excecoes.*;
import br.com.sjc.fachada.rest.paginacao.Ordenacao;
import br.com.sjc.fachada.rest.paginacao.Pagina;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.*;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.*;
import br.com.sjc.modelo.cfg.*;
import br.com.sjc.modelo.dto.*;
import br.com.sjc.modelo.dto.Arquivo;
import br.com.sjc.modelo.dto.aprovacaoDocumento.ItemNFPedidoAprovacaoDocumento;
import br.com.sjc.modelo.dto.aprovacaoDocumento.RomaneioAprovacaoDocumento;
import br.com.sjc.modelo.dto.aprovacaoDocumento.TabelaClassificacaoAprovacaoDocumento;
import br.com.sjc.modelo.dto.aprovacaoDocumento.UsuarioAprovacaoDocumento;
import br.com.sjc.modelo.enums.*;
import br.com.sjc.modelo.relatorio.GerarRelatorioExcel;
import br.com.sjc.modelo.response.*;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoCallerService;
import br.com.sjc.modelo.sap.email.EmailRomaneioBloqueadoPorIndiceClassificacao;
import br.com.sjc.modelo.sap.request.RfcZITSSAGRO_DIVERG_PESO_ROMANEIORequest;
import br.com.sjc.modelo.sap.request.item.RfcStatusRomaneioRequestItem;
import br.com.sjc.modelo.sap.response.RfcZITSSAGRO_DIVERG_PESO_ROMANEIOResponse;
import br.com.sjc.modelo.sap.response.item.RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_DADOS_Item;
import br.com.sjc.modelo.util.RomaneioUtil;
import br.com.sjc.persistencia.dao.LaudoDAO;
import br.com.sjc.persistencia.dao.ProdutorDAO;
import br.com.sjc.persistencia.dao.RomaneioDAO;
import br.com.sjc.persistencia.dao.RomaneioRequestKeyDAO;
import br.com.sjc.persistencia.dao.fachada.ItemNFPedidoCustomRepository;
import br.com.sjc.persistencia.dao.fachada.ItensClassificacaoCustomRepository;
import br.com.sjc.persistencia.dao.fachada.RomaneioCustomRepository;
import br.com.sjc.util.*;
import br.com.sjc.util.bundle.MessageSupport;
import br.com.sjc.util.rest.MapBuilder;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.NonUniqueResultException;
import javax.persistence.criteria.*;
import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.ws.rs.core.Context;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static br.com.sjc.fachada.rest.BaseEndpoint.TOKEN_USER;

@Slf4j
@Service
public class RomaneioServiceImpl extends ServicoGenerico<Long, Romaneio> implements RomaneioService {

    public static final String EXISTE_ROMANEIO_VINCULADO_A_ESSA_SENHA = "MSGE036";

    private static final String NFE_DE_TRANSPORTE_STATUS_DIFERENTE_100 = "MSGE009";

    private static final String NFE_DE_TRANSPORTE_JA_ESTA_SENDO_UTILIZADA = "MSGE010";

    private static final String NUMERO_INFORMADO_NAO_ENCONTRADO = "MSGE011";

    private static final String NUMERO_INFORMADO_UTILIZADO = "MSGE016";

    private static final String NFE_JA_FOI_ESCRITURADA = "MSGE012";

    private static final String STATUS_NFE_OK = "100";

    private static final String REL_ROMANEIO_JASPER = "jasper/rel_romaneio.jasper";

    private static final String REL_ROMANEIO_CLASSIFICACAO_ORIGEM_JASPER = "jasper/rel_romaneio_sub_report_classificacao_origem.jasper";

    private static final String REL_ROMANEIO_CLASSIFICACAO_DESTINO_JASPER = "jasper/rel_romaneio_sub_report_classificacao_destino.jasper";

    private static final String REL_ROMANEIO_GP_JASPER = "jasper/gestao-patio/romaneio/rel_romaneio.jasper";

    private static final String LOGO_PNG = "img/logo.png";

    private static final String DEPOSITO_CODIGO_PORTAL_FORNECEDOR_GRAOS = "ASF3";

    private static final String NAO_FOI_POSSIVEL_REALIZAR_A_IMPRESSAO_DO_PDF = "Não foi possivel realizar a impressão do pdf.";

    @Context
    protected HttpServletRequest request;

    @Autowired
    private RomaneioRequestKeyDAO romaneioRequestKeyDAO;

    @Autowired
    private RomaneioDAO dao;

    @Autowired
    private SenhaService senhaService;

    @Autowired
    private DadosSincronizacaoService dadosSincronizacaoService;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private ParametroService parametroService;

    @Autowired
    private CentroService centroService;

    @Autowired
    private RetornoNFeService retornoNFeService;

    @Autowired
    private LacreService lacreService;

    @Autowired
    private FluxoEtapaService fluxoEtapaService;

    @Autowired
    private ItensClassificacaoCustomRepository itensClassificacaoCustomRepository;

    @Autowired
    private RomaneioCustomRepository romaneioCustomRepository;

    @Autowired
    private PesagemService pesagemService;

    @Autowired
    private EmailSenderServiceImpl emailSenderService;

    @Autowired
    private ConfiguracaoEmailService configuracaoEmailService;

    @Autowired
    private TabelaClassificacaoService tabelaClassificacaoService;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private SAPService sapService;

    @Autowired
    private ItemNFPedidoCustomRepository itemNFPedidoCustomRepository;

    @Autowired
    private LaudoDAO laudoDAO;

    @Autowired
    private TabelaValorService tabelaValorService;

    @Autowired
    private DepositoService depositoService;

    @Autowired
    private ProdutorDAO produtorDAO;

    @Autowired
    private ItemNFPedidoService itemNFPedidoService;

    @Autowired
    private ClassificacaoService classificacaoService;

    @Autowired
    private BridgeService bridgeService;

    @Autowired
    private CampoOperacaoService campoOperacaoService;

    @Autowired
    private ConversaoLitragemService conversaoLitragemService;

    @Autowired
    private DashboardFilaService dashboardFilaService;

    @Autowired
    private ProdutorService produtorService;

    @Autowired
    private RomaneioLogEnvioService romaneioLogEnvioService;

    @Override
    public RomaneioLogEnvio obterRomaneioLogEnvioPorId(Long id) {
        return romaneioLogEnvioService.obterPorRomaneioId(id);
    }

    @Override
    public Centro obterCentroLogado() {
        String codigo = this.parametroService.obterPorTipoParametro(TipoParametroEnum.CENTRO_SAP).getValor();
        return centroService.obterPorCodigo(codigo);
    }

    @Autowired
    private SaldoReservadoPedidoService saldoReservadoPedidoService;

    @Override
    public void atualizarDadosAtendimento(String numeroRomaneio, Integer numeroCartaoAcesso, Boolean atendido, Date dataAtendimento) {

        dao.updateRomaneioAtendido(atendido, dataAtendimento, numeroRomaneio, numeroCartaoAcesso);
    }

    @Override
    public List<ItemNFPedidoDTO> obterRomaneiosParaAutoatendimento() {

        return itemNFPedidoCustomRepository.obterItensDeRomaneioParaAutoatendimento();
    }

    @Override
    public List<ItemNFPedidoDTO> obterItensDeRomaneioParaEntradaLiberada() {

        return itemNFPedidoCustomRepository.obterItensDeRomaneioParaEntradaLiberada();
    }

    @Override
    public void simplesPersistencia(Romaneio romaneio) {

        this.salvar(romaneio);
    }

    @Override
    public Optional<Romaneio> obterRomaneioParaAutoatendimento(AutoAtendimentoMotorista autoAtendimentoMotorista) {

        return dao.findOne(((root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (StringUtil.isNotNullEmpty(autoAtendimentoMotorista.getNumeroCartaoDeAcesso())) {

                predicates.add(criteriaBuilder.equal(root.get(Romaneio_.numeroCartaoAcesso), Integer.parseInt(autoAtendimentoMotorista.getNumeroCartaoDeAcesso())));

                query.orderBy(criteriaBuilder.desc(root.get(Romaneio_.dataCadastro)));
            }

            if (StringUtil.isNotNullEmpty(autoAtendimentoMotorista.getNumeroRomaneio())) {

                predicates.add(criteriaBuilder.equal(root.get(Romaneio_.numeroRomaneio), autoAtendimentoMotorista.getNumeroRomaneio()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));
        }));
    }

    @Override
    public List<RfcStatusRomaneioRequestItem> listarRequestKeysDeRomaneiosPorId(final List<Long> idRomaneios) {

        return romaneioCustomRepository.listarRequestKeysDeRomaneiosPorId(idRomaneios);
    }

    @Override
    public List<RomaneioConversaoLitragemDTO> obterRomaneiosComConversaoLitragem(List<String> romaneios) {

        if (ColecaoUtil.isEmpty(romaneios)) {

            return null;
        }

        return romaneioCustomRepository.obterRomaneiosComConversaoLitragem(romaneios);
    }

    @Override
    public List<RfcStatusRomaneioRequestItem> obterRequestsDeRomaneiosSemNota(final List<Long> idRomaneios) {

        return romaneioCustomRepository.obterRequestsDeRomaneiosSemNota(idRomaneios);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional
    public void atualizarRequest(Long romaneioId) {

        Collection<RomaneioBridgeDTO> bridges = bridgeService.dtoListar(((root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.join(Bridge_.romaneio).get(Romaneio_.id), romaneioId)), RomaneioBridgeDTO.class);

        List<RomaneioEItemDTO> idsItens = romaneioCustomRepository.obterIdItens(bridges.stream().map(RomaneioBridgeDTO::getRomaneio).map(Romaneio::getId).collect(Collectors.toList()));

        dao.deletarTodasAsRequests(Arrays.asList(romaneioId));

        bridges.stream().forEach(bridge -> {

            if (ColecaoUtil.isEmpty(idsItens)) {

                Romaneio romaneio = new Romaneio();

                romaneio.setId(romaneioId);

                romaneio.setRequestKey(StringUtil.completarZeroAEsquerda(20, bridge.getRequestKey()));

                romaneioRequestKeyDAO.save(new RomaneioRequestKey(romaneio, romaneio.getRequestKey()));

                alterarAtributos(romaneio, Romaneio_.REQUEST_KEY);

            } else {

                idsItens.stream().filter(i -> !i.isRequestSalva() && i.getIdRomaneio().equals(bridge.getRomaneio().getId())).findFirst().ifPresent(itemDTO -> {

                    idsItens.stream().filter(i -> Objects.equals(i, itemDTO)).forEach(i -> i.setRequestSalva(true));

                    Romaneio romaneio = new Romaneio();

                    romaneio.setId(itemDTO.getIdRomaneio());

                    romaneioRequestKeyDAO.save(new RomaneioRequestKey(romaneio, StringUtil.completarZeroAEsquerda(20, bridge.getRequestKey())));

                    ItemNFPedido item = new ItemNFPedido();

                    item.setId(itemDTO.getIdItem());

                    item.setRequestKey(StringUtil.completarZeroAEsquerda(20, bridge.getRequestKey()));

                    itemNFPedidoService.alterarAtributos(item, ItemNFPedido_.REQUEST_KEY);
                });
            }
        });
    }

    @Override
    public List<PesagemManualDTO> obterPesagensManuais(final ItemNFPedido filtro) {

        return pesagemService.obterPesagensManuais(filtro);
    }

    @Override
    @Transactional
    public List<RelatorioComercialDTO> obterRelatorioComercial(RelatorioComercialFiltroDTO filtro) {

        List<RelatorioComercialDTO> byDataCadastroAndMaterialIdOrdOrderByIdDesc = romaneioCustomRepository.findByDataCadastroAndMaterialIdOrdOrderByIdDesc(filtro);

        if (ColecaoUtil.isNotEmpty(byDataCadastroAndMaterialIdOrdOrderByIdDesc)) {

            List<String> numeroRomaneios = byDataCadastroAndMaterialIdOrdOrderByIdDesc.stream().map(RelatorioComercialDTO::getNumeroRomaneio).distinct().collect(Collectors.toList());

            List<String> codigoOrdens = byDataCadastroAndMaterialIdOrdOrderByIdDesc.stream().map(RelatorioComercialDTO::getCodigoOrdemVenda).filter(o -> StringUtil.isNotNullEmpty(o)).distinct().collect(Collectors.toList());

            List<RestricaoOrdemDTO> restricaoOrdems = romaneioCustomRepository.getRestricoesDasOrdens(numeroRomaneios, codigoOrdens);

            List<PesagemHistoricoRomaneioDTO> pesagensBrutas = romaneioCustomRepository.obterQuantidadePesagensBrutasPorRomaneios(numeroRomaneios);

            if (ColecaoUtil.isNotEmpty(restricaoOrdems)) {

                byDataCadastroAndMaterialIdOrdOrderByIdDesc.stream().forEach(relatorioComercialDTO -> {
                    List<RestricaoOrdemDTO> restricoesDoItem =
                            restricaoOrdems.stream().filter(r -> r.getRomaneio().getNumeroRomaneio().equals(relatorioComercialDTO.getNumeroRomaneio()) || (r.getRomaneio().getNumeroRomaneio().equals(relatorioComercialDTO.getNumeroRomaneio()) && StringUtil.isNotNullEmpty(r.getCodigoOrdemVenda()) && StringUtil.isNotNullEmpty(relatorioComercialDTO.getCodigoOrdemVenda()) && relatorioComercialDTO.getCodigoOrdemVenda().equals(r.getCodigoOrdemVenda()))).collect(Collectors.toList());
                    if (ColecaoUtil.isNotEmpty(restricoesDoItem)) {
                        relatorioComercialDTO.setRestricoes(restricoesDoItem.stream().map(RestricaoOrdemDTO::getMensagem).filter(Objects::nonNull).collect(Collectors.joining(" | ")));
                    }
                });
            }

            if (ColecaoUtil.isNotEmpty(pesagensBrutas)) {
                byDataCadastroAndMaterialIdOrdOrderByIdDesc.stream().forEach(relatorioComercialDTO -> {
                    List<PesagemHistoricoRomaneioDTO> pesagensDoRomaneio = pesagensBrutas.stream().filter(r -> r.getNumeroRomaneio().equals(relatorioComercialDTO.getNumeroRomaneio())).collect(Collectors.toList());
                    if (ColecaoUtil.isNotEmpty(pesagensDoRomaneio)) {
                        relatorioComercialDTO.setQuantidadePesagensBrutas(pesagensDoRomaneio.stream().map(PesagemHistoricoRomaneioDTO::getQuantidade).reduce(0L, (q1, q2) -> q1 + q2));
                    }
                });
            }

            this.popularDatasPesagem(byDataCadastroAndMaterialIdOrdOrderByIdDesc);

        }

        return byDataCadastroAndMaterialIdOrdOrderByIdDesc;
    }

    private void popularDatasPesagem(List<RelatorioComercialDTO> romaneiosRelatorioComercialDto) {

        List<Long> idPesagens = romaneiosRelatorioComercialDto.stream().map(RelatorioComercialDTO::getIdPesagem).collect(Collectors.toList());

        if (ColecaoUtil.isNotEmpty(idPesagens)) {

            List<PesagemHistoricoDTO> historicosPrimeiraPesagem = pesagemService.obterPrimeiraPesagemDeRomaneios(idPesagens);

            List<PesagemHistoricoDTO> historicosSegundaPesagem = pesagemService.obterSegundaPesagemDeRomaneios(idPesagens);

            romaneiosRelatorioComercialDto.stream().filter(romaneio -> ObjetoUtil.isNotNull(romaneio.getIdPesagem())).forEach(romaneio -> {

                Map<String, Date> mapDatasPesagens = pesagemService.getDatasDePesagem(historicosPrimeiraPesagem, historicosSegundaPesagem, romaneio.getIdPesagem());

                if (!mapDatasPesagens.isEmpty()) {

                    romaneio.setDataPrimeiraPesagem(mapDatasPesagens.get(PesagemServiceImpl.DATA_PRIMEIRA_PESAGEM));

                    romaneio.setDataSegundaPesagem(mapDatasPesagens.get(PesagemServiceImpl.DATA_SEGUNDA_PESAGEM));
                }
            });
        }
    }

    @Override
    public List<RelatorioComercial_V2_DTO> obterRelatorioComercial_V2(RelatorioComercialFiltroDTO filtro) {

        return romaneioCustomRepository.findByDataCadastroAndMaterialIdOrdOrderByIdDesc_V2(filtro);
    }

    @Override
    @Transactional
    public List<RelatorioFluxoDeCarregamentoDTO> obterRelatorioFluxoDeCarregamento(RelatorioFluxoDeCarregamentoFiltroDTO filtro) {

        List<RelatorioFluxoDeCarregamentoDTO> relatorioFluxoDeCarregamentos = romaneioCustomRepository.findByDataCadastroAndMaterialIdOrdOrderByIdDescCarregamento(filtro);

        if (ColecaoUtil.isNotEmpty(relatorioFluxoDeCarregamentos)) {

            List<Long> idPesagens = relatorioFluxoDeCarregamentos.stream().map(RelatorioFluxoDeCarregamentoDTO::getIdPesagem).collect(Collectors.toList());

            if (ColecaoUtil.isNotEmpty(idPesagens)) {

                List<PesagemHistoricoDTO> historicosPrimeiraPesagem = pesagemService.obterPrimeiraPesagemDeRomaneios(idPesagens);

                List<PesagemHistoricoDTO> historicosSegundaPesagem = pesagemService.obterSegundaPesagemDeRomaneios(idPesagens);

                relatorioFluxoDeCarregamentos.stream().filter(relatorioFluxoDeCarregamento -> ObjetoUtil.isNotNull(relatorioFluxoDeCarregamento.getIdPesagem())).forEach(relatorioFluxoDeCarregamento -> {

                    Map<String, Date> mapDatasPesagens = pesagemService.getDatasDePesagem(historicosPrimeiraPesagem, historicosSegundaPesagem, relatorioFluxoDeCarregamento.getIdPesagem());

                    if (!mapDatasPesagens.isEmpty()) {

                        relatorioFluxoDeCarregamento.setDtPrimeiraPesagem(mapDatasPesagens.get(PesagemServiceImpl.DATA_PRIMEIRA_PESAGEM));

                        relatorioFluxoDeCarregamento.setDtSegundaPesagem(mapDatasPesagens.get(PesagemServiceImpl.DATA_SEGUNDA_PESAGEM));
                    }
                });
            }
        }

        return relatorioFluxoDeCarregamentos;
    }

    @Override
    @Transactional
    public List<RelComercialNFeDTO> getRelatorioComercialNFeDTOS(RelatorioComercialFiltroDTO filtro) {

        return romaneioCustomRepository.getRelatorioComercialNFeDTOS(filtro);
    }

    @Override
    public List<RelatorioAnaliseConversaoDTO> obterRelatorioAnaliseConversao(RelatorioAnaliseConversaoFiltroDTO filtro) {

        List<RelatorioAnaliseConversaoDTO> conversoes = romaneioCustomRepository.obterRelatorioAnaliseConversao(filtro);

        if (ColecaoUtil.isNotEmpty(conversoes)) {

            List<String> romaneios = conversoes.stream().map(RelatorioAnaliseConversaoDTO::getNumeroRomaneio).collect(Collectors.toList());

            List<ConversaoLitragemRomaneioDTO> conversaoLitragemRomaneios = romaneioCustomRepository.countConversoesPorRomaneio(romaneios);

            conversoes.stream().forEach(conversao -> conversao.setQuantidadeConversoes(conversaoLitragemRomaneios.stream().filter(_conversao -> _conversao.getNumeroRomaneio().equals(conversao.getNumeroRomaneio())).map(ConversaoLitragemRomaneioDTO::getTotal).findFirst().orElse(0L)));
        }

        return conversoes;
    }

    @Override
    public List<Romaneio> obterRomaneioProjetandoApenasIdEOperacao(final Set<String> numeroRomaneios) {

        return romaneioCustomRepository.obterIdEOperacaoPorNumeroRomaneio(numeroRomaneios);
    }

    @Override
    public void validarEtapas(final Romaneio romaneio) throws ServicoException {

        if (romaneio.getOperacao().isGestaoPatio()) {

            final Material material = romaneio.getItens().stream().filter(i -> ObjetoUtil.isNotNull(i.getMaterial())).map(ItemNFPedido::getMaterial).findFirst().orElse(null);

            if (ObjetoUtil.isNotNull(material) && material.isLacrarCarga() && ObjetoUtil.isNotNull(romaneio.getId())) {

                validarEtapaLacragem(romaneio, material);
            }

            if (ColecaoUtil.isNotEmpty(romaneio.getAnalises())) {

                validarDatasDeFabricaoDeLotes(romaneio);

                if (ColecaoUtil.isNotEmpty(material.getCampos()) && material.getCampos().stream().filter(CampoOperacaoMaterial::isObrigatorio).count() > 0) {

                    validarAnaliseQualidade(romaneio);
                }
            }
        }
    }

    private List<RelatorioDepositoDTO> obterDadosCompletosRelatorioDeposito(List<RomaneioDepositoDTO> romaneios) {

        Paginacao paginacao = new Paginacao();

        paginacao.setListarTodos(Boolean.TRUE);

        paginacao.setSpecification(((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(criteriaBuilder.or(criteriaBuilder.isNotNull(root.get(ItemNFPedido_.numeroNfe)),
                        criteriaBuilder.isNotNull(root.get(ItemNFPedido_.numeroNotaFiscalTransporte))),
                root.join(ItemNFPedido_.romaneio).get(Romaneio_.numeroRomaneio).in(romaneios.stream().map(RomaneioDepositoDTO::getNumeroRomaneio).collect(Collectors.toList())))));

        Collection<ItemNFNotaListagemDTO> notas = RomaneioServiceImpl.this.itemNFPedidoService.dtoListar(paginacao, ItemNFNotaListagemDTO.class).getContent();

        if (ColecaoUtil.isNotEmpty(notas)) {

            Collection<ItemNFNotaListagemDTO> finalNotas = notas;
            paginacao.setSpecification(((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(root.join(ItemNFPedido_.romaneio).get(Romaneio_.numeroRomaneio).in(romaneios.stream().map(RomaneioDepositoDTO::getNumeroRomaneio).collect(Collectors.toList())), criteriaBuilder.not(root.join(ItemNFPedido_.romaneio).get(Romaneio_.id).in(finalNotas.stream().map(ItemNFNotaListagemDTO::getRomaneio).map(Romaneio::getId).collect(Collectors.toList()))))));

        } else {

            paginacao.setSpecification(((root, criteriaQuery, criteriaBuilder) -> root.join(ItemNFPedido_.romaneio).get(Romaneio_.numeroRomaneio).in(romaneios.stream().map(RomaneioDepositoDTO::getNumeroRomaneio).collect(Collectors.toList()))));
        }

        Collection<RetornoNFEListagemDTO> notasRomaneio = retornoNFeService.dtoListar(paginacao, RetornoNFEListagemDTO.class).getContent();

        if (ColecaoUtil.isEmpty(notas)) {

            notas = new ArrayList<>();
        }

        Collection<ItemNFNotaListagemDTO> finalNotas1 = notas;

        if (ColecaoUtil.isNotEmpty(notasRomaneio)) {

            notasRomaneio.stream().forEach(retornoNFEListagemDTO -> finalNotas1.add(ItemNFNotaListagemDTO.builderItemNfNotaListagemDto().romaneio(retornoNFEListagemDTO.getRomaneio()).numeroNfe(retornoNFEListagemDTO.getNfe()).numeroNfTransporte(retornoNFEListagemDTO.getNfeTransporte()).requestKey(retornoNFEListagemDTO.getRequestKey()).build()));
        }

        Map<String, List<RomaneioDepositoDTO>> mapRomaneiosPorNumero = romaneios.stream().collect(Collectors.groupingBy(RomaneioDepositoDTO::getNumeroRomaneio));

        List<RelatorioDepositoDTO> relatorioDepositos = new ArrayList<>();

        mapRomaneiosPorNumero.keySet().stream().forEach(chave -> {

            List<RomaneioDepositoDTO> romaneiosPorChave = mapRomaneiosPorNumero.get(chave);

            relatorioDepositos.addAll(romaneiosPorChave.stream().map(romaneioDepositoDTO -> new RelatorioDepositoDTO(romaneiosPorChave.size() > 1, romaneioDepositoDTO, finalNotas1)).collect(Collectors.toList()));
        });

        return relatorioDepositos;
    }

    private List<ItemClassificacaoDepositoDTO> obterItensClassificacaoRelatorioDeposito(List<RomaneioDepositoDTO> romaneios) {

        List<Long> idPesagens = romaneios.stream().map(RomaneioDepositoDTO::getIdPesagem).collect(Collectors.toList());
        List<PesagemHistoricoDTO> historicosPrimeiraPesagem = pesagemService.obterPrimeiraPesagemDeRomaneios(idPesagens);
        List<PesagemHistoricoDTO> historicosSegundaPesagem = pesagemService.obterSegundaPesagemDeRomaneios(idPesagens);
        romaneios.stream().filter(romaneio -> ObjetoUtil.isNotNull(romaneio.getIdPesagem())).forEach(romaneio -> {
            Map<String, Date> mapDatasPesagens = pesagemService.getDatasDePesagem(historicosPrimeiraPesagem, historicosSegundaPesagem, romaneio.getIdPesagem());
            if (!mapDatasPesagens.isEmpty()) {
                romaneio.setDataPrimeiraPesagem(mapDatasPesagens.get(PesagemServiceImpl.DATA_PRIMEIRA_PESAGEM));
                romaneio.setDataSegundaPesagem(mapDatasPesagens.get(PesagemServiceImpl.DATA_SEGUNDA_PESAGEM));
            }
        });
        return itensClassificacaoCustomRepository.obterDadosDeClassificaoParaRelatorioDeDeposito(romaneios);
    }

    @Override
    public List<RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_DADOS_Item> filtrarRelatorioFornecedorGraosPagamento(RelatorioDepositoFiltroDTO filtro) {

        String centroSap = parametroService.obterPorTipoParametro(TipoParametroEnum.CENTRO_SAP).getValor();

        filtro.setCentro(centroService.obterPorCodigo(centroSap));

        filtro.setDeposito(depositoService.obterPorCodigo(DEPOSITO_CODIGO_PORTAL_FORNECEDOR_GRAOS));

        filtro.getDepositante().setCodigo(produtorDAO.findCodigoById(filtro.getDepositante().getId()));

        final RfcZITSSAGRO_DIVERG_PESO_ROMANEIORequest request =
                RfcZITSSAGRO_DIVERG_PESO_ROMANEIORequest.builderRfcZitssAgroDiverPesoRomaneioRequest().safra(filtro.getSafra()).centro(filtro.getCentro()).deposito(filtro.getDeposito()).fornecedor(filtro.getDepositante()).material(filtro.getMaterial()).dataNfeDe(filtro.getDataNfeDe()).dataNfeAte(filtro.getDataNfeAte()).dataPedidoDe(filtro.getDataPedidoDe()).dataPedidoAte(filtro.getDataPedidoAte()).dataVencimentoDe(filtro.getDataVencimentoDe()).dataVencimentoAte(filtro.getDataVencimentoAte()).build();

        final RfcZITSSAGRO_DIVERG_PESO_ROMANEIOResponse response = new RfcZITSSAGRO_DIVERG_PESO_ROMANEIOResponse();

        final Map<Long, JCoCallerService> connectionsMap = RFCMultipleProviders.DESTINATIONS;

        if (ObjetoUtil.isNotNull(connectionsMap)) {

            final JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.get(connectionsMap.keySet().stream().findFirst().get());

            sap.call(request, response);

            List<RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_DADOS_Item> t_dados = response.getT_dados();

            t_dados.stream().forEach(item -> item.setCentro(filtro.getCentro().toString()));

            return t_dados;
        }

        return null;
    }

    @Override
    public Object filtrarDepositosFornecedorGraosEntregas(RelatorioDepositoFiltroDTO filtro) {

        Long totalRomaneios = this.romaneioCustomRepository.obterCountDadosDoRomaneioParaRelatorioDeDeposito(filtro);
        if (totalRomaneios > 0) {
            List<RomaneioDepositoDTO> romaneios = this.romaneioCustomRepository.obterDadosDoRomaneioParaRelatorioDeDeposito(filtro);
            List<ItemClassificacaoDepositoDTO> itensClassificacao = obterItensClassificacaoRelatorioDeposito(romaneios);
            List<RelatorioDepositoDTO> relatorioDepositos = obterDadosCompletosRelatorioDeposito(romaneios);
            SortedSet<ItemClassificacaoDTO> itensClassificacaoColunas = new TreeSet<ItemClassificacaoDTO>(Comparator.comparing(ItemClassificacaoDTO::getCodigoItemClassificacao));
            itensClassificacaoColunas.addAll(itensClassificacao.stream().map(item -> new ItemClassificacaoDTO(item.getDescricaoItemClassificacao(), item.getCodigoItemClassificacao())).collect(Collectors.toSet()));
            inicializarMapParaItensClassificacao(itensClassificacao, relatorioDepositos, itensClassificacaoColunas);
            return MapBuilder.create("depositos", relatorioDepositos).map("itensClassificacaoColunas", itensClassificacaoColunas).map("totalDepositos", totalRomaneios).build();
        }
        return MapBuilder.create("depositos", null).map("itensClassificacaoColunas", null).map("totalDepositos", 0L).build();
    }

    private void inicializarMapParaItensClassificacao(List<ItemClassificacaoDepositoDTO> itensClassificacao, List<RelatorioDepositoDTO> relatorioDepositos, SortedSet<ItemClassificacaoDTO> itensClassificacaoColunas) {

        relatorioDepositos.stream().forEach(deposito -> {
            AtomicInteger desconto = new AtomicInteger(0);
            deposito.setValoresItensClassificacao(new HashMap<>());
            itensClassificacaoColunas.forEach(itemClassificacao -> {
                ItemClassificacaoDepositoDTO itemClassificacaoDepositoDTO =
                        itensClassificacao.stream().filter(item -> item.getIdClassificacao().equals(deposito.getIdClassificacao()) && item.getCodigoItemClassificacao().equals(itemClassificacao.getCodigoItemClassificacao())).findFirst().orElse(null);
                boolean apresentarPercentual = ObjetoUtil.isNotNull(itemClassificacaoDepositoDTO) && !deposito.isPossuiRateio();
                if (apresentarPercentual) {
                    desconto.addAndGet(itemClassificacaoDepositoDTO.getDesconto().intValue());
                }
                deposito.getValoresItensClassificacao().put("descricaoItemClassificacao_" + itemClassificacao.getCodigoItemClassificacao(), apresentarPercentual ? itemClassificacaoDepositoDTO.getDesconto().doubleValue() : new Double(0));
                deposito.getValoresItensClassificacao().put("descricaoItemClassificacaoIndice_" + itemClassificacao.getCodigoItemClassificacao(), apresentarPercentual ? itemClassificacaoDepositoDTO.getIndice().doubleValue() :
                        new Double(0));
                deposito.getValoresItensClassificacao().put("descricaoItemClassificacaoPercDesconto_" + itemClassificacao.getCodigoItemClassificacao(), apresentarPercentual ?
                        itemClassificacaoDepositoDTO.getPercentualDesconto().doubleValue() : new Double(0));
            });
            deposito.setDesconto(new BigDecimal(desconto.doubleValue()));
            deposito.setPesoLiquidoSeco(deposito.isPossuiRateio() ? deposito.getRateio() : deposito.getPesoLiquidoUmido().subtract(new BigDecimal(desconto.doubleValue())));
            if (!BigDecimalUtil.isMaiorQueZero(deposito.getQuantidadeNfe())) {
                deposito.setQuantidadeNfe(deposito.getPesoLiquidoSeco());
            }
        });
    }

    @Override
    public FileResponse obterRelatorioDepositos(RelatorioDepositoFiltroDTO filtro) {

        List<RomaneioDepositoDTO> romaneios = this.romaneioCustomRepository.obterDadosDoRomaneioParaRelatorioDeDeposito(filtro);
        if (ColecaoUtil.isNotEmpty(romaneios)) {
            List<ItemClassificacaoDepositoDTO> itensClassificacao = obterItensClassificacaoRelatorioDeposito(romaneios);
            List<ItemClassificacaoDepositoDTO> itensClassificacaoOrigem = itensClassificacaoCustomRepository.obterDadosDeClassificaoOrigemParaRelatorioDeDeposito(romaneios);
            List<RelatorioDepositoDTO> relatorioDepositos = obterDadosCompletosRelatorioDeposito(romaneios);
            return GerarRelatorioExcel.obterRelatorioDeDepositos(relatorioDepositos, itensClassificacao, itensClassificacaoOrigem);
        }
        return null;
    }

    @Override
    public FileResponse obterRelatorioDeDepositosFornecedorGraosEntregas(RelatorioDepositoFiltroDTO filtro) {

        List<RomaneioDepositoDTO> romaneios = this.romaneioCustomRepository.obterDadosDoRomaneioParaRelatorioDeDeposito(filtro);
        if (ColecaoUtil.isNotEmpty(romaneios)) {
            List<ItemClassificacaoDepositoDTO> itensClassificacao = obterItensClassificacaoRelatorioDeposito(romaneios);
            List<RelatorioDepositoDTO> relatorioDepositos = obterDadosCompletosRelatorioDeposito(romaneios);
            return GerarRelatorioExcel.obterRelatorioDeDepositosFornecedorGraosEntregas(relatorioDepositos, itensClassificacao);
        }
        return null;
    }

    @Override
    public FileResponse obterRelatorioDeDepositosFornecedorGraosPagamento(RelatorioDepositoFiltroDTO filtro) {

        List<RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_DADOS_Item> dados = this.filtrarRelatorioFornecedorGraosPagamento(filtro);
        if (ColecaoUtil.isNotEmpty(dados)) {
            return GerarRelatorioExcel.obterRelatorioDeDepositosFornecedorGraosPagamento(dados);
        }
        return null;
    }

    @Override
    public FileResponse obterRelatorioDeLacresVinculadosARomaneio(RelatorioLacreFiltroDTO relatorioLacreFiltro) {

        List<RelatorioLacreDTO> lacres = this.lacreService.obterRelatorioDeLacresVinculadosARomaneio(relatorioLacreFiltro);

        if (ColecaoUtil.isNotEmpty(lacres)) {

            return GerarRelatorioExcel.obterRelatorioDeLacresVinculadosARomaneio(lacres);
        }

        return null;
    }

    private void validarDatasDeFabricaoDeLotes(Romaneio romaneio) {

        List<Date> datasDeFabricaoDeLotes = romaneio.getAnalises().stream().filter(i -> ObjetoUtil.isNotNull(i.getLoteAnaliseQualidade()) && ObjetoUtil.isNotNull(i.getLoteAnaliseQualidade().getDataFabricacao())).map(i -> i.getLoteAnaliseQualidade().getDataFabricacao()).collect(Collectors.toList());

        if (ColecaoUtil.isNotEmpty(datasDeFabricaoDeLotes)) {

            boolean existeDatasInvalidas = datasDeFabricaoDeLotes.stream().filter(d -> DateUtil.maiorQue(DateUtil.obterDataHoraZerada(d), DateUtil.obterDataHoraZerada(DateUtil.hoje()))).count() > 0;

            if (existeDatasInvalidas) {

                throw new ServicoException("Existe Dt. Fabricação de Lote inválidas.\nDt. Fabricação de Lote " + "não pode ser maior que a data atual.");
            }
        }
    }

    private void validarEtapaLacragem(Romaneio romaneio, Material material) {

        FluxoEtapaDTO fluxoEtapa = this.fluxoEtapaService.obterPorCentroEtapaMaterial(romaneio.getCentro(), EtapaEnum.LACRAGEM, ObjetoUtil.isNotNull(material) ? material.getCodigo() : null, ObjetoUtil.isNotNull(material) ?
                material.getDescricao() : null, ObjetoUtil.isNotNull(romaneio.getSenha()) ? romaneio.getSenha().getTipoFluxo() : null);

        if (ObjetoUtil.isNotNull(fluxoEtapa) && (ObjetoUtil.isNull(romaneio.getLacragem()) || ColecaoUtil.isEmpty(romaneio.getLacragem().getLacres()))) {

            throw new ServicoException(MessageSupport.getMessage("RNA5011.MSGE008"));
        }
    }

    private void validarAnaliseQualidade(Romaneio romaneio) {

        Material material = romaneio.getItens().stream().filter(i -> ObjetoUtil.isNotNull(i.getMaterial())).map(ItemNFPedido::getMaterial).findFirst().get();

        List<String> camposASerIgnoradoDoLocalCarregamento = Arrays.asList(new String[]{"identificador", "dataInicioDoCarregamento", "dataFimDoCarregamento"});

        List<String> variaveisObrigatorias =
                material.getCampos().stream().filter(c -> c.isObrigatorio() && !camposASerIgnoradoDoLocalCarregamento.contains(c.getCampo().getDescricao())).map(CampoOperacaoMaterial::getCampo).map(Campo::getDescricao).collect(Collectors.toList());

        for (AnaliseQualidade analiseQualidade : romaneio.getAnalises()) {

            Class<AnaliseQualidade> analiseQualidadeClass = AnaliseQualidade.class;

            for (String variavel : variaveisObrigatorias) {

                try {

                    Field valor = analiseQualidadeClass.getDeclaredField(variavel);

                    valor.setAccessible(true);

                    Object objetoDaAnalise = valor.get(analiseQualidade);

                    if (ObjetoUtil.isNull(objetoDaAnalise)) {

                        throw new ServicoException(MessageSupport.getMessage("MSGE033"));
                    }

                } catch (NoSuchFieldException | IllegalAccessException e) {

                    throw new ServicoException(e.getMessage());
                }
            }
        }
    }

    @Override
    public void validarChaveDeAcesso(final String chave) throws Exception {

        if (StringUtil.isNotNullEmpty(chave)) {

            final Parametro parametro = this.parametroService.obterPorTipoParametro(TipoParametroEnum.DIRETORIO_XML);

            final File fileXml = new File(parametro.getValor());

            final List<String> xmls = Arrays.asList(fileXml.list()).stream().map(x -> StringUtil.apenasNumeros(x)).collect(Collectors.toList());

            if (xmls.stream().filter(x -> x.equals(chave)).count() <= 0) {

                throw new Exception("Chave de acesso informada não foi encontrada.");
            }
        }
    }

    @Override
    public Romaneio obterSimplesEntrada(final String numero) {

        return this.getDAO().findByNumeroRomaneioAndStatusRomaneioInAndOperacaoEntidade(numero, Arrays.asList(StatusRomaneioEnum.CONCLUIDO, StatusRomaneioEnum.PENDENTE_INDICES_CLASSIFICACAO), DadosSincronizacaoEnum.SIMPLES_ENTRADA);
    }

    @Override
    public byte[] imprimirPdf(Romaneio romaneio, final Long id) throws JRException {

        if (ObjetoUtil.isNull(romaneio)) {

            romaneio = this.get(id);
        }

        InputStream inputStream = RomaneioServiceImpl.class.getClassLoader().getResourceAsStream(romaneio.getOperacao().isGestaoPatio() ? REL_ROMANEIO_GP_JASPER : REL_ROMANEIO_JASPER);

        final RelatorioRomaneioDTO dto = this.obterFieldsRomaneio(romaneio);

        final HashMap<String, Object> parametros = romaneio.getOperacao().isGestaoPatio() ? this.montarParametrosRelatorioGestaoPatio() : this.montarParametrosRelatorioItssAgro();

        final List<RelatorioRomaneioDTO> lista = new ArrayList<>();

        lista.add(dto);

        if (!romaneio.getOperacao().isGestaoPatio()) {

            lista.add(dto.getClone());
        }

        return RelatorioUtil.gerarPdf(lista, inputStream, parametros);
    }

    @Override
    public List<RequestItemDTO> obterRequestsSemFaturas(List<Long> idsRomaneio) {

        return romaneioCustomRepository.obterRequestKeys(idsRomaneio);
    }

    @Override
    public void updateDocumentoFaturamento(Long documentoFaturamento, String requestKey, String numeroRomaneio) {

        itemNFPedidoService.atualizarDocumentoFaturamento(documentoFaturamento, requestKey, numeroRomaneio);
    }

    private HashMap<String, Object> montarParametrosRelatorioItssAgro() throws JRException {

        final HashMap<String, Object> parametros = new HashMap<String, Object>();

        final InputStream inputStreamOrigem = RomaneioServiceImpl.class.getClassLoader().getResourceAsStream(REL_ROMANEIO_CLASSIFICACAO_ORIGEM_JASPER);

        final InputStream inputStreamDestino = RomaneioServiceImpl.class.getClassLoader().getResourceAsStream(REL_ROMANEIO_CLASSIFICACAO_DESTINO_JASPER);

        final InputStream inputStreamLogo = RomaneioServiceImpl.class.getClassLoader().getResourceAsStream(LOGO_PNG);

        parametros.put("SUBREPORT_DIR_ORIGEM", (JasperReport) JRLoader.loadObject(inputStreamOrigem));

        parametros.put("SUBREPORT_DIR_DESTINO", (JasperReport) JRLoader.loadObject(inputStreamDestino));

        parametros.put("LOGO", inputStreamLogo);

        return parametros;
    }

    private HashMap<String, Object> montarParametrosRelatorioGestaoPatio() throws JRException {

        final HashMap<String, Object> parametros = new HashMap<String, Object>();

        final InputStream inputStreamLogo = RomaneioServiceImpl.class.getClassLoader().getResourceAsStream(LOGO_PNG);

        parametros.put("LOGO", inputStreamLogo);

        return parametros;
    }

    private RelatorioRomaneioDTO obterFieldsRomaneio(final Romaneio romaneio) {

        ItemNFPedido item = romaneio.getOperacao().isPossuiDivisaoCarga() ? romaneio.getItens().stream().findFirst().orElse(null) : null;

        RelatorioRomaneioDTO dto = this.obterDadosCabecalho(romaneio, item);

        this.obterProdutorRomaneio(dto, romaneio, ObjetoUtil.isNotNull(item) ? item.getProdutor() : romaneio.getProdutor());

        if (romaneio.getOperacao().isGestaoPatio()) {

            final Material material = romaneio.getItens().stream().map(ItemNFPedido::getMaterial).filter(Objects::nonNull).findFirst().get();

            dto.setPossuiConversao(material.isRealizarConversaoLitragem());

            dto.setPossuiAnalise(material.isRealizarAnaliseQualidade() && ColecaoUtil.isNotEmpty(romaneio.getAnalises()) && romaneio.getAnalises().stream().filter(i -> (ObjetoUtil.isNotNull(i.getLoteAnaliseQualidade()) && StringUtil.isNotNullEmpty(i.getLoteAnaliseQualidade().getCodigo())) || StringUtil.isNotNullEmpty(i.getLacreAmostra())).count() > 0);

            dto.setPossuiLacragem(material.isLacrarCarga() && ObjetoUtil.isNotNull(romaneio.getLacragem()) && ColecaoUtil.isNotEmpty(romaneio.getLacragem().getLacres()));

            if (dto.isPossuiLacragem()) {

                dto.setLacres(romaneio.getLacragem().getLacres().stream().filter(l -> StatusLacreRomaneioEnum.VINCULADO.equals(l.getStatusLacreRomaneio())).map(LacreRomaneio::getLacre).map(Lacre::getCodigo).sorted().collect(Collectors.joining("; ")));
            }

            if (dto.isPossuiAnalise()) {

                dto.setAnalises(romaneio.getAnalises().stream().map(i -> new AnaliseQualidadeDTO(i.getLacreAmostra(), ObjetoUtil.isNotNull(i.getLoteAnaliseQualidade()) ? i.getLoteAnaliseQualidade().getCodigo() : "")).collect(Collectors.toList()));
            }

            if (ColecaoUtil.isNotEmpty(romaneio.getRetornoNfes())) {

                dto.setNfes(romaneio.getRetornoNfes().stream().map(i -> MessageFormat.format("{0} - {1}", i.getNfe(), i.getSerie())).collect(Collectors.joining("\n")));
            }

            if (dto.isPossuiConversao() && ObjetoUtil.isNotNull(romaneio.getConversaoLitragem()) && ColecaoUtil.isNotEmpty(romaneio.getConversaoLitragem().getConversoes())) {

                romaneio.getConversaoLitragem().getConversoes().sort(Comparator.comparing(ConversaoLitragemItem::getDataCadastro).reversed());

                romaneio.getConversaoLitragem().setMaterial(material);

                final ConversaoLitragemItem itemConversao = romaneio.getConversaoLitragem().getConversoes().stream().findFirst().get();

                final TabelaValorItem tabelaValor = tabelaValorService.obterItensParaConversao(romaneio.getConversaoLitragem());

                dto.setConversao(new ConversaoLitragemDTO(itemConversao, tabelaValor, romaneio.getConversaoLitragem()));
            }
        }

        if (romaneio.getOperacao().isPossuiDivisaoCarga()) {

            dto.setDivisoesCarga(romaneio.getItens().stream().map(ItemNFPedido::toRelatorioDto).collect(Collectors.toList()));

            dto.setItensOrdemVenda(romaneio.getItens().stream().map(ItemNFPedido::toItemOrdemVendaDTO).collect(Collectors.toList()));
        }

        this.obterClassificacaoDestino(romaneio, dto);

        if (romaneio.getOperacao().isPossuiClassificacaoOrigem()) {

            this.obterClassificacaoOrigem(romaneio, dto);
        }

        return dto;
    }

    private RelatorioRomaneioDTO obterDadosCabecalho(final Romaneio romaneio, final ItemNFPedido item) {

        final RelatorioRomaneioDTO dto = new RelatorioRomaneioDTO();

        dto.setDataPortaria(ObjetoUtil.isNotNull(romaneio.getDtSimplesEntradaReferencia()) ? romaneio.getDtSimplesEntradaReferencia() : romaneio.getDataPortaria());

        dto.setPossuiDadosNfe(romaneio.getOperacao().isPossuiDadosNfe());

        dto.setPossuiDivisaoCarga(romaneio.getOperacao().isPossuiDivisaoCarga());

        dto.setCentroDestino(ObjetoUtil.isNotNull(romaneio.getCentroDestino()) ? romaneio.getCentroDestino().toString() : StringUtil.empty());

        dto.setCentroOrigem(ObjetoUtil.isNotNull(romaneio.getCentro()) ? romaneio.getCentro().toString() : StringUtil.empty());

        dto.setDataCriacao(ObjetoUtil.isNotNull(romaneio.getDtSimplesEntradaReferencia()) ? romaneio.getDtSimplesEntradaReferencia() : romaneio.getDataCadastro());

        if (ObjetoUtil.isNotNull(romaneio.getPesagem())) {

            PesagemHistoricoDTO pesagemHistoricoSegundaPesagem = pesagemService.obterSegundaPesagemDoRomaneio(romaneio.getPesagem().getId());

            if (ObjetoUtil.isNotNull(pesagemHistoricoSegundaPesagem)) {

                dto.setDataSaida(pesagemHistoricoSegundaPesagem.getDataCadastro());
            }
        }

        dto.setNumero(romaneio.getNumeroRomaneio());

        dto.setObservacao(romaneio.getObservacao());

        dto.setObservacaoNota(romaneio.getObservacaoNota());

        dto.setOperacao(romaneio.getOperacao().getOperacao().concat("-").concat(romaneio.getOperacao().getDescricao()));

        dto.setStatusRomaneio(romaneio.getStatusRomaneio().getDescricao());

        if (ObjetoUtil.isNotNull(romaneio.getIdUsuarioAutorCadastro())) {

            final Usuario usuario = this.usuarioService.get(romaneio.getIdUsuarioAutorCadastro());

            dto.setUsuario(usuario.getNome());
        }

        if (ObjetoUtil.isNotNull(item)) {

            dto.setDeposito(ObjetoUtil.isNotNull(item.getDeposito()) ? item.getDeposito().toString() : StringUtil.empty());

            dto.setMaterial(ObjetoUtil.isNotNull(item.getMaterial()) ? item.getMaterial().toString() : StringUtil.empty());

            dto.setMotorista(ObjetoUtil.isNotNull(item.getMotorista()) ? item.getMotorista().toString() : StringUtil.empty());

            dto.setNumeroNotaFiscalTransporte(StringUtil.isNotNullEmpty(item.getNumeroNotaFiscalTransporte()) ? item.getNumeroNotaFiscalTransporte() : StringUtil.empty());

            dto.setSerieNotaFiscalTransporte(StringUtil.isNotNullEmpty(item.getSerieNotaFiscalTransporte()) ? item.getSerieNotaFiscalTransporte() : StringUtil.empty());

            dto.setSafra(ObjetoUtil.isNotNull(item.getSafra()) ? item.getSafra().getDescricao() : StringUtil.empty());

            dto.setTransportadora(ObjetoUtil.isNotNull(item.getTransportadora()) ? item.getTransportadora().toString() : StringUtil.empty());

            dto.setVeiculo(ObjetoUtil.isNotNull(item.getPlacaCavalo()) ? StringUtil.toPlaca(item.getPlacaCavalo().getPlaca1()) : StringUtil.empty());

        } else {

            dto.setDeposito(ObjetoUtil.isNotNull(romaneio.getDeposito()) ? romaneio.getDeposito().toString() : StringUtil.empty());

            dto.setMaterial(ObjetoUtil.isNotNull(romaneio.getMaterial()) ? romaneio.getMaterial().toString() : DadosSincronizacaoEnum.SIMPLES_PESAGEM.equals(romaneio.getOperacao().getEntidade()) ? romaneio.getMaterialSimplesPesagem() :
                    StringUtil.empty());

            dto.setMotorista(ObjetoUtil.isNotNull(romaneio.getMotorista()) ? romaneio.getMotorista().toString() : StringUtil.empty());

            dto.setSafra(ObjetoUtil.isNotNull(romaneio.getSafra()) ? romaneio.getSafra().getDescricao() : StringUtil.empty());

            dto.setTransportadora(ObjetoUtil.isNotNull(romaneio.getTransportadora()) ? romaneio.getTransportadora().toString() : StringUtil.empty());

            dto.setVeiculo(ObjetoUtil.isNotNull(romaneio.getPlacaCavalo()) ? StringUtil.toPlaca(romaneio.getPlacaCavalo().getPlaca1()) : StringUtil.empty());
        }

        if (ObjetoUtil.isNotNull(romaneio.getNumeroCartaoAcesso())) {
            dto.setNumeroCartaoAcesso(romaneio.getNumeroCartaoAcesso().toString());
        }

        return dto;
    }

    private void obterProdutorRomaneio(final RelatorioRomaneioDTO dto, Romaneio romaneio, final Produtor produtor) {

        if (DadosSincronizacaoEnum.SIMPLES_PESAGEM.equals(romaneio.getOperacao().getEntidade())) {

            dto.setProdutor(StringUtil.isNotNullEmpty(romaneio.getNomeSimplesPesagem()) ? romaneio.getNomeSimplesPesagem() : StringUtil.empty());

        } else if (ObjetoUtil.isNotNull(produtor)) {

            dto.setProdutor(produtor.toString());

            dto.setCpfCnpj(StringUtil.isEmpty(produtor.getCpf()) ? produtor.getCnpj() : produtor.getCpf());

            dto.setEndereco(produtor.getEndFazenda());

            dto.setFazenda(produtor.getFazenda());

            dto.setInscricaoEstadual(produtor.getInscricaoEstadual());

        } else if (romaneio.getOperacao().isGestaoPatio()) {

            final Parametro parametro = parametroService.obterPorTipoParametro(TipoParametroEnum.CENTRO_CLIENTE);

            final Cliente sjc = clienteService.obterPorCodigo(parametro.getValor());

            dto.setProdutor(sjc.toString());

            dto.setCpfCnpj(sjc.getCpfCnpj());

            dto.setEndereco(sjc.getEndereco());

            dto.setInscricaoEstadual(sjc.getInscricaoEstadual());
        }
    }

    private void obterClassificacaoOrigem(final Romaneio romaneio, final RelatorioRomaneioDTO dto) {

        if (romaneio.getOperacao().isPossuiClassificacaoOrigem() && ColecaoUtil.isNotEmpty(romaneio.getItensClassificacaoOrigem())) {

            dto.setPesoOrigemBruto(ObjetoUtil.isNotNull(romaneio.getPesoBrutoOrigem()) ? StringUtil.toMilhar(romaneio.getPesoBrutoOrigem()) : StringUtil.toMilhar(BigDecimal.ZERO));

            dto.setPesoOrigemTara(ObjetoUtil.isNotNull(romaneio.getPesoTaraOrigem()) ? StringUtil.toMilhar(romaneio.getPesoTaraOrigem()) : StringUtil.toMilhar(BigDecimal.ZERO));

            final BigDecimal pesoBOrigem = ObjetoUtil.isNotNull(romaneio.getPesoBrutoOrigem()) ? romaneio.getPesoBrutoOrigem() : BigDecimal.ZERO;

            final BigDecimal pesoTOrigem = ObjetoUtil.isNotNull(romaneio.getPesoTaraOrigem()) ? romaneio.getPesoTaraOrigem() : BigDecimal.ZERO;

            dto.setPesoOrigemUmido(StringUtil.toMilhar(pesoBOrigem.subtract(pesoTOrigem)));

            dto.setPossuiClassificacaoOrigem(true);

            final List<RelatorioClassificacaoDTO> classificacoes = new ArrayList<RelatorioClassificacaoDTO>();

            final AtomicReference<BigDecimal> totalDescontos = new AtomicReference<>(BigDecimal.ZERO);

            romaneio.getItensClassificacaoOrigem().forEach(item -> {

                final RelatorioClassificacaoDTO classDto = new RelatorioClassificacaoDTO();

                classDto.setDesconto(StringUtil.toMilhar(item.getDescontoValido()));

                classDto.setDescricao(item.getDescricaoItem());

                classDto.setIndice(ObjetoUtil.isNotNull(item.getIndice()) ? item.getIndice().toString() : BigDecimal.ZERO.toString());

                classDto.setPercentualDesconto(StringUtil.toMilhar(item.getPercentualDescontoValido(), 2));

                classificacoes.add(classDto);

                totalDescontos.set(totalDescontos.accumulateAndGet(item.getDescontoValido(), BigDecimal::add).setScale(0, RoundingMode.HALF_EVEN));

                dto.setDescontosOrigem(StringUtil.toMilhar(totalDescontos.get(), 0));
            });

            dto.setPesoOrigemSeco(StringUtil.toMilhar(pesoBOrigem.subtract(pesoTOrigem).subtract(totalDescontos.get())));

            dto.setClassificacaoOrigem(classificacoes);
        }
    }

    private void obterClassificacaoDestino(final Romaneio romaneio, final RelatorioRomaneioDTO dto) {

        AtomicReference<BigDecimal> descontos = new AtomicReference<>(BigDecimal.ZERO);

        if (ObjetoUtil.isNotNull(romaneio.getClassificacao()) && RomaneioUtil.isClassificacaoPossuiIndices(romaneio.getClassificacao())) {

            dto.setPossuiClassificacaoDestino(true);

            final List<RelatorioClassificacaoDTO> classificacoes = new ArrayList<RelatorioClassificacaoDTO>();

            final AtomicInteger index = new AtomicInteger(0);

            AtomicReference<BigDecimal> pesoLiquido = new AtomicReference<>(BigDecimal.ZERO);

            AtomicReference<BigDecimal> pesoDescontado = new AtomicReference<>(BigDecimal.ZERO);

            romaneio.getClassificacao().getItens().forEach(item -> {

                final RelatorioClassificacaoDTO classDto = new RelatorioClassificacaoDTO();

                if ((ObjetoUtil.isNotNull(romaneio.getPesagem())) && (ObjetoUtil.isNotNull(romaneio.getPesagem().getPesoInicial()) && ObjetoUtil.isNotNull(romaneio.getPesagem().getPesoFinal()))) {

                    pesoLiquido.set(romaneio.getPesagem().getPesoInicial().subtract(romaneio.getPesagem().getPesoFinal()));

                    pesoDescontado.set(pesoLiquido.get().subtract(item.getDescontoValido()));
                }

                classDto.setDesconto(StringUtil.toMilhar(item.getDescontoValido(), 0));

                classDto.setDescricao(item.getDescricaoItem());

                classDto.setIndice(ObjetoUtil.isNotNull(item.getIndice()) ? item.getIndice().toString() : BigDecimal.ZERO.toString());

                classDto.setPercentualDesconto(StringUtil.toMilhar(item.getPercentualDescontoValido(), 2));

                classificacoes.add(classDto);

                descontos.set(descontos.accumulateAndGet(item.getDescontoValido(), BigDecimal::add).setScale(0, RoundingMode.HALF_EVEN));

                dto.setDescontos(StringUtil.toMilhar(descontos.get(), 0));

                index.incrementAndGet();
            });

            dto.setClassificacaoDestino(classificacoes);
        }

        if (ObjetoUtil.isNotNull(romaneio.getPesagem())) {

            final BigDecimal pesoB = ObjetoUtil.isNotNull(romaneio.getPesagem().getPesoInicial()) ? romaneio.getPesagem().getPesoInicial() : BigDecimal.ZERO;

            final BigDecimal pesoT = ObjetoUtil.isNotNull(romaneio.getPesagem().getPesoFinal()) ? romaneio.getPesagem().getPesoFinal() : BigDecimal.ZERO;

            dto.setPesoBruto(ObjetoUtil.isNotNull(romaneio.getPesagem().getPesoInicial()) ? StringUtil.toMilhar(romaneio.getPesagem().getPesoInicial()) : StringUtil.toMilhar(BigDecimal.ZERO));

            dto.setPesoTara(ObjetoUtil.isNotNull(romaneio.getPesagem().getPesoFinal()) ? StringUtil.toMilhar(romaneio.getPesagem().getPesoFinal()) : StringUtil.toMilhar(BigDecimal.ZERO));

            dto.setPesoUmido(StringUtil.toMilhar(pesoB.subtract(pesoT)));

            dto.setPesoLiquidoSeco(StringUtil.toMilhar(pesoB.subtract(pesoT).subtract(descontos.get())));
        }
    }

    @Override
    public void validarItensClassificacaoRestritos(Romaneio entidade, Usuario usuarioToken) throws ItensClassificacaoException {

        final boolean usuarioPodeAplicarIndice = usuarioToken.getPerfis().stream().filter(i -> i.isAplicarIndiceRestrito()).count() > 0;

        if (ColecaoUtil.isNotEmpty(entidade.getItensRestritosPorPerfil()) && !usuarioPodeAplicarIndice) {

            final List<String> itens = entidade.getItensRestritosPorPerfil().stream().map(TabelaClassificacao::getDescricaoItemClassificacao).collect(Collectors.toList());

            throw new ItensClassificacaoException(itens);
        }

        //        todo: Verificar com o Fábio se essa condição continua com o historico de aprovações
        //        if (ObjetoUtil.isNull(entidade.getUsuarioAprovador())) {
        //
        //        }
    }

    @Override
    public void validarEtapasDoRomaneioObrigatorias(Romaneio entidade) throws ProcessoNaoConcluidoException {

        if (!(RomaneioUtil.isOperacaoSemPesagemEClassificacao(entidade.getOperacao())
                || (RomaneioUtil.isOperacaoSimplesPesagem(entidade.getOperacao()) && RomaneioUtil.isRomaneioFoiPesado(entidade))
                || (RomaneioUtil.isRomaneioFoiPesado(entidade)
                && RomaneioUtil.isOperacaoSemClassificacao(entidade.getOperacao(), entidade.getItens().stream().map(ItemNFPedido::getMaterial).findFirst().orElse(null)))
                || (RomaneioUtil.isRomaneioFoiClassificado(entidade) && RomaneioUtil.isRomaneioFoiPesado(entidade)
                && RomaneioUtil.isConversaoRealizada(entidade)) || (!entidade.getCopiarDadosDeRomaneiosEstornados()
                && RomaneioUtil.isRomaneioFoiPesado(entidade) && RomaneioUtil.isOperacaoSemClassificacao(entidade.getOperacao()))
                || (!entidade.getCopiarDadosDeRomaneiosEstornados() && RomaneioUtil.isRomaneioFoiClassificado(entidade)
                && RomaneioUtil.isRomaneioFoiPesado(entidade) && RomaneioUtil.isConversaoRealizada(entidade)))) {
            throw new ProcessoNaoConcluidoException();
        }
    }

    @Override
    public void validarValorUnitarioPedidoENota(Romaneio entidade) throws DivergenciaPedidoNotaException {

        if (entidade.getOperacao().isPossuiValidacaoEntreVlrUniNotaEPedido()) {

            if (entidade.getOperacao().isPossuiDivisaoCarga()) {

                final boolean existeValoresDivergentes = entidade.getItens().stream().filter(i -> {

                    BigDecimal vlrPedido = i.getValorUnitarioPedido().multiply(i.getPesoTotalNfe());

                    BigDecimal vlrNfe = i.getValorUnitarioNfe().multiply(i.getPesoTotalNfe());

                    BigDecimal diferenca;

                    if (BigDecimalUtil.isMaiorQue(vlrPedido, vlrNfe)) {

                        diferenca = vlrPedido.subtract(vlrNfe).setScale(6, RoundingMode.HALF_EVEN);

                    } else {

                        diferenca = vlrNfe.subtract(vlrPedido).setScale(6, RoundingMode.HALF_EVEN);
                    }

                    return BigDecimalUtil.isMaiorQue(diferenca, entidade.getOperacao().getToleranciaEntreVlrUniNotaEPedido());

                }).count() > 0;

                if (existeValoresDivergentes) {

                    throw new DivergenciaPedidoNotaException();
                }
            }
        }
    }

    @Override
    public List<Long> listarIdDeRomaneiosIntegradosComSAP(Date dataUltimaSincronizacao) {

        return this.getDAO().findIdByStatusIntegracaoSAPInAndDataCadastroGreaterThanEqual(Arrays.asList(new StatusIntegracaoSAPEnum[]{StatusIntegracaoSAPEnum.COMPLETO, StatusIntegracaoSAPEnum.AGUARDANDO, StatusIntegracaoSAPEnum.ERRO}),
                DateUtil.obterDataHoraZerada(dataUltimaSincronizacao));
    }

    @Override
    public Romaneio obterRomaneioPorTicketOrigem(final String ticket) throws Exception {

        final Romaneio entidade = this.getDAO().findByRomaneioArmazemTerceirosTicketOrigem(ticket);

        if (ObjetoUtil.isNull(entidade)) {

            throw new Exception("Não foi encontrado romaneio referenciando o número informado.");
        }

        return entidade;
    }

    @Override
    public Romaneio obterRomaneioPorNumeroNotaFiscal(final String numero, final Long idRomaneio) throws Exception {

        RetornoNFe retornoNFe = this.retornoNFeService.obterNotaPorNumero(numero);

        if (ObjetoUtil.isNull(retornoNFe)) {

            throw new Exception(MessageFormat.format(MessageSupport.getMessage(NUMERO_INFORMADO_NAO_ENCONTRADO), numero));
        }

        if (this.retornoNFeService.notaEstaSendoUtilizada(numero, idRomaneio)) {

            throw new Exception(MessageFormat.format(MessageSupport.getMessage(NFE_DE_TRANSPORTE_JA_ESTA_SENDO_UTILIZADA), numero));
        }

        if (!STATUS_NFE_OK.equals(retornoNFe.getStatus())) {

            throw new Exception(MessageFormat.format(MessageSupport.getMessage(NFE_DE_TRANSPORTE_STATUS_DIFERENTE_100), numero, retornoNFe.getStatus()));
        }

        return retornoNFe.toRomaneio();
    }

    @Override
    public Romaneio obterRomaneioArmazemDeTerceiros(final String numero) throws Exception {

        final Romaneio romaneio = this.getDAO().findByNumeroRomaneioOrRomaneioArmazemTerceirosTicketOrigemAndOperacaoEntidade(numero, numero, DadosSincronizacaoEnum.ENTRADA_ARMAZEM_DE_TERCEIROS);

        if (ObjetoUtil.isNull(romaneio)) {

            throw new Exception(MessageFormat.format(MessageSupport.getMessage(NUMERO_INFORMADO_NAO_ENCONTRADO), numero));
        }

        if (this.getDAO().existsByTicketOrigem(numero, StatusIntegracaoSAPEnum.ESTORNADO, StatusRomaneioEnum.ESTORNADO)) {

            throw new Exception(MessageFormat.format(MessageSupport.getMessage(NUMERO_INFORMADO_UTILIZADO), numero));
        }

        this.limparReferenciasRomaneioArmazemTerceiros(numero, romaneio);

        return romaneio;
    }

    @Override
    public Romaneio obterRomaneioParaDevolucao(final String numero, final DadosSincronizacaoEnum operacao) throws Exception {

        Romaneio romaneio = new Romaneio();

        switch (operacao) {
            case EM_POS_DEVOLUCAO_PF: {
                romaneio = this.dao.findByNumeroRomaneioAndStatusRomaneioInAndOperacaoEntidade(numero, Arrays.asList(new StatusRomaneioEnum[]{StatusRomaneioEnum.CONCLUIDO}), DadosSincronizacaoEnum.PRODUTOR_PF_DIRETO_UPG);
                break;
            }
            case EM_POS_DEVOLUCAO_PJ: {
                romaneio = this.dao.findByNumeroRomaneioAndStatusRomaneioInAndOperacaoEntidade(numero, Arrays.asList(new StatusRomaneioEnum[]{StatusRomaneioEnum.CONCLUIDO}), DadosSincronizacaoEnum.PRODUTOR_PJ_DIRETO_UPG);
                break;
            }
            case EM_POS_DEVOLUCAO_PF_DIRETO_NA_GO206: {
                romaneio = this.dao.findByNumeroRomaneioAndStatusRomaneioInAndOperacaoEntidade(numero, Arrays.asList(new StatusRomaneioEnum[]{StatusRomaneioEnum.CONCLUIDO}), DadosSincronizacaoEnum.PRODUTOR_PF_DIRETO_GO206);
                break;
            }
            case EM_POS_DEVOLUCAO_PF_DIRETO_NA_AGROVALE: {
                romaneio = this.dao.findByNumeroRomaneioAndStatusRomaneioInAndOperacaoEntidade(numero, Arrays.asList(new StatusRomaneioEnum[]{StatusRomaneioEnum.CONCLUIDO}), DadosSincronizacaoEnum.PRODUTOR_PF_DIRETO_AGROVALE);
                break;
            }
            case EM_POS_DEVOLUCAO_PF_DIRETO_NA_CARAMURU: {
                romaneio = this.dao.findByNumeroRomaneioAndStatusRomaneioInAndOperacaoEntidade(numero, Arrays.asList(new StatusRomaneioEnum[]{StatusRomaneioEnum.CONCLUIDO}), DadosSincronizacaoEnum.PRODUTOR_PF_DIRETO_CARAMURU);
                break;
            }
            case PRODUTOR_PF_DIRETO_EM_ARMAZEM_TERC: {
                romaneio = this.dao.findByNumeroRomaneioAndStatusRomaneioInAndOperacaoEntidade(numero, Arrays.asList(new StatusRomaneioEnum[]{StatusRomaneioEnum.CONCLUIDO}), DadosSincronizacaoEnum.PRODUTOR_PF_DIRETO_EM_ARMAZEM_TERC);
                break;
            }
        }

        if (ObjetoUtil.isNull(romaneio)) {

            throw new Exception(MessageFormat.format(MessageSupport.getMessage(NUMERO_INFORMADO_NAO_ENCONTRADO), numero));
        }

        Classificacao classificacaoTemp = romaneio.getClassificacao();

        Romaneio romaneioParaDevolucao = romaneio.newReferenceToDevolucao(numero, this.dadosSincronizacaoService.obterPorEntidade(operacao));

        if (!Objects.isNull(classificacaoTemp)) {
            Classificacao classificacaoClone = new Classificacao();
            classificacaoClone.setObservacao(classificacaoTemp.getObservacao());
            classificacaoClone.setPlacaCavalo(classificacaoTemp.getPlacaCavalo());
            classificacaoClone.setSafra(classificacaoTemp.getSafra());
            classificacaoClone.setUsuarioClassificador(classificacaoTemp.getUsuarioClassificador());

            List<ItensClassificacao> itens = new ArrayList<>();

            classificacaoTemp.getItens().forEach(itemTemp -> {
                ItensClassificacao item = new ItensClassificacao();
                item.setItemClassificao(itemTemp.getItemClassificao());
                item.setDescricaoItem(itemTemp.getDescricaoItem());
                item.setCodigoTabela(itemTemp.getCodigoTabela());
                item.setIndice(itemTemp.getIndice());
                item.setPercentualDesconto(itemTemp.getPercentualDesconto());
                item.setDesconto(itemTemp.getDesconto());
                item.setDescontoClassificacao(itemTemp.getDescontoClassificacao());
                item.setCodigoMaterial(itemTemp.getCodigoMaterial());
                item.setCodigoArmazemTerceiros(itemTemp.getCodigoArmazemTerceiros());
                item.setIndiceNumerico(itemTemp.getIndiceNumerico());
                itens.add(item);
            });

            classificacaoClone.setItens(itens);

            romaneioParaDevolucao.setClassificacao(classificacaoClone);
        }

        return romaneioParaDevolucao;
    }

    private void limparReferenciasRomaneioArmazemTerceiros(String numero, Romaneio romaneio) {

        romaneio.setId(null);

        if (ColecaoUtil.isNotEmpty(romaneio.getItensClassificacaoOrigem())) {

            romaneio.getItensClassificacaoOrigem().forEach(item -> {

                item.setId(null);
            });
        }

        if (ObjetoUtil.isNotNull(romaneio.getPesagem())) {

            romaneio.getPesagem().setId(null);
        }

        if (ObjetoUtil.isNotNull(romaneio.getClassificacao())) {

            romaneio.getClassificacao().setId(null);

            if (ColecaoUtil.isNotEmpty(romaneio.getClassificacao().getItens())) {

                romaneio.getClassificacao().getItens().forEach(item -> {

                    item.setId(null);
                });
            }
        }

        if (ColecaoUtil.isNotEmpty(romaneio.getItens())) {

            romaneio.getItens().forEach(item -> {

                item.setId(null);

                item.setArmazemTerceirosReferencia(numero);
            });
        }
    }

    @Override
    public Romaneio obterPorNumeroRomaneio(final String numeroRomaneio) {

        return this.getDAO().findByNumeroRomaneio(numeroRomaneio);
    }

    @Override
    public ObterEntidadeResponse obterEntidade(final Long id) {
        Romaneio romaneio = this.get(id);

        List<CampoOperacaoDTO> campos = this.campoOperacaoService.obterCamposPorOperacao(romaneio.getOperacao().getEntidade());

        boolean habilitarLacragem = !StatusRomaneioEnum.PENDENTE.equals(romaneio.getStatusRomaneio()) && romaneio.getOperacao().isGestaoPatio() && ColecaoUtil.isNotEmpty(romaneio.getItens()) && romaneio.getItens().stream().findFirst().map(ItemNFPedido::getMaterial).map(Material::isLacrarCarga).orElse(false);
        boolean habilitarConversao = !StatusRomaneioEnum.PENDENTE.equals(romaneio.getStatusRomaneio()) && romaneio.getOperacao().isGestaoPatio() && ColecaoUtil.isNotEmpty(romaneio.getItens()) && romaneio.getItens().stream().findFirst().map(ItemNFPedido::getMaterial).map(Material::isRealizarConversaoLitragem).orElse(false);
        boolean habilitarPesagem = RomaneioUtil.isPossuiPesagem(romaneio) && !StatusRomaneioEnum.PENDENTE.equals(romaneio.getStatusRomaneio());
        boolean habilitarClassificacao = RomaneioUtil.isPossuiClassificacao(romaneio) && !StatusRomaneioEnum.PENDENTE.equals(romaneio.getStatusRomaneio());
        boolean habilitarCarregamento = romaneio.getItens().stream().map(ItemNFPedido::getMaterial).anyMatch(Material::isPossuiEtapaCarregamento);

        return ObterEntidadeResponse.builder().entidade(romaneio).campos(campos).habilitarLacragem(habilitarLacragem)
                .habilitarConversao(habilitarConversao).habilitarPesagem(habilitarPesagem)
                .habilitarClassificacao(habilitarClassificacao).habilitarCarregamento(habilitarCarregamento).build();
    }

    @Override
    public Romaneio obterRomaneioParaReferenciar(final String numeroRomaneio) {

        Romaneio romaneio = getDAO().findByNumeroRomaneio(numeroRomaneio);

        return new Romaneio(romaneio.getPesagem(), romaneio.getCentro(), romaneio.getUsuarioAprovador(), romaneio.getConversaoLitragem(), romaneio.getObservacao(), romaneio.getObservacaoNota(), romaneio.getItens(), romaneio.getAnalises()
                , romaneio.getMotivos(), romaneio.getSenha(), romaneio.getNumeroCartaoAcesso(), romaneio.getCarregamento(), romaneio.getLacragem(), romaneio.getOperacao(), romaneio.getStatusRomaneio(), romaneio.getStatusEtapa());
    }

    @Override
    @Transactional
    public ObterRomaneioSemPesagemResponse obterRomaneioSemPesagem(final String numero) throws Exception {

        Romaneio romaneio = getDAO().findByNumeroRomaneio(numero);

        if (ObjetoUtil.isNull(romaneio)) {

            throw new Exception(MessageFormat.format(MessageSupport.getMessage(NUMERO_INFORMADO_NAO_ENCONTRADO), numero));
        }

        boolean habilitarLacragem = !StatusRomaneioEnum.PENDENTE.equals(romaneio.getStatusRomaneio()) && romaneio.getOperacao().isGestaoPatio() && ColecaoUtil.isNotEmpty(romaneio.getItens()) && romaneio.getItens().stream().findFirst().map(ItemNFPedido::getMaterial).map(Material::isLacrarCarga).orElse(false);
        boolean habilitarConversao = !StatusRomaneioEnum.PENDENTE.equals(romaneio.getStatusRomaneio()) && romaneio.getOperacao().isGestaoPatio() && ColecaoUtil.isNotEmpty(romaneio.getItens()) && romaneio.getItens().stream().findFirst().map(ItemNFPedido::getMaterial).map(Material::isRealizarConversaoLitragem).orElse(false);
        boolean habilitarPesagem = RomaneioUtil.isPossuiPesagem(romaneio) && !StatusRomaneioEnum.PENDENTE.equals(romaneio.getStatusRomaneio());
        boolean habilitarClassificacao = RomaneioUtil.isPossuiClassificacao(romaneio) && !StatusRomaneioEnum.PENDENTE.equals(romaneio.getStatusRomaneio());

        return ObterRomaneioSemPesagemResponse.builder().entidade(RomaneioSemPesagemResponse.builder().centro(romaneio.getCentro())
                        .usuarioAprovador(romaneio.getUsuarioAprovador()).conversaoLitragem(romaneio.getConversaoLitragem())
                        .observacao(romaneio.getObservacao()).observacaoNota(romaneio.getObservacaoNota())
                        .itens(romaneio.getItens()).analises(romaneio.getAnalises())
                        .motivos(romaneio.getMotivos()).numeroCartaoAcesso(romaneio.getNumeroCartaoAcesso())
                        .carregamento(romaneio.getCarregamento()).operacao(romaneio.getOperacao())
                        .statusRomaneio(romaneio.getStatusRomaneio()).statusEtapa(romaneio.getStatusEtapa()).build())
                .habilitarLacragem(habilitarLacragem).habilitarConversao(habilitarConversao).habilitarPesagem(habilitarPesagem)
                .habilitarClassificacao(habilitarClassificacao).build();
    }

    @Override
    public ObterRomaneioEstornadoResponse obterRomaneioEstornado(final String numero) throws Exception {

        Romaneio romaneio = get(((root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.and(
                        criteriaBuilder.equal(root.get(Romaneio_.numeroRomaneio), numero),
                        criteriaBuilder.or(
                                criteriaBuilder.equal(root.get(Romaneio_.statusRomaneio), StatusRomaneioEnum.ESTORNADO),
                                criteriaBuilder.equal(root.get(Romaneio_.statusIntegracaoSAP), StatusIntegracaoSAPEnum.ESTORNADO)
                        )
                )));

        if (ObjetoUtil.isNull(romaneio)) {

            throw new Exception(MessageFormat.format(MessageSupport.getMessage(NUMERO_INFORMADO_NAO_ENCONTRADO), numero));
        }

        RomaneioEstornadoResponse romaneioEstornadoResponse = RomaneioEstornadoResponse.builder().pesagem(romaneio.getPesagem())
                .centro(romaneio.getCentro()).usuarioAprovador(romaneio.getUsuarioAprovador())
                .conversaoLitragem(romaneio.getConversaoLitragem()).observacao(romaneio.getObservacao())
                .observacaoNota(romaneio.getObservacaoNota()).itens(romaneio.getItens())
                .analises(romaneio.getAnalises()).motivos(romaneio.getMotivos())
                .senha(romaneio.getSenha()).numeroCartaoAcesso(romaneio.getNumeroCartaoAcesso())
                .carregamento(romaneio.getCarregamento()).lacragem(romaneio.getLacragem())
                .operacao(romaneio.getOperacao()).statusRomaneio(romaneio.getStatusRomaneio())
                .statusEtapa(romaneio.getStatusEtapa()).build();

        alterarStatusDosLacresNoEstornoDoRomaneio(romaneioEstornadoResponse, StatusLacreRomaneioEnum.VINCULADO, StatusLacreEnum.VINCULADO);

        boolean habilitarLacragem = !StatusRomaneioEnum.PENDENTE.equals(romaneio.getStatusRomaneio()) && romaneio.getOperacao().isGestaoPatio() && ColecaoUtil.isNotEmpty(romaneio.getItens()) && romaneio.getItens().stream().findFirst().map(ItemNFPedido::getMaterial).map(Material::isLacrarCarga).orElse(false);
        boolean habilitarConversao = !StatusRomaneioEnum.PENDENTE.equals(romaneio.getStatusRomaneio()) && romaneio.getOperacao().isGestaoPatio() && ColecaoUtil.isNotEmpty(romaneio.getItens()) && romaneio.getItens().stream().findFirst().map(ItemNFPedido::getMaterial).map(Material::isRealizarConversaoLitragem).orElse(false);
        boolean habilitarPesagem = RomaneioUtil.isPossuiPesagem(romaneio) && !StatusRomaneioEnum.PENDENTE.equals(romaneio.getStatusRomaneio());
        boolean habilitarClassificacao = RomaneioUtil.isPossuiClassificacao(romaneio) && !StatusRomaneioEnum.PENDENTE.equals(romaneio.getStatusRomaneio());

        return ObterRomaneioEstornadoResponse.builder().entidade(romaneioEstornadoResponse).habilitarLacragem(habilitarLacragem)
                .habilitarConversao(habilitarConversao).habilitarPesagem(habilitarPesagem).habilitarClassificacao(habilitarClassificacao).build();
    }

    @Override
    public Romaneio salvarRomaneio(Romaneio entidade) {

        try {

            if (ObjetoUtil.isNull(entidade.getId())) {

                entidade.setNumeroRomaneio(this.gerarNumeroRomaneio());

                Parametro parametro = this.parametroService.obterPorTipoParametro(TipoParametroEnum.CENTRO_SAP);

                if (ObjetoUtil.isNotNull(parametro)) {

                    entidade.setCentro(this.centroService.obterPorCodigo(parametro.getValor()));
                }
            }

            atualizarSaldoReservadoPedido(entidade);

            return super.salvar(entidade);

        } catch (NonUniqueResultException nonUniqueResultException) {

            throw new ServicoException("O romaneio já foi criado e está em processamento, aguarde um momento.");
        }
    }

    @Override
    public Romaneio salvarSemIntegracaoComSap(Romaneio entidade) throws Exception {
        atribuirUsuario(entidade);

        atribuirUsuario(entidade);

        validacoesRomaneio(entidade);

        if (entidade.getOperacao().isGestaoPatio()) {

            entidade = this.salvarConversao(entidade, false);

            entidade = this.salvarAnaliseQualidade(entidade);

        } else {

            entidade = this.salvarClassificacao(entidade);
        }

        entidade = this.salvarPesagem(entidade);

        if (entidade.getOperacao().isCriarRemessa()) {

            sapService.estornarRemessa(entidade, true);

            sapService.criarRemessa(entidade);

            sapService.criarOuModificarDocumentoTransporte(entidade);
        }

        Romaneio romaneio = salvarRomaneio(entidade);

        executarAutomacaoParaRomaneiosDoItssAgro(entidade.getIndiceEtapaRomaneio(), romaneio, CriterioTipoEnum.SUCESSO);

        return romaneio;
    }

    @Override
    public Romaneio salvarComRetorno(Romaneio entidade) throws Exception {

        try {

            atribuirUsuario(entidade);

            validacoesRomaneio(entidade);

            if (entidade.getOperacao().isGestaoPatio()) {

                entidade = this.salvarConversao(entidade, false);

                entidade = this.salvarAnaliseQualidade(entidade);

            } else {

                entidade = this.salvarClassificacao(entidade);
            }

            entidade = this.salvarPesagem(entidade);

            if (entidade.getOperacao().isCriarRemessa()) {

                sapService.estornarRemessa(entidade, true);

                sapService.criarRemessa(entidade);

                sapService.criarOuModificarDocumentoTransporte(entidade);
            }

            Romaneio romaneio = salvarRomaneio(entidade);

            executarAutomacaoParaRomaneiosDoItssAgro(entidade.getIndiceEtapaRomaneio(), romaneio, CriterioTipoEnum.SUCESSO);

            if ((!entidade.getCopiarDadosDeRomaneiosEstornados() && !entidade.getOperacao().isGestaoPatio()) || (StringUtil.isEmpty(entidade.getNumeroRomaneioOrigem()) && entidade.getOperacao().isGestaoPatio())) {

                this.sapService.enviarRomaneio(entidade, false);
            }

            return romaneio;

        } catch (Exception e) {

            e.printStackTrace();

            executarAutomacaoParaRomaneiosDoItssAgro(entidade.getIndiceEtapaRomaneio(), entidade, CriterioTipoEnum.FALHA);

            throw e;
        }
    }

    @Override
    public void enviarRomaneio(Romaneio entidade) throws Exception {
        atribuirUsuario(entidade);

        validacoesRomaneio(entidade);

        validarEtapas(entidade);

        if (entidade.getOperacao().isGestaoPatio()) {

            entidade = this.salvarConversao(entidade, true);

            entidade = this.salvarAnaliseQualidade(entidade);

        } else {

            entidade = this.salvarClassificacao(entidade);
        }

        entidade = this.salvarPesagem(entidade);

        if (entidade.getOperacao().isCriarRemessa()) {

            sapService.estornarRemessa(entidade, true);

            sapService.criarRemessa(entidade);

            sapService.criarOuModificarDocumentoTransporte(entidade);
        }

        Romaneio romaneio = salvarRomaneio(entidade);

        executarAutomacaoParaRomaneiosDoItssAgro(entidade.getIndiceEtapaRomaneio(), romaneio, CriterioTipoEnum.SUCESSO);

        if ((!entidade.getCopiarDadosDeRomaneiosEstornados() && !entidade.getOperacao().isGestaoPatio()) || (((ObjetoUtil.isNotNull(entidade.getFuncionalidade()) && FuncionalidadeEnum.ROMANEIO.equals(entidade.getFuncionalidade())) || ObjetoUtil.isNull(entidade.getSenha())) && StringUtil.isEmpty(entidade.getNumeroRomaneioOrigem()) && entidade.getOperacao().isGestaoPatio())) {

            this.sapService.enviarRomaneio(romaneio, false);
        }
    }

    @Override
    public SalvarRomaneioV2Response salvarRomaneioV2(Romaneio entidade, boolean validarEtapas, boolean salvarPesagem, boolean validarConversao) throws Exception {
        atribuirUsuario(entidade);

        validacoesRomaneio(entidade);

        if (validarEtapas) {

            validarEtapas(entidade);
        }

        if (entidade.getOperacao().isGestaoPatio()) {

            entidade = this.salvarConversao(entidade, validarConversao);

            entidade = this.salvarAnaliseQualidade(entidade);

        } else {

            entidade = this.salvarClassificacao(entidade);
        }

        if (salvarPesagem) {

            entidade = this.salvarPesagem(entidade);
        }

        if (entidade.getOperacao().isCriarRemessa()) {

            sapService.estornarRemessa(entidade, true);

            sapService.criarRemessa(entidade);

            sapService.criarOuModificarDocumentoTransporte(entidade);
        }

        Romaneio romaneio = salvarRomaneio(entidade);

        executarAutomacaoParaRomaneiosDoItssAgro(entidade.getIndiceEtapaRomaneio(), romaneio, CriterioTipoEnum.SUCESSO);

        boolean enviadoAoSAP = false;

        if ((!entidade.getCopiarDadosDeRomaneiosEstornados() && !entidade.getOperacao().isGestaoPatio()) || (((ObjetoUtil.isNotNull(entidade.getFuncionalidade()) && FuncionalidadeEnum.ROMANEIO.equals(entidade.getFuncionalidade())) || ObjetoUtil.isNull(entidade.getSenha())) && StringUtil.isEmpty(entidade.getNumeroRomaneioOrigem()) && entidade.getOperacao().isGestaoPatio())) {

            enviadoAoSAP = this.sapService.enviarRomaneio(romaneio, false);
        }

        byte[] pdfBytes = this.impressaoPorEtapa(romaneio);

        final Parametro parametro = this.parametroService.obterPorTipoParametro(TipoParametroEnum.TIPO_FLUXO);

        final Object mensagemFinal = StringUtil.isNotNullEmpty(entidade.getAlerta()) ?
                MessageFormat.format("Romaneio {0} salvo com sucesso, mas com um alerta: {1}", romaneio.getNumeroRomaneio(), entidade.getAlerta())
                : MessageFormat.format("Romaneio {0} salvo com sucesso!", romaneio.getNumeroRomaneio());

        return SalvarRomaneioV2Response.builder().entidade(romaneio)
                .habilitarLacragem(!StatusRomaneioEnum.PENDENTE.equals(entidade.getStatusRomaneio()) && entidade.getOperacao().isGestaoPatio() && ColecaoUtil.isNotEmpty(entidade.getItens()) && entidade.getItens().stream().findFirst().map(ItemNFPedido::getMaterial).map(Material::isLacrarCarga).orElse(false))
                .habilitarConversao(!StatusRomaneioEnum.PENDENTE.equals(entidade.getStatusRomaneio()) && entidade.getOperacao().isGestaoPatio() && ColecaoUtil.isNotEmpty(entidade.getItens()) && entidade.getItens().stream().findFirst().map(ItemNFPedido::getMaterial).map(Material::isRealizarConversaoLitragem).orElse(false))
                .habilitarPesagem(RomaneioUtil.isPossuiPesagem(romaneio) && !StatusRomaneioEnum.PENDENTE.equals(entidade.getStatusRomaneio()))
                .habilitarClassificacao(RomaneioUtil.isPossuiClassificacao(romaneio) && !StatusRomaneioEnum.PENDENTE.equals(entidade.getStatusRomaneio()))
                .enviadoAoSAP(enviadoAoSAP)
                .pdfBytes(pdfBytes)
                .tipoFluxo(parametro.getValor())
                .sucesso(mensagemFinal.toString()).build();
    }

    @Override
    public ObterOperacaoResponse obterOperacao(String operacao) {
        DadosSincronizacao dadosSincronizacao = dadosSincronizacaoService.obterPorOperacao(operacao);

        List<CampoOperacaoDTO> campos = campoOperacaoService.obterCamposPorOperacao(dadosSincronizacao.getEntidade());

        ObterOperacaoResponse.ObterOperacaoResponseBuilder builder = ObterOperacaoResponse.builder().operacao(dadosSincronizacao).campos(campos);

        if (DadosSincronizacaoEnum.REMESSA_GO206_PARA_UPG.equals(dadosSincronizacao.getEntidade())) {

            Parametro parametro = parametroService.obterPorTipoParametro(TipoParametroEnum.CODIGO_FILIAL);

            Produtor armazemFilialParaOp200 = produtorService.obterPorCodigo(parametro.getValor());

            builder.armazemFilial(armazemFilialParaOp200);
        }

        return builder.build();
    }

    @Override
    public ObterRomaneioPorSenhaResponse obterRomaneioPorSenha(Long senha) {
        Romaneio romaneio = obterPorSenha(new Senha(senha));

        List<CampoOperacaoDTO> campos = this.campoOperacaoService.obterCamposPorOperacao(romaneio.getOperacao().getEntidade());

        return ObterRomaneioPorSenhaResponse.builder().entidade(romaneio).campos(campos).build();
    }

    @Override
    public ObterRomaneioPorIdEEtapaResponse obterRomaneioPorIdEEtapa(Long id, EtapaEnum etapa) {
        Romaneio romaneio = this.get(id);

        List<CampoOperacaoDTO> campos = this.campoOperacaoService.obterCamposPorOperacao(romaneio.getOperacao().getEntidade());

        return ObterRomaneioPorIdEEtapaResponse.builder().entidade(romaneio).campos(campos).build();
    }

    private byte[] impressaoPorEtapa(Romaneio entidadeSaved) {

        if (entidadeSaved.getOperacao().isPossuiImpressaoPorEtapa()) {

            try {

                if (DadosSincronizacaoEnum.SIMPLES_ENTRADA.equals(entidadeSaved.getOperacao().getEntidade())) {

                    return this.classificacaoService.imprimirPdf(entidadeSaved, null);
                }

                EtapaRomaneioEnum etapaRomaneio = EtapaRomaneioEnum.values()[entidadeSaved.getIndiceEtapaRomaneio()];

                switch (etapaRomaneio) {

                    case ABA_ROMANEIO:
                    case ABA_PESAGEM: {

                        return imprimirPdf(entidadeSaved, null);
                    }

                    case ABA_CLASSIFICACAO: {

                        return this.classificacaoService.imprimirPdf(entidadeSaved, null);
                    }
                }

            } catch (JRException e) {

                e.printStackTrace();

            } catch (Exception e) {

                e.printStackTrace();
            }
        }

        return null;
    }

    private Romaneio salvarClassificacao(Romaneio entidade) {

        try {

            Boolean possuiClassificacaoDoRomaneioDeOrigem = DadosSincronizacaoEnum.possuiClassificacaoDependenteDeRomaneioDeOrigem(entidade.getOperacao().getEntidade());

            if ((possuiClassificacaoDoRomaneioDeOrigem || entidade.getOperacao().isPossuiClassificacao()) && ObjetoUtil.isNotNull(entidade.getClassificacao()) && RomaneioUtil.isClassificacaoPossuiIndices(entidade.getClassificacao())) {

                if (ObjetoUtil.isNull(entidade.getClassificacao().getUsuarioClassificador()) && ObjetoUtil.isNotNull(entidade.getIdUsuarioAutorAlteracao())) {

                    entidade.getClassificacao().setUsuarioClassificador(this.usuarioService.get(entidade.getIdUsuarioAutorAlteracao()));
                }

                entidade.setClassificacao(this.classificacaoService.salvar(entidade.getClassificacao()));

                this.validarItensClassificacaoRestritos(entidade, this.usuarioService.get(ObjetoUtil.isNotNull(entidade.getIdUsuarioAutorAlteracao()) ? entidade.getIdUsuarioAutorAlteracao() :
                        entidade.getIdUsuarioAutorCadastro()));

                entidade.setStatusRomaneio(StatusRomaneioEnum.EM_PROCESSAMENTO);

            } else {

                entidade.setClassificacao(null);
            }

        } catch (ItensClassificacaoException e) {

            entidade.setStatusRomaneio(StatusRomaneioEnum.PENDENTE_INDICES_CLASSIFICACAO);

            enviarEmailRomaneioBloqueadoPorIndiceClassificacao(entidade);

            return salvar(entidade);
        }

        return entidade;
    }

    private Romaneio salvarPesagem(Romaneio entidade) {

        if (entidade.getOperacao().isPossuiPesagem() && (ObjetoUtil.isNotNull(entidade.getPesagem()) && (BigDecimalUtil.isMaiorZero(entidade.getPesagem().getPesoInicial()) || BigDecimalUtil.isMaiorZero(entidade.getPesagem().getPesoFinal())))) {

            entidade.getPesagem().setDadosSincronizacaoEnum(entidade.getOperacao().getEntidade());

            if (ObjetoUtil.isNotNull(entidade.getPesagem().getPlacaCavalo()) && ObjetoUtil.isNull(entidade.getPesagem().getPlacaCavalo().getId())) {

                entidade.setPlacaCavalo(null);
            }

            if (StringUtil.isNotNullEmpty(entidade.getNumeroRomaneio())) {

                entidade.getPesagem().setNumeroRomaneio(entidade.getNumeroRomaneio());
            }

            atribuirUsuario(entidade.getPesagem());

            entidade.setPesagem(this.pesagemService.salvar(entidade.getPesagem()));

        } else {

            entidade.setPesagem(null);
        }

        return entidade;
    }

    private Romaneio salvarConversao(Romaneio entidade, boolean validarConversao) throws ServicoException {

        if (ObjetoUtil.isNotNull(entidade.getConversaoLitragem()) && entidade.getConversaoLitragem().isNotNull()) {

            final Parametro parametro = this.parametroService.obterPorTipoParametro(TipoParametroEnum.TOLERANCIA_NA_PESAGEM_BRUTO_ETANOL);

            entidade.getConversaoLitragem().setValidarConversao(validarConversao);

            this.conversaoLitragemService.salvar(entidade, parametro);

            try {

                this.setarConversaoLitragem(entidade.getConversaoLitragem().getId(), entidade.getNumeroRomaneio());

            } catch (Exception e) {

                e.printStackTrace();

                throw new ServicoException("Ocorreu um erro inesperado ao salvar a conversão de litragem, por favor, deixe somente uma aba da aplicação aberta, atualize a mesma e faça a conversão novamente.");
            }

        } else {

            entidade.setConversaoLitragem(null);
        }

        return entidade;
    }

    private Romaneio salvarAnaliseQualidade(Romaneio entidade) {
        try {

            if (entidade.getOperacao().isPossuiClassificacao() && ColecaoUtil.isNotEmpty(entidade.getAnalises())) {

                if (ColecaoUtil.isNotEmpty(entidade.getRestricoes())) {
                    entidade.getRestricoes().removeIf(r -> TipoRestricaoEnum.ANALISE_QUALIDADE.equals(r.getTipoRestricao()));
                }

                entidade.getAnalises().forEach(a -> a.setRomaneio(entidade));

                for (final AnaliseQualidade analiseQualidade : entidade.getAnalises()) {

                    if (ObjetoUtil.isNotNull(analiseQualidade.getLoteAnaliseQualidade()) && ObjetoUtil.isNull(analiseQualidade.getLoteAnaliseQualidade().getId())) {

                        analiseQualidade.setLoteAnaliseQualidade(null);
                    }

                    this.validarItensDaAnalise(analiseQualidade);
                }

                if (ColecaoUtil.isEmpty(entidade.getRestricoes())) {

                    entidade.setStatusRomaneio(StatusRomaneioEnum.EM_PROCESSAMENTO);
                }
            }

        } catch (Exception e) {

            if (ColecaoUtil.isEmpty(entidade.getRestricoes())) {

                entidade.setRestricoes(new ArrayList<>());
            }

            entidade.getRestricoes().add(new RestricaoOrdem(e.getMessage(), null, TipoRestricaoEnum.ANALISE_QUALIDADE, entidade));

            entidade.setStatusRomaneio(StatusRomaneioEnum.PENDENTE);
        }

        return entidade;
    }

    private void validarItensDaAnalise(final AnaliseQualidade analiseQualidade) throws Exception {

        if (ObjetoUtil.isNotNull(analiseQualidade) && ColecaoUtil.isNotEmpty(analiseQualidade.getItens())) {

            for (final AnaliseQualidadeItem item : analiseQualidade.getItens()) {

                if (StringUtil.isNotNullEmpty(item.getResultado())) {

                    String condicao = StringUtil.empty();

                    if (StringUtil.isNotNullEmpty(item.getAnaliseItem().getValorInicial()) && StringUtil.isNotNullEmpty(item.getAnaliseItem().getValorFinal())) {

                        condicao = "RESULTADO >= CONDICAO_UM && RESULTADO <= CONDICAO_DOIS";
                    } else {

                        condicao = String.format("RESULTADO %s CONDICAO_UM", item.getAnaliseItem().getCondicao().getOperador());
                    }

                    ScriptEngineManager scriptEngineManager = new ScriptEngineManager();

                    ScriptEngine scriptEngine = scriptEngineManager.getEngineByName("javascript");

                    Bindings bindings = scriptEngine.getBindings(ScriptContext.ENGINE_SCOPE);

                    bindings.put("RESULTADO", BigDecimalUtil.converterStringParaDigitoSeForNumerico(item.getResultado().trim().toUpperCase()));

                    bindings.put("CONDICAO_UM", BigDecimalUtil.converterStringParaDigitoSeForNumerico(item.getAnaliseItem().getValorInicial().trim().toUpperCase()));

                    if (StringUtil.isNotNullEmpty(item.getAnaliseItem().getValorInicial()) && StringUtil.isNotNullEmpty(item.getAnaliseItem().getValorFinal())) {

                        bindings.put("CONDICAO_DOIS", BigDecimalUtil.converterStringParaDigitoSeForNumerico(item.getAnaliseItem().getValorFinal().trim().toUpperCase()));
                    }

                    if (!(Boolean) scriptEngine.eval(condicao)) {

                        throw new Exception(MessageSupport.getMessage("MSGE023"));
                    }
                }
            }
        }
    }

    private void executarAutomacaoParaRomaneiosDoItssAgro(int indiceEtapaRomaneio, Romaneio romaneio, CriterioTipoEnum criterioTipo) {

        if (!romaneio.getOperacao().isGestaoPatio()) {

            EtapaRomaneioEnum etapaRomaneio = EtapaRomaneioEnum.values()[indiceEtapaRomaneio];

            EtapaEnum etapaAtual = null;

            switch (etapaRomaneio) {
                case ABA_ROMANEIO: {
                    etapaAtual = EtapaEnum.CRIAR_ROMANEIO;
                    break;
                }
                case ABA_CLASSIFICACAO:
                    etapaAtual = EtapaEnum.ANALISE_QUALIDADE;
                    break;
            }

            if (ObjetoUtil.isNotNull(etapaAtual)) {

                this.dashboardFilaService.validarEventosDaAutomacao(romaneio, etapaAtual, criterioTipo);
            }
        }
    }

    @Override
    public void atualizarSaldoReservadoPedido(Romaneio entidade) {
        if (Arrays.asList(DadosSincronizacaoEnum.OPERACAO_TRANSFERENCIA, DadosSincronizacaoEnum.TRANSFERENCIA_GERAIS).contains(entidade.getOperacao().getEntidade())) {
            ItemNFPedido itemNFPedido = entidade.getItens().get(0);
            switch (entidade.getStatusRomaneio()) {
                case EM_PROCESSAMENTO:
                    BigDecimal valor = itemNFPedido.getQuantidadeCarregar();

                    if (itemNFPedido.getMaterial().isRealizarConversaoLitragem() && ObjetoUtil.isNotNull(entidade.getConversaoLitragem())
                            && ColecaoUtil.isNotEmpty(entidade.getConversaoLitragem().getConversoes())) {
                        List<ConfiguracaoUnidadeMedidaMaterial> unidades = this.sapService.obterMedidasParaConversao(itemNFPedido.getMaterial());

                        ConversaoLitragemItem conversaoLitragemItem = entidade.getConversaoLitragem().getConversoes().stream()
                                .sorted(Comparator.comparing(ConversaoLitragemItem::getDataCadastro).reversed()).findFirst().orElse(null);

                        BigDecimal resultadoConversaoLitragem = conversaoLitragemItem.getVolumeSetaVinteGraus().setScale(0, RoundingMode.HALF_UP);

                        valor = this.sapService.converterQuantidade(unidades, "L", itemNFPedido.getUnidadeMedidaOrdem(), resultadoConversaoLitragem.doubleValue());

                    } else if (ObjetoUtil.isNotNull(entidade.getPesagem()) && ObjetoUtil.isNotNull(entidade.getPesagem().getPesoInicial()) && ObjetoUtil.isNotNull(entidade.getPesagem().getPesoFinal())) {
                        List<ConfiguracaoUnidadeMedidaMaterial> unidades = this.sapService.obterMedidasParaConversao(itemNFPedido.getMaterial());

                        BigDecimal pesoLiquido = entidade.getPesagem().getPesoInicial().subtract(entidade.getPesagem().getPesoFinal()).setScale(0, RoundingMode.HALF_UP);

                        valor = this.sapService.converterQuantidade(unidades, "KG", itemNFPedido.getUnidadeMedidaOrdem(), pesoLiquido.doubleValue());
                    }

                    this.saldoReservadoPedidoService.reservarSaldoPedido(entidade.getNumeroRomaneio(), itemNFPedido.getNumeroPedido(), itemNFPedido.getItemPedido(), valor);
                    break;
                default:
                    this.saldoReservadoPedidoService.liberarSaldoPedido(entidade.getNumeroRomaneio(), itemNFPedido.getNumeroPedido(), itemNFPedido.getItemPedido());
                    break;
            }
        }
    }

    @Override
    public void preSalvar(Romaneio entidade) {

        if (ColecaoUtil.isEmpty(entidade.getRestricoes())) {

            if ((DadosSincronizacaoEnum.SIMPLES_ENTRADA.equals(entidade.getOperacao().getEntidade()) && RomaneioUtil.isRomaneioFoiClassificado(entidade) && !StatusRomaneioEnum.PENDENTE_INDICES_CLASSIFICACAO.equals(entidade.getStatusRomaneio())) || DadosSincronizacaoEnum.ENTRADA_ARMAZEM_DE_TERCEIROS.equals(entidade.getOperacao().getEntidade())) {

                entidade.setStatusRomaneio(StatusRomaneioEnum.CONCLUIDO);

            } else if (!StatusRomaneioEnum.PENDENTE_INDICES_CLASSIFICACAO.equals(entidade.getStatusRomaneio()) && !StatusRomaneioEnum.PENDENTE.equals(entidade.getStatusRomaneio()) && !StatusRomaneioEnum.CONCLUIDO.equals(entidade.getStatusRomaneio())) {

                entidade.setStatusRomaneio(StatusRomaneioEnum.EM_PROCESSAMENTO);
            }
        }

        if (entidade.getOperacao().isPossuiDivisaoCarga()) {

            entidade.getItens().forEach(itemNFPedido -> itemNFPedido.setRomaneio(entidade));
        }

        if (entidade.getOperacao().isPossuiClassificacaoOrigem() && ColecaoUtil.isNotEmpty(entidade.getItensClassificacaoOrigem())) {

            entidade.getItensClassificacaoOrigem().forEach(itensClassificacao -> itensClassificacao.setRomaneio(entidade));
        }

        if (ColecaoUtil.isNotEmpty(entidade.getRomaneioHistoricoAprovacaoIndices())) {

            entidade.getRomaneioHistoricoAprovacaoIndices().forEach(romaneioHistoricoAprovacaoIndice -> romaneioHistoricoAprovacaoIndice.setRomaneio(entidade));
        }

        if (!entidade.getOperacao().isPossuiPesagem()) {

            entidade.setPesagem(null);
        }

        if (!entidade.getOperacao().isPossuiClassificacao()) {

            entidade.setClassificacao(null);
        }

        if (entidade.getOperacao().isGestaoPatio()) {

            entidade.getItens().stream().filter(itemNFPedido -> !ObjetoUtil.isFieldsChanged(itemNFPedido.getDeposito())).forEach(itemNFPedido -> itemNFPedido.setDeposito(null));

            entidade.setClassificacao(null);

            entidade.getRestricoes().forEach(r -> r.setRomaneio(entidade));

            if (entidade.getOperacao().isPossuiClassificacaoOrigem() && ColecaoUtil.isNotEmpty(entidade.getItensClassificacaoOrigem())) {

                entidade.getItensClassificacaoOrigem().forEach(itensClassificacao -> itensClassificacao.setRomaneio(entidade));
            }

            if (!ObjetoUtil.isFieldsChanged(entidade.getSafra())) {

                entidade.setSafra(null);
            }

            if (!ObjetoUtil.isFieldsChanged(entidade.getCentro())) {

                entidade.setCentro(null);
            }

            if (!ObjetoUtil.isFieldsChanged(entidade.getPlacaCavalo())) {

                entidade.setPlacaCavalo(null);
            }

            if (!ObjetoUtil.isFieldsChanged(entidade.getSenha()) || (ObjetoUtil.isNotNull(entidade.getSenha()) && ObjetoUtil.isNull(entidade.getSenha().getId()))) {

                entidade.setSenha(null);
            }

            if (!ObjetoUtil.isFieldsChanged(entidade.getCliente())) {

                entidade.setCliente(null);
            }

            if (ObjetoUtil.isFieldsChanged(entidade.getPesagem())) {

                entidade.getPesagem().setSafra(entidade.getSafra());

                entidade.getPesagem().setPlacaCavalo(entidade.getPlacaCavalo());

            } else {

                entidade.setPesagem(null);
            }

            if (!ObjetoUtil.isFieldsChanged(entidade.getMotorista())) {

                entidade.setMotorista(null);
            }

            if (!ObjetoUtil.isFieldsChanged(entidade.getTransportadora())) {

                entidade.setTransportadora(null);
            }

            if (!ObjetoUtil.isFieldsChanged(entidade.getMaterial())) {

                entidade.setMaterial(null);
            }

            if (!ObjetoUtil.isFieldsChanged(entidade.getEmissaoDocumento())) {

                entidade.setEmissaoDocumento(null);
            }

            if (!ObjetoUtil.isFieldsChanged(entidade.getConferencia())) {

                entidade.setConferencia(null);
            }

            if (!ObjetoUtil.isFieldsChanged(entidade.getPendencia())) {

                entidade.setPendencia(null);
            }

            if (!ObjetoUtil.isFieldsChanged(entidade.getCarregamento())) {

                entidade.setCarregamento(null);
            }

            if (!ObjetoUtil.isFieldsChanged(entidade.getLacragem())) {

                entidade.setLacragem(null);

            } else {

                inicializarLacreStatusLacre(entidade);
            }

            if (ColecaoUtil.isNotEmpty(entidade.getAnalises())) {

                entidade.getAnalises().forEach(a -> a.setRomaneio(entidade));
            }

            if (ColecaoUtil.isNotEmpty(entidade.getRetornoNfes())) {

                entidade.getRetornoNfes().forEach(r -> r.setRomaneio(entidade));
            }
        }
    }

    @Override
    public void inicializarLacreStatusLacre(Romaneio entidade) {

        if (ObjetoUtil.isNotNull(entidade.getLacragem()) && ColecaoUtil.isNotEmpty(entidade.getLacragem().getLacres())) {

            entidade.getLacragem().getLacres().forEach(lacre -> {

                lacre.setLacragem(entidade.getLacragem());

                lacre.getLacre().setLote(lacreService.get(lacre.getLacre().getId()).getLote());

                boolean atualizarStatusLacre = false;

                if (!StatusLacreRomaneioEnum.INUTILIZADO.equals(lacre.getStatusLacreRomaneio())) {

                    switch (entidade.getStatusRomaneio()) {

                        case EM_PROCESSAMENTO:
                            atualizarStatusLacre = true;
                            lacre.setStatusLacreRomaneio(StatusLacreRomaneioEnum.VINCULADO);
                            lacre.getLacre().setStatusLacre(StatusLacreEnum.VINCULADO);
                            break;

                        case CONCLUIDO:
                            atualizarStatusLacre = true;
                            lacre.setStatusLacreRomaneio(StatusLacreRomaneioEnum.VINCULADO);
                            if (entidade.getOperacao().getEntidade().equals(DadosSincronizacaoEnum.OPERACAO_SIMPLES_PESAGEM) || (ObjetoUtil.isNotNull(entidade.getStatusIntegracaoSAP()) && StatusIntegracaoSAPEnum.COMPLETO.equals(entidade.getStatusIntegracaoSAP()))) {
                                lacre.getLacre().setStatusLacre(StatusLacreEnum.UTILIZADO);
                            } else {
                                lacre.getLacre().setStatusLacre(StatusLacreEnum.VINCULADO);
                            }
                            break;

                        case ESTORNADO:
                            atualizarStatusLacre = true;
                            lacre.setStatusLacreRomaneio(StatusLacreRomaneioEnum.INUTILIZADO);
                            lacre.getLacre().setStatusLacre(StatusLacreEnum.DISPONIVEL);
                            break;
                    }

                } else {

                    atualizarStatusLacre = true;

                    if (!StatusLacreEnum.DISPONIVEL.equals(lacre.getLacre().getStatusLacre())) {

                        lacre.getLacre().setStatusLacre(StatusLacreEnum.INUTILIZADO);
                    }
                }

                if (atualizarStatusLacre) {

                    lacreService.salvar(lacre.getLacre());
                }
            });
        }
    }

    @Override
    public void validarSaldoOrdemDeVenda(Romaneio entidade) throws OrdemVendaSemSaldoException {

        if (entidade.getOperacao().isGestaoPatio() && ColecaoUtil.isNotEmpty(entidade.getItens()) && RomaneioUtil.isRomaneioFoiPesado(entidade) && entidade.getItens().stream().map(ItemNFPedido::getCodigoOrdemVenda).filter(i -> StringUtil.isNotNullEmpty(i)).count() == entidade.getItens().size()) {

            if (ColecaoUtil.isNotEmpty(entidade.getRestricoes())) {

                entidade.getRestricoes().removeIf(restricaoOrdem -> TipoRestricaoEnum.SALDO_ORDEM_VENDA.equals(restricaoOrdem.getTipoRestricao()));
            }

            Material material = entidade.getItens().stream().map(ItemNFPedido::getMaterial).filter(Objects::nonNull).findFirst().get();

            String unidadeOrdem = entidade.getItens().stream().map(ItemNFPedido::getUnidadeMedidaOrdem).filter(Objects::nonNull).findFirst().get();

            List<ConfiguracaoUnidadeMedidaMaterial> unidades = sapService.obterMedidasParaConversao(material);

            BigDecimal totalOrdemConvertidoPelaUnidadeMaterial = BigDecimal.ZERO;

            if (!unidades.stream().findFirst().get().getQuantidadeEquivalente().equals(unidadeOrdem)) {

                HashMap<String, AtomicLong> stringAtomicLongHashMap = RomaneioUtil.converterPesoLiquidoParaUnidadeMedidaDaOrdem(entidade);

                totalOrdemConvertidoPelaUnidadeMaterial = sapService.converterQuantidade(unidades, stringAtomicLongHashMap.keySet().stream().findFirst().get(),
                        stringAtomicLongHashMap.get(stringAtomicLongHashMap.keySet().stream().findFirst().get()).doubleValue());
            }

            if (BigDecimalUtil.isMaiorQueZero(new BigDecimal(totalOrdemConvertidoPelaUnidadeMaterial.doubleValue()))) {

                BigDecimal totalSaldo = sapService.converterQuantidade(unidades, unidadeOrdem, entidade.getItens().stream().map(ItemNFPedido::getSaldoOrdemVenda).reduce(BigDecimal.ZERO, BigDecimal::add).doubleValue());

                if (BigDecimalUtil.isMaiorQue(totalOrdemConvertidoPelaUnidadeMaterial, totalSaldo)) {

                    entidade.setStatusRomaneio(StatusRomaneioEnum.PENDENTE);

                    if (ObjetoUtil.isNull(entidade.getRestricoes())) {

                        entidade.setRestricoes(new ArrayList<>());
                    }

                    entidade.getRestricoes().add(new RestricaoOrdem(MessageSupport.getMessage("MSGE030"), StringUtil.empty(), TipoRestricaoEnum.SALDO_ORDEM_VENDA, entidade));
                }
            }

            if (ColecaoUtil.isNotEmpty(entidade.getRestricoes()) && entidade.getRestricoes().stream().filter(restricaoOrdem -> TipoRestricaoEnum.SALDO_ORDEM_VENDA.equals(restricaoOrdem.getTipoRestricao())).count() > 0) {

                throw new OrdemVendaSemSaldoException();
            }
        }
    }

    @Override
    public void validarSaldoProdutoAcabado(Romaneio entidade) {

        if (entidade.getOperacao().isGestaoPatio() && ColecaoUtil.isNotEmpty(entidade.getItens()) && entidade.getItens().stream().map(ItemNFPedido::getCodigoOrdemVenda).filter(StringUtil::isNotNullEmpty).count() == entidade.getItens().size()) {

            if (ColecaoUtil.isNotEmpty(entidade.getRestricoes())) {

                entidade.getRestricoes().removeIf(restricaoOrdem -> TipoRestricaoEnum.SALDO_PRODUTO_ACABADO.equals(restricaoOrdem.getTipoRestricao()));
            }

            final Material material = entidade.getItens().stream().map(ItemNFPedido::getMaterial).filter(Objects::nonNull).findFirst().orElse(null);

            HashMap<String, BigDecimal> floatHashMap = obterValorASerComparadaParaValidarSaldoProduto(entidade, material);

            List<ConfiguracaoUnidadeMedidaMaterial> unidades = sapService.obterMedidasParaConversao(material);

            String unidadeACarregar = floatHashMap.keySet().stream().findFirst().get();

            BigDecimal valorASerComparado = floatHashMap.get(unidadeACarregar);

            if (!unidades.stream().findFirst().get().getQuantidadeEquivalente().equals(material.getUnidadeMedida())) {

                valorASerComparado = sapService.converterQuantidade(unidades, unidadeACarregar, valorASerComparado.doubleValue());
            }

            if (BigDecimalUtil.isMaiorZero(BigDecimal.valueOf(valorASerComparado.doubleValue()))) {

                BigDecimal totalSaldo = sapService.converterQuantidade(unidades, material.getUnidadeMedida(),
                        entidade.getItens().stream().map(ItemNFPedido::getSaldoEstoqueProdutoAcabado).filter(Objects::nonNull).findFirst().orElse(BigDecimal.ZERO).doubleValue());

                if (BigDecimalUtil.isMaiorQue(valorASerComparado, totalSaldo)) {

                    entidade.setStatusRomaneio(StatusRomaneioEnum.PENDENTE);

                    if (ObjetoUtil.isNull(entidade.getRestricoes())) {

                        entidade.setRestricoes(new ArrayList<>());
                    }

                    entidade.getRestricoes().add(new RestricaoOrdem(MessageSupport.getMessage("MSGE032"), StringUtil.empty(), TipoRestricaoEnum.SALDO_PRODUTO_ACABADO, entidade));
                }
            }
        }
    }

    private HashMap<String, BigDecimal> obterValorASerComparadaParaValidarSaldoProduto(Romaneio entidade, Material material) {

        BigDecimal valorASerComparado;

        HashMap<String, BigDecimal> unidadeEValor = new HashMap<>();

        if (entidade.getItens().size() > 1 && RomaneioUtil.isRomaneioFoiPesado(entidade)) {

            valorASerComparado = entidade.getItens().stream().map(ItemNFPedido::getRateio).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add);

            unidadeEValor.put("KG", valorASerComparado);

        } else if (material.isRealizarConversaoLitragem() && RomaneioUtil.isConversaoRealizada(entidade)) {

            valorASerComparado =
                    entidade.getConversaoLitragem().getConversoes().stream().sorted((i1, i2) -> Long.compare(i2.getDataCadastro().getTime(), i1.getDataCadastro().getTime())).map(ConversaoLitragemItem::getVolumeSetaVinteGraus).findFirst().orElse(BigDecimal.ZERO);

            unidadeEValor.put("L", valorASerComparado);

        } else if (RomaneioUtil.isRomaneioFoiPesado(entidade)) {

            BigDecimal pesoLiquido = entidade.getPesagem().getPesoInicial().subtract(entidade.getPesagem().getPesoFinal()).setScale(0, RoundingMode.HALF_UP);

            unidadeEValor.put("KG", pesoLiquido);

        } else {

            valorASerComparado = entidade.getItens().stream().map(ItemNFPedido::getQuantidadeCarregar).filter(Objects::nonNull).reduce(BigDecimal.ZERO, BigDecimal::add);

            unidadeEValor.put(entidade.getItens().stream().map(ItemNFPedido::getUnidadeMedidaOrdem).findFirst().get(), valorASerComparado);
        }

        return unidadeEValor;
    }

    @Override
    public Specification<Romaneio> obterEspecification(final Paginacao<Romaneio> paginacao) {

        return (root, query, builder) -> {

            Join<Romaneio, DadosSincronizacao> joinOperacao = root.join(Romaneio_.operacao, JoinType.LEFT);

            Join<Romaneio, ItemNFPedido> joinItemNf = root.join(Romaneio_.itens, JoinType.LEFT);

            Join<Romaneio, RetornoNFe> joinRetornoNFe = root.join(Romaneio_.retornoNfes, JoinType.LEFT);

            Join<Romaneio, Veiculo> joinVeiculoCabecalho = root.join(Romaneio_.placaCavalo, JoinType.LEFT);

            Join<Romaneio, Produtor> joinProdutorCabecalho = root.join(Romaneio_.produtor, JoinType.LEFT);

            Join<Romaneio, Material> joinMaterialCabecalho = root.join(Romaneio_.material, JoinType.LEFT);

            Join<ItemNFPedido, Cliente> joinClienteItem = joinItemNf.join(ItemNFPedido_.cliente, JoinType.LEFT);

            Join<ItemNFPedido, Cliente> joinOperadorLogisticoItem = joinItemNf.join(ItemNFPedido_.operadorLogistico, JoinType.LEFT);

            Join<ItemNFPedido, Veiculo> joinVeiculoItem = joinItemNf.join(ItemNFPedido_.placaCavalo, JoinType.LEFT);

            Join<ItemNFPedido, Produtor> joinProdutorItem = joinItemNf.join(ItemNFPedido_.produtor, JoinType.LEFT);

            Join<ItemNFPedido, Material> joinMaterialItem = joinItemNf.join(ItemNFPedido_.material, JoinType.LEFT);

            final Romaneio entidade = paginacao.getEntidade();

            List<Predicate> predicates = new ArrayList<>();

            predicates.add(builder.and(builder.equal(joinOperacao.get(DadosSincronizacao_.gestaoPatio), entidade.isApenasOpGestao())));

            if (entidade.isApenasOpGestao()) {

                if (entidade.isRomaneioComPesagem()) {

                    predicates.add(builder.and(builder.equal(joinOperacao.get(DadosSincronizacao_.possuiPesagem), true), builder.notEqual(root.get(Romaneio_.statusRomaneio), StatusRomaneioEnum.PENDENTE)));
                }

                if (entidade.isRomaneioComAnalise()) {

                    predicates.add(builder.and(builder.equal(joinOperacao.get(DadosSincronizacao_.possuiClassificacao), true), builder.notEqual(root.get(Romaneio_.statusRomaneio), StatusRomaneioEnum.PENDENTE),
                            builder.equal(joinMaterialItem.get(Material_.realizarAnaliseQualidade), true)));
                }

                if (entidade.isRomaneioComConversao()) {

                    predicates.add(builder.and(builder.notEqual(root.get(Romaneio_.statusRomaneio), StatusRomaneioEnum.PENDENTE), builder.equal(joinMaterialItem.get(Material_.realizarConversaoLitragem), true)));
                }

                if (entidade.isRomaneioComLacragem()) {

                    predicates.add(builder.and(builder.notEqual(root.get(Romaneio_.statusRomaneio), StatusRomaneioEnum.PENDENTE), builder.equal(joinMaterialItem.get(Material_.lacrarCarga), true)));
                }

                if (entidade.isRomaneioComCarregamento()) {

                    predicates.add(builder.and(builder.notEqual(root.get(Romaneio_.statusRomaneio), StatusRomaneioEnum.PENDENTE), builder.equal(joinMaterialItem.get(Material_.possuiEtapaCarregamento), true)));
                }

                if (entidade.isRomaneioComPesagem()) {

                    predicates.add(builder.and(builder.equal(joinOperacao.get(DadosSincronizacao_.possuiPesagem), true)));
                }
            }

            if (entidade.isRomaneioComPesagem()) {

                predicates.add(builder.and(builder.equal(joinOperacao.get(DadosSincronizacao_.possuiPesagem), true)));

                if (ObjetoUtil.isNotNull(entidade.getFiltroOperadorLogistico())) {

                    predicates.add(builder.and(builder.equal(joinOperadorLogisticoItem.get(Cliente_.id), entidade.getFiltroOperadorLogistico().getId())));
                }
            }

            if (entidade.isRomaneioComClassificacao()) {

                predicates.add(builder.and(builder.equal(joinOperacao.get(DadosSincronizacao_.possuiClassificacao), true)));
            }

            if (StringUtil.isNotNullEmpty(entidade.getNumeroRomaneio())) {

                predicates.add(builder.equal(root.get(Romaneio_.numeroRomaneio), entidade.getNumeroRomaneio()));
            }

            if (ObjetoUtil.isNotNull(entidade.getDataInicioFiltro()) && ObjetoUtil.isNotNull(entidade.getDataFimFiltro())) {

                predicates.add(builder.between(root.get(Romaneio_.dataCadastro), DateUtil.obterDataHoraZerada(entidade.getDataInicioFiltro()), DateUtil.converterDateFimDia(entidade.getDataFimFiltro())));

            } else if (ObjetoUtil.isNotNull(entidade.getDataInicioFiltro())) {

                predicates.add(builder.greaterThanOrEqualTo(root.get(Romaneio_.dataCadastro), DateUtil.obterDataHoraZerada(entidade.getDataInicioFiltro())));

            } else if (ObjetoUtil.isNotNull(entidade.getDataFimFiltro())) {

                predicates.add(builder.lessThanOrEqualTo(root.get(Romaneio_.dataCadastro), DateUtil.converterDateFimDia(entidade.getDataFimFiltro())));
            }

            if (ObjetoUtil.isNotNull(entidade.getOperacao())) {

                predicates.add(builder.equal(joinOperacao.get(DadosSincronizacao_.operacao), entidade.getOperacao().getOperacao()));
            }

            if (StringUtil.isNotNullEmpty(entidade.getPlacaFiltro())) {

                final Predicate predicateItem = builder.equal(joinVeiculoItem.get(Veiculo_.placa1), StringUtil.removerCaracteresEspeciais(entidade.getPlacaFiltro()).toUpperCase());

                final Predicate predicateCabecalho = builder.equal(joinVeiculoCabecalho.get(Veiculo_.placa1), StringUtil.removerCaracteresEspeciais(entidade.getPlacaFiltro()).toUpperCase());

                predicates.add(builder.or(predicateCabecalho, predicateItem));
            }

            if (ObjetoUtil.isNotNull(entidade.getProdutor())) {

                final Predicate predicateItem = builder.equal(joinProdutorItem.get(Produtor_.id), entidade.getProdutor().getId());

                final Predicate predicateCabecalho = builder.equal(joinProdutorCabecalho.get(Produtor_.id), entidade.getProdutor().getId());

                final Predicate predicateProdutorSimplesPesagem = builder.like(builder.upper(root.get(Romaneio_.nomeSimplesPesagem)), "%" + entidade.getProdutor().getNome().toUpperCase() + "%");

                predicates.add(builder.or(predicateItem, predicateCabecalho, predicateProdutorSimplesPesagem));
            }

            if (ObjetoUtil.isNotNull(entidade.getCliente())) {

                predicates.add(builder.or(builder.equal(joinClienteItem.get(Cliente_.id), entidade.getCliente().getId())));
            }

            if (StringUtil.isNotNullEmpty(entidade.getMaterialFiltro())) {

                final Predicate predicateItem = builder.or(builder.like(builder.upper(joinMaterialItem.get(Material_.codigo)), "%" + entidade.getMaterialFiltro().toUpperCase() + "%"),
                        builder.like(builder.upper(joinMaterialItem.get(Material_.descricao)), "%" + entidade.getMaterialFiltro().toUpperCase() + "%"));

                final Predicate predicateCabecalho = builder.or(builder.like(builder.upper(joinMaterialCabecalho.get(Material_.codigo)), "%" + entidade.getMaterialFiltro().toUpperCase() + "%"),
                        builder.like(builder.upper(joinMaterialCabecalho.get(Material_.descricao)), "%" + entidade.getMaterialFiltro().toUpperCase() + "%"));

                final Predicate predicateMaterialSimplesPesagem = builder.like(builder.upper(root.get(Romaneio_.materialSimplesPesagem)), "%" + entidade.getMaterialFiltro().toUpperCase() + "%");

                predicates.add(builder.or(predicateItem, predicateCabecalho, predicateMaterialSimplesPesagem));

            } else if (ObjetoUtil.isNotNull(entidade.getMaterial())) {

                if (StringUtil.isNotNullEmpty(entidade.getMaterial().getCodigo()) && StringUtil.isNotNullEmpty(entidade.getMaterial().getDescricao())) {

                    final Predicate predicateItem = builder.or(builder.like(builder.upper(joinMaterialItem.get(Material_.codigo)), "%" + entidade.getMaterial().getCodigo().toUpperCase() + "%"),
                            builder.like(builder.upper(joinMaterialItem.get(Material_.descricao)), "%" + entidade.getMaterial().getDescricao().toUpperCase() + "%"));

                    final Predicate predicateCabecalho = builder.or(builder.like(builder.upper(joinMaterialCabecalho.get(Material_.codigo)), "%" + entidade.getMaterial().getCodigo().toUpperCase() + "%"),
                            builder.like(builder.upper(joinMaterialCabecalho.get(Material_.descricao)), "%" + entidade.getMaterial().getDescricao().toUpperCase() + "%"));

                    predicates.add(builder.or(predicateItem, predicateCabecalho));
                }

                if (entidade.isApenasOpGestao() && entidade.getMaterial().isLacrarCarga()) {

                    final Predicate predicateItemLacragemCarga = builder.equal(joinMaterialItem.get(Material_.lacrarCarga), true);

                    predicates.add(predicateItemLacragemCarga);
                }
            }

            if (ObjetoUtil.isNotNull(entidade.getSenha()) && ObjetoUtil.isNotNull(entidade.getSenha().getEtapaAtual()) && ObjetoUtil.isNotNull(entidade.getSenha().getEtapaAtual().getEtapa())) {

                predicates.add(builder.equal(root.join(Romaneio_.senha).join(Senha_.etapaAtual).join(SenhaEtapa_.etapa).get(FluxoEtapa_.etapa), entidade.getSenha().getEtapaAtual().getEtapa().getEtapa()));
            }

            if (ObjetoUtil.isNotNull(entidade.getStatusPedido())) {

                predicates.add(builder.equal(root.get(Romaneio_.statusPedido), entidade.getStatusPedido()));
            }
            if (ObjetoUtil.isNotNull(entidade.getStatusRomaneio())) {

                predicates.add(builder.equal(root.get(Romaneio_.statusRomaneio), entidade.getStatusRomaneio()));
            }

            if (ObjetoUtil.isNotNull(entidade.getStatusIntegracaoSAP())) {
                predicates.add(builder.equal(root.get(Romaneio_.statusIntegracaoSAP), entidade.getStatusIntegracaoSAP()));
            }

            if (StringUtil.isNotNullEmpty(entidade.getCodigoOrdemVenda())) {
                predicates.add(builder.equal(joinItemNf.get(ItemNFPedido_.CODIGO_ORDEM_VENDA), entidade.getCodigoOrdemVenda()));
            }

            if (StringUtil.isNotNullEmpty(entidade.getNumeroNotaFiscal())) {
                predicates.add(builder.equal(joinRetornoNFe.get(RetornoNFe_.NFE), StringUtils.leftPad(entidade.getNumeroNotaFiscal(), 9, "0")));
            }

            return builder.and(predicates.toArray(new Predicate[]{}));
        };
    }

    private String obterUltimoNumeroRomaneio() {

        return this.getDAO().getMaxId().toString();
    }

    private String gerarNumeroRomaneio() {

        final Parametro parametro = this.parametroService.obterPorTipoParametro(TipoParametroEnum.CENTRO_SAP);

        String codigo = this.obterUltimoNumeroRomaneio();

        final String centro = parametro.getValor();

        final Integer numeroSequencia = Integer.parseInt(codigo) + 1;

        final int totalCaracter = centro.length() + String.valueOf(numeroSequencia).length();

        final int totalFaltando = 10 - totalCaracter;

        return centro + StringUtil.completarZeroAEsquerda(totalFaltando + String.valueOf(numeroSequencia).length(), String.valueOf(numeroSequencia));
    }

    @Override
    public void validarEscrituracaoDeNota(final String nota, final String serie, final Date dataNota, final Long idProdutor, final Long idSafra, final Long idMaterial, final Long idItem) throws Exception {

        if (StringUtil.isNotNullEmpty(nota) && StringUtil.isNotNullEmpty(serie) && ObjetoUtil.isNotNull(dataNota) && ObjetoUtil.isNotNull(idProdutor) && ObjetoUtil.isNotNull(idSafra) && ObjetoUtil.isNotNull(idMaterial)) {

            if (itemNFPedidoService.existeNotaSerieDataInicioDataFimProdutorSafraMaterialStatusSapStatusRomaneioIdItem(nota, serie, DateUtil.obterDataHoraZerada(dataNota), DateUtil.converterDateFimDia(dataNota), idProdutor, idSafra,
                    idMaterial, StatusIntegracaoSAPEnum.ESTORNADO, StatusRomaneioEnum.ESTORNADO, idItem)) {

                throw new Exception(MessageFormat.format(MessageSupport.getMessage(NFE_JA_FOI_ESCRITURADA), nota));
            }
        }
    }

    @Override
    public void estornarRomaneioSemIntegracaoComAutomacao(final Romaneio entidade) {

        estornar(entidade, true);
    }

    @Override
    public void estornar(final Romaneio entidade, boolean estornoEReferenciamento) {

        final Romaneio romaneio = ObjetoUtil.isNotNull(entidade.getId()) ? this.get(entidade.getId()) : this.get(((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(Romaneio_.numeroRomaneio),
                entidade.getNumeroRomaneio())));

        if (romaneio.getOperacao().isGestaoPatio()) {

            romaneio.getItens().stream().filter(i -> StringUtil.isNotNullEmpty(i.getNumeroRemessa())).forEach(i -> i.setStatusRemessa(StatusRemessaEnum.ESTORNADA));
        }

        this.sapService.estornarRemessa(romaneio, false);

        romaneio.setStatusRomaneio(StatusRomaneioEnum.ESTORNADO);

        romaneio.setMotivoEstorno(entidade.getMotivoEstorno());

        if (ObjetoUtil.isNotNull(romaneio.getSenha()) && !romaneio.getSenha().isNew()) {

            romaneio.setSenha(senhaService.get(romaneio.getSenha().getId()));

            if (!estornoEReferenciamento) {

                FluxoEtapa fluxoEtapa = fluxoEtapaService.obterFluxoEtapaPorMaterialEEtapa(romaneio.getSenha().getTipoFluxo(), romaneio.getSenha().getMaterial(), EtapaEnum.DISTRIBUICAO_SENHA);

                this.senhaService.adicionarEtapa(romaneio.getSenha(), fluxoEtapa);

                this.senhaService.estornarRomaneioNaAutomacao(romaneio);
            }

            romaneio.setSenhaEstornada(new Senha());

            romaneio.getSenhaEstornada().setId(romaneio.getSenha().getId());

            romaneio.setSenha(null);
        }

        inicializarLacreStatusLacre(romaneio);

        atualizarSaldoReservadoPedido(romaneio);

        this.getDAO().save(romaneio);

        romaneio.setUsuarioLogado(entidade.getUsuarioLogado());

        registrarLogAtividade(romaneio, LogAtividadeAcao.ESTORNAR);
    }

    private void alterarStatusDosLacresNoEstornoDoRomaneio(RomaneioEstornadoResponse romaneio, StatusLacreRomaneioEnum statusLacreRomaneio, StatusLacreEnum statusLacre) {

        if (ObjetoUtil.isNotNull(romaneio.getLacragem()) && ColecaoUtil.isNotEmpty(romaneio.getLacragem().getLacres())) {

            romaneio.getLacragem().getLacres().forEach(lacreRomaneio -> {

                lacreRomaneio.setStatusLacreRomaneio(statusLacreRomaneio);

                if (ObjetoUtil.isNotNull(lacreRomaneio.getLacre()) && !StatusLacreEnum.EXTRAVIADO.equals(lacreRomaneio.getLacre().getStatusLacre()) && !StatusLacreEnum.INATIVO.equals(lacreRomaneio.getLacre().getStatusLacre())) {

                    lacreRomaneio.getLacre().setStatusLacre(statusLacre);

                    lacreRomaneio.getLacre().setDataUtilizacao(DateUtil.hoje());
                }
            });
        }
    }

    @Override
    public void tornarPendente(Romaneio entidade) {

        if (ColecaoUtil.isNotEmpty(entidade.getMotivos())) {

            final List<Motivo> motivos = entidade.getMotivos();

            Romaneio romaneioConsultado = this.get(entidade.getId());

            romaneioConsultado.setStatusRomaneio(StatusRomaneioEnum.PENDENTE);

            romaneioConsultado.setMotivos(motivos);

            atualizarSaldoReservadoPedido(romaneioConsultado);

            this.getDAO().save(romaneioConsultado);

            this.registrarLogAtividade(entidade, LogAtividadeAcao.TORNAR_PENDENTE);
        }
    }

    @Override
    public void tornarEmProcessamento(final Romaneio entidade) {

        this.getDAO().updateStatusRomaneio(entidade.getId(), StatusRomaneioEnum.EM_PROCESSAMENTO, null);

        new Thread(() -> {
            Romaneio romaneio = obterPorNumeroRomaneio(entidade.getNumeroRomaneio());
            atualizarSaldoReservadoPedido(romaneio);
        }).start();

        this.registrarLogAtividade(entidade, LogAtividadeAcao.LIBERAR_ORDEM);
    }

    @Override
    public List<Romaneio> listarRomaneiosComErroDeIntegracaoPorData(final Date data) {

        return ObjetoUtil.isNotNull(data) ? this.getDAO().findByDataCadastroGreaterThanEqualAndStatusIntegracaoSAP(DateUtil.obterDataHoraZerada(data), StatusIntegracaoSAPEnum.ERRO_INTEGRACAO) :
                this.getDAO().findByStatusIntegracaoSAP(StatusIntegracaoSAPEnum.ERRO_INTEGRACAO);
    }

    @Override
    public List<RelatorioLaudoSintetico> obterRelatorioLaudoSintetico(RelatorioLaudoFiltroDTO relatorioLaudoFiltro) {

        List<RelatorioLaudoSintetico> relatorioLaudoSinteticos = romaneioCustomRepository.obterRelatorioLaudoSintetico(relatorioLaudoFiltro);

        List<Long> idLaudos = relatorioLaudoSinteticos.stream().map(RelatorioLaudoSintetico::getIdLaudo).filter(Objects::nonNull).collect(Collectors.toList());

        if (ColecaoUtil.isNotEmpty(idLaudos)) {

            List<Laudo> laudos = laudoDAO.findLaudoNumeroCertificadoById(idLaudos);

            laudos.stream().filter(laudo -> StringUtil.isNotNullEmpty(laudo.getNumeroCertificado())).forEach(laudo -> relatorioLaudoSinteticos.stream().filter(relatorioLaudoSintetico -> ObjetoUtil.isNotNull(relatorioLaudoSintetico.getIdLaudo()) && relatorioLaudoSintetico.getIdLaudo().equals(laudo.getId())).forEach(relatorioLaudoSintetico -> relatorioLaudoSintetico.setCertificado(laudo.getNumeroCertificado())));
        }

        if (ColecaoUtil.isNotEmpty(relatorioLaudoSinteticos)) {

            relatorioLaudoSinteticos.stream().filter(relatorioLaudoSintetico -> StringUtil.isEmpty(relatorioLaudoSintetico.getCertificado())).forEach(relatorioLaudoSintetico -> relatorioLaudoSintetico.setCertificado(ObjetoUtil.isNotNull(relatorioLaudoSintetico.getIdAnalise()) ? relatorioLaudoSintetico.getIdAnalise().toString() : StringUtil.empty()));
        }

        return relatorioLaudoSinteticos;
    }

    @Override
    public List<RelatorioLaudoAnalitico> obterRelatorioLaudoAnalitico(RelatorioLaudoFiltroDTO relatorioLaudoFiltro) {

        List<RelatorioLaudoAnalitico> relatorioLaudoAnaliticos = romaneioCustomRepository.obterRelatorioLaudoAnalitico(relatorioLaudoFiltro);

        List<Long> idLaudos = relatorioLaudoAnaliticos.stream().map(relatorioLaudoAnalitico -> relatorioLaudoAnalitico.getAnaliseQualidade().getIdLaudo()).filter(Objects::nonNull).collect(Collectors.toList());

        if (ColecaoUtil.isNotEmpty(idLaudos)) {

            List<Laudo> laudos = laudoDAO.findLaudoNumeroCertificadoById(idLaudos);

            laudos.stream().filter(laudo -> StringUtil.isNotNullEmpty(laudo.getNumeroCertificado())).forEach(laudo -> relatorioLaudoAnaliticos.stream().filter(relatorioLaudoAnalitico -> ObjetoUtil.isNotNull(relatorioLaudoAnalitico.getAnaliseQualidade()) && ObjetoUtil.isNotNull(relatorioLaudoAnalitico.getAnaliseQualidade().getIdLaudo()) && relatorioLaudoAnalitico.getAnaliseQualidade().getIdLaudo().equals(laudo.getId())).forEach(relatorioLaudoAnalitico -> relatorioLaudoAnalitico.getAnaliseQualidade().setCertificado(laudo.getNumeroCertificado())));
        }

        if (ColecaoUtil.isNotEmpty(relatorioLaudoAnaliticos)) {

            relatorioLaudoAnaliticos.stream().filter(relatorioLaudoAnalitico -> ObjetoUtil.isNotNull(relatorioLaudoAnalitico.getAnaliseQualidade()) && StringUtil.isEmpty(relatorioLaudoAnalitico.getAnaliseQualidade().getCertificado())).forEach(relatorioLaudoAnalitico -> relatorioLaudoAnalitico.getAnaliseQualidade().setCertificado(relatorioLaudoAnalitico.getAnaliseQualidade().getId().toString()));
        }

        return relatorioLaudoAnaliticos;
    }

    private void popularDadosRomaneioNfe(List<RomaneioGestaoItssAgroDTO> romaneios) {

        List<String> numRomaneios = romaneios.stream().map(RomaneioGestaoItssAgroDTO::getNumRomaneio).collect(Collectors.toList());

        if (ColecaoUtil.isNotEmpty(numRomaneios)) {

            List<RetornoNfeDTO> retornoNfeDTOs = retornoNFeService.obterRetornoNFePorRomaneios(numRomaneios);

            romaneios.stream().filter(romaneio -> ObjetoUtil.isNotNull(romaneio.getRequestKey())).forEach(romaneio -> {

                Optional<RetornoNfeDTO> retornoNfeDTO = retornoNfeDTOs.stream().filter(nfeDTO -> {
                    return (nfeDTO.getRequestKey() != null && nfeDTO.getRequestKey().equalsIgnoreCase(romaneio.getRequestKey()));
                }).findFirst();


                if (retornoNfeDTO.isPresent()) {

                    romaneio.setDataCriacaoNotaFiscal(retornoNfeDTO.get().getDataCriacaoNotaMastersaf());

                }
            });
        }
    }

    @Override
    public List<RomaneioGestaoItssAgroDTO> listarRomaneioGestaoItssAgroPorData(final Date dataInicial, Date dataFinal) {

        List<RomaneioGestaoItssAgroDTO> romaneios = romaneioCustomRepository.listarRomaneioGestaoItssAgroPorData(dataInicial, ObjetoUtil.isNull(dataFinal) ? new Date(System.currentTimeMillis()) : dataFinal);

        if (ColecaoUtil.isNotEmpty(romaneios)) {

            List<Long> idPesagens = romaneios.stream().map(RomaneioGestaoItssAgroDTO::getIdPesagem).collect(Collectors.toList());

            if (ColecaoUtil.isNotEmpty(idPesagens)) {

                List<PesagemHistoricoDTO> historicosPrimeiraPesagem = pesagemService.obterPrimeiraPesagemDeRomaneios(idPesagens);

                List<PesagemHistoricoDTO> historicosSegundaPesagem = pesagemService.obterSegundaPesagemDeRomaneios(idPesagens);

                romaneios.stream().filter(romaneio -> ObjetoUtil.isNotNull(romaneio.getIdPesagem())).forEach(romaneio -> {

                    Map<String, Date> mapDatasPesagens = pesagemService.getDatasDePesagem(historicosPrimeiraPesagem, historicosSegundaPesagem, romaneio.getIdPesagem());

                    if (!mapDatasPesagens.isEmpty()) {

                        romaneio.setDataPrimeiraPesagem(mapDatasPesagens.get(PesagemServiceImpl.DATA_PRIMEIRA_PESAGEM));

                        romaneio.setDataSegundaPesagem(mapDatasPesagens.get(PesagemServiceImpl.DATA_SEGUNDA_PESAGEM));
                    }
                });
            }

            this.popularDadosRomaneioNfe(romaneios);


        }

        return romaneios;
    }

    @Override
    public void atualizarStatusPedido(final String romaneio, final String pedido, final StatusPedidoEnum statusPedido) {

        this.getDAO().updateStatusPedido(romaneio, pedido, statusPedido);
    }

    @Override
    @Transactional
    public Romaneio obterDadosRomaneioEstornado(DadosSincronizacaoEnum operacaoEntidade, String numero) throws ServicoException {

        Romaneio romaneio = getDAO().findByOperacaoEntidadeAndNumeroRomaneioAndStatusIntegracaoSAP(operacaoEntidade, numero, StatusIntegracaoSAPEnum.ESTORNADO);

        if (ObjetoUtil.isNull(romaneio)) {

            throw new ServicoException(MessageFormat.format("Romaneio {0} não encontrado.", numero));
        }

        return new Romaneio(romaneio.getProdutor(), romaneio.getArmazemTerceiros(), romaneio.getSafra(), romaneio.getSafraPlantil(), romaneio.getMaterial(), romaneio.getDeposito(), romaneio.getMotorista(), romaneio.getPlacaCavalo(),
                romaneio.getPlacaCavaloUm(), romaneio.getPlacaCavaloDois(), romaneio.getTransportadora(), romaneio.getTransportadoraSubcontratada(), romaneio.getTipoVeiculo(), romaneio.getPesagem(), romaneio.getClassificacao(),
                romaneio.getCentro(), romaneio.getCentroDestino(), romaneio.getTipoContrato(), romaneio.getCondicaoPag(), romaneio.getOrgCompra(), romaneio.getGpCompradores(), romaneio.getUsuarioAprovador(), romaneio.getIva(),
                romaneio.getRomaneioArmazemTerceiros(), romaneio.getLocalPesagem(), romaneio.getTipoFrete(), romaneio.getIncotermsUm(), romaneio.getDirecaoSimplesPesagem(), romaneio.getStatusPedido(), romaneio.getCessaoCredito(),
                romaneio.getNumeroRomaneio(), romaneio.getNumeroRomaneioOrigem(), romaneio.getObservacao(), romaneio.getObservacaoNota(), romaneio.getJsonRequest(), romaneio.getInscricaoEstadual(), romaneio.getFazenda(),
                romaneio.getIncotermsDois(), romaneio.getNomeSimplesPesagem(), romaneio.getMaterialSimplesPesagem(), romaneio.getMotoristaSimplesPesagem(), romaneio.getPlacaCavaloSimplesPesagem(), romaneio.getMotivoEstorno(),
                romaneio.getNumeroPedido(), romaneio.getItemPedido(), romaneio.getNumeroContrato(), romaneio.getNumeroNfe(), romaneio.getSerieNfe(), romaneio.getSimplesEntradaReferencia(), romaneio.getDevolucaoReferencia(),
                romaneio.getRecebedor(), romaneio.getQuantidadePesoOrigem(), romaneio.getQuantidadeMaterial(), romaneio.getUnidadePrecoMaterial(), romaneio.getValorMaterial(), romaneio.getPesoBrutoOrigem(), romaneio.getPesoTaraOrigem(),
                romaneio.getDescontoOrigem(), romaneio.getPesoLiquidoUmidoOrigem(), romaneio.getPesoLiquidoSecoOrigem(), romaneio.getPrecoFrete(), romaneio.getToleranciaKg(), romaneio.getSaldoPedido(), romaneio.isVeiculoLongo(),
                romaneio.isPermitirExecutarFluxo(), romaneio.isPossuiDadosOrigem(), romaneio.getDataEnvio(), romaneio.getDtPagamento(), romaneio.getDtInicioEntrega(), romaneio.getDtFinalEntrega(),
                romaneio.getDtSimplesEntradaReferencia(), romaneio.getItensClassificacaoOrigem(), romaneio.getItensRestritosPorPerfil(), romaneio.getItens(), romaneio.getToleranciaPedido(), romaneio.getValorTotalCte(), romaneio.getUfEmissoraCte(),
                romaneio.getUfTomadoraCte(), romaneio.getPedidoCte(), romaneio.getItemPedidoCte(), romaneio.getIvaCte(), romaneio.getParceiroCte(), romaneio.getChaveAcessoCte(),
                romaneio.getNumeroCte(), romaneio.getSerieCte(), romaneio.getLogCte(), romaneio.getNumeroAleatorioCte(), romaneio.getDigitoVerificadorCte(), romaneio.getDataCte());
    }

    @Override
    public ListaRomaneiosPendenteAprovacaoIndicesResponse listarRomaneiosPendenteAprovacaoIndices() {
        Collection<RomaneioAprovacaoDocumento> romaneios = listarRomaneiosPendentesAprovacaoClassificacao();
        Collection<UsuarioAprovacaoDocumento> usuarios = this.usuarioService.listarUsuariosComPermissaoParaAprovarIndices();

        return ListaRomaneiosPendenteAprovacaoIndicesResponse.builder().romaneios(romaneios).usuarios(usuarios).build();
    }

    @Override
    public byte[] imprimirPdfClassificacao(Long id) throws Exception {
        byte[] pdfBytes = this.classificacaoService.imprimirPdf(null, id);

        if (ObjetoUtil.isNull(pdfBytes)) {

            throw new Exception(NAO_FOI_POSSIVEL_REALIZAR_A_IMPRESSAO_DO_PDF);
        }

        return pdfBytes;
    }

    @Override
    public Arquivo imprimirPdfClassificacaoPorNumeroRomaneio(String numeroRomaneio) throws Exception {
        Long romaneioId = getValorAtributo(Romaneio_.ID, (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(Romaneio_.numeroRomaneio), numeroRomaneio));

        byte[] bufferPdf = imprimirPdfClassificacao(romaneioId);

        return Arquivo.builder().buffer(bufferPdf).fileName(numeroRomaneio + "_" + DateUtil.formatData_ddMMyyyyHHmmss(new Date()) + ".pdf").build();
    }

    @Override
    public Arquivo imprimirPdfPorNumeroRomaneio(String numeroRomaneio) throws Exception {
        Long romaneioId = getValorAtributo(Romaneio_.ID, (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(Romaneio_.numeroRomaneio), numeroRomaneio));

        byte[] bufferPdf = imprimirPdf(null, romaneioId);

        return Arquivo.builder().buffer(bufferPdf).fileName(numeroRomaneio + "_" + DateUtil.formatData_ddMMyyyyHHmmss(new Date()) + ".pdf").build();
    }

    private Collection<RomaneioAprovacaoDocumento> listarRomaneiosPendentesAprovacaoClassificacao() {

        Paginacao paginacao = new Paginacao();

        paginacao.setListarTodos(true);

        paginacao.setSpecification(((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(Romaneio_.statusRomaneio), StatusRomaneioEnum.PENDENTE_INDICES_CLASSIFICACAO)));

        paginacao.setOrdenacao(Ordenacao.ordenacaoBuilder().ordenacao(Sort.Direction.DESC).campo(Romaneio_.ID).build());

        Collection<RomaneioAprovacaoDocumento> romaneios = dtoListar(paginacao, RomaneioAprovacaoDocumento.class).getContent();

        if (ColecaoUtil.isNotEmpty(romaneios)) {

            List<TabelaClassificacao> limitesIndices = tabelaClassificacaoService.getLimiteTodosIndices();

            List<Classificacao> classificacoes = classificacaoService.listar(((root, criteriaQuery, criteriaBuilder) -> root.get(Classificacao_.id).in(romaneios.stream().map(r -> r.getClassificacao().getId()).collect(Collectors.toList()))));

            List<ItemNFPedidoAprovacaoDocumento> itens = new ArrayList<>(itemNFPedidoService.dtoListar(((root, criteriaQuery, criteriaBuilder) ->
                    root.join(ItemNFPedido_.romaneio).get(Romaneio_.id).in(romaneios.stream().map(RomaneioAprovacaoDocumento::getId).collect(Collectors.toList()))), ItemNFPedidoAprovacaoDocumento.class));

            romaneios.parallelStream().forEach(romaneioAprovacaoDocumento -> {

                if (ColecaoUtil.isNotEmpty(itens)) {

                    romaneioAprovacaoDocumento.setItemNFPedidoAprovacaoDocumentos(itens.stream().filter(i -> i.getRomaneio().getId().equals(romaneioAprovacaoDocumento.getId())).collect(Collectors.toList()));
                }

                classificacoes.stream().filter(_classificacao -> _classificacao.getId().equals(romaneioAprovacaoDocumento.getClassificacao().getId())).findFirst().ifPresent(classificacao -> {

                    if (ColecaoUtil.isNotEmpty(classificacao.getItens())) {

                        classificacao.getItens().forEach(item -> {

                            TabelaClassificacao limiteClassificacao = limitesIndices.stream().filter(limiteTemp -> limiteTemp.getDescricaoItemClassificacao().equalsIgnoreCase(item.getDescricaoItem())).findFirst().orElse(null);

                            if (!Objects.isNull(limiteClassificacao)) {

                                if (item.getIndice().doubleValue() >= limiteClassificacao.getIndiceNumerico()) {

                                    item.setLimite(limiteClassificacao.getIndiceNumerico());

                                    romaneioAprovacaoDocumento.getLimitesIndices().add(TabelaClassificacaoAprovacaoDocumento.builder().item(item).build());
                                }
                            }
                        });
                    }
                });
            });
        }

        return romaneios;
    }

    @Override
    public void enviarEmailRomaneioBloqueadoPorIndiceClassificacao(Romaneio romaneio) {

        List<TabelaClassificacao> limitesIndices = tabelaClassificacaoService.getLimiteTodosIndices();

        List<ItensClassificacao> indicesBloqueados = new ArrayList<>();

        romaneio.getClassificacao().getItens().forEach(item -> {

            TabelaClassificacao limiteClassificacao = limitesIndices.stream().filter(limiteTemp -> limiteTemp.getDescricaoItemClassificacao().equalsIgnoreCase(item.getDescricaoItem())).findFirst().orElse(null);

            if (!Objects.isNull(limiteClassificacao)) {

                if (item.getIndice().doubleValue() >= limiteClassificacao.getIndiceNumerico()) {

                    item.setLimite(limiteClassificacao.getIndiceNumerico());

                    indicesBloqueados.add(item);
                }
            }
        });

        if (!indicesBloqueados.isEmpty()) {

            romaneio.setIndicesBloqueados(indicesBloqueados);

            romaneio.setEmails(configuracaoEmailService.get().getEmailsClassificacao());

            emailSenderService.sendEmail(new EmailRomaneioBloqueadoPorIndiceClassificacao(romaneio));
        }
    }

    @Override
    public Pagina<ItemNFPedidoDTO> listarPaginadoItemNFPedido(Paginacao<ItemNFPedido> paginacao) {

        final Specification<ItemNFPedido> specification = this.obterSpecificationItem(paginacao);

        return this.convertToPageItem(itemNFPedidoService.getDAO().findAll(specification, paginacao));
    }

    @Override
    public void validacoesRomaneio(Romaneio entidade) throws Exception {

        if (ObjetoUtil.isNull(entidade.getId()) && ObjetoUtil.isNotNull(entidade.getNumeroCartaoAcesso())) {

            String nrRomaneio = romaneioCustomRepository.obterRomaneioQueEstaUtilizandoOCartaoDeAcesso(entidade.getNumeroCartaoAcesso());

            if (StringUtil.isNotNullEmpty(nrRomaneio)) {

                throw new ServicoException("O Romaneio " + nrRomaneio + " já está utilizando o cartão de acesso informado.");
            }
        }

        if (entidade.getOperacao().isGestaoPatio()) {

            if (ObjetoUtil.isNotNull(entidade.getSenha())) {

                if (romaneioCustomRepository.existeRomaneioVinculadoAsenha(entidade.getId(), entidade.getSenha().getId())) {

                    throw new ServicoException(MessageSupport.getMessage(EXISTE_ROMANEIO_VINCULADO_A_ESSA_SENHA));
                }
            }

            validarSePlacaEstaSendoUtilizadaNonTransactional(entidade.getItens().stream().map(ItemNFPedido::getPlacaCavalo).filter(Objects::nonNull).findFirst().orElse(null), entidade.getId(), null);

            final Motorista motorista = entidade.getItens().stream().map(ItemNFPedido::getMotorista).filter(Objects::nonNull).findFirst().orElse(null);

            if (ObjetoUtil.isNotNull(motorista)) {

                motorista.verificarSeMotoristaEstaBloqueado();
            }

            this.sapService.validarStatusOrdemDeVenda(entidade);

            validarSaldoProdutoAcabado(entidade);

            validarConferenciaDePeso(entidade);
        }
    }

    private void validarConferenciaDePeso(Romaneio entidade) {

        if (ColecaoUtil.isNotEmpty(entidade.getRestricoes())) {

            entidade.getRestricoes().removeIf(restricaoOrdem -> TipoRestricaoEnum.CONFERENCIA_PESO.equals(restricaoOrdem.getTipoRestricao()));
        }

        if (ObjetoUtil.isNull(entidade.getRestricoes())) {

            entidade.setRestricoes(new ArrayList<>());
        }

        boolean validarConferenciaDePeso = ObjetoUtil.isNotNull(entidade.getMotivo()) && entidade.getMotivo().getConferenciaPeso();

        Material material = entidade.getItens().stream().map(ItemNFPedido::getMaterial).findFirst().get();

        if (validarConferenciaDePeso && ObjetoUtil.isNotNull(material.getToleranciaEmKg()) && StringUtil.isNotNullEmpty(entidade.getRomaneioVinculadoPorMotivo()) && !entidade.isValidacaoConferenciaPesoRealizada()) {

            BigDecimal pesoLiquido = BigDecimal.ZERO;

            if (ObjetoUtil.isNotNull(entidade.getPesagem()) && BigDecimalUtil.isMaiorZero(entidade.getPesagem().getPesoInicial()) && BigDecimalUtil.isMaiorZero(entidade.getPesagem().getPesoFinal())) {

                RomaneioConferenciaPeso romaneioConferenciaPeso = romaneioCustomRepository.getRomaneioConferenciaPeso(entidade.getRomaneioVinculadoPorMotivo());

                if (ObjetoUtil.isNotNull(romaneioConferenciaPeso) && BigDecimalUtil.isMaiorZero(romaneioConferenciaPeso.getPesagem().getPesoInicial()) && BigDecimalUtil.isMaiorZero(romaneioConferenciaPeso.getPesagem().getPesoFinal())) {

                    pesoLiquido = entidade.getPesagem().getPesoInicial().subtract(entidade.getPesagem().getPesoFinal());

                    BigDecimal pesoLiquidoRomaneioReferenciado = romaneioConferenciaPeso.getPesagem().getPesoInicial().subtract(romaneioConferenciaPeso.getPesagem().getPesoFinal());

                    double diferenca = pesoLiquido.doubleValue() - pesoLiquidoRomaneioReferenciado.doubleValue();

                    if (diferenca < 0) {

                        diferenca = Math.abs(diferenca);
                    }

                    if (diferenca > material.getToleranciaEmKg().doubleValue()) {

                        entidade.setValidacaoConferenciaPesoRealizada(true);

                        entidade.setStatusRomaneio(StatusRomaneioEnum.PENDENTE);

                        entidade.getRestricoes().add(new RestricaoOrdem(MessageSupport.getMessage("MSGE037", entidade.getMotivo().getDescricao()), StringUtil.empty(), TipoRestricaoEnum.CONFERENCIA_PESO, entidade));
                    }
                }
            }
        }
    }

    @Override
    public Romaneio obterPorSenha(Senha senha) {

        return getDAO().findFirstBySenhaId(senha.getId());
    }

    @Override
    public RomaneioDTO obterPorSenha(Long senhaId) {

        return this.getDAO().getBySenhaId(senhaId);
    }

    @Override
    public void setarConversaoLitragem(final Long idConversao, final String numeroRomaneio) {

        dao.updateConversaoLitragem(idConversao, numeroRomaneio);
    }

    @Override
    public boolean existeItemNFPedidoPorTipoVeiculoSeta(Long tipoVeiculoSetaId) {

        return getDAO().existsItemNFPedidoByTipoVeiculoSeta(tipoVeiculoSetaId);
    }

    @Override
    @Transactional
    public String obterMotivoEstorno(Long id) {

        return getDAO().findMotivoEstornoById(id);
    }

    @Override
    public boolean existeNumeroRomaneio(final String numeroRomaneio) {

        return romaneioCustomRepository.existeNumeroRomaneio(numeroRomaneio);
    }

    @Override
    public BigDecimal obterSaldoReservado(String pedido, String item) {
        return getDAO().obterSaldoReservado(pedido, item);
    }

    @Override
    public BigDecimal obterSaldoOrdemVendaBloqueado(String ordemVenda, String romaneio){
        return getDAO().obterSaldoOrdemVendaBloqueado(ordemVenda, romaneio);
    }

    private <T> Pagina<ItemNFPedidoDTO> convertToPageItem(final Page<ItemNFPedido> page) {

        try {

            final Pagina<ItemNFPedidoDTO> pagina = new Pagina<>();

            pagina.setContent(this.listToDTOItem(page.getContent()));

            pagina.setTotalElements(page.getTotalElements());

            return pagina;

        } catch (final Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    private <T> Collection<ItemNFPedidoDTO> listToDTOItem(final Collection<ItemNFPedido> lista) {

        try {

            return lista.stream().map(l -> {

                try {

                    return new ItemNFPedidoDTO(l);

                } catch (final Exception e) {

                    e.printStackTrace();

                    return null;
                }
            }).collect(Collectors.toList());

        } catch (final Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    private Specification<ItemNFPedido> obterSpecificationItem(final Paginacao<ItemNFPedido> paginacao) {

        final ItemNFPedido entidade = paginacao.getEntidade();

        final Specification<ItemNFPedido> specification = new Specification<ItemNFPedido>() {

            @Override
            public Predicate toPredicate(final Root<ItemNFPedido> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                if (ObjetoUtil.isNotNull(entidade.getSafra())) {

                    predicates.add(builder.equal(root.get("safra"), entidade.getSafra()));
                }

                if (ObjetoUtil.isNotNull(entidade.getMaterial())) {

                    predicates.add(builder.equal(root.get("material"), entidade.getMaterial()));
                }

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Override
    public List<Cliente> listarClientesPorRomaneio(final Long idRomaneio) {

        return romaneioCustomRepository.obterClientesPorRomaneio(idRomaneio);
    }

    @Override
    @Transactional
    public List<RetornoNFe> listarNfePorRomaneio(final Long idRomaneio) {

        return retornoNFeService.listarNfePorRomaneio(idRomaneio);
    }

    private void validarSePlacaEstaSendoUtilizadaNonTransactional(final Veiculo veiculo, final Long idRomaneio, final Long idSenha) throws ServicoException {
        if (ObjetoUtil.isNotNull(veiculo)) {

            boolean estaSendoUtilizadoEmSenha = false;

            boolean estaSendoUtilizadaEmRomaneio = false;

            if (ObjetoUtil.isNotNull(veiculo.getId())) {

                if (ObjetoUtil.isNotNull(idSenha)) {

                    estaSendoUtilizadoEmSenha = this.senhaService.existsByVeiculoIdAndStatusSenhaNotInAndIdNot(veiculo.getId(), Arrays.asList(StatusSenhaEnum.COMPLETA, StatusSenhaEnum.PENDENTE, StatusSenhaEnum.CANCELADA), idSenha);

                } else {

                    estaSendoUtilizadoEmSenha = this.senhaService.existsByVeiculoIdAndStatusSenhaNotIn(veiculo.getId(), Arrays.asList(StatusSenhaEnum.COMPLETA, StatusSenhaEnum.PENDENTE, StatusSenhaEnum.CANCELADA));
                }

                if (ObjetoUtil.isNotNull(idRomaneio)) {

                    estaSendoUtilizadaEmRomaneio = itemNFPedidoService.existePlacaCavaloStatusRomaneioRomaneioDiferenteDe(veiculo.getId(), StatusRomaneioEnum.EM_PROCESSAMENTO, idRomaneio);

                } else {

                    estaSendoUtilizadaEmRomaneio = itemNFPedidoService.existePlacaCavaloStatusRomaneio(veiculo.getId(), StatusRomaneioEnum.EM_PROCESSAMENTO);
                }
            }

            if (estaSendoUtilizadoEmSenha || estaSendoUtilizadaEmRomaneio) {

                throw new ServicoException(MessageSupport.getMessage("MSGE025", StringUtil.toPlaca(veiculo.getPlaca1())));
            }
        }
    }

    @Transactional
    @Override
    public void validarSePlacaEstaSendoUtilizada(final Veiculo veiculo, final Long idRomaneio, final Long idSenha) throws ServicoException {
        validarSePlacaEstaSendoUtilizadaNonTransactional(veiculo, idRomaneio, idSenha);
    }

    @Override
    public void aprovarRomaneioComItensRestritos(RomaneioAprovacaoDocumento romaneioAprovacaoDocumento) {

        Usuario usuarioAprovador = romaneioAprovacaoDocumento.getUsuarioAprovador();

        Romaneio romaneio = get(romaneioAprovacaoDocumento.getId());

        if (DadosSincronizacaoEnum.SIMPLES_ENTRADA.equals(romaneio.getOperacao().getEntidade())) {

            romaneio.setStatusRomaneio(StatusRomaneioEnum.CONCLUIDO);

        } else {

            romaneio.setStatusRomaneio(StatusRomaneioEnum.EM_PROCESSAMENTO);
        }

        if (ColecaoUtil.isEmpty(romaneio.getRomaneioHistoricoAprovacaoIndices())) {

            romaneio.setRomaneioHistoricoAprovacaoIndices(new ArrayList<>());
        }

        romaneio.getRomaneioHistoricoAprovacaoIndices().add(new RomaneioHistoricoAprovacaoIndice(usuarioAprovador, romaneio));

        super.salvar(romaneio);
    }

    @Override
    public void aprovarRomaneioComItensRestritos(Romaneio romaneio) {

        Usuario usuarioAprovador = romaneio.getUsuarioAprovador();

        romaneio = get(romaneio.getId());

        if (DadosSincronizacaoEnum.SIMPLES_ENTRADA.equals(romaneio.getOperacao().getEntidade())) {

            romaneio.setStatusRomaneio(StatusRomaneioEnum.CONCLUIDO);

        } else {

            romaneio.setStatusRomaneio(StatusRomaneioEnum.EM_PROCESSAMENTO);
        }

        if (ColecaoUtil.isEmpty(romaneio.getRomaneioHistoricoAprovacaoIndices())) {

            romaneio.setRomaneioHistoricoAprovacaoIndices(new ArrayList<>());
        }

        romaneio.getRomaneioHistoricoAprovacaoIndices().add(new RomaneioHistoricoAprovacaoIndice(usuarioAprovador, romaneio));

        super.salvar(romaneio);
    }

    @Override
    @Transactional
    public List<Motivo> buscarMotivosPorId(Long id) {

        return dao.findMotivosById(id);
    }

    @Override
    public RomaneioDAO getDAO() {

        return this.dao;
    }

    @Override
    public Class<RomaneioListagemDTO> getDtoListagem() {

        return RomaneioListagemDTO.class;
    }

    @Override
    public Boolean getRegistrarLogAtividade() {

        return Boolean.TRUE;
    }

    @Override
    public Pagina<RomaneioListagemDTO> dtoListar(Paginacao paginacao) {

        paginacao.setOrdenacao(Ordenacao.ordenacaoBuilder().campo(Romaneio_.ID).ordenacao(Sort.Direction.DESC).build());

        Pagina<RomaneioListagemDTO> romaneioListagemDTOPagina = super.dtoListar(paginacao, RomaneioListagemDTO.class);

        if (romaneioListagemDTOPagina.getTotalElements() > 0l) {

            List<Long> romaneiosId = romaneioListagemDTOPagina.getContent().stream().map(RomaneioListagemDTO::getId).collect(Collectors.toList());

            Paginacao paginacaoItensRomaneio = new Paginacao();

            paginacaoItensRomaneio.setListarTodos(true);

            paginacaoItensRomaneio.setSpecification(((root, criteriaQuery, criteriaBuilder) -> root.get(ItemNFPedido_.romaneio).get(Romaneio_.id).in(romaneiosId)));

            Paginacao paginacaoNotasRomaneio = new Paginacao();

            paginacaoNotasRomaneio.setListarTodos(true);

            paginacaoNotasRomaneio.setSpecification(((root, criteriaQuery, criteriaBuilder) -> root.get(RetornoNFe_.romaneio).get(Romaneio_.id).in(romaneiosId)));

            Collection<ItemNFPedidoListagemDTO> itensRomaneios = new ArrayList(itemNFPedidoService.dtoListar(paginacaoItensRomaneio).getContent());

            Collection<RetornoNFEListagemDTO> notasRomaneios = new ArrayList(retornoNFeService.dtoListar(paginacaoNotasRomaneio).getContent());

            if (ColecaoUtil.isNotEmpty(itensRomaneios)) {

                romaneioListagemDTOPagina.getContent().stream().forEach(content -> {

                    content.setRetornoNfes(notasRomaneios.stream().filter(item -> item.getRomaneio().getId().equals(content.getId())).collect(Collectors.toList()));

                    content.setItens(itensRomaneios.stream().filter(item -> item.getRomaneio().getId().equals(content.getId())).collect(Collectors.toList()));

                    if (content.getPossuiItens()) {

                        content.setPlacaCavalo(content.getItens().stream().map(ItemNFPedidoListagemDTO::getPlacaCavalo).filter(Objects::nonNull).findFirst().orElse(null));

                        content.setProdutor(content.getItens().stream().map(ItemNFPedidoListagemDTO::getProdutor).filter(Objects::nonNull).findFirst().orElse(null));

                        content.setMaterial(content.getItens().stream().map(ItemNFPedidoListagemDTO::getMaterial).filter(Objects::nonNull).findFirst().orElse(null));

                        content.setTransportadora(content.getItens().stream().map(ItemNFPedidoListagemDTO::getTransportadora).filter(Objects::nonNull).findFirst().orElse(null));

                        content.setMotorista(content.getItens().stream().map(ItemNFPedidoListagemDTO::getMotorista).filter(Objects::nonNull).findFirst().orElse(null));

                        content.setCliente(content.getItens().stream().map(ItemNFPedidoListagemDTO::getCliente).filter(Objects::nonNull).findFirst().orElse(null));
                    }
                });
            }
        }

        return romaneioListagemDTOPagina;
    }

    @Override
    public Pagina<RomaneioClassificacaoListagemDTO> listarClassificacaoDTO(Paginacao<Romaneio> paginacao) {

        paginacao.setOrdenacao(Ordenacao.ordenacaoBuilder().campo(Romaneio_.ID).ordenacao(Sort.Direction.DESC).build());

        Pagina<RomaneioClassificacaoListagemDTO> romaneioClassificacaoListagemDTOPagina = super.dtoListar(paginacao, RomaneioClassificacaoListagemDTO.class);

        if (romaneioClassificacaoListagemDTOPagina.getTotalElements() > 0l) {

            List<Long> romaneiosId = romaneioClassificacaoListagemDTOPagina.getContent().stream().map(RomaneioClassificacaoListagemDTO::getId).collect(Collectors.toList());

            Paginacao paginacaoItensRomaneio = new Paginacao();

            paginacaoItensRomaneio.setListarTodos(true);

            paginacaoItensRomaneio.setSpecification(((root, criteriaQuery, criteriaBuilder) -> root.get(ItemNFPedido_.romaneio).get(Romaneio_.id).in(romaneiosId)));

            Collection<ItemNFPedidoListagemDTO> itensRomaneios = new ArrayList(itemNFPedidoService.dtoListar(paginacaoItensRomaneio).getContent());

            if (ColecaoUtil.isNotEmpty(itensRomaneios)) {

                romaneioClassificacaoListagemDTOPagina.getContent().stream().forEach(content -> {

                    content.setItens(itensRomaneios.stream().filter(item -> item.getRomaneio().getId().equals(content.getId())).collect(Collectors.toList()));

                    if (content.getPossuiItens()) {

                        content.setPlacaCavalo(content.getItens().stream().map(ItemNFPedidoListagemDTO::getPlacaCavalo).filter(Objects::nonNull).findFirst().orElse(null));

                        content.setSafra(content.getItens().stream().map(ItemNFPedidoListagemDTO::getSafra).filter(Objects::nonNull).findFirst().orElse(null));
                    }
                });
            }
        }

        return romaneioClassificacaoListagemDTOPagina;
    }

    @Override
    public Pagina<RomaneioPesagemListagemDTO> listarPesagemDTO(Paginacao<Romaneio> paginacao) {

        paginacao.setOrdenacao(Ordenacao.ordenacaoBuilder().campo(Romaneio_.ID).ordenacao(Sort.Direction.DESC).build());

        Pagina<RomaneioPesagemListagemDTO> romaneioPesagemListagemDTOPagina = super.dtoListar(paginacao, RomaneioPesagemListagemDTO.class);

        if (romaneioPesagemListagemDTOPagina.getTotalElements() > 0l) {

            List<Long> romaneiosId = romaneioPesagemListagemDTOPagina.getContent().stream().map(RomaneioPesagemListagemDTO::getId).collect(Collectors.toList());

            Paginacao paginacaoItensRomaneio = new Paginacao();

            paginacaoItensRomaneio.setListarTodos(true);

            paginacaoItensRomaneio.setSpecification(((root, criteriaQuery, criteriaBuilder) -> root.get(ItemNFPedido_.romaneio).get(Romaneio_.id).in(romaneiosId)));

            Collection<ItemNFPedidoListagemDTO> itensRomaneios = new ArrayList(itemNFPedidoService.dtoListar(paginacaoItensRomaneio).getContent());

            if (ColecaoUtil.isNotEmpty(itensRomaneios)) {

                romaneioPesagemListagemDTOPagina.getContent().stream().forEach(content -> {

                    content.setItens(itensRomaneios.stream().filter(item -> item.getRomaneio().getId().equals(content.getId())).collect(Collectors.toList()));

                    if (content.getPossuiItens()) {

                        content.setPlacaCavalo(content.getItens().stream().map(ItemNFPedidoListagemDTO::getPlacaCavalo).filter(Objects::nonNull).findFirst().orElse(null));

                        content.setSafra(content.getItens().stream().map(ItemNFPedidoListagemDTO::getSafra).filter(Objects::nonNull).findFirst().orElse(null));
                    }
                });
            }
        }

        return romaneioPesagemListagemDTOPagina;
    }

    @Override
    public Pagina<RomaneioLacreListagemDTO> listarLacreDTO(Paginacao paginacao) {

        paginacao.setOrdenacao(Ordenacao.ordenacaoBuilder().campo(Romaneio_.ID).ordenacao(Sort.Direction.DESC).build());

        Pagina<RomaneioLacreListagemDTO> romaneioLacreListagemDTOPagina = super.dtoListar(paginacao, RomaneioLacreListagemDTO.class);

        if (romaneioLacreListagemDTOPagina.getTotalElements() > 0l) {

            List<Long> romaneiosId = romaneioLacreListagemDTOPagina.getContent().stream().map(RomaneioLacreListagemDTO::getId).collect(Collectors.toList());

            Paginacao paginacaoItensRomaneio = new Paginacao();

            paginacaoItensRomaneio.setListarTodos(true);

            paginacaoItensRomaneio.setSpecification(((root, criteriaQuery, criteriaBuilder) -> root.get(ItemNFPedido_.romaneio).get(Romaneio_.id).in(romaneiosId)));

            Collection<ItemNFPedidoListagemDTO> itensRomaneios = new ArrayList(itemNFPedidoService.dtoListar(paginacaoItensRomaneio).getContent());

            if (ColecaoUtil.isNotEmpty(itensRomaneios)) {

                romaneioLacreListagemDTOPagina.getContent().stream().forEach(content -> {

                    content.setItens(itensRomaneios.stream().filter(item -> item.getRomaneio().getId().equals(content.getId())).collect(Collectors.toList()));

                    if (content.getPossuiItens()) {

                        content.setPlacaCavalo(content.getItens().stream().map(ItemNFPedidoListagemDTO::getPlacaCavalo).filter(Objects::nonNull).findFirst().orElse(null));

                        content.setMaterial(content.getItens().stream().map(ItemNFPedidoListagemDTO::getMaterial).filter(Objects::nonNull).findFirst().orElse(null));
                    }
                });
            }
        }

        return romaneioLacreListagemDTOPagina;
    }

    private void atribuirUsuario(final Object entidade) {

        try {

            ((EntidadeGenerica) entidade).setUsuarioLogado(this.getUsuarioToken());

        } catch (final Exception ignored) {

            ignored.printStackTrace();
        }

    }

    private Usuario getUsuarioToken() {

        return (Usuario) this.request.getAttribute(TOKEN_USER);
    }
}
