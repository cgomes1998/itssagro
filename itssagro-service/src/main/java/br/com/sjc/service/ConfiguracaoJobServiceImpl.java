package br.com.sjc.service;

import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.ConfiguracaoJobService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.cfg.ConfiguracaoJob;
import br.com.sjc.modelo.dto.ConfiguracaoJobDTO;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.persistencia.dao.ConfiguracaoJobDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class ConfiguracaoJobServiceImpl extends ServicoGenerico<Long, ConfiguracaoJob> implements ConfiguracaoJobService {

    @Autowired
    private ConfiguracaoJobDAO dao;

    @Override
    public Specification<ConfiguracaoJob> obterEspecification(final Paginacao<ConfiguracaoJob> paginacao) {

        final Specification<ConfiguracaoJob> specification = new Specification<ConfiguracaoJob>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<ConfiguracaoJob> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Override
    public void alterarStatus(ConfiguracaoJob entidade) {

        entidade.setAtivo(entidade.isAtivo() ? false : true);

        this.getDAO().save(entidade);
    }

    @Override
    public ConfiguracaoJob obterPorNome(final String nome) {

        return this.dao.findByNome(nome);
    }

    @Override
    public ConfiguracaoJobDAO getDAO() {
        return this.dao;
    }

    @Override
    public Class<? extends DataDTO> getDtoListagem() {
        return ConfiguracaoJobDTO.class;
    }
}
