package br.com.sjc.service;

import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.ConfiguracaoEmailService;
import br.com.sjc.fachada.service.TabelaClassificacaoService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.dto.TabelaClassificacaoDTO;
import br.com.sjc.modelo.dto.TabelaClassificacaoItemDTO;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.sap.TabelaClassificacao;
import br.com.sjc.modelo.sap.email.EmailAlteracaoTabelaClassificacao;
import br.com.sjc.modelo.sap.request.EventOutRequest;
import br.com.sjc.modelo.sap.sync.SAPTabelaClassificacao;
import br.com.sjc.persistencia.dao.TabelaClassificacaoDAO;
import br.com.sjc.persistencia.dao.fachada.TabelaClassificacaoCustomRepository;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class TabelaClassificacaoServiceImpl extends ServicoGenerico<Long, TabelaClassificacao> implements TabelaClassificacaoService {

    private static final Logger LOGGER = Logger.getLogger(TabelaClassificacaoServiceImpl.class.getName());

    @Autowired
    private TabelaClassificacaoDAO dao;

    @Autowired
    private TabelaClassificacaoCustomRepository tabelaClassificacaoCustomRepository;

    @Autowired
    private ConfiguracaoEmailService configuracaoEmailService;

    @Autowired
    private EmailSenderServiceImpl emailSenderService;

    @Override
    public void adicionarItensParaCalculoDeIndice(final TabelaClassificacao entidade) {

        final TabelaClassificacao entidadeSaved = this.get(entidade.getId());

        entidadeSaved.setItensParaCalculoIndice(entidade.getItensParaCalculoIndice());

        this.getDAO().save(entidadeSaved);
    }

    @Override
    public void atualizarIndices(final List<TabelaClassificacaoItemDTO> itens) {

        itens.stream().forEach(i -> {

            this.getDAO().updatePermitirIndicePorPerfil(i.getIdItem(), i.isIndicePermitidoPorPerfil());
        });
    }

    @Override
    public void atualizarCodigoDoLeitor(final TabelaClassificacao entidade) {

        if (ColecaoUtil.isNotEmpty(entidade.getItens())) {

            List<Long> ids = entidade.getItens().stream().map(TabelaClassificacaoItemDTO::getIdItem).collect(Collectors.toList());

            this.getDAO().updateCodigoLeitor(ids, entidade.getCodigoLeitor(), entidade.getHostLeitor(), entidade.getPortaLeitor());
        }
    }

    @Override
    public void atualizarCodigoArmazemDeTerceiros(final TabelaClassificacao entidade) {

        if (ColecaoUtil.isNotEmpty(entidade.getItens())) {

            List<Long> ids = entidade.getItens().stream().map(TabelaClassificacaoItemDTO::getIdItem).collect(Collectors.toList());

            this.getDAO().updateCodigoOrigem(ids, entidade.getCodigoArmazemTerceiros());
        }
    }

    @Override
    public Specification<TabelaClassificacao> obterEspecification(final Paginacao<TabelaClassificacao> paginacao) {

        final TabelaClassificacao entidade = paginacao.getEntidade();

        final Specification<TabelaClassificacao> specification = new Specification<TabelaClassificacao>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<TabelaClassificacao> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                if (StringUtil.isNotNullEmpty(entidade.getItemClassificacao())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("itemClassificacao")), "%" + entidade.getItemClassificacao().toUpperCase() + "%")));
                }

                if (StringUtil.isNotNullEmpty(entidade.getDescricaoItemClassificacao())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("descricaoItemClassificacao")), "%" + entidade.getDescricaoItemClassificacao().toUpperCase() + "%")));
                }

                if (ObjetoUtil.isNotNull(entidade.getCodigoMaterial())) {

                    predicates.add(builder.equal(root.get("codigoMaterial"), entidade.getCodigoMaterial()));
                }

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Override
    public List<String> listarTabelaOrdernandoPorItem() {

        return this.getDAO().findDistinctDescricaoItemClassificacaoByOrderByItemClassificacaoAsc();
    }

    @Override
    public void salvarSAP(final EventOutRequest request, final String host, final String mandante) {

        if (ColecaoUtil.isNotEmpty(request.getData())) {

            final Map<String, SAPTabelaClassificacao> groupTabelaClassificacao = this.groupTabelaClassificacao(request);

            try {

                groupTabelaClassificacao.keySet().stream().forEach(itemGrupo -> {

                    try {

                        final SAPTabelaClassificacao tabelaClassificacao = groupTabelaClassificacao.get(itemGrupo);

                        TabelaClassificacao entidadeConsultada = this.tabelaClassificacaoCustomRepository.obterPorCodigoEItemClassificacaoEDescricaoItemClassificacaoEIndice(tabelaClassificacao.getCodigoMaterial().trim(),
                                tabelaClassificacao.getCodigoTabela().trim(), tabelaClassificacao.getItemClassificacao(), tabelaClassificacao.getDescricaoItemClassificacao(), tabelaClassificacao.getIndice());

                        entidadeConsultada = tabelaClassificacao.copy(entidadeConsultada);

                        String percentualAnterior = entidadeConsultada.getPercentualDesconto();

                        if (entidadeConsultada.isNew()) {

                            this.salvar(entidadeConsultada);

                        } else {

                            if (StringUtil.isNotNullEmpty(percentualAnterior) && !tabelaClassificacao.getPercentualDesconso().equals(percentualAnterior)) {

                                this.emailSenderService.sendEmail(new EmailAlteracaoTabelaClassificacao(entidadeConsultada, configuracaoEmailService.get(), percentualAnterior));
                            }

                            this.dao.updateTabelaClassificacao(entidadeConsultada.getId(), entidadeConsultada.getCodigo(), entidadeConsultada.getCodigoMaterial(), entidadeConsultada.getDescricaoItemClassificacao(),
                                    entidadeConsultada.getIndice(), entidadeConsultada.getItemClassificacao(), entidadeConsultada.getPercentualDesconto(), entidadeConsultada.getIndiceNumerico(), entidadeConsultada.isIndicePermitidoPorPerfil());
                        }

                    } catch (Exception e) {

                        TabelaClassificacaoServiceImpl.LOGGER.info("ERRO: " + e.getMessage());
                    }
                });

            } catch (Exception e) {

                TabelaClassificacaoServiceImpl.LOGGER.info("ERRO: " + e.getMessage());
            }
        }
    }

    private Map<String, SAPTabelaClassificacao> groupTabelaClassificacao(final EventOutRequest request) {

        final Map<String, SAPTabelaClassificacao> groupTabelaClassificacao = new HashMap<String, SAPTabelaClassificacao>();

        request.getData().forEach(registro -> {

            try {

                SAPTabelaClassificacao entidade = groupTabelaClassificacao.get(registro.getItemDocumento().trim());

                if (ObjetoUtil.isNull(entidade)) {

                    entidade = SAPTabelaClassificacao.newInstance();
                }

                entidade.update(registro.getCampo().trim(), registro.getValor().trim());

                groupTabelaClassificacao.put(registro.getItemDocumento().trim(), entidade);

            } catch (final Exception e) {

                e.printStackTrace();
            }
        });

        return groupTabelaClassificacao;
    }

    @Override
    public List<TabelaClassificacao> listarSincronizados(final Date data) {

        return ObjetoUtil.isNull(data) ? this.dao.findByStatusCadastroSapIn(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO})) :
                this.dao.findByStatusCadastroSapInAndDataCadastroGreaterThanEqual(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO}), data);
    }

    @Override
    public void sincronizar(final List<TabelaClassificacao> entidades) {

        if (ColecaoUtil.isNotEmpty(entidades)) {

            entidades.forEach(item -> {

                TabelaClassificacao entidadeSaved = this.getDAO().findByCodigo(item.getCodigo().trim());

                item.setId(null);

                if (ObjetoUtil.isNotNull(entidadeSaved)) {

                    item.setId(entidadeSaved.getId());
                }
            });

            this.salvar(entidades);
        }
    }

    @Override
    public TabelaClassificacaoDTO listByItemClassificacao(String itemClassificacao, String codigoMaterial) {

        return new TabelaClassificacaoDTO(this.getDAO().findByItemClassificacaoAndCodigoMaterialOrderByIndiceNumericoAsc(itemClassificacao, codigoMaterial));
    }

    @Override
    public TabelaClassificacaoDAO getDAO() {

        return this.dao;
    }

    @Override
    public Class<TabelaClassificacaoDTO> getDtoListagem() {

        return TabelaClassificacaoDTO.class;
    }

    public List<TabelaClassificacao> getLimiteTodosIndices() {

        List<TabelaClassificacao> todosItens = dao.findAll();

        List<TabelaClassificacao> limiteIndices = new ArrayList<>();

        todosItens.stream().filter(TabelaClassificacao::isIndicePermitidoPorPerfil).collect(Collectors.groupingBy(TabelaClassificacao::getDescricaoItemClassificacao)).forEach((indice, listaTabelas) -> {
            limiteIndices.add(listaTabelas.stream().min((tabela_1, tabela_2) -> tabela_1.getIndiceNumerico().compareTo(tabela_2.getIndiceNumerico())).get());
        });

        return limiteIndices;
    }

}
