package br.com.sjc.service;

import br.com.sjc.fachada.service.ItemNFPedidoService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.dto.ItemNFPedidoListagemDTO;
import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.sap.ItemNFPedido;
import br.com.sjc.persistencia.dao.ItemNFPedidoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ItemNFPedidoServiceImpl extends ServicoGenerico<Long, ItemNFPedido> implements ItemNFPedidoService {

    @Autowired
    private ItemNFPedidoDAO dao;

    @Override
    public Class<ItemNFPedidoListagemDTO> getDtoListagem() {

        return ItemNFPedidoListagemDTO.class;
    }

    @Override
    public ItemNFPedidoDAO getDAO() {

        return dao;
    }

    @Override
    public void atualizarRequestKey(String requestKey, Long id) {

        dao.updateRequestKey(requestKey, id);
    }

    @Override
    public void atualizarDocumentoFaturamento(Long documentoFaturamento, String requestKey, String numeroRomaneio) {

        dao.updateDocumentoFaturamento(documentoFaturamento, requestKey, numeroRomaneio);
    }

    @Override
    public Boolean existeNotaSerieDataInicioDataFimProdutorSafraMaterialStatusSapStatusRomaneioIdItem(final String nota, final String serie, final Date dataInicio, final Date dataFim, final Long idProdutor, final Long idSafra,
                                                                                                      final Long idMaterial, StatusIntegracaoSAPEnum status, final StatusRomaneioEnum statusRomaneio, final Long idItem) {

        return dao.existsByNumeroNfeAndSerieNfeAndDataNfeAndProdutorIdAndSafraIdAndMaterialIdAndRomaneioStatusIntegracaoSAPOrRomaneioStatusRomaneioAndIdNe(nota, serie, dataInicio, dataFim, idProdutor, idSafra, idMaterial, status, statusRomaneio, idItem);
    }

    @Override
    public Boolean existePlacaCavaloStatusRomaneioRomaneioDiferenteDe(final Long idPlaca, final StatusRomaneioEnum statusRomaneio, final Long idRomaneio) {

        return dao.existsByPlacaCavaloIdAndRomaneioStatusRomaneioAndRomaneioIdNot(idPlaca, statusRomaneio, idRomaneio);
    }

    @Override
    public Boolean existePlacaCavaloStatusRomaneio(final Long idPlaca, final StatusRomaneioEnum statusRomaneio) {

        return dao.existsByPlacaCavaloIdAndRomaneioStatusRomaneio(idPlaca, statusRomaneio);
    }
}
