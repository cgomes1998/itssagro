package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.ConversaoLitragemService;
import br.com.sjc.fachada.service.TabelaValorService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.TabelaValorItem;
import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.sap.ConversaoLitragem;
import br.com.sjc.modelo.sap.ConversaoLitragemItem;
import br.com.sjc.modelo.sap.ItemNFPedido;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.persistencia.dao.ConversaoLitragemDAO;
import br.com.sjc.util.BigDecimalUtil;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.bundle.MessageSupport;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.Date;

@Service
public class ConversaoLitragemServiceImpl extends ServicoGenerico<Long, ConversaoLitragem> implements ConversaoLitragemService {

    private ConversaoLitragemDAO conversaoLitragemDAO;

    private TabelaValorService tabelaValorService;

    public ConversaoLitragemServiceImpl(ConversaoLitragemDAO conversaoLitragemDAO, TabelaValorService tabelaValorService) {

        this.conversaoLitragemDAO = conversaoLitragemDAO;
        this.tabelaValorService = tabelaValorService;
    }

    @Override
    public void preSalvar(ConversaoLitragem entidade) {

        super.preSalvar(entidade);
        entidade.getConversoes().forEach(conversaoLitragemItem -> conversaoLitragemItem.setConversaoLitragem(entidade));
    }

    @Override
    public ConversaoLitragem salvar(Romaneio romaneio, Parametro parametro) {

        if (ObjetoUtil.isNotNull(parametro) && romaneio.getConversaoLitragem().isValidarConversao()) {
            BigDecimal diferencaLitros = this.calcularDiferencaLitros(romaneio);
            if (diferencaLitros != null && diferencaLitros.compareTo(new BigDecimal(parametro.getValor()).abs()) > 0 && romaneio.getOperacao().isPossuiValidacaoConversaoLitragem()) {
                throw new ServicoException(MessageSupport.getMessage("MSGE022"));
            } else if (diferencaLitros != null && diferencaLitros.compareTo(new BigDecimal(parametro.getValor()).abs()) > 0) {
                romaneio.setAlerta(MessageSupport.getMessage("MSGE022"));
            }
        }
        return this.salvar(romaneio.getConversaoLitragem());
    }

    @Override
    public BigDecimal obterDiferencaLitros(Romaneio romaneio) {

        if (ObjetoUtil.isNotNull(romaneio) && ObjetoUtil.isNotNull(romaneio.getPesagem()) && BigDecimalUtil.isMaiorZero(this.obterPesoLiquido(romaneio)) && ObjetoUtil.isNotNull(romaneio.getConversaoLitragem()) && romaneio.getConversaoLitragem().isNotNull()) {
            if (ColecaoUtil.isEmpty(romaneio.getConversaoLitragem().getConversoes())) {
                romaneio.getConversaoLitragem().setMaterial(ColecaoUtil.isNotEmpty(romaneio.getItens()) ? romaneio.getItens().stream().map(ItemNFPedido::getMaterial).findFirst().get() : romaneio.getSenha().getMaterial());
                TabelaValorItem tabelaValorItem = this.tabelaValorService.obterItensParaConversao(romaneio.getConversaoLitragem());
                romaneio.getConversaoLitragem().getConversoes().add(this.getConversaoLitragemItem(romaneio, tabelaValorItem));
                this.salvar(romaneio.getConversaoLitragem());
            }
            return this.calcularDiferencaLitros(romaneio);
        }
        return null;
    }

    private ConversaoLitragemItem getConversaoLitragemItem(Romaneio romaneio, TabelaValorItem tabelaValorItem) {

        ConversaoLitragemItem newItem = new ConversaoLitragemItem();
        newItem.setDataCadastro(new Date());
        newItem.setTanque(romaneio.getConversaoLitragem().getTanque());
        newItem.setTempTanque(romaneio.getConversaoLitragem().getTempTanque());
        newItem.setGrauInpm(romaneio.getConversaoLitragem().getGrauInpm());
        newItem.setAcidez(romaneio.getConversaoLitragem().getAcidez());
        newItem.setVolumeSetaVinteGraus(tabelaValorItem.getFatorReducao().multiply(BigDecimal.valueOf(romaneio.getSenha().getSeta().getQtd())));
        newItem.setVolumeDensidadeLVinteGraus(this.obterPesoLiquido(romaneio).divide(tabelaValorItem.getMassaEspecificaVinteGraus().divide(BigDecimal.valueOf(1000), 5, RoundingMode.HALF_UP), 2, RoundingMode.HALF_UP));
        newItem.setDiferencaLitros(newItem.getVolumeSetaVinteGraus().subtract(newItem.getVolumeDensidadeLVinteGraus()));
        return newItem;
    }

    private BigDecimal obterPesoLiquido(Romaneio romaneio) {

        if (BigDecimalUtil.isMaiorZero(romaneio.getPesagem().getPesoInicial()) && BigDecimalUtil.isMaiorZero(romaneio.getPesagem().getPesoFinal())) {
            return romaneio.getPesagem().getPesoInicial().subtract(romaneio.getPesagem().getPesoFinal());
        }
        return BigDecimal.ZERO;
    }

    private BigDecimal calcularDiferencaLitros(Romaneio romaneio) {

        if (ObjetoUtil.isNotNull(romaneio)) {
            return romaneio.getConversaoLitragem().getConversoes().stream().max(Comparator.comparing(ConversaoLitragemItem::getDataCadastro)).map(item -> item.getDiferencaLitros().multiply(BigDecimal.valueOf(100)).divide(item.getVolumeSetaVinteGraus(), RoundingMode.HALF_UP).abs()).orElse(null);
        }
        return null;
    }

    @Override
    public Specification<ConversaoLitragem> obterEspecification(final Paginacao<ConversaoLitragem> paginacao) {

        return null;
    }

    @Override
    public DAO<Long, ConversaoLitragem> getDAO() {

        return this.conversaoLitragemDAO;
    }

    @Override
    public Class<? extends DataDTO> getDtoListagem() {

        return null;
    }

}
