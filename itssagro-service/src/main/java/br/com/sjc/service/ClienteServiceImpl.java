package br.com.sjc.service;

import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.ClienteService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.dto.ClienteDTO;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.sap.Cliente;
import br.com.sjc.modelo.sap.response.item.RfcClienteResponseItem;
import br.com.sjc.persistencia.dao.ClienteDAO;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

/**
 * Created by julio.bueno on 27/06/2019.
 */
@Service
public class ClienteServiceImpl extends ServicoGenerico<Long, Cliente> implements ClienteService {

    private static final Logger LOG = Logger.getLogger(ClienteServiceImpl.class.getName());

    @Autowired
    private ClienteDAO dao;

    @Override
    public ClienteDAO getDAO() {

        return dao;
    }

    @Override
    public void salvarSAP(List<RfcClienteResponseItem> retorno) {

        if (ColecaoUtil.isNotEmpty(retorno)) {

            retorno.stream().forEach(item -> {

                try {

                    if (StringUtil.isNotNullEmpty(item.getCodigo())) {

                        Cliente entidadeSaved = this.getDAO().findByCodigo(new Long(item.getCodigo()).toString());

                        if (ObjetoUtil.isNull(entidadeSaved)) {

                            entidadeSaved = new Cliente();

                            entidadeSaved.setDataCadastro(DateUtil.hoje());

                            entidadeSaved.setStatus(StatusEnum.ATIVO);

                        } else {

                            entidadeSaved.setDataAlteracao(DateUtil.hoje());
                        }

                        entidadeSaved = item.copy(entidadeSaved);

                        this.getDAO().save(entidadeSaved);
                    }

                } catch (Exception e) {

                    ClienteServiceImpl.LOG.info(MessageFormat.format("ERRO AO PERSISTIR CLIENTE: {0}, ERRO: {1}", item.getCodigo(), e.getMessage()));
                }
            });
        }
    }

    @Override
    public List<Cliente> buscarClientesAutoComplete(String value) {

        return this.dao.findTop10ByNomeIgnoreCaseContainingOrCodigoIgnoreCaseContainingOrCpfCnpjIgnoreCaseContainingOrderByCodigoAsc(value, value, value);
    }

    @Override
    public Cliente obterPorCodigo(String codigoCliente) {

        return this.dao.findByCodigo(codigoCliente);
    }

    @Override
    public Specification<Cliente> obterEspecification(Paginacao<Cliente> paginacao) {

        return (Specification<Cliente>) (root, query, criteriaBuilder) -> {
            Cliente cliente = paginacao.getEntidade();

            List<Predicate> predicates = new ArrayList<>();

            if (StringUtil.isNotNullEmpty(cliente.getCodigo())) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("codigo")), MatchMode.ANYWHERE.toMatchString(cliente.getCodigo())));
            }

            if (StringUtil.isNotNullEmpty(cliente.getNome())) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("nome")), MatchMode.ANYWHERE.toMatchString(cliente.getNome())));
            }

            if (StringUtil.isNotNullEmpty(cliente.getCpfCnpj())) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("cpfCnpj")), StringUtil.removerCaracteresEspeciais(cliente.getCpfCnpj())));
            }

            if (!Objects.isNull(cliente.getStatusRegistro())) {
                predicates.add(criteriaBuilder.equal(root.get("statusRegistro"), cliente.getStatusRegistro()));
            }

            if (!Objects.isNull(cliente.getTipoPessoa())) {
                predicates.add(criteriaBuilder.equal(root.get("tipoPessoa"), cliente.getTipoPessoa()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    @Override
    public Class<ClienteDTO> getDtoListagem() {

        return ClienteDTO.class;
    }

}
