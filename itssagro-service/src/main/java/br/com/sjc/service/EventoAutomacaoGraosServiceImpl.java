package br.com.sjc.service;

import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.EventoAutomacaoGraosService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.*;
import br.com.sjc.modelo.cfg.Ambiente;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.cfg.DadosSincronizacao_;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.dto.EventoAutomacaoGraosDTO;
import br.com.sjc.modelo.enums.CriterioTipoEnum;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.persistencia.dao.EventoAutomacaoGraosDAO;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Service
public class EventoAutomacaoGraosServiceImpl extends ServicoGenerico<Long, EventoAutomacaoGraos> implements EventoAutomacaoGraosService {

	private static final Logger LOGGER = Logger.getLogger ( EventoAutomacaoGraosServiceImpl.class.getName () );

	@Autowired
	private EventoAutomacaoGraosDAO dao;

	@Override
	public Specification<EventoAutomacaoGraos> obterEspecification(final Paginacao<EventoAutomacaoGraos> paginacao) {

		final EventoAutomacaoGraos entidade = paginacao.getEntidade();

		final Specification<EventoAutomacaoGraos> specification = new Specification<EventoAutomacaoGraos>() {

			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(final Root<EventoAutomacaoGraos> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

				final List<Predicate> predicates = new ArrayList<>();

				if (StringUtil.isNotNullEmpty(entidade.getDescricao())) {

					predicates.add(builder.and(builder.like(builder.upper(root.get("descricao")), "%" + entidade.getDescricao().toUpperCase() + "%")));
				}

				return builder.and(predicates.toArray(new Predicate[]{}));
			}
		};

		return specification;
	}

	@Override
	public void preSalvar(EventoAutomacaoGraos entidade) {

		super.preSalvar(entidade);

		if (ColecaoUtil.isNotEmpty(entidade.getEtapas())) {

			entidade.getEtapas().forEach(eventoAutomacaoGraosEtapa -> {
				
				eventoAutomacaoGraosEtapa.setEventoAutomacaoGraos(entidade);

				if (ColecaoUtil.isNotEmpty(eventoAutomacaoGraosEtapa.getEventos())) {

					eventoAutomacaoGraosEtapa.getEventos().forEach(etapaCriterioEvento -> etapaCriterioEvento.setEventoAutomacaoGraosEtapa(eventoAutomacaoGraosEtapa));
				}
			});
		}
	}

	@Override
	public Optional<EventoAutomacaoGraos> obterEventoASerExecutado(Romaneio romaneio, EtapaEnum etapaAtual, CriterioTipoEnum criterioDaEtapaAtual) {
		
		try {

			Optional<EventoAutomacaoGraos> optionalEventoAutomacaoGraos = Optional.of(get(((root, query, criteriaBuilder) -> {

				Join<EventoAutomacaoGraos, DadosSincronizacao> dadosSincronizacaoJoin = root.join(EventoAutomacaoGraos_.operacoes);

				Join<EventoAutomacaoGraos, EventoAutomacaoGraosEtapa> eventoAutomacaoGraosEtapaJoin = root.join(EventoAutomacaoGraos_.etapas);

				Join<EventoAutomacaoGraosEtapa, EtapaCriterioEvento> etapaCriterioEventoJoin = eventoAutomacaoGraosEtapaJoin.join(EventoAutomacaoGraosEtapa_.eventos);

				List<Predicate> predicates = new ArrayList<>();

				predicates.add(criteriaBuilder.equal(dadosSincronizacaoJoin.get(DadosSincronizacao_.id), romaneio.getOperacao().getId()));

				predicates.add(criteriaBuilder.equal(eventoAutomacaoGraosEtapaJoin.get(EventoAutomacaoGraosEtapa_.etapa), etapaAtual));

				predicates.add(criteriaBuilder.equal(etapaCriterioEventoJoin.get(EtapaCriterioEvento_.criterioTipo), criterioDaEtapaAtual.name()));

				return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));
			})));

			return optionalEventoAutomacaoGraos;

		} catch (IncorrectResultSizeDataAccessException e) {

			e.printStackTrace();

			EventoAutomacaoGraosServiceImpl.LOGGER.severe(
				   String.format(
				   	   "Mais de um evento encontrado para os seguintes parametros: \nId da Operação: %s \nEtapa: %s \nCriterio Tipo: %s",
					      romaneio.getOperacao().getId().toString(),
					      etapaAtual.name(),
					      criterioDaEtapaAtual.name()
				   )
			);
		}

		return Optional.empty();
	}

	@Override
	public EventoAutomacaoGraosDAO getDAO() {

		return this.dao;
	}

	@Override
	public Class<? extends DataDTO> getDtoListagem() {

		return EventoAutomacaoGraosDTO.class;
	}

}
