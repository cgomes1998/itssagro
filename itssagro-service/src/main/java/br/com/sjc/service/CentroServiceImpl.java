package br.com.sjc.service;

import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.CentroService;
import br.com.sjc.fachada.service.ParametroService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.sap.Centro;
import br.com.sjc.modelo.sap.Deposito;
import br.com.sjc.modelo.dto.CentroDTO;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.sap.request.EventOutRequest;
import br.com.sjc.modelo.sap.sync.SAPCentro;
import br.com.sjc.persistencia.dao.CentroDAO;
import br.com.sjc.persistencia.dao.DepositoDAO;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;
import java.util.logging.Logger;

@Service
public class CentroServiceImpl extends ServicoGenerico<Long, Centro> implements CentroService {

    private static final Logger LOG = Logger.getLogger(CentroServiceImpl.class.getName());

    @Autowired
    private CentroDAO dao;

    @Autowired
    private DepositoDAO depositoDAO;

    @Autowired
    private ParametroService parametroService;

    @Override
    public List<Centro> listar() {
        return this.getDAO().findByOrderByCodigoAsc();
    }

    @Override
    public Specification<Centro> obterEspecification(final Paginacao<Centro> paginacao) {

        final Centro entidade = paginacao.getEntidade();

        final Specification<Centro> specification = new Specification<Centro>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<Centro> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                if (StringUtil.isNotNullEmpty(entidade.getCodigo())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("codigo")), "%" + entidade.getCodigo().toUpperCase() + "%")));
                }

                if (StringUtil.isNotNullEmpty(entidade.getDescricao())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("descricao")), "%" + entidade.getDescricao().toUpperCase() + "%")));
                }

                if (ObjetoUtil.isNotNull(entidade.getStatusRegistro())) {

                    predicates.add(builder.equal(root.get("statusRegistro"), entidade.getStatusRegistro()));
                }

                if (ObjetoUtil.isNotNull(entidade.getStatusCadastroSap())) {

                    predicates.add(builder.equal(root.get("statusCadastroSap"), entidade.getStatusCadastroSap()));
                }

                query.orderBy(builder.asc(root.get("codigo")));

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Override
    public void salvarSAP(final EventOutRequest request, final String host, final String mandante) {

        if (ColecaoUtil.isNotEmpty(request.getData())) {

            final Map<String, SAPCentro> groupCentro = this.groupCentro(request);

            groupCentro.keySet().forEach(itemGrupo -> {

                final SAPCentro entidadeSAP = groupCentro.get(itemGrupo);

                if (StringUtil.isNotNullEmpty(entidadeSAP.getCodigo()) && StringUtil.isNotNullEmpty(entidadeSAP.getCodigoDeposito())) {

                    this.salvarDeposito(entidadeSAP, this.salvarCentro(entidadeSAP));
                }
            });
        }
    }

    private Centro salvarCentro(final SAPCentro entidadeSAP) {

        Centro entidade = this.getDAO().findByCodigo(entidadeSAP.getCodigo());

        entidade = entidadeSAP.copy(entidade);

        entidade = this.salvar(entidade);

        return entidade;
    }

    private void salvarDeposito(final SAPCentro entidadeSAP, final Centro centro) {

        Deposito entidade = this.depositoDAO.findByCodigoAndCentroId(entidadeSAP.getCodigoDeposito(), centro.getId());

        entidade = entidadeSAP.copyDeposito(entidade);

        entidade.setCentro(centro);

        this.depositoDAO.save(entidade);
    }

    private Map<String, SAPCentro> groupCentro(final EventOutRequest request) {

        final Map<String, SAPCentro> groupCentro = new HashMap<String, SAPCentro>();

        request.getData().forEach(registro -> {

            SAPCentro entidade = groupCentro.get(registro.getItemDocumento().trim());

            if (ObjetoUtil.isNull(entidade)) {

                entidade = SAPCentro.newInstance();
            }

            try {

                entidade.update(registro.getCampo().trim(), registro.getValor().trim());

                groupCentro.put(registro.getItemDocumento().trim(), entidade);

            } catch (final Exception e) {

                CentroServiceImpl.LOG.info("ERRO: " + e.getMessage());
            }
        });

        return groupCentro;
    }


    @Override
    public void sincronizar(final List<Centro> entidades) {

        if (ColecaoUtil.isNotEmpty(entidades)) {

            entidades.forEach(item -> {

                Centro entidadeSaved = this.getDAO().findByCodigo(item.getCodigo());

                item.setId(null);

                if (ObjetoUtil.isNotNull(entidadeSaved)) {

                    item.setId(entidadeSaved.getId());
                }

                item.getDepositos().forEach(d -> {

                    d.setId(null);

                    d.setCentro(item);
                });
            });

            this.salvar(entidades);
        }
    }

    @Override
    public List<Centro> listarSincronizados(final Date data) {
        return ObjetoUtil.isNull(data) ? this.dao.findByStatusCadastroSapIn(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO})) : this.dao.findByStatusCadastroSapInAndDataCadastroGreaterThanEqual(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO}), data);
    }

    @Override
    public Centro obterPorCodigo(final String codigo) {
        return this.dao.findByCodigo(codigo);
    }

    @Override
    public CentroDAO getDAO() {
        return this.dao;
    }

    @Override
    public Class<CentroDTO> getDtoListagem() {
        return CentroDTO.class;
    }
}
