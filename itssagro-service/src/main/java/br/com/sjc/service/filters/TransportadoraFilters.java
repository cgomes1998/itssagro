package br.com.sjc.service.filters;

import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.request.EventInRequest;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;
import br.com.sjc.modelo.sap.sync.SAPTransportadora;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransportadoraFilters extends Filters {

    @Override
    public JCoRequest instace(final String centro, final String requestKey, final DadosSincronizacaoEnum entidade, final DadosSincronizacao dadosSincronizacao, final String rfcProgramID, final String codigo, final Date ultimaSincronizacao) {

        return EventInRequest.instance(dadosSincronizacao, centro, rfcProgramID, this.getTransportadoraFilters(requestKey, ultimaSincronizacao), false, null);
    }

    private List<EventInRequestItem> getTransportadoraFilters(final String requestKey, final Date ultimaSincronizacao) {

        final Map<String, String> fields = new HashMap<String, String>();

        if (ObjetoUtil.isNotNull(ultimaSincronizacao)) {

            fields.put(SAPTransportadora.COLUMN_INPUT.DATA_ULT_INT.name(), DateUtil.formatToSAP(ultimaSincronizacao));
        }

        return this.addFilters(requestKey, fields, "1");
    }

}
