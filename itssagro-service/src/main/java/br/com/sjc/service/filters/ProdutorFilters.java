package br.com.sjc.service.filters;

import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.request.EventInRequest;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;
import br.com.sjc.modelo.sap.sync.SAPProdutorRural;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProdutorFilters extends Filters {

    @Override
    public JCoRequest instace(final String centro, final String requestKey, final DadosSincronizacaoEnum entidade, final DadosSincronizacao dadosSincronizacao, final String rfcProgramID, final String codigo, final Date ultimaSincronizacao) {

        return EventInRequest.instance(dadosSincronizacao, centro, rfcProgramID, this.getProdutorRuralFilters(requestKey, ultimaSincronizacao), false, null);
    }

    private List<EventInRequestItem> getProdutorRuralFilters(final String requestKey, final Date ultimaSincronizacao) {

        final Map<String, String> fields = new HashMap<String, String>();

        if (ObjetoUtil.isNotNull(ultimaSincronizacao)) {

            fields.put(SAPProdutorRural.COLUMN_INPUT.DT_ULT_INF.name(), ObjetoUtil.isNull(ultimaSincronizacao) ? StringUtil.empty() : DateUtil.formatToSAP(ultimaSincronizacao));
        }

        return this.addFilters(requestKey, fields, "1");
    }

}
