package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.service.CarregamentoService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.Carregamento;
import br.com.sjc.persistencia.dao.CarregamentoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class CarregamentoServiceImpl extends ServicoGenerico<Long, Carregamento> implements CarregamentoService {

    @Autowired
    private CarregamentoDAO carregamentoDAO;

    @Override
    public Integer obterProximoCertificado(Long material, String tanque) {
        return Integer.valueOf(String.valueOf(getDAO().count() + 1).concat(String.valueOf(LocalDate.now().getYear())));
    }

    @Override
    public DAO<Long, Carregamento> getDAO() {
        return this.carregamentoDAO;
    }


}
