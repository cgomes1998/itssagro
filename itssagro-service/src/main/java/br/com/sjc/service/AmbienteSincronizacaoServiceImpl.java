package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.service.AmbienteSincronizacaoService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.cfg.*;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoCallerService;
import br.com.sjc.persistencia.dao.AmbienteDAO;
import br.com.sjc.persistencia.dao.AmbienteSincronizacaoDAO;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class AmbienteSincronizacaoServiceImpl extends ServicoGenerico<Long, AmbienteSincronizacao> implements AmbienteSincronizacaoService {

    @Autowired
    private AmbienteSincronizacaoDAO dao;

    @Autowired
    private AmbienteDAO ambienteDAO;

    @Override
    public AmbienteSincronizacao obterPorDadosSincronizacao(final DadosSincronizacao dadosSincronizacao) {
        return get(((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(
                root.join(AmbienteSincronizacao_.dadosSincronizacao).get(DadosSincronizacao_.id), dadosSincronizacao.getId()
        )));
    }

    @Override
    public void updateRequestCount(DadosSincronizacao dadosSincronizacao, JCoCallerService sap) {

        try {

            AmbienteSincronizacao sincronizacao = obterPorDadosSincronizacao(dadosSincronizacao);

            if (ObjetoUtil.isNotNull(sincronizacao)) {

                final long requestCount = sincronizacao.getRequestCount();

                sincronizacao.setUltimaSincronizacao(DateUtil.hoje());

                final long newRequestCount = requestCount + 1;

                sincronizacao.setRequestCount(newRequestCount);

            } else {

                sincronizacao = new AmbienteSincronizacao();

                sincronizacao.setRequestCount(2);

                sincronizacao.setAmbiente(ambienteDAO.findByHostAndMandante(sap.getService().getApplicationServerHost(), sap.getService().getClient()));

                sincronizacao.setDadosSincronizacao(dadosSincronizacao);
            }

            salvar(sincronizacao);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Override
    public void updateRequestCount(AmbienteSincronizacao sincronizacao) {

        if (ObjetoUtil.isNull(sincronizacao)) {

            return;
        }

        if (ObjetoUtil.isNull(sincronizacao.getId())) {

            sincronizacao.setUltimaSincronizacao(new Date());

            this.dao.save(sincronizacao);

            return;
        }

        sincronizacao = this.get(sincronizacao.getId());

        final long requestCount = sincronizacao.getRequestCount();

        sincronizacao.setUltimaSincronizacao(DateUtil.hoje());

        final long newRequestCount = requestCount + 1;

        sincronizacao.setRequestCount(newRequestCount);

        this.dao.save(sincronizacao);
    }

    @Override
    public AmbienteSincronizacao obterAmbienteSincronizacao(final DadosSincronizacao dadosSincronizacao, final JCoCallerService sap) {

        if (ObjetoUtil.isNull(dadosSincronizacao)) {

            return null;
        }

        final String mandante = sap.getService().getClient();

        final String host = sap.getService().getApplicationServerHost();

        final Ambiente ambiente = this.ambienteDAO.findByHostAndMandante(host, mandante);

        AmbienteSincronizacao sincronizacao = obterPorAmbienteESincronizacao(dadosSincronizacao, ambiente);

        if (ObjetoUtil.isNull(sincronizacao)) {

            sincronizacao = new AmbienteSincronizacao();

            sincronizacao.setAmbiente(ambiente);

            sincronizacao.setDadosSincronizacao(dadosSincronizacao);

            sincronizacao.setRequestCount(1l);
        }

        return sincronizacao;
    }

    @Override
    public AmbienteSincronizacao obterPorAmbienteESincronizacao(final DadosSincronizacao dadosSincronizacao, final Ambiente ambiente) {

        return get(((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(
                criteriaBuilder.equal(root.join(AmbienteSincronizacao_.dadosSincronizacao).get(DadosSincronizacao_.id), dadosSincronizacao.getId()),
                criteriaBuilder.equal(root.join(AmbienteSincronizacao_.ambiente).get(Ambiente_.id), ambiente.getId())
        )));
    }

    @Override
    public DAO<Long, AmbienteSincronizacao> getDAO() {
        return this.dao;
    }

    @Override
    public Class<? extends DataDTO> getDtoListagem() {
        return null;
    }
}
