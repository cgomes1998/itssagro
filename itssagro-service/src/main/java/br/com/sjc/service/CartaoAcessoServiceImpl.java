package br.com.sjc.service;

import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.CartaoAcesso;
import br.com.sjc.modelo.CartaoAcesso_;
import br.com.sjc.modelo.dto.AmbienteDTO;
import br.com.sjc.modelo.dto.CartaoAcessoDTO;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.persistencia.dao.CartaoAcessoDAO;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class CartaoAcessoServiceImpl extends ServicoGenerico<Long, CartaoAcesso> {

    @Autowired
    private CartaoAcessoDAO dao;

    @Override
    public void preSalvar(CartaoAcesso entidade) {

        super.preSalvar(entidade);

        cartaoDeAcessoJaCadastrado(entidade);
    }

    private void cartaoDeAcessoJaCadastrado(CartaoAcesso entidade) {

        Long count = dao.count(((root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            if (ObjetoUtil.isNotNull(entidade.getId())) {

                predicates.add(criteriaBuilder.notEqual(root.get(CartaoAcesso_.id), entidade.getId()));
            }

            predicates.add(criteriaBuilder.or(criteriaBuilder.equal(root.get(CartaoAcesso_.codigo), entidade.getCodigo()), criteriaBuilder.equal(root.get(CartaoAcesso_.numeroCartaoAcesso), entidade.getNumeroCartaoAcesso())));

            return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));
        }));

        if (ObjetoUtil.isNotNull(count) && count > 0L) {

            throw new ServicoException("O cartão de acesso informado já foi cadastrado.");
        }
    }

    public CartaoAcesso obterCartaoDeAcesso(String numero) {

        CartaoAcesso cartaoAcesso = dao.findOne(((root, query, criteriaBuilder) -> {
            Integer numeroCartaoAcesso = StringUtil.parseInt(StringUtil.apenasNumeros(numero));
            if (ObjetoUtil.isNotNull(numeroCartaoAcesso)) {
                return criteriaBuilder.or(criteriaBuilder.equal(root.get(CartaoAcesso_.numeroCartaoAcesso), numeroCartaoAcesso), criteriaBuilder.equal(root.get(CartaoAcesso_.codigo), numero));
            }
            return criteriaBuilder.equal(root.get(CartaoAcesso_.codigo), numero);
        })).orElse(null);

        if (ObjetoUtil.isNull(cartaoAcesso)) {

            throw new ServicoException("O cartão de acesso informado não foi cadastrado.");
        }

        return cartaoAcesso;
    }

    @Override
    public Specification<CartaoAcesso> obterEspecification(final Paginacao<CartaoAcesso> paginacao) {

        final CartaoAcesso entidade = paginacao.getEntidade();

        final Specification<CartaoAcesso> specification = new Specification<CartaoAcesso>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<CartaoAcesso> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                if (StringUtil.isNotNullEmpty(entidade.getCodigo())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("codigo")), "%" + entidade.getCodigo().toUpperCase() + "%")));
                }

                if (ObjetoUtil.isNotNull(entidade.getNumeroCartaoAcesso())) {

                    predicates.add(builder.equal(root.get("numeroCartaoAcesso"), entidade.getNumeroCartaoAcesso()));
                }

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Override
    public CartaoAcessoDAO getDAO() {

        return this.dao;
    }

    @Override
    public Class<CartaoAcessoDTO> getDtoListagem() {

        return CartaoAcessoDTO.class;
    }

    @Override
    public Boolean getRegistrarLogAtividade() {

        return Boolean.TRUE;
    }

}
