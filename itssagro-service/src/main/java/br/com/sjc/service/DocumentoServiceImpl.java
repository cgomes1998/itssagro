package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.DocumentoService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.Documento;
import br.com.sjc.modelo.dto.DocumentoDTO;
import br.com.sjc.persistencia.dao.DocumentoDAO;
import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by julio.bueno on 03/07/2019.
 */
@Service
public class DocumentoServiceImpl extends ServicoGenerico<Long, Documento> implements DocumentoService {

    @Autowired
    private DocumentoDAO documentoDAO;

    @Override
    public Documento obterAssinatura() {

        return documentoDAO.findByAssinaturaTrue();
    }

    @Override
    public DAO<Long, Documento> getDAO() {

        return documentoDAO;
    }

    @Override
    public Class<DocumentoDTO> getDtoListagem() {

        return DocumentoDTO.class;
    }

    @Override
    public Specification<Documento> obterEspecification(Paginacao<Documento> paginacao) {

        return (Specification<Documento>) (root, query, criteriaBuilder) -> {
            Documento documento = paginacao.getEntidade();

            List<Predicate> predicates = new ArrayList<>();

            if (!Objects.isNull(documento.getCodigo())) {
                predicates.add(criteriaBuilder.like(root.get("codigo"), MatchMode.ANYWHERE.toMatchString(documento.getCodigo().toLowerCase())));
            }

            if (!StringUtils.isEmpty(documento.getDescricao())) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("descricao")), MatchMode.ANYWHERE.toMatchString(documento.getDescricao().toLowerCase())));
            }

            if (!Objects.isNull(documento.getStatus())) {
                predicates.add(criteriaBuilder.equal(root.get("status"), documento.getStatus()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    @Override
    public Boolean getRegistrarLogAtividade() {

        return Boolean.TRUE;
    }

}
