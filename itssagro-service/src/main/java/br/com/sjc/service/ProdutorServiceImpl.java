package br.com.sjc.service;

import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.ParametroService;
import br.com.sjc.fachada.service.ProdutorService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.dto.ProdutorDTO;
import br.com.sjc.modelo.endpoint.request.item.FornecedorRequest;
import br.com.sjc.modelo.endpoint.response.item.FornecedorResponse;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.sap.Cliente;
import br.com.sjc.modelo.sap.Produtor;
import br.com.sjc.modelo.sap.response.item.RfcProdutorResponseItem;
import br.com.sjc.modelo.sap.sync.SAPProdutorRural;
import br.com.sjc.persistencia.dao.ProdutorDAO;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.encrypt.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class ProdutorServiceImpl extends ServicoGenerico<Long, Produtor> implements ProdutorService {

    private static final Logger LOGGER = Logger.getLogger(ProdutorServiceImpl.class.getName());

    @Autowired
    private ProdutorDAO dao;

    @Autowired
    private ParametroService parametroService;

    @Override
    public Specification<Produtor> obterEspecification(final Paginacao<Produtor> paginacao) {

        final Produtor entidade = paginacao.getEntidade();

        final Specification<Produtor> specification = new Specification<Produtor>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<Produtor> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                if (StringUtil.isNotNullEmpty(entidade.getCodigo())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("codigo")), "%" + entidade.getCodigo().toUpperCase() + "%")));
                }

                if (StringUtil.isNotNullEmpty(entidade.getNome())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("nome")), "%" + entidade.getNome().toUpperCase() + "%")));
                }

                if (ObjetoUtil.isNotNull(entidade.getStatusRegistro())) {

                    predicates.add(builder.equal(root.get("statusRegistro"), entidade.getStatusRegistro()));
                }

                if (ObjetoUtil.isNotNull(entidade.getStatusCadastroSap())) {

                    predicates.add(builder.equal(root.get("statusCadastroSap"), entidade.getStatusCadastroSap()));
                }

                if (ObjetoUtil.isNotNull(entidade.getTipo())) {

                    predicates.add(builder.equal(root.get("tipo"), entidade.getTipo()));
                }

                if (StringUtil.isNotNullEmpty(entidade.getCnpj())) {

                    predicates.add(builder.equal(root.get("cnpj"), StringUtil.removerCaracteresEspeciais(entidade.getCnpj())));
                }

                if (StringUtil.isNotNullEmpty(entidade.getCpf())) {

                    predicates.add(builder.equal(root.get("cpf"), StringUtil.removerCaracteresEspeciais(entidade.getCpf())));
                }

                query.orderBy(builder.asc(root.get("codigo")));

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Override
    public void salvarSAP(final List<RfcProdutorResponseItem> data) {

        if (ColecaoUtil.isNotEmpty(data)) {

            data.stream().forEach(produtor -> {

                Produtor entidadeSaved = this.getDAO().findByCodigo(produtor.getCodigoProdutor());

                entidadeSaved = produtor.copy(entidadeSaved);

                if (StringUtil.isNotNullEmpty(entidadeSaved.getCodigo())) {

                    this.salvar(entidadeSaved);
                }
            });
        }
    }

    @Override
    public List<Produtor> listarSincronizados(final Date data) {

        return ObjetoUtil.isNull(data) ? this.getDAO().findByStatusCadastroSapIn(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO})) : this.getDAO().findByStatusCadastroSapInAndDataCadastroGreaterThanEqual(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO}), data);
    }

    @Override
    public void sincronizar(final List<Produtor> entidades) {

        if (ColecaoUtil.isNotEmpty(entidades)) {

            entidades.forEach(item -> {

                Produtor entidadeSaved = this.getDAO().findByCodigo(item.getCodigo());

                item.setId(null);

                if (ObjetoUtil.isNotNull(entidadeSaved)) {

                    item.setId(entidadeSaved.getId());
                }
            });

            this.salvar(entidades);
        }
    }

    @Override
    public List<Produtor> buscarProdutoresAutoComplete(final String value) {
        return this.getDAO().findTop10ByGrpContaProdutorInAndStatusCadastroSapAndNomeIgnoreCaseContainingOrCodigoIgnoreCaseContainingOrCnpjIgnoreCaseContainingOrCpfIgnoreCaseContainingOrderByCodigoAsc(SAPProdutorRural.CODIGOS_PRODUTORES, StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO, value, value, value, value);
    }

    @Override
    public List<Produtor> buscarArmazemTerceirosAutoComplete(final String value) {
        return this.getDAO().findTop10ByGrpContaProdutorInAndStatusCadastroSapAndNomeIgnoreCaseContainingOrCodigoIgnoreCaseContainingOrCnpjIgnoreCaseContainingOrCpfIgnoreCaseContainingOrderByCodigoAsc(Arrays.asList(SAPProdutorRural.CODIGO_ARMAZEM_TERCEIRO), StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO, value, value, value, value);
    }

    @Override
    public List<FornecedorResponse> listarParaArmazemDeTerceiros(final List<FornecedorRequest> requests) {

        if (ColecaoUtil.isNotEmpty(requests)) {

            final List<String> cpfs = requests.stream().filter(i -> StringUtil.isNotNullEmpty(i.getDocumento()) && i.getDocumento().length() == 11).map(FornecedorRequest::getDocumento).collect(Collectors.toList());

            final List<String> cnpjs = requests.stream().filter(i -> StringUtil.isNotNullEmpty(i.getDocumento()) && i.getDocumento().length() == 14).map(FornecedorRequest::getDocumento).collect(Collectors.toList());

            final List<Produtor> produtores = this.getDAO().findByCnpjInOrCpfIn(cnpjs, cpfs);

            if (ColecaoUtil.isNotEmpty(produtores)) {

                return produtores.stream().map(Produtor::toResponse).collect(Collectors.toList());
            }
        }

        return null;
    }

    @Override
    public Produtor obterPorCodigo(final String codigo) {
        return this.getDAO().findByCodigo(codigo);
    }

    @Override
    public String gerarSenha(Produtor produtor) throws Exception {

        String novaSenha = MD5Util.geraNovaSenha();

        dao.updateSenha(MD5Util.cript(novaSenha), produtor.getId());

        return novaSenha;
    }

    @Override
    public Produtor autenticarFornecedor(final String login, final String senha) {

        try {


            final String senhaEcriptada = MD5Util.cript(senha);

            Produtor produtor = dao.findByCpfOrCnpjAndSenha(login, login, senhaEcriptada);

            return produtor;

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    @Override
    public ProdutorDAO getDAO() {
        return this.dao;
    }

    @Override
    public Class<ProdutorDTO> getDtoListagem() {
        return ProdutorDTO.class;
    }
}
