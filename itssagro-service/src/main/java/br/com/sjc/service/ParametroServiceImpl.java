package br.com.sjc.service;

import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.ParametroService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.dto.ParametroDTO;
import br.com.sjc.modelo.enums.TipoParametroEnum;
import br.com.sjc.persistencia.dao.ParametroDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class ParametroServiceImpl extends ServicoGenerico<Long, Parametro> implements ParametroService {

    @Autowired
    private ParametroDAO dao;

    @Override
    public Specification<Parametro> obterEspecification(final Paginacao<Parametro> paginacao) {

        final Parametro entidade = paginacao.getEntidade();

        final Specification<Parametro> specification = new Specification<Parametro>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<Parametro> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Override
    public Parametro obterPorTipoParametro(final TipoParametroEnum tipoParametroEnum) {

        return this.dao.findByTipoParametro(tipoParametroEnum);
    }

    @Override
    public List<Parametro> obterPorTipoParametro(final List<TipoParametroEnum> tipoParametros) {

        return this.getDAO().findByTipoParametroIn(tipoParametros);
    }

    @Override
    public ParametroDAO getDAO() {
        return this.dao;
    }

    @Override
    public Class<? extends DataDTO> getDtoListagem() {
        return ParametroDTO.class;
    }
}
