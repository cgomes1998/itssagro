package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.rest.paginacao.Pagina;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.LacreService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.*;
import br.com.sjc.modelo.dto.LacreCountDTO;
import br.com.sjc.modelo.dto.LacreDTO;
import br.com.sjc.modelo.dto.RelatorioLacreDTO;
import br.com.sjc.modelo.dto.RelatorioLacreFiltroDTO;
import br.com.sjc.modelo.enums.StatusLacreEnum;
import br.com.sjc.modelo.enums.StatusLacreRomaneioEnum;
import br.com.sjc.modelo.view.VwLacreDetalhe;
import br.com.sjc.persistencia.dao.LacreDAO;
import br.com.sjc.persistencia.dao.fachada.LacreCustomRepository;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class LacreServiceImpl extends ServicoGenerico<Long, Lacre> implements LacreService {

    @Autowired
    private LacreDAO lacreDAO;

    @Autowired
    private LacreCustomRepository lacreCustomRepository;

    @Override
    public DAO<Long, Lacre> getDAO() {

        return this.lacreDAO;
    }

    @Override
    public Class<LacreDTO> getDtoListagem() {

        return LacreDTO.class;
    }

    @Override
    public void inutilizarLacres(final List<Lacre> lacres) {

        Usuario usuario = lacres.stream().map(Lacre::getUsuarioQueInutilizou).findFirst().get();
        Motivo motivoInutilizacao = lacres.stream().map(Lacre::getMotivoInutilizacao).findFirst().get();
        List<Long> lacresId = lacres.stream().map(Lacre::getId).collect(Collectors.toList());
        lacreDAO.alterarStatusDoLacres(lacresId, StatusLacreEnum.INUTILIZADO, DateUtil.hoje(), usuario.getId(), motivoInutilizacao.getId());
        lacreDAO.alterarStatusDoLacreRomaneios(StatusLacreRomaneioEnum.INUTILIZADO, lacresId);
    }

    @Override
    public void alterarStatus(final Lacre lacre) {

        lacreDAO.alterarStatusDoLacre(lacre.getStatusLacre(), lacre.getId());
    }

    @Override
    public void alterarStatusDosLacres(final List<Lacre> lacres) {

        lacres.stream().forEach(lacre -> lacreDAO.alterarStatusLacreEObservacaoStatusLacreAlteracao(lacre.getStatusLacre(), lacre.getObservacaoStatusLacreAlterar(), lacre.getId()));
    }

    @Override
    public List<Lacre> buscarLacresAutoComplete(String value) {

        return this.lacreDAO.findTop10ByCodigoIgnoreCaseContainingAndStatusLacreOrderByCodigoAsc(value, StatusLacreEnum.DISPONIVEL);
    }

    @Override
    public Pagina<Lacre> listarLacres(Paginacao<Lacre> paginacao) {

        Page<Lacre> page = this.obterPaginacao(paginacao);
        Pagina<Lacre> pagina = new Pagina<>();
        pagina.setContent(page.getContent());
        pagina.setTotalElements(page.getTotalElements());
        return pagina;
    }

    public Page<Lacre> obterPaginacao(final Paginacao<Lacre> paginacao) {

        Specification<Lacre> specification = this.obterEspecificacaoLacre(paginacao);
        if (specification != null) {
            return this.lacreDAO.findAll(specification, paginacao);
        }
        return this.lacreDAO.findAll(paginacao);
    }

    public Specification<Lacre> obterEspecificacaoLacre(Paginacao<Lacre> paginacao) {

        return (root, criteriaQuery, criteriaBuilder) -> {
            Lacre lacre = paginacao.getEntidade();
            List<Predicate> predicates = new ArrayList<>();
            if (Objects.nonNull(lacre.getLote())) {
                if (Objects.nonNull(lacre.getLote().getCodigo())) {
                    predicates.add(criteriaBuilder.like(root.get(Lacre_.codigo), MatchMode.ANYWHERE.toMatchString(lacre.getLote().getCodigo())));
                }
                if (ObjetoUtil.isNotNull(lacre.getLote().getDataCadastroDe())) {
                    predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(Lacre_.dataCadastro), DateUtil.obterDataHoraZerada(lacre.getLote().getDataCadastroDe())));
                }
                if (ObjetoUtil.isNotNull(lacre.getLote().getDataCadastroAte())) {
                    predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(Lacre_.dataCadastro), DateUtil.converterDateFimDia(lacre.getLote().getDataCadastroAte())));
                }
                if (ObjetoUtil.isNotNull(lacre.getLote().getId())) {
                    predicates.add(criteriaBuilder.equal(root.join(Lacre_.lote).get(Lote_.id), lacre.getLote().getId()));
                }
            }
            if (Objects.nonNull(lacre.getStatusLacre())) {
                predicates.add(criteriaBuilder.equal(root.get(Lacre_.statusLacre), lacre.getStatusLacre()));
            }
            criteriaQuery.distinct(true);
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    @Override
    public List<Lacre> listarLacres(Long loteId) {

        return this.lacreDAO.findAll((root, query, builder) -> builder.and(builder.equal(root.get("lote"), loteId)));
    }

    @Override
    public void alterarTodosStatusLacre(Lote lote) {

        List<Lacre> lacres = this.listarLacres(lote.getId());
        lacres.forEach(lacre -> lacre.setStatusLacre(lote.getStatusLacre()));
        this.salvar(lacres);
    }

    @Override
    public List<VwLacreDetalhe> obterLacreVinculadoARomaneio(final List<Lacre> lacres) {
        List<VwLacreDetalhe> vwLacreDetalheConsulta = lacreCustomRepository.obterLacreVinculadoARomaneio(lacres);
    
        List<VwLacreDetalhe> vwLacreDetalhe = new ArrayList<>();
    
        if (ColecaoUtil.isNotEmpty(vwLacreDetalheConsulta)) {
            List<VwLacreDetalhe> vwLacreDetalhesComRomaneio = removerLacreEmRomaneioAnterior(vwLacreDetalheConsulta.stream().filter(vwLacreDetalheTmp -> ObjetoUtil.isNotNull(vwLacreDetalheTmp.getNumeroRomaneio())).sorted(Comparator.comparingLong(VwLacreDetalhe::getNumeroRomaneio).reversed()).collect(Collectors.toList()));
    
            List<VwLacreDetalhe> vwLacreDetalheSemRomaneio = vwLacreDetalheConsulta.stream().filter(vwLacreDetalheTmp -> ObjetoUtil.isNull(vwLacreDetalheTmp.getNumeroRomaneio())).collect(Collectors.toList());
    
            vwLacreDetalhe.addAll(vwLacreDetalheSemRomaneio);
            
            if (ColecaoUtil.isNotEmpty(vwLacreDetalhesComRomaneio)) {
                limparDadosDeRomaneioSeStatusDoLacreEstiverDiferenteDeVinculadoEUtilizado(vwLacreDetalhesComRomaneio);
    
                vwLacreDetalhe.addAll(vwLacreDetalhesComRomaneio);
                
            }
    
            vwLacreDetalhe = vwLacreDetalhe.stream().sorted(Comparator.comparing(VwLacreDetalhe::getCodigo)).collect(Collectors.toList());
        }
        
        return vwLacreDetalhe;
    }
    
    private void limparDadosDeRomaneioSeStatusDoLacreEstiverDiferenteDeVinculadoEUtilizado(List<VwLacreDetalhe> vwLacreDetalhesComRomaneio) {
        
        vwLacreDetalhesComRomaneio.stream().forEach(vwLacreDetalheTmp -> {
            
            if (!Arrays.asList(StatusLacreEnum.VINCULADO, StatusLacreEnum.UTILIZADO).stream().anyMatch(statusLacreEnum -> vwLacreDetalheTmp.getStatusLacre().equals(statusLacreEnum))) {

                vwLacreDetalheTmp.setUsuarioQueUtilizou(null);
                vwLacreDetalheTmp.setDataUtilizacao(null);
                vwLacreDetalheTmp.setNumeroRomaneio(null);
                vwLacreDetalheTmp.setStatusRomaneio(null);
                vwLacreDetalheTmp.setStatusIntegracaoSAP(null);
                vwLacreDetalheTmp.setNfe(null);
            }
        });
    }
    
    private List<VwLacreDetalhe> removerLacreEmRomaneioAnterior(List<VwLacreDetalhe> vwLacreDetalhesComRomaneio) {
        
        List<VwLacreDetalhe> vwLacreDetalhesUnicos = new ArrayList<>();
    
        if (ColecaoUtil.isNotEmpty(vwLacreDetalhesComRomaneio)) {
    
            vwLacreDetalhesComRomaneio.stream().forEach(vwLacreDetalhe -> {
        
                if (!vwLacreDetalhesUnicos.stream().anyMatch(vwLacreDetalhe1 -> vwLacreDetalhe1.getCodigo().equals(vwLacreDetalhe.getCodigo()))) {
            
                    vwLacreDetalhesUnicos.add(vwLacreDetalhe);
                }
            });
        }
     
        return vwLacreDetalhesUnicos;
    }

    @Override
    public List<LacreCountDTO> obterTotalDeLacresPorStatus(StatusLacreEnum statusLacre) {

        return lacreCustomRepository.obterTotalDeLacresPorStatus(statusLacre);
    }

    @Override
    public List<RelatorioLacreDTO> obterRelatorioDeLacresVinculadosARomaneio(RelatorioLacreFiltroDTO relatorioLacreFiltro) {

        List<RelatorioLacreDTO> lacres = lacreCustomRepository.obterRelatorioDeLacresSemVinculoARomaneio(relatorioLacreFiltro);
        relatorioLacreFiltro.setLacres(lacres);
        List<RelatorioLacreDTO> lacresEmRomaneios = lacreCustomRepository.obterRelatorioDeLacresVinculadosARomaneio(relatorioLacreFiltro);
        List<RelatorioLacreDTO> lacresEmRomaneiosUnicos = new ArrayList<>();
        if (ColecaoUtil.isNotEmpty(lacres)) {
            lacres.removeIf(relatorioLacre -> lacresEmRomaneios.stream().anyMatch(relatorioLacreEmRomaneio -> relatorioLacreEmRomaneio.getLacre().getCodigo().equals(relatorioLacre.getLacre().getCodigo())));
        } else {
            lacres = new ArrayList<>();
        }
        if (ColecaoUtil.isNotEmpty(lacresEmRomaneios)) {
            lacresEmRomaneios.stream().forEach(relatorioLacre -> {
                boolean lacreJaAdicionado = lacresEmRomaneiosUnicos.stream().anyMatch(relatorioLacreUnico -> relatorioLacreUnico.getLacre().getCodigo().equalsIgnoreCase(relatorioLacre.getLacre().getCodigo()));
                if (!lacreJaAdicionado) {
                    lacresEmRomaneiosUnicos.add(relatorioLacre);
                }
            });
        }
        lacres.addAll(lacresEmRomaneiosUnicos);
        return lacres;
    }

    @Override
    public Boolean getRegistrarLogAtividade() {

        return Boolean.TRUE;
    }

}
