package br.com.sjc.service;

import br.com.sjc.fachada.service.ClassificacaoService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.Classificacao;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.dto.ItensCalculoIndiceDTO;
import br.com.sjc.modelo.dto.LaudoClassificacao;
import br.com.sjc.modelo.dto.RelatorioClassificacaoDTO;
import br.com.sjc.modelo.sap.ItemNFPedido;
import br.com.sjc.modelo.sap.ItensClassificacao;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.TabelaClassificacao;
import br.com.sjc.persistencia.dao.ClassificacaoDAO;
import br.com.sjc.persistencia.dao.RomaneioDAO;
import br.com.sjc.persistencia.dao.TabelaClassificacaoDAO;
import br.com.sjc.persistencia.dao.fachada.TabelaClassificacaoCustomRepository;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.RelatorioUtil;
import br.com.sjc.util.StringUtil;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class ClassificacaoServiceImpl extends ServicoGenerico<Long, Classificacao> implements ClassificacaoService {

    private static final String REL_CLASSIFICACAO_JASPER = "jasper/rel_classificacao.jasper";

    private static final String LOGO_PNG = "img/logo.png";

    @Autowired
    private ClassificacaoDAO dao;

    @Autowired
    private RomaneioDAO romaneioDAO;

    @Autowired
    private TabelaClassificacaoDAO tabelaClassificacaoDAO;

    @Autowired
    private TabelaClassificacaoCustomRepository tabelaClassificacaoCustomRepository;

    @Override
    public Classificacao salvarClassificacaoDeArmazemTerceiros(final Classificacao entidade) {

        if (ColecaoUtil.isNotEmpty(entidade.getItens())) {

            final List<String> codigosOrigem = entidade.getItens().stream().filter(i -> StringUtil.isNotNullEmpty(i.getCodigoArmazemTerceiros())).map(ItensClassificacao::getCodigoArmazemTerceiros).collect(Collectors.toList());

            final List<TabelaClassificacao> tabelaClassificacaos = this.tabelaClassificacaoDAO.findByCodigoArmazemTerceirosIn(codigosOrigem);

            entidade.getItens().forEach(item -> {

                final TabelaClassificacao itemClassificacao = tabelaClassificacaos.stream().filter(t -> t.getCodigoArmazemTerceiros().equals(item.getCodigoArmazemTerceiros())).findFirst().orElse(null);

                if (ObjetoUtil.isNotNull(itemClassificacao)) {

                    item.setCodigoTabela(itemClassificacao.getCodigo());

                    item.setDescricaoItem(itemClassificacao.getDescricaoItemClassificacao());

                    item.setItemClassificao(itemClassificacao.getItemClassificacao());
                }
            });
        }

        return this.salvar(entidade);
    }

    @Override
    public byte[] imprimirPdf(Romaneio romaneioSaved, final Long idRomaneio) throws JRException {

        final InputStream inputStream = RomaneioServiceImpl.class.getClassLoader().getResourceAsStream(REL_CLASSIFICACAO_JASPER);

        if (ObjetoUtil.isNull(romaneioSaved)) {

            romaneioSaved = this.romaneioDAO.findById(idRomaneio).get();
        }

        final LaudoClassificacao entidadeRelatorio = this.obterFieldsClassificacao(romaneioSaved);

        final HashMap<String, Object> parametros = this.montarParametrosRelatorio();

        final List<LaudoClassificacao> lista = new ArrayList<>();

        lista.add(entidadeRelatorio);

        lista.add(entidadeRelatorio.getClone());

        return RelatorioUtil.gerarPdf(lista, inputStream, parametros);
    }

    private LaudoClassificacao obterFieldsClassificacao(Romaneio romaneio) {

        LaudoClassificacao entidadeRelatorio = new LaudoClassificacao();

        entidadeRelatorio.setNumero(romaneio.getNumeroRomaneio());

        entidadeRelatorio.setNumeroNfe(romaneio.getNumeroNfe());

        entidadeRelatorio.setSerieNfe(romaneio.getSerieNfe());

        entidadeRelatorio.setDataCriacao(romaneio.getDataCadastro());

        if (romaneio.getOperacao().isPossuiDivisaoCarga()) {

            ItemNFPedido item = romaneio.getItens().stream().findFirst().get();

            entidadeRelatorio.setMaterial(ObjetoUtil.isNotNull(item.getMaterial()) ? item.getMaterial().toString() : StringUtil.empty());

            entidadeRelatorio.setVeiculo(ObjetoUtil.isNotNull(item.getPlacaCavalo()) ? StringUtil.toPlaca(item.getPlacaCavalo().getPlaca1()) : StringUtil.empty());

        } else {

            entidadeRelatorio.setMaterial(ObjetoUtil.isNotNull(romaneio.getMaterial()) ? romaneio.getMaterial().toString() : StringUtil.empty());

            entidadeRelatorio.setVeiculo(ObjetoUtil.isNotNull(romaneio.getPlacaCavalo()) ? StringUtil.toPlaca(romaneio.getPlacaCavalo().getPlaca1()) : StringUtil.empty());
        }

        this.obterItensClassificacao(romaneio, entidadeRelatorio);

        return entidadeRelatorio;
    }

    private void obterItensClassificacao(final Romaneio romaneio, final LaudoClassificacao entidadeRelatorio) {

        if (ObjetoUtil.isNotNull(romaneio.getClassificacao()) && ColecaoUtil.isNotEmpty(romaneio.getClassificacao().getItens())) {

            final List<RelatorioClassificacaoDTO> classificacoes = new ArrayList<RelatorioClassificacaoDTO>();

            final AtomicInteger index = new AtomicInteger(0);

            AtomicReference<BigDecimal> pesoLiquido = new AtomicReference<>(BigDecimal.ZERO);

            AtomicReference<BigDecimal> pesoDescontado = new AtomicReference<>(BigDecimal.ZERO);

            romaneio.getClassificacao().getItens().forEach(item -> {

                final RelatorioClassificacaoDTO classDto = new RelatorioClassificacaoDTO();

                BigDecimal desconto = BigDecimal.valueOf(0);

                if ((ObjetoUtil.isNotNull(romaneio.getPesagem()))
                        && (ObjetoUtil.isNotNull(romaneio.getPesagem().getPesoInicial())
                        && ObjetoUtil.isNotNull(romaneio.getPesagem().getPesoFinal()))) {

                    pesoLiquido.set(romaneio.getPesagem().getPesoInicial().subtract(romaneio.getPesagem().getPesoFinal()));

                    if (index.get() == 0) {

                        desconto = pesoLiquido.get().multiply(item.getPercentualDescontoValido().divide(BigDecimal.valueOf(100))).setScale(0, RoundingMode.HALF_EVEN);

                        pesoDescontado.set(pesoLiquido.get().subtract(desconto));

                    } else {

                        desconto = pesoDescontado.get().multiply(item.getPercentualDescontoValido().divide(BigDecimal.valueOf(100))).setScale(0, RoundingMode.HALF_EVEN);

                        pesoDescontado.set(pesoDescontado.get().subtract(desconto));
                    }
                }

                classDto.setDesconto(StringUtil.toMilhar(desconto, 0));

                classDto.setDescricao(item.getDescricaoItem());

                classDto.setIndice(ObjetoUtil.isNotNull(item.getIndice()) ? item.getIndice().toString() : BigDecimal.ZERO.toString());

                classDto.setPercentualDesconto(StringUtil.toMilhar(item.getPercentualDescontoValido(), 2));

                classificacoes.add(classDto);

                index.incrementAndGet();
            });

            entidadeRelatorio.setObservacao(romaneio.getClassificacao().getObservacao());

            entidadeRelatorio.setItensClassificacao(classificacoes);
        }
    }

    private HashMap<String, Object> montarParametrosRelatorio() throws JRException {

        final HashMap<String, Object> parametros = new HashMap<String, Object>();

        final InputStream inputStreamLogo = RomaneioServiceImpl.class.getClassLoader().getResourceAsStream(LOGO_PNG);

        parametros.put("LOGO", inputStreamLogo);

        return parametros;
    }

    @Override
    public void preSalvar(Classificacao entidade) {

        entidade.getItens().stream().forEach(i -> {

            i.setClassificacao(entidade);
        });
    }

    @Override
    public List<TabelaClassificacao> listarItensClassificacaoPorMaterial(final String codigoMaterial) {
        List<TabelaClassificacao> tabelaClassificacaoList = this.tabelaClassificacaoCustomRepository.obterPorMaterial(codigoMaterial);
        if (ColecaoUtil.isNotEmpty(tabelaClassificacaoList)) {
            List<Long> ids = tabelaClassificacaoList
                    .stream()
                    .map(TabelaClassificacao::getId)
                    .collect(Collectors.toList());
            List<ItensCalculoIndiceDTO> itensCalculoIndiceDTOList = this.tabelaClassificacaoCustomRepository.obterItensParaCalculoDeIndicesPorTabela(ids);
            if (ColecaoUtil.isNotEmpty(itensCalculoIndiceDTOList)) {
                tabelaClassificacaoList
                        .stream()
                        .filter(tabelaClassificacao -> itensCalculoIndiceDTOList
                                .stream()
                                .filter(item -> item.getIdTabelaClassificacao().longValue() == tabelaClassificacao.getId())
                                .count() > 0)
                        .forEach(tabelaClassificacao -> {
                            tabelaClassificacao.setItensParaCalculoIndice(itensCalculoIndiceDTOList
                                    .stream()
                                    .filter(item -> item.getIdTabelaClassificacao().longValue() == tabelaClassificacao.getId())
                                    .map(ItensCalculoIndiceDTO::getDescricao)
                                    .collect(Collectors.toSet()));
                        });
            }
        }
        return tabelaClassificacaoList;
    }

    @Override
    public boolean indiceInformadoExisteNaTabelaDeClassificacao(ItensClassificacao itensClassificacao) {
        return tabelaClassificacaoCustomRepository.indiceInformadoExisteNaTabelaDeClassificacao(itensClassificacao);
    }

    @Override
    public ClassificacaoDAO getDAO() {
        return this.dao;
    }

    @Override
    public Class<? extends DataDTO> getDtoListagem() {
        return null;
    }

}
