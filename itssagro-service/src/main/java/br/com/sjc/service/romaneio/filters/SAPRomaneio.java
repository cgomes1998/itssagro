package br.com.sjc.service.romaneio.filters;

public class SAPRomaneio {

    public enum SIMPLES_PESAGEM {

        C_DATA_PSB,
        C_DATA_PST,
        C_HORA_PSB,
        C_HORA_PST,
        C_PESO_PSB,
        C_PESO_TAR,
        C_PESOLS,
        C_QUANT,
        C_USUA_PSB,
        C_USUA_PST;

    }

    public enum BRIDGES_CLASSIFICACAO {

        C_INDICC,
        C_ITENSC,
        C_OCLASS,
        C_PERCC,
        C_PESOC,
        C_TRANSG,
        C_USER_CLAS;

    }

    public enum BRIDGES_PESAGEM {

        C_LPESA,
        C_PBRUTO,
        C_PSECO,
        C_PTARA,
        C_PUMIDO,
        C_TDESC,
        C_USER_PB,
        C_USER_PT;

    }

    public enum BRIDGES {

        C_ARMT,
        C_BRANCH,
        C_CENTRO,
        C_CENTRO1,
        C_COND,
        C_DAT1,
        C_DTFIXAVC,
        C_DAT2,
        C_DAT3,
        C_DATA,
        C_DATUM,
        C_DELIVER,
        C_DEPOS,
        C_DIVPESO,
        C_DNFE,
        C_DOCDAT,
        C_DTATUAL,
        C_DVNFE,
        C_FAZEN,
        C_FRE1,
        C_FRE2,
        C_GRUP,
        C_HNFE,
        C_INCO1,
        C_INSCE,
        C_ITEM,
        C_ITEMP,
        C_IVA,
        C_KUNNR,
        C_LGORT,
        C_LIFNR,
        C_LOCALP,
        C_LOGNFE,
        C_MATERIAL,
        C_MATNR,
        C_MENGE,
        C_MESSAGE,
        C_MESSAGE1,
        C_MOTORIST,
        C_NANFE,
        C_NOME,
        C_NTRANSP,
        C_REMESSA,
        C_NUMNFE,
        C_OPERA,
        C_ORGC,
        C_OV,
        C_PARID,
        C_PEDIDO,
        C_PBRUTORT,
        C_PUMIDORT,
        C_PL_CAR1,
        C_PL_CAR2,
        C_PL_CAV,
        C_PLCAV_UF,
        C_PLACA,
        C_PLACA1,
        C_PLACA2,
        C_PLACAC,
        C_PNFE,
        C_PRECO,
        C_PRODUT,
        C_QUANT,
        C_QUEBRA,
        C_REFITM,
        C_REGIO,
        C_ROMAN,
        C_SAFRA,
        C_SAFRA_PLAN,
        C_SALDO,
        C_SERIENFE,
        C_SHPMRK,
        C_STATUS,
        C_STRANSP,
        C_TEXT3,
        C_TEXTOB,
        C_TIPCO,
        C_TIPOCAP,
        C_TIPOFRE,
        C_TRANSP,
        C_TRANSP_C,
        C_TRANSP_N,
        C_TKNUM,
        C_UF,
        C_UM,
        C_UNIDP,
        C_USER_ROMA,
        C_VALORP,
        C_VLR_FRE,
        C_VLRQUEBRA,
        C_VNFE,
        C_VTNFE,
        C_WERKS,
        C_TOLERAN,
        C_RECEBEDO,
        C_CRED_PEN,
        C_DJUR,
        C_STCONTRA,
        C_COMPLET,
        C_PLIQUIDO,
        C_ARMZTERC,

        C_VALOCTE,

        C_UFEMIT,

        C_UFTOMA,

        C_POCTE,

        C_ITEMCTE,

        C_IVACTE,

        C_PARCCTE,

        C_CHAVECTE,

        C_NUNCTE,

        C_SERIECTE,

        C_LOGCTE,

        C_NUMALE,

        C_DATACTE,

        C_DIGCTE,

        C_EBELN,

        C_BSMNG,

        C_CHNFE,

        C_NAME1,

        C_PESOFIM;
    }
}
