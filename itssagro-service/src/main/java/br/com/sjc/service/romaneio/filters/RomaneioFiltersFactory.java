package br.com.sjc.service.romaneio.filters;

import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.request.EventInRequest;

public class RomaneioFiltersFactory {

    public static EventInRequest create(
            final DadosSincronizacao dadosSincronizacao,
            final String centro,
            final String rfcProgramID,
            final Romaneio romaneio,
            final String requestKey
    ) {

        switch (romaneio.getOperacao().getEntidade()) {

            case CONTRATO_PEDIDO_COMPRAS: {

                EventInRequest request = (EventInRequest) new ContratoPedidoComprasFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case NF_TRANSPORTE: {

                EventInRequest request = (EventInRequest) new NotaFiscalTransporteFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case PRODUTOR_PF_DIRETO_UPG: {

                EventInRequest request = (EventInRequest) new ProdutorPfDiretoUpgFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case PRODUTOR_PJ_DIRETO_UPG: {

                EventInRequest request = (EventInRequest) new ProdutorPjDiretoUpgFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case PRODUTOR_PF_DIRETO_GO206: {

                EventInRequest request = (EventInRequest) new ProdutorPfDiretoGo206Filters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case PRODUTOR_PJ_DIRETO_GO206: {

                EventInRequest request = (EventInRequest) new ProdutorPjDiretoGo206Filters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case PRODUTOR_PF_DIRETO_AGROVALE: {

                EventInRequest request = (EventInRequest) new ProdutorPfDiretoAgrovaleFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case PRODUTOR_PF_DIRETO_CARAMURU: {

                EventInRequest request = (EventInRequest) new ProdutorPfDiretoCaramuruFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case PRODUTOR_PF_DIRETO_EM_ARMAZEM_TERC: {

                EventInRequest request = (EventInRequest) new ProdutorPfDiretoEmArmazemTercFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case PRODUTOR_PJ_DIRETO_AGROVALE: {

                EventInRequest request = (EventInRequest) new ProdutorPjDiretoAgrovaleFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case PRODUTOR_PJ_DIRETO_CARAMURU: {

                EventInRequest request = (EventInRequest) new ProdutorPjDiretoCaramuruFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case AGROVALE_PARA_DEP_FECHADO_GO206: {

                EventInRequest request = (EventInRequest) new AgrovaleParaDepFechadoGo206Filters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }
    
            case RT_TERCEIRO_TRANSF_TERCEIROS: {
        
                EventInRequest request = (EventInRequest) new TerceiroTransfTerceirosFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);
        
                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());
        
                return request;
            }

            case TRANS_PROP_MERC_DEPOSITADA: {

                EventInRequest request = (EventInRequest) new TransDePropDeMercDepositadaFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case REMESSA_AGROVALE_UPG: {

                EventInRequest request = (EventInRequest) new RemessaAgrovaleParaUpgFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case RETORNO_CARAMURU_UPG: {

                EventInRequest request = (EventInRequest) new RetornoCaramuruParaUpgFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case RETORNO_ARMAZEM_TERCEIROS_PARA_UPG: {

                EventInRequest request = (EventInRequest) new RetornoArmazemTerceirosParaUpgFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }
    
            case RT_BAIXA_QUEBRA_ARMAZEM_TERCEIROS: {
        
                EventInRequest request = (EventInRequest) new BaixaQuebraArmazemTerceirosFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);
        
                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());
        
                return request;
            }

            case REMESSA_GO206_PARA_UPG: {

                EventInRequest request = (EventInRequest) new RemessaGO206ParaUpgFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case SIMPLES_PESAGEM: {

                EventInRequest request = (EventInRequest) new SimplesPesagemFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case NF_COMP_PRODUTOR_PJ_DIRETO_UPG: {

                EventInRequest request = (EventInRequest) new NfComplementoProdutorPjDiretoUpgFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case NF_COMP_PRODUTOR_PJ_DIRETO_GO206: {

                EventInRequest request = (EventInRequest) new NfComplementoProdutorPjDiretoGo206Filters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case NF_COMP_PRODUTOR_PJ_DIRETO_AGROVALE: {

                EventInRequest request = (EventInRequest) new NfComplementoProdutorPjDiretoAgrovaleFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case RT_COMP_AGROVALE_PARA_DEP_FECHADO_GO206: {

                EventInRequest request = (EventInRequest) new RtCompAgrovaleParaDepFechadoGo206Filters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case RT_COMP_AGROVALE_PARA_UPG: {

                EventInRequest request = (EventInRequest) new RtCompAgrovaleParaUpgFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case RT_COMP_CARAMURU_PARA_UPG: {

                EventInRequest request = (EventInRequest) new RtCompCaramuruParaUpgFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case EM_POS_DEVOLUCAO_PF: {

                EventInRequest request = (EventInRequest) new EmPosDevolucaoPfFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case EM_POS_DEVOLUCAO_PJ: {

                EventInRequest request = (EventInRequest) new EmPosDevolucaoPjFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case EM_POS_DEVOLUCAO_PF_DIRETO_NA_GO206: {

                EventInRequest request = (EventInRequest) new EmPosDevolucaoPfDiretoNaGO206Filters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case EM_POS_DEVOLUCAO_PF_DIRETO_NA_AGROVALE: {

                EventInRequest request = (EventInRequest) new EmPosDevolucaoPfDiretoNaAgrovaleFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case EM_POS_DEVOLUCAO_PF_DIRETO_NA_CARAMURU: {

                EventInRequest request = (EventInRequest) new EmPosDevolucaoPfDiretoNaCaramuruFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case SM_OPERACAO_SAIDA: {

                EventInRequest request = (EventInRequest) new OperacaoSaidaFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case ENTRADA_FISICA_TRANSFERENCIA: {

                EventInRequest request = (EventInRequest) new EntradaFisicaTransferenciaFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case OPERACAO_TRANSFERENCIA: {

                EventInRequest request = (EventInRequest) new OperacaoTransferenciaFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            case TRANSFERENCIA_GERAIS: {

                EventInRequest request = (EventInRequest) new TransferenciagGeraisFilters().instace(romaneio, centro, requestKey, dadosSincronizacao, rfcProgramID);

                request.setNumeroRomaneio(romaneio.getNumeroRomaneio());

                return request;
            }

            default:

                return null;
        }
    }

}
