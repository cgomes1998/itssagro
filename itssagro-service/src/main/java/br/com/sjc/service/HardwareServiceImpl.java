package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.HardwareService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.Hardware;
import br.com.sjc.modelo.dto.HardwareDTO;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.persistencia.dao.HardwareDAO;
import br.com.sjc.util.Util;
import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by julio.bueno on 02/07/2019.
 */
@Service
public class HardwareServiceImpl extends ServicoGenerico<Long, Hardware> implements HardwareService {

    @Autowired
    private HardwareDAO hardwareDAO;

    @Override
    public DAO<Long, Hardware> getDAO() {

        return hardwareDAO;
    }

    @Override
    public Class<HardwareDTO> getDtoListagem() {

        return HardwareDTO.class;
    }

    @Override
    public Specification<Hardware> obterEspecification(Paginacao<Hardware> paginacao) {

        return (Specification<Hardware>) (root, query, criteriaBuilder) -> {
            Hardware hardware = paginacao.getEntidade();

            List<Predicate> predicates = new ArrayList<>();

            if (!Objects.isNull(hardware.getCodigoFormatado())) {
                predicates.add(criteriaBuilder.like(root.get("codigoFormatado"), MatchMode.ANYWHERE.toMatchString(hardware.getCodigoFormatado())));
            }

            if (!StringUtils.isEmpty(hardware.getDescricao())) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("descricao")), MatchMode.ANYWHERE.toMatchString(hardware.getDescricao().toLowerCase())));
            }

            if (!Objects.isNull(hardware.getStatus())) {
                predicates.add(criteriaBuilder.equal(root.get("status"), hardware.getStatus()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }

    @Override
    public void preSalvar(Hardware entidade) {

        super.preSalvar(entidade);
        validarIdentificacaoDuplicada(entidade);
        gerarCodigo(entidade);
    }

    private void gerarCodigo(Hardware entidade) {

        if (entidade.isNew()) {
            Long ultimoCodigo = hardwareDAO.getMaxCodigo();
            Long proximoCodigo = Objects.isNull(ultimoCodigo) ? 1L : ultimoCodigo + 1;

            entidade.setCodigo(proximoCodigo);
            entidade.setCodigoFormatado(Util.formatarCodigo(Hardware.PREFIXO, proximoCodigo));
        }
    }

    private void validarIdentificacaoDuplicada(Hardware entidade) {

        if (!StringUtils.isEmpty(entidade.getIdentificacao())) {
            Long id = Objects.isNull(entidade.getId()) ? 0 : entidade.getId();
            if (hardwareDAO.existsOutroComMesmaIdentificacao(id, entidade.getIdentificacao())) {
                throw new ServicoException("Já existe um Hardware com esse Código de Identificação: " + entidade.getIdentificacao());
            }
        }
    }

    @Override
    public synchronized Hardware salvar(Hardware entidade) {

        return super.salvar(entidade);
    }

    @Override
    public List<Hardware> listarHardwaresAtivos() {

        return hardwareDAO.findByStatus(StatusEnum.ATIVO);
    }

    @Override
    public Boolean getRegistrarLogAtividade() {

        return Boolean.TRUE;
    }

}
