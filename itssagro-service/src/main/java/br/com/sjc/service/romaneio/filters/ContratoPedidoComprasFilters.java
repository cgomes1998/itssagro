package br.com.sjc.service.romaneio.filters;

import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.request.EventInRequest;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;

import java.util.LinkedList;
import java.util.List;

public class ContratoPedidoComprasFilters extends RomaneioFilters {

    @Override
    public JCoRequest instace(final Romaneio romaneio, final String centro, final String requestKey, final DadosSincronizacao dadosSincronizacao, final String rfcProgramID) {

        return EventInRequest.instance(dadosSincronizacao, centro, rfcProgramID, this.getFilters(romaneio, requestKey), false, null);
    }

    private List<EventInRequestItem> getFilters(final Romaneio romaneio, final String requestKey) {

        final List<EventInRequestItem> filters = new LinkedList<EventInRequestItem>();

        Integer seq = 0;

        seq = this.montarBridgeParaUsuarioCriadorRomaneio(romaneio, requestKey, filters, seq);
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_ROMAN.name(), String.valueOf(seq++), romaneio.getNumeroRomaneio()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_OPERA.name(), String.valueOf(seq++), romaneio.getOperacao().getOperacao()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_DATA.name(), String.valueOf(seq++), DateUtil.formatToSAP(DateUtil.hoje())));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_TIPCO.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getTipoContrato()) ? romaneio.getTipoContrato().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_LOCALP.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getLocalPesagem()) ? romaneio.getLocalPesagem().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_LIFNR.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getProdutor()) ? romaneio.getProdutor().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_INSCE.name(), String.valueOf(seq++), StringUtil.isNotNullEmpty(romaneio.getInscricaoEstadual()) ? romaneio.getInscricaoEstadual() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_FAZEN.name(), String.valueOf(seq++), StringUtil.isNotNullEmpty(romaneio.getFazenda()) ? romaneio.getFazenda() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_SAFRA.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getSafra()) ? romaneio.getSafra().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_SAFRA_PLAN.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getSafraPlantil()) ? romaneio.getSafraPlantil().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_MATNR.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getMaterial()) ? romaneio.getMaterial().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_MENGE.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getQuantidadeMaterial()) ? romaneio.getQuantidadeMaterial().toString() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_UNIDP.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getUnidadePrecoMaterial()) ? String.valueOf(romaneio.getUnidadePrecoMaterial().intValue()) : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_PRECO.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getValorMaterial()) ? romaneio.getValorMaterial().toString() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_WERKS.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getCentro()) ? romaneio.getCentro().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_LGORT.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getDeposito()) ? romaneio.getDeposito().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_COND.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getCondicaoPag()) ? romaneio.getCondicaoPag().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_ORGC.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getOrgCompra()) ? romaneio.getOrgCompra().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_GRUP.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getGpCompradores()) ? romaneio.getGpCompradores().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_DAT1.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getDtPagamento()) ? DateUtil.formatToSAP(romaneio.getDtPagamento()) : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_DTFIXAVC.name(), String.valueOf(seq++), romaneio.getDataFixaPagamento() ? RomaneioFilters.SAP_TRUE : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_DAT2.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getDtInicioEntrega()) ? DateUtil.formatToSAP(romaneio.getDtInicioEntrega()) : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_DAT3.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getDtFinalEntrega()) ? DateUtil.formatToSAP(romaneio.getDtFinalEntrega()) : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_FRE1.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getIncotermsUm()) ? romaneio.getIncotermsUm().name() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_FRE2.name(), String.valueOf(seq++), StringUtil.isNotNullEmpty(romaneio.getIncotermsDois()) ? romaneio.getIncotermsDois() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_VLR_FRE.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getPrecoFrete()) ? romaneio.getPrecoFrete().toString() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_IVA.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getIva()) ? romaneio.getIva().getCodigo() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_TOLERAN.name(), String.valueOf(seq++), romaneio.getTolerancia().toString()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_RECEBEDO.name(), String.valueOf(seq++), romaneio.getRecebedor()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_CRED_PEN.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getCessaoCredito()) ? romaneio.getCessaoCredito().getValor() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_DJUR.name(), String.valueOf(seq++), StringUtil.isNotNullEmpty(romaneio.getNumeroContratoDjur()) ? romaneio.getNumeroContratoDjur() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_STCONTRA.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getStatusContrato()) ? romaneio.getStatusContrato().getCodigoSap() + "" : StringUtil.empty()));

        List<String> listaObs = converterStringFormatoSAP(romaneio.getObservacao());

        int contadorItensObs = 1;

        int grupo = 1;

        for (String obs : listaObs) {
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_TEXTOB.name(), String.valueOf(seq++), String.valueOf(contadorItensObs++), String.valueOf(grupo), obs));
        }

        adicionarRequestRomaneioSemDivisao(romaneio, requestKey);

        return filters;
    }

}
