package br.com.sjc.service.romaneio.filters;

import br.com.sjc.fachada.servico.impl.ServiceInject;
import br.com.sjc.modelo.Classificacao;
import br.com.sjc.modelo.Pesagem;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.enums.OrigemClassificacaoEnum;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;
import br.com.sjc.util.BigDecimalUtil;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public abstract class RomaneioFilters {

    public static final int LIMITE_CHARACTER_SAP = 100;

    public static final String SAP_TRUE = "X";

    public static final String UM = "1";

    public static final String ZERO = "0";

    public static final String ERM = "ERM";

    public static final int DEZ = 10;

    private static final BigDecimal CEM = new BigDecimal(100);

    public static List<String> converterStringFormatoSAP(String str) {

        List<String> result = new ArrayList<String>();

        if (!StringUtil.isEmpty(str)) {

            str = str.replaceAll("\r", " ");
            str = str.replaceAll("\t", " ");
            str = str.replaceAll("\n", " ");

            if (str.length() > LIMITE_CHARACTER_SAP) {

                result.add(str.substring(0, LIMITE_CHARACTER_SAP));

                result.addAll(converterStringFormatoSAP(str.substring(LIMITE_CHARACTER_SAP, str.length())));

            } else {

                result.add(str);
            }
        }

        return result;
    }

    public abstract JCoRequest instace(Romaneio romaneio, String centro, String requestKey, DadosSincronizacao dadosSincronizacao, String rfcProgramID);

    public void adicionarRequestRomaneioSemDivisao(Romaneio romaneio, String requestKey) {

        romaneio.setRequests(new ArrayList<>());
        romaneio.getRequests().add(new RomaneioRequestKey(romaneio, requestKey));
    }

    public void adicionarRequestParaRomaneioComDivisao(Romaneio romaneio, int countRequest, String novaRequest) {

        if (countRequest == DEZ) {
            romaneio.setRequests(new ArrayList<>());
        }

        romaneio.getRequests().add(new RomaneioRequestKey(romaneio, novaRequest));
    }

    public String montarRequestKeyPorMap(String requestKey, String pattern, String[] requests, String request, int countRequest, Map<String, List<ItemNFPedido>> itensPorChaveMap) {

            String novaRequest = StringUtil.empty();

        if (itensPorChaveMap.size() == 1) {

            novaRequest = requestKey;

        } else {

            Integer requestKeyDivisao = Integer.parseInt(request + countRequest);
            novaRequest = StringUtil.completarZeroAEsquerda(9, requestKeyDivisao.toString()) + pattern + requests[1];
        }

        return novaRequest;
    }

    public int mapeamentoBridgeParaRateio(Romaneio romaneio, List<EventInRequestItem> filters, int seq, String novaRequest, int codItem, ItemNFPedido item, BigDecimal descontos) {

        BigDecimal pesoLiquidoUmido = BigDecimal.ZERO;

        if (ObjetoUtil.isNull(romaneio.getPesagem()) || ObjetoUtil.isNull(romaneio.getPesagem().getPesoInicial())) {
            romaneio.setPesagem(new Pesagem());
            romaneio.getPesagem().setPesoInicial(BigDecimal.ZERO);
        }

        if (ObjetoUtil.isNull(romaneio.getPesagem()) || ObjetoUtil.isNull(romaneio.getPesagem().getPesoFinal())) {
            romaneio.setPesagem(ObjetoUtil.isNull(romaneio.getPesagem()) ? new Pesagem() : romaneio.getPesagem());
            romaneio.getPesagem().setPesoFinal(BigDecimal.ZERO);
        }

        pesoLiquidoUmido = romaneio.getPesagem().getPesoInicial().subtract(romaneio.getPesagem().getPesoFinal()).setScale(0, RoundingMode.HALF_UP);

        final BigDecimal pesoLiquidoSeco = pesoLiquidoUmido.subtract(descontos).setScale(0, RoundingMode.HALF_UP);

        final BigDecimal complemento = inicializarBridgePesoLiquidoUmidoRateioERetornaSeHouverComplemento(romaneio, filters, seq, novaRequest, codItem, item, pesoLiquidoUmido);
    
        final BigDecimal quebra = inicializarBridgeDivisaoDePesoERetornaSeHouverQuebra(romaneio, filters, seq, novaRequest, codItem, item, pesoLiquidoSeco);

        addQuebraOuComplemento(romaneio.isPossuiDadosOrigem(), descontos, quebra, complemento, novaRequest, seq, codItem, filters);
    
        return seq;
    }
    
    private void addQuebraOuComplemento(boolean possuiDadosOrigem, BigDecimal desconto, BigDecimal quebra, BigDecimal complemento, String novaRequest, int seq, int codItem, List<EventInRequestItem> filters) {
        if (possuiDadosOrigem) {
            if (complemento.compareTo(BigDecimal.ZERO) == 0 && quebra.compareTo(BigDecimal.ZERO) == 1) {
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_QUEBRA.name(), String.valueOf(seq++), String.valueOf(codItem), quebra.toString()));
            } else if (desconto.compareTo(complemento) == 1 && quebra.compareTo(BigDecimal.ZERO) == 1) {
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_QUEBRA.name(), String.valueOf(seq++), String.valueOf(codItem), quebra.toString()));
            } else if(complemento.compareTo(BigDecimal.ZERO) == 1) {
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_COMPLET.name(), String.valueOf(seq++), String.valueOf(codItem), BigDecimalUtil.subtract(complemento, desconto).toString()));
            }
        } else {
            if ((desconto.compareTo(complemento) == 1 || complemento.compareTo(BigDecimal.ZERO) == 0) && quebra.compareTo(BigDecimal.ZERO) == 1) {
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_QUEBRA.name(), String.valueOf(seq++), String.valueOf(codItem), quebra.toString()));
            }
        }
    }

    private BigDecimal inicializarBridgePesoLiquidoUmidoRateioERetornaSeHouverComplemento(Romaneio romaneio, List<EventInRequestItem> filters, int seq, String novaRequest, int codItem, ItemNFPedido item, BigDecimal pesoLiquidoUmido) {

        if (romaneio.getOperacao().isPossuiRegraComplemento()) {
            if (ColecaoUtil.isNotEmpty(romaneio.getItens()) && romaneio.getItens().size() > 1) {
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PLIQUIDO.name(), String.valueOf(seq++), String.valueOf(codItem), item.getPesoLiquidoUmido().toString()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES_PESAGEM.C_PUMIDO.name(), String.valueOf(seq++), String.valueOf(codItem), item.getPesoLiquidoUmido().toString()));
            } else {
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PLIQUIDO.name(), String.valueOf(seq++), String.valueOf(codItem), pesoLiquidoUmido.toString()));
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES_PESAGEM.C_PUMIDO.name(), String.valueOf(seq++), String.valueOf(codItem), pesoLiquidoUmido.toString()));
            }
            if (romaneio.isPossuiDadosOrigem() && ColecaoUtil.isNotEmpty(romaneio.getItens())) {
                if (romaneio.getOperacao().isPossuiDadosNfe()) {
                    if (ColecaoUtil.isNotEmpty(romaneio.getItens()) && romaneio.getItens().size() > 1) {
                        if (ObjetoUtil.isNotNull(item.getPesoTotalNfe()) && item.getPesoTotalNfe().compareTo(item.getPesoLiquidoUmido()) == -1) {
                            return BigDecimalUtil.positivo(item.getPesoLiquidoUmido().subtract(item.getPesoTotalNfe()));
                        }
                    } else if (ColecaoUtil.isNotEmpty(romaneio.getItens())) {
                        if (ObjetoUtil.isNotNull(item.getPesoTotalNfe()) && item.getPesoTotalNfe().compareTo(pesoLiquidoUmido) == -1) {
                            return BigDecimalUtil.positivo(pesoLiquidoUmido.subtract(item.getPesoTotalNfe()));
                        }
                    }
                }
            }
        }

        return BigDecimal.ZERO;
    }

    private BigDecimal inicializarBridgeDivisaoDePesoERetornaSeHouverQuebra(Romaneio romaneio, List<EventInRequestItem> filters, int seq, String novaRequest, int codItem, ItemNFPedido item, BigDecimal pesoLiquidoSeco){
        BigDecimal quebra = BigDecimal.ZERO;

        if (!romaneio.getOperacao().isPossuiDadosNfe() && romaneio.getOperacao().isPossuiRateio() && romaneio.getItens().size() == 1 && romaneio.isPossuiDadosOrigem()) {

            quebra = romaneio.getPesoLiquidoSecoOrigem().subtract(pesoLiquidoSeco);
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_DIVPESO.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(romaneio.getPesoLiquidoSecoOrigem()) ? romaneio.getPesoLiquidoSecoOrigem().toString() : BigDecimal.ZERO.toString()));

        } else if (romaneio.getOperacao().isPossuiRateio() && romaneio.getItens().size() > 1) {

            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_DIVPESO.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(item.getDivPeso()) ? item.getDivPeso().toString() : BigDecimal.ZERO.toString()));

            if (romaneio.getOperacao().isPossuiDadosNfe() || romaneio.isPossuiDadosOrigem()) {
                quebra = item.getQuebra();
            }

        } else if (romaneio.getOperacao().isPossuiRateio() && romaneio.getItens().size() == 1) {

            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_DIVPESO.name(), String.valueOf(seq++), String.valueOf(codItem), ObjetoUtil.isNotNull(pesoLiquidoSeco) ? pesoLiquidoSeco.toString() : BigDecimal.ZERO.toString()));

            if ((romaneio.getOperacao().isPossuiDadosNfe() || romaneio.isPossuiDadosOrigem()) && ObjetoUtil.isNotNull(item.getPesoTotalNfe())) {
                quebra = BigDecimalUtil.positivo(item.getPesoTotalNfe()).subtract(pesoLiquidoSeco);
            } else if ((romaneio.getOperacao().isPossuiDadosNfe() || romaneio.isPossuiDadosOrigem()) && romaneio.getPesoLiquidoSecoOrigem().compareTo(BigDecimal.ZERO) == 1){
                quebra = romaneio.getPesoLiquidoSecoOrigem().subtract(pesoLiquidoSeco);
            }
        }

        return quebra;
    }

    public Integer montarBridgeParaUsuarioCriadorRomaneio(Romaneio romaneio, String requestKey, List<EventInRequestItem> filters, Integer seq) {

        if (ObjetoUtil.isNotNull(romaneio.getIdUsuarioAutorCadastro())) {
            final String login = ServiceInject.usuarioService.obterLoginPorId(romaneio.getIdUsuarioAutorCadastro());
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_USER_ROMA.name(), String.valueOf(seq++), login));
        }

        return seq;
    }

    public void montarObservacaoNota(Romaneio romaneio, String requestKey, List<EventInRequestItem> filters, Integer seq, int item) {

        int contadorItensObs = 1;
        List<String> listaObsNota = converterStringFormatoSAP(romaneio.getObservacaoNota());
        int ultimoGrupo = filters.stream().mapToInt(EventInRequestItem::getGrupoValid).max().orElseThrow(NoSuchElementException::new);
        for (String obsNota : listaObsNota) {
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_MESSAGE1.name(), String.valueOf(seq++), String.valueOf(contadorItensObs++), String.valueOf(ultimoGrupo + 1), obsNota));
        }
    }

    public Integer montarObservacao(Romaneio romaneio, String requestKey, List<EventInRequestItem> filters, Integer seq, int item) {

        List<String> listaObs = converterStringFormatoSAP(romaneio.getObservacao());
        int contadorItensObs = 1;
        for (String obs : listaObs) {
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_MESSAGE.name(), String.valueOf(seq++), String.valueOf(contadorItensObs++), String.valueOf(item), obs));
        }
        return seq;
    }

    public Integer montarPesagemDestino(Romaneio romaneio, BigDecimal descontos, String requestKey, List<EventInRequestItem> filters, Integer seq) {

        Pesagem pesagem = romaneio.getPesagem();
        final String destino = new Long(OrigemClassificacaoEnum.DESTINO.getCodigo()).toString();
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_PESAGEM.C_USER_PB.name(), String.valueOf(seq++), UM, destino, ObjetoUtil.isNotNull(pesagem.getUsuarioPesoInicial()) ? pesagem.getUsuarioPesoInicial().getLogin() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_PESAGEM.C_USER_PT.name(), String.valueOf(seq++), UM, destino, ObjetoUtil.isNotNull(pesagem.getUsuarioPesoFinal()) ? pesagem.getUsuarioPesoFinal().getLogin() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_PESAGEM.C_LPESA.name(), String.valueOf(seq++), UM, destino, OrigemClassificacaoEnum.DESTINO.getCodigo()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_PESAGEM.C_PBRUTO.name(), String.valueOf(seq++), UM, destino, ObjetoUtil.isNotNull(pesagem.getPesoInicial()) ? pesagem.getPesoInicial().toString() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_PESAGEM.C_PTARA.name(), String.valueOf(seq++), UM, destino, ObjetoUtil.isNotNull(pesagem.getPesoFinal()) ? pesagem.getPesoFinal().toString() : StringUtil.empty()));

        BigDecimal pesoLiquidoUmido = BigDecimal.ZERO;

        if (ObjetoUtil.isNull(pesagem.getPesoInicial())) {
            pesagem.setPesoInicial(BigDecimal.ZERO);
        }

        if (ObjetoUtil.isNull(pesagem.getPesoFinal())) {
            pesagem.setPesoFinal(BigDecimal.ZERO);
        }

        pesoLiquidoUmido = pesagem.getPesoInicial().subtract(pesagem.getPesoFinal()).setScale(0, RoundingMode.HALF_UP);
        final BigDecimal pesoLiquidoSeco = pesoLiquidoUmido.subtract(descontos).setScale(0, RoundingMode.HALF_UP);
        if (!romaneio.getOperacao().isPossuiRegraComplemento()) {
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_PESAGEM.C_PUMIDO.name(), String.valueOf(seq++), UM, destino, ObjetoUtil.isNotNull(pesoLiquidoUmido) ? pesoLiquidoUmido.toString() : StringUtil.empty()));
        }

        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_PESAGEM.C_PSECO.name(), String.valueOf(seq++), UM, destino, ObjetoUtil.isNotNull(pesoLiquidoSeco) ? pesoLiquidoSeco.toString() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_PESAGEM.C_TDESC.name(), String.valueOf(seq++), UM, destino, ObjetoUtil.isNotNull(descontos) ? descontos.toString() : StringUtil.empty()));

        return seq;
    }

    public Integer montarPesagemDestinoRateio(Pesagem pesagem, String requestKey, List<EventInRequestItem> filters, Integer seq, ItemNFPedido item, Romaneio romaneio, BigDecimal rateio) {

        final String destino = new Long(OrigemClassificacaoEnum.DESTINO.getCodigo()).toString();
        final BigDecimal pesoBruto = pesagem.getPesoInicial().setScale(0, RoundingMode.HALF_UP);
        final BigDecimal valorASerUsadoParaCalculoProporcionalidade;
        final boolean possuiConversaoLitragem = item.getMaterial().isRealizarConversaoLitragem();

        if (possuiConversaoLitragem) {
            final ConversaoLitragemItem conversaoLitragemItem = romaneio.getConversaoLitragem().getConversoes().stream().sorted(Comparator.comparing(ConversaoLitragemItem::getDataCadastro).reversed()).findFirst().orElse(null);
            valorASerUsadoParaCalculoProporcionalidade = conversaoLitragemItem.getVolumeSetaVinteGraus().setScale(0, RoundingMode.HALF_UP);
        } else {
            valorASerUsadoParaCalculoProporcionalidade = pesoBruto.subtract(pesagem.getPesoFinal()).setScale(0, RoundingMode.HALF_UP);
        }

        final BigDecimal porcentagemPorNota = (rateio.multiply(CEM)).divide(valorASerUsadoParaCalculoProporcionalidade, 2, RoundingMode.HALF_UP);
        final BigDecimal resultadoProporcional = (porcentagemPorNota.multiply(pesoBruto)).divide(CEM, 0, RoundingMode.HALF_UP);

        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES.C_PBRUTORT.name(), String.valueOf(seq++), UM, destino, ObjetoUtil.isNotNull(resultadoProporcional) ? resultadoProporcional.toString() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_PESAGEM.C_PBRUTO.name(), String.valueOf(seq++), UM, destino, ObjetoUtil.isNotNull(resultadoProporcional) ? resultadoProporcional.toString() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_PESAGEM.C_USER_PB.name(), String.valueOf(seq++), UM, destino, ObjetoUtil.isNotNull(pesagem.getUsuarioPesoInicial()) ? pesagem.getUsuarioPesoInicial().getLogin() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_PESAGEM.C_USER_PT.name(), String.valueOf(seq++), UM, destino, ObjetoUtil.isNotNull(pesagem.getUsuarioPesoFinal()) ? pesagem.getUsuarioPesoFinal().getLogin() : StringUtil.empty()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_PESAGEM.C_LPESA.name(), String.valueOf(seq++), UM, destino, OrigemClassificacaoEnum.DESTINO.getCodigo()));

        return seq;
    }

    public Integer montarPesagemOrigem(Romaneio romaneio, String requestKey, List<EventInRequestItem> filters, Integer seq) {

        final String origem = new Long(OrigemClassificacaoEnum.ORIGEM.getCodigo()).toString();

        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_PESAGEM.C_LPESA.name(), String.valueOf(seq++), UM, origem, OrigemClassificacaoEnum.ORIGEM.getCodigo()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_PESAGEM.C_PBRUTO.name(), String.valueOf(seq++), UM, origem, ObjetoUtil.isNotNull(romaneio.getPesoBrutoOrigem()) ? romaneio.getPesoBrutoOrigem().toString() : BigDecimal.ZERO.toString()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_PESAGEM.C_PTARA.name(), String.valueOf(seq++), UM, origem, ObjetoUtil.isNotNull(romaneio.getPesoTaraOrigem()) ? romaneio.getPesoTaraOrigem().toString() : BigDecimal.ZERO.toString()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_PESAGEM.C_PUMIDO.name(), String.valueOf(seq++), UM, origem, ObjetoUtil.isNotNull(romaneio.getPesoLiquidoUmidoOrigem()) ? romaneio.getPesoLiquidoUmidoOrigem().toString() : BigDecimal.ZERO.toString()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_PESAGEM.C_PSECO.name(), String.valueOf(seq++), UM, origem, ObjetoUtil.isNotNull(romaneio.getPesoLiquidoSecoOrigem()) ? romaneio.getPesoLiquidoSecoOrigem().toString() : BigDecimal.ZERO.toString()));
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_PESAGEM.C_TDESC.name(), String.valueOf(seq++), UM, origem, ObjetoUtil.isNotNull(romaneio.getDescontoOrigem()) ? romaneio.getDescontoOrigem().toString() : BigDecimal.ZERO.toString()));

        return seq;
    }

    public Integer montarClassificacaoOrigem(List<ItensClassificacao> itens, String requestKey, List<EventInRequestItem> filters, Integer seq) {

        final String origem = new Long(OrigemClassificacaoEnum.ORIGEM.getCodigo()).toString();

        for (ItensClassificacao item : itens) {

            final String itemClassificacao = new Long(item.getItemClassificao()).toString();

            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_CLASSIFICACAO.C_OCLASS.name(), String.valueOf(seq++), itemClassificacao, origem, OrigemClassificacaoEnum.ORIGEM.getCodigo()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_CLASSIFICACAO.C_ITENSC.name(), String.valueOf(seq++), itemClassificacao, origem, ObjetoUtil.isNotNull(item.getItemClassificao()) ? item.getItemClassificao() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_CLASSIFICACAO.C_INDICC.name(), String.valueOf(seq++), itemClassificacao, origem, ObjetoUtil.isNotNull(item.getIndice()) ? item.getIndice().toString() : BigDecimal.ZERO.toString()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_CLASSIFICACAO.C_PERCC.name(), String.valueOf(seq++), itemClassificacao, origem, item.getPercentualDescontoValido().toString()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_CLASSIFICACAO.C_PESOC.name(), String.valueOf(seq++), itemClassificacao, origem, item.getDescontoValido().toString()));
        }

        return seq;
    }

    public Integer montarClassificacaoDestino(Classificacao classificacao, String requestKey, List<EventInRequestItem> filters, Integer seq) {

        final String destino = new Long(OrigemClassificacaoEnum.DESTINO.getCodigo()).toString();
        filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_CLASSIFICACAO.C_USER_CLAS.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(classificacao.getUsuarioClassificador()) ? classificacao.getUsuarioClassificador().getLogin() : StringUtil.empty()));

        for (ItensClassificacao item : classificacao.getItens()) {

            final String itemClassificacao = new Long(item.getItemClassificao()).toString();

            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_CLASSIFICACAO.C_OCLASS.name(), String.valueOf(seq++), itemClassificacao, destino, OrigemClassificacaoEnum.DESTINO.getCodigo()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_CLASSIFICACAO.C_ITENSC.name(), String.valueOf(seq++), itemClassificacao, destino, ObjetoUtil.isNotNull(item.getItemClassificao()) ? item.getItemClassificao() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_CLASSIFICACAO.C_INDICC.name(), String.valueOf(seq++), itemClassificacao, destino, ObjetoUtil.isNotNull(item.getIndice()) ? item.getIndice().toString() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_CLASSIFICACAO.C_PERCC.name(), String.valueOf(seq++), itemClassificacao, destino, ObjetoUtil.isNotNull(item.getPercentualDesconto()) ? item.getPercentualDesconto().toString() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(requestKey, SAPRomaneio.BRIDGES_CLASSIFICACAO.C_PESOC.name(), String.valueOf(seq++), itemClassificacao, destino, item.getDescontoValido().toString()));
        }

        return seq;
    }

}
