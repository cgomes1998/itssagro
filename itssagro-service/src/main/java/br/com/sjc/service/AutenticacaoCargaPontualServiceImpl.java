package br.com.sjc.service;

import br.com.sjc.fachada.service.AutenticacaoCargaPontualService;
import br.com.sjc.fachada.service.cargaPontual.AutenticacaoService;
import br.com.sjc.modelo.pojo.cargaPontual.autenticacao.Autenticacao;
import br.com.sjc.modelo.pojo.cargaPontual.autenticacao.RetornoGerarToken;
import br.com.sjc.modelo.pojo.cargaPontual.autenticacao.RetornoValidadeToken;
import br.com.sjc.util.ObjetoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class AutenticacaoCargaPontualServiceImpl implements AutenticacaoCargaPontualService {

    @Value("${cargaPontual.servico.usuario}")
    private String usuario;

    @Value("${cargaPontual.servico.senha}")
    private String senha;

    @Value("${cargaPontual.servico.tokenCombinado}")
    private String tokenCombinado;

    @Value("${cargaPontual.servico.identificadorEmpresa}")
    private int identificadorEmpresa;

    @Autowired
    private AutenticacaoService autenticacaoService;

    private RetornoGerarToken retornoGerarToken;

    private Boolean consultarValidadeTokenAutenticacao() throws Exception {

        Boolean valido = Boolean.FALSE;

        if (ObjetoUtil.isNotNull(retornoGerarToken)) {

            RetornoValidadeToken retornoValidadeToken = autenticacaoService.consultarValidadeToken(retornoGerarToken.getRetorno().getConteudo().getToken());

            lancarExceptionErroIntegracao(retornoValidadeToken.getRetorno().getStatus(), "Erro ao consultar validade do token do carga pontual\nStatus: %s\nMensagem: %s", String.valueOf(retornoValidadeToken.getRetorno().getStatus()), retornoValidadeToken.getRetorno().getMensagem());

            valido = retornoValidadeToken.getRetorno().getConteudo().isValido();
        }

        return valido;
    }

    @Override
    public void gerarTokenAutenticacao() throws Exception {

        Boolean tokenValido = consultarValidadeTokenAutenticacao();

        if (!tokenValido) {

            RetornoGerarToken retornoGerarTokenTmp = autenticacaoService.gerarToken(Autenticacao.builder()
                    .identificadorEmpresa(identificadorEmpresa)
                    .loginUsuario(usuario)
                    .loginUsuarioSenha(senha)
                    .tokenCombinado(tokenCombinado).build());

            if (ObjetoUtil.isNotNull(retornoGerarTokenTmp)) {

                lancarExceptionErroIntegracao(retornoGerarTokenTmp.getRetorno().getStatus(), "Erro ao gerar token do carga pontual\nStatus: %s\nMensagem: %s", String.valueOf(retornoGerarTokenTmp.getRetorno().getStatus()), retornoGerarTokenTmp.getRetorno().getMensagem());

                retornoGerarToken = retornoGerarTokenTmp;
            }
        }
    }

    @Override
    public RetornoGerarToken getRetornoGerarToken() {

        return retornoGerarToken;
    }

    private void lancarExceptionErroIntegracao(int status, String mensagem, String... params) throws Exception {

        if (status != 200) {

            throw new Exception(String.format(mensagem, params));
        }
    }

}
