package br.com.sjc.service;

import br.com.sjc.fachada.service.PortariaVeiculoService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.cfg.PortariaVeiculo;
import br.com.sjc.modelo.dto.PortariaMotivoDTO;
import br.com.sjc.modelo.dto.PortariaVeiculoDTO;
import br.com.sjc.persistencia.dao.PortariaVeiculoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PortariaVeiculoServiceImpl extends ServicoGenerico<Long, PortariaVeiculo> implements PortariaVeiculoService {
    @Autowired
    private PortariaVeiculoDAO dao;

    @Override
    public Class<PortariaVeiculoDTO> getDtoListagem() {
        return PortariaVeiculoDTO.class;
    }

    @Override
    public PortariaVeiculoDAO getDAO() {
        return this.dao;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PortariaVeiculoDTO> listarVeiculosAtivos() {
        List<PortariaVeiculoDTO> motivos = dao.listarAtivos().stream().map(PortariaVeiculoDTO::new).collect(Collectors.toList());
        return motivos;
    }
}
