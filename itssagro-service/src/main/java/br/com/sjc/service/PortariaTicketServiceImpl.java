package br.com.sjc.service;

import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.PortariaTicketService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.PortariaTicket;
import br.com.sjc.modelo.Senha;
import br.com.sjc.modelo.SenhaEtapa;
import br.com.sjc.modelo.cfg.DadosSincronizacao_;
import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.cfg.Portaria;
import br.com.sjc.modelo.dto.*;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.enums.StatusSenhaEnum;
import br.com.sjc.modelo.enums.TipoParametroEnum;
import br.com.sjc.modelo.relatorio.GerarRelatorioExcel;
import br.com.sjc.modelo.response.FileResponse;
import br.com.sjc.persistencia.dao.PortariaTicketDAO;
import br.com.sjc.persistencia.dao.fachada.PortariaTicketCustomRepository;
import br.com.sjc.persistencia.dao.fachada.RomaneioCustomRepository;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.File;
import java.io.OutputStream;
import java.net.Socket;
import java.text.Normalizer;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.CompletableFuture;

@Service
public class PortariaTicketServiceImpl extends ServicoGenerico<Long, PortariaTicket> implements PortariaTicketService {
    @Autowired
    private PortariaTicketDAO dao;

    @Autowired
    private PortariaTicketCustomRepository portariaTicketCustomRepository;

    @Override
    public Class<PortariaTicketDTO> getDtoListagem() {
        return PortariaTicketDTO.class;
    }

    @Override
    public PortariaTicketDAO getDAO() {
        return this.dao;
    }


    @Override
    public PortariaTicket salvar(PortariaTicket ticket){
        ticket.setSenha(this.gerarSenha(ticket));

        return super.salvar(ticket);
    }

    @Override
    public PortariaTicket informarSaida(Long idTicket) {

        PortariaTicket ticketSalva = get(idTicket);

        ticketSalva.setDataSaida(new Date());

        return getDAO().save(ticketSalva);
    }

    @Override
    public PortariaTicket informarEntrada(Long idTicket) {

        PortariaTicket ticketSalva = get(idTicket);

        ticketSalva.setDataEntrada(new Date());

        return getDAO().save(ticketSalva);
    }

    @Override
    public void imprimirTicketEntrada(Long id) {

        PortariaTicket ticket = get(id);

        imprimirSenhaBematech(ticket, 1);
    }


    @Override
    public void imprimirTicketSaida(Long id) {

        PortariaTicket ticket = get(id);

        imprimirSenhaBematech(ticket, 2);
    }


    @Override
    public void imprimirTicketMarcacao(Long id) {

        PortariaTicket ticket = get(id);

        imprimirSenhaBematech(ticket, 3);
    }


    @Override
    public FileResponse obterRelatorioPortariaTicket(PortariaTicket filtro) {

        List<PortariaTicket> tickets = this.portariaTicketCustomRepository.obterDadosRelatorioPortariaTicket(filtro);
        if (ColecaoUtil.isNotEmpty(tickets)) {
            return GerarRelatorioExcel.obterRelatorioPortariaTicket(tickets);
        }

        return null;
    }

    @Override
    public Specification<PortariaTicket> obterEspecification(final Paginacao<PortariaTicket> paginacao) {

        final PortariaTicket entidade = paginacao.getEntidade();

        final Specification<PortariaTicket> specification = new Specification<PortariaTicket>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<PortariaTicket> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                if (StringUtil.isNotNullEmpty(entidade.getSenha())) {
                    predicates.add(builder.and(builder.equal(builder.upper(root.get("senha")), entidade.getSenha().toUpperCase())));
                }

                if (ObjetoUtil.isNotNull(entidade.getDataEntrada())) {
                    predicates.add(builder.and(builder.greaterThanOrEqualTo(root.get("dataEntrada"), entidade.getDataEntrada())));
                }

                if (ObjetoUtil.isNotNull(entidade.getDataSaida())) {
                    predicates.add(builder.and(builder.or(builder.lessThanOrEqualTo(root.get("dataSaida"), entidade.getDataSaida()), builder.isNull(root.get("dataSaida")))));
                }

                if (StringUtil.isNotNullEmpty(entidade.getCnpj())) {
                    predicates.add(builder.and(builder.like(builder.upper(root.get("cnpj")), entidade.getCnpj().toUpperCase() + "%")));
                }

                if (StringUtil.isNotNullEmpty(entidade.getCpfMotorista())) {
                    predicates.add(builder.and(builder.like(builder.upper(root.get("cpfMotorista")), entidade.getCpfMotorista().toUpperCase() + "%")));
                }

                if (StringUtil.isNotNullEmpty(entidade.getMotorista())) {
                    predicates.add(builder.and(builder.like(builder.upper(root.get("motorista")), "%" + entidade.getMotorista().toUpperCase() + "%")));
                }

                if (StringUtil.isNotNullEmpty(entidade.getPlacaVeiculo())) {
                    predicates.add(builder.and(builder.like(builder.upper(root.get("placaVeiculo")), entidade.getPlacaVeiculo().toUpperCase() + "%")));
                }

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    private String gerarSenha(PortariaTicket ticket){
        if(StringUtil.isNotNullEmpty(ticket.getSenha()) || ticket.getId() != null){
            return ticket.getSenha();
        }

        LocalDate hoje = LocalDate.now();
        return "#" + ticket.getPortaria().getSigla()+ "-" + getAbreviacaoProduto(ticket) + "-" + String.format("%02d", hoje.getYear() % 100) + String.format("%02d", hoje.getMonthValue()) + String.format("%03d"
                , getDAO().count() + 1);
    }

    private String getAbreviacaoProduto(PortariaTicket ticket) {
        String produto = ticket.getProduto().getDescricao();

        String normalizedString = Normalizer.normalize(produto, Normalizer.Form.NFD);
        String result = normalizedString.replaceAll("[^\\p{ASCII}]", "");

        return result.substring(0, 3).toUpperCase();
    }

    private void imprimirSenhaBematech(PortariaTicket ticket, int modeloImpressao) throws ServicoException {
        String ipImpressora = ticket.getPortaria().getIpImpressora();
        String portaImpressora = ticket.getPortaria().getPortaImpressora();

        if(StringUtil.isNotNullEmpty(ipImpressora) && StringUtil.isNotNullEmpty(portaImpressora)) {

            String quebraLinha = "" + (char) 10;
            String defineTamanhoPadraoLinha = "" + (char) 27 + (char) 50;
            String modoExpandido = "" + (char) 24 + (char) 14;
            String iniciarImpressora = ("" + (char) 27 + (char) 116 + (char) 8 + (char) 10);
            String tabImpressora = ("" + (char) 9);
            String ativarNegrito = "" + (char) 27 + "E ";
            String desativaNegrito = "" + (char) 27 + "F ";
            String centralizarTexto = "" + (char) 27 + (char) 97 + "1 ";
            String esquerdaTexto = "" + (char) 27 + (char) 97 + "0 ";
            String direitaTexto = "" + (char) 27 + (char) 97 + "2 ";
            String acionarGuilhotina = "" + (char) 27 + (char) 105;

            Portaria portaria = ticket.getPortaria();
            String tipoFluxo = "ENTRADA";
            if(modeloImpressao == 2){
                tipoFluxo = "SAÍDA";
            } else if(modeloImpressao == 3){
                tipoFluxo = "MARCAÇÃO DE VEZ";
            }

            String senhaLayout = "";
            senhaLayout += iniciarImpressora;
            senhaLayout += defineTamanhoPadraoLinha;
            senhaLayout += centralizarTexto + "------------------------------------------------" + centralizarTexto + quebraLinha;
            senhaLayout += esquerdaTexto + "SJC BIO ENERGIA " + quebraLinha;
            senhaLayout += centralizarTexto + "Ticket de Entrada e Saída de Veiculos e Caminhões" + centralizarTexto + quebraLinha;
            senhaLayout += esquerdaTexto + "Portaria " + tabImpressora + "Fluxo " + centralizarTexto + quebraLinha;
            senhaLayout += esquerdaTexto + ativarNegrito + portaria.getDescricao() + tabImpressora + tipoFluxo + desativaNegrito + centralizarTexto + quebraLinha;
            senhaLayout += centralizarTexto + "------------------------------------------------" + centralizarTexto + quebraLinha;
            senhaLayout += esquerdaTexto + ativarNegrito + "Posição:" + desativaNegrito + ticket.getSenha() + quebraLinha;

            if(modeloImpressao == 3 || modeloImpressao == 2){
                String dataMarcacao = DateUtil.formatData(ticket.getDataMarcacao());
                String horaMarcacao = DateUtil.formatHoraMinuto(ticket.getDataMarcacao());

                senhaLayout += esquerdaTexto + ativarNegrito + "Data Marcação Vez:" + desativaNegrito + dataMarcacao + "  " + direitaTexto + ativarNegrito + "Hora:" + desativaNegrito + horaMarcacao + quebraLinha;
            }

            if(modeloImpressao == 1 || modeloImpressao == 2){
                String dataChegada = DateUtil.formatData(ticket.getDataEntrada());
                String horaChegada = DateUtil.formatHoraMinuto(ticket.getDataEntrada());

                senhaLayout += esquerdaTexto + ativarNegrito + "Data Chegada:" + desativaNegrito + dataChegada + "  " + direitaTexto + ativarNegrito + "Hora:" + desativaNegrito + horaChegada + quebraLinha;
            }

            if (modeloImpressao == 2) {
                String dataSaida = DateUtil.formatData(ticket.getDataSaida());
                String horaSaida = DateUtil.formatHoraMinuto(ticket.getDataSaida());

                senhaLayout += esquerdaTexto + ativarNegrito + "Data Saída:" + desativaNegrito + dataSaida + "  " + direitaTexto + ativarNegrito + "Hora:" + desativaNegrito + horaSaida + quebraLinha;
            }

            senhaLayout += esquerdaTexto + " " + quebraLinha;
            senhaLayout += esquerdaTexto + ativarNegrito + "Produto:" + desativaNegrito + exibeConteudoLinhaBematech(ticket.getProduto().getDescricao(), 8) + quebraLinha;
            senhaLayout += esquerdaTexto + " " + quebraLinha;
            senhaLayout += ativarNegrito + "Motorista:" + desativaNegrito + exibeConteudoLinhaBematech(ticket.getMotorista(), 10) + quebraLinha;
            senhaLayout += ativarNegrito + "Placa / Cavalo:" + desativaNegrito + exibeTextoImpressao(ticket.getPlacaVeiculo()) + quebraLinha;
            senhaLayout += ativarNegrito + "Carreta 1 ....:" + desativaNegrito + exibeTextoImpressao(ticket.getPlacaCarreta1()) + quebraLinha;
            senhaLayout += ativarNegrito + "Carreta 2 ....:" + desativaNegrito + exibeTextoImpressao(ticket.getPlacaCarreta2()) + quebraLinha;
            senhaLayout += ativarNegrito + "Carreta 3 ....:" + desativaNegrito + exibeTextoImpressao(ticket.getPlacaCarreta3()) + quebraLinha;
            senhaLayout += centralizarTexto + "------------------------------------------------" + centralizarTexto + quebraLinha;
            senhaLayout += esquerdaTexto + ativarNegrito + "ID Usuário Portaria:" + desativaNegrito + exibeConteudoLinhaBematech(exibeTextoImpressao(ticket.getIdColaborador()), 20) + quebraLinha;
            senhaLayout += centralizarTexto + "------------------------------------------------" + quebraLinha;
            senhaLayout += quebraLinha + quebraLinha + quebraLinha + quebraLinha + quebraLinha + quebraLinha + quebraLinha;
            senhaLayout += acionarGuilhotina;

            conectarAImpressoraBematechEImprimirConteudo(senhaLayout, ipImpressora, portaImpressora);

        } else {
            throw new ServicoException("Portaria não possui os dados da impressora configurado. Verifique e tente novamente.");
        }
    }

    public void conectarAImpressoraBematechEImprimirConteudo(String textoImpressao, String ip, String porta){
        try {
            Socket socket = new Socket(ip, Integer.parseInt(porta));

            OutputStream outputStream = socket.getOutputStream();

            outputStream.write(textoImpressao.getBytes());

            try {

                Thread.sleep(5000);

            } catch (InterruptedException e) {
            }

            outputStream.flush();

            outputStream.close();

            socket.close();
        }catch (Exception e){
            e.printStackTrace();

            throw new ServicoException("Erro ao realizar a impressão dos arquivos, por favor contate o adminstrador. Erro conexão com bematech.");
        }

    }

    private String exibeConteudoLinhaBematech(String valor, int descontar){
        int caracteresLimite = 47 - descontar;

        return valor.length() <= caracteresLimite ? valor : valor.substring(0, caracteresLimite - 3) + "...";
    }

    private String exibeTextoImpressao(String texto){
        if(StringUtil.isNotNullEmpty(texto)){
            return texto;
        }

        return "-";
    }
}
