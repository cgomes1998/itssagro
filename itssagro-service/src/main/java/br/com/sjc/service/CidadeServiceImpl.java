package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.service.CidadeService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.Cidade;
import br.com.sjc.modelo.dto.CidadeDTO;
import br.com.sjc.modelo.enums.UFEnum;
import br.com.sjc.persistencia.dao.CidadeDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CidadeServiceImpl extends ServicoGenerico<Long, Cidade> implements CidadeService {

    @Autowired
    private CidadeDAO dao;

    @Override
    public DAO<Long, Cidade> getDAO() {

        return dao;

    }

    @Override
    public Class<CidadeDTO> getDtoListagem() {
        return CidadeDTO.class;
    }

    @Override
    public List<Cidade> listarCidadeUF(UFEnum uf) {
        return this.dao.findByEstado(uf);
    }
}
