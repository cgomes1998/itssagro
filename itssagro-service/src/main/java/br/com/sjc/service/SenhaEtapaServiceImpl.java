package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.service.SenhaEtapaService;
import br.com.sjc.fachada.service.SenhaService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.Senha;
import br.com.sjc.modelo.SenhaEtapa;
import br.com.sjc.modelo.dto.FluxoEtapaDTO;
import br.com.sjc.modelo.dto.SenhaEtapaDTO;
import br.com.sjc.modelo.dto.SenhaEtapaRelatorioDTO;
import br.com.sjc.persistencia.dao.SenhaDAO;
import br.com.sjc.persistencia.dao.SenhaEtapaDAO;
import br.com.sjc.persistencia.dao.fachada.SenhaCustomRepository;
import br.com.sjc.persistencia.dao.fachada.SenhaEtapaCustomRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Log4j2
@Service
public class SenhaEtapaServiceImpl extends ServicoGenerico<Long, SenhaEtapa> implements SenhaEtapaService {

    @Autowired
    private SenhaEtapaDAO dao;

    @Autowired
    private SenhaEtapaCustomRepository senhaEtapaCustomRepository;

    @Override
    public List<SenhaEtapaRelatorioDTO> obterSenhasEtapasPorFluxoEtapas(Date data, Date dataFim, List<FluxoEtapaDTO> fluxoEtapas) {
        return senhaEtapaCustomRepository.obterSenhasEtapasPorFluxoEtapas(data, dataFim, fluxoEtapas);
    }

    @Override
    public SenhaEtapaDAO getDAO() {
        return dao;
    }
}
