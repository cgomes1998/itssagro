package br.com.sjc.service;

import br.com.sjc.fachada.service.DepositoService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.sap.Deposito;
import br.com.sjc.modelo.enums.TipoParametroEnum;
import br.com.sjc.persistencia.dao.DepositoDAO;
import br.com.sjc.persistencia.dao.ParametroDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepositoServiceImpl extends ServicoGenerico<Long, Deposito> implements DepositoService {

    @Autowired
    private DepositoDAO dao;

    @Autowired
    private ParametroDAO parametroDAO;

    @Override
    public Deposito obterPorCodigo(final String codigo) {
        return this.getDAO().findByCodigo(codigo);
    }

    @Override
    public Deposito obterUnicoPorCodigo(final String codigo) {
        return this.getDAO().findFirstByCodigo(codigo);
    }

    @Override
    public List<Deposito> obterDepositosPorCentroId(final Long idCentro) {
        return this.getDAO().findByCentroIdOrderByCodigoAsc(idCentro);
    }

    @Override
    public List<Deposito> obterDepositosPorCentroLogado() {

        final String codigoCentro = this.parametroDAO.findByTipoParametro(TipoParametroEnum.CENTRO_SAP).getValor();

        return this.dao.findByCentroCodigoOrderByCodigoAsc(codigoCentro);
    }

    @Override
    public DepositoDAO getDAO() {
        return this.dao;
    }
}
