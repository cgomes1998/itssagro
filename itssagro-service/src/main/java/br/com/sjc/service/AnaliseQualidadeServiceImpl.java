package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.service.*;
import br.com.sjc.fachada.servico.impl.ServiceInject;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.*;
import br.com.sjc.modelo.cfg.Empresa;
import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.dto.AnaliseFiltroDTO;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.enums.TipoParametroEnum;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.persistencia.dao.AnaliseDAO;
import br.com.sjc.persistencia.dao.AnaliseQualidadeDAO;
import br.com.sjc.persistencia.dao.fachada.AnaliseQualidadeCustomRepository;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.RelatorioUtil;
import br.com.sjc.util.StringUtil;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AnaliseQualidadeServiceImpl extends ServicoGenerico<Long, AnaliseQualidade> implements AnaliseQualidadeService {

    private static final String REL_ANALISE_JASPER = "jasper/rel_analise.jasper";

    private static final String REL_ANALISE_SUB_JASPER = "jasper/rel_analise_subreport.jasper";

    private static final String LOGO_PNG = "img/logo.png";

    @Autowired
    private AnaliseQualidadeDAO analiseQualidadeDAO;

    @Autowired
    private AnaliseDAO analiseDAO;

    @Autowired
    private RomaneioService romaneioService;

    @Autowired
    private ParametroService parametroService;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private EmpresaService empresaService;

    @Autowired
    private AnaliseQualidadeCustomRepository analiseQualidadeCustomRepository;

    @Autowired
    private LaudoService laudoService;

    @Autowired
    private AnaliseService analiseService;

    @Override
    public byte[] imprimirLaudo(Long idLaudo) throws Exception {

        final Laudo laudo = laudoService.get(idLaudo);

        InputStream inputStream = AnaliseQualidadeServiceImpl.class.getClassLoader().getResourceAsStream(REL_ANALISE_JASPER);

        Map<String, Object> parametros = this.montarParametrosRelatorio(null, laudo);

        AnaliseQualidade analise = new AnaliseQualidade();

        analise.setRomaneio(new Romaneio());

        analise.getRomaneio().setMaterial(laudo.getMaterial());

        analise.setCertificado(laudo.getNumeroCertificado());

        analise.setDataCertificado(laudo.getDataCadastro());

        analise.setLoteAnaliseQualidade(laudo.getLoteAnaliseQualidade());

        if (ObjetoUtil.isNotNull(analise.getLoteAnaliseQualidade())) {

            analise.getLoteAnaliseQualidade().setCodigo(laudo.getTanque());

        } else {

            analise.setLoteAnaliseQualidade(LoteAnaliseQualidade.builder().codigo(laudo.getTanque()).build());
        }

        if (ObjetoUtil.isNull(analise.getAssinatura()) && ObjetoUtil.isNotNull(laudo.getAssinatura()) && ObjetoUtil.isNotNull(laudo.getAssinatura().getArquivo())) {

            analise.setAssinaturaStream(new ByteArrayInputStream(Base64.decodeBase64(laudo.getAssinatura().getArquivo().getBase64())));

        } else if (ObjetoUtil.isNotNull(analise.getAssinatura()) && ObjetoUtil.isNotNull(analise.getAssinatura().getArquivo())) {

            analise.setAssinaturaStream(new ByteArrayInputStream(Base64.decodeBase64(analise.getAssinatura().getArquivo().getBase64())));
        }

        if(StringUtil.isNotNullEmpty(laudo.getVolumeCertificado())){
            String valorFormatado = (new DecimalFormat("0,000").format(Double.parseDouble(laudo.getVolumeCertificado()))).replace(",", ".");
            analise.setVolumeCertificado(String.join(" ", valorFormatado, laudo.getUnidadeMedidaCertificado()));
        }

        if (ObjetoUtil.isNotNull(laudo.getMateriaPrima())) {
            analise.setMateriaPrima(laudo.getMateriaPrima().getDescricao());
        } else {
            analise.setMateriaPrima("");
        }

        analise.setObservacao(laudo.getObservacao());

        analise.setItens(laudo.getItens().stream().map(i -> new AnaliseQualidadeItem(i)).collect(Collectors.toList()));

        List<AnaliseQualidade> analises = new ArrayList<>();

        analises.add(analise);

        return RelatorioUtil.gerarPdf(analises, inputStream, parametros);
    }

    @Override
    public byte[] imprimirRomaneio(Long idRomaneio, Romaneio romaneioConsultado) throws Exception {

        Romaneio romaneio = ObjetoUtil.isNotNull(romaneioConsultado) ? romaneioConsultado : romaneioService.get(idRomaneio);

        if (ColecaoUtil.isEmpty(romaneio.getAnalises())) {

            throw new Exception("Nenhuma Análise de Qualidade encontrada");
        }

        InputStream inputStream = AnaliseQualidadeServiceImpl.class.getClassLoader().getResourceAsStream(REL_ANALISE_JASPER);

        Map<String, Object> parametros = this.montarParametrosRelatorio(romaneio, null);

        romaneio.setMaterial(romaneio.getItens().stream().map(ItemNFPedido::getMaterial).filter(Objects::nonNull).findFirst().orElse(romaneio.getMaterial()));

        final Long idLaudo = romaneio.getAnalises().stream().map(AnaliseQualidade::getIdLaudo).filter(Objects::nonNull).findFirst().orElse(null);

        if (romaneio.getMaterial().isBuscarCertificadoQualidade() && ObjetoUtil.isNotNull(idLaudo)) {

            final Laudo laudo = ServiceInject.laudoService.get(idLaudo);

            romaneio.getAnalises().forEach(i -> {

                i.setRomaneio(romaneio);

                i.setCertificado(laudo.getNumeroCertificado());

                i.setDataCertificado(laudo.getDataCadastro());

                if(StringUtil.isNotNullEmpty(laudo.getVolumeCertificado())){
                    String valorFormatado = (new DecimalFormat("0,000").format(Double.parseDouble(laudo.getVolumeCertificado()))).replace(",", ".");
                    i.setVolumeCertificado(String.join(" ", valorFormatado, laudo.getUnidadeMedidaCertificado()));
                }

                if (ObjetoUtil.isNotNull(laudo.getMateriaPrima())) {
                    i.setMateriaPrima(laudo.getMateriaPrima().getDescricao());
                } else {
                    i.setMateriaPrima("");
                }

                if (ObjetoUtil.isNull(i.getAssinatura()) && ObjetoUtil.isNotNull(laudo.getAssinatura()) && ObjetoUtil.isNotNull(laudo.getAssinatura().getArquivo())) {

                    i.setAssinaturaStream(new ByteArrayInputStream(Base64.decodeBase64(laudo.getAssinatura().getArquivo().getBase64())));

                } else if (ObjetoUtil.isNotNull(i.getAssinatura()) && ObjetoUtil.isNotNull(i.getAssinatura().getArquivo())) {

                    i.setAssinaturaStream(new ByteArrayInputStream(Base64.decodeBase64(i.getAssinatura().getArquivo().getBase64())));

                }
            });
        } else {

            romaneio.getAnalises().forEach(i -> {

                i.setRomaneio(romaneio);

                i.setCertificado(i.getId().toString());

                i.setDataCertificado(i.getDataCadastro());

                if (ObjetoUtil.isNotNull(i.getAssinatura()) && ObjetoUtil.isNotNull(i.getAssinatura().getArquivo())) {

                    i.setAssinaturaStream(new ByteArrayInputStream(Base64.decodeBase64(i.getAssinatura().getArquivo().getBase64())));
                }
            });
        }

        return RelatorioUtil.gerarPdf(romaneio.getAnalises(), inputStream, parametros);
    }

    @Override
    public List<AnaliseQualidade> obterAnaliseQualidadeParaCarregamento(AnaliseFiltroDTO filtroDTO) {

        final Laudo laudo = laudoService.obterCertificadoPorMaterialETanque(filtroDTO);

        if (ObjetoUtil.isNull(laudo)) {

            return null;
        }

        Romaneio romaneio = analiseQualidadeCustomRepository.obterAnaliseQualidadeParaCarregamento(filtroDTO, laudo);

        if (ObjetoUtil.isNull(romaneio)) {

            Analise analise = analiseDAO.findByMaterialId(filtroDTO.getMaterial().getId());

            List<AnaliseItem> analiseItens = analise.getItens();

            analiseItens = analiseItens.stream().filter(i -> StatusEnum.ATIVO.equals(i.getStatus())).collect(Collectors.toList());

            AnaliseQualidade analiseQualidade = new AnaliseQualidade();

            analiseQualidade.setIdLaudo(laudo.getId());

            analiseQualidade.setLoteAnaliseQualidade(laudo.getLoteAnaliseQualidade());

            analiseQualidade.setDataCertificado(laudo.getDataCadastro());

            analiseQualidade.setLacreAmostra(laudo.getLacreAmostra());

            analiseQualidade.setCertificado(laudo.getNumeroCertificado());

            analiseQualidade.setAssinatura(laudo.getAssinatura());

            analiseQualidade.setItens(analiseItens.stream().map(analiseItem -> {

                AnaliseQualidadeItem analiseQualidadeItem = new AnaliseQualidadeItem();

                analiseQualidadeItem.setAnaliseItem(analiseItem);

                LaudoItem laudoItem =
                        laudo.getItens().stream().filter(li -> li.getCaracteristica().concat(":").concat(li.getSubCaracteristica()).equalsIgnoreCase(analiseItem.getCaracteristica().concat(":").concat(analiseItem.getSubCaracteristica()))).findFirst().orElse(null);

                if (ObjetoUtil.isNotNull(laudoItem)) {

                    analiseQualidadeItem.setResultado(laudoItem.getResultado());
                }

                return analiseQualidadeItem;

            }).collect(Collectors.toList()));

            return Collections.singletonList(analiseQualidade);
        }

        List<AnaliseQualidade> analises = romaneio.getAnalises();

        this.removerReferenciaDaAnaliseQualidade(analises);

        return analises.stream().filter(analiseQualidade -> ObjetoUtil.isNotNull(analiseQualidade.getIdLaudo()) && analiseQualidade.getIdLaudo().equals(laudo.getId())).collect(Collectors.toList());
    }

    @Override
    public boolean laudoEmUso(Long idLaudo) {
        return analiseQualidadeDAO.laudoEmUso(idLaudo);
    }

    private void removerReferenciaDaAnaliseQualidade(List<AnaliseQualidade> analises) {

        analises.stream().forEach(analiseQualidade -> {

            analiseQualidade.setId(null);

            analiseQualidade.getItens().stream().forEach(item -> item.setId(null));
        });
    }

    private Map<String, Object> montarParametrosRelatorio(Romaneio romaneio, Laudo laudo) throws JRException {

        Map<String, Object> parametros = new HashMap<>();
        boolean exibirCamposANP = false;

        parametros.put("LOGO", AnaliseQualidadeServiceImpl.class.getClassLoader().getResourceAsStream(LOGO_PNG));

        parametros.put("SUBREPORT", (JasperReport) JRLoader.loadObject(AnaliseQualidadeServiceImpl.class.getClassLoader().getResourceAsStream(REL_ANALISE_SUB_JASPER)));

        if (ObjetoUtil.isNotNull(romaneio)) {

            parametros.put("NOTAS_FISCAIS", romaneio.getRetornoNfes().stream().map(RetornoNFe::getNfe).distinct().filter(Objects::nonNull).collect(Collectors.joining(",")));

            parametros.put("SAFRA", romaneio.getItens().stream().map(ItemNFPedido::getSafra).map(Safra::getDescricao).filter(Objects::nonNull).findFirst().orElse(""));

            if(romaneio.getMaterial() != null){
                exibirCamposANP = romaneio.getMaterial().isPossuiResolucaoAnp();
            }
        }

        if(laudo != null){
            exibirCamposANP = laudo.getMaterial().isPossuiResolucaoAnp();
        }

        parametros.put("EXIBIR_CAMPOS_ANP", exibirCamposANP);

        this.montarParametrosRelatorioEmpresa(parametros);

        this.montarParametrosRelatorioEndereco(parametros);

        return parametros;
    }

    private void montarParametrosRelatorioEndereco(Map<String, Object> parametros) {

        Parametro parametro = this.parametroService.obterPorTipoParametro(TipoParametroEnum.CENTRO_CLIENTE);

        if (!parametro.isNew()) {

            Cliente cliente = this.clienteService.obterPorCodigo(parametro.getValor());

            if (!cliente.isNew()) {

                parametros.put("ENDERECO", String.join(", ", cliente.getEndereco(), cliente.getBairro()));

                parametros.put("MUNICIPIO", String.join(" - ", cliente.getMunicipio(), cliente.getUf().getSigla()));

                parametros.put("CEP", cliente.getCep());

                parametros.put("TELEFONE", cliente.getTelefone());
            }
        }
    }

    private void montarParametrosRelatorioEmpresa(Map<String, Object> parametros){

        List<Empresa> empresas = empresaService.listar();
        if(!empresas.isEmpty()){
            Empresa empresa = empresas.stream().findFirst().get();

            parametros.put("RAZAO_SOCIAL_EMPRESA", empresa.getRazaoSocial());
            parametros.put("ENDERECO_EMPRESA", empresa.getEndereco());
            parametros.put("CIDADE_ESTADO_EMPRESA", String.join("/", empresa.getCidade(), empresa.getEstado()));
            parametros.put("CNPJ_EMPRESA", empresa.getCnpj());
            parametros.put("CEP_EMPRESA", empresa.getCep());
            parametros.put("CODIGO_ANP_EMPRESA", empresa.getCodigoAnp());
        }
    }

    @Override
    public DAO<Long, AnaliseQualidade> getDAO() {

        return analiseQualidadeDAO;
    }

}
