package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.service.FluxoEtapaService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.Fluxo;
import br.com.sjc.modelo.FluxoEtapa;
import br.com.sjc.modelo.TipoFluxoEnum;
import br.com.sjc.modelo.dto.EtapaCampoDTO;
import br.com.sjc.modelo.dto.FluxoEtapaDTO;
import br.com.sjc.modelo.dto.ProximoFluxoEtapaDTO;
import br.com.sjc.modelo.enums.EtapaCampoEnum;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.sap.Centro;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.persistencia.dao.FluxoEtapaDAO;
import br.com.sjc.persistencia.dao.fachada.FluxoEtapaCustomRepository;
import br.com.sjc.util.ColecaoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FluxoEtapaServiceImpl extends ServicoGenerico<Long, FluxoEtapa> implements FluxoEtapaService {

    @Autowired
    private FluxoEtapaDAO fluxoEtapaDAO;

    @Autowired
    private FluxoEtapaCustomRepository fluxoEtapaCustomRepository;

    @Override
    public void preSalvar(FluxoEtapa fluxoEtapa) {

        super.preSalvar(fluxoEtapa);
        fluxoEtapa.getCriterios().forEach(etapaCriterio -> {
            etapaCriterio.setFluxoEtapa(fluxoEtapa);
            etapaCriterio.getCondicoes().forEach(t -> t.setEtapaCriterio(etapaCriterio));
            etapaCriterio.getEventos().forEach(t -> t.setEtapaCriterio(etapaCriterio));
            etapaCriterio.getAcoes().forEach(t -> t.setEtapaCriterio(etapaCriterio));
        });
    }

    @Override
    public List<EtapaCampoDTO> listarCampos(EtapaEnum etapa) {

        return EtapaCampoEnum.obterPorEtapa(etapa).stream().map(EtapaCampoDTO::new).collect(Collectors.toList());
    }

    @Override
    public FluxoEtapaDTO obterPorCentroEtapaMaterial(Centro centro, EtapaEnum etapa, String codigoMaterial, String descricaoMaterial, TipoFluxoEnum tipoFluxo) {

        List<FluxoEtapaDTO> fluxoEtapaDTOS = fluxoEtapaCustomRepository.obterPorCentroEtapaMaterial(centro, etapa, codigoMaterial, descricaoMaterial, tipoFluxo);

        if (ColecaoUtil.isEmpty(fluxoEtapaDTOS)) {

            throw new ServicoException("Nenhum Fluxo Etapa configurado para os parâmetros informados.");
        }

        if (ColecaoUtil.isNotEmpty(fluxoEtapaDTOS) && ColecaoUtil.quantidade(fluxoEtapaDTOS) > 1) {

            String mensagem = "Mais de um Fluxo Etapa configurado para o centro ".concat(centro != null ? centro.getDescricao() : "").concat(", etapa ").concat(etapa.getDescricao()).concat(" e material ").concat(descricaoMaterial);

            throw new ServicoException(mensagem);
        }

        return fluxoEtapaDTOS.stream().findFirst().get();
    }

    @Override
    public List<FluxoEtapaDTO> listarDtoPorFluxo(List<Long> idFluxos) {

        return fluxoEtapaCustomRepository.listarPorFluxo(idFluxos);
    }

    @Override
    public List<FluxoEtapa> listarPorFluxo(Long fluxoId) {

        return this.getDAO().findAll((root, query, builder) -> builder.equal(root.get("fluxo"), fluxoId));
    }

    @Override
    public FluxoEtapa obterFluxoEtapaPorMaterialEEtapa(TipoFluxoEnum tipoFluxo, Material material, EtapaEnum etapaEnum) {

        return fluxoEtapaDAO.findTop1ByFluxoTipoFluxoAndFluxoMateriaisMaterialIdAndEtapa(tipoFluxo, material.getId(), etapaEnum);
    }

    @Override
    public List<ProximoFluxoEtapaDTO> listarProximasEtapasDoFluxo(Fluxo fluxo) {

        return fluxoEtapaCustomRepository.listarProximasEtapasDoFluxo(fluxo);
    }

    @Override
    public DAO<Long, FluxoEtapa> getDAO() {

        return fluxoEtapaDAO;
    }

    @Override
    public Class<FluxoEtapaDTO> getDtoListagem() {

        return FluxoEtapaDTO.class;
    }
}
