package br.com.sjc.service;

import br.com.sjc.fachada.service.CampoOperacaoMaterialService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.cfg.Campo;
import br.com.sjc.modelo.cfg.CampoOperacaoMaterial;
import br.com.sjc.modelo.dto.CampoOperacaoDTO;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.persistencia.dao.CampoDAO;
import br.com.sjc.persistencia.dao.CampoOperacaoMaterialDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CampoOperacaoMaterialServiceImpl extends ServicoGenerico<Long, CampoOperacaoMaterial> implements CampoOperacaoMaterialService {

    @Autowired
    private CampoOperacaoMaterialDAO dao;

    @Autowired
    private CampoDAO campoDAO;

    @Override
    public void salvar(List<CampoOperacaoMaterial> lista) {

        lista.stream().forEach(item -> {

            item.setCampo(this.campoDAO.getOne(item.getIdCampo()));

            this.dao.save(item);
        });
    }

    @Override
    public Campo obterCampo(String variavel) {

        return campoDAO.findByDescricao(variavel);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CampoOperacaoMaterial> obterCamposPorMaterial(final Material material) {

        return dao.findByMaterialId(material.getId());
    }

    @Override
    public CampoOperacaoMaterialDAO getDAO() {

        return this.dao;
    }

    @Override
    public Class<? extends DataDTO> getDtoListagem() {

        return CampoOperacaoDTO.class;
    }
}
