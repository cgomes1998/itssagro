package br.com.sjc.service.romaneio.filters;

import br.com.sjc.fachada.servico.impl.ServiceInject;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.request.EventInRequest;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class TransferenciagGeraisFilters extends RomaneioFilters {
    public static final String KG = "KG";
    public static final String L = "L";

    @Override
    public JCoRequest instace(final Romaneio romaneio, final String centro, final String requestKey, final DadosSincronizacao dadosSincronizacao,
                              final String rfcProgramID) {

        return EventInRequest.instance(dadosSincronizacao, centro, rfcProgramID, this.getFilters(romaneio, requestKey, centro), false, null);
    }

    private List<EventInRequestItem> getFilters(final Romaneio romaneio, final String requestKey, final String centro) {

        final List<EventInRequestItem> filters = new LinkedList<EventInRequestItem>();

        int seq = 1;

        String pattern = ERM + centro;

        String requests[] = requestKey.split(pattern);

        String request = requests[0];

        int countRequest = 0;

        int codItem = 0;

        romaneio.setObservacaoNota(gerarObservacaoNota(romaneio));

        for (ItemNFPedido item : romaneio.getItens()) {

            codItem = codItem + 1;

            countRequest = countRequest + 10;

            String novaRequest = StringUtil.empty();

            Integer requestKeyDivisao = Integer.parseInt(request + countRequest);

            if (romaneio.getItens().size() == 1) {

                novaRequest = requestKey;

            } else {

                novaRequest = requestKeyDivisao + pattern + requests[1];
            }

            seq = this.montarBridgeParaUsuarioCriadorRomaneio(romaneio, novaRequest, filters, seq);
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_ROMAN.name(), String.valueOf(seq++), romaneio.getNumeroRomaneio()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_DATUM.name(), String.valueOf(seq++), DateUtil.formatToSAP(romaneio.getDataCadastro())));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_DTATUAL.name(), String.valueOf(seq++), DateUtil.formatToSAP(DateUtil.hoje())));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_WERKS.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(romaneio.getCentro()) ? romaneio.getCentro().getCodigo() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_OPERA.name(), String.valueOf(seq++), romaneio.getOperacao().getOperacao()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_LGORT.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(item.getDeposito()) ? item.getDeposito().getCodigo() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_MATNR.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(item.getMaterial()) ? item.getMaterial().getCodigo() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PEDIDO.name(), String.valueOf(seq++), StringUtil.isNotNullEmpty(item.getNumeroPedido()) ? item.getNumeroPedido() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_ITEMP.name(), String.valueOf(seq++), StringUtil.isNotNullEmpty(item.getItemPedido()) ? item.getItemPedido() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_ITEM.name(), String.valueOf(seq++), String.valueOf(countRequest / 10)));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_SAFRA.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(item.getSafra()) ? item.getSafra().getCodigo() : StringUtil.empty()));

            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_QUANT.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(item.getQuantidadeCarregar()) ? item.getQuantidadeCarregar().toString() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_UM.name(), String.valueOf(seq++), StringUtil.isNotNullEmpty(item.getUnidadeMedidaOrdem()) ? item.getUnidadeMedidaOrdem() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_TRANSP.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(item.getTransportadora()) ? item.getTransportadora().getCodigo() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_MOTORIST.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(item.getMotorista()) ? item.getMotorista().getCodigo() : StringUtil.empty()));
            if (ObjetoUtil.isNotNull(item.getPlacaCavalo())) {
                filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PL_CAV.name(), String.valueOf(seq++), StringUtil.toPlaca(item.getPlacaCavalo().getPlaca1())));
                if (ObjetoUtil.isNotNull(item.getPlacaCavalo().getUfPlaca1())) {
                    filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PLCAV_UF.name(), String.valueOf(seq++), String.format("%s %s", StringUtil.toPlaca(item.getPlacaCavalo().getPlaca1()), item.getPlacaCavalo().getUfPlaca1().getSigla())));
                    filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_UF.name(), String.valueOf(seq++), item.getPlacaCavalo().getUfPlaca1().getSigla()));
                }
                if (ObjetoUtil.isNotNull(item.getPlacaCavalo().getTipo())) {
                    filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_TIPOCAP.name(), String.valueOf(seq++), item.getPlacaCavalo().getTipo().getCodigo()));
                }
            }
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PL_CAR1.name(), String.valueOf(seq++), StringUtil.isNotNullEmpty(item.getPlacaCarretaUm()) ? StringUtil.removerCaracteresEspeciais(item.getPlacaCarretaUm()) : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PL_CAR2.name(), String.valueOf(seq++), StringUtil.isNotNullEmpty(item.getPlacaCarretaDois()) ? StringUtil.removerCaracteresEspeciais(item.getPlacaCarretaDois()) : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_TIPOFRE.name(), String.valueOf(seq++), ObjetoUtil.isNotNull(item.getTipoFrete()) ? item.getTipoFrete().name() : StringUtil.empty()));
            filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_NUMNFE.name(), String.valueOf(seq++), StringUtil.isNotNullEmpty(item.getNumeroNfe()) ? item.getNumeroNfe() : StringUtil.empty()));

            if (ObjetoUtil.isNotNull(romaneio.getPesagem())) {
                List<ConfiguracaoUnidadeMedidaMaterial> unidades = ServiceInject.sapService.obterMedidasParaConversao(item.getMaterial());

                final boolean rateio = romaneio.getItens().size() > 1;
                final BigDecimal pesoLiquido = romaneio.getPesagem().getPesoInicial().subtract(romaneio.getPesagem().getPesoFinal()).setScale(0, RoundingMode.HALF_UP);

                BigDecimal valor;
                if (rateio) {
                    valor = ServiceInject.sapService.converterQuantidade(unidades, KG, item.getUnidadeMedidaOrdem(), item.getRateio().doubleValue());
                } else {
                    valor = ServiceInject.sapService.converterQuantidade(unidades, KG, item.getUnidadeMedidaOrdem(), pesoLiquido.doubleValue());
                }
                if (rateio) {
                    filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PUMIDORT.name(), String.valueOf(seq++), item.getRateio().setScale(0, RoundingMode.HALF_UP) + StringUtil.empty()));
                } else {
                    filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PBRUTORT.name(), String.valueOf(seq++), romaneio.getPesagem().getPesoInicial().setScale(0, RoundingMode.HALF_UP) + StringUtil.empty()));
                    filters.add(EventInRequestItem.instance(novaRequest, SAPRomaneio.BRIDGES.C_PUMIDORT.name(), String.valueOf(seq++), pesoLiquido.setScale(0, RoundingMode.HALF_UP) + StringUtil.empty()));
                }

                if (rateio) {

                    seq = this.montarPesagemDestinoRateio(romaneio.getPesagem(), novaRequest, filters, seq, item, romaneio, valor);
                } else {
                    seq = this.montarPesagemDestino(romaneio, BigDecimal.ZERO, novaRequest, filters, seq);
                }
            }

            int grupo = 1;

            seq = this.montarObservacao(romaneio, novaRequest, filters, seq, grupo);

            grupo++;

            this.montarObservacaoNota(romaneio, novaRequest, filters, seq, grupo);

            adicionarRequestParaRomaneioComDivisao(romaneio, countRequest, novaRequest);
        }

        return filters;
    }

    private String gerarObservacaoNota(Romaneio romaneio) {

        StringBuilder sb = new StringBuilder();

        if (StringUtil.isNotNullEmpty(romaneio.getObservacaoNota()) && romaneio.getObservacaoNota().split(";").length > 1) {
            final String observacaoDigitadaPeloUsuario = romaneio.getObservacaoNota().split(";")[0];
            sb.append(MessageFormat.format("{0};\n", observacaoDigitadaPeloUsuario));
        }

        if (ColecaoUtil.isNotEmpty(romaneio.getAnalises())) {
            final Material material = romaneio.getItens().stream().filter(i -> ObjetoUtil.isNotNull(i.getMaterial())).map(ItemNFPedido::getMaterial).findFirst().orElse(null);
            Long tanque = null;
            if (ObjetoUtil.isNotNull(romaneio.getCarregamento()) && StringUtil.isNotNullEmpty(romaneio.getCarregamento().getTanque())) {
                tanque = Long.valueOf(romaneio.getCarregamento().getTanque());
            } else if (ObjetoUtil.isNotNull(romaneio.getConversaoLitragem()) && ObjetoUtil.isNotNull(romaneio.getConversaoLitragem().getTanque())) {
                tanque = romaneio.getConversaoLitragem().getTanque();
            }
            final String certificadoQualidade = material.isBuscarCertificadoQualidade() ? ServiceInject.laudoService.obterNumeroDoCertificadoPorMaterial(material.getId(), tanque) : romaneio.getAnalises().stream().map(i -> i.getId().toString()).collect(Collectors.joining(", ")).toString();
            sb.append(MessageFormat.format("Certificado de Qualidade: {0}\n", certificadoQualidade));
        }

        final Motorista motorista = romaneio.getItens().stream().filter(i -> ObjetoUtil.isNotNull(i.getMotorista())).map(ItemNFPedido::getMotorista).findFirst().orElse(null);
        if (ObjetoUtil.isNotNull(motorista)) {
            sb.append(MessageFormat.format("Motorista: {0}\n", motorista.getDescricaoNota()));
        }

        final Veiculo veiculo = romaneio.getItens().stream().filter(i -> ObjetoUtil.isNotNull(i.getPlacaCavalo())).map(ItemNFPedido::getPlacaCavalo).findFirst().orElse(null);
        if (ObjetoUtil.isNotNull(veiculo)) {
            sb.append(MessageFormat.format("Placa cavalo: {0}\n", StringUtil.toPlaca(veiculo.getPlaca1())));
        }

        final String placaCarretaUm = romaneio.getItens().stream().filter(i -> StringUtil.isNotNullEmpty(i.getPlacaCarretaUm())).map(ItemNFPedido::getPlacaCarretaUm).findFirst().orElse(null);
        if (StringUtil.isNotNullEmpty(placaCarretaUm)) {
            sb.append(MessageFormat.format("Placa carreta 1: {0}\n", StringUtil.toPlaca(placaCarretaUm)));
        }

        final String placaCarretaDois = romaneio.getItens().stream().filter(i -> StringUtil.isNotNullEmpty(i.getPlacaCarretaDois())).map(ItemNFPedido::getPlacaCarretaDois).findFirst().orElse(null);
        if (StringUtil.isNotNullEmpty(placaCarretaDois)) {
            sb.append(MessageFormat.format("Placa carreta 2: {0}\n", placaCarretaDois));
        }

        return sb.toString();
    }

}
