package br.com.sjc.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.PerfilService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.Perfil;
import br.com.sjc.modelo.PerfilFuncionalidade;
import br.com.sjc.modelo.PerfilOperacao;
import br.com.sjc.modelo.dto.PerfilDTO;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.persistencia.dao.PerfilDAO;
import br.com.sjc.persistencia.dao.PerfilFuncionalidadeDAO;
import br.com.sjc.persistencia.dao.PerfilOperacaoDAO;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class PerfilServiceImpl extends ServicoGenerico<Long, Perfil> implements PerfilService {

    @Autowired
    private PerfilDAO dao;

    @Autowired
    private PerfilFuncionalidadeDAO perfilFuncionalidadeDAO;

    @Autowired
    private PerfilOperacaoDAO perfilOperacaoDAO;

    @Override
    public List<Perfil> findByStatus(final StatusEnum status) {
        return this.dao.findByStatus(status);
    }

    @Override
    public void preSalvar(Perfil entidade) {

        this.perfilJaExiste(entidade);

        entidade.getFuncionalidades().forEach(i -> {
            i.setPerfil(entidade);
        });

        entidade.getOperacoes().forEach(i -> {
            i.setPerfil(entidade);
        });
    }

    private void perfilJaExiste(final Perfil entidade) {

        final Boolean isCadastro = entidade.getId() == null;

        if ((isCadastro && dao.existsByNome(entidade.getNome()))
                || (!isCadastro && dao.existsByNomeAndNeId(entidade.getNome(), entidade.getId()))) {

            throw new ServicoException("Já existe um perfil com o nome: " + entidade.getNome());
        }
    }

    @Override
    public Specification<Perfil> obterEspecification(final Paginacao<Perfil> paginacao) {

        final Perfil entidade = paginacao.getEntidade();

        final Specification<Perfil> specification = new Specification<Perfil>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<Perfil> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                if (StringUtil.isNotNullEmpty(entidade.getNome())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("nome")), "%" + entidade.getNome().toUpperCase() + "%")));
                }

                if (ObjetoUtil.isNotNull(entidade.getStatus())) {

                    predicates.add(builder.equal(root.get("status"), entidade.getStatus()));

                } else {

                    predicates.add(builder.equal(root.get("status"), StatusEnum.ATIVO));
                }

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Override
    public List<PerfilFuncionalidade> listarFuncionalidadesPorPerfilId(final Long id) {

        return this.perfilFuncionalidadeDAO.findByPerfilId(id);
    }

    @Override
    public List<PerfilOperacao> listarOperacoesPorPerfilId(final Long id) {

        return this.perfilOperacaoDAO.findByPerfilId(id);
    }

    @Override
    public Class<PerfilDTO> getDtoListagem() {
        return PerfilDTO.class;
    }

    @Override
    public DAO<Long, Perfil> getDAO() {

        return dao;
    }
}
