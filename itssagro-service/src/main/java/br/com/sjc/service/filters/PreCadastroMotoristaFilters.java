package br.com.sjc.service.filters;

import br.com.sjc.fachada.service.MotoristaService;
import br.com.sjc.fachada.servico.impl.ServiceInject;
import br.com.sjc.modelo.cfg.AmbienteSincronizacao;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.sap.Motorista;
import br.com.sjc.modelo.sap.Motorista_;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.request.EventInRequest;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;
import br.com.sjc.modelo.sap.sync.SAPMotorista;
import br.com.sjc.service.SAPServiceImpl;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.quartz.JobDataMap;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

public class PreCadastroMotoristaFilters extends PreCadastroFilters {

    private static final Logger LOGGER = Logger.getLogger(PreCadastroMotoristaFilters.class.getName());
    public static final String UM = "1";


    @Override
    public JCoRequest instace(
            JobDataMap jobDataMap,
            final String centro,
            final String requestKey,
            final DadosSincronizacaoEnum entidade,
            final DadosSincronizacao dadosSincronizacao,
            final String rfcProgramID,
            final String codigo,
            final AmbienteSincronizacao sincronizacao
    ) {

        try {

            final List<Motorista> entidades = ObjetoUtil.isNotNull(ServiceInject.motoristaService) ? ServiceInject.motoristaService.listarPreCadastro() : this.getMotoristaService(jobDataMap).listarPreCadastro();

            return ColecaoUtil.isNotEmpty(entidades) ? EventInRequest.instance(dadosSincronizacao, centro, rfcProgramID, this.getMotoristaFilters(jobDataMap, requestKey, entidades, sincronizacao, centro), false, entidades) : null;

        } catch (final Exception e) {

            PreCadastroMotoristaFilters.LOGGER.info("ERRO NA CRIACAO DE REQUEST PARA PRE-CADASTRO: " + e.getMessage());
        }

        return null;
    }

    private List<EventInRequestItem> getMotoristaFilters(
            JobDataMap jobDataMap,
            String requestKey,
            final List<Motorista> entidades,
            AmbienteSincronizacao sincronizacao,
            String centro
    ) throws Exception {

        final List<EventInRequestItem> filters = new LinkedList<EventInRequestItem>();

        if (ColecaoUtil.isNotEmpty(entidades)) {

            for (final Motorista motorista : entidades) {

                motorista.setRequestKey(requestKey);

                filters.add(EventInRequestItem.instance(requestKey, SAPMotorista.COLUMN_INPUT.PRE_CAD.name(), UM, UM, SAPServiceImpl.FLAG_SAP));
                filters.add(EventInRequestItem.instance(requestKey, SAPMotorista.COLUMN_INPUT.NOME_MOTORISTA.name(), "2", UM, motorista.getNome()));
                filters.add(EventInRequestItem.instance(requestKey, SAPMotorista.COLUMN_INPUT.CPF_MOTORISTA.name(), "3", UM, motorista.getCpf()));
                filters.add(EventInRequestItem.instance(requestKey, SAPMotorista.COLUMN_INPUT.CNH_MOTORISTA.name(), "4", UM, motorista.getCnh()));
                filters.add(EventInRequestItem.instance(requestKey, SAPMotorista.COLUMN_INPUT.RUA_MOTORISTA.name(), "5", UM, motorista.getRua()));
                filters.add(EventInRequestItem.instance(requestKey, SAPMotorista.COLUMN_INPUT.CEP_MOTORISTA.name(), "6", UM, motorista.getCep()));
                filters.add(EventInRequestItem.instance(requestKey, SAPMotorista.COLUMN_INPUT.CIDADE_MOTORISTA.name(), "7", UM, motorista.getMunicipio()));
                filters.add(EventInRequestItem.instance(requestKey, SAPMotorista.COLUMN_INPUT.FONE_MOTORISTA.name(), "8", UM, motorista.getTelefone()));
                filters.add(EventInRequestItem.instance(requestKey, SAPMotorista.COLUMN_INPUT.NRO_MOTORISTA.name(), "9", UM, motorista.getNumero()));
                filters.add(EventInRequestItem.instance(requestKey, SAPMotorista.COLUMN_INPUT.ID_MOTORISTA.name(), "10", UM, motorista.getId().toString()));
                filters.add(EventInRequestItem.instance(requestKey, SAPMotorista.COLUMN_INPUT.BAIRRO_MOTORISTA.name(), "11", UM, motorista.getBairro()));
                filters.add(EventInRequestItem.instance(requestKey, SAPMotorista.COLUMN_INPUT.REGIAO_MOTORISTA.name(), "12", UM, ObjetoUtil.isNotNull(motorista.getUf()) ? motorista.getUf().getSigla() : StringUtil.empty()));
                filters.add(EventInRequestItem.instance(requestKey, SAPMotorista.COLUMN_INPUT.FONE_MOTORISTA2.name(), "13", UM, motorista.getCelular()));

                if (ObjetoUtil.isNotNull(ServiceInject.motoristaService)) {

                    ServiceInject.motoristaService.alterarAtributos(motorista, Motorista_.REQUEST_KEY);

                } else {

                    this.getMotoristaService(jobDataMap).alterarAtributos(motorista, Motorista_.REQUEST_KEY);
                }

                getAmbienteSincronizadoService(jobDataMap).updateRequestCount(sincronizacao);

                sincronizacao = this.obterAmbienteSincronizacao(jobDataMap, sincronizacao.getDadosSincronizacao(), sincronizacao.getAmbiente());

                requestKey = EventInRequestItem.generateKey(sincronizacao.getRequestCount(), SAPServiceImpl.CHAMADA_EXTERNA_SAP, sincronizacao.getDadosSincronizacao().getModulo().getCodigo(), centro, DateUtil.getYear(DateUtil.hoje()));
            }
        }

        return filters;
    }

    public MotoristaService getMotoristaService(JobDataMap jobDataMap) {

        return (MotoristaService) jobDataMap.get("motoristaService");
    }

}
