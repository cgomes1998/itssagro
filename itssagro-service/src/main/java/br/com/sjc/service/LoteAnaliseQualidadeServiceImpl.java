package br.com.sjc.service;

import br.com.sjc.fachada.rest.paginacao.Ordenacao;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.AnaliseQualidadeService;
import br.com.sjc.fachada.service.LoteAnaliseQualidadeService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.AnaliseQualidade_;
import br.com.sjc.modelo.LoteAnaliseQualidade;
import br.com.sjc.modelo.LoteAnaliseQualidade_;
import br.com.sjc.modelo.dto.AnaliseQualidadeRomaneioLoteDTO;
import br.com.sjc.modelo.dto.LoteAnaliseQualidadeDTO;
import br.com.sjc.modelo.dto.LoteAnaliseQualidadeMotivoInativacaoDTO;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.relatorio.GerarRelatorioExcel;
import br.com.sjc.modelo.response.FileResponse;
import br.com.sjc.modelo.sap.Material_;
import br.com.sjc.persistencia.dao.LoteAnaliseQualidadeDAO;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class LoteAnaliseQualidadeServiceImpl extends ServicoGenerico<Long, LoteAnaliseQualidade> implements LoteAnaliseQualidadeService {

    @Autowired
    private LoteAnaliseQualidadeDAO dao;

    @Autowired
    private AnaliseQualidadeService analiseQualidadeService;

    @Override
    public LoteAnaliseQualidadeDAO getDAO() {

        return this.dao;
    }

    @Override
    public Class<LoteAnaliseQualidadeDTO> getDtoListagem() {

        return LoteAnaliseQualidadeDTO.class;
    }

    @Override
    public Specification<LoteAnaliseQualidade> obterEspecification(Paginacao<LoteAnaliseQualidade> paginacao) {

        final LoteAnaliseQualidade entidade = paginacao.getEntidade();

        final Specification<LoteAnaliseQualidade> specification = new Specification<LoteAnaliseQualidade>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<LoteAnaliseQualidade> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                if (StringUtil.isNotNullEmpty(entidade.getCodigo())) {

                    predicates.add(builder.and(builder.equal(root.get(LoteAnaliseQualidade_.codigo), entidade.getCodigo())));
                }

                if (ObjetoUtil.isNotNull(entidade.getDataFabricacao())) {

                    predicates.add(builder.between(root.get(LoteAnaliseQualidade_.dataFabricacao), DateUtil.obterDataHoraZerada(entidade.getDataFabricacao()), DateUtil.converterDateFimDia(entidade.getDataFabricacao())));
                }

                if (ObjetoUtil.isNotNull(entidade.getDataVencimento())) {

                    predicates.add(builder.between(root.get(LoteAnaliseQualidade_.dataVencimento), DateUtil.obterDataHoraZerada(entidade.getDataVencimento()), DateUtil.converterDateFimDia(entidade.getDataVencimento())));
                }

                if (ObjetoUtil.isNotNull(entidade.getMaterial())) {

                    predicates.add(builder.equal(root.join(LoteAnaliseQualidade_.material).get(Material_.id), entidade.getMaterial().getId()));
                }

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Transactional
    @Override
    public void alterarStatusLoteAnaliseQualidade(LoteAnaliseQualidade loteAnaliseQualidade) {

        loteAnaliseQualidade.setStatus(StatusEnum.ATIVO.equals(loteAnaliseQualidade.getStatus()) ? StatusEnum.INATIVO : StatusEnum.ATIVO);

        if (StatusEnum.ATIVO.equals(loteAnaliseQualidade.getStatus())) {

            loteAnaliseQualidade.setMotivoInativacao(null);
        }

        alterarAtributos(loteAnaliseQualidade, LoteAnaliseQualidade_.STATUS, LoteAnaliseQualidade_.MOTIVO_INATIVACAO);
    }

    @Override
    public LoteAnaliseQualidadeMotivoInativacaoDTO obterMotivoInativacaoDoLote(LoteAnaliseQualidade loteAnaliseQualidade) {

        Collection<LoteAnaliseQualidadeMotivoInativacaoDTO> lotes = dtoListar(((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(LoteAnaliseQualidade_.id), loteAnaliseQualidade.getId())),
                LoteAnaliseQualidadeMotivoInativacaoDTO.class);

        return lotes.stream().findFirst().get();
    }

    @Override
    public LoteAnaliseQualidade obterLote(LoteAnaliseQualidade entidadeFiltro) {

        return get(((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(criteriaBuilder.equal(root.get(LoteAnaliseQualidade_.codigo), entidadeFiltro.getCodigo()),
                criteriaBuilder.equal(root.join(LoteAnaliseQualidade_.material).get(Material_.id), entidadeFiltro.getMaterial().getId()))));
    }

    @Override
    public FileResponse obterRelatorioLotes(LoteAnaliseQualidade entidadeFiltro) {

        Paginacao paginacao = new Paginacao();

        paginacao.setEntidade(entidadeFiltro);

        paginacao.setListarTodos(true);

        paginacao.setSpecification(obterEspecification(paginacao));

        paginacao.setOrdenacao(Ordenacao.ordenacaoBuilder().campo(LoteAnaliseQualidade_.ID).ordenacao(Sort.Direction.DESC).build());

        Collection<LoteAnaliseQualidadeDTO> lotes = dtoListar(paginacao, LoteAnaliseQualidadeDTO.class).getContent();

        if (ColecaoUtil.isNotEmpty(lotes)) {

            Set<Long> lotesId = lotes.stream().map(LoteAnaliseQualidadeDTO::getId).collect(Collectors.toSet());

            Collection<AnaliseQualidadeRomaneioLoteDTO> analisesLotes =
                    analiseQualidadeService.dtoListar((root, criteriaQuery, criteriaBuilder) -> root.join(AnaliseQualidade_.loteAnaliseQualidade).get(LoteAnaliseQualidade_.id).in(lotesId), AnaliseQualidadeRomaneioLoteDTO.class);

            if (ColecaoUtil.isNotEmpty(analisesLotes)) {

                lotes.stream().forEach(lote -> lote.setRomaneios(analisesLotes.stream().filter(analiseQualidadeRomaneioLoteDTO -> analiseQualidadeRomaneioLoteDTO.getLoteAnaliseQualidade().getId().equals(lote.getId())).map(AnaliseQualidadeRomaneioLoteDTO::getRomaneio).collect(Collectors.toList())));

                return GerarRelatorioExcel.obterRelatorioLoteAnaliseQualidade(lotes);
            }
        }

        return null;
    }

}
