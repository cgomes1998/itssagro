package br.com.sjc.service.filters;

import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.sap.request.EventInRequest;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;
import br.com.sjc.modelo.sap.sync.SAPCentro;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CentroFilters extends Filters {

    @Override
    public EventInRequest instace(final String centro, final String requestKey, final DadosSincronizacaoEnum entidade, final DadosSincronizacao dadosSincronizacao, final String rfcProgramID, final String codigo, final Date utilmaSincronizacao) {

        return EventInRequest.instance(dadosSincronizacao, centro, rfcProgramID, this.getCentroFilters(requestKey, utilmaSincronizacao), false, null);
    }

    private List<EventInRequestItem> getCentroFilters(final String requestKey, final Date ultimaSincronizacao) {

        final Map<String, String> fields = new HashMap<String, String>();

        if (ObjetoUtil.isNotNull(ultimaSincronizacao)) {

            fields.put(SAPCentro.COLUMN_INPUT.DT_ULT_INF.name(), DateUtil.formatToSAP(ultimaSincronizacao));
        }

        return this.addFilters(requestKey, fields, "1");
    }

}
