package br.com.sjc.service;

import br.com.sjc.fachada.service.*;
import br.com.sjc.modelo.Fluxo;
import br.com.sjc.modelo.FluxoEtapa;
import br.com.sjc.modelo.FluxoMaterial;
import br.com.sjc.modelo.Senha;
import br.com.sjc.modelo.dto.*;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.enums.StatusSenhaEnum;
import br.com.sjc.modelo.enums.TipoParametroEnum;
import br.com.sjc.modelo.sap.Centro;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ItssAgroThreadPool;
import br.com.sjc.util.ObjetoUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
@Service
public class DashboardGerencialServiceImpl implements DashboardGerencialService {

    private FluxoEtapaService fluxoEtapaService;

    private FluxoService fluxoService;

    private CentroService centroService;

    private SenhaService senhaService;

    private RomaneioService romaneioService;

    private ParametroService parametroService;

    private SimpMessagingTemplate template;

    public DashboardGerencialServiceImpl(
            FluxoEtapaService fluxoEtapaService,
            FluxoService fluxoService,
            CentroService centroService,
            SenhaService senhaService,
            RomaneioService romaneioService,
            ParametroService parametroService,
            SimpMessagingTemplate template
                                        ) {

        this.fluxoEtapaService = fluxoEtapaService;
        this.fluxoService = fluxoService;
        this.centroService = centroService;
        this.senhaService = senhaService;
        this.romaneioService = romaneioService;
        this.parametroService = parametroService;
        this.template = template;
    }

    @Override
    public List<DashboardGerencialDTO> obterFilas(DashboardGerencialFiltroDTO filtro) {

        Date dataInicio = this.obterDataInicio();

        return filtro.getFluxos()
                     .stream()
                     .map(fluxo -> this.gerar(fluxo,
                              filtro.getMateriais(),
                              fluxo.getEtapas()
                                   .stream()
                                   .filter(fluxoEtapa -> !fluxoEtapa.isEtapaFinal())
                                   .collect(Collectors.toList()),
                              dataInicio
                     ))
                     .collect(Collectors.toList());
    }

    @Override
    public DashboardGerencialDTO obterFilas(DashboardGerencialFiltroEtapaDTO filtroEtapa) {

        return this.gerar(
                 filtroEtapa.getFluxo(),
                 filtroEtapa.getMateriais(),
                 filtroEtapa.getEtapas()
                            .stream()
                            .filter(fluxoEtapa -> !fluxoEtapa.isEtapaFinal())
                            .collect(Collectors.toList()),
                 this.obterDataInicio()
        );
    }

    @Override
    public void atualizarDashboard() {

        ItssAgroThreadPool.executarTarefa(() -> template.convertAndSend("/senha/dashboard-gerencial", Boolean.TRUE));
    }

    private Date obterDataInicio() {

        int horasValidadeSenha = Integer.parseInt(this.parametroService.obterPorTipoParametro(TipoParametroEnum.QUANTIDADE_HORAS_VALIDADE_SENHA).getValor());
        return Date.from(ZonedDateTime.now().minusHours(horasValidadeSenha).toInstant());
    }

    private DashboardGerencialDTO gerar(
            Fluxo fluxo,
            List<Material> materiais,
            List<FluxoEtapa> etapas,
            Date dataInicio
                                       ) {

        Date seteDias = Date.from(ZonedDateTime.now().minusDays(7).toInstant());

        List<Material> materiaisFiltro = ColecaoUtil.isNotEmpty(materiais) ? materiais : fluxo.getMateriais().stream().map(FluxoMaterial::getMaterial).collect(Collectors.toList());

        List<FluxoEtapaDTO> fluxoEtapas = etapas.stream().map(etapa -> new FluxoEtapaDTO(etapa.getId(), fluxo.getId(), etapa.getSequencia(), etapa.getEtapa(), fluxo.getTipoFluxo())).collect(Collectors.toList());

        List<SenhaDTO> senhas = this.senhaService.listarPorMaterialEPlacaENumeroRomaneioENumeroCartaoAcesso(materiaisFiltro, null, null, null, seteDias.before(dataInicio) ? seteDias : dataInicio, fluxoEtapas, Arrays.asList(StatusSenhaEnum.CANCELADA, StatusSenhaEnum.PENDENTE));

        DashboardGerencialDTO dto = new DashboardGerencialDTO();

        dto.setFluxo(new FluxoDTO(fluxo));

        dto.setSenhas(this.obterSenhas(senhas, dataInicio));

        dto.setUltimaChamado(dto.getSenhas().isEmpty() ? null : dto.getSenhas().get(0));

        dto.setEtapas(this.obterEtapas(etapas, dto.getSenhas().stream().map(DashboardGerencialSenhaDTO::getSenha).collect(Collectors.toList()), dataInicio));

        dto.setTotalFila(dto.getEtapas().stream().filter(etapaDTO -> !etapaDTO.isEtapaFinal()).mapToLong(DashboardGerencialEtapaDTO::getTotalFila).sum());

        return dto;
    }

    private List<DashboardGerencialSenhaDTO> obterSenhas(List<SenhaDTO> senhas, Date dataInicio) {

        List<DashboardGerencialSenhaDTO> list = senhas.stream().filter(s -> s.getDataCadastro().compareTo(dataInicio) >= 0).map(this::obterDashboardGerencialSenhaDTO).collect(Collectors.toList());

        return list.stream().filter(item ->
                                            (Arrays.asList(new EtapaEnum[]{EtapaEnum.DISTRIBUICAO_SENHA, EtapaEnum.CRIAR_ROMANEIO}).contains(item.getSenha().getEtapaAtual().getEtapa().getEtapa())
                                            || (ObjetoUtil.isNotNull(item.getRomaneio()) && !StatusRomaneioEnum.ESTORNADO.equals(item.getRomaneio().getStatusRomaneio())))
                                            && !EtapaEnum.SAIDA.equals(item.getSenha().getEtapaAtual().getEtapa().getEtapa()))
                   .collect(Collectors.toList());
    }

    private List<DashboardGerencialEtapaDTO> obterEtapas(List<FluxoEtapa> etapas, List<SenhaDTO> senhas, Date dataInicio) {

        List<Long> ids = senhas.stream().map(SenhaDTO::getId).collect(Collectors.toList());

        List<TempoPorEtapaDTO> tempoPorEtapaDTOS = ColecaoUtil.isNotEmpty(ids) && ColecaoUtil.isNotEmpty(etapas) ? senhaService.obterTempoMedioPorEtapaESenhas(ids, etapas) : null;

        return etapas.stream().map(etapa ->
                this.obterDashboardGerencialEtapaDTO(
                        etapa,
                        senhas.stream().filter(s -> s.getDataCadastro().compareTo(dataInicio) >= 0
                                && s.getEtapaAtual().getEtapa().getEtapa().equals(etapa.getEtapa())
                                && s.getEtapaAtual().getEtapa().getSequencia().equals(etapa.getSequencia())).count(), ColecaoUtil.isNotEmpty(ids)
                                && ColecaoUtil.isNotEmpty(etapas) ? tempoPorEtapaDTOS.stream()
                                .filter(t -> t.getEtapa().equals(etapa.getEtapa()) && t.getSequencia().equals(etapa.getSequencia())).mapToLong(TempoPorEtapaDTO::getDuracao).average()
                                .orElse(0) : Long.parseLong("0")
                )
        ).collect(Collectors.toList());
    }

    private DashboardGerencialEtapaDTO obterDashboardGerencialEtapaDTO(FluxoEtapa etapa, long totalFila, double duracaoMedia) {

        boolean eEtapaFinal = etapa.isEtapaFinal();

        DashboardGerencialEtapaDTO dto = new DashboardGerencialEtapaDTO();

        dto.setEtapa(etapa);

        dto.setEtapaFinal(eEtapaFinal);

        if (!eEtapaFinal) {

            dto.setTotalFila(totalFila);

            dto.setDuracaoMedia(duracaoMedia);
        }

        return dto;
    }

    private DashboardGerencialSenhaDTO obterDashboardGerencialSenhaDTO(SenhaDTO senha) {

        return new DashboardGerencialSenhaDTO(senha, this.romaneioService.obterPorSenha(senha.getId()), senha.getTotalDuracao());
    }

}
