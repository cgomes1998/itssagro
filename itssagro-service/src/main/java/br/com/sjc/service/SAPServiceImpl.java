package br.com.sjc.service;

import br.com.sjc.arquitetura.providers.RFCMultipleProviders;
import br.com.sjc.fachada.excecoes.*;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.*;
import br.com.sjc.fachada.servico.impl.ServiceInject;
import br.com.sjc.modelo.EntidadeGenerica;
import br.com.sjc.modelo.FluxoEtapa;
import br.com.sjc.modelo.cfg.Ambiente;
import br.com.sjc.modelo.cfg.AmbienteSincronizacao;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.dto.DetalheLogDTO;
import br.com.sjc.modelo.dto.LogIntegracaoDTO;
import br.com.sjc.modelo.dto.OrdemVendaDTO;
import br.com.sjc.modelo.dto.RequestItemDTO;
import br.com.sjc.modelo.enums.*;
import br.com.sjc.modelo.response.DadosNfeTransferenciaResponse;
import br.com.sjc.modelo.response.SaldoPedidoEToleranciaPedidoTransferenciaResponse;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoCallerService;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.request.*;
import br.com.sjc.modelo.sap.request.item.*;
import br.com.sjc.modelo.sap.response.*;
import br.com.sjc.modelo.sap.response.item.*;
import br.com.sjc.modelo.sap.sync.SAPSync;
import br.com.sjc.persistencia.dao.AmbienteDAO;
import br.com.sjc.persistencia.dao.DadosSincronizacaoDAO;
import br.com.sjc.service.filters.FiltersFactory;
import br.com.sjc.service.romaneio.filters.RomaneioFiltersFactory;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.bundle.MessageSupport;
import com.sap.conn.jco.*;
import com.sap.conn.jco.server.JCoServerContext;
import com.sap.conn.jco.server.JCoServerFunctionHandler;
import org.quartz.JobDataMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class SAPServiceImpl implements SAPService {

    public static final String CHAMADA_EXTERNA_SAP = "E";

    public static final String FLAG_SAP = "X";

    public static final String VAZIO = null;

    private static final String DOIS = "2";

    private static final String HUM = "1";

    private static final String MSG_OPERACAO_REALIZADA_COM_SUCESSO = "MSG001";

    private static final String MSG_ROMANEIO_NAO_POSSUI_INTEGRACAO_COM_SAP = "O ROMANEIO NÃO FOI POSSUI INTEGRAÇÃO COM O SAP";

    private static final String MSG_NAO_HOUVE_RETORNO_DO_SAP_PARA_ESSE_ROMANEIO = "NAO HOUVE RETORNO DO SAP PARA ESSE ROMANEIO";

    private static final String MSG_O_ROMANEIO_ENVIADO_COM_SUCESSO = "O ROMANEIO FOI ENVIADO COM SUCESSO";

    private static final String MSG_ROMANEIO_COM_ERRO_DE_SINCRONIZAÇÃO = "ROMANEIO COM ERRO DE SINCRONIZAÇÃO";

    private static final String MSG_PEDIDO_NAO_APROVADO = "MSGE014";

    private static final Logger LOGGER = Logger.getLogger(SAPServiceImpl.class.getName());

    public static final String KG = "KG";

    @Autowired
    private AmbienteDAO ambienteDAO;

    @Autowired
    private DadosSincronizacaoDAO dadosSincronizacaoDAO;

    @Autowired
    private RetornoNFeService retornoNFeService;

    @Autowired
    private RomaneioService romaneioService;

    @Autowired
    private BridgeService bridgeService;

    @Autowired
    private AmbienteSincronizacaoService ambienteSincronizacaoService;

    @Autowired
    private ParametroService parametroService;

    @Autowired
    private MaterialService materialService;

    @Autowired
    private CentroService centroService;

    @Autowired
    private SafraService safraService;

    @Autowired
    private TransportadoraService transportadoraService;

    @Autowired
    private ProdutorService produtorService;

    @Autowired
    private TipoVeiculoService tipoVeiculoService;

    @Autowired
    private MotoristaService motoristaService;

    @Autowired
    private VeiculoService veiculoService;

    @Autowired
    private TabelaClassificacaoService tabelaClassificacaoService;

    @Autowired
    private RomaneioLogEnvioService romaneioLogEnvioService;

    @Autowired
    private RomaneioRequestKeyService romaneioRequestKeyService;

    @Autowired
    private ContratoService contratoService;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private DepositoService depositoService;

    @Autowired
    private SenhaService senhaService;

    @Autowired
    private FluxoEtapaService fluxoEtapaService;

    @Override
    public void initRFCServer() {

        final List<Ambiente> ambienteSAPList = this.ambienteDAO.findAll();

        if (ColecaoUtil.isNotEmpty(ambienteSAPList)) {

            RFCMultipleProviders.start(
                    ambienteSAPList,
                    new JCoServerFunctionHandler() {

                        @Override
                        public void handleRequest(
                                final JCoServerContext context, final JCoFunction function
                        ) throws AbapException, AbapClassException {

                            final String mandante = context.getConnectionAttributes().getClient();

                            final String host = context.getServer().getGatewayHost();

                            final SAPParameter sincronizacao = (SAPParameter) (EventOutRequest.class.getDeclaredAnnotations()[0]);

                            if (function.getName().equals(sincronizacao.function())) {

                                SAPServiceImpl.this.retornarSucessoSAP(function);

                                SAPServiceImpl.this.procesarSincronizacao(
                                        function,
                                        mandante,
                                        host
                                );
                            }
                        }

                    },
                    new EventOutRequest()
            );

        } else {

            SAPServiceImpl.LOGGER.warning("NENHUM AMBIENTE ENCONTRADO NA BASE DO MIDDLEWARE");
        }
    }

    @Override
    public void realizarPing(final Ambiente ambienteLogado) throws Exception {

        final JCoDestination destination = JCoDestinationManager.getDestination(ambienteLogado.getHost() + "-" + ambienteLogado
                .getMandante());

        destination.ping();
    }

    private void retornarSucessoSAP(
            final JCoFunction function
    ) {

        final JCoTable jcoTable = function.getTableParameterList().getTable("CNRETURN");

        jcoTable.insertRow(0);

        jcoTable.setValue(
                "RETNR",
                SAPServiceImpl.DOIS
        );

        jcoTable.setValue(
                "DESCR",
                MessageSupport.getMessage(SAPServiceImpl.MSG_OPERACAO_REALIZADA_COM_SUCESSO)
        );
    }

    private void procesarSincronizacao(
            final JCoFunction function, final String mandante, final String host
    ) {

        final EventOutRequest request = new EventOutRequest();

        SAPServiceImpl.this.atualizarModelo(
                function,
                request
        );

        SAPServiceImpl.this.startThreadPersistirAtualizacao(
                mandante,
                host,
                request
        );
    }

    private void atualizarModelo(
            final JCoFunction function, final EventOutRequest request
    ) {

        try {

            RFCMultipleProviders.initRequestParameters(
                    function,
                    request
            );

        } catch (final Exception e) {

            SAPServiceImpl.LOGGER.warning(e.getMessage());
        }
    }

    private void startThreadPersistirAtualizacao(
            final String mandante, final String host, final EventOutRequest request
    ) {

        try {

            final Thread thread = new Thread(new Runnable() {

                @Override
                public void run() {

                    SAPServiceImpl.this.persistirEntidades(
                            request,
                            mandante,
                            host
                    );
                }
            });

            thread.start();

            Thread.sleep(1000);

        } catch (InterruptedException e) {

            SAPServiceImpl.LOGGER.warning("THREAD EM ESPERA POR 1 SEGUNDO.");
        }
    }

    @Override
    public void enviarCadastro(final DadosSincronizacaoEnum entidade, JobDataMap jobDataMap) {

        final EventInResponse response = new EventInResponse();

        final Map<Long, JCoCallerService> connectionsMap = RFCMultipleProviders.DESTINATIONS;

        if (ObjetoUtil.isNotNull(connectionsMap)) {

            final DadosSincronizacao dadosSincronizacao = this.dadosSincronizacaoDAO.findByEntidadeAndModulo(entidade, ModuloOperacaoEnum.DADOS_MESTRES);

            connectionsMap.keySet().forEach(key -> {

                final JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.get(key);

                if (ObjetoUtil.isNotNull(sap)) {

                    try {

                        final AmbienteSincronizacao sincronizacao = this.ambienteSincronizacaoService.obterAmbienteSincronizacao(dadosSincronizacao, sap);

                        final EventInRequest request = this.createRequests(jobDataMap, entidade, dadosSincronizacao,
                                sap.getRfcProgramID(), sincronizacao);

                        if (ObjetoUtil.isNotNull(request)) {

                            sap.call(request, response);

                            if (ObjetoUtil.isNotNull(response) && this.verificarRespostaRecebimento(response)) {

                                this.atualizarStatusCadastro(request.getEntidades(), entidade);
                            }
                        }

                    } catch (final Exception e) {

                        SAPServiceImpl.LOGGER.info(MessageFormat.format(
                                "ERRO NO ENVIO DO PRÉ-CADASTRO {0} AO SAP: " + e.getMessage(),
                                entidade.name()
                        ));
                    }
                }
            });
        }
    }

    @Override
    public List<LogIntegracaoDTO> obterEtapasDoRomaneio(final String numeroRomaneio) {

        final RfcEtapasRomaneioRequest request = new RfcEtapasRomaneioRequest();

        request.setNumeroRomaneio(numeroRomaneio);

        final RfcEtapasRomaneioResponse response = new RfcEtapasRomaneioResponse();

        final Map<Long, JCoCallerService> connectionsMap = RFCMultipleProviders.DESTINATIONS;

        if (ObjetoUtil.isNotNull(connectionsMap)) {

            final JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.get(connectionsMap.keySet()
                    .stream()
                    .findFirst()
                    .get());

            sap.call(
                    request,
                    response
            );

            return this.preencherRetornoEtapas(response);
        }

        return null;
    }

    private List<LogIntegracaoDTO> preencherRetornoEtapas(RfcEtapasRomaneioResponse response) {

        if (ObjetoUtil.isNotNull(response)) {

            final List<TableFlowOut> cabecalho = response.getFlowOuts();

            final Set<String> setList = new TreeSet<String>();

            final List<LogIntegracaoDTO> logs = new ArrayList<LogIntegracaoDTO>();

            cabecalho.forEach(item -> {
                setList.add(item.getChave());
            });

            AtomicInteger idRaiz = new AtomicInteger(0);

            setList.forEach(chave -> {

                final LogIntegracaoDTO raiz = new LogIntegracaoDTO();

                raiz.setId(new Long(idRaiz.incrementAndGet()));

                raiz.setChave(chave);

                raiz.setSubLogs(new ArrayList<LogIntegracaoDTO>());

                AtomicInteger idLog = new AtomicInteger(0);

                cabecalho.stream().filter(item -> chave.equals(item.getChave())).forEach(item -> {

                    raiz.setProdutor(item.getProdutor());

                    final LogIntegracaoDTO log = new LogIntegracaoDTO();

                    log.setId(new Long(idLog.incrementAndGet()));

                    log.setDenominacao(item.getDenominacao());

                    log.setEtapa(item.getEtapa().trim());

                    log.setStatus(StatusIntegracaoEnum.get(item.getStatus()));

                    final List<TableLogReturn> detalhes = response.getLogReturns();

                    log.setDetalhes(new ArrayList<>());

                    AtomicInteger idDetalhe = new AtomicInteger(0);

                    detalhes.stream().filter(d -> d.getEtapa().trim().equals(log.getEtapa())).forEach(d -> {

                        final DetalheLogDTO detalhe = new DetalheLogDTO();

                        detalhe.setId(new Long(idDetalhe.getAndIncrement()));

                        detalhe.setEtapa(d.getEtapa().trim());

                        detalhe.setMensagem(d.getMensagem());

                        detalhe.setStatus("S".equals(d.getTipoMensagem()) ? StatusIntegracaoEnum.A : "E".equals(d.getTipoMensagem()) ? StatusIntegracaoEnum.E : null);

                        log.getDetalhes().add(detalhe);
                    });

                    log.setDetalhes(new ArrayList<DetalheLogDTO>(log.getDetalhes()));

                    Collections.sort(log.getDetalhes());

                    raiz.getSubLogs().add(log);
                });

                Collections.sort(raiz.getSubLogs());

                logs.add(raiz);
            });

            Collections.sort(
                    logs,
                    (l1, l2) -> l1.getChave().compareTo(l2.getChave())
            );

            return logs;
        }
        return null;
    }

    private boolean verificarRespostaRecebimento(final EventInResponse response) {

        if (ColecaoUtil.isNotEmpty(response.getRetorno())) {

            return response.getRetorno().stream().filter(item -> item.getCodReturn().trim().equals(SAPServiceImpl.DOIS)).count() > 0;
        }

        return false;
    }

    protected void persistirEntidades(final EventOutRequest request, final String mandante, final String host) {

        DadosSincronizacao dadosSincronizacao = new DadosSincronizacao();

        try {

            dadosSincronizacao = ServiceInject.dadosSincronizacaoService.obterPorModuloEOperacao(ModuloOperacaoEnum.get(request.getModulo()), request.getOperacao());

        } catch (Exception e) {

            SAPServiceImpl.LOGGER.info(MessageFormat.format(
                    "ERRO AO OBTER DADOS SINCRONIZAÇÃO PARA MODULO {0} E OPERACAO {1}",
                    ModuloOperacaoEnum.get(request.getModulo()),
                    request.getOperacao()
            ));
        }

        if (ObjetoUtil.isNotNull(dadosSincronizacao) && ObjetoUtil.isNotNull(dadosSincronizacao.getEntidade())) {

            SAPServiceImpl.LOGGER.info(StringUtil.log("------ VERIFICANDO O DADO MESTRE ----------- " + dadosSincronizacao.getEntidade().name()));

            try {
                switch (dadosSincronizacao.getEntidade()) {
                    case SAFRA:
                        this.safraService.salvarSAP(request, host, mandante);
                        break;
                    case CENTRO:
                        this.centroService.salvarSAP(request, host, mandante);
                        break;
                    case MATERIAL:
                        this.materialService.salvarSAP(request, host, mandante);
                        break;
                    case VEICULO:
                        this.veiculoService.salvarSAP(request, host, mandante);
                        break;
                    case MOTORISTA:
                        this.motoristaService.salvarSAP(request, host, mandante);
                        break;
                    case TRANSPORTADORA:
                        this.transportadoraService.salvarSAP(request, host, mandante);
                        break;
                    case TIPO_VEICULO:
                        this.tipoVeiculoService.salvarSAP(request, host, mandante);
                        break;
                    case TABELA_CLASSIFICACAO:
                        this.tabelaClassificacaoService.salvarSAP(request, host, mandante);
                        break;
                    default: {
                        synchronized (this) {
                            final Romaneio romaneio = this.romaneioService.obterPorNumeroRomaneio(request.getNumeroRomaneio());
                            if (ObjetoUtil.isNotNull(romaneio)) {
                                this.obterRespostaRomaneio(romaneio, request, dadosSincronizacao.getEntidade());
                                final RomaneioLogEnvio romaneioLogEnvio = this.romaneioLogEnvioService.obterPorRomaneioId(romaneio.getId());
                                this.alterarLogEnvio(romaneioLogEnvio, romaneio);
                            }
                        }
                        break;
                    }
                }
            } catch (final Exception e) {
                LOGGER.log(
                        Level.SEVERE,
                        "ERRO AO PERSISITIR DADOS SINCRONIZADOS DO SAP NA BASE DE DADOS LOCAL " + e.getMessage()
                );
            }
        }
    }

    private void alterarLogEnvio(final RomaneioLogEnvio romaneioLogEnvio, final Romaneio romaneio) {

        romaneioLogEnvio.setDataResposta(new Date());

        romaneioLogEnvio.setRomaneio(romaneio);

        this.romaneioLogEnvioService.getDAO().save(romaneioLogEnvio);
    }

    private void obterRespostaRomaneio(final Romaneio romaneio, final EventOutRequest request, final DadosSincronizacaoEnum entidade) {

        if (ObjetoUtil.isNotNull(romaneio)) {

            try {

                this.preencherResposta(romaneio, request);

                this.preencherLogRomaneio(romaneio, request);

                this.romaneioService.atualizarSaldoReservadoPedido(romaneio);

                this.romaneioService.getDAO().save(romaneio);

            } catch (Exception e) {

                SAPServiceImpl.LOGGER.info(MessageFormat.format("ERRO AO OBTER RESPOSTA DO ROMANEIO {0}, {1}", romaneio.getNumeroRomaneio(), e.getMessage()));
            }

        } else {

            SAPServiceImpl.LOGGER.info("ENTIDADE " + entidade.name() + " NÃO MAPEADA");
        }
    }

    private void preencherResposta(Romaneio romaneio, EventOutRequest request) {

        if (ColecaoUtil.isNotEmpty(request.getData())) {

            for (EventOutRequestItem data : request.getData()) {

                final EnumRespostaRomaneio resposta = EnumRespostaRomaneio.obterValor(data.getCampo());

                if (ObjetoUtil.isNotNull(resposta)) {

                    SAPServiceImpl.LOGGER.info(resposta + ": " + data.getValor());

                    switch (resposta) {

                        case NR_PEDIDO: {

                            romaneio.setNumeroPedido(data.getValor().trim());

                            break;
                        }

                        case NR_CONTRATO: {

                            romaneio.setNumeroContrato(data.getValor().trim());

                            break;
                        }
                    }
                }
            }
        }
    }

    private void preencherLogRomaneio(final Romaneio romaneio, final EventOutRequest request) {

        romaneio.setStatusRomaneio(StatusRomaneioEnum.CONCLUIDO);

        if (ObjetoUtil.isNotNull(request)) {

            if (!StringUtil.isEmpty(request.getStatusSAP())) {

                romaneio.setStatusIntegracaoSAP(StatusIntegracaoSAPEnum.get(Integer.valueOf(request.getStatusSAP())));
            }

            if (!StatusIntegracaoSAPEnum.COMPLETO.equals(romaneio.getStatusIntegracaoSAP()) && !StatusIntegracaoSAPEnum.ESTORNADO.equals(romaneio.getStatusIntegracaoSAP())) {

                romaneio.setPermitirExecutarFluxo(Boolean.TRUE);

            } else {

                if (StatusIntegracaoSAPEnum.ESTORNADO.equals(romaneio.getStatusIntegracaoSAP())) {

                    romaneioService.estornar(romaneio, false);

                } else if (StatusIntegracaoSAPEnum.COMPLETO.equals(romaneio.getStatusIntegracaoSAP())) {

                    romaneio.setStatusIntegracaoSAP(StatusIntegracaoSAPEnum.COMPLETO);

                    romaneioService.inicializarLacreStatusLacre(romaneio);
                }

                romaneio.setPermitirExecutarFluxo(Boolean.FALSE);
            }
        }
    }

    private void atualizarStatusCadastro(
            final List<? extends EntidadeGenerica> entidades, final DadosSincronizacaoEnum entidade
    ) {

        if (ColecaoUtil.isNotEmpty(entidades)) {

            final String logSAP = "{0} ENVIADO AO SAP EM " + DateUtil.format(
                    "dd/MM/yyyy HH:mm:ss",
                    DateUtil.hoje()
            );

            switch (entidade) {

                case VEICULO: {

                    final List<Veiculo> veiculos = (List<Veiculo>) entidades;

                    veiculos.stream().forEach(veiculo -> {

                        veiculo.setStatusCadastroSap(StatusCadastroSAPEnum.ENVIADO);

                        veiculo.setLogSAP(MessageFormat.format(
                                logSAP,
                                "VEÍCULO"
                        ));

                        this.veiculoService.getDAO().save(veiculo);
                    });

                    break;
                }

                case MOTORISTA: {

                    final List<Motorista> motoristas = (List<Motorista>) entidades;

                    motoristas.stream().forEach(motorista -> {

                        motorista.setStatusCadastroSap(StatusCadastroSAPEnum.ENVIADO);

                        motorista.setLogSAP(MessageFormat.format(
                                logSAP,
                                "MOTORISTA"
                        ));

                        this.motoristaService.getDAO().save(motorista);
                    });

                    break;
                }
            }
        }
    }

    @Override
    public void atualizarRomaneios(Long idRomaneio, Date dataUltimaSincronizacao) {

        final Map<Long, JCoCallerService> connectionsMap = RFCMultipleProviders.DESTINATIONS;

        if (ObjetoUtil.isNotNull(connectionsMap)) {

            final Long key = connectionsMap.keySet().stream().findFirst().get();

            final JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.get(key);

            if (ObjetoUtil.isNotNull(sap)) {

                RfcStatusRomaneioRequest request = new RfcStatusRomaneioRequest();

                List<Long> idsRomaneios = ObjetoUtil.isNotNull(idRomaneio) ? Arrays.asList(idRomaneio) : this.romaneioService.listarIdDeRomaneiosIntegradosComSAP(dataUltimaSincronizacao);

                if (ColecaoUtil.isNotEmpty(idsRomaneios)) {

                    request.setRomaneios(this.romaneioService.listarRequestKeysDeRomaneiosPorId(idsRomaneios));
                }

                RfcStatusRomaneioResponse response = new RfcStatusRomaneioResponse();

                if (ColecaoUtil.isNotEmpty(request.getRomaneios())) {

                    sap.call(request, response);

                    atualizarPedidosENotasEStatusRomaneio(
                            response,
                            ColecaoUtil.isNotEmpty(idsRomaneios) && idsRomaneios.size() > 1,
                            idsRomaneios
                    );

                    obterFaturas(idsRomaneios);
                }
            }
        }
    }

    private void obterFaturas(List<Long> idsRomaneios) {

        List<RequestItemDTO> requests = romaneioService.obterRequestsSemFaturas(idsRomaneios);

        if (ColecaoUtil.isNotEmpty(requests)) {

            final Map<Long, JCoCallerService> connectionsMap = RFCMultipleProviders.DESTINATIONS;

            if (ObjetoUtil.isNotNull(connectionsMap)) {

                final Long key = connectionsMap.keySet().stream().findFirst().get();

                final JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.get(key);

                requests.stream().forEach(requestKey -> {

                    RfcObterFaturaRequest request = new RfcObterFaturaRequest(
                            "RM",
                            requestKey.getOperacao(),
                            requestKey.getRequestKey()
                    );

                    RfcObterFaturaResponse response = new RfcObterFaturaResponse();

                    sap.call(
                            request,
                            response
                    );

                    if (ObjetoUtil.isNotNull(response) && StringUtil.isNotNullEmpty(response.getFatura())) {

                        romaneioService.updateDocumentoFaturamento(
                                new Long(response.getFatura()),
                                requestKey.getRequestKey(),
                                requestKey.getNumeroRomaneio()
                        );
                    }
                });
            }
        }
    }

    private void atualizarPedidosENotasEStatusRomaneio(
            RfcStatusRomaneioResponse response, boolean realizarBuscaParaNotas, List<Long> ids
    ) {

        new Thread() {

            @Override
            public void run() {

                atualizarPedidosDeComprasRomaneios(response);
            }

        }.start();

        new Thread() {

            @Override
            public void run() {

                atualizarStatusRomaneios(response);
            }

        }.start();

        new Thread() {

            @Override
            public void run() {

                atualizarNotasFiscaisRomaneios(
                        response,
                        realizarBuscaParaNotas,
                        ids
                );
            }

        }.start();
    }

    private void atualizarPedidosDeComprasRomaneios(RfcStatusRomaneioResponse response) {

        if (ColecaoUtil.isNotEmpty(response.getTStatusPed())) {

            response.getTStatusPed().stream().forEach(i -> {

                if (StringUtil.isNotNullEmpty(i.getNumeroPedido()) && StringUtil.isNotNullEmpty(i.getCodigoStatus())) {

                    this.romaneioService.atualizarStatusPedido(
                            i.getRomaneio(),
                            new Long(i.getNumeroPedido().trim()).toString(),
                            StatusPedidoEnum.obterStatusPorCodigo(Integer.valueOf(i.getCodigoStatus()))
                    );
                }
            });
        }
    }

    private void atualizarNotasFiscaisRomaneios(
            RfcStatusRomaneioResponse response, boolean realizarBuscaParaNotas, List<Long> ids
    ) {

        RfcStatusRomaneioResponse novoResponse = realizarBuscaParaNotas ? new RfcStatusRomaneioResponse() : response;

        if (realizarBuscaParaNotas) {

            final Map<Long, JCoCallerService> connectionsMap = RFCMultipleProviders.DESTINATIONS;
            final Long key = connectionsMap.keySet().stream().findFirst().get();
            final JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.get(key);

            RfcStatusRomaneioRequest request = new RfcStatusRomaneioRequest();

            request.setRomaneios(romaneioService.obterRequestsDeRomaneiosSemNota(ids));

            sap.call(request, novoResponse);
        }

        if (ColecaoUtil.isNotEmpty(novoResponse.getTActive())) {

            Collections.reverse(novoResponse.getTActive());
    
            List<Romaneio> romaneios = this.romaneioService.obterRomaneioProjetandoApenasIdEOperacao(novoResponse.getTActive()
                    .stream().map(RfcStatusNfeItem::getRomaneio).collect(Collectors.toSet()));
            List<RetornoNFe> notas = this.retornoNFeService.obterEntidadeProjetantoApenasId(novoResponse.getTActive()
                    .stream().map(RfcStatusNfeItem::getNfe).collect(Collectors.toSet()));

            List<RetornoNFe> listaRetornoNfe = novoResponse.getTActive().stream().filter(i ->
                            StringUtil.isNotNullEmpty(i.getNfe()) && StringUtil.isNotNullEmpty(i.getSerieNfe()))
                    .map(i -> {
    
                        RetornoNFe retornoNFe = notas.stream().filter(item ->
                                        (StringUtil.isNotNullEmpty(item.getNfe()) && item.getNfe().equals(i.getNfe()))
                                                || (StringUtil.isNotNullEmpty(item.getNfeTransporte()) && item.getNfeTransporte().equals(i.getNfe())))
                                .findFirst().orElse(new RetornoNFe());
                        
                        romaneios.stream().filter(item -> item.getNumeroRomaneio().equals(i.getRomaneio())).findFirst().ifPresent(romaneio -> {
    
                            if (DadosSincronizacaoEnum.NF_TRANSPORTE.equals(romaneio.getOperacao().getEntidade())) {
                                retornoNFe.setNfeTransporte(i.getNfe().trim());
                            } else {
                                retornoNFe.setNfe(i.getNfe().trim());
                            }
    
                            if (ObjetoUtil.isNull(retornoNFe.getDataCadastro())) {
                                retornoNFe.setDataCadastro(new Date());
                            }
    
                            if (ObjetoUtil.isNotNull(i.getDataCriacao()) && ObjetoUtil.isNotNull(i.getHoraCriacao())) {
                                retornoNFe.setDataCriacaoNotaMastersaf(DateUtil.setarHora(i.getDataCriacao(), i.getHoraCriacao()));
                            }
    
                            retornoNFe.setSerie(i.getSerieNfe().trim());
                            retornoNFe.setStatus(i.getStatus());
                            retornoNFe.setRequestKey(i.getChave());
                            retornoNFe.setRomaneio(romaneio);
                        });

                        return retornoNFe;
                    }).collect(Collectors.toList());

            retornoNFeService.salvarListaRetornoNfe(listaRetornoNfe);
        }
    }

    private void atualizarStatusRomaneios(RfcStatusRomaneioResponse response) {

        if (ColecaoUtil.isNotEmpty(response.getTStatus())) {

            response.getTStatus().stream().forEach(i -> {

                if (ObjetoUtil.isNotNull(i.getStatus())) {

                    StatusIntegracaoSAPEnum statusIntegracaoSAP = StatusIntegracaoSAPEnum.get(i.getStatus());

                    this.romaneioRequestKeyService.atualizarStatusSAPRomaneioPorOperacaoERequest(
                            i.getChave(),
                            i.getOperacao(),
                            statusIntegracaoSAP
                    );

                    if (StatusIntegracaoSAPEnum.ESTORNADO.equals(statusIntegracaoSAP)) {

                        RomaneioRequestKey romaneioRequestKey = romaneioRequestKeyService.obterPorRequestEOperacao(i.getChave(), i.getOperacao());

                        romaneioService.estornar(romaneioRequestKey.getRomaneio(), false);
                    }
                }
            });
        }
    }

    @Override
    public void sincronizarRFC(final DadosSincronizacaoEnum entidade, Date ultimaSincronizacao) {

        final Map<Long, JCoCallerService> connectionsMap = RFCMultipleProviders.DESTINATIONS;

        if (ObjetoUtil.isNotNull(connectionsMap)) {

            connectionsMap.keySet().forEach(key -> {

                final JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.get(key);

                if (ObjetoUtil.isNotNull(sap)) {

                    switch (entidade) {

                        case DADOS_CONTRATO: {

                            final RfcDadosContratoResponse response = new RfcDadosContratoResponse();
                            final RfcDadosContratoRequest request = new RfcDadosContratoRequest();

                            sap.call(request, response);

                            contratoService.salvarTipoContrato(response.getTiposContrato()
                                    .stream()
                                    .map(RfcTiposContratoItem::toEntidade)
                                    .collect(Collectors
                                            .toList()));
                            contratoService.salvarCondicaoPagamento(response.getCondicoesPagamento()
                                    .stream()
                                    .map(RfcCondicoesPagamentoItem::toEntidade)
                                    .collect(Collectors
                                            .toList()));
                            contratoService.salvarGrupoCompradores(response.getGruposComrpadores()
                                    .stream()
                                    .map(RfcGruposCompradoresItem::toEntidade)
                                    .collect(Collectors
                                            .toList()));
                            contratoService.salvarOrganizacaoCompras(response.getOrganizacoesCompra()
                                    .stream()
                                    .map(RfcOrganizacoesCompraItem::toEntidade)
                                    .collect(Collectors
                                            .toList()));
                            break;
                        }

                        case MOTORISTA: {

                            final RfcMotoristaResponse response = new RfcMotoristaResponse();
                            RfcMotoristaRequest request = new RfcMotoristaRequest();

                            if (ObjetoUtil.isNotNull(ultimaSincronizacao)) {
                                request.setDataUltimaIntegracao(DateUtil.formatToSAP(ultimaSincronizacao));
                            }

                            sap.call(request,response);

                            if (ColecaoUtil.isNotEmpty(response.getMensagens())) {
                                response.getMensagens().forEach(mensagem -> SAPServiceImpl.LOGGER.info(mensagem.getLinha()));
                            }

                            motoristaService.salvarSAP(response.getRetorno());

                            break;
                        }

                        case VEICULO: {

                            final RfcVeiculoResponse response = new RfcVeiculoResponse();
                            RfcVeiculoRequest request = new RfcVeiculoRequest();

                            if (ObjetoUtil.isNotNull(ultimaSincronizacao)) {
                                request.setDataUltimaIntegracao(DateUtil.formatToSAP(ultimaSincronizacao));
                            }

                            sap.call(request,response);

                            veiculoService.salvarSAP(response.getRetorno());

                            break;
                        }

                        case CLIENTE: {

                            final RfcClienteResponse response = new RfcClienteResponse();
                            RfcClienteRequest request = new RfcClienteRequest();

                            if (ObjetoUtil.isNotNull(ultimaSincronizacao)) {
                                request.setDataUltimaIntegracao(DateUtil.formatToSAP(ultimaSincronizacao));
                                sap.call(request, response);
                                clienteService.salvarSAP(response.getClientes());
                            }
                            break;
                        }
                    }
                }
            });
        }
    }


    @Override
    public void sincronizar(final DadosSincronizacaoEnum entidade, Date ultimaSincronizacao) {

        final Map<Long, JCoCallerService> connectionsMap = RFCMultipleProviders.DESTINATIONS;

        if (ObjetoUtil.isNotNull(connectionsMap)) {

            final DadosSincronizacao dadosSincronizacao = this.dadosSincronizacaoDAO.findByEntidadeAndModulo(entidade, ModuloOperacaoEnum.DADOS_MESTRES);

            connectionsMap.keySet().forEach(key -> {

                switch (entidade) {

                    case PRODUTOR: {
                        final RfcProdutorResponse response = new RfcProdutorResponse();
                        final RfcProdutorRequest request = new RfcProdutorRequest();

                        if (ObjetoUtil.isNotNull(ultimaSincronizacao)) {
                            request.setDataUltimaIntegracao(DateUtil.formatToSAP(ultimaSincronizacao));
                        }

                        final JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.get(key);
                        sap.call(request, response);

                        produtorService.salvarSAP(response.getRetorno());
                        break;
                    }

                    case MOTORISTA: {

                        final RfcMotoristaResponse response = new RfcMotoristaResponse();
                        RfcMotoristaRequest request = new RfcMotoristaRequest();

                        if (ObjetoUtil.isNotNull(ultimaSincronizacao)) {
                            request.setDataUltimaIntegracao(DateUtil.formatToSAP(ultimaSincronizacao));
                        }

                        final JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.get(key);
                        sap.call(request, response);

                        if (ColecaoUtil.isNotEmpty(response.getMensagens())) {
                            response.getMensagens().forEach(mensagem -> SAPServiceImpl.LOGGER.info(mensagem.getLinha()));
                        }

                        motoristaService.salvarSAP(response.getRetorno());

                        break;
                    }

                    case VEICULO: {

                        final RfcVeiculoResponse response = new RfcVeiculoResponse();
                        RfcVeiculoRequest request = new RfcVeiculoRequest();

                        if (ObjetoUtil.isNotNull(ultimaSincronizacao)) {
                            request.setDataUltimaIntegracao(DateUtil.formatToSAP(ultimaSincronizacao));
                        }

                        final JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.get(key);
                        sap.call(request, response);

                        veiculoService.salvarSAP(response.getRetorno());

                        break;
                    }

                    default: {

                        final JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.get(key);

                        if (ObjetoUtil.isNotNull(sap)) {

                            final EventInResponse response = new EventInResponse();

                            final AmbienteSincronizacao sincronizacao = this.ambienteSincronizacaoService.obterAmbienteSincronizacao(dadosSincronizacao, sap);

                            if (ObjetoUtil.isNotNull(sincronizacao)) {

                                final String centro = this.parametroService.obterPorTipoParametro(TipoParametroEnum.CENTRO_SAP).getValor();

                                final String requestKey = EventInRequestItem.generateKey(sincronizacao.getRequestCount(),
                                        SAPServiceImpl.CHAMADA_EXTERNA_SAP, dadosSincronizacao.getModulo().getCodigo(),
                                        centro, DateUtil.getYear(DateUtil.hoje()));

                                sap.call(this.createRequests(centro, requestKey, entidade, dadosSincronizacao, sap.getRfcProgramID(),
                                                StringUtil.empty(), ObjetoUtil.isNull(ultimaSincronizacao) ? null : DateUtil.obterDataHoraZerada(ultimaSincronizacao)), response);

                                sincronizacao.setUltimaSincronizacao(new Date());

                                this.ambienteSincronizacaoService.updateRequestCount(sincronizacao);

                                SAPServiceImpl.LOGGER.info(StringUtil.log("SOLICITAÇÃO DE SINCRONIZAÇÃO ENVIADA PARA " + entidade.name() + " || REQ: " + requestKey));
                            }
                        }

                        break;
                    }
                }
            });
        }
    }

    private EventInRequest createRequests(
            final String centro,
            final String requestKey,
            final DadosSincronizacaoEnum entidade,
            final DadosSincronizacao dadosSincronizacao,
            final String rfcProgramID,
            final String codigo,
            final Date ultimaSincronizacao
    ) {

        if (ObjetoUtil.isNotNull(dadosSincronizacao)) {

            return (EventInRequest) FiltersFactory.create(
                    centro,
                    requestKey,
                    entidade,
                    dadosSincronizacao,
                    rfcProgramID,
                    codigo,
                    ultimaSincronizacao
            );
        }

        SAPServiceImpl.LOGGER.info("ENTIDADE" + entidade.name() + " NÃO MAPEADA");

        return null;
    }

    private <T extends EntidadeGenerica> EventInRequest createRequests(
            JobDataMap jobDataMap,
            final DadosSincronizacaoEnum dadosSincronizacaoEnum,
            final DadosSincronizacao dadosSincronizacao,
            final String rfcProgramID,
            final AmbienteSincronizacao ambienteSincronizacao
    ) {

        final String centro = this.parametroService.obterPorTipoParametro(TipoParametroEnum.CENTRO_SAP).getValor();

        if (ObjetoUtil.isNotNull(dadosSincronizacao)) {

            final String requestKey = EventInRequestItem.generateKey(
                    ambienteSincronizacao.getRequestCount(),
                    SAPServiceImpl.CHAMADA_EXTERNA_SAP,
                    dadosSincronizacao.getModulo().getCodigo(),
                    centro,
                    DateUtil.getYear(DateUtil.hoje())
            );

            return (EventInRequest) FiltersFactory.createPreCadastro(
                    jobDataMap,
                    centro,
                    requestKey,
                    dadosSincronizacaoEnum,
                    dadosSincronizacao,
                    rfcProgramID,
                    null,
                    ambienteSincronizacao
            );
        }

        SAPServiceImpl.LOGGER.info("ENTIDADE " + dadosSincronizacaoEnum.name() + " NÃO MAPEADA");

        return null;
    }

    @Override
    public boolean enviarRomaneio(Romaneio romaneio, final boolean reenvio) {

        try {

            if (ObjetoUtil.isNotNull(romaneio.getStatusIntegracaoSAP()) && !reenvio) {

                return true;
            }

            this.romaneioService.validarSaldoOrdemDeVenda(romaneio);

            this.romaneioService.validarEtapasDoRomaneioObrigatorias(romaneio);

            this.romaneioService.validarValorUnitarioPedidoENota(romaneio);

            SAPServiceImpl.LOGGER.info(StringUtil.log("O " + romaneio.getNumeroRomaneio() + " E DA OPERACAO " + romaneio.getOperacao()
                    .getDescricao()));

            if (DadosSincronizacaoEnum.ENTRADA_ARMAZENAGEM_GO206.equals(romaneio.getOperacao().getEntidade())) {

                this.registrarLogIntegracao(romaneio, StatusRomaneioEnum.CONCLUIDO,null, Boolean.TRUE,
                        SAPServiceImpl.MSG_ROMANEIO_NAO_POSSUI_INTEGRACAO_COM_SAP, null);

            } else if (DadosSincronizacaoEnum.OPERACAO_SIMPLES_PESAGEM.equals(romaneio.getOperacao().getEntidade())) {

                Romaneio romaneioSalvo = this.registrarLogIntegracao(romaneio, StatusRomaneioEnum.CONCLUIDO,null,
                        Boolean.TRUE, SAPServiceImpl.MSG_ROMANEIO_NAO_POSSUI_INTEGRACAO_COM_SAP,null);

                this.romaneioService.inicializarLacreStatusLacre(romaneioSalvo);

            } else {

                final EventInResponse response = new EventInResponse();

                final Map<Long, JCoCallerService> connectionsMap = RFCMultipleProviders.DESTINATIONS;

                Long key = connectionsMap.keySet().stream().findFirst().orElse(null);

                SAPServiceImpl.LOGGER.info(StringUtil.log("O ROMANEIO NUMERO " + romaneio.getNumeroRomaneio() + " CONCLUIU TODAS ETAPAS E SERA ENVIADO  "));

                final JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.get(key);

                final AmbienteSincronizacao sincronizado = this.ambienteSincronizacaoService.obterAmbienteSincronizacao(romaneio.getOperacao(), sap);

                this.verificarConexaoComSAP();

                this.verificarPreCadastros(romaneio);

                if (ObjetoUtil.isNotNull(sap)) {

                    EventInRequest request = this.createRequests(romaneio, romaneio.getOperacao(), sap.getRfcProgramID(),
                            sincronizado, sap);

                    sap.call(request, response);

                    this.tratarRetornoDoSapParaRomaneio(romaneio, response, request, sap);

                    return true;

                } else {

                    romaneio.setDataEnvio(new Date());

                    this.registrarLogIntegracao(romaneio, StatusRomaneioEnum.CONCLUIDO, StatusIntegracaoSAPEnum.ERRO_INTEGRACAO,
                            Boolean.FALSE, SAPServiceImpl.MSG_NAO_HOUVE_RETORNO_DO_SAP_PARA_ESSE_ROMANEIO, null);
                }
            }

        } catch (final JCoComunicationException | ServicoException e) {

            SAPServiceImpl.LOGGER.info(StringUtil.log(SAPServiceImpl.MSG_ROMANEIO_COM_ERRO_DE_SINCRONIZAÇÃO));

            romaneio.setDataEnvio(new Date());

            this.registrarLogIntegracao(romaneio, StatusRomaneioEnum.CONCLUIDO, StatusIntegracaoSAPEnum.ERRO_INTEGRACAO,
                    Boolean.FALSE, SAPServiceImpl.MSG_ROMANEIO_COM_ERRO_DE_SINCRONIZAÇÃO, e.getMessage());

        } catch (DivergenciaPedidoNotaException e) {

            this.registrarLogIntegracao(romaneio, StatusRomaneioEnum.CONCLUIDO, StatusIntegracaoSAPEnum.ERRO_DIVERGENCIA_PEDIDO_NOTA,
                    Boolean.FALSE, e.getMessage(),null);

        } catch (ProcessoNaoConcluidoException e) {

            SAPServiceImpl.LOGGER.info(StringUtil.log(e.getMessage()));

        } catch (OrdemVendaSemSaldoException e) {

            this.registrarLogIntegracao(romaneio, StatusRomaneioEnum.PENDENTE, null, Boolean.FALSE, SAPServiceImpl.MSG_ROMANEIO_NAO_POSSUI_INTEGRACAO_COM_SAP,
                    e.getMessage());
        }

        return false;
    }

    @Override
    public List<ConfiguracaoUnidadeMedidaMaterial> obterMedidasParaConversao(Material material) {

        return materialService.obterConfiguracoesUnidadesMedidas(material.getCodigo());
    }

    @Override
    public BigDecimal converterQuantidade(List<ConfiguracaoUnidadeMedidaMaterial> unidades, String unidadeBalanca, Double quantidade) {

        return converterQuantidade(unidades, unidadeBalanca, KG, quantidade);
    }

    @Override
    public BigDecimal converterQuantidade(List<ConfiguracaoUnidadeMedidaMaterial> unidades, String unidadeBalanca, String unidadeASerEnviadaAoSAP, Double quantidade) {

        ConfiguracaoUnidadeMedidaMaterial unidadeMedida = unidades.stream()
                .filter(u -> u.getUnidadeMedidaASerCalculada()
                        .trim()
                        .equalsIgnoreCase(unidadeBalanca.trim()) && u.getUnidadeMedidaEquivalente()
                        .equalsIgnoreCase(unidadeASerEnviadaAoSAP))
                .findFirst()
                .orElse(null);

        if (ObjetoUtil.isNull(unidadeMedida)) {

            throw new ServicoException(String.format(
                    "Necessário cadastrar a conversão de %s para %s para o material utilizado",
                    unidadeBalanca,
                    unidadeASerEnviadaAoSAP
            ));
        }

        Double quantidadeConvertida = (unidadeMedida.getQuantidadeEquivalente().doubleValue() * quantidade) / unidadeMedida.getQuantidadeASerCalculada().doubleValue();

        if (unidadeMedida.isUtilizarCasasDecimais()) {

            return new BigDecimal(quantidadeConvertida).setScale(2, RoundingMode.HALF_UP);
        }

        return new BigDecimal(quantidadeConvertida).setScale(0, RoundingMode.HALF_UP);
    }

    private void tratarRetornoDoSapParaRomaneio(Romaneio romaneio, EventInResponse response, EventInRequest request, JCoCallerService sap) throws JCoComunicationException {

        this.bridgeService.salvar(request.getData(), romaneio);

        if (ColecaoUtil.isNotEmpty(response.getRetorno())) {

            for (final EventInResponseItem item : response.getRetorno()) {

                if (item.getCodReturn().trim().equals(SAPServiceImpl.HUM)) {

                    throw new JCoComunicationException(item.getDescReturn());
                }
            }

            romaneio.setDataEnvio(new Date());

            this.registrarLogIntegracao(
                    romaneio,
                    StatusRomaneioEnum.CONCLUIDO,
                    StatusIntegracaoSAPEnum.AGUARDANDO,
                    Boolean.TRUE,
                    SAPServiceImpl.MSG_O_ROMANEIO_ENVIADO_COM_SUCESSO,
                    null
            );

            if (ObjetoUtil.isNotNull(romaneio.getSenha())) {

                FluxoEtapa fluxoEtapa = fluxoEtapaService.obterFluxoEtapaPorMaterialEEtapa(romaneio.getSenha().getTipoFluxo(), romaneio.getMaterial(), EtapaEnum.SAIDA);

                this.senhaService.adicionarEtapa(romaneio.getSenha(), fluxoEtapa);
            }

        } else {

            romaneio.setDataEnvio(new Date());

            this.registrarLogIntegracao(
                    romaneio,
                    StatusRomaneioEnum.CONCLUIDO,
                    StatusIntegracaoSAPEnum.ERRO_INTEGRACAO,
                    Boolean.FALSE,
                    SAPServiceImpl.MSG_NAO_HOUVE_RETORNO_DO_SAP_PARA_ESSE_ROMANEIO,
                    null
            );
        }
    }

    private void verificarPreCadastros(Romaneio romaneio) throws ServicoException {

        boolean veiculoSemCodigo = Boolean.FALSE;

        boolean motoristaSemCodigo = Boolean.FALSE;

        if (romaneio.getOperacao().isPossuiDivisaoCarga()) {

            veiculoSemCodigo = romaneio.getItens()
                    .stream()
                    .filter(i -> ObjetoUtil.isNotNull(i.getPlacaCavalo()) && StringUtil.isEmpty(i.getPlacaCavalo()
                            .getCodigo()))
                    .count() > 0;

            motoristaSemCodigo = romaneio.getItens()
                    .stream()
                    .filter(i -> ObjetoUtil.isNotNull(i.getMotorista()) && StringUtil.isEmpty(i.getMotorista()
                            .getCodigo()))
                    .count() > 0;

        } else {

            veiculoSemCodigo = ObjetoUtil.isNotNull(romaneio.getPlacaCavalo()) && StringUtil.isEmpty(romaneio.getPlacaCavalo()
                    .getCodigo());

            motoristaSemCodigo = ObjetoUtil.isNotNull(romaneio.getMotorista()) && StringUtil.isEmpty(romaneio.getMotorista()
                    .getCodigo());
        }

        if (veiculoSemCodigo || motoristaSemCodigo) {

            throw new ServicoException(MessageSupport.getMessage("MSGE015"));
        }
    }

    @Override
    public void reenviarRomaneioPorId(Long id) {

        this.reenviarRomaneio(Collections.singletonList(this.romaneioService.get(id)));
    }

    @Override
    public void reenviarRomaneioPorData(Date data) {

        this.reenviarRomaneio(this.romaneioService.listarRomaneiosComErroDeIntegracaoPorData(data));
    }

    private void reenviarRomaneio(List<Romaneio> romaneios) {

        if (ColecaoUtil.isEmpty(romaneios)) {
            return;
        }

        romaneios.stream().forEach(romaneio -> {

            try {

                this.enviarRomaneio(
                        romaneio,
                        true
                );

            } catch (final Exception e) {

                SAPServiceImpl.LOGGER.log(
                        Level.SEVERE,
                        "ERRO AO REENVIAR ROMANEIO."
                );
            }
        });
    }

    private synchronized EventInRequest createRequests(
            final Romaneio romaneio,
            final DadosSincronizacao dadosSincronizacao,
            final String rfcProgramID,
            final AmbienteSincronizacao sincronizado,
            final JCoCallerService sap
    ) {

        if (ObjetoUtil.isNotNull(dadosSincronizacao)) {

            final String centro = this.parametroService.obterPorTipoParametro(TipoParametroEnum.CENTRO_SAP)
                    .getValor();

            String requestKey = romaneio.getOperacao().isGestaoPatio() ? EventInRequestItem.generateKey(
                    sincronizado.getRequestCount(),
                    SAPServiceImpl.CHAMADA_EXTERNA_SAP,
                    dadosSincronizacao.getModulo().getCodigo(),
                    centro,
                    DateUtil.getYear(DateUtil.hoje())
            ) : EventInRequestItem.generateKey(
                    sincronizado.getRequestCount(),
                    SAPServiceImpl.CHAMADA_EXTERNA_SAP,
                    dadosSincronizacao.getModulo().getCodigo(),
                    DateUtil.format(
                            "ddMMyyyy",
                            DateUtil.hoje()
                    )
            );

            this.ambienteSincronizacaoService.updateRequestCount(
                    romaneio.getOperacao(),
                    sap
            );

            return RomaneioFiltersFactory.create(
                    dadosSincronizacao,
                    centro,
                    rfcProgramID,
                    romaneio,
                    requestKey
            );
        }

        return null;
    }

    private void verificarConexaoComSAP() throws JCoComunicationException {

        if (!this.possuiConexaoComSAPPorAmbiente()) {

            throw new JCoComunicationException();
        }
    }

    private boolean possuiConexaoComSAPPorAmbiente() {

        final Ambiente ambiente = this.ambienteDAO.findAll().stream().findFirst().get();

        try {

            final JCoDestination destination = JCoDestinationManager.getDestination(ambiente.getHost() + "-" + ambiente.getMandante());

            destination.ping();

            SAPSync.ONLINE = true;

            return true;

        } catch (final JCoException e) {

            SAPSync.ONLINE = false;

            SAPServiceImpl.LOGGER.severe("ERRO AO REALIZAR O PING NO SAP, ITSSAGRO OFFLINE.");

            return false;
        }
    }

    @Override
    public Romaneio registrarLogIntegracao(
            Romaneio romaneio,
            final StatusRomaneioEnum statusRomaneio,
            final StatusIntegracaoSAPEnum statusSap,
            final boolean enviado,
            final String log,
            final String exception
    ) {

        romaneio.setStatusRomaneio(statusRomaneio);

        romaneio.setStatusIntegracaoSAP(statusSap);

        romaneio.setRomaneioEnviado(enviado);

        this.romaneioService.atualizarSaldoReservadoPedido(romaneio);

        romaneio = this.romaneioService.getDAO().save(romaneio);

        this.registrarLogEnvio(
                romaneio,
                log,
                exception
        );

        return romaneio;
    }

    private void registrarLogEnvio(
            final Romaneio romaneio, final String log, final String ex
    ) {

        RomaneioLogEnvio romaneioLogEnvio = this.romaneioLogEnvioService.obterPorRomaneio(romaneio);

        if (ObjetoUtil.isNull(romaneioLogEnvio)) {

            romaneioLogEnvio = new RomaneioLogEnvio();

            romaneioLogEnvio.setLog(log.trim());

            romaneioLogEnvio.setException(ex);

            romaneioLogEnvio.setDataEnvio(new Date());

            romaneioLogEnvio.setRomaneio(romaneio);

            this.romaneioLogEnvioService.salvar(romaneioLogEnvio);

        } else {

            romaneioLogEnvio.setLog(log.trim());

            romaneioLogEnvio.setException(ex);

            romaneioLogEnvio.setDataResposta(new Date());

            romaneioLogEnvio.setRomaneio(romaneio);

            this.romaneioLogEnvioService.salvar(romaneioLogEnvio);
        }
    }

    @Override
    public RfcSaldoOrdemPedidoResponse buscarSaldoPedido(
            String pedido, final String item, final String operacao
    ) throws Exception {

        RfcSaldoOrdemPedidoRequest request = new RfcSaldoOrdemPedidoRequest();

        request.setOperacao(operacao);

        request.setItemPedido(StringUtil.completarZeroAEsquerda(
                5,
                item
        ));

        request.setPedido(pedido);

        RfcSaldoOrdemPedidoResponse response = new RfcSaldoOrdemPedidoResponse();

        final Map<Long, JCoCallerService> connectionsMap = RFCMultipleProviders.DESTINATIONS;

        if (ObjetoUtil.isNotNull(connectionsMap)) {

            Long key = connectionsMap.keySet().stream().findFirst().get();

            try {

                final JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.get(key);

                sap.call(
                        request,
                        response
                );

                if (StringUtil.isNotNullEmpty(response.getMensagemException())) {

                    throw new Exception(response.getMensagemException());
                }

                this.validarStatusPedido(
                        sap,
                        pedido
                );

                return response;

            } catch (Exception e) {

                throw new Exception(e.getMessage());
            }
        }

        return null;
    }

    private RfcSaldoOrdemPedidoTransferenciaResponse buscarSaldoPedidoTransferencia(String pedido, final String item, final String operacao) throws Exception {

        RfcSaldoOrdemPedidoTransferenciaRequest request = new RfcSaldoOrdemPedidoTransferenciaRequest();

        request.setOperacao(operacao);

        request.setItemPedido(StringUtil.completarZeroAEsquerda(5, item));

        request.setPedido(pedido);

        RfcSaldoOrdemPedidoTransferenciaResponse response = new RfcSaldoOrdemPedidoTransferenciaResponse();

        final Map<Long, JCoCallerService> connectionsMap = RFCMultipleProviders.DESTINATIONS;

        if (ObjetoUtil.isNotNull(connectionsMap)) {

            Long key = connectionsMap.keySet().stream().findFirst().get();

            try {

                final JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.get(key);

                sap.call(request, response);

                if (StringUtil.isNotNullEmpty(response.getMensagemException())) {

                    throw new Exception(response.getMensagemException());
                }

                this.validarStatusPedido(sap, pedido);

                return response;

            } catch (Exception e) {

                throw new Exception(e.getMessage());
            }
        }

        return null;
    }

    @Override
    public RfcToleranciaOrdemPedidoResponse buscarToleranciaPedido(String pedido, final String item, final String operacao) throws Exception {

        RfcToleranciaOrdemPedidoRequest request = new RfcToleranciaOrdemPedidoRequest();

        request.setOperacao(operacao);

        request.setItemPedido(StringUtil.completarZeroAEsquerda(5, item));

        request.setPedido(pedido);

        RfcToleranciaOrdemPedidoResponse response = new RfcToleranciaOrdemPedidoResponse();

        final Map<Long, JCoCallerService> connectionsMap = RFCMultipleProviders.DESTINATIONS;

        if (ObjetoUtil.isNotNull(connectionsMap)) {

            Long key = connectionsMap.keySet().stream().findFirst().get();

            try {

                final JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.get(key);

                sap.call(request, response);

                if (StringUtil.isNotNullEmpty(response.getMensagemException())) {

                    throw new Exception(response.getMensagemException());
                }

                this.validarStatusPedido(sap, pedido);

                return response;

            } catch (Exception e) {

                throw new Exception(e.getMessage());
            }
        }

        return null;
    }

    @Override
    public RfcObterNfeResponse buscarDadosNfe(
            String chave
    ) throws Exception {

        RfcObterNfeRequest request = new RfcObterNfeRequest();

        request.setChave(chave);

        RfcObterNfeResponse response = new RfcObterNfeResponse();
        final Map<Long, JCoCallerService> connectionsMap = RFCMultipleProviders.DESTINATIONS;

        if (ObjetoUtil.isNotNull(connectionsMap)) {

            Long key = connectionsMap.keySet().stream().findFirst().get();

            try {

                final JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.get(key);

                sap.call(
                        request,
                        response
                );

//                if (StringUtil.isNotNullEmpty(response.getMensagemException())) {
//
//                    throw new Exception(response.getMensagemException());
//                }

                return response;

            } catch (Exception e) {

                throw new Exception(e.getMessage());
            }
        }

        return null;
    }

    @Override
    public DadosNfeTransferenciaResponse buscarDadosNfeTransferencia(String chave, String remessa) throws Exception {

        RfcObterNfeTransferenciaRequest sapRequest = new RfcObterNfeTransferenciaRequest();

        sapRequest.setChave(StringUtil.isNotNullEmpty(chave) ? chave : StringUtil.empty());
        sapRequest.setRemessa(StringUtil.isNotNullEmpty(remessa) ? StringUtil.completarZeroAEsquerda(10, remessa) : StringUtil.empty());

        RfcObterNfeTransferenciaResponse sapResponse = new RfcObterNfeTransferenciaResponse();
        final Map<Long, JCoCallerService> connectionsMap = RFCMultipleProviders.DESTINATIONS;

        if (ObjetoUtil.isNotNull(connectionsMap)) {

            Long key = connectionsMap.keySet().stream().findFirst().get();

            try {

                final JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.get(key);

                sap.call(sapRequest, sapResponse);

                final Safra safra = StringUtil.isNotNullEmpty(sapResponse.getItem().getSafra()) ? this.safraService.obterPorCodigo(sapResponse.getItem().getSafra()) : null;

                final Deposito deposito = StringUtil.isNotNullEmpty(sapResponse.getItem().getLgort_d()) ? this.depositoService.obterUnicoPorCodigo(sapResponse.getItem().getLgort_d()) : null;

                final Material material = StringUtil.isNotNullEmpty(sapResponse.getItem().getMatnr()) ? this.materialService.obterPorCodigo(new Long(sapResponse.getItem().getMatnr()).toString()) : null;

                final Centro centro = StringUtil.isNotNullEmpty(sapResponse.getItem().getWerks_d()) ? this.centroService.obterPorCodigo(new Long(sapResponse.getItem().getWerks_d()).toString()) : null;

                final Date dataNfe = DateUtil.dateHorarioSAPformat(sapResponse.getHeader().getDhemi(), sapResponse.getHeader().getTime());

                return DadosNfeTransferenciaResponse.builder().remessa(sapResponse.getRemessa()).chave(sapResponse.getChave())
                        .safra(safra).deposito(deposito).material(material).centro(centro).nnf(sapResponse.getHeader().getNnf())
                        .serie(sapResponse.getHeader().getSerie()).dataNfe(DateUtil.format("YYYYMMddHHmmss", dataNfe))
                        .nprot(sapResponse.getHeader().getNprot()).dig(sapResponse.getHeader().getDig())
                        .nrAle(sapResponse.getHeader().getNrAle()).vnf(sapResponse.getHeader().getVnf())
                        .status(sapResponse.getHeader().getStatus()).docStatus(sapResponse.getDocStatus())
                        .quantidade(sapResponse.getItem().getMenge()).medida(sapResponse.getItem().getMeins())
                        .remessaApta(StringUtil.isNotNullEmpty(sapResponse.getRemessaApta())
                                && "X".equalsIgnoreCase(sapResponse.getRemessaApta().trim())).build();

            } catch (Exception e) {

                throw new Exception(e.getMessage());
            }
        }

        return null;
    }

    @Override
    public RfcObterCteResponse buscarDadosCte(
            String chave
    ) throws Exception {

        RfcObterCteRequest request = new RfcObterCteRequest();

        request.setChave(chave);

        RfcObterCteResponse response = new RfcObterCteResponse();
        final Map<Long, JCoCallerService> connectionsMap = RFCMultipleProviders.DESTINATIONS;

        if (ObjetoUtil.isNotNull(connectionsMap)) {

            Long key = connectionsMap.keySet().stream().findFirst().get();

            try {

                final JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.get(key);

                sap.call(
                        request,
                        response
                );

//                if (StringUtil.isNotNullEmpty(response.getMensagemException())) {
//
//                    throw new Exception(response.getMensagemException());
//                }

                return response;

            } catch (Exception e) {

                throw new Exception(e.getMessage());
            }
        }

        return null;
    }

    @Override
    public OrdemVendaDTO buscarOrdemVenda(String codigoOrdemVenda) throws Exception {

        try {

            JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.values().stream().findFirst().orElse(null);

            if (sap != null) {

                RfcOrdemVendaRequest request = new RfcOrdemVendaRequest();

                request.setItem(new RfcOrdemVendaItemRequest(
                        FLAG_SAP,
                        FLAG_SAP,
                        VAZIO,
                        FLAG_SAP,
                        FLAG_SAP,
                        VAZIO,
                        VAZIO,
                        VAZIO,
                        VAZIO,
                        VAZIO,
                        VAZIO,
                        VAZIO,
                        VAZIO,
                        VAZIO,
                        VAZIO,
                        VAZIO,
                        VAZIO
                ));

                request.setOrdensVenda(Collections.singletonList(new RfcOrdemVendaDocumentoVendaRequest(StringUtil
                        .completarZeroAEsquerda(
                                10,
                                codigoOrdemVenda
                        ))));

                RfcOrdemVendaResponse response = new RfcOrdemVendaResponse();

                sap.call(
                        request,
                        response
                );

                if (ColecaoUtil.isEmpty(response.getCabecalhos())) {
                    throw new Exception("Ordem de Venda (SAP) informada não encontrada");
                }

                validarTipoDeOrdemParametrizada(response);

                final Cliente cliente = response.getParceiros()
                        .stream()
                        .filter(p -> TipoParceiroSAPEnum.CLIENTE.getCodigo()
                                .equals(p.getTipoParceiro()))
                        .findFirst()
                        .map(p -> clienteService.obterPorCodigo(new Long(p.getCodigoCliente())
                                .toString()))
                        .orElse(null);

                final Transportadora transportadora = response.getParceiros()
                        .stream()
                        .filter(p -> TipoParceiroSAPEnum.TRANSPORTADORA.getCodigo()
                                .equals(p.getTipoParceiro()))
                        .findFirst()
                        .map(p -> transportadoraService.obterPorCodigo(new Long(p.getCodigoTransportadora())
                                .toString()))
                        .orElse(null);

                final Transportadora transportadoraSub = response.getParceiros()
                        .stream()
                        .filter(p -> TipoParceiroSAPEnum.TRANSPORTADORA_SUB
                                .getCodigo()
                                .equals(p.getTipoParceiro()))
                        .findFirst()
                        .map(p -> transportadoraService.obterPorCodigo(new Long(p.getCodigoCliente())
                                .toString()))
                        .orElse(null);

                final Cliente operadorLogistico = response.getParceiros()
                        .stream()
                        .filter(p -> TipoParceiroSAPEnum.OPERADOR_LOGISTICO
                                .getCodigo()
                                .equals(p.getTipoParceiro()))
                        .findFirst()
                        .map(p -> clienteService.obterPorCodigo(new Long(p.getCodigoCliente())
                                .toString()))
                        .orElse(null);

                final Deposito deposito = response.getItens()
                        .stream()
                        .filter(i -> StringUtil.isNotNullEmpty(i.getCodigoDeposito()))
                        .findFirst()
                        .map(i -> depositoService.obterPorCodigo(new Long(i.getCodigoDeposito())
                                .toString()))
                        .orElse(null);

                final Material material = response.getItens()
                        .stream()
                        .filter(i -> StringUtil.isNotNullEmpty(i.getCodigoMaterial()))
                        .findFirst()
                        .map(i -> materialService.obterPorCodigo(new Long(i.getCodigoMaterial())
                                .toString()))
                        .orElse(null);

                final Centro centro = response.getItens()
                        .stream()
                        .filter(i -> StringUtil.isNotNullEmpty(i.getCentro()))
                        .findFirst()
                        .map(i -> centroService.obterPorCodigo(new Long(i.getCentro()).toString()))
                        .orElse(null);

                final RfcSaldoOrdemVendaResponse rfcSaldoOrdemVendaResponse = this.buscarSaldoOrdemVenda(StringUtil
                        .completarZeroAEsquerda(
                                10,
                                codigoOrdemVenda
                        ));

                final String itemOrdem = response.getItens()
                        .stream()
                        .filter(i -> StringUtil.isNotNullEmpty(i.getNumeroItem()))
                        .findFirst()
                        .map(RfcOrdemVendaItemSaidaResponse::getNumeroItem)
                        .orElse(null);

                final String empresa = response.getCabecalhos()
                        .stream()
                        .filter(i -> StringUtil.isNotNullEmpty(i.getEmpresa()))
                        .findFirst()
                        .map(RfcOrdemVendaCabecalhoSaidaResponse::getEmpresa)
                        .orElse(null);

                final String localExpedicao = response.getItens()
                        .stream()
                        .filter(i -> StringUtil.isNotNullEmpty(i.getLocalExpedicao()))
                        .findFirst()
                        .map(RfcOrdemVendaItemSaidaResponse::getLocalExpedicao)
                        .orElse(null);

                final String unidadeMedidaOrdem = response.getItens()
                        .stream()
                        .filter(i -> StringUtil.isNotNullEmpty(i.getUnidadeVenda()))
                        .findFirst()
                        .map(RfcOrdemVendaItemSaidaResponse::getUnidadeVenda)
                        .orElse(null);

                final String numeroPedido = response.getCabecalhos()
                        .stream()
                        .filter(i -> StringUtil.isNotNullEmpty(i.getNumeroPedido()))
                        .findFirst()
                        .map(RfcOrdemVendaCabecalhoSaidaResponse::getNumeroPedido)
                        .orElse(null);

                final BigDecimal saldoOrdem = ObjetoUtil.isNotNull(rfcSaldoOrdemVendaResponse) ? rfcSaldoOrdemVendaResponse
                        .getSaldo() : BigDecimal.ZERO;

                final TipoFreteEnum tipoFreteEnum = response.getDadosGerais()
                        .stream()
                        .map(i -> TipoFreteEnum.valueOf(i.getFrete()))
                        .findFirst()
                        .orElse(null);

                return new OrdemVendaDTO(
                        cliente,
                        operadorLogistico,
                        transportadora,
                        transportadoraSub,
                        deposito,
                        material,
                        centro,
                        tipoFreteEnum,
                        itemOrdem,
                        empresa,
                        localExpedicao,
                        unidadeMedidaOrdem,
                        numeroPedido,
                        saldoOrdem,
                        BigDecimal.ZERO
                );
            }

        } catch (Exception e) {

            e.printStackTrace();

            throw new Exception(e.getMessage());
        }

        return null;
    }

    private void validarTipoDeOrdemParametrizada(RfcOrdemVendaResponse response) throws Exception {

        final String tipoOrdem = response.getCabecalhos()
                .stream()
                .map(RfcOrdemVendaCabecalhoSaidaResponse::getTipoOrdem)
                .filter(Objects::nonNull)
                .findFirst()
                .orElse(null);

        if (StringUtil.isNotNullEmpty(tipoOrdem)) {

            final Parametro parametroTipoOrdem = parametroService.obterPorTipoParametro(TipoParametroEnum.TIPO_ORDEM_INUTILIZADA);

            if (ObjetoUtil.isNotNull(parametroTipoOrdem)) {

                List<String> tipos = Arrays.asList(parametroTipoOrdem.getValor().split(";"));

                if (ColecaoUtil.isNotEmpty(tipos)) {

                    boolean ordemBloqueada = tipos.stream().filter(Predicate.isEqual(tipoOrdem)).count() > 0;

                    if (ordemBloqueada) {

                        throw new Exception(MessageSupport.getMessage(
                                "MSGE034",
                                tipoOrdem
                        ));
                    }
                }
            }
        }
    }

    @Override
    public RfcSaldoOrdemVendaResponse buscarSaldoOrdemVenda(String codigoOrdemVenda) throws Exception {

        try {

            JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.values().stream().findFirst().orElse(null);

            if (sap != null) {

                RfcSaldoOrdemVendaRequest request = new RfcSaldoOrdemVendaRequest(StringUtil.completarZeroAEsquerda(
                        10,
                        codigoOrdemVenda
                ));

                RfcSaldoOrdemVendaResponse response = new RfcSaldoOrdemVendaResponse();

                sap.call(
                        request,
                        response
                );

                return response;
            }

        } catch (Exception e) {

            e.printStackTrace();

            throw new Exception(e.getMessage());
        }

        return null;
    }

    @Override
    public RfcSaldoEstoqueProdutoAcabadoResponse buscarSaldoEstoqueProdutoAcabado(
            final String material, final String centro
    ) {

        try {

            JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.values().stream().findFirst().orElse(null);

            if (sap != null) {

                RfcSaldoEstoqueProdutoAcabadoRequest request = new RfcSaldoEstoqueProdutoAcabadoRequest(
                        StringUtil.completarZeroAEsquerda(
                                18,
                                material
                        ),
                        centro
                );

                RfcSaldoEstoqueProdutoAcabadoResponse response = new RfcSaldoEstoqueProdutoAcabadoResponse();

                sap.call(
                        request,
                        response
                );

                return response;
            }

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void criarOuModificarDocumentoTransporte(final Romaneio entidade) {

        if (entidade.getOperacao().isGestaoPatio() && entidade.getOperacao().isCriarDocumentoTransporte()) {

            if (!romaneioPossuiRestricaoPorOrdemVendaBloqueada(entidade)) {

                JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.values().stream().findFirst().orElse(null);

                if (ObjetoUtil.isNotNull(sap) && ColecaoUtil.isNotEmpty(entidade.getItens())) {

                    if (ColecaoUtil.isNotEmpty(entidade.getRestricoes())) {

                        entidade.getRestricoes()
                                .removeIf(r -> TipoRestricaoEnum.CRIAR_DOC_TRANSPORTE.equals(r.getTipoRestricao()));

                    } else {

                        entidade.setRestricoes(new ArrayList<>());
                    }

                    final Centro centro = entidade.getCentro();

                    final Material material = entidade.getItens()
                            .stream()
                            .filter(i -> ObjetoUtil.isNotNull(i.getMaterial()))
                            .map(ItemNFPedido::getMaterial)
                            .findFirst()
                            .orElse(null);

                    final Motorista motorista = entidade.getItens()
                            .stream()
                            .filter(i -> ObjetoUtil.isNotNull(i.getMotorista()))
                            .map(ItemNFPedido::getMotorista)
                            .findFirst()
                            .orElse(null);

                    final Transportadora transportadora = entidade.getItens()
                            .stream()
                            .filter(i -> ObjetoUtil.isNotNull(i.getTransportadora()))
                            .map(ItemNFPedido::getTransportadora)
                            .findFirst()
                            .orElse(null);

                    final Veiculo veiculo = entidade.getItens()
                            .stream()
                            .filter(i -> ObjetoUtil.isNotNull(i.getPlacaCavalo()))
                            .map(ItemNFPedido::getPlacaCavalo)
                            .findFirst()
                            .orElse(null);

                    final String placaUm = entidade.getItens()
                            .stream()
                            .filter(i -> StringUtil.isNotNullEmpty(i.getPlacaCarretaUm()))
                            .map(ItemNFPedido::getPlacaCarretaUm)
                            .findFirst()
                            .orElse(null);

                    final String placaDois = entidade.getItens()
                            .stream()
                            .filter(i -> StringUtil.isNotNullEmpty(i.getPlacaCarretaDois()))
                            .map(ItemNFPedido::getPlacaCarretaDois)
                            .findFirst()
                            .orElse(null);

                    final String documentoTransporte = entidade.getItens()
                            .stream()
                            .filter(i -> StringUtil.isNotNullEmpty(i.getNumeroOrdemTransporte()))
                            .map(ItemNFPedido::getNumeroOrdemTransporte)
                            .findFirst()
                            .orElse(null);

                    entidade.getItens().stream().sorted(Comparator.comparing(i -> new Long(i.getNumeroRemessa())));

                    final boolean modificar = entidade.getItens()
                            .stream()
                            .filter(i -> StringUtil.isNotNullEmpty(i.getNumeroOrdemTransporte()))
                            .count() > 0;

                    if (modificar) {

                        modificarDocumentoTransporte(
                                entidade,
                                sap,
                                documentoTransporte
                        );

                    } else if (entidade.getItens()
                            .stream()
                            .filter(i -> StringUtil.isEmpty(i.getNumeroOrdemTransporte()))
                            .count() == entidade.getItens().size() && ObjetoUtil.isNotNull(centro)) {

                        criarDocumentoTransporte(
                                entidade,
                                sap,
                                centro,
                                material,
                                motorista,
                                transportadora,
                                veiculo,
                                placaUm,
                                placaDois
                        );
                    }
                }
            }
        }
    }

    private void deletarDocumentoTransporte(
            Romaneio entidade, JCoCallerService sap, String documentoTransporte
    ) {

        if (entidade.getItens()
                .stream()
                .filter(i -> StatusRemessaEnum.ESTORNADA.equals(i.getStatusRemessa()))
                .count() > 0 && StringUtil.isNotNullEmpty(documentoTransporte)) {

            final RfcChangeDocTransporteRequest request = new RfcChangeDocTransporteRequest();

            final RfcChangeDocTransporteResponse response = new RfcChangeDocTransporteResponse();

            request.setAcao(AcaoTransporteEnum.D.name());

            request.setBapiShipmentHeader(new BapiShipmentHeader(StringUtil.completarZeroAEsquerda(
                    10,
                    documentoTransporte
            )));

            request.setBapiShipmentItems(entidade.getItens()
                    .stream()
                    .filter(i -> StatusRemessaEnum.ESTORNADA.equals(i.getStatusRemessa()) && StringUtil
                            .isNotNullEmpty(i.getNumeroOrdemTransporte()))
                    .map(i -> new BapiShipmentItem(
                            StringUtil.completarZeroAEsquerda(
                                    10,
                                    i.getNumeroRemessa()
                            ),
                            StringUtil.completarZeroAEsquerda(
                                    4,
                                    i.getItemTransporte().toString()
                            )
                    ))
                    .collect(Collectors.toList()));

            request.setBapiShipmentItemActions(Collections.singletonList(new BapiShipmentItemAction(
                    "D",
                    "D"
            )));

            if (ColecaoUtil.isNotEmpty(request.getBapiShipmentItems())) {

                if (ColecaoUtil.isNotEmpty(entidade.getRestricoes())) {

                    entidade.getRestricoes()
                            .removeIf(r -> TipoRestricaoEnum.CRIAR_DOC_TRANSPORTE.equals(r.getTipoRestricao()));

                } else {

                    entidade.setRestricoes(new ArrayList<>());
                }

                sap.call(
                        request,
                        response
                );

                final List<RfcReturnItem> erros = response.getTReturn()
                        .stream()
                        .filter(r -> "E".equals(r.getTipoMensagem()))
                        .collect(Collectors.toList());

                if (!erros.isEmpty()) {

                    entidade.getRestricoes().addAll(erros.stream().map(r -> {
                        final List<String> mensagensErro = Arrays.asList(new String[]{
                                r.getMensagem(),
                                r.getMensagemUm(),
                                r.getMensagemDois(),
                                r.getMensagemTres(),
                                r.getMensagemQuatro()
                        });
                        return new RestricaoOrdem(
                                mensagensErro.stream()
                                        .filter(m -> StringUtil.isNotNullEmpty(m))
                                        .collect(Collectors.joining("; ")),
                                "Erro ao criar documento de transporte",
                                TipoRestricaoEnum.CRIAR_DOC_TRANSPORTE,
                                entidade
                        );
                    }).collect(Collectors.toList()));

                    entidade.getItens()
                            .stream()
                            .filter(i -> StatusRemessaEnum.ESTORNADA.equals(i.getStatusRemessa()) && StringUtil
                                    .isNotNullEmpty(i.getNumeroOrdemTransporte()))
                            .forEach(i -> i.setStatusDocTransporte(StatusDocTransporteEnum.ERRO_AO_DELETAR));

                } else {

                    entidade.getItens()
                            .stream()
                            .filter(i -> StatusRemessaEnum.ESTORNADA.equals(i.getStatusRemessa()) && StringUtil
                                    .isNotNullEmpty(i.getNumeroOrdemTransporte()))
                            .forEach(i -> {
                                i.setStatusDocTransporte(StatusDocTransporteEnum.DELETADA);
                                i.setNumeroOrdemTransporte(null);
                            });
                }
            }
        }
    }

    private void modificarDocumentoTransporte(
            Romaneio entidade, JCoCallerService sap, String documentoTransporte
    ) {

        final RfcChangeDocTransporteRequest request = new RfcChangeDocTransporteRequest();

        final RfcChangeDocTransporteResponse response = new RfcChangeDocTransporteResponse();

        request.setAcao(AcaoTransporteEnum.A.name());

        request.setBapiShipmentHeader(new BapiShipmentHeader(StringUtil.completarZeroAEsquerda(
                10,
                documentoTransporte
        )));

        request.setBapiShipmentItems(entidade.getItens()
                .stream()
                .filter(i -> StringUtil.isEmpty(i.getNumeroOrdemTransporte()) && StatusRemessaEnum.CRIADA
                        .equals(i.getStatusRemessa()))
                .map(i -> {
                    final Long ultimoItemTransporte = entidade.getItens()
                            .stream()
                            .filter(item -> ObjetoUtil
                                    .isNotNull(item.getItemTransporte()))
                            .map(ItemNFPedido::getItemTransporte)
                            .max(Comparator.comparing(Long::doubleValue))
                            .orElse(new Long(0));
                    i.setItemTransporte(new Double(ultimoItemTransporte.doubleValue() + 10)
                            .longValue());
                    return new BapiShipmentItem(
                            StringUtil.completarZeroAEsquerda(
                                    10,
                                    i.getNumeroRemessa()
                            ),
                            StringUtil.completarZeroAEsquerda(
                                    4,
                                    i.getItemTransporte().toString()
                            )
                    );
                })
                .collect(Collectors.toList()));

        request.setBapiShipmentItemActions(Collections.singletonList(new BapiShipmentItemAction(
                "A",
                "A"
        )));

        if (ColecaoUtil.isNotEmpty(request.getBapiShipmentItems())) {

            sap.call(
                    request,
                    response
            );

            final List<RfcReturnItem> erros = response.getTReturn()
                    .stream()
                    .filter(r -> "E".equals(r.getTipoMensagem()))
                    .collect(Collectors.toList());

            if (!erros.isEmpty()) {

                entidade.getRestricoes().addAll(erros.stream().map(r -> {

                    final List<String> mensagensErro = Arrays.asList(new String[]{
                            r.getMensagem(),
                            r.getMensagemUm(),
                            r.getMensagemDois(),
                            r.getMensagemTres(),
                            r.getMensagemQuatro()
                    });

                    return new RestricaoOrdem(
                            mensagensErro.stream()
                                    .filter(m -> StringUtil.isNotNullEmpty(m))
                                    .collect(Collectors.joining("; ")),
                            "Erro ao modificar documento de transporte",
                            TipoRestricaoEnum.CRIAR_DOC_TRANSPORTE,
                            entidade
                    );

                }).collect(Collectors.toList()));

                entidade.getItens()
                        .stream()
                        .filter(i -> StringUtil.isEmpty(i.getNumeroOrdemTransporte()) && StatusRemessaEnum.CRIADA
                                .equals(i.getStatusRemessa()))
                        .forEach(i -> i.setStatusDocTransporte(StatusDocTransporteEnum.ERRO_AO_MODIFICAR));

            } else {

                entidade.getItens()
                        .stream()
                        .filter(i -> StringUtil.isEmpty(i.getNumeroOrdemTransporte()) && StatusRemessaEnum.CRIADA
                                .equals(i.getStatusRemessa()))
                        .forEach(i -> {
                            i.setStatusDocTransporte(StatusDocTransporteEnum.CRIADA);
                            i.setNumeroOrdemTransporte(entidade.getItens()
                                    .stream()
                                    .filter(pedido -> StringUtil.isNotNullEmpty(pedido.getNumeroOrdemTransporte()))
                                    .map(ItemNFPedido::getNumeroOrdemTransporte)
                                    .findFirst()
                                    .orElse(null));
                        });
            }
        }
    }

    private void criarDocumentoTransporte(
            Romaneio entidade,
            JCoCallerService sap,
            Centro centro,
            Material material,
            Motorista motorista,
            Transportadora transportadora,
            Veiculo veiculo,
            String placaUm,
            String placaDois
    ) {

        final RfcCriarDocTransporteRequest request = new RfcCriarDocTransporteRequest();

        final RfcCriarDocTransporteResponse response = new RfcCriarDocTransporteResponse();

        TipoTransporteEnum tipoTransporte = getTipoTransporte(entidade, material);

        request.setBapiShipmentHeader(new BapiShipmentHeader(
                tipoTransporte.getCodigo(),
                centro.getCodigo(),
                veiculo.getPlacaMaisUf(),
                StringUtil.completarZeroAEsquerda(10, transportadora.getCodigo()),
                placaUm,
                placaDois,
                motorista.getNome())
        );

        request.setBev1RptdbesI(new Bev1RptdbesI(
                StringUtil.completarZeroAEsquerda(10, motorista.getCodigo()),
                StringUtil.toPlaca(veiculo.getPlaca1()),
                placaUm
        ));

        request.setBapiShipmentItems(entidade.getItens()
                .stream()
                .filter(i -> (i.getStatusDocTransporte() == null || StatusDocTransporteEnum.ERRO_AO_CRIAR.equals(i.getStatusDocTransporte())) && StatusRemessaEnum.CRIADA.equals(i.getStatusRemessa()))
                .map(i -> {
                    final Long ultimoItemTransporte = entidade.getItens().stream().filter(item -> ObjetoUtil.isNotNull(item.getItemTransporte())).map(ItemNFPedido::getItemTransporte).max(Comparator.comparing(Long::doubleValue)).orElse(new Long(0));
                    i.setItemTransporte(new Double(ultimoItemTransporte.doubleValue() + 10).longValue());
                    return new BapiShipmentItem(
                            StringUtil.completarZeroAEsquerda(10, i.getNumeroRemessa()),
                            StringUtil.completarZeroAEsquerda(4, i.getItemTransporte().toString())
                    );
                })
                .collect(Collectors.toList()));

        request.setStageDataItems(entidade.getItens()
                .stream()
                .filter(i -> (i.getStatusDocTransporte() == null || StatusDocTransporteEnum.ERRO_AO_CRIAR
                        .equals(i.getStatusDocTransporte())) && StatusRemessaEnum.CRIADA
                        .equals(i.getStatusRemessa()))
                .map(i -> new StageDataItem(
                        "1",
                        "0001",
                        "01",
                        "4",
                        centro.getCodigo(),
                        "GESTAO-PATIO",
                        StringUtil.completarZeroAEsquerda(10, i.getCliente().getCodigo()),
                        StringUtil.completarZeroAEsquerda(10, i.getTransportadora().getCodigo()),
                        FLAG_SAP
                ))
                .collect(Collectors.toList()));

        if (ColecaoUtil.isNotEmpty(request.getBapiShipmentItems()) && ColecaoUtil.isNotEmpty(request.getStageDataItems())) {

            sap.call(request, response);

            final List<RfcReturnItem> erros = response.getTReturn().stream().filter(r -> "E".equals(r.getTipoMensagem())).collect(Collectors.toList());

            if (!erros.isEmpty()) {

                entidade.getRestricoes().addAll(erros.stream().map(r -> {

                    final List<String> mensagensErro = Arrays.asList(new String[]{
                            r.getMensagem(),
                            r.getMensagemUm(),
                            r.getMensagemDois(),
                            r.getMensagemTres(),
                            r.getMensagemQuatro()
                    });

                    return new RestricaoOrdem(
                            mensagensErro.stream().filter(m -> StringUtil.isNotNullEmpty(m)).collect(Collectors.joining("; ")),
                            "Erro ao criar documento de transporte",
                            TipoRestricaoEnum.CRIAR_DOC_TRANSPORTE,
                            entidade
                    );

                }).collect(Collectors.toList()));

                entidade.getItens()
                        .stream()
                        .filter(i -> (i.getStatusDocTransporte() == null || StatusDocTransporteEnum.ERRO_AO_CRIAR
                                .equals(i.getStatusDocTransporte())) && StatusRemessaEnum.CRIADA
                                .equals(i.getStatusRemessa()))
                        .forEach(i -> i.setStatusDocTransporte(StatusDocTransporteEnum.ERRO_AO_CRIAR));

            } else {

                entidade.getItens()
                        .stream()
                        .filter(i -> (i.getStatusDocTransporte() == null || StatusDocTransporteEnum.ERRO_AO_CRIAR
                                .equals(i.getStatusDocTransporte())) && StatusRemessaEnum.CRIADA
                                .equals(i.getStatusRemessa()))
                        .forEach(i -> {
                            i.setStatusDocTransporte(StatusDocTransporteEnum.CRIADA);
                            i.setNumeroOrdemTransporte(new Long(response.getDocumentoTransporte()).toString());
                        });
            }
        }
    }

    private TipoTransporteEnum getTipoTransporte(Romaneio entidade, Material material) {

        final Optional<Transportadora> optionalTransportadora = entidade.getItens().stream().map(ItemNFPedido::getTransportadora).filter(Objects::nonNull).filter(transportadora -> ObjetoUtil.isNotNull(transportadora.getTipoTransporte())).findFirst();

        final Optional<Cliente> optionalCliente = entidade.getItens().stream().map(ItemNFPedido::getCliente).filter(Objects::nonNull).filter(cliente -> ObjetoUtil.isNotNull(cliente.getTipoTransporte())).findFirst();

        TipoTransporteEnum tipoTransporte = null;

        if (optionalTransportadora.isPresent()) {

            tipoTransporte = optionalTransportadora.get().getTipoTransporte();

        } else if (optionalCliente.isPresent()) {

            tipoTransporte = optionalCliente.get().getTipoTransporte();

        } else {

            if (ObjetoUtil.isNull(material.getTipoTransporte())) {

                throw new ServicoException(MessageSupport.getMessage("MSGE035"));
            }

            tipoTransporte = material.getTipoTransporte();
        }
        return tipoTransporte;
    }

    @Override
    public void criarRemessa(final Romaneio entidade) {

        if (entidade.getOperacao().isGestaoPatio() && !entidade.getOperacao().getOperacao().equals("550") && !entidade.getOperacao().getOperacao().equals("560")) {

            if (!romaneioPossuiRestricaoPorOrdemVendaBloqueada(entidade)) {

                JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.values().stream().findFirst().orElse(null);

                if (ObjetoUtil.isNotNull(sap) && ColecaoUtil.isNotEmpty(entidade.getItens())) {

                    if (ColecaoUtil.isNotEmpty(entidade.getRestricoes())) {

                        entidade.getRestricoes()
                                .removeIf(r -> TipoRestricaoEnum.CRIAR_REMESSA.equals(r.getTipoRestricao()));

                    } else {

                        entidade.setRestricoes(new ArrayList<>());
                    }

                    entidade.getItens()
                            .stream()
                            .filter(item -> (ObjetoUtil.isNull(item.getStatusRemessa()) || StatusRemessaEnum.ERRO_AO_CRIAR
                                    .equals(item.getStatusRemessa())) && ObjetoUtil
                                    .isNotNull(item.getDeposito()))
                            .forEach(item -> {

                                final RfcCriarRemessaRequest request = new RfcCriarRemessaRequest();

                                final RfcCriarRemessaResponse response = new RfcCriarRemessaResponse();

                                request.setLocalExpedicao(StringUtil.completarZeroAEsquerda(
                                        4,
                                        item.getLocalExpedicao()
                                ));

                                request.setDataCriacao(DateUtil.format(
                                        "yyyyMMdd",
                                        DateUtil.hoje()
                                ));

                                request.setDesbloquear(FLAG_SAP);

                                request.setItens(Collections.singletonList(new RfcCriarRemessaItemRequest(
                                        StringUtil.completarZeroAEsquerda(
                                                10,
                                                item.getCodigoOrdemVenda()
                                        ),
                                        StringUtil.completarZeroAEsquerda(
                                                6,
                                                item.getItemOrdem()
                                        ),
                                        item.getUnidadeMedidaOrdem().toUpperCase(),
                                        item.getDeposito().getCodigo(),
                                        item.getQuantidadeCarregar()
                                )));

                                sap.call(
                                        request,
                                        response
                                );

                                final List<RfcReturnItem> erros = response.getTReturn()
                                        .stream()
                                        .filter(r -> "E".equals(r.getTipoMensagem()))
                                        .collect(Collectors.toList());

                                if (!erros.isEmpty()) {

                                    entidade.getRestricoes().addAll(erros.stream().map(r -> {

                                        final List<String> mensagensErro = Arrays.asList(new String[]{
                                                r.getMensagem(),
                                                r.getMensagemUm(),
                                                r.getMensagemDois(),
                                                r.getMensagemTres(),
                                                r.getMensagemQuatro()
                                        });

                                        return new RestricaoOrdem(
                                                mensagensErro.stream()
                                                        .filter(m -> StringUtil.isNotNullEmpty(m))
                                                        .collect(Collectors.joining("; ")),
                                                item.getCodigoOrdemVenda(),
                                                TipoRestricaoEnum.CRIAR_REMESSA,
                                                entidade
                                        );

                                    }).collect(Collectors.toList()));

                                    item.setStatusRemessa(StatusRemessaEnum.ERRO_AO_CRIAR);

                                } else {

                                    item.setNumeroRemessa(response.getNumeroRemessa() != null ? new Long(response.getNumeroRemessa())
                                            .toString() : null);

                                    item.setStatusRemessa(StatusRemessaEnum.CRIADA);
                                }
                            });

                    entidade.setStatusRomaneio(ColecaoUtil.isNotEmpty(entidade.getRestricoes()) ? StatusRomaneioEnum.PENDENTE : StatusRomaneioEnum.EM_PROCESSAMENTO);
                }
            }
        }
    }

    private boolean romaneioPossuiRestricaoPorOrdemVendaBloqueada(Romaneio entidade) {
        return ColecaoUtil.isNotEmpty(entidade.getRestricoes())
                && entidade.getRestricoes().stream().anyMatch(restricaoOrdem -> TipoRestricaoEnum.ORDEM_VENDA_BLOQUEADA.equals(restricaoOrdem.getTipoRestricao()));
    }

    @Override
    public void estornarRemessa(final Romaneio romaneio, final boolean deletarItensEstornados) {

        if (romaneio.getOperacao().isGestaoPatio()) {

            JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.values().stream().findFirst().orElse(null);

            if (!romaneioPossuiRestricaoPorOrdemVendaBloqueada(romaneio)) {

                if (sap != null && !romaneio.getItens().isEmpty()) {

                    if (ColecaoUtil.isNotEmpty(romaneio.getRestricoes())) {

                        romaneio.getRestricoes()
                                .removeIf(r -> TipoRestricaoEnum.ESTORNAR_REMESSA.equals(r.getTipoRestricao()));

                    } else {

                        romaneio.setRestricoes(new ArrayList<>());
                    }

                    if (romaneio.getOperacao().isCriarDocumentoTransporte()) {

                        deletarDocumentoTransporte(
                                romaneio,
                                sap,
                                romaneio.getItens()
                                        .stream()
                                        .filter(i -> StringUtil.isNotNullEmpty(i.getNumeroOrdemTransporte()))
                                        .map(ItemNFPedido::getNumeroOrdemTransporte)
                                        .findFirst()
                                        .orElse(null)
                        );
                    }

                    romaneio.getItens()
                            .stream()
                            .filter(item -> StringUtil.isNotNullEmpty(item.getNumeroRemessa()) && ObjetoUtil.isNotNull(item.getStatusRemessa()) && (StatusRemessaEnum.ESTORNADA
                                    .equals(item.getStatusRemessa()) || StatusRemessaEnum.ERRO_AO_ESTORNAR
                                    .equals(item.getStatusRemessa())))
                            .forEach(item -> {

                                final RfcEstornarRemessaRequest request = new RfcEstornarRemessaRequest();

                                request.setHeaderData(new RfcEstornarRemessaHeaderDataRequest(StringUtil.completarZeroAEsquerda(
                                        10,
                                        item.getNumeroRemessa()
                                )));

                                request.setHeaderControl(new RfcEstornarRemessaHeaderControlRequest(
                                        StringUtil.completarZeroAEsquerda(
                                                10,
                                                item.getNumeroRemessa()
                                        ),
                                        FLAG_SAP
                                ));

                                RfcEstornarRemessaResponse response = new RfcEstornarRemessaResponse();

                                sap.call(
                                        request,
                                        response
                                );

                                final List<RfcReturnItem> erros = response.getTReturn()
                                        .stream()
                                        .filter(r -> "E".equals(r.getTipoMensagem()))
                                        .collect(Collectors.toList());

                                if (!erros.isEmpty()) {

                                    romaneio.getRestricoes().addAll(erros.stream().map(r -> {

                                        final List<String> mensagensErro = Arrays.asList(new String[]{
                                                r.getMensagem(),
                                                r.getMensagemUm(),
                                                r.getMensagemDois(),
                                                r.getMensagemTres(),
                                                r.getMensagemQuatro()
                                        });

                                        return new RestricaoOrdem(
                                                mensagensErro.stream()
                                                        .filter(m -> StringUtil.isNotNullEmpty(m))
                                                        .collect(Collectors.joining("; ")),
                                                item.getCodigoOrdemVenda(),
                                                TipoRestricaoEnum.ESTORNAR_REMESSA,
                                                romaneio
                                        );

                                    }).collect(Collectors.toList()));

                                    item.setStatusRemessa(StatusRemessaEnum.ERRO_AO_ESTORNAR);
                                }
                            });

                    if (deletarItensEstornados) {

                        romaneio.getItens()
                                .removeIf(item -> StatusRemessaEnum.ESTORNADA.equals(item.getStatusRemessa()));
                    }

                    romaneio.setStatusRomaneio(ColecaoUtil.isNotEmpty(romaneio.getRestricoes()) ? StatusRomaneioEnum.PENDENTE : StatusRomaneioEnum.EM_PROCESSAMENTO);
                }
            }
        }
    }

    @Override
    public RfcOrdensVendaResponse buscarOrdens(
            Paginacao<ItemNFPedido> filtro
    ) {

        JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.values().stream().findFirst().orElse(null);

        final String codigoMaterial = ObjetoUtil.isNotNull(filtro) && ObjetoUtil.isNotNull(filtro.getEntidade()) && ObjetoUtil
                .isNotNull(filtro.getEntidade()
                        .getMaterial()) ? filtro.getEntidade()
                .getMaterial()
                .getCodigo() : null;

        final String codigoCliente = ObjetoUtil.isNotNull(filtro) && ObjetoUtil.isNotNull(filtro.getEntidade()) && ObjetoUtil
                .isNotNull(filtro.getEntidade()
                        .getCliente()) ? filtro.getEntidade()
                .getCliente()
                .getCodigo() : null;

        final RfcOrdensVendaRequest request = new RfcOrdensVendaRequest(
                StringUtil.completarZeroAEsquerda(
                        10,
                        codigoCliente
                ),
                "I130",
                StringUtil.completarZeroAEsquerda(
                        18,
                        codigoMaterial
                )
        );

        final RfcOrdensVendaResponse response = new RfcOrdensVendaResponse();

        sap.call(
                request,
                response
        );

        removerOrdensComStatusParametrizado(response);

        return response;
    }

    private void removerOrdensComStatusParametrizado(RfcOrdensVendaResponse response) {

        final Parametro parametroTipoOrdem = parametroService.obterPorTipoParametro(TipoParametroEnum.TIPO_ORDEM_INUTILIZADA);

        if (ObjetoUtil.isNotNull(parametroTipoOrdem)) {

            List<String> tipos = Arrays.asList(parametroTipoOrdem.getValor().split(";"));

            if (ColecaoUtil.isNotEmpty(tipos)) {

                response.getOrdens()
                        .removeIf(o -> tipos.stream().filter(Predicate.isEqual(o.getTipoOrdem())).count() > 0);
            }
        }
    }

    @Override
    public void validarStatusOrdemDeVenda(Romaneio entidade) {

        JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.values().stream().findFirst().orElse(null);

        if (sap != null) {

            RfcOrdemVendaRequest request = new RfcOrdemVendaRequest();

            request.setItem(new RfcOrdemVendaItemRequest(FLAG_SAP, FLAG_SAP, VAZIO, FLAG_SAP, FLAG_SAP, VAZIO, VAZIO,
                    VAZIO, VAZIO, VAZIO, VAZIO, VAZIO, VAZIO, VAZIO, VAZIO, VAZIO, VAZIO));

            request.setOrdensVenda(entidade.getItens()
                    .stream()
                    .map(ItemNFPedido::getCodigoOrdemVenda)
                    .filter(Objects::nonNull)
                    .map(ordem -> new RfcOrdemVendaDocumentoVendaRequest(StringUtil.completarZeroAEsquerda(10, ordem)))
                    .collect(Collectors.toList()));

            RfcOrdemVendaResponse response = new RfcOrdemVendaResponse();

            if (ColecaoUtil.isNotEmpty(request.getOrdensVenda())) {

                sap.call(request, response);

                if (ColecaoUtil.isNotEmpty(response.getCabecalhos())) {

                    if (ColecaoUtil.isNotEmpty(entidade.getRestricoes())) {

                        entidade.getRestricoes()
                                .removeIf(restricaoOrdem -> TipoRestricaoEnum.ORDEM_VENDA_BLOQUEADA.equals(restricaoOrdem
                                        .getTipoRestricao()));
                    }

                    response.getCabecalhos()
                            .stream()
                            .filter(rfcOrdemVendaCabecalhoSaidaResponse -> StringUtil.isNotNullEmpty(rfcOrdemVendaCabecalhoSaidaResponse
                                    .getStatus()))
                            .forEach(rfcOrdemVendaCabecalhoSaidaResponse -> {

                                entidade.setStatusRomaneio(StatusRomaneioEnum.PENDENTE);

                                if (ObjetoUtil.isNull(entidade.getRestricoes())) {

                                    entidade.setRestricoes(new ArrayList<>());
                                }

                                final StatusOrdemVendaEnum statusOrdemVendaEnum = StatusOrdemVendaEnum.obterDescricaoPorCodigoSap(rfcOrdemVendaCabecalhoSaidaResponse
                                        .getStatus());

                                entidade.getRestricoes().add(new RestricaoOrdem(
                                        MessageFormat.format(
                                                MessageSupport.getMessage("MSGE031"),
                                                new Long(rfcOrdemVendaCabecalhoSaidaResponse.getNumeroDocumentoVenda())
                                                        .toString(),
                                                ObjetoUtil.isNotNull(statusOrdemVendaEnum) ? statusOrdemVendaEnum
                                                        .getDescricao() : "STATUS " + rfcOrdemVendaCabecalhoSaidaResponse
                                                        .getStatus() + " NÃO FOI IMPLEMENTADA NA WEB"
                                        ),
                                        new Long(rfcOrdemVendaCabecalhoSaidaResponse.getNumeroDocumentoVenda())
                                                .toString(),
                                        TipoRestricaoEnum.ORDEM_VENDA_BLOQUEADA,
                                        entidade
                                ));
                            });
                }
            }
        }
    }

    public SaldoPedidoEToleranciaPedidoTransferenciaResponse buscarSaldoPedidoEToleranciaPedidoTransferencia(final String pedido, final String item, final String operacao) throws Exception {

        RfcSaldoOrdemPedidoTransferenciaResponse response = this.buscarSaldoPedidoTransferencia(pedido, item, operacao);

        final BigDecimal saldoPedido = response.getSaldo();

        final BigDecimal tolerancia = response.getTolerancia();

        final BigDecimal saldoReservado = this.romaneioService.obterSaldoReservado(pedido, item);

        final String unidadeMedida = response.getUnidadeMedida();

        final Produtor produtor = StringUtil.isNotNullEmpty(response.getProdutor()) ? this.produtorService.obterPorCodigo(new Long(response.getProdutor()).toString()) : null;

        final Safra safra = StringUtil.isNotNullEmpty(response.getSafra()) ? this.safraService.obterPorCodigo(response.getSafra()) : null;

        final Centro centroLogadoPermitido = StringUtil.isNotNullEmpty(response.getCentroLogadoPermitido()) ? this.centroService.obterPorCodigo(new Long(response.getCentroLogadoPermitido()).toString()) : null;

        final Centro centro = StringUtil.isNotNullEmpty(response.getCentro()) ? this.centroService.obterPorCodigo(new Long(response.getCentro()).toString()) : null;

        final Deposito deposito = StringUtil.isNotNullEmpty(response.getDeposito()) ? this.depositoService.obterUnicoPorCodigo(response.getDeposito()) : null;

        final Material material = StringUtil.isNotNullEmpty(response.getMaterial()) ? this.materialService.obterPorCodigo(new Long(response.getMaterial()).toString()) : null;

        final OrigemClassificacaoEnum localPesagem = StringUtil.isNotNullEmpty(response.getLocalPesagem()) ? OrigemClassificacaoEnum.getLocalPesagem(response.getLocalPesagem()) : null;

        return SaldoPedidoEToleranciaPedidoTransferenciaResponse.builder().unidadeMedida(unidadeMedida)
                .saldoPedido(saldoPedido).toleranciaPedido(tolerancia).saldoReservado(saldoReservado)
                .saldoDisponivel(saldoPedido.subtract(saldoReservado)).localPesagem(localPesagem)
                .produtor(produtor).safra(safra).centro(centro).deposito(deposito).material(material)
                .centroLogadoPermitido(centroLogadoPermitido).build();
    }

    private void validarStatusPedido(
            final JCoCallerService sap, final String pedido
    ) throws Exception {

        final RfcStatusPedidoRequest request = new RfcStatusPedidoRequest();

        final RfcStatusPedidoResponse response = new RfcStatusPedidoResponse();

        request.setTEbeln(Arrays.asList(pedido)
                .stream()
                .map(i -> new RfcStatusPedidoRequestItem(i))
                .collect(Collectors.toList()));

        sap.call(
                request,
                response
        );

        if (ColecaoUtil.isNotEmpty(response.getTStatus())) {

            final StatusPedidoEnum statusPedido = response.getTStatus()
                    .stream()
                    .map(RfcStatusPedidoResponseItem::getStatusPedido)
                    .findFirst()
                    .orElse(null);

            if (ObjetoUtil.isNotNull(statusPedido) && StatusPedidoEnum.EM_APROVACAO.equals(statusPedido)) {

                throw new Exception(MessageFormat.format(
                        MessageSupport.getMessage(MSG_PEDIDO_NAO_APROVADO),
                        pedido
                ));
            }
        }
    }

}
