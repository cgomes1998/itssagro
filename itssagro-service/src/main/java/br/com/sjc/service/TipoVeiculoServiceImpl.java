package br.com.sjc.service;

import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.TipoVeiculoService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.modelo.dto.TipoVeiculoDTO;
import br.com.sjc.modelo.dto.TipoVeiculoSetaDTO;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.sap.TipoVeiculo;
import br.com.sjc.modelo.sap.TipoVeiculoSeta;
import br.com.sjc.modelo.sap.request.EventOutRequest;
import br.com.sjc.modelo.sap.sync.SAPTipoVeiculo;
import br.com.sjc.persistencia.dao.TipoVeiculoDAO;
import br.com.sjc.persistencia.dao.fachada.TipoVeiculoCustomRepository;
import br.com.sjc.persistencia.dao.fachada.TipoVeiculoSetaCustomRepository;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class TipoVeiculoServiceImpl extends ServicoGenerico<Long, TipoVeiculo> implements TipoVeiculoService {

    private static final Logger LOGGER = Logger.getLogger(TipoVeiculoServiceImpl.class.getName());

    @Autowired
    private TipoVeiculoDAO dao;

    @Autowired
    private TipoVeiculoCustomRepository tipoVeiculoCustomRepository;

    @Autowired
    private TipoVeiculoSetaCustomRepository tipoVeiculoSetaCustomRepository;

    @Override
    public List<TipoVeiculo> listar() {

        List<TipoVeiculo> tipoVeiculos = tipoVeiculoCustomRepository.listarPorStatus(StatusEnum.ATIVO);

        if (ColecaoUtil.isNotEmpty(tipoVeiculos)) {
            List<Long> ids = tipoVeiculos.stream().map(TipoVeiculo::getId).collect(Collectors.toList());

            List<TipoVeiculoSeta> tipoVeiculoSetas = tipoVeiculoSetaCustomRepository.listarPorTipoVeiculo(ids);

            tipoVeiculos.stream().filter(tipoVeiculo -> tipoVeiculoSetas.stream().filter(seta -> ObjetoUtil.isNotNull(seta.getTipoVeiculo()) && seta.getTipoVeiculo().getId().equals(tipoVeiculo.getId())).count() > 0).forEach(tipoVeiculo -> tipoVeiculo.setSetas(tipoVeiculoSetas.stream().filter(seta -> ObjetoUtil.isNotNull(seta.getTipoVeiculo()) && seta.getTipoVeiculo().getId().equals(tipoVeiculo.getId())).collect(Collectors.toList())));
        }

        return tipoVeiculos;
    }

    @Override
    public List<TipoVeiculoSetaDTO> listarSetasDtoPorTipoVeiculo(final Long id) {

        return tipoVeiculoSetaCustomRepository.listarDtoPorTipoVeiculo(id);
    }

    @Override
    public Specification<TipoVeiculo> obterEspecification(final Paginacao<TipoVeiculo> paginacao) {

        final TipoVeiculo entidade = paginacao.getEntidade();

        final Specification<TipoVeiculo> specification = new Specification<TipoVeiculo>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<TipoVeiculo> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                if (StringUtil.isNotNullEmpty(entidade.getCodigo())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("codigo")), "%" + entidade.getCodigo().toUpperCase() + "%")));
                }

                if (StringUtil.isNotNullEmpty(entidade.getDescricao())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("descricao")), "%" + entidade.getDescricao().toUpperCase() + "%")));
                }

                if (StringUtil.isNotNullEmpty(entidade.getTipo())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("tipo")), "%" + entidade.getTipo().toUpperCase() + "%")));
                }

                if (StringUtil.isNotNullEmpty(entidade.getTextoTipo())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("textoTipo")), "%" + entidade.getTextoTipo().toUpperCase() + "%")));
                }

                if (ObjetoUtil.isNotNull(entidade.getStatus())) {

                    predicates.add(builder.equal(root.get("status"), entidade.getStatus()));
                }

                if (ObjetoUtil.isNotNull(entidade.getStatusCadastroSap())) {

                    predicates.add(builder.equal(root.get("statusCadastroSap"), entidade.getStatusCadastroSap()));
                }

                query.orderBy(builder.asc(root.get("codigo")));

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Override
    public void preSalvar(TipoVeiculo entidade) {

        super.preSalvar(entidade);

        entidade.getSetas().forEach(seta -> seta.setTipoVeiculo(entidade));
    }

    @Override
    public void salvarSAP(final EventOutRequest request, final String host, String mandante) {

        if (ColecaoUtil.isNotEmpty(request.getData())) {

            final Map<String, SAPTipoVeiculo> groupTipoVeiculo = this.groupTipoVeiculo(request);

            for (final String itemGrupo : groupTipoVeiculo.keySet()) {

                final SAPTipoVeiculo tipoVeiculo = groupTipoVeiculo.get(itemGrupo);

                TipoVeiculo entidadeSaved = this.dao.findByCodigo(tipoVeiculo.getCodigo());

                entidadeSaved = tipoVeiculo.copy(entidadeSaved);

                if (StringUtil.isNotNullEmpty(entidadeSaved.getCodigo())) {

                    this.salvar(entidadeSaved);
                }
            }
        }
    }

    private Map<String, SAPTipoVeiculo> groupTipoVeiculo(final EventOutRequest request) {

        final Map<String, SAPTipoVeiculo> groupTipoVeiculo = new HashMap<String, SAPTipoVeiculo>();

        request.getData().forEach(registro -> {

            SAPTipoVeiculo entidade = groupTipoVeiculo.get(registro.getItemDocumento().trim());

            try {

                if (ObjetoUtil.isNull(entidade)) {

                    entidade = SAPTipoVeiculo.newInstance();
                }

                entidade.update(registro.getCampo().trim(), registro.getValor().trim());

                groupTipoVeiculo.put(registro.getItemDocumento().trim(), entidade);

            } catch (final Exception e) {

                TipoVeiculoServiceImpl.LOGGER.info("ERRO: " + e.getMessage());
            }
        });

        return groupTipoVeiculo;
    }

    @Override
    public List<TipoVeiculo> listarSincronizados(final Date data) {

        return ObjetoUtil.isNull(data) ? this.dao.findByStatusCadastroSapIn(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO})) :
                this.dao.findByStatusCadastroSapInAndDataCadastroGreaterThanEqual(Arrays.asList(new StatusCadastroSAPEnum[]{StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO}), data);
    }

    @Override
    public void sincronizar(final List<TipoVeiculo> entidades) {

        if (ColecaoUtil.isNotEmpty(entidades)) {

            entidades.forEach(item -> {

                TipoVeiculo entidadeSaved = this.getDAO().findByCodigo(item.getCodigo());

                item.setId(null);

                if (ObjetoUtil.isNotNull(entidadeSaved)) {

                    item.setId(entidadeSaved.getId());
                }
            });

            this.salvar(entidades);
        }
    }

    @Override
    public TipoVeiculoDAO getDAO() {

        return this.dao;
    }

    @Override
    public Class<TipoVeiculoDTO> getDtoListagem() {

        return TipoVeiculoDTO.class;
    }

    @Override
    public Boolean getRegistrarLogAtividade() {

        return Boolean.TRUE;
    }

}
