package br.com.sjc.service;

import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.CampoOperacaoService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.cfg.CampoOperacao;
import br.com.sjc.modelo.dto.CampoOperacaoDTO;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.persistencia.dao.CampoOperacaoDAO;
import br.com.sjc.persistencia.dao.fachada.CampoOperacaoCustomRepository;
import br.com.sjc.util.ObjetoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class CampoOperacaoServiceImpl extends ServicoGenerico<Long, CampoOperacao> implements CampoOperacaoService {

    @Autowired
    private CampoOperacaoDAO dao;

    @Autowired
    private CampoOperacaoCustomRepository campoOperacaoCustomRepository;

    @Override
    public void salvar(List<CampoOperacao> lista) {

        lista.stream().forEach(item -> dao.save(item));
    }

    @Override
    public Specification<CampoOperacao> obterEspecification(final Paginacao<CampoOperacao> paginacao) {

        final CampoOperacao entidade = paginacao.getEntidade();

        final Specification<CampoOperacao> specification = new Specification<CampoOperacao>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<CampoOperacao> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                if (ObjetoUtil.isNotNull(entidade.getOperacao())) {

                    predicates.add(builder.equal(root.get("operacao"), entidade.getOperacao()));
                }

                query.orderBy(builder.asc(root.get("display")));

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Override
    @Transactional(readOnly = true)
    public List<CampoOperacaoDTO> obterCamposPorOperacao(final DadosSincronizacaoEnum dadosSincronizacaoEnum) {

        return campoOperacaoCustomRepository.listar(dadosSincronizacaoEnum);
    }

    @Override
    public CampoOperacaoDAO getDAO() {

        return this.dao;
    }

    @Override
    public Class<CampoOperacaoDTO> getDtoListagem() {

        return CampoOperacaoDTO.class;
    }

}
