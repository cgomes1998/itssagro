package br.com.sjc.service;

import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.DadosSincronizacaoService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.dto.DadosSincronizacaoDTO;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.enums.ModuloOperacaoEnum;
import br.com.sjc.persistencia.dao.DadosSincronizacaoDAO;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class DadosSincronizacaoServiceImpl extends ServicoGenerico<Long, DadosSincronizacao> implements DadosSincronizacaoService {

    @Autowired
    private DadosSincronizacaoDAO dao;

    @Override
    public Specification<DadosSincronizacao> obterEspecification(final Paginacao<DadosSincronizacao> paginacao) {

        final DadosSincronizacao entidade = paginacao.getEntidade();

        final Specification<DadosSincronizacao> specification = new Specification<DadosSincronizacao>() {

            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(final Root<DadosSincronizacao> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

                final List<Predicate> predicates = new ArrayList<>();

                if (StringUtil.isNotNullEmpty(entidade.getOperacao())) {

                    predicates.add(builder.equal(root.get("operacao"), entidade.getOperacao()));
                }

                if (StringUtil.isNotNullEmpty(entidade.getDescricao())) {

                    predicates.add(builder.and(builder.like(builder.upper(root.get("descricao")), "%" + entidade.getDescricao().toUpperCase() + "%")));
                }

                if (ObjetoUtil.isNotNull(entidade.getModulo())) {

                    predicates.add(builder.equal(root.get("modulo"), entidade.getModulo()));
                }

                return builder.and(predicates.toArray(new Predicate[]{}));
            }
        };

        return specification;
    }

    @Override
    public List<DadosSincronizacao> listarOperacoes() {

        return this.getDAO().findByModuloInOrderByOperacaoAsc(Arrays.asList(new ModuloOperacaoEnum[]{ModuloOperacaoEnum.ROMANEIO, ModuloOperacaoEnum.COMPRAS}));
    }

    @Override
    public DadosSincronizacao obterPorOperacao(final String operacao) {

        return this.getDAO().findByModuloInAndOperacao(Arrays.asList(new ModuloOperacaoEnum[]{ModuloOperacaoEnum.ROMANEIO, ModuloOperacaoEnum.COMPRAS}), operacao);
    }

    @Override
    public DadosSincronizacao obterPorModuloEOperacao(final ModuloOperacaoEnum moduloOperacao, final String operacao) {

        return this.getDAO().findByModuloAndOperacao(moduloOperacao, operacao);
    }

    @Override
    public DadosSincronizacao obterPorEntidade(final DadosSincronizacaoEnum entidade) {

        return this.getDAO().findByEntidade(entidade);
    }

    @Override
    public DadosSincronizacaoDAO getDAO() {

        return this.dao;
    }

    @Override
    public Class<DadosSincronizacaoDTO> getDtoListagem() {

        return DadosSincronizacaoDTO.class;
    }

}
