package br.com.sjc.service;

import br.com.sjc.fachada.service.LogSenhaService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.LogSenha;
import br.com.sjc.modelo.dto.LogSenhaDTO;
import br.com.sjc.persistencia.dao.LogSenhaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogSenhaServiceImpl extends ServicoGenerico<Long, LogSenha> implements LogSenhaService {


    @Autowired
    private LogSenhaDAO dao;

    @Override
    public Class<LogSenhaDTO> getDtoListagem() {
        return LogSenhaDTO.class;
    }

    @Override
    public LogSenhaDAO getDAO() {
        return dao;
    }

}
