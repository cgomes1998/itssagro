package br.com.sjc.service;

import br.com.sjc.arquitetura.providers.RFCMultipleProviders;
import br.com.sjc.fachada.service.IvaService;
import br.com.sjc.fachada.servico.impl.ServicoGenerico;
import br.com.sjc.modelo.sap.Iva;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoCallerService;
import br.com.sjc.modelo.sap.request.RfcIvaRequest;
import br.com.sjc.modelo.sap.response.RfcIvaResponse;
import br.com.sjc.modelo.sap.sync.SAPSync;
import br.com.sjc.persistencia.dao.IvaDAO;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Service
public class IvaServiceImpl extends ServicoGenerico<Long, Iva> implements IvaService {

    private static final Logger LOGGER = Logger.getLogger(IvaServiceImpl.class.getName());

    @Autowired
    private IvaDAO dao;

    @Override
    public List<Iva> listarIvasOrdernandoPorCodigoAcrescente() {
        return this.getDAO().findByOrderByCodigoAsc();
    }

    @Override
    public void sincronizar() {

        if (SAPSync.ONLINE) {

            final RfcIvaRequest request = new RfcIvaRequest();

            final RfcIvaResponse response = new RfcIvaResponse();

            final Map<Long, JCoCallerService> connectionsMap = RFCMultipleProviders.DESTINATIONS;

            if (ObjetoUtil.isNotNull(connectionsMap)) {

                connectionsMap.keySet().forEach(key -> {

                    try {

                        final JCoCallerService sap = RFCMultipleProviders.DESTINATIONS.get(key);

                        sap.call(request, response);

                        this.salvar(response);

                    } catch (Exception e) {

                        IvaServiceImpl.LOGGER.info(MessageFormat.format("ERRO AO BUSCAR IVAS: {0}", e.getMessage()));
                    }
                });
            }
        }
    }

    private void salvar(final RfcIvaResponse response) {

        if (ColecaoUtil.isNotEmpty(response.getIvas())) {

            response.getIvas().stream().forEach(item -> {

                Iva entidadeSaved = this.getDAO().findByCodigo(item.getCodigo());

                entidadeSaved = item.toEntidade(entidadeSaved);

                this.salvar(entidadeSaved);
            });
        }
    }

    @Override
    public IvaDAO getDAO() {
        return this.dao;
    }
}
