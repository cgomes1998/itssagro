package br.com.sjc.socket;

import br.com.sjc.util.DateUtil;
import br.com.sjc.util.StringUtil;
import org.apache.commons.net.telnet.TelnetClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class TcpIpConectService {

    private static final Logger LOGGER = Logger.getLogger ( TcpIpConectService.class.getName () );

    public static Double obterPeso (
            final String ip,
            final Integer porta,
            final Integer salto,
            final Integer tempoEsperandoDados,
            final int timeout
    ) throws Exception {

        Double peso = new Double ( 0.0 );

        TelnetClient client = new TelnetClient ();

        try {

            client.connect ( ip, porta );

            peso = TcpIpConectService.getPesoFromInputStream ( client.getInputStream (), salto, tempoEsperandoDados );

        } catch ( final Exception e ) {

            e.printStackTrace ();

        } finally {

            if ( client.isConnected () ) {

                client.disconnect ();
            }
        }

        return peso;
    }

    public static Double pesoFormatado (
            String retornoBalanca,
            int tamanhoSalto
    ) {

        try {

            if ( StringUtil.isNotNullEmpty ( retornoBalanca ) ) {

                final int tamanhoPeso = 12;

                retornoBalanca = retornoBalanca.substring(retornoBalanca.length() - tamanhoPeso);

                retornoBalanca = StringUtil.apenasNumeros ( retornoBalanca );

                retornoBalanca = new Long ( retornoBalanca ).toString ();

                retornoBalanca = retornoBalanca.substring ( 0, retornoBalanca.length () - tamanhoSalto );

                return Double.valueOf ( retornoBalanca );
            }

            return new Double ( 0 );

        } catch ( Exception e ) {

            return new Double ( 0 );
        }
    }

    private static Double getPesoFromInputStream (
            InputStream is,
            int tamanhoSalto,
            int tempoMaximoLeitura
    ) throws IOException, InterruptedException {

        List<Double> valoreEstabilizados = new ArrayList<Double> ();

        BufferedReader entrada = null;

        for ( int contador = 0; contador < tempoMaximoLeitura; contador++ ) {

            entrada = new BufferedReader ( new InputStreamReader ( is ) );

            Double valor = pesoFormatado ( entrada.readLine (), tamanhoSalto );

            TcpIpConectService.LOGGER.warning ( MessageFormat.format ( "RETORNO DA BALANÇA: {0}", entrada.readLine () ) );
            TcpIpConectService.LOGGER.warning ( MessageFormat.format ( "PESO FORMATADO: {0}", valor ) );

            valoreEstabilizados.add ( valor );

            Thread.sleep ( 1000 );
        }

        Map<Double, Integer> numerosRepetidos = new HashMap<> ();

        valoreEstabilizados.forEach ( n -> {

            if ( numerosRepetidos.isEmpty () || !numerosRepetidos.keySet ().stream ().filter ( i -> i.equals ( n ) ).findFirst ().isPresent () ) {

                numerosRepetidos.put ( n, 1 );

            } else {

                numerosRepetidos.put ( n, Long.valueOf ( numerosRepetidos.keySet ().stream ().filter ( i -> i.equals ( n ) ).count () ).intValue () );
            }
        } );

        numerosRepetidos.keySet ().stream ().sorted ( ( n1, n2 ) -> n2.compareTo ( n1 ) );

        entrada.close ();

        return valoreEstabilizados.get ( 0 );
    }

    public static Double testeConexaoServidor (
            final String ip,
            final Integer porta,
            final Integer salto,
            final Integer tempoEsperandoDados
    ) throws Exception {

        TelnetClient client = new TelnetClient ();

        client.connect ( ip, porta );

        Double valor = TcpIpConectService.getPesoFromInputStream ( client.getInputStream (), salto, tempoEsperandoDados );

        return valor;
    }
}
