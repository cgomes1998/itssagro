package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.BaseEndpoint;
import br.com.sjc.fachada.service.DashboardFilaService;
import br.com.sjc.modelo.TipoFluxoEnum;
import br.com.sjc.modelo.dto.*;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.relatorio.GerarRelatorioExcel;
import br.com.sjc.modelo.response.FileResponse;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Component
@Path("fluxo/dashboard-fila")
public class DashboardFilaEndpoint extends BaseEndpoint {

    private static final String FILAS = "filas";

    @Autowired
    private DashboardFilaService dashboardFilaService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response obterFilas(DashboardFilaFiltroDTO filtro) {

        try {

            List<EtapaFilaDTO> filas = this.dashboardFilaService.obterFilas(filtro);

            return Response.ok(MapBuilder.create(FILAS, filas).build()).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("obter-rel-fila")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<byte[]> obterRelatorioFila(final RelatorioFilaFiltroDTO filtro) {

        try {

            List<SenhaEtapaRelatorioDTO> senhasEtapas = this.dashboardFilaService.obterFilasRelatorio(filtro);

            if (ColecaoUtil.isEmpty(senhasEtapas)) {
                throw new Exception("Nenhum registro encontrado de acordo com os parâmetros informados.");
            }

            FileResponse file = GerarRelatorioExcel.obterRelatorioFilas(senhasEtapas);

            HttpHeaders headers = new HttpHeaders();

            headers.setContentDispositionFormData(file.getName(), file.getName());

            return new ResponseEntity<byte[]>(file.getBuffer(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    @POST
    @Path("/validar-condicao-saida-etapa")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response validarCondicaoSaidaEtapa(EtapaSenhaFilaDTO etapaSenhaFila) {

        try {

            this.dashboardFilaService.validarCondicaoSaidaEtapa(etapaSenhaFila);

            return Response.ok().build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("/estornar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response estornar(EtapaSenhaFilaDTO etapaSenhaFila) {

        try {

            this.dashboardFilaService.estornar(etapaSenhaFila);

            return Response.ok().build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("/obterFluxoEtapaPorMaterialEEtapa/{tipoFluxo}/{etapaEnum}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response obterFluxoEtapaPorMaterialEEtapa(@PathParam("tipoFluxo") TipoFluxoEnum tipoFluxo, Material material, @PathParam("etapaEnum") EtapaEnum etapaEnum) {

        return Response.ok(dashboardFilaService.obterFluxoEtapaPorMaterialEEtapa(tipoFluxo, material, etapaEnum)).build();
    }

}
