package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.TransportadoraService;
import br.com.sjc.modelo.sap.Transportadora;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Component
@Path("dados-mestres/transportadora")
public class TransportadoraEndpoint extends ManutencaoEndpoint<Transportadora> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private TransportadoraService servico;

    @GET
    @Path("/buscar-transportadoras/{value}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarTransportadorasAutoComplete(@PathParam("value") String value) {

        List<Transportadora> transportadoras = this.servico.buscarTransportadorasAutoComplete(value);

        return Response.ok(transportadoras).build();
    }

    @Override
    protected TransportadoraService getService() {
        return servico;
    }
}
