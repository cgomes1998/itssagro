package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.dto.SelectItemDTO;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.HardwareService;
import br.com.sjc.modelo.Hardware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by julio.bueno on 02/07/2019.
 */
@Component
@Path("cadastros/hardware")
public class HardwareEndpoint extends ManutencaoEndpoint<Hardware> {

    @Autowired
    private HardwareService hardwareService;

    @GET
    @Path("/listarItem")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public List<SelectItemDTO<Hardware>> listarItem() {
        List<SelectItemDTO<Hardware>> hardwares = new ArrayList<>();
        hardwares.add(new SelectItemDTO<Hardware>("Selecione", null));
        hardwares.addAll(this.getService().listarHardwaresAtivos().stream().map(i -> new SelectItemDTO<Hardware>(i.toString(), i)).collect(Collectors.toList()));
        return hardwares;
    }

    @Override
    protected HardwareService getService() {
        return hardwareService;
    }
}
