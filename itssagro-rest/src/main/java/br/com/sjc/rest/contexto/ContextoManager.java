package br.com.sjc.rest.contexto;

import br.com.sjc.jobs.mid.JOBPre;
import br.com.sjc.jobs.mid.JOBSync;
import br.com.sjc.jobs.sap.RFCServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import java.util.HashMap;
import java.util.Map;

@Component
public class ContextoManager {

    private static final String MANAGER_CONTEXT = "MANAGER.CONTEXT.APP";

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private RFCServer rfcServer;

    @Autowired
    private JOBSync jobSync;

    @Autowired
    private JOBPre jobPre;

    @Value("${spring.quartz.job-enable}")
    private boolean jobAtivo;

    @Value("${sap.connection-enable}")
    private boolean conexaoSapAtiva;

    @PostConstruct
    public void init() {

        this.servletContext.setAttribute(MANAGER_CONTEXT, new HashMap<String, Object>());

        this.rfcServer.injetarDependeciasEmMemoria();

        if (conexaoSapAtiva) {

            this.rfcServer.iniciarServicosSAP();
        }

        if (jobAtivo) {

            this.jobPre.iniciarJobs();

            this.jobSync.iniciarJobs();
        }
    }

    @SuppressWarnings("unchecked")
    public void armazenar(String chave, Object valor) {

        ((Map<String, Object>) this.servletContext.getAttribute(MANAGER_CONTEXT)).put(chave, valor);
    }

    @SuppressWarnings("unchecked")
    public <T> T obter(String chave) {

        return (T) ((Map<String, Object>) this.servletContext.getAttribute(MANAGER_CONTEXT)).get(chave);
    }

    @SuppressWarnings("unchecked")
    public void remover(String chave) {

        ((Map<String, Object>) this.servletContext.getAttribute(MANAGER_CONTEXT)).remove(chave);
    }

    @SuppressWarnings("unchecked")
    public boolean existe(String chave) {

        return ((Map<String, Object>) this.servletContext.getAttribute(MANAGER_CONTEXT)).containsKey(chave);
    }
}
