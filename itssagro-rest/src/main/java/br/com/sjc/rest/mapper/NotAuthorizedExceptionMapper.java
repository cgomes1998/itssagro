package br.com.sjc.rest.mapper;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NotAuthorizedExceptionMapper implements ExceptionMapper<NotAuthorizedException> {

    public Response toResponse(NotAuthorizedException exception) {

        return MapperUtils.create(Response.Status.UNAUTHORIZED, exception.getMessage());
    }
}