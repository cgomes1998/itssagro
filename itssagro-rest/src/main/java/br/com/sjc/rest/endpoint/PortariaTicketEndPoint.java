package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.BaseEndpoint;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.PortariaTicketService;
import br.com.sjc.modelo.PortariaTicket;
import br.com.sjc.modelo.response.FileResponse;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("fluxo/portaria-ticket")
@PermitAll
public class PortariaTicketEndPoint extends ManutencaoEndpoint<PortariaTicket> {
    @Autowired
    private PortariaTicketService portariaTicketService;

    @Override
    protected PortariaTicketService getService() {
        return portariaTicketService;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response salvar(PortariaTicket entidade) {

        try {

            this.atribuirUsuario(entidade);

            PortariaTicket saved = this.getService().salvar(entidade);

            return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSG001")).map("entity", saved).build()).build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response alterar(PortariaTicket entidade) {

        try {

            this.atribuirUsuario(entidade);

            PortariaTicket saved = this.getService().salvar(entidade);

            return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSG002")).map("entity", saved).build()).build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @PUT
    @Path("/informar-saida/{senha}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response informarSaida(@PathParam("senha") Long id) {
        try {
            PortariaTicket ticket = this.getService().informarSaida(id);
            return Response.ok(ticket).build();
        } catch (Exception e) {
            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @PUT
    @Path("/informar-entrada/{senha}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response informarEntrada(@PathParam("senha") Long id) {
        try {
            PortariaTicket ticket = this.getService().informarEntrada(id);
            return Response.ok(ticket).build();
        } catch (Exception e) {
            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/imprimir-ticket-marcacao/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response imprimirTicketMarcacao(@PathParam("id") Long id) {

        try {

            this.portariaTicketService.imprimirTicketMarcacao(id);

            return Response.noContent().build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, ObjetoUtil.isNotNull(e.getMessage()) ? e.getMessage() : e.getCause()).build()).build();
        }
    }

    @GET
    @Path("/imprimir-ticket-entrada/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response imprimirTicketEntrada(@PathParam("id") Long id) {

        try {

            this.portariaTicketService.imprimirTicketEntrada(id);

            return Response.noContent().build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, ObjetoUtil.isNotNull(e.getMessage()) ? e.getMessage() : e.getCause()).build()).build();
        }
    }


    @GET
    @Path("/imprimir-ticket-saida/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response imprimirTicketSaida(@PathParam("id") Long id) {

        try {
            this.portariaTicketService.imprimirTicketSaida(id);

            return Response.noContent().build();
        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, ObjetoUtil.isNotNull(e.getMessage()) ? e.getMessage() : e.getCause()).build()).build();
        }
    }


    @POST
    @Path("obter-rel-portaria-ticket")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<byte[]> obterRelatorioPortariaTicket(final PortariaTicket filtro) {

        try {

            FileResponse file = portariaTicketService.obterRelatorioPortariaTicket(filtro);

            HttpHeaders headers = new HttpHeaders();

            headers.setContentDispositionFormData(file.getName(), file.getName());

            return new ResponseEntity<byte[]>(file.getBuffer(), headers, HttpStatus.OK);

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

}
