package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.AnaliseQualidadeService;
import br.com.sjc.fachada.service.ParametroService;
import br.com.sjc.fachada.service.RomaneioService;
import br.com.sjc.fachada.service.SenhaService;
import br.com.sjc.modelo.Arquivo;
import br.com.sjc.modelo.Senha;
import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.dto.AutoAtendimentoMotorista;
import br.com.sjc.modelo.dto.ItemNFPedidoDTO;
import br.com.sjc.modelo.enums.TipoParametroEnum;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.service.AutoatendimentoMotoristaServiceImpl;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.rest.MapBuilder;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Component
@Path("autoatendimento/motorista")
public class AutoatendimentoMotoristaEndpoint extends ManutencaoEndpoint<Romaneio> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private RomaneioService romaneioService;

    @Autowired
    private AnaliseQualidadeService analiseQualidadeService;

    @Autowired
    private AutoatendimentoMotoristaServiceImpl autoatendimentoMotoristaServiceImpl;

    @Autowired
    private SenhaService senhaService;

    @Autowired
    private ParametroService parametroService;

    @GET
    @Path("obterItensDeRomaneioParaEntradaLiberada")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ItemNFPedidoDTO> obterItensDeRomaneioParaEntradaLiberada() {

        return romaneioService.obterItensDeRomaneioParaEntradaLiberada();
    }

    @GET
    @Path("obterRomaneiosParaAutoatendimento")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ItemNFPedidoDTO> obterRomaneiosParaAutoatendimento() {

        return romaneioService.obterRomaneiosParaAutoatendimento();
    }

    @POST
    @Path("imprimirSenha/{senha}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response imprimirSenha(@PathParam("senha") Long senha) {

        Response response;

        try {
            Parametro parametro = parametroService.obterPorTipoParametro(TipoParametroEnum.TIPO_IMPRESSORA_ENTRADA_MOTORISTA);

            if(parametro != null && StringUtil.isNotNullEmpty(parametro.getValor()) && parametro.getValor().equals("bematech")){
                Senha senhaEntity = senhaService.get(senha);
                autoatendimentoMotoristaServiceImpl.imprimirSenhaBematech(senhaEntity);
            } else {
                final byte[] pdfBytes = senhaService.imprimirPdf(senha);
                autoatendimentoMotoristaServiceImpl.imprimirSenha(Arquivo.arquivoBuilder().buffer(pdfBytes).nome("senha_" + DateUtil.formatData_ddMMyyyyHHmmss_semCaracteres(DateUtil.hoje()) + ".pdf").build());
            }

            response = Response.noContent().build();

        } catch (Exception e) {

            response = Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }

        return response;
    }

    @POST
    @Path("imprimirArquivos/{numeroRomaneio}/{numeroCartaoAcesso}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response imprimirArquivos(@PathParam("numeroRomaneio") String numeroRomaneio, @PathParam("numeroCartaoAcesso") Integer numeroCartaoAcesso, List<Arquivo> arquivos) {

        Response response;

        try {

            autoatendimentoMotoristaServiceImpl.imprimirArquivos(arquivos);

            response = Response.noContent().build();

            romaneioService.atualizarDadosAtendimento(numeroRomaneio, numeroCartaoAcesso, Boolean.TRUE, DateUtil.hoje());

        } catch (ServicoException e) {

            response = Response.ok(MapBuilder.create("erro", e.getMessage()).build()).build();
        }

        return response;
    }

    @POST
    @Path("/obterRomaneioParaAutoatendimento")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterRomaneioParaAutoatendimento(AutoAtendimentoMotorista entidadeFiltro) {

        return Response.ok(romaneioService.obterRomaneioParaAutoatendimento(entidadeFiltro).orElse(null)).build();
    }

    @POST
    @Path("imprimirRomaneioParaAutoatendimento")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response imprimirRomaneioParaAutoatendimento(Romaneio romaneio) {

        Response response = null;

        try {

            final byte[] pdfBytes = this.getService().imprimirPdf(romaneio, null);

            if (ObjetoUtil.isNull(pdfBytes)) {

                throw new Exception("Não foi possível realizar a impressão do PDF do Romaneio, contate o administrador.");
            }

            response = Response.ok(MapBuilder.create("pdfBytes", pdfBytes).build()).build();

        } catch (JRException e) {

            e.printStackTrace();

            response = Response.ok(MapBuilder.create("erro", e.getMessage()).build()).build();

        } catch (Exception e) {

            e.printStackTrace();

            response = Response.ok(MapBuilder.create("erro", e.getMessage()).build()).build();
        }

        return response;
    }

    @POST
    @Path("imprimirLaudoParaAutoatendimento")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response imprimirLaudoParaAutoatendimento(Romaneio romaneio) {

        Response response = null;

        try {

            final byte[] pdfBytes = analiseQualidadeService.imprimirRomaneio(null, romaneio);

            if (ObjetoUtil.isNull(pdfBytes)) {

                throw new Exception("Não foi possível realizar a impressão do PDF do Romaneio, contate o administrador.");
            }

            response = Response.ok(MapBuilder.create("pdfBytes", pdfBytes).build()).build();

        } catch (JRException e) {

            e.printStackTrace();

            response = Response.ok(MapBuilder.create("erro", e.getMessage()).build()).build();

        } catch (Exception e) {

            e.printStackTrace();

            response = Response.ok(MapBuilder.create("erro", e.getMessage()).build()).build();
        }

        return response;
    }

    @Override
    public RomaneioService getService() {

        return this.romaneioService;
    }

}
