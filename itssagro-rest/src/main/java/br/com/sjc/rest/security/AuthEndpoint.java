package br.com.sjc.rest.security;

import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.rest.BaseEndpoint;
import br.com.sjc.fachada.service.ParametroService;
import br.com.sjc.fachada.service.ProdutorService;
import br.com.sjc.fachada.service.UsuarioService;
import br.com.sjc.fachada.service.ldap.LdapAuthenticationService;
import br.com.sjc.modelo.Perfil;
import br.com.sjc.modelo.PerfilFuncionalidade;
import br.com.sjc.modelo.Usuario;
import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.dto.PerfilFuncionalidadeDTO;
import br.com.sjc.modelo.dto.UsuarioDTO;
import br.com.sjc.modelo.enums.AplicacaoEnum;
import br.com.sjc.modelo.enums.ModuloEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.enums.TipoParametroEnum;
import br.com.sjc.modelo.sap.Produtor;
import br.com.sjc.rest.contexto.ContextoManager;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.bundle.MessageSupport;
import br.com.sjc.util.encrypt.UtilEncoder;
import br.com.sjc.util.rest.MapBuilder;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@PermitAll
@Component
@Path("auth")
@Log4j2
public class AuthEndpoint extends BaseEndpoint {

    private static final long serialVersionUID = 1L;

    @Autowired
    private ContextoManager contextoManager;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private ProdutorService produtorService;

    @Autowired
    private ParametroService parametroService;

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMddhhmmss");

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response auth(final Usuario usuario) {

        Usuario usuarioLogado = new Usuario();

        String token = UtilEncoder.get().encrypt(String.format("%s:%s:%s", usuario.getLogin(), usuario.getSenha(), this.now()));

        token = token.replace("\n", "");

        token = token.replace("\r", "");

        try {

            usuarioLogado = this.processLogin(usuario.getLogin(), usuario.getSenha(), false);

            this.registrarToken(usuarioLogado, token);

            this.usuarioService.atualizarDataUltimoAcesso(usuarioLogado.getId(), DateUtil.hoje());

        } catch (Exception e) {

            log.error(e);

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, e.getMessage()).build()).build();
        }

        return this.createResponseAuth(token, usuarioLogado);
    }

    @POST
    @Path("fornecedor")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response authFornecedor(final Usuario usuario) {

        Produtor fornecedorLogado = new Produtor();

        String token = UtilEncoder.get().encrypt(String.format("%s:%s:%s", usuario.getLogin(), usuario.getSenha(), this.now()));

        token = token.replace("\n", "");

        token = token.replace("\r", "");

        try {

            fornecedorLogado = this.processLoginFornecedor(usuario.getLogin(), usuario.getSenha());

            this.registrarTokenFornecedor(fornecedorLogado, token);

        } catch (Exception e) {

            log.error(e);

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, e.getMessage()).build()).build();
        }

        return this.createResponseAuthFornecedor(token, fornecedorLogado);
    }

    @POST
    @Path("auth-senha-criptografada")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response authSenhaCriptograda(Usuario usuario) {

        Usuario usuarioLogado = new Usuario();

        String token = UtilEncoder.get().encrypt(String.format("%s:%s:%s", usuario.getLogin(), usuario.getSenha(), this.now()));

        token = token.replace("\n", "");

        token = token.replace("\r", "");

        try {

            usuarioLogado = this.processLogin(usuario.getLogin(), usuario.getSenha(), true);

            this.registrarToken(usuarioLogado, token);

            this.usuarioService.atualizarDataUltimoAcesso(usuarioLogado.getId(), DateUtil.hoje());

        } catch (Exception e) {
            log.error(e);
            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, e.getMessage()).build()).build();
        }

        return this.createResponseAuth(token, usuarioLogado);
    }

    @GET
    @Path("logout")
    public Response logout() {

        if (this.request.getHeader(BaseEndpoint.AUTHORIZATION_PROPERTY) != null) {

            this.processLogout(this.request.getHeader(BaseEndpoint.AUTHORIZATION_PROPERTY));
        }

        return Response.noContent().build();
    }

    protected Produtor processLoginFornecedor(String login, String senha) throws NotAuthorizedException {

        final Produtor produtor = produtorService.autenticarFornecedor(login, senha);

        if (produtor == null) {

            throw new ServicoException(MessageSupport.getMessage("MSGE001"));
        }

        return produtor;
    }

    protected Usuario processLogin(String login, String senha, boolean senhaCriptografada) throws NotAuthorizedException {

        System.out.println("Autenticado");
        Usuario usuario = usuarioService.obterPorLogin(login);

        if (usuario == null || usuario != null && usuario.getStatus().equals(StatusEnum.INATIVO)) {
            throw new ServicoException(MessageSupport.getMessage("MSGE001"));
        }

        Boolean autenticado = senhaCriptografada;
        if(!autenticado && !usuario.isPermiteAcessoExterno()){
            if(StringUtil.isEmpty(LdapAuthenticationService.SERVIDOR_LDAP)){
                List<Parametro> parametrosLdap = parametroService.obterPorTipoParametro(Arrays.asList(new TipoParametroEnum[]{TipoParametroEnum.SERVIDOR_LDAP, TipoParametroEnum.DOMINIO_LDAP, TipoParametroEnum.BASE_DN_LDAP}));
                LdapAuthenticationService.SERVIDOR_LDAP = parametrosLdap.stream().filter(p -> TipoParametroEnum.SERVIDOR_LDAP.equals(p.getTipoParametro())).findFirst().get().getValor();
                LdapAuthenticationService.DOMINIO_LDAP = parametrosLdap.stream().filter(p -> TipoParametroEnum.DOMINIO_LDAP.equals(p.getTipoParametro())).findFirst().get().getValor();
                LdapAuthenticationService.BASE_DN_LDAP = parametrosLdap.stream().filter(p -> TipoParametroEnum.BASE_DN_LDAP.equals(p.getTipoParametro())).findFirst().get().getValor();
            }


            autenticado = LdapAuthenticationService.getInstance().authentication(login, senha);
        }

        if (autenticado || usuario.isPermiteAcessoExterno())
        {
            if (usuario.isPermiteAcessoExterno()) {
                usuario = usuarioService.autenticarUsuario(login, senha, senhaCriptografada);
            }

            if (usuario == null) {
                throw new ServicoException(MessageSupport.getMessage("MSGE001"));
            }

            if (ColecaoUtil.isEmpty((List<?>) usuario.getPerfis())) {

                throw new ServicoException(MessageSupport.getMessage("MSGE003"));
            }

            if (ColecaoUtil.isEmpty(usuario.getPerfis().stream().map(Perfil::getAplicacoes).collect(Collectors.toList()))) {

                throw new ServicoException(MessageSupport.getMessage("MSGE024"));
            }

            return usuario;
        }
        else
        {
            throw new ServicoException(MessageSupport.getMessage("MSGE038"));
        }
    }

    protected Response createResponseAuthFornecedor(String token, Produtor produtor) {

        final Response response = Response.ok(

                MapBuilder.create("token", token)

                        .map("usuario", new UsuarioDTO(produtor))

                        .build(), MediaType.APPLICATION_JSON).build();

        return response;
    }

    protected Response createResponseAuth(String token, Usuario usuario) {

        final Set<PerfilFuncionalidadeDTO> cadastros = new HashSet<>();

        final Set<PerfilFuncionalidadeDTO> dadosMestres = new HashSet<>();

        final Set<PerfilFuncionalidadeDTO> configuracoes = new HashSet<>();

        final Set<PerfilFuncionalidadeDTO> fluxos = new HashSet<>();

        final Set<PerfilFuncionalidadeDTO> relatorios = new HashSet<>();

        final Set<PerfilFuncionalidadeDTO> autoatendimentos = new HashSet<>();

        usuario.getPerfis().stream().forEach(p -> {

            cadastros.addAll(getCollectFuncionalidade(p, ModuloEnum.CADASTRO));

            dadosMestres.addAll(getCollectFuncionalidade(p, ModuloEnum.DADOS_MESTRES));

            configuracoes.addAll(getCollectFuncionalidade(p, ModuloEnum.CONFIGURACAO));

            fluxos.addAll(getCollectFuncionalidade(p, ModuloEnum.FLUXO));

            relatorios.addAll(getCollectFuncionalidade(p, ModuloEnum.RELATORIOS));

            autoatendimentos.addAll(getCollectFuncionalidade(p, ModuloEnum.AUTOATENDIMENTO));
        });

        usuario.getPerfis().stream().forEach(p -> {

            cadastros.forEach(funcoesPermitidasConsumer(p));

            dadosMestres.forEach(funcoesPermitidasConsumer(p));

            configuracoes.forEach(funcoesPermitidasConsumer(p));

            fluxos.forEach(funcoesPermitidasConsumer(p));

            relatorios.forEach(funcoesPermitidasConsumer(p));

            autoatendimentos.forEach(funcoesPermitidasConsumer(p));
        });

        final Response response = Response.ok(

                MapBuilder.create("token", token)

                        .map("usuario", new UsuarioDTO(usuario))

                        .map("cadastros", cadastros)

                        .map("dadosMestres", dadosMestres)

                        .map("configuracoes", configuracoes)

                        .map("fluxos", fluxos)

                        .map("relatorios", relatorios)

                        .map("autoatendimentos", autoatendimentos)

                        .map("possuiAcessoAoItssCommons", usuario.getPerfis().stream().anyMatch(i -> i.getAplicacoes().stream().anyMatch(a -> AplicacaoEnum.ITSSCOMMONS.equals(a))))

                        .map("possuiAcessoAoItssAgro", usuario.getPerfis().stream().anyMatch(i -> i.getAplicacoes().stream().anyMatch(a -> AplicacaoEnum.ITSSAGRO.equals(a))))

                        .map("possuiAcessoAoGestaoPatio", usuario.getPerfis().stream().anyMatch(i -> i.getAplicacoes().stream().anyMatch(a -> AplicacaoEnum.GESTAO_PATIO.equals(a))))

                        .map("permitirLiberacoes", usuario.getPerfis().stream().anyMatch(i -> i.isPermitirLiberacoes()))

                        .map("pesagemManual", usuario.getPerfis().stream().anyMatch(i -> i.isPesagemManual()))

                        .map("aplicarIndiceRestrito", usuario.getPerfis().stream().anyMatch(i -> i.isAplicarIndiceRestrito()))

                        .map("primeiroAcesso", ObjetoUtil.isNull(usuario.getUltimoAcesso()))

                        .build(), MediaType.APPLICATION_JSON).build();

        return response;
    }

    private Set<PerfilFuncionalidadeDTO> getCollectFuncionalidade(Perfil p, ModuloEnum modulo) {

        return p.getFuncionalidades()
                .stream()
                .filter(i -> modulo.equals(i.getFuncionalidade().getModulo()))
                .map(PerfilFuncionalidade::toDTOApenasFuncionalidade)
                .collect(Collectors.toSet());
    }

    private Consumer<PerfilFuncionalidadeDTO> funcoesPermitidasConsumer(Perfil p) {
        return dto -> {
            if (!dto.isAlterar()) {
                dto.setAlterar(p.getFuncionalidades()
                        .stream()
                        .filter(i -> dto.getFuncionalidade().equals(i.getFuncionalidade()) && i.isAlterar())
                        .map(PerfilFuncionalidade::isAlterar)
                        .findFirst()
                        .orElse(Boolean.FALSE)
                );
            }
            if (!dto.isConsultar()) {
                dto.setConsultar(p.getFuncionalidades()
                        .stream()
                        .filter(i -> dto.getFuncionalidade().equals(i.getFuncionalidade()) && i.isConsultar())
                        .map(PerfilFuncionalidade::isConsultar)
                        .findFirst()
                        .orElse(Boolean.FALSE)
                );
            }
            if (!dto.isExcluir()) {
                dto.setExcluir(p.getFuncionalidades()
                        .stream()
                        .filter(i -> dto.getFuncionalidade().equals(i.getFuncionalidade()) && i.isExcluir())
                        .map(PerfilFuncionalidade::isExcluir)
                        .findFirst()
                        .orElse(Boolean.FALSE)
                );
            }
            if (!dto.isIncluir()) {
                dto.setIncluir(p.getFuncionalidades()
                        .stream()
                        .filter(i -> dto.getFuncionalidade().equals(i.getFuncionalidade()) && i.isIncluir())
                        .map(PerfilFuncionalidade::isIncluir)
                        .findFirst()
                        .orElse(Boolean.FALSE)
                );
            }
            if (!dto.isVisualizar()) {
                dto.setVisualizar(p.getFuncionalidades()
                        .stream()
                        .filter(i -> dto.getFuncionalidade().equals(i.getFuncionalidade()) && i.isVisualizar())
                        .map(PerfilFuncionalidade::isVisualizar)
                        .findFirst()
                        .orElse(Boolean.FALSE)
                );
            }
        };
    }

    protected void processLogout(String token) {

        this.contextoManager.remover(token);
    }

    protected void registrarTokenFornecedor(Produtor usuarioEndpoint, String token) {

        this.contextoManager.armazenar(token, usuarioEndpoint);
    }

    protected void registrarToken(
            Usuario usuarioEndpoint,
            String token
    ) {

        this.contextoManager.armazenar(token, usuarioEndpoint);
    }

    protected String now() {

        return DATE_FORMAT.format(new Date());
    }

}
