package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.AmbienteSincronizacaoService;
import br.com.sjc.fachada.service.DadosSincronizacaoService;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

@Component
@Path("configuracoes/dados-sincronizacao")
public class DadosSincronizacaoEndPoint extends ManutencaoEndpoint<DadosSincronizacao> {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(DadosSincronizacaoEndPoint.class.getName());

    @Autowired
    private DadosSincronizacaoService servico;

    @Autowired
    private AmbienteSincronizacaoService ambienteSincronizacaoService;

    @Override
    public Response salvar(DadosSincronizacao entidade) {

        DadosSincronizacao entidadeSaved = this.servico.salvar(entidade);

        if (ObjetoUtil.isNotNull(entidade.getAmbienteSincronizacao())) {

            entidade.getAmbienteSincronizacao().setDadosSincronizacao(new DadosSincronizacao());

            entidade.getAmbienteSincronizacao().getDadosSincronizacao().setId(entidadeSaved.getId());

            this.ambienteSincronizacaoService.salvar(entidade.getAmbienteSincronizacao());
        }

        return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSG001")).build()).build();
    }

    @Override
    public Response alterar(DadosSincronizacao entidade) {

        DadosSincronizacao entidadeSaved = this.servico.salvar(entidade);

        if (ObjetoUtil.isNotNull(entidade.getAmbienteSincronizacao())) {

            entidade.getAmbienteSincronizacao().setDadosSincronizacao(new DadosSincronizacao());

            entidade.getAmbienteSincronizacao().getDadosSincronizacao().setId(entidadeSaved.getId());

            this.ambienteSincronizacaoService.salvar(entidade.getAmbienteSincronizacao());
        }

        return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSG001")).build()).build();
    }

    @Override
    public DadosSincronizacao dtoGet(Long id) {

        DadosSincronizacao entidade = super.dtoGet(id);

        entidade.setAmbienteSincronizacao(this.ambienteSincronizacaoService.obterPorDadosSincronizacao(entidade));

        if (ObjetoUtil.isNotNull(entidade.getAmbienteSincronizacao())) {

            entidade.getAmbienteSincronizacao().setIdDadosSincronizacao(id);
        }

        return entidade;
    }

    @Override
    public DadosSincronizacaoService getService() {
        return this.servico;
    }
}
