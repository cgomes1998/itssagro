package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.AmbienteService;
import br.com.sjc.modelo.cfg.Ambiente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Path;

@Component
@Path("cadastros/ambiente")
public class AmbienteEndPoint extends ManutencaoEndpoint<Ambiente> {

    @Autowired
    private AmbienteService servico;

    @Override
    public AmbienteService getService() {
        return this.servico;
    }
}
