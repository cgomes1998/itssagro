package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.dto.SelectItemDTO;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.CentroService;
import br.com.sjc.fachada.service.DepositoService;
import br.com.sjc.modelo.sap.Centro;
import br.com.sjc.modelo.sap.Deposito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Path("dados-mestres/centro")
public class CentroEndpoint extends ManutencaoEndpoint<Centro> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private CentroService servico;

    @Autowired
    private DepositoService depositoService;

    @GET
    @Path("/listar-centros")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<Centro>> listarCentros() {

        List<SelectItemDTO<Centro>> itens = new ArrayList<>();

        itens.add(new SelectItemDTO<Centro>("Selecione", null));

        itens.addAll(this.servico.listar().stream().map(i -> new SelectItemDTO<Centro>(i.toString(), i)).collect(Collectors.toList()));

        return itens;
    }

    @GET
    @Path("/obter-depositos-centro-logado")
    @Produces(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<Deposito>> obterDepositosCentroLogado() {

        List<SelectItemDTO<Deposito>> itens = new ArrayList<>();

        itens.add(new SelectItemDTO<Deposito>("Selecione", null));

        itens.addAll(this.depositoService.obterDepositosPorCentroLogado().stream().map(i -> new SelectItemDTO<Deposito>(i.toString(), i)).collect(Collectors.toList()));

        return itens;
    }

    @GET
    @Path("/obter-depositos-por-centro/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<Deposito>> obterDepositosPorCentroId(@PathParam("id") Long idCentro) {

        List<SelectItemDTO<Deposito>> itens = new ArrayList<>();

        itens.add(new SelectItemDTO<Deposito>("Selecione", null));

        itens.addAll(this.depositoService.obterDepositosPorCentroId(idCentro).stream().map(i -> new SelectItemDTO<Deposito>(i.toString(), i)).collect(Collectors.toList()));

        return itens;
    }

    @Override
    protected CentroService getService() {
        return servico;
    }
}
