package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.ParametroService;
import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.enums.TipoParametroEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Component
@Path("configuracoes/parametro-integracao")
public class ParametroEndpoint extends ManutencaoEndpoint<Parametro> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private ParametroService servico;

    @GET
    @Path("obter-por-tipo/{tipo}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Parametro dtoGet(@PathParam("tipo") TipoParametroEnum tipo) {
        return this.getService().obterPorTipoParametro(tipo);
    }

    @Override
    protected ParametroService getService() {
        return servico;
    }

}
