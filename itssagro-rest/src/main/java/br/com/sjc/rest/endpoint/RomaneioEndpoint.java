package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.dto.SelectItemDTO;
import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.rest.BaseEndpoint;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.rest.paginacao.Pagina;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.*;
import br.com.sjc.modelo.AnaliseQualidade;
import br.com.sjc.modelo.Motivo;
import br.com.sjc.modelo.Usuario;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.dto.*;
import br.com.sjc.modelo.dto.aprovacaoDocumento.RomaneioAprovacaoDocumento;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.enums.OrigemClassificacaoEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.relatorio.GerarRelatorioExcel;
import br.com.sjc.modelo.response.*;
import br.com.sjc.modelo.rest.LongParam;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.modelo.sap.response.*;
import br.com.sjc.modelo.sap.response.item.RfcOrdensVendaItemResponse;
import br.com.sjc.modelo.sap.response.item.RfcOrdensVendaItemTxtResponse;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.bundle.MessageSupport;
import br.com.sjc.util.rest.MapBuilder;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Path("fluxo/romaneio")
@PermitAll
public class RomaneioEndpoint extends ManutencaoEndpoint<Romaneio> {

    private static final long serialVersionUID = 1L;

    private static final String ERRO_INESPERADO_AO_REALIZAR_A_IMPRESSAO_DO_ROMANEIO = "Erro inesperado ao realizar a impressão do romaneio: {0}";

    private static final String NAO_FOI_POSSIVEL_REALIZAR_A_IMPRESSAO_DO_PDF = "Não foi possivel realizar a impressão do pdf.";

    @Autowired
    private RomaneioService romaneioService;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private SAPService sapService;

    @Autowired
    private CentroService centroService;

    @Autowired
    private DepositoService depositoService;

    @Autowired
    private ContratoService contratoService;

    @Autowired
    private SafraService safraService;

    @Autowired
    private ProdutorService produtorService;

    @Autowired
    private MaterialService materialService;

    @Autowired
    private BridgeService bridgeService;

    @Autowired
    private IvaService ivaService;

    @Autowired
    private CarregamentoService carregamentoService;

    @Autowired
    private AnaliseQualidadeService analiseQualidadeService;

    @GET
    @Path("existeNumeroRomaneio/{numeroRomaneio}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response existeNumeroRomaneio(@PathParam("numeroRomaneio") String numeroRomaneio) {

        return Response.ok(romaneioService.existeNumeroRomaneio(numeroRomaneio)).build();
    }

    @POST
    @Path("simplesPersistencia")
    @Consumes(MediaType.APPLICATION_JSON)
    public void simplesPersistencia(Romaneio romaneio) {

        this.romaneioService.simplesPersistencia(romaneio);
    }

    @POST
    @Path("obter-rel-pesagens-manuais")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<byte[]> obterRelatorioPesagensManuais(final ItemNFPedido filtro) {

        try {

            List<PesagemManualDTO> pesagens = romaneioService.obterPesagensManuais(filtro);

            if (ColecaoUtil.isEmpty(pesagens)) {
                throw new Exception("Nenhum registro encontrado de acordo com os parâmetros informados.");
            }

            FileResponse file = GerarRelatorioExcel.obterRelatorioPesagemManuais(pesagens);

            HttpHeaders headers = new HttpHeaders();

            headers.setContentDispositionFormData(file.getName(), file.getName());

            return new ResponseEntity<byte[]>(file.getBuffer(), headers, HttpStatus.OK);

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    @POST
    @Path("obter-rel-ordem-venda")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<byte[]> obterRelatorioOrdemVenda(final ItemNFPedido filtro) {

        try {

            Paginacao<ItemNFPedido> itemNFPedidoPaginacao = new Paginacao<>();
            itemNFPedidoPaginacao.setEntidade(filtro);
            RfcOrdensVendaResponse response = sapService.buscarOrdens(itemNFPedidoPaginacao);

            // método para remover itens que tenham ordem de venda ZVFA e ZECO
            removeAlgunsTiposOrdemVenda(response);

            List<TextoOrdemDTO> textosDasOrdens = getTextoOrdemDTOS(response);

            if (ColecaoUtil.isEmpty(response.getOrdens())) {
                throw new Exception("Nenhum registro encontrado de acordo com os parâmetros informados.");
            }

            FileResponse file =GerarRelatorioExcel.obterRelatorioOrdemVenda(response.getOrdens().stream().sorted(Comparator.comparing(RfcOrdensVendaItemResponse::getDataOrdem).reversed()).collect(Collectors.toList()), textosDasOrdens);

            HttpHeaders headers = new HttpHeaders();

            headers.setContentDispositionFormData(file.getName(), file.getName());

            return new ResponseEntity<byte[]>(file.getBuffer(), headers, HttpStatus.OK);

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    private void removeAlgunsTiposOrdemVenda(RfcOrdensVendaResponse response) {
        response.getOrdens()
                .removeIf(item -> item.getOrdemVenda().equals("ZVFA") ||
                        item.getOrdemVenda().equals("ZECO"));
    }

    @POST
    @Path("obterRelatorioLaudoAnalitico")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<byte[]> obterRelatorioLaudoAnalitico(RelatorioLaudoFiltroDTO relatorioLaudoFiltro) {

        try {
            List<RelatorioLaudoAnalitico> laudos = romaneioService.obterRelatorioLaudoAnalitico(relatorioLaudoFiltro);

            if (ColecaoUtil.isEmpty(laudos)) {
                throw new Exception("Nenhum registro encontrado de acordo com os parâmetros informados.");
            }

            FileResponse file =GerarRelatorioExcel.obterRelatorioLaudoAnalitico(laudos.stream().sorted(Comparator.comparing(r -> r.getRomaneio().getId())).collect(Collectors.toList()));

            HttpHeaders headers = new HttpHeaders();

            headers.setContentDispositionFormData(file.getName(), file.getName());

            return new ResponseEntity<byte[]>(file.getBuffer(), headers, HttpStatus.OK);

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    @POST
    @Path("obterRelatorioLaudoSintetico")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<byte[]> obterRelatorioLaudoSintetico(RelatorioLaudoFiltroDTO relatorioLaudoFiltro) {

        try {
            List<RelatorioLaudoSintetico> laudos = romaneioService.obterRelatorioLaudoSintetico(relatorioLaudoFiltro);

            if (ColecaoUtil.isEmpty(laudos)) {
                throw new Exception("Nenhum registro encontrado de acordo com os parâmetros informados.");
            }

            FileResponse file =GerarRelatorioExcel.obterRelatorioLaudoSintetico(laudos.stream().sorted(Comparator.comparing(RelatorioLaudoSintetico::getIdRomaneio)).collect(Collectors.toList()));

            HttpHeaders headers = new HttpHeaders();

            headers.setContentDispositionFormData(file.getName(), file.getName());

            return new ResponseEntity<byte[]>(file.getBuffer(), headers, HttpStatus.OK);

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    @POST
    @Path("obter-rel-romaneio-gestao-itss-agro")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<byte[]> listarRomaneioGestaoItssAgroPorData(RelatorioLaudoFiltroDTO filtroDTO) {

        try {

            List<RomaneioGestaoItssAgroDTO> romaneios = romaneioService.listarRomaneioGestaoItssAgroPorData(filtroDTO.getDataInicio(), filtroDTO.getDataFim());

            if (ColecaoUtil.isEmpty(romaneios)) {
                throw new Exception("Nenhum registro encontrado de acordo com os parâmetros informados.");
            }

            FileResponse file =GerarRelatorioExcel.obterRelatorioRomaneiosGestaoItssAgro(romaneios.stream().sorted(Comparator.comparing(RomaneioGestaoItssAgroDTO::getNumRomaneio)).collect(Collectors.toList()));

            HttpHeaders headers = new HttpHeaders();

            headers.setContentDispositionFormData(file.getName(), file.getName());

            return new ResponseEntity<byte[]>(file.getBuffer(), headers, HttpStatus.OK);

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    @POST
    @Path("obter-rel-comercial")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<byte[]> obterRelatorioComercial(final RelatorioComercialFiltroDTO filtro) {

        try {

            final List<RelatorioComercialDTO> dtos = romaneioService.obterRelatorioComercial(filtro);

            final List<RelComercialNFeDTO> notas = romaneioService.getRelatorioComercialNFeDTOS(filtro);

            if (ColecaoUtil.isEmpty(dtos)) {
                throw new Exception("Nenhum registro encontrado de acordo com os parâmetros informados.");
            }

            dtos.stream().filter(i -> StringUtil.isEmpty(i.getCodigoOrdemVenda())).forEach(i -> i.setCodigoOrdemVenda(StringUtil.empty()));

            dtos.stream().forEach(i -> {
                for (RelComercialNFeDTO nota : notas) {
                    if (nota.getNumeroRomaneio().equals(i.getNumeroRomaneio())) {
                        i.setNota(nota.getNumeroNfe());
                        i.setDataCriacaoNota(nota.getDataNfe());
                        notas.remove(nota);
                        break;
                    }
                }
            });

            FileResponse file =GerarRelatorioExcel.obterRelatorioComercial(dtos);

            HttpHeaders headers = new HttpHeaders();

            headers.setContentDispositionFormData(file.getName(), file.getName());

            return new ResponseEntity<byte[]>(file.getBuffer(), headers, HttpStatus.OK);

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    @POST
    @Path("obter-rel-comercial-v2")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<byte[]> obterRelatorioComercial_V2(final RelatorioComercialFiltroDTO filtro) {

        try {

            final List<RelatorioComercial_V2_DTO> dtos = getService().obterRelatorioComercial_V2(filtro);

            if (ColecaoUtil.isEmpty(dtos)) {
                throw new Exception("Nenhum registro encontrado de acordo com os parâmetros informados.");
            }

            FileResponse file =GerarRelatorioExcel.obterRelatorioComercial_V2(dtos);

            HttpHeaders headers = new HttpHeaders();

            headers.setContentDispositionFormData(file.getName(), file.getName());

            return new ResponseEntity<byte[]>(file.getBuffer(), headers, HttpStatus.OK);

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    @POST
    @Path("obter-rel-fluxo-carregamento")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<byte[]> obterRelatorioFluxoDeCarregamento(final RelatorioFluxoDeCarregamentoFiltroDTO filtro) {

        try {

            final List<RelatorioFluxoDeCarregamentoDTO> dtos = romaneioService.obterRelatorioFluxoDeCarregamento(filtro);

            final List<RomaneioConversaoLitragemDTO> conversoes = romaneioService.obterRomaneiosComConversaoLitragem(dtos.stream().filter(d -> StringUtil.isNotNullEmpty(d.getNumeroRomaneio()) && d.getRealizarConversaoLitragem()).map(RelatorioFluxoDeCarregamentoDTO::getNumeroRomaneio).collect(Collectors.toList()));

            if (ColecaoUtil.isNotEmpty(conversoes)) {

                dtos.stream().filter(d -> StringUtil.isNotNullEmpty(d.getNumeroRomaneio()) && !d.isRomaneioTemRateio() && d.getRealizarConversaoLitragem()).forEach(d -> {

                    List<RomaneioConversaoLitragemDTO> conversoesDoRomaneio =
                            conversoes.stream().filter(c -> StringUtil.isNotNullEmpty(c.getNumeroRomaneio()) && c.getNumeroRomaneio().equals(d.getNumeroRomaneio())).collect(Collectors.toList());

                    if (ColecaoUtil.isNotEmpty(conversoesDoRomaneio)) {

                        Date maiorData = conversoesDoRomaneio.stream().map(RomaneioConversaoLitragemDTO::getDataCadastro).max(Date::compareTo).get();

                        d.setQtdKgL(conversoesDoRomaneio.stream().filter(c -> c.getDataCadastro().equals(maiorData)).map(RomaneioConversaoLitragemDTO::getValor).findFirst().orElse(BigDecimal.ZERO));
                    }
                });
            }

            Collections.reverse(dtos);

            if (ColecaoUtil.isEmpty(dtos)) {
                throw new Exception("Nenhum registro encontrado de acordo com os parâmetros informados.");
            }

            FileResponse file = GerarRelatorioExcel.obterRelatorioFluxoDeCarregamento(dtos);

            HttpHeaders headers = new HttpHeaders();

            headers.setContentDispositionFormData(file.getName(), file.getName());

            return new ResponseEntity<byte[]>(file.getBuffer(), headers, HttpStatus.OK);

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    @POST
    @Path("obter-rel-analise-conversao")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<byte[]> obterRelatorioAnaliseConversao(final RelatorioAnaliseConversaoFiltroDTO filtro) {

        try {
            List<RelatorioAnaliseConversaoDTO> dtos = getService().obterRelatorioAnaliseConversao(filtro);

            if (ColecaoUtil.isEmpty(dtos)) {
                throw new Exception("Nenhum registro encontrado de acordo com os parâmetros informados.");
            }

            FileResponse file = GerarRelatorioExcel.obterRelatorioAnaliseConversao(dtos);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentDispositionFormData(file.getName(), file.getName());

            return new ResponseEntity<>(file.getBuffer(), headers, HttpStatus.OK);

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    @POST
    @Path("obter-rel-deposito")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<byte[]> obterRelatorioDeDepositos(final RelatorioDepositoFiltroDTO filtro) {

        try {

            FileResponse file = romaneioService.obterRelatorioDepositos(filtro);

            HttpHeaders headers = new HttpHeaders();

            headers.setContentDispositionFormData(file.getName(), file.getName());

            return new ResponseEntity<byte[]>(file.getBuffer(), headers, HttpStatus.OK);

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    @POST
    @Path("filtrarRelatorioFornecedorGraosPagamento")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response filtrarRelatorioFornecedorGraosPagamento(final RelatorioDepositoFiltroDTO filtro) {

        try {

            return Response.ok(romaneioService.filtrarRelatorioFornecedorGraosPagamento(filtro)).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("filtrarDepositosFornecedorGraosEntregas")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response filtrarDepositosFornecedorGraosEntregas(final RelatorioDepositoFiltroDTO filtro) {

        try {

            return Response.ok(romaneioService.filtrarDepositosFornecedorGraosEntregas(filtro)).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("obterRelatorioDeDepositosFornecedorGraosEntregas")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Arquivo obterRelatorioDeDepositosFornecedorGraosEntregas(final RelatorioDepositoFiltroDTO filtro) {

        try {

            FileResponse file = romaneioService.obterRelatorioDeDepositosFornecedorGraosEntregas(filtro);

            return Arquivo.builder().buffer(file.getBuffer()).fileName(file.getName()).build();

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    @POST
    @Path("obterRelatorioDeDepositosFornecedorGraosPagamento")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Arquivo obterRelatorioDeDepositosFornecedorGraosPagamento(final RelatorioDepositoFiltroDTO filtro) {

        try {

            FileResponse file = romaneioService.obterRelatorioDeDepositosFornecedorGraosPagamento(filtro);

            return Arquivo.builder().buffer(file.getBuffer()).fileName(file.getName()).build();

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    @POST
    @Path("obterRelatorioDeLacresVinculadosARomaneio")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<byte[]> obterRelatorioDeLacresVinculadosARomaneio(RelatorioLacreFiltroDTO relatorioLacreFiltro) {

        try {

            FileResponse file = romaneioService.obterRelatorioDeLacresVinculadosARomaneio(relatorioLacreFiltro);

            HttpHeaders headers = new HttpHeaders();

            headers.setContentDispositionFormData(file.getName(), file.getName());

            return new ResponseEntity<byte[]>(file.getBuffer(), headers, HttpStatus.OK);

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }

    @POST
    @Path("salvar-com-retorno")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response salvarComRetorno(Romaneio entidade) {

        try {

            Romaneio response = getService().salvarComRetorno(entidade);

            return Response.ok(response).build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("estornarRomaneioSemIntegracaoComAutomacao")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response estornarRomaneioSemIntegracaoComAutomacao(Romaneio romaneio) {

        try {

            romaneioService.estornarRomaneioSemIntegracaoComAutomacao(romaneio);

            return Response.ok(Response.noContent()).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("salvar-sem-integracao-sap")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response salvarSemIntegracaoComSap(Romaneio entidade) {

        try {

            Romaneio response = getService().salvarSemIntegracaoComSap(entidade);

            return Response.ok(response).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/validar-chave-acesso/{chave}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response validarChaveDeAcesso(@PathParam("chave") String chave) {

        try {

            this.romaneioService.validarChaveDeAcesso(chave);

            return Response.status(Response.Status.OK).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @PermitAll
    @Path("/aprovacao-documento/listar-romaneios-pendente-aprovacao-indices")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response listarRomaneiosPendentesAprovacaoClassificacao() {
        ListaRomaneiosPendenteAprovacaoIndicesResponse response = getService().listarRomaneiosPendenteAprovacaoIndices();

        return Response.ok(response).build();
    }

    @POST
    @PermitAll
    @Path("/aprovacao-documento/aprovar-romaneio")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response aprovacaoDocumentoaprovarRomaneioComIndicesRestritos(RomaneioAprovacaoDocumento romaneioAprovacaoDocumento) {

        romaneioService.aprovarRomaneioComItensRestritos(romaneioAprovacaoDocumento);

        return Response.noContent().build();
    }

    @POST
    @Path("/aprovar-romaneio")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response aprovarRomaneioComIndicesRestritos(Romaneio romaneio) {

        romaneioService.aprovarRomaneioComItensRestritos(romaneio);

        return Response.ok(MapBuilder.create(BaseEndpoint.RESPONSE_MENSAGEM, MessageSupport.getMessage("MSG001")).build()).build();
    }

    @GET
    @Path("/obter-simples-entrada/{numero}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterSimplesEntrada(@PathParam("numero") String numero) {

        final Romaneio romaneio = this.getService().obterSimplesEntrada(numero);

        if (ObjetoUtil.isNull(romaneio)) {

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, MessageFormat.format("Romaneio {0} não encontrado.", numero)).build()).build();
        }

        if (StatusRomaneioEnum.PENDENTE_INDICES_CLASSIFICACAO.equals(romaneio.getStatusRomaneio())) {

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, MessageFormat.format("Romaneio {0} bloqueado, {1}, deve ser aprovado para utilização como referência.", numero, romaneio.getStatusRomaneio().getDescricao())).build()).build();
        }

        return Response.ok(MapBuilder.create(BaseEndpoint.ENTIDADE, romaneio).build()).build();
    }

    @GET
    @Path("/obter-dados-romaneio-estornado/{operacaoEntidade}/{numero}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterDadosRomaneioEstornado(@PathParam("operacaoEntidade") DadosSincronizacaoEnum operacaoEntidade, @PathParam("numero") String numero) {

        try {

            Romaneio romaneio = this.getService().obterDadosRomaneioEstornado(operacaoEntidade, numero);

            return Response.ok(MapBuilder.create(BaseEndpoint.ENTIDADE, romaneio).build()).build();

        } catch (ServicoException e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/obter-romaneio-armazem-terceiros/{numero}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterRomaneioArmazemDeTerceiros(@PathParam("numero") String numero) {

        try {

            Romaneio romaneio = this.getService().obterRomaneioArmazemDeTerceiros(numero);

            return Response.ok(MapBuilder.create(ENTIDADE, romaneio).build()).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/obter-romaneio-para-devolucao/{numero}/{operacao}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterRomaneioParaDevolucao(@PathParam("numero") String numero, @PathParam("operacao") DadosSincronizacaoEnum operacao) {

        try {

            Romaneio romaneio = romaneioService.obterRomaneioParaDevolucao(numero, operacao);

            return Response.ok(MapBuilder.create(ENTIDADE, romaneio).build()).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/obter-romaneio-estornado/{numeroRomaneioOrigem}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterRomaneioEstornado(@PathParam("numeroRomaneioOrigem") String numeroRomaneioOrigem) {

        try {
            ObterRomaneioEstornadoResponse response = romaneioService.obterRomaneioEstornado(numeroRomaneioOrigem);

            return Response.ok(response).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/obter-romaneio-sem-pesagem/{numeroRomaneio}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterRomaneioSemPesagem(@PathParam("numeroRomaneio") String numeroRomaneio) {

        try {
            ObterRomaneioSemPesagemResponse response = this.getService().obterRomaneioSemPesagem(numeroRomaneio);

            return Response.ok(response).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/obterRomaneioParaReferenciar/{numeroRomaneioOrigem}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterRomaneioParaReferenciar(@PathParam("numeroRomaneioOrigem") String numeroRomaneioOrigem) {

        try {

            return Response.ok(MapBuilder.create(ENTIDADE, romaneioService.obterRomaneioParaReferenciar(numeroRomaneioOrigem)).build()).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/imprimir-pdf-classificacao/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response imprimirPdfClassificacao(@PathParam("id") Long id) {

        try {

            byte[] pdfBytes = getService().imprimirPdfClassificacao(id);

            return Response.ok(MapBuilder.create("pdfBytes", pdfBytes).build()).build();

        } catch (JRException e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, MessageFormat.format(ERRO_INESPERADO_AO_REALIZAR_A_IMPRESSAO_DO_ROMANEIO, e.getMessage())).build()).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/portal-fornecedor-graos/imprimir-pdf-classificacao/{numeroRomaneio}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response portalFornecedorGraosImprimirClassificacao(@PathParam("numeroRomaneio") String numeroRomaneio) {

        try {

            Arquivo arquivo = getService().imprimirPdfClassificacaoPorNumeroRomaneio(numeroRomaneio);

            return Response.ok().entity(arquivo).build();

        } catch (JRException e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, MessageFormat.format(ERRO_INESPERADO_AO_REALIZAR_A_IMPRESSAO_DO_ROMANEIO, e.getMessage())).build()).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/portal-fornecedor-graos/imprimir-pdf/{numeroRomaneio}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response portalFornecedorGraosImprimirPdf(@PathParam("numeroRomaneio") String numeroRomaneio) {

        try {

            Arquivo response = getService().imprimirPdfPorNumeroRomaneio(numeroRomaneio);

            return Response.ok().entity(response).build();

        } catch (JRException e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, MessageFormat.format(ERRO_INESPERADO_AO_REALIZAR_A_IMPRESSAO_DO_ROMANEIO, e.getMessage())).build()).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/imprimir-pdf/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response imprimirPdf(@PathParam("id") Long id) {

        try {

            final byte[] pdfBytes = this.getService().imprimirPdf(null, id);

            if (ObjetoUtil.isNull(pdfBytes)) {

                throw new Exception(NAO_FOI_POSSIVEL_REALIZAR_A_IMPRESSAO_DO_PDF);
            }

            return Response.ok(MapBuilder.create("pdfBytes", pdfBytes).build()).build();

        } catch (JRException e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, MessageFormat.format(ERRO_INESPERADO_AO_REALIZAR_A_IMPRESSAO_DO_ROMANEIO, e.getMessage())).build()).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, ObjetoUtil.isNotNull(e.getMessage()) ? e.getMessage() : e.getCause()).build()).build();
        }
    }

    @GET
    @Path("/imprimir-pdf-laudo/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response imprimirPdfLaudo(@PathParam("id") Long id) {

        try {

            final byte[] pdfBytes = this.analiseQualidadeService.imprimirRomaneio(id, null);

            if (ObjetoUtil.isNull(pdfBytes)) {

                throw new Exception(NAO_FOI_POSSIVEL_REALIZAR_A_IMPRESSAO_DO_PDF);
            }

            return Response.ok(MapBuilder.create("pdfBytes", pdfBytes).build()).build();

        } catch (JRException e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, MessageFormat.format(ERRO_INESPERADO_AO_REALIZAR_A_IMPRESSAO_DO_ROMANEIO, e.getMessage())).build()).build();
        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, ObjetoUtil.isNotNull(e.getMessage()) ? e.getMessage() : e.getCause()).build()).build();
        }
    }

    @GET
    @Path("/listar-bridges/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<Bridge> listarBridgesEnviadas(@PathParam("id") Long idRomaneio) {

        return this.bridgeService.listarBridges(idRomaneio);
    }

    @GET
    @Path("/listar-tipos-contratos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<TipoContrato>> listarTiposContratos() {

        List<SelectItemDTO<TipoContrato>> itens = new ArrayList<>();

        itens.add(new SelectItemDTO<>("Selecione", null));

        itens.addAll(this.contratoService.listarTipoContratos().stream().map(i -> new SelectItemDTO<TipoContrato>(i.toString(), i)).collect(Collectors.toList()));

        return itens;
    }

    @GET
    @Path("/listar-ivas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<Iva>> listarIvas() {

        List<SelectItemDTO<Iva>> itens = new ArrayList<>();

        itens.add(new SelectItemDTO<>("Selecione", null));

        itens.addAll(this.ivaService.listarIvasOrdernandoPorCodigoAcrescente().stream().map(i -> new SelectItemDTO<Iva>(i.toString(), i)).collect(Collectors.toList()));

        return itens;
    }

    @GET
    @Path("/listar-cond-pagamentos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<CondicaoPagamento>> listarCondPagamentos() {

        List<SelectItemDTO<CondicaoPagamento>> itens = new ArrayList<>();

        itens.add(new SelectItemDTO<>("Selecione", null));

        itens.addAll(this.contratoService.listarCondicoesPagamento().stream().map(i -> new SelectItemDTO<CondicaoPagamento>(i.toString(), i)).collect(Collectors.toList()));

        return itens;
    }

    @GET
    @Path("/listar-org-compra")
    @Produces(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<OrganizacaoCompra>> listarOrgCompra() {

        List<SelectItemDTO<OrganizacaoCompra>> itens = new ArrayList<>();

        itens.add(new SelectItemDTO<>("Selecione", null));

        itens.addAll(this.contratoService.listarOrganizacaoCompras().stream().map(i -> new SelectItemDTO<OrganizacaoCompra>(i.toString(), i)).collect(Collectors.toList()));

        return itens;
    }

    @GET
    @Path("/listar-gp-compradores")
    @Produces(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<GrupoCompradores>> listarGpCompradores() {

        List<SelectItemDTO<GrupoCompradores>> itens = new ArrayList<>();

        itens.add(new SelectItemDTO<>("Selecione", null));

        itens.addAll(this.contratoService.listarGrupoCompradores().stream().map(i -> new SelectItemDTO<GrupoCompradores>(i.toString(), i)).collect(Collectors.toList()));

        return itens;
    }

    @GET
    @Path("/obter-etapas-romaneio/{numeroRomaneio}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterEtapasDoRomaneio(@PathParam("numeroRomaneio") String numeroRomaneio) {

        try {

            return Response.ok(MapBuilder.create(ENTIDADE, this.sapService.obterEtapasDoRomaneio(numeroRomaneio)).build()).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("/atualizar-romaneios")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response atualizarRomaneios(final Long id) {

        try {

            if (Objects.nonNull(id)) {

                this.romaneioService.atualizarRequest(id);
            }

            this.sapService.atualizarRomaneios(id, null);

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, e.getMessage()).build()).build();
        }

        return Response.ok(MapBuilder.create(BaseEndpoint.RESPONSE_MENSAGEM, this.getMensagem("MSG001")).build()).build();
    }

    @GET
    @Path("/obter-entidade/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response obterEntidade(@PathParam("id") Long id) {

        ObterEntidadeResponse response = this.getService().obterEntidade(id);

        return Response.ok(response).build();
    }

    @GET
    @Path("/obter-nf-transporte/{numero}/{idRomaneio: .*}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterRomaneioPorNumeroNotaFiscal(@PathParam("numero") String numero, @PathParam("idRomaneio") LongParam idRomaneio) {

        try {

            return Response.ok(MapBuilder.create("romaneio", this.romaneioService.obterRomaneioPorNumeroNotaFiscal(numero, idRomaneio.getNumber())).build()).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/obter-log/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterLogEnvioRomaneio(@PathParam("id") Long id) {

        return Response.ok(getService().obterRomaneioLogEnvioPorId(id)).build();
    }

    @POST
    @Path("/validar-escrituracao-nfe")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response validarEscrituracaoNotaFiscal(ItemNFPedido item) {

        try {

            this.getService().validarEscrituracaoDeNota(item.getNumeroNfe(), item.getSerieNfe(), item.getDataNfe(), item.getProdutor().getId(), item.getSafra().getId(), item.getMaterial().getId(), item.getId());

            return Response.ok().build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("/estornar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response estornar(Romaneio dto) {

        try {

            dto.setUsuarioLogado(this.getUsuarioToken());

            romaneioService.estornar(dto, false);

            return Response.ok(MapBuilder.create(BaseEndpoint.RESPONSE_MENSAGEM, MessageSupport.getMessage("MSG001")).build()).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("/tornar-pendente")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response tornarPendente(Romaneio entidade) {

        try {

            this.atribuirUsuario(entidade);

            this.getService().tornarPendente(entidade);

            return Response.ok(MapBuilder.create(BaseEndpoint.RESPONSE_MENSAGEM, MessageSupport.getMessage("MSG001")).build()).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, e.getMessage()).build()).build();
        }
    }

    @Path("/liberar-ordem")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response liberarOrdem(Romaneio entidade) {

        this.atribuirUsuario(entidade);

        this.getService().tornarEmProcessamento(entidade);

        return Response.ok(MapBuilder.create("sucesso", MessageFormat.format("Romaneio {0} salvo com sucesso!", entidade.getNumeroRomaneio())).build()).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response salvar(Romaneio entidade) {

        try {

            SalvarRomaneioV2Response response = this.romaneioService.salvarRomaneioV2(entidade, true, true, true);

            return Response.ok(response).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, StringUtil.isNotNullEmpty(e.getMessage()) ? e.getMessage() : "NULLPOINTER EXCEPTION").build()).build();
        }
    }

    @GET
    @Path("/reenviar-romaneio/{idRomaneio}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response reenviarRomaneio(@PathParam("idRomaneio") Long idRomaneio) {

        this.sapService.reenviarRomaneioPorId(idRomaneio);

        return Response.ok(MapBuilder.create("mensagem", this.getMensagem("MSG001")).build()).build();
    }

    @POST
    @Path("/enviar-romaneio")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response enviarRomaneio(Romaneio entidade) {

        try {

            getService().enviarRomaneio(entidade);

            return Response.ok(MapBuilder.create("mensagem", this.getMensagem("MSG001")).build()).build();

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/buscar-saldo-pedido-tolerancia-pedido/{pedido}/{item}/{operacao}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarSaldoPedidoEToleranciaPedido(@PathParam("pedido") String pedido, @PathParam("item") String item, @PathParam("operacao") String operacao) {

        try {

            RfcSaldoOrdemPedidoResponse response = this.sapService.buscarSaldoPedido(pedido, item, operacao);

            RfcToleranciaOrdemPedidoResponse responseTolerancia = this.sapService.buscarToleranciaPedido(pedido, item, operacao);

            final BigDecimal saldoPedido = response.getSaldo();

            final BigDecimal toleranciaPedido = responseTolerancia.getSaldo();

            final BigDecimal valorUnitarioPedido = response.getValorUnitario();

            final BigDecimal saldoReservado = this.romaneioService.obterSaldoReservado(pedido, item);

            final Produtor produtor = StringUtil.isNotNullEmpty(response.getProdutor()) ? this.produtorService.obterPorCodigo(new Long(response.getProdutor()).toString()) : null;

            final Safra safra = StringUtil.isNotNullEmpty(response.getSafra()) ? this.safraService.obterPorCodigo(response.getSafra()) : null;

            final Centro centro = StringUtil.isNotNullEmpty(response.getCentro()) ? this.centroService.obterPorCodigo(new Long(response.getCentro()).toString()) : null;

            final Deposito deposito = StringUtil.isNotNullEmpty(response.getDeposito()) ? this.depositoService.obterPorCodigo(response.getDeposito()) : null;

            final Material material = StringUtil.isNotNullEmpty(response.getMaterial()) ? this.materialService.obterPorCodigo(new Long(response.getMaterial()).toString()) : null;

            final OrigemClassificacaoEnum localPesagem = StringUtil.isNotNullEmpty(response.getLocalPesagem()) ? OrigemClassificacaoEnum.getLocalPesagem(response.getLocalPesagem()) : null;

            return Response.ok(MapBuilder.create("saldoPedido", saldoPedido).map("valorUnitarioPedido", valorUnitarioPedido).map("produtor", produtor).map("safra", safra).map("centro", centro).map("deposito", deposito).map("material",
                    material).map("localPesagem", localPesagem).map("toleranciaPedido", toleranciaPedido).map("saldoReservado", saldoReservado).map("saldoDisponivel", (saldoPedido.subtract(saldoReservado))).build()).build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/buscar-saldo-pedido-tolerancia-pedido-transferencia/{pedido}/{item}/{operacao}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarSaldoPedidoEToleranciaPedidoTransferencia(@PathParam("pedido") String pedido, @PathParam("item") String item, @PathParam("operacao") String operacao) {

        try {

            SaldoPedidoEToleranciaPedidoTransferenciaResponse response = this.sapService.buscarSaldoPedidoEToleranciaPedidoTransferencia(pedido, item, operacao);

            return Response.ok(response).build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/buscar-dados-nfe/{chave}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarDadosNfe(@PathParam("chave") String chave) {

        try {

            RfcObterNfeResponse response = this.sapService.buscarDadosNfe(chave);

            return Response.ok(MapBuilder.create("header", response.getHeader()).map("item", response.getB_item()).build()).build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/buscar-dados-nfe-transferencia/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarDadosNfeTransferencia(@QueryParam("chave") String chave, @QueryParam("remessa") String remessa) {

        try {

            DadosNfeTransferenciaResponse response = this.sapService.buscarDadosNfeTransferencia(chave, remessa);

            return Response.ok(response).build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/buscar-dados-cte/{chave}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarDadosCte(@PathParam("chave") String chave) {

        try {

            RfcObterCteResponse response = this.sapService.buscarDadosCte(chave);

            return Response.ok(
                    MapBuilder.create("chave", response.getHeader().getChave())
                    .map("nct", response.getHeader().getNct())
                    .map("serie", response.getHeader().getSerie())
                    .map("dhemi", response.getHeader().getDhemi())
                    .map("cnpjEmit", response.getHeader().getCnpjEmit())
                    .map("ufEmit", response.getHeader().getUfEmit())
                    .map("cnpjToma", response.getHeader().getCnpjToma())
                    .map("ufToma", response.getHeader().getUfToma())
                    .map("cfop", response.getHeader().getCfop())
                    .map("valorCte", response.getHeader().getValorCte())
                    .map("valorIcms", response.getHeader().getValorIcms())
                    .map("valorTotalTrib", response.getHeader().getValorTotalTrib())
                    .map("nprot", response.getHeader().getNprot())
                    .map("credat", response.getHeader().getCredat())
                    .map("cretim", response.getHeader().getCretim())
                    .map("nferef", response.getHeader().getNferef())
                    .map("status", response.getHeader().getStatus())
                    .build()
            ).build();
        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }
    @GET
    @Path("/obter-operacao/{operacao}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterOperacao(@PathParam("operacao") String operacao) {
        ObterOperacaoResponse response = getService().obterOperacao(operacao);

        return Response.ok(response).build();
    }

    @GET
    @Path("/listar-operacoes/{idUsuarioLogado}{gestaoPatio:(/gestaoPatio/[^/]+?)?}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<DadosSincronizacao>> listarOperacoes(@PathParam("idUsuarioLogado") Long idUsuarioLogado, @PathParam("gestaoPatio") String gestaoPatio) {

        List<SelectItemDTO<DadosSincronizacao>> itens = new ArrayList<>();

        itens.add(new SelectItemDTO<DadosSincronizacao>("Selecione", null));

        final Usuario usuarioLogado = this.usuarioService.get(idUsuarioLogado);

        if (ColecaoUtil.isNotEmpty((List<?>) usuarioLogado.getPerfis())) {

            List<DadosSincronizacao> operacoes = new ArrayList<>();

            usuarioLogado.getPerfis().stream().forEach(p -> {
                p.getOperacoes().stream().forEach(o -> {

                    if (StringUtil.isNotNullEmpty(gestaoPatio) && Boolean.valueOf(gestaoPatio.split("/")[2])) {
                        if (o.getOperacao().isGestaoPatio() && operacoes.stream().filter(os -> os.getOperacao().equals(o.getOperacao().getOperacao())).count() <= 0) {
                            operacoes.add(o.getOperacao());
                        }
                    } else {
                        if (operacoes.stream().filter(os -> os.getOperacao().equals(o.getOperacao().getOperacao())).count() <= 0) {
                            operacoes.add(o.getOperacao());
                        }
                    }
                });
            });

            Collections.sort(operacoes, (op1, op2) -> op1.getOperacao().compareTo(op2.getOperacao()));

            itens.addAll(operacoes.stream().map(i -> new SelectItemDTO<DadosSincronizacao>(i.getOperacao().concat(" - ").concat(i.getDescricao()), i)).collect(Collectors.toList()));
        }

        return itens;
    }

    @GET
    @Path("/listar-operacoes-entidades/{idUsuarioLogado}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<DadosSincronizacao> listarOperacoesEntidades(@PathParam("idUsuarioLogado") Long idUsuarioLogado) {

        List<DadosSincronizacao> operacoes = new ArrayList<>();

        final Usuario usuarioLogado = this.usuarioService.get(idUsuarioLogado);

        if (ColecaoUtil.isNotEmpty((List<?>) usuarioLogado.getPerfis())) {

            usuarioLogado.getPerfis().stream().forEach(p -> {
                p.getOperacoes().stream().forEach(o -> {
                    if (operacoes.stream().filter(os -> os.getOperacao().equals(o.getOperacao().getOperacao())).count() <= 0) {
                        operacoes.add(o.getOperacao());
                    }
                });
            });

            Collections.sort(operacoes, (op1, op2) -> op1.getOperacao().compareTo(op2.getOperacao()));
        }

        return operacoes;
    }

    @GET
    @Path("/obter-centro-logado")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obterCentroLogado() {

        return Response.ok().entity(getService().obterCentroLogado()).build();
    }

    @POST
    @Path("/consultar-itens")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Pagina<ItemNFPedidoDTO> consultarItens(Paginacao<ItemNFPedido> paginacao) {

        return this.getService().listarPaginadoItemNFPedido(paginacao);
    }

    @GET
    @Path("/buscar-ordem-venda/{ordemVenda}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarOrdemVenda(@PathParam("ordemVenda") String ordemVenda, @QueryParam("romaneio") String romaneio) {

        try {
            OrdemVendaDTO dadosOrdem = this.sapService.buscarOrdemVenda(ordemVenda);
            dadosOrdem.setSaldoOrdemVendaBloqueado(this.romaneioService.obterSaldoOrdemVendaBloqueado(ordemVenda, romaneio));

            return Response.ok(MapBuilder.create("dadosOrdem", dadosOrdem).build()).build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/buscar-saldo-ordem-venda-bloqueado/{ordemVenda}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarOrdemVendaBloqueado(@PathParam("ordemVenda") String ordemVenda, @QueryParam("romaneio") String romaneio) {

        try {
            BigDecimal saldoOrdemVendaBloqueado = this.romaneioService.obterSaldoOrdemVendaBloqueado(ordemVenda, romaneio);

            return Response.ok(MapBuilder.create("saldoOrdemVendaBloqueado", saldoOrdemVendaBloqueado).build()).build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/saldo-ordem-venda/{ordemVenda}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarSaldoOrdemVenda(@PathParam("ordemVenda") String ordemVenda) {

        try {

            RfcSaldoOrdemVendaResponse response = this.sapService.buscarSaldoOrdemVenda(ordemVenda);

            return Response.ok(MapBuilder.create("saldo", response.getSaldo()).build()).build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/obter-certificado/{material}/{tanque}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarSaldoOrdemVenda(@PathParam("material") Long material, @PathParam("tanque") String tanque) {

        try {

            return Response.ok(MapBuilder.create("certificado", carregamentoService.obterProximoCertificado(material, tanque)).build()).build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/buscar-saldo-estoque-produto-acabado/{material}/{centro}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarSaldoEstoqueProdutoAcabado(@PathParam("material") String material, @PathParam("centro") String centro) {

        try {

            final RfcSaldoEstoqueProdutoAcabadoResponse response = sapService.buscarSaldoEstoqueProdutoAcabado(material, centro);




            return Response.ok(MapBuilder.create("detalhesDoEstoque", ObjetoUtil.isNotNull(response) ? response.getDetalhesDoEstoque() : null).build()).build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/obter-por-senha/{senha}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterRomaneioPorSenha(@PathParam("senha") Long senhaId) {

        try {

            ObterRomaneioPorSenhaResponse response = getService().obterRomaneioPorSenha(senhaId);

            return Response.ok(response).build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("/buscar-ordens")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarOrdens(Paginacao<ItemNFPedido> filtro) {

        RfcOrdensVendaResponse response = sapService.buscarOrdens(filtro);

        List<TextoOrdemDTO> textosDasOrdens = getTextoOrdemDTOS(response);

        return Response.ok(MapBuilder.create("ordens", response.getOrdens().stream().sorted(Comparator.comparing(RfcOrdensVendaItemResponse::getDataOrdem).reversed()).collect(Collectors.toList())).map("textosDasOrdens", textosDasOrdens).build()).build();
    }

    private List<TextoOrdemDTO> getTextoOrdemDTOS(RfcOrdensVendaResponse response) {

        Map<String, List<RfcOrdensVendaItemTxtResponse>> listMap = response.getTextos().stream().collect(Collectors.groupingBy(RfcOrdensVendaItemTxtResponse::getOrdemVenda));

        List<TextoOrdemDTO> textosDasOrdens = new ArrayList<>();

        listMap.keySet().forEach(ordem -> {
            List<RfcOrdensVendaItemTxtResponse> rfcOrdensVendaItemTxtResponses = listMap.get(ordem);
            textosDasOrdens.add(new TextoOrdemDTO(ordem, rfcOrdensVendaItemTxtResponses.stream().map(RfcOrdensVendaItemTxtResponse::getTexto).collect(Collectors.joining(" "))));
        });
        return textosDasOrdens;
    }

    @GET
    @Path("existe-item-tipo-veiculo-seta/{tipoVeiculoSetaId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public boolean existeItemNFPedidoPorTipoVeiculoSeta(@PathParam("tipoVeiculoSetaId") Long tipoVeiculoSetaid) {

        return this.getService().existeItemNFPedidoPorTipoVeiculoSeta(tipoVeiculoSetaid);
    }

    @GET
    @Path("listar-clientes-por-romaneio/{idRomaneio}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Cliente> listarClientesPorRomaneio(@PathParam("idRomaneio") Long idRomaneio) {

        return this.getService().listarClientesPorRomaneio(idRomaneio);
    }

    @GET
    @Path("listar-nfe-por-romaneio/{idRomaneio}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<RetornoNFe> listarNfePorRomaneio(@PathParam("idRomaneio") Long idRomaneio) {

        return this.getService().listarNfePorRomaneio(idRomaneio);
    }

    @GET
    @Path("obter-motivo-estorno/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obterMotivoEstorno(@PathParam("id") Long idRomaneio) {

        return Response.ok(MapBuilder.create("motivo", getService().obterMotivoEstorno(idRomaneio)).build()).build();
    }

    @GET
    @Path("/buscar-motivos/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<Motivo> obterMotivoPorId(@PathParam("id") Long id) {

        return getService().buscarMotivosPorId(id);
    }

    @GET
    @Path("/obter-por-id-e-etapa/{id}/{etapa}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterPorIdEEtapa(@PathParam("id") Long id, @PathParam("etapa") EtapaEnum etapa) {

        try {

            ObterRomaneioPorIdEEtapaResponse response = getService().obterRomaneioPorIdEEtapa(id, etapa);

            return Response.ok(response).build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("/obter-analise-qualidade-carregamento")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<AnaliseQualidade> obterAnaliseQualidadeParaCarregamento(AnaliseFiltroDTO filtroDTO) {

        return analiseQualidadeService.obterAnaliseQualidadeParaCarregamento(filtroDTO);
    }

    @POST
    @Path("/listar/classificacao")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Pagina<RomaneioClassificacaoListagemDTO> listarClassificacaoDTO(final Paginacao<Romaneio> paginacao) {

        return romaneioService.listarClassificacaoDTO(paginacao);
    }

    @POST
    @Path("/listar/pesagem")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Pagina<RomaneioPesagemListagemDTO> listarPesagemDTO(final Paginacao<Romaneio> paginacao) {

        return romaneioService.listarPesagemDTO(paginacao);
    }

    @POST
    @Path("/listar/lacre")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Pagina<RomaneioLacreListagemDTO> listarLacreDTO(final Paginacao<Romaneio> paginacao) {

        return romaneioService.listarLacreDTO(paginacao);
    }

    @Override
    protected RomaneioService getService() {

        return romaneioService;
    }

}
