package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.ConfiguracaoEmailService;
import br.com.sjc.modelo.ConfiguracaoEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by julio.bueno on 23/07/2019.
 */
@Component
@Path("configuracoes/email")
public class ConfiguracaoEmailEndpoint extends ManutencaoEndpoint<ConfiguracaoEmail> {

    @Autowired
    private ConfiguracaoEmailService configuracaoEmailService;

    @Override
    protected ConfiguracaoEmailService getService() {
        return configuracaoEmailService;
    }

    @GET
    @Path("/consultar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultar() {
        return Response.ok(configuracaoEmailService.get()).build();
    }
}
