package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ConsultaEndpoint;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.ClienteService;
import br.com.sjc.modelo.sap.Cliente;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by julio.bueno on 27/06/2019.
 */
@Component
@Path("dados-mestres/cliente")
public class ClienteEndpoint extends ManutencaoEndpoint<Cliente> {

    @Autowired
    private ClienteService clienteService;

    @Override
    protected ClienteService getService() {
        return clienteService;
    }

    @GET
    @Path("/buscar-clientes/{value}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarClientesAutoComplete(@PathParam("value") String value) {

        List<Cliente> clientes = this.clienteService.buscarClientesAutoComplete(value);

        return Response.ok(clientes).build();
    }
}
