package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.dto.SelectItemDTO;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.PortariaService;
import br.com.sjc.modelo.cfg.Portaria;
import br.com.sjc.modelo.dto.PortariaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Path( "cadastros/portaria" )
public class PortariaEndPoint extends ManutencaoEndpoint<Portaria> {
    @Autowired
    private PortariaService portariaService;

    @Override
    protected PortariaService getService() {

        return portariaService;
    }

    @GET
    @Path("/listar-portarias")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<PortariaDTO>> listarPortarias() {

        List<SelectItemDTO<PortariaDTO>> portarias = new ArrayList<>();
        portarias.add(new SelectItemDTO<PortariaDTO>("Selecione", null));
        portarias.addAll(this.portariaService.listarPortariasAtivas().stream().map(i -> new SelectItemDTO<PortariaDTO>(i.getDescricao().toString(), i)).collect(Collectors.toList()));
        return portarias;
    }

}
