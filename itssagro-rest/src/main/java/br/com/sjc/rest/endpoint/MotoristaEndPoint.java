package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.MotoristaService;
import br.com.sjc.modelo.sap.Motorista;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Component
@Path("dados-mestres/motorista")
public class MotoristaEndPoint extends ManutencaoEndpoint<Motorista> {

    @Autowired
    private MotoristaService servico;

    @POST
    @Path("salvar-com-retorno")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response salvarComRetorno(Motorista entidade) {

        try {
            this.atribuirUsuario(entidade);
            this.getService().salvar(entidade);
            return Response.ok(entidade).build();
        } catch (Exception e) {
            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/buscar-motoristas/{value}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarMotoristasAutoComplete(@PathParam("value") String value) {

        List<Motorista> motoristas = this.servico.buscarMotoristasAutoComplete(value);

        return Response.ok(motoristas).build();
    }

    @GET
    @Path("/motoristaPorVeiculo/{veiculoId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Motorista getMorotistaPorVeiculo(@PathParam("veiculoId") Long veiculoId) {

        return this.servico.getMorotistaPorVeiculo(veiculoId);
    }

    @Override
    protected MotoristaService getService() {

        return servico;
    }

}
