package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.dto.SelectItemDTO;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.FluxoService;
import br.com.sjc.modelo.Fluxo;
import br.com.sjc.modelo.TipoFluxoEnum;
import br.com.sjc.modelo.dto.FluxoDTO;
import br.com.sjc.modelo.dto.MaterialDTO;
import br.com.sjc.util.ColecaoUtil;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Path("configuracoes/fluxo")
public class FluxoEndpoint extends ManutencaoEndpoint<Fluxo> {

    private FluxoService fluxoService;

    public FluxoEndpoint(FluxoService fluxoService) {

        this.fluxoService = fluxoService;
    }

    @GET
    @Path("/listarTiposFluxo")
    @Produces(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<TipoFluxoEnum>> listarTiposFluxo() {

        List<SelectItemDTO<TipoFluxoEnum>> selectItemDTOS = new ArrayList<>();

        selectItemDTOS.add(new SelectItemDTO<TipoFluxoEnum>("Selecione", null));

        selectItemDTOS.addAll(Arrays.asList(TipoFluxoEnum.values()).stream().map(t -> new SelectItemDTO<TipoFluxoEnum>(t.getDescricao(), t)).collect(Collectors.toList()));

        return selectItemDTOS;
    }

    @GET
    @Path("/listarItemDto")
    @Produces(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<FluxoDTO>> listarItemDto() {

        List<FluxoDTO> fluxoDTOS = fluxoService.listarOrdernandoDeFormaCrescentePorCodigo();

        List<SelectItemDTO<FluxoDTO>> selectItemDTOS = new ArrayList<>();

        selectItemDTOS.add(new SelectItemDTO<FluxoDTO>("Selecione", null));

        if (ColecaoUtil.isNotEmpty(fluxoDTOS)) {

            selectItemDTOS.addAll(fluxoDTOS.stream().map(f -> new SelectItemDTO<FluxoDTO>(f.getCodigo() + " - " + f.getDescricao(), f)).collect(Collectors.toList()));
        }

        return selectItemDTOS;
    }

    @GET
    @Path("/listarMateriaisDoFluxo/{idFluxo}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<MaterialDTO>> listarMateriaisDoFluxo(@PathParam("idFluxo") Long idFluxo) {

        List<MaterialDTO> materialDTOS = fluxoService.listarMateriaisDoFluxo(Collections.singletonList(idFluxo));

        List<SelectItemDTO<MaterialDTO>> selectItemDTOS = new ArrayList<>();

        selectItemDTOS.add(new SelectItemDTO<MaterialDTO>("Selecione", null));

        if (ColecaoUtil.isNotEmpty(materialDTOS)) {

            selectItemDTOS.addAll(materialDTOS.stream().map(f -> new SelectItemDTO<MaterialDTO>(f.getCodigo() + " - " + f.getDescricao(), f)).collect(Collectors.toList()));
        }

        return selectItemDTOS;
    }

    @Override
    protected FluxoService getService() {

        return fluxoService;
    }

}
