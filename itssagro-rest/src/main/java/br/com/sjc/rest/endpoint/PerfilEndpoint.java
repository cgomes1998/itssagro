package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.dto.SelectItemDTO;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.DadosSincronizacaoService;
import br.com.sjc.fachada.service.PerfilService;
import br.com.sjc.modelo.Perfil;
import br.com.sjc.modelo.PerfilFuncionalidade;
import br.com.sjc.modelo.PerfilOperacao;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.dto.PerfilDTO;
import br.com.sjc.modelo.enums.FuncionalidadeEnum;
import br.com.sjc.modelo.enums.ModuloEnum;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Path( "cadastros/perfil" )
public class PerfilEndpoint extends ManutencaoEndpoint<Perfil> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private PerfilService perfilService;

    @Autowired
    private DadosSincronizacaoService dadosSincronizacaoService;

    @POST
    @Consumes( MediaType.APPLICATION_JSON )
    @Produces( MediaType.APPLICATION_JSON )
    public Response salvar ( final Perfil entidade ) {

        final Perfil salvar = this.getService().salvar ( entidade );

        return Response.ok ( MapBuilder.create ( "mensagem", this.getMensagem ( "MSG001" ) ).build () ).build ();
    }

    @GET
    @Path( "obter-funcionalidades-por-perfilId/{id}" )
    @Consumes( MediaType.APPLICATION_JSON )
    @Produces( MediaType.APPLICATION_JSON )
    public Response obterFuncionalidadesPorPerfilId ( @PathParam( "id" ) Long id ) {

        List<PerfilFuncionalidade> funcionalidades = this.perfilService.listarFuncionalidadesPorPerfilId ( id );

        List<PerfilOperacao> operacoes = this.perfilService.listarOperacoesPorPerfilId ( id );

        Collections.sort ( operacoes, ( op1, op2 ) -> op1.getOperacao ().getOperacao ().compareTo ( op2.getOperacao ().getOperacao () ) );

        final List<PerfilFuncionalidade> cadastros = funcionalidades.stream ().filter ( i -> ModuloEnum.CADASTRO.equals ( i.getFuncionalidade ().getModulo () ) ).collect ( Collectors.toList () );

        final List<PerfilFuncionalidade> dadosMestres = funcionalidades.stream ().filter ( i -> ModuloEnum.DADOS_MESTRES.equals ( i.getFuncionalidade ().getModulo () ) ).collect ( Collectors.toList () );

        final List<PerfilFuncionalidade> configuracoes = funcionalidades.stream ().filter ( i -> ModuloEnum.CONFIGURACAO.equals ( i.getFuncionalidade ().getModulo () ) ).collect ( Collectors.toList () );

        final List<PerfilFuncionalidade> fluxos = funcionalidades.stream ().filter ( i -> ModuloEnum.FLUXO.equals ( i.getFuncionalidade ().getModulo () ) ).collect ( Collectors.toList () );

        final List<PerfilFuncionalidade> relatorios = funcionalidades.stream ().filter ( i -> ModuloEnum.RELATORIOS.equals ( i.getFuncionalidade ().getModulo () ) ).collect ( Collectors.toList () );

        final List<PerfilFuncionalidade> autoatendimentos = funcionalidades.stream ().filter ( i -> ModuloEnum.AUTOATENDIMENTO.equals ( i.getFuncionalidade ().getModulo () ) ).collect ( Collectors.toList () );

        return Response.ok ( MapBuilder.create ( "cadastros", cadastros )
                                     .map ( "dadosMestres", dadosMestres )
                                     .map ( "configuracoes", configuracoes )
                                     .map ( "fluxos", fluxos )
                                     .map ( "operacoes", operacoes )
                                     .map ( "relatorios", relatorios )
                                     .map ( "autoatendimentos", autoatendimentos )
                                     .build () ).build ();
    }

    @GET
    @Path( "get-perfil/{id}" )
    @Consumes( MediaType.APPLICATION_JSON )
    @Produces( MediaType.APPLICATION_JSON )
    public Response getPerfilComFuncionalidades ( @PathParam( "id" ) Long id ) {

        Perfil entidade = super.dtoGet ( id );

        final List<PerfilFuncionalidade> cadastros = entidade.getFuncionalidades ().stream ().filter ( i -> ModuloEnum.CADASTRO.equals ( i.getFuncionalidade ().getModulo () ) ).collect ( Collectors.toList () );

        final List<PerfilFuncionalidade> dadosMestres = entidade.getFuncionalidades ().stream ().filter ( i -> ModuloEnum.DADOS_MESTRES.equals ( i.getFuncionalidade ().getModulo () ) ).collect ( Collectors.toList () );

        final List<PerfilFuncionalidade> configuracoes = entidade.getFuncionalidades ().stream ().filter ( i -> ModuloEnum.CONFIGURACAO.equals ( i.getFuncionalidade ().getModulo () ) ).collect ( Collectors.toList () );

        final List<PerfilFuncionalidade> fluxos = entidade.getFuncionalidades ().stream ().filter ( i -> ModuloEnum.FLUXO.equals ( i.getFuncionalidade ().getModulo () ) ).collect ( Collectors.toList () );

        final List<PerfilFuncionalidade> relatorios = entidade.getFuncionalidades ().stream ().filter ( i -> ModuloEnum.RELATORIOS.equals ( i.getFuncionalidade ().getModulo () ) ).collect ( Collectors.toList () );

        final List<PerfilFuncionalidade> autoatendimentos = entidade.getFuncionalidades ().stream ().filter ( i -> ModuloEnum.AUTOATENDIMENTO.equals ( i.getFuncionalidade ().getModulo () ) ).collect ( Collectors.toList () );

        if ( ColecaoUtil.isNotEmpty ( ( List<?> ) entidade.getOperacoes () ) ) {

            entidade.getOperacoes ().forEach ( i -> {
                i.setOperacaoAtribuidaAoPerfil ( true );
            } );
        }

        final List<PerfilOperacao> operacoes = this.dadosSincronizacaoService.listarOperacoes ().stream ().map ( DadosSincronizacao::toPerfilOperacao ).collect ( Collectors.toList () );

        Arrays.asList ( FuncionalidadeEnum.values () ).stream ().filter ( i -> ModuloEnum.AUTOATENDIMENTO.equals ( i.getModulo () ) ).map ( FuncionalidadeEnum::toEntidade ).collect ( Collectors.toList () ).forEach ( i -> {
            if ( autoatendimentos.stream ().filter ( c -> i.getFuncionalidade ().equals ( c.getFuncionalidade () ) ).count () <= 0 ) {
                autoatendimentos.add ( i );
            }
        } );

        Arrays.asList ( FuncionalidadeEnum.values () ).stream ().filter ( i -> ModuloEnum.CADASTRO.equals ( i.getModulo () ) ).map ( FuncionalidadeEnum::toEntidade ).collect ( Collectors.toList () ).forEach ( i -> {
            if ( cadastros.stream ().filter ( c -> i.getFuncionalidade ().equals ( c.getFuncionalidade () ) ).count () <= 0 ) {
                cadastros.add ( i );
            }
        } );

        Arrays.asList ( FuncionalidadeEnum.values () ).stream ().filter ( i -> ModuloEnum.DADOS_MESTRES.equals ( i.getModulo () ) ).map ( FuncionalidadeEnum::toEntidade ).collect ( Collectors.toList () ).forEach ( i -> {
            if ( dadosMestres.stream ().filter ( c -> i.getFuncionalidade ().equals ( c.getFuncionalidade () ) ).count () <= 0 ) {
                dadosMestres.add ( i );
            }
        } );

        Arrays.asList ( FuncionalidadeEnum.values () ).stream ().filter ( i -> ModuloEnum.CONFIGURACAO.equals ( i.getModulo () ) ).map ( FuncionalidadeEnum::toEntidade ).collect ( Collectors.toList () ).forEach ( i -> {
            if ( configuracoes.stream ().filter ( c -> i.getFuncionalidade ().equals ( c.getFuncionalidade () ) ).count () <= 0 ) {
                configuracoes.add ( i );
            }
        } );

        Arrays.asList ( FuncionalidadeEnum.values () ).stream ().filter ( i -> ModuloEnum.FLUXO.equals ( i.getModulo () ) ).map ( FuncionalidadeEnum::toEntidade ).collect ( Collectors.toList () ).forEach ( i -> {
            if ( fluxos.stream ().filter ( c -> i.getFuncionalidade ().equals ( c.getFuncionalidade () ) ).count () <= 0 ) {
                fluxos.add ( i );
            }
        } );

        Arrays.asList ( FuncionalidadeEnum.values () ).stream ().filter ( i -> ModuloEnum.RELATORIOS.equals ( i.getModulo () ) ).map ( FuncionalidadeEnum::toEntidade ).collect ( Collectors.toList () ).forEach ( i -> {
            if ( fluxos.stream ().filter ( c -> i.getFuncionalidade ().equals ( c.getFuncionalidade () ) ).count () <= 0 ) {
                fluxos.add ( i );
            }
        } );

        operacoes.forEach ( i -> {
            if ( entidade.getOperacoes ().stream ().filter ( o -> i.getOperacao ().getOperacao ().equals ( o.getOperacao ().getOperacao () ) ).count () <= 0 ) {
                entidade.getOperacoes ().add ( i );
            }
        } );

        Collections.sort ( ( List<PerfilOperacao> ) entidade.getOperacoes (), ( ( o1, o2 ) -> o1.getOperacao ().getOperacao ().compareTo ( o2.getOperacao ().getOperacao () ) ) );

        return Response.ok ( MapBuilder.create ( "cadastros", cadastros )
                                     .map ( "dadosMestres", dadosMestres )
                                     .map ( "configuracoes", configuracoes )
                                     .map ( "fluxos", fluxos )
                                     .map ( "entidade", entidade )
                                     .map ( "relatorios", relatorios )
                                       .map ( "autoatendimentos", autoatendimentos )
                                     .map ( "operacoes", entidade.getOperacoes () )
                                     .build () ).build ();
    }

    @GET
    @Path( "/listar-funcionalidades" )
    @Consumes( MediaType.APPLICATION_JSON )
    @Produces( MediaType.APPLICATION_JSON )
    public Response listarFuncionalidades () {

        final List<PerfilFuncionalidade> cadastros = Arrays.asList ( FuncionalidadeEnum.values () ).stream ().filter ( i -> ModuloEnum.CADASTRO.equals ( i.getModulo () ) ).map ( FuncionalidadeEnum::toEntidade ).collect ( Collectors.toList () );

        final List<PerfilFuncionalidade> dadosMestres = Arrays.asList ( FuncionalidadeEnum.values () ).stream ().filter ( i -> ModuloEnum.DADOS_MESTRES.equals ( i.getModulo () ) ).map ( FuncionalidadeEnum::toEntidade ).collect ( Collectors.toList () );

        final List<PerfilFuncionalidade> configuracoes = Arrays.asList ( FuncionalidadeEnum.values () ).stream ().filter ( i -> ModuloEnum.CONFIGURACAO.equals ( i.getModulo () ) ).map ( FuncionalidadeEnum::toEntidade ).collect ( Collectors.toList () );

        final List<PerfilFuncionalidade> fluxos = Arrays.asList ( FuncionalidadeEnum.values () ).stream ().filter ( i -> ModuloEnum.FLUXO.equals ( i.getModulo () ) ).map ( FuncionalidadeEnum::toEntidade ).collect ( Collectors.toList () );

        final List<PerfilFuncionalidade> relatorios = Arrays.asList ( FuncionalidadeEnum.values () ).stream ().filter ( i -> ModuloEnum.RELATORIOS.equals ( i.getModulo () ) ).map ( FuncionalidadeEnum::toEntidade ).collect ( Collectors.toList () );

        final List<PerfilFuncionalidade> autoatendimentos = Arrays.asList ( FuncionalidadeEnum.values () ).stream ().filter ( i -> ModuloEnum.AUTOATENDIMENTO.equals ( i.getModulo () ) ).map ( FuncionalidadeEnum::toEntidade ).collect ( Collectors.toList () );

        final List<PerfilOperacao> operacoes = this.dadosSincronizacaoService.listarOperacoes ().stream ().map ( DadosSincronizacao::toPerfilOperacao ).collect ( Collectors.toList () );

        Collections.sort ( operacoes, ( op1, op2 ) -> op1.getOperacao ().getOperacao ().compareTo ( op2.getOperacao ().getOperacao () ) );

        return Response.ok ( MapBuilder.create ( "cadastros", cadastros )
                                     .map ( "dadosMestres", dadosMestres )
                                     .map ( "configuracoes", configuracoes )
                                     .map ( "fluxos", fluxos )
                                     .map ( "relatorios", relatorios )
                                     .map ( "operacoes", operacoes )
                                     .map("autoatendimentos", autoatendimentos)
                                     .build () ).build ();
    }

    @GET
    @Path( "listar-perfis" )
    @Produces( MediaType.APPLICATION_JSON )
    public List<SelectItemDTO<PerfilDTO>> listarPerfis () {

        List<SelectItemDTO<PerfilDTO>> perfis = new ArrayList<> ();

        perfis.add ( new SelectItemDTO<PerfilDTO> ( "Selecione", null ) );

        perfis.addAll ( this.perfilService.listar ().stream ().map ( i -> new PerfilDTO ( i ) ).map ( i -> new SelectItemDTO<PerfilDTO> ( i.getNome (), i ) ).collect ( Collectors.toList () ) );

        return perfis;
    }

    @DELETE
    @Path( "{id}" )
    @Consumes( MediaType.APPLICATION_JSON )
    @Produces( MediaType.APPLICATION_JSON )
    public Response excluir ( @PathParam( "id" ) Long id ) {

        this.getService().excluirPorId ( id );

        return Response.ok ( MapBuilder.create ( RESPONSE_MENSAGEM, this.getMensagem ( "MSG003" ) ).build () ).build ();
    }

    @Override
    protected PerfilService getService() {

        return perfilService;
    }
}
