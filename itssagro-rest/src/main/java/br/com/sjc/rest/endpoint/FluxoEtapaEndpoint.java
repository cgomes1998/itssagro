package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.dto.SelectItemDTO;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.FluxoEtapaService;
import br.com.sjc.modelo.Fluxo;
import br.com.sjc.modelo.FluxoEtapa;
import br.com.sjc.modelo.dto.ProximoFluxoEtapaDTO;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Path("configuracoes/fluxo-etapa")
public class FluxoEtapaEndpoint extends ManutencaoEndpoint<FluxoEtapa> {

    @Autowired
    private FluxoEtapaService fluxoEtapaService;

    @Override
    protected FluxoEtapaService getService() {
        return fluxoEtapaService;
    }

    @GET
    @Path("/listarCampos/{etapa}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<String>> listarCampos(@PathParam("etapa") EtapaEnum etapa) {
        List<SelectItemDTO<String>> campos = new ArrayList<>();
        campos.add(new SelectItemDTO<>("Selecione", null));
        campos.addAll(this.getService().listarCampos(etapa).stream().map(i -> new SelectItemDTO<>(i.getDescricao(), i.getCampo().name())).collect(Collectors.toList()));
        return campos;
    }

    @POST
    @Path("/salvarEtapa/{fluxoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response salvar(FluxoEtapa entidade, @PathParam("fluxoId") Long fluxoId) {
        try {
            Fluxo fluxo = new Fluxo();
            fluxo.setId(fluxoId);
            entidade.setFluxo(fluxo);
            this.atribuirUsuario(entidade);
            this.getService().salvar(entidade);
            return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSG001")).build()).build();
        } catch (Exception e) {
            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @PUT
    @Path("/alterarEtapa/{fluxoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response alterar(FluxoEtapa entidade, @PathParam("fluxoId") Long fluxoId) {
        try {
            Fluxo fluxo = new Fluxo();
            fluxo.setId(fluxoId);
            entidade.setFluxo(fluxo);
            this.atribuirUsuario(entidade);
            this.getService().salvar(entidade);
            return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSG002")).build()).build();
        } catch (Exception e) {
            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("/listarProximasEtapasDoFluxo")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<ProximoFluxoEtapaDTO> listarProximasEtapasDoFluxo(Fluxo fluxo) {
        return this.fluxoEtapaService.listarProximasEtapasDoFluxo(fluxo);
    }

}
