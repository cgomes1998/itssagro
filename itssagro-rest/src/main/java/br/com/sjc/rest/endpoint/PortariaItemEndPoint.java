package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.dto.SelectItemDTO;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.PortariaItemService;
import br.com.sjc.modelo.cfg.PortariaItem;
import br.com.sjc.modelo.dto.PortariaItemDTO;
import br.com.sjc.modelo.dto.PortariaMotivoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Path( "cadastros/portaria-item" )
public class PortariaItemEndPoint extends ManutencaoEndpoint<PortariaItem> {
    @Autowired
    private PortariaItemService portariaItemService;

    @Override
    protected PortariaItemService getService() {
        return portariaItemService;
    }

    @GET
    @Path("/listar-itens")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<PortariaItemDTO>> listarItens() {

        List<SelectItemDTO<PortariaItemDTO>> itens = new ArrayList<>();
        itens.add(new SelectItemDTO<PortariaItemDTO>("Selecione", null));
        itens.addAll(this.portariaItemService.listarItensAtivos().stream().map(i -> new SelectItemDTO<PortariaItemDTO>(i.getDescricao().toString(), i)).collect(Collectors.toList()));
        return itens;
    }
}
