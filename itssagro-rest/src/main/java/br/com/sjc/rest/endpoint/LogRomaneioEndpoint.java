package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.LogRomaneioService;
import br.com.sjc.modelo.LogRomaneio;
import br.com.sjc.modelo.dto.LogRomaneioDTO;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Component
@Path("fluxo/log-romaneio")
public class LogRomaneioEndpoint extends ManutencaoEndpoint<LogRomaneio> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private LogRomaneioService logRomaneioService;

    @POST
    @Path("salvar-log")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response salvar(Romaneio romaneio) {

        this.logRomaneioService.salvar(romaneio, this.getUsuarioToken());

        return Response.ok(MapBuilder.create("mensagem", this.getMensagem("MSG001")).build()).build();
    }

    @GET
    @Path("obter-log/{idRomaneio}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<LogRomaneioDTO> obterLogPorRomaneio(@PathParam("idRomaneio") Long idRomaneio) {

        return logRomaneioService.obterLogPorRomaneio(idRomaneio);
    }

    @Override
    protected LogRomaneioService getService() {
        return logRomaneioService;
    }
}
