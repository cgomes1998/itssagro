package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.dto.SelectItemDTO;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.PortariaVeiculoService;
import br.com.sjc.modelo.cfg.PortariaVeiculo;
import br.com.sjc.modelo.dto.PortariaMotivoDTO;
import br.com.sjc.modelo.dto.PortariaVeiculoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Path( "cadastros/portaria-veiculo" )
public class PortariaVeiculoEndPoint extends ManutencaoEndpoint<PortariaVeiculo> {
    @Autowired
    private PortariaVeiculoService portariaVeiculoService;

    @Override
    protected PortariaVeiculoService getService() {

        return portariaVeiculoService;
    }

    @GET
    @Path("/listar-veiculos")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<PortariaVeiculoDTO>> listarMotivos() {

        List<SelectItemDTO<PortariaVeiculoDTO>> veiculos = new ArrayList<>();
        veiculos.add(new SelectItemDTO<PortariaVeiculoDTO>("Selecione", null));
        veiculos.addAll(this.portariaVeiculoService.listarVeiculosAtivos().stream().map(i -> new SelectItemDTO<PortariaVeiculoDTO>(i.getDescricao().toString(), i)).collect(Collectors.toList()));
        return veiculos;
    }
}
