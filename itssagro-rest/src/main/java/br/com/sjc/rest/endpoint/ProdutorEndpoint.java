package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.ParametroService;
import br.com.sjc.fachada.service.ProdutorService;
import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.enums.TipoParametroEnum;
import br.com.sjc.modelo.sap.Produtor;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Component
@Path("dados-mestres/produtor")
public class ProdutorEndpoint extends ManutencaoEndpoint<Produtor> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private ProdutorService produtorService;

    @Autowired
    private ParametroService parametroService;

    @GET
    @Path("/obter-por-tipo-parametro/{tipo}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterPorTipoParametro(@PathParam("tipo") TipoParametroEnum tipo) {

        final Parametro parametro = this.parametroService.obterPorTipoParametro(tipo);

        return Response.ok(MapBuilder.create(ENTIDADE, this.produtorService.obterPorCodigo(parametro.getValor())).build()).build();
    }

    @GET
    @Path("/obter-por-codigo/{codigo}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterPorCodigo(@PathParam("codigo") String codigo) {

        return Response.ok(MapBuilder.create(ENTIDADE, this.produtorService.obterPorCodigo(codigo)).build()).build();
    }

    @GET
    @Path("/buscar-armazem-terceiros/{value}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarArmazemTerceirosAutoComplete(@PathParam("value") String value) {

        List<Produtor> armazens = this.produtorService.buscarArmazemTerceirosAutoComplete(value);

        return Response.ok(armazens).build();
    }

    @GET
    @Path("/buscar-produtores/{value}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarProdutoresAutoComplete(@PathParam("value") String value) {

        List<Produtor> produtores = this.produtorService.buscarProdutoresAutoComplete(value);

        return Response.ok(produtores).build();
    }

    @GET
    @Path("/gerarSenha/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response gerarSenha(@PathParam("id") Long id) {
        try {
            Produtor produtor = new Produtor();
            produtor.setId(id);
            return Response.ok(MapBuilder.create("senha", produtorService.gerarSenha(produtor)).build()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ok(ERRO, e.getMessage()).build();
        }
    }

    @Override
    protected ProdutorService getService() {
        return produtorService;
    }
}
