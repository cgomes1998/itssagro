package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.BaseEndpoint;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.AnaliseQualidadeService;
import br.com.sjc.fachada.service.LaudoService;
import br.com.sjc.modelo.Laudo;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.rest.MapBuilder;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.MessageFormat;

/**
 * Created by julio.bueno on 03/07/2019.
 */
@Component
@Path("fluxo/laudo")
public class LaudoEndpoint extends ManutencaoEndpoint<Laudo> {

    private static final String NAO_FOI_POSSIVEL_REALIZAR_A_IMPRESSAO_DO_PDF = "Não foi possivel realizar a impressão do pdf.";

    @Autowired
    private LaudoService laudoService;

    @Autowired
    private AnaliseQualidadeService analiseQualidadeService;


    @GET
    @Path("em-uso/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response emUso(@PathParam("id") Long id) {
        return Response.ok(MapBuilder.create("emUso", this.analiseQualidadeService.laudoEmUso(id)).build()).build();
    }

    @GET
    @Path("/imprimir-pdf-laudo/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response imprimirPdfLaudo(@PathParam("id") Long id) {

        try {
            final byte[] pdfBytes = this.analiseQualidadeService.imprimirLaudo(id);
            if (ObjetoUtil.isNull(pdfBytes)) {
                throw new Exception(NAO_FOI_POSSIVEL_REALIZAR_A_IMPRESSAO_DO_PDF);
            }
            return Response.ok(MapBuilder.create("pdfBytes", pdfBytes).build()).build();
        } catch (JRException e) {
            e.printStackTrace();
            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, MessageFormat.format(NAO_FOI_POSSIVEL_REALIZAR_A_IMPRESSAO_DO_PDF + ": {0}", e.getMessage())).build()).build();
        } catch (Exception e) {
            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, ObjetoUtil.isNotNull(e.getMessage()) ? e.getMessage() : e.getCause()).build()).build();
        }
    }

    @Override
    protected LaudoService getService() {

        return laudoService;
    }
}
