package br.com.sjc.rest.mapper;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class BadRequestExceptionMapper implements ExceptionMapper<BadRequestException> {

    public Response toResponse(BadRequestException exception) {
    	
    	exception.printStackTrace();

        return MapperUtils.create(Response.Status.BAD_REQUEST, exception.getMessage());
    }
}