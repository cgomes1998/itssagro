package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.rest.BaseEndpoint;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.ParametroService;
import br.com.sjc.fachada.service.PerfilService;
import br.com.sjc.fachada.service.UsuarioService;
import br.com.sjc.modelo.Perfil;
import br.com.sjc.modelo.Usuario;
import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.enums.TipoParametroEnum;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.bundle.MessageSupport;
import br.com.sjc.util.encrypt.MD5Util;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.function.Function;
import java.util.logging.Logger;

@Component
@Path("cadastros/usuario")
public class UsuarioEndpoint extends ManutencaoEndpoint<Usuario> {
    
    private static final long serialVersionUID = 1L;
    
    private static final Logger LOGGER = Logger.getLogger(UsuarioEndpoint.class.getName());
    
    @Autowired
    private UsuarioService usuarioService;
    
    @Autowired
    private PerfilService perfilService;
    
    @Autowired
    private ParametroService parametroService;
    
    @POST
    @Path("/resetar-senha")
    public Response resetarSenha(Usuario usuario) {
        
        try {
            
            final Parametro parametro = this.parametroService.obterPorTipoParametro(TipoParametroEnum.SENHA_PADRAO);
            
            usuario = this.getService().get(usuario.getId());
            
            usuario.setSenha(MD5Util.cript(parametro.getValor()));
            
            this.getService().getDAO().save(usuario);
            
            return Response.ok(MapBuilder.create(BaseEndpoint.ENTIDADE, parametro.getValor()).build()).build();
            
        } catch (Exception e) {
            
            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, e.getMessage()).build()).build();
        }
    }
    
    @POST
    @Path("/redefinirSenhaPrimeiroAcesso")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response redefinirSenhaPrimeiroAcesso(final Usuario usuario) {
    
        try {
        
            final Usuario entidade = this.getService().get(usuario.getId());
        
            entidade.setSenha(MD5Util.cript(usuario.getNovaSenha()));
        
            this.getService().getDAO().save(entidade);
        
        } catch (Exception e) {
        
            throw new ServicoException(e.getMessage());
        }
    
        return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSG001")).build()).build();
    }
    
    @POST
    @Path("/redefinir-senha")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response redefinirSenha(final Usuario usuario) {
        
        try {
            
            if (StringUtil.isNotNullEmpty(usuario.getSenha())) {
                
                final boolean existe = this.usuarioService.existeUsuarioComLoginESenha(usuario.getLogin(), MD5Util.cript(usuario.getSenha()));
                
                if (!existe) {
                    
                    throw new Exception(MessageSupport.getMessage("MSGE004"));
                }
            }
            
            final Usuario entidade = this.getService().get(usuario.getId());
            
            entidade.setSenha(MD5Util.cript(usuario.getNovaSenha()));
            
            this.getService().getDAO().save(entidade);
            
        } catch (Exception e) {
            
            throw new ServicoException(e.getMessage());
        }
        
        return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSG001")).build()).build();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response salvar(final Usuario entidade) {
        
        String senha = StringUtil.empty();
        
        if (ObjetoUtil.isNull(entidade.getId())) {
            
            try {
                
                final Parametro parametro = this.parametroService.obterPorTipoParametro(TipoParametroEnum.SENHA_PADRAO);
                
                if (ObjetoUtil.isNull(parametro)) {
                    
                    throw new Exception(this.getMensagem("MSGE002"));
                }
                
                senha = parametro.getValor();
                
                entidade.setSenha(MD5Util.cript(senha));
                
                final Usuario entidadeSaved = this.getService().salvar(entidade);
                
            } catch (Exception e) {
                
                throw new ServicoException(e.getMessage());
            }
        }
        
        return Response.ok(MapBuilder.create("mensagem", this.getMensagem("MSG001")).map("senha", senha).build()).build();
    }
    
    @GET
    @Path("/obter-perfis-para-atribuicao/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response obterPerfisParaAtribuicao(
            @PathParam("id")
                    Long id) {
        
        final Usuario entidade = this.usuarioService.get(id);
        
        final List<Perfil> perfisAtivos = this.perfilService.findByStatus(StatusEnum.ATIVO);
        
        perfisAtivos.forEach(i -> {
            i.setAtribuidoAoUsuario(entidade.getPerfis().stream().filter(p -> i.getId().equals(p.getId())).count() > 0);
        });
        
        return Response.ok(MapBuilder.create("entidade", entidade).map("perfisAtivos", perfisAtivos).build()).build();
    }
    
    @GET
    @Path("/obter-nome-por-id/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obterNomePorId(
            @PathParam("id")
                    Long id) {
        
        return Response.ok(MapBuilder.create("nome", getService().obterNomePorId(id)).build()).build();
    }
    
    @Override
    protected Function<? super Usuario, ? extends String> toStringItemSelectIem() {
        
        return u -> u.getNome();
        
    }
    
    @Override
    protected UsuarioService getService() {
        
        return usuarioService;
    }
    
}
