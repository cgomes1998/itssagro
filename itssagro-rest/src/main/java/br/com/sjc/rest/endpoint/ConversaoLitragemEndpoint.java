package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.ConversaoLitragemService;
import br.com.sjc.fachada.service.RomaneioService;
import br.com.sjc.modelo.sap.ConversaoLitragem;
import br.com.sjc.modelo.sap.Romaneio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Component
@Path( "fluxo/conversao-litragem" )
public class ConversaoLitragemEndpoint extends ManutencaoEndpoint<ConversaoLitragem> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private ConversaoLitragemService servico;

    @Autowired
    private RomaneioService romaneioService;

    @GET
    @Path( "/get-conversao/{id}" )
    @Consumes( MediaType.APPLICATION_JSON )
    @Produces( MediaType.APPLICATION_JSON )
    public Romaneio dtoGetConversao ( @PathParam( "id" ) Long id ) {

        return this.romaneioService.get ( id );
    }

    @Override
    public ConversaoLitragemService getService() {

        return this.servico;
    }
}
