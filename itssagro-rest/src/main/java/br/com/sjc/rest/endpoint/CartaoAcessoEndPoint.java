package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.modelo.CartaoAcesso;
import br.com.sjc.service.CartaoAcessoServiceImpl;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("cadastros/cartao-acesso")
public class CartaoAcessoEndPoint extends ManutencaoEndpoint<CartaoAcesso> {

	@Autowired
	private CartaoAcessoServiceImpl cartaoAcessoService;

	@GET
	@Path("obterCartaoDeAcesso/{numero}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response obterCartaoDeAcesso(@PathParam("numero") String numero) {

		Response response = null;

		try {

			response = Response.ok(cartaoAcessoService.obterCartaoDeAcesso(numero)).build();

		} catch (ServicoException e) {

			response = Response.ok(MapBuilder.create("erro", e.getMessage()).build()).build();
		}

		return response;
	}

	@Override
	public CartaoAcessoServiceImpl getService() {

		return this.cartaoAcessoService;
	}

}
