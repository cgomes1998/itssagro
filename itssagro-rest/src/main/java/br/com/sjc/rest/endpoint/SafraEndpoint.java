package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.dto.SelectItemDTO;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.SafraService;
import br.com.sjc.modelo.sap.Safra;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Path("dados-mestres/safra")
public class SafraEndpoint extends ManutencaoEndpoint<Safra> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private SafraService servico;

    @GET
    @Path("/buscar-safras/{value}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarSafrasAutoComplete(@PathParam("value") String value) {

        List<Safra> safras = this.servico.buscarSafrasAutoComplete(value);

        return Response.ok(safras).build();
    }

    @GET
    @Path("/listarItem")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<Safra>> listarItem() {
        final List<SelectItemDTO<Safra>> safras = new ArrayList<>();
        safras.add(new SelectItemDTO<>("Selecione", null));
        safras.addAll(servico.listar().stream().map(i -> new SelectItemDTO<Safra>(i.getDescricao(), i)).collect(Collectors.toList()));
        return safras;
    }

    @Override
    protected SafraService getService() {
        return servico;
    }
}
