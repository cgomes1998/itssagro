package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.LoteAnaliseQualidadeService;
import br.com.sjc.modelo.LoteAnaliseQualidade;
import br.com.sjc.modelo.dto.LoteAnaliseQualidadeMotivoInativacaoDTO;
import br.com.sjc.modelo.response.FileResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Component
@Path("cadastros/lote-analise-qualidade")
public class LoteAnaliseQualidadeEndPoint extends ManutencaoEndpoint<LoteAnaliseQualidade> {

    @Autowired
    private LoteAnaliseQualidadeService loteAnaliseQualidadeService;

    @Override
    public LoteAnaliseQualidadeService getService() {

        return this.loteAnaliseQualidadeService;
    }

    @POST
    @Path("/alterarStatusLoteAnaliseQualidade")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void alterarStatusLoteAnaliseQualidade(LoteAnaliseQualidade loteAnaliseQualidade) {

        loteAnaliseQualidadeService.alterarStatusLoteAnaliseQualidade(loteAnaliseQualidade);
    }

    @POST
    @Path("/obterLote")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public LoteAnaliseQualidade obterLote(LoteAnaliseQualidade loteAnaliseQualidade) {

        return loteAnaliseQualidadeService.obterLote(loteAnaliseQualidade);
    }

    @GET
    @Path("/obterMotivoInativacaoDoLote/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public LoteAnaliseQualidadeMotivoInativacaoDTO obterMotivoInativacaoDoLote(@PathParam("id") Long id) {

        LoteAnaliseQualidade loteAnaliseQualidade = new LoteAnaliseQualidade();

        loteAnaliseQualidade.setId(id);

        return loteAnaliseQualidadeService.obterMotivoInativacaoDoLote(loteAnaliseQualidade);
    }

    @POST
    @Path("obterRelatorioLotes")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<byte[]> obterRelatorioLotes(LoteAnaliseQualidade entidadeFiltro) {

        try {

            FileResponse file = loteAnaliseQualidadeService.obterRelatorioLotes(entidadeFiltro);

            HttpHeaders headers = new HttpHeaders();

            headers.setContentDispositionFormData(file.getName(), file.getName());

            return new ResponseEntity<byte[]>(file.getBuffer(), headers, HttpStatus.OK);

        } catch (Exception e) {

            e.printStackTrace();

            return null;
        }
    }
}
