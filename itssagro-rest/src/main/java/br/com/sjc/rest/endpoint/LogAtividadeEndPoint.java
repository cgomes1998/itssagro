package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.LogAtividadeService;
import br.com.sjc.modelo.LogAtividade;
import br.com.sjc.modelo.dto.LogAtividadeDTO;
import br.com.sjc.modelo.enums.EntidadeTipo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Component
@Path("log-atividade")
public class LogAtividadeEndPoint extends ManutencaoEndpoint<LogAtividade> {

    @Autowired
    private LogAtividadeService logAtividadeService;

    @Override
    public LogAtividadeService getService() {

        return this.logAtividadeService;
    }

    @GET
    @Path("/getLogAtividades/{entidadeTipo}/{entidadeId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<LogAtividadeDTO> getLogAtividades(@PathParam("entidadeTipo") EntidadeTipo entidadeTipo, @PathParam("entidadeId") Long entidadeId) {

        return logAtividadeService.getLogAtividades(entidadeTipo, entidadeId);
    }
}
