package br.com.sjc.rest.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import br.com.sjc.fachada.excecoes.ServicoException;

@Provider
public class ServicoExceptionMapper implements ExceptionMapper<ServicoException> {

    @Override
    public Response toResponse(ServicoException exception) {

        return MapperUtils.create(Response.Status.fromStatusCode(412), exception.buildMensagem());
    }
}
