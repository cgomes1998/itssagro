package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.BaseEndpoint;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.BalancaService;
import br.com.sjc.modelo.Balanca;
import br.com.sjc.modelo.dto.CapturaBalanca;
import br.com.sjc.socket.TcpIpConectService;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.bundle.MessageSupport;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.MessageFormat;
import java.util.List;

@Component
@Path( "cadastros/balanca" )
public class BalancaEndPoint extends ManutencaoEndpoint<Balanca> {

    @Autowired
    private BalancaService balancaService;

    @GET
    @Path( "teste-conexao/{ip}/{porta}/{salto}/{tempoEsperandoDados}" )
    @Consumes( MediaType.APPLICATION_JSON )
    @Produces( MediaType.APPLICATION_JSON )
    public Response testarConexaoBalanca (
            @PathParam( "ip" ) String ip,
            @PathParam( "porta" ) Integer porta,
            @PathParam( "salto" ) Integer salto,
            @PathParam( "tempoEsperandoDados" ) Integer tempoEsperandoDados
    ) {

        try {

            final Double peso = TcpIpConectService.testeConexaoServidor ( ip, porta, salto, tempoEsperandoDados );

            return Response.ok ( MapBuilder.create ( BaseEndpoint.RESPONSE_MENSAGEM, MessageFormat.format ( "Teste de conexão realizado com sucesso. Peso capturado {0}", peso.toString () ) ).build () ).build ();

        } catch ( Exception e ) {

            return Response.ok ( MapBuilder.create ( BaseEndpoint.ERRO, MessageFormat.format ( MessageSupport.getMessage ( "MSGE013" ), e.getMessage () ) ).build () ).build ();
        }
    }

    @POST
    @Path( "obter-peso" )
    @Consumes( MediaType.APPLICATION_JSON )
    @Produces( MediaType.APPLICATION_JSON )
    public Response capturarPeso ( CapturaBalanca entidade ) {

        List<Balanca> balancas = this.getService().obterBalancasPorDirecaoETipoPeso ( entidade );

        if ( ColecaoUtil.isNotEmpty ( balancas ) ) {

            if ( balancas.size () == 1 ) {

                try {

                    final Balanca balanca = balancas.get ( 0 );

                    return Response.ok ( MapBuilder.create ( "peso", TcpIpConectService.obterPeso ( balanca.getIp (), balanca.getPorta (), balanca.getTamanhoSaltos (), balanca.getTempoMaximoEspera (), 5000 ) ).build () ).build ();

                } catch ( Exception e ) {

                    return Response.ok ( MapBuilder.create ( BaseEndpoint.ERRO, MessageFormat.format ( "Erro ao obter o peso da balança: {0}", e.getMessage () ) ).build () ).build ();
                }

            } else {

                return Response.ok ( MapBuilder.create ( "balancas", balancas ).build () ).build ();
            }
        }

        return Response.ok ( MapBuilder.create ( "peso", new Double ( 0 ) ).build () ).build ();
    }

    @POST
    @Path( "obter-peso-balanca" )
    @Consumes( MediaType.APPLICATION_JSON )
    @Produces( MediaType.APPLICATION_JSON )
    public Response capturarPesoPorBalanca ( Balanca balanca ) {

        try {

            return Response.ok ( MapBuilder.create ( "peso", TcpIpConectService.obterPeso ( balanca.getIp (), balanca.getPorta (), balanca.getTamanhoSaltos (), balanca.getTempoMaximoEspera (), 5000 ) ).build () ).build ();

        } catch ( Exception e ) {

            return Response.ok ( MapBuilder.create ( BaseEndpoint.ERRO, MessageFormat.format ( "Erro ao obter o peso da balança: {0}", e.getMessage () ) ).build () ).build ();
        }
    }

    @POST
    @Consumes( MediaType.APPLICATION_JSON )
    @Produces( MediaType.APPLICATION_JSON )
    public Response salvar ( final Balanca entidade ) {

        final Balanca salvar = this.getService().salvar ( entidade );

        return Response.ok ( MapBuilder.create ( "mensagem", this.getMensagem ( "MSG001" ) ).build () ).build ();
    }

    @DELETE
    @Path( "{id}" )
    @Consumes( MediaType.APPLICATION_JSON )
    @Produces( MediaType.APPLICATION_JSON )
    public Response excluir ( @PathParam( "id" ) Long id ) {

        this.getService().excluirPorId ( id );

        return Response.ok ( MapBuilder.create ( RESPONSE_MENSAGEM, this.getMensagem ( "MSG003" ) ).build () ).build ();
    }

    @Override
    protected BalancaService getService() {

        return balancaService;
    }
}
