package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.EventoAutomacaoGraosService;
import br.com.sjc.modelo.EventoAutomacaoGraos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Path;

@Component
@Path("configuracoes/evento-automacao-graos")
public class EventoAutomacaoGraosEndPoint extends ManutencaoEndpoint<EventoAutomacaoGraos> {

    @Autowired
    private EventoAutomacaoGraosService servico;

    @Override
    public EventoAutomacaoGraosService getService() {
        return this.servico;
    }
}
