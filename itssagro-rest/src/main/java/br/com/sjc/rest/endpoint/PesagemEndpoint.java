package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.PesagemService;
import br.com.sjc.fachada.service.RomaneioService;
import br.com.sjc.modelo.Pesagem;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.util.ObjetoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Component
@Path("fluxo/pesagem")
public class PesagemEndpoint extends ManutencaoEndpoint<Pesagem> {

	@Autowired
	private PesagemService servico;

	@Autowired
	private RomaneioService romaneioService;

	@GET
	@Path("/get-pesagem/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Romaneio dtoGetPesagem(@PathParam("id") Long id) {

		final Romaneio entidadeSaved = this.romaneioService.get(id);
		if (ObjetoUtil.isNull(entidadeSaved.getPesagem())) {
			entidadeSaved.setPesagem(new Pesagem());
			if (entidadeSaved.getOperacao().isPossuiDivisaoCarga()) {
				entidadeSaved.getPesagem()
				             .setPlacaCavalo(entidadeSaved.getItens()
				                                          .stream()
				                                          .filter(i -> ObjetoUtil.isNotNull(i.getPlacaCavalo()))
				                                          .map(i -> i.getPlacaCavalo())
				                                          .findFirst()
				                                          .orElse(null));
				entidadeSaved.getPesagem()
				             .setSafra(entidadeSaved.getItens()
				                                    .stream()
				                                    .filter(i -> ObjetoUtil.isNotNull(i.getSafra()))
				                                    .map(i -> i.getSafra())
				                                    .findFirst()
				                                    .orElse(null));
			} else {
				entidadeSaved.getPesagem().setPlacaCavalo(entidadeSaved.getPlacaCavalo());
				entidadeSaved.getPesagem().setSafra(entidadeSaved.getSafra());
			}
		}
		return entidadeSaved;
	}

	@Override
	public PesagemService getService() {

		return this.servico;
	}

}
