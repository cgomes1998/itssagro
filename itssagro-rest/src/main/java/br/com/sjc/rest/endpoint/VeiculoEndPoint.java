package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.VeiculoService;
import br.com.sjc.modelo.sap.Veiculo;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Component
@Path( "dados-mestres/veiculo" )
public class VeiculoEndPoint extends ManutencaoEndpoint<Veiculo> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private VeiculoService servico;

    @POST
    @Path( "salvar-com-retorno" )
    @Consumes( MediaType.APPLICATION_JSON )
    @Produces( MediaType.APPLICATION_JSON )
    public Response salvarComRetorno ( Veiculo entidade ) {

        try {
            this.atribuirUsuario ( entidade );
            this.getService().salvar ( entidade );
            return Response.ok ( entidade ).build ();
        } catch ( Exception e ) {
            return Response.ok ( MapBuilder.create ( ERRO, e.getMessage () ).build () ).build ();
        }
    }

    @GET
    @Path( "/buscar-veiculos/{value}" )
    @Produces( MediaType.APPLICATION_JSON )
    @Consumes( MediaType.APPLICATION_JSON )
    public Response buscarVeiculosAutoComplete ( @PathParam( "value" ) String value ) {

        List<Veiculo> veiculos = this.servico.buscarVeiculosAutoComplete ( value );

        return Response.ok ( veiculos ).build ();
    }

    @GET
    @Path( "/buscar-veiculos/ativos/{value}" )
    @Produces( MediaType.APPLICATION_JSON )
    @Consumes( MediaType.APPLICATION_JSON )
    public Response buscarVeiculosAtivosAutoComplete ( @PathParam( "value" ) String value ) {

        List<Veiculo> veiculos = this.servico.buscarVeiculosAtivosAutoComplete ( value );

        return Response.ok ( veiculos ).build ();
    }

    @Override
    protected VeiculoService getService() {

        return servico;
    }
}
