package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.BaseEndpoint;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.AgendamentoCargaPontualService;
import br.com.sjc.fachada.service.RomaneioService;
import br.com.sjc.fachada.service.SenhaService;
import br.com.sjc.modelo.Motivo;
import br.com.sjc.modelo.Senha;
import br.com.sjc.modelo.SenhaFormulario;
import br.com.sjc.modelo.dto.ConsultaSenhaMotoristaEntrada;
import br.com.sjc.modelo.dto.PainelSenhaDTO;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.rest.MapBuilder;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.MessageFormat;
import java.util.List;

@Component
@Path("fluxo/senha")
public class SenhaEndPoint extends ManutencaoEndpoint<Senha> {

    @Autowired
    private SenhaService senhaService;

    @Autowired
    private RomaneioService romaneioService;

    @Autowired
    private AgendamentoCargaPontualService agendamentoCargaPontualService;

    @POST
    @Path("trocaStatusEntradaMotorista")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response trocaStatusEntradaMotorista(Senha senha) {

        try {

            if (ObjetoUtil.isNotNull(senha.getMotorista())) {

                senha.getMotorista().verificarSeMotoristaEstaBloqueado();
            }

            this.agendamentoCargaPontualService.trocaStatusEntradaMotorista(senha);

            return Response.noContent().build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("salvar-com-retorno")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response salvarComRetorno(Senha entidade) {

        try {
            this.atribuirUsuario(entidade);
            this.getService().salvar(entidade);
            return Response.ok(entidade).build();
        } catch (Exception e) {
            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("/gerar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response gerar(Senha senha) {

        try {
            if (ObjetoUtil.isNotNull(senha.getMotorista())) {
                senha.getMotorista().verificarSeMotoristaEstaBloqueado();
            }
            this.romaneioService.validarSePlacaEstaSendoUtilizada(senha.getVeiculo(), null, senha.getId());
            this.atribuirUsuario(senha);
            this.getService().gerar(senha);
            return Response.ok(senha).build();
        } catch (Exception e) {
            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("/alterar-status")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response alterarStatus(Senha senha) {

        try {
            this.atribuirUsuario(senha);
            this.getService().alterarStatusSenha(senha);
            return Response.ok(senha).build();
        } catch (Exception e) {
            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("/enviar-email-liberacao")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response enviarEmailLiberacao(Senha senha) {

        try {
            this.atribuirUsuario(senha);
            this.getService().enviarEmailLiberacao(senha);
            return Response.ok(senha).build();
        } catch (Exception e) {
            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("/imprimir-pdf/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response imprimirPdf(@PathParam("id") Long id) {

        try {
            final byte[] pdfBytes = this.getService().imprimirPdf(id);
            if (ObjetoUtil.isNull(pdfBytes)) {
                throw new Exception("Não foi possivel realizar a impressão do pdf");
            }
            return Response.ok(MapBuilder.create("pdfBytes", pdfBytes).build()).build();
        } catch (JRException e) {
            e.printStackTrace();
            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, MessageFormat.format("Erro inesperado ao realizar a impressão da senha: {0}", e.getMessage())).build()).build();
        } catch (Exception e) {
            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("obter-senha/{senha}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterPorSenha(@PathParam("senha") String senha) {

        try {
            Senha obj = this.getService().obterPorSenha("#".concat(senha));
            if (ObjetoUtil.isNull(obj)) {
                throw new Exception("Nenhum registro encontrado.");
            }
            return Response.ok(MapBuilder.create(ENTIDADE, obj).build()).build();
        } catch (Exception e) {
            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("obter-ultima-senha")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Senha obterUltimaSenhaChamada() {

        return this.senhaService.obterUltimaSenhaChamada();
    }

    @GET
    @Path("obter-ultimas-senhas/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<PainelSenhaDTO> obterUltimasSenhasChamadas(@PathParam("id") Long id) {

        return this.senhaService.obterUltimasSenhasChamadas(id);
    }

    @POST
    @Path("salvar-formulario")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response salvarFormulario(SenhaFormulario entidade) {

        try {
            this.atribuirUsuario(entidade);
            Senha senha = this.getService().salvarFormulario(entidade);
            return Response.ok(MapBuilder.create(ENTIDADE, senha).build()).build();
        } catch (Exception e) {
            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("existe-tipo-veiculo-seta/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public boolean existeSenhaPorTipoVeiculoSeta(@PathParam("id") Long tipoVeiculoSetaid) {

        return this.senhaService.existeSenhaPorTipoVeiculoSeta(tipoVeiculoSetaid);
    }

    @GET
    @Path("/buscar-senhas/{value}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarSenhasAutoComplete(@PathParam("value") String value) {

        return Response.ok(this.senhaService.buscarSenhasAutoComplete(value)).build();
    }

    @GET
    @Path("/buscar-motivos/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<Motivo> obterMotivoPorId(@PathParam("id") Long id) {

        return senhaService.obterMotivoPorId(id);
    }

    @POST
    @Path("/obterPorIdAgendamentoOuPlaca")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterPorIdAgendamentoOuPlaca(ConsultaSenhaMotoristaEntrada consultaSenhaMotoristaEntrada) {

        return Response.ok(senhaService.obterPorIdAgendamentoOuPlaca(consultaSenhaMotoristaEntrada)).build();
    }

    @Override
    protected SenhaService getService() {

        return senhaService;
    }

}
