package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.dto.SelectItemDTO;
import br.com.sjc.fachada.rest.BaseEndpoint;
import br.com.sjc.fachada.service.DashboardGerencialService;
import br.com.sjc.fachada.service.FluxoEtapaService;
import br.com.sjc.fachada.service.FluxoService;
import br.com.sjc.modelo.dto.DashboardGerencialFiltroDTO;
import br.com.sjc.modelo.dto.DashboardGerencialFiltroEtapaDTO;
import br.com.sjc.modelo.dto.FluxoEtapaDTO;
import br.com.sjc.modelo.dto.MaterialDTO;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Path("fluxo/dashboard-gerencial")
public class DashboardGerencialEndpoint extends BaseEndpoint {

    private DashboardGerencialService dashboardGerencialService;

    private FluxoEtapaService fluxoEtapaService;

    private FluxoService fluxoService;

    public DashboardGerencialEndpoint(DashboardGerencialService dashboardGerencialService, FluxoEtapaService fluxoEtapaService, FluxoService fluxoService) {
        this.dashboardGerencialService = dashboardGerencialService;
        this.fluxoEtapaService = fluxoEtapaService;
        this.fluxoService = fluxoService;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response obterFilas(DashboardGerencialFiltroDTO filtro) {
        try {
            return Response.ok(this.dashboardGerencialService.obterFilas(filtro)).build();
        } catch (Exception e) {
            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("/filtrar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterFilasEtapas(DashboardGerencialFiltroEtapaDTO filtroEtapa) {

        try {

            return Response.ok(this.dashboardGerencialService.obterFilas(filtroEtapa)).build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("obterEtapasPorFluxo")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<FluxoEtapaDTO>> obterEtapasPorFluxo(List<Long> idFluxos) {

        List<FluxoEtapaDTO> fluxoEtapaDTOS = fluxoEtapaService.listarDtoPorFluxo(idFluxos);

        return fluxoEtapaDTOS.stream().map(f -> new SelectItemDTO<FluxoEtapaDTO>(f.getEtapa().getDescricao(), f)).collect(Collectors.toList());
    }

    @POST
    @Path("obterMateriaisPorFluxo")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<MaterialDTO> obterMateriaisPorFluxo(List<Long> idFluxos) {

        return fluxoService.listarMateriaisDoFluxo(idFluxos);
    }
}
