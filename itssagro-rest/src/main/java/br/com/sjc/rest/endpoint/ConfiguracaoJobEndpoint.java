package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.*;
import br.com.sjc.modelo.cfg.ConfiguracaoJob;
import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.dto.ConfiguracaoJobDTO;
import br.com.sjc.modelo.enums.TipoJobEnum;
import br.com.sjc.modelo.enums.TipoParametroEnum;
import br.com.sjc.util.MailUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Component
@Path("configuracoes/jobs")
public class ConfiguracaoJobEndpoint extends ManutencaoEndpoint<ConfiguracaoJob> {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = Logger.getLogger(ConfiguracaoJobEndpoint.class.getName());
    private static final String IMAPS = "imaps";

    @Autowired
    private ConfiguracaoJobService servico;

    @Autowired
    private SAPService sapService;

    @Autowired
    private ParametroService parametroService;

    @Autowired
    private IvaService ivaService;

    @Autowired
    private AgendamentoCargaPontualService agendamentoCargaPontualService;

    @GET
    @Path("/listar-jobs")
    @Produces({MediaType.APPLICATION_JSON})
    public Response listarJobs() {

        List<ConfiguracaoJob> jobs = super.listar();

        List<ConfiguracaoJob> pre = jobs.stream().filter(i -> TipoJobEnum.PRE_CAD.equals(i.getTipoJob())).collect(Collectors.toList());

        Collections.sort(pre, (p1, p2) -> p1.getDadosSincronizacao().getOperacao().compareTo(p2.getDadosSincronizacao().getOperacao()));

        List<ConfiguracaoJob> sync = jobs.stream().filter(i -> TipoJobEnum.SYNC.equals(i.getTipoJob())).collect(Collectors.toList());

        Collections.sort(sync, (s1, s2) -> s1.getDadosSincronizacao().getOperacao().compareTo(s2.getDadosSincronizacao().getOperacao()));

        return Response.ok(MapBuilder.create("pre", pre.stream().map(i -> new ConfiguracaoJobDTO(i)).collect(Collectors.toList()))
                .map("sync", sync.stream().map(i -> new ConfiguracaoJobDTO(i)).collect(Collectors.toList())).build()).build();
    }

    @POST
    @Path("/executar-sincronizacao")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response executarSincronizacao(final ConfiguracaoJob job) {

        try {

            switch (job.getDadosSincronizacao()) {

                case AGENDAMENTO_CARGA_PONTUAL:
                    this.agendamentoCargaPontualService.gerarSenhaCargaPontual(job.getDataUltimaSincronizacao());
                    break;

                case DADOS_CONTRATO:
                case CLIENTE:
                case MOTORISTA:
                case VEICULO: {
                    this.sapService.sincronizarRFC(job.getDadosSincronizacao(), job.getDataUltimaSincronizacao());
                    break;
                }

                case XML_NFE: {

                    new Thread() {

                        @Override
                        public void run() {

                            List<Parametro> parametros = parametroService.obterPorTipoParametro(TipoParametroEnum.listarParametrosParaDonwloadNfe());

                            final Parametro usuario = parametros.stream().filter(i -> TipoParametroEnum.MAIL_USUARIO.equals(i.getTipoParametro())).findFirst().orElse(null);

                            final Parametro senha = parametros.stream().filter(i -> TipoParametroEnum.MAIL_PASSWORD.equals(i.getTipoParametro())).findFirst().orElse(null);

                            final Parametro host = parametros.stream().filter(i -> TipoParametroEnum.MAIL_IMAP_HOST.equals(i.getTipoParametro())).findFirst().orElse(null);

                            final Parametro porta = parametros.stream().filter(i -> TipoParametroEnum.MAIL_IMAP_PORT.equals(i.getTipoParametro())).findFirst().orElse(null);

                            final Parametro diretorio = parametros.stream().filter(i -> TipoParametroEnum.DIRETORIO_XML.equals(i.getTipoParametro())).findFirst().orElse(null);

                            if (ObjetoUtil.isNotNull(usuario) && ObjetoUtil.isNotNull(senha) && ObjetoUtil.isNotNull(host) && ObjetoUtil.isNotNull(porta) && ObjetoUtil.isNotNull(diretorio)) {

                                MailUtil.downloadDeAnexos(job.getDataUltimaSincronizacao(), host.getValor(), porta.getValor(), IMAPS, usuario.getValor(), senha.getValor(), diretorio.getValor());
                            }
                        }
                    }.start();

                    break;
                }

                case IVA: {

                    new Thread() {

                        @Override
                        public void run() {

                            ivaService.sincronizar();
                        }

                    }.start();

                    break;
                }

                case NFE_NOTA_ST_ROMANEIO: {

                    this.sapService.atualizarRomaneios(null, job.getDataUltimaSincronizacao());

                    break;
                }

                case ROMANEIO: {

                    new Thread() {

                        @Override
                        public void run() {

                            sapService.reenviarRomaneioPorData(job.getDataUltimaSincronizacao());
                        }
                    }.start();

                    break;
                }

                default: {

                    this.sapService.sincronizar(job.getDadosSincronizacao(), job.getDataUltimaSincronizacao());

                    break;
                }
            }

        } catch (Exception e) {

            e.printStackTrace();

            return Response.ok(MapBuilder.create("erro", "ERRO NA SINCRONIZAÇÃO DE DADOS: " + e.getMessage()).build()).build();
        }

        return Response.ok(MapBuilder.create("sucesso", this.getMensagem("MSG001")).build()).build();
    }

    @POST
    @Path("/executar-pre-cadastro")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response executarPreCadastro(final ConfiguracaoJob job) {

        try {

            this.sapService.enviarCadastro(job.getDadosSincronizacao(), null);

        } catch (Exception e) {

            return Response.ok(MapBuilder.create("erro", "ERRO NA SINCRONIZAÇÃO DE DADOS: " + e.getMessage()).build()).build();
        }

        return Response.ok(MapBuilder.create("sucesso", this.getMensagem("MSG001")).build()).build();
    }

    @Override
    protected ConfiguracaoJobService getService() {
        return servico;
    }
}
