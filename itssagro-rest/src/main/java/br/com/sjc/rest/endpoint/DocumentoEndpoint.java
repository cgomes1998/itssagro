package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.DocumentoService;
import br.com.sjc.modelo.Documento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Path;
import java.util.function.Function;

/**
 * Created by julio.bueno on 03/07/2019.
 */
@Component
@Path("cadastros/documento")
public class DocumentoEndpoint extends ManutencaoEndpoint<Documento> {

    @Autowired
    private DocumentoService documentoService;

    @Override
    protected DocumentoService getService() {
        return documentoService;
    }

    protected Function<? super Documento, ? extends String> toStringItemSelectIem() {
        return t -> t.getDescricao();
    }

}
