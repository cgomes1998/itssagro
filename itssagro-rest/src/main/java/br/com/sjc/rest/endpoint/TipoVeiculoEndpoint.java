package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.dto.SelectItemDTO;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.TipoVeiculoService;
import br.com.sjc.modelo.dto.TipoVeiculoSetaDTO;
import br.com.sjc.modelo.sap.TipoVeiculo;
import br.com.sjc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Path("dados-mestres/tipo-veiculo")
public class TipoVeiculoEndpoint extends ManutencaoEndpoint<TipoVeiculo> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private TipoVeiculoService servico;

    @GET
    @Path("/listarItemDto")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<TipoVeiculo>> listarItemDto() {

        final List<SelectItemDTO<TipoVeiculo>> tiposVeiculos = new ArrayList<>();
        tiposVeiculos.add(new SelectItemDTO<>("Selecione", null));
        tiposVeiculos.addAll(servico.listar().stream().map(i -> new SelectItemDTO<TipoVeiculo>(i.toString(), i)).collect(Collectors.toList()));
        return tiposVeiculos;
    }

    @GET
    @Path("/listar-setas/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<TipoVeiculoSetaDTO>> listarSetas(@PathParam("id") Long id) {

        final List<SelectItemDTO<TipoVeiculoSetaDTO>> tiposVeiculos = new ArrayList<>();
        tiposVeiculos.add(new SelectItemDTO<>("Selecione", null));
        tiposVeiculos.addAll(servico.listarSetasDtoPorTipoVeiculo(id).stream().map(i -> new SelectItemDTO<TipoVeiculoSetaDTO>(MessageFormat.format("{0} - {1}", i.getDescricao(), StringUtil.toMilhar(i.getQtdValida(), 2)), i)).collect(Collectors.toList()));
        return tiposVeiculos;
    }

    @Override
    protected TipoVeiculoService getService() {

        return servico;
    }
}
