package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.dto.SelectItemDTO;
import br.com.sjc.fachada.rest.BaseEndpoint;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.FormularioService;
import br.com.sjc.modelo.Formulario;
import br.com.sjc.modelo.FormularioItem;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static br.com.sjc.modelo.rest.MediaType.APPLICATION_PDF;

/**
 * Created by julio.bueno on 04/07/2019.
 */
@Component
@Path("cadastros/formulario")
public class FormularioEndpoint extends ManutencaoEndpoint<Formulario> {

    @Autowired
    private FormularioService formularioService;

    @Autowired
    private Environment environment;

    @Override
    protected FormularioService getService() {

        return formularioService;
    }

    @GET
    @Path("/verificarSeFormularioEstaEmUso/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response verificarSeFormularioEstaEmUso(@PathParam("id") Long id) {

        return Response.ok(MapBuilder.create("valor", formularioService.verificarSeFormularioEstaEmUso(id)).build()).build();
    }

    @POST
    @Path("/publicar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response publicar(Formulario formulario) {

        return onExecuteService(formulario, new OnExecuteServiceCallBack() {

            @Override
            public void executar() throws Exception {

                formularioService.publicar(formulario);
            }

            @Override
            public String getMensagem() {

                return "formulario.MSGS002";
            }
        });
    }

    @POST
    @Path("/descontinuar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response descontinuar(Formulario formulario) {

        return onExecuteService(formulario, new OnExecuteServiceCallBack() {

            @Override
            public void executar() throws Exception {

                formularioService.descontinuar(formulario);
            }

            @Override
            public String getMensagem() {

                return "formulario.MSGS003";
            }
        });
    }

    @POST
    @Path("/clonar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response clonar(Formulario formulario) {

        return onExecuteService(formulario, () -> formularioService.clonar(formulario.getId()));
    }

    @POST
    @Path("/download")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response download(Formulario formulario) {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        formularioService.gerarPdfFormulario(formulario.getId(), byteArrayOutputStream);

        return downloadArquiboBase64(formulario.getDescricao() + ".pdf", APPLICATION_PDF, byteArrayOutputStream);
    }

    @GET
    @Path("/download-romaneio/{idFormulario}/{idRomaneio}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response downloadRomaneio(
            @PathParam("idFormulario") Long idFormulario,
            @PathParam("idRomaneio") Long idRomaneio
    ) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            formularioService.gerarPdfFormularioRomaneio(idFormulario, idRomaneio, byteArrayOutputStream);

            return downloadArquiboBase64(idRomaneio.toString() + ".pdf", APPLICATION_PDF, byteArrayOutputStream);

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, ObjetoUtil.isNotNull(e.getMessage()) ? e.getMessage() : e.getCause()).build()).build();
        }
    }

    @GET
    @Path("/listarItens/{formularioId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<FormularioItem> listarCampos(@PathParam("formularioId") Long formularioId) {

        return formularioService.listarItens(formularioId);
    }

    @GET
    @Path("/listar-publicados")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<Formulario>> listarPublicados() {

        List<SelectItemDTO<Formulario>> formularios = new ArrayList<>();
        formularios.add(new SelectItemDTO<Formulario>("Selecione", null));
        formularios.addAll(this.formularioService.listarPublicados().stream().map(i -> new SelectItemDTO<Formulario>(i.getDescricao(), i)).collect(Collectors.toList()));
        return formularios;
    }

    protected Function<? super Formulario, ? extends String> toStringItemSelectIem() {

        return Formulario::getDescricao;
    }

}
