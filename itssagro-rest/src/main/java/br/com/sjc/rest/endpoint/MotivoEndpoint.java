package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.MotivoService;
import br.com.sjc.modelo.Motivo;
import br.com.sjc.modelo.enums.MotivoTipoEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by julio.bueno on 24/06/2019.
 */
@Component
@Path("cadastros/motivo")
public class MotivoEndpoint extends ManutencaoEndpoint<Motivo> {

    @Autowired
    private MotivoService motivoService;

    @Override
    protected MotivoService getService() {
        return motivoService;
    }

    @GET
    @Path("getMotivosPorTipo/{tipo}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getMotivosPorTipo(@PathParam("tipo") MotivoTipoEnum motivoTipoEnum) {
        return Response.ok(motivoService.getMotivosPorTipo(motivoTipoEnum)).build();
    }
}
