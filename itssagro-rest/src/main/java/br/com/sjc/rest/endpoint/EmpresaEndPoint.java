package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.EmpresaService;
import br.com.sjc.modelo.cfg.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;

@Component
@Path( "cadastros/empresa" )
public class EmpresaEndPoint extends ManutencaoEndpoint<Empresa> {

    @Autowired
    private EmpresaService empresaService;

    @Override
    protected EmpresaService getService() {

        return empresaService;
    }
}
