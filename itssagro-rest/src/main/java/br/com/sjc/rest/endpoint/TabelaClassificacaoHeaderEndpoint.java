package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.modelo.sap.TabelaClassificacaoHeader;
import br.com.sjc.service.TabelaClassificacaoHeaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Path;

@Component
@Path("dados-mestres/tabela-classificacao/header")
public class TabelaClassificacaoHeaderEndpoint extends ManutencaoEndpoint<TabelaClassificacaoHeader> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private TabelaClassificacaoHeaderService servico;

    @Override
    protected TabelaClassificacaoHeaderService getService() {
        return servico;
    }
}
