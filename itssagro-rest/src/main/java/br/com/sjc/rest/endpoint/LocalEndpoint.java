package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.dto.SelectItemDTO;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.LocalService;
import br.com.sjc.modelo.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by julio.bueno on 03/07/2019.
 */
@Component
@Path("cadastros/local")
public class LocalEndpoint extends ManutencaoEndpoint<Local> {

    @Autowired
    private LocalService localService;

    @GET
    @Path("/listarItem")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public List<SelectItemDTO<Local>> listarItem() {
        List<SelectItemDTO<Local>> locais = new ArrayList<>();
        locais.add(new SelectItemDTO<Local>("Selecione", null));
        locais.addAll(this.getService().listarLocaisAtivos().stream().map(i -> new SelectItemDTO<Local>(i.toString(), i)).collect(Collectors.toList()));
        return locais;
    }

    @Override
    protected LocalService getService() {
        return localService;
    }
}
