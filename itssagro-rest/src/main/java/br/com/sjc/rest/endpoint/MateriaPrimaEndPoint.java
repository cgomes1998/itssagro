package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.MateriaPrimaService;
import br.com.sjc.modelo.Documento;
import br.com.sjc.modelo.MateriaPrima;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Path;
import java.util.function.Function;

@Component
@Path("dados-mestres/materia-prima")
public class MateriaPrimaEndPoint extends ManutencaoEndpoint<MateriaPrima> {
    private static final long serialVersionUID = 1L;

    @Autowired
    private MateriaPrimaService servico;

    @Override
    protected MateriaPrimaService getService() {

        return servico;
    }

    protected Function<? super MateriaPrima, ? extends String> toStringItemSelectIem() {
        return t -> t.getDescricao();
    }
}
