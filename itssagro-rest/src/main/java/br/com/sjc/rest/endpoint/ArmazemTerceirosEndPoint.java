package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.BaseEndpoint;
import br.com.sjc.fachada.service.*;
import br.com.sjc.modelo.Usuario;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.dto.RomaneioArmazemTercDTO;
import br.com.sjc.modelo.endpoint.request.DMRequest;
import br.com.sjc.modelo.endpoint.request.RomaneioRequest;
import br.com.sjc.modelo.endpoint.response.DMResponse;
import br.com.sjc.modelo.endpoint.response.MensagemResponse;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.persistencia.dao.RomaneioArmazemTerceirosDAO;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.bundle.MessageSupport;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Set;
import java.util.SortedSet;
import java.util.stream.Collectors;

@Component
@Path("sync")
public class ArmazemTerceirosEndPoint {

    @Autowired
    private RomaneioService romaneioService;

    @Autowired
    private ProdutorService produtorService;

    @Autowired
    private TransportadoraService transportadoraService;

    @Autowired
    private VeiculoService veiculoService;

    @Autowired
    private MotoristaService motoristaService;

    @Autowired
    private PesagemService pesagemService;

    @Autowired
    private ClassificacaoService classificacaoService;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private CentroService centroService;

    @Autowired
    private SafraService safraService;

    @Autowired
    private MaterialService materialService;

    @Autowired
    private DepositoService depositoService;

    @Autowired
    private DadosSincronizacaoService dadosSincronizacaoService;

    @Autowired
    private RomaneioArmazemTerceirosDAO romaneioArmazemTerceirosDAO;

    @GET
    @PermitAll
    @Path("consultar-romaneio/{numero}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarStatusRomaneioENfe(@PathParam("numero") String numero) {

        try {

            final Romaneio entidade = this.romaneioService.obterRomaneioPorTicketOrigem(numero);

            return Response.ok(MapBuilder.create(BaseEndpoint.ENTIDADE, new RomaneioArmazemTercDTO(entidade)).build()).build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @PermitAll
    @Path("set-romaneio")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response registrarRomaneio(@HeaderParam("usuario") final String usuario, final RomaneioRequest request) {

        try {

            if (ObjetoUtil.isNotNull(request) && ColecaoUtil.isNotEmpty(request.getRomaneio())) {

                request.getRomaneio().forEach(romaneioRequestItem -> {

                    final Usuario usuarioArmazem = this.usuarioService.obterPorLogin(romaneioRequestItem.getUsuarioIntegracao());

                    final Centro centro = StringUtil.isNotNullEmpty(romaneioRequestItem.getCodigoCentro()) ? this.centroService.obterPorCodigo(romaneioRequestItem.getCodigoCentro()) : null;

                    final DadosSincronizacao operacao = this.dadosSincronizacaoService.obterPorEntidade(DadosSincronizacaoEnum.ENTRADA_ARMAZEM_DE_TERCEIROS);

                    Romaneio romaneio = new Romaneio();

                    final Set<ItemNFPedido> itens = romaneioRequestItem.getItens().stream().map(item -> {

                        final Produtor produtor = StringUtil.isNotNullEmpty(item.getCodigoFornecedor()) ? this.produtorService.obterPorCodigo(item.getCodigoFornecedor()) : null;

                        final Safra safra = StringUtil.isNotNullEmpty(item.getCodigoSafra()) ? this.safraService.obterPorCodigo(item.getCodigoSafra()) : null;

                        final Material material = StringUtil.isNotNullEmpty(item.getCodigoMaterial()) ? this.materialService.obterPorCodigo(item.getCodigoMaterial()) : null;

                        final Deposito deposito = StringUtil.isNotNullEmpty(item.getCodigoDeposito()) ? this.depositoService.obterPorCodigo(item.getCodigoDeposito()) : null;

                        final Transportadora transportadora = StringUtil.isNotNullEmpty(item.getCodigoTransportadora()) ? this.transportadoraService.obterPorCodigo(item.getCodigoTransportadora()) : null;

                        final Motorista motorista = StringUtil.isNotNullEmpty(item.getCodigoMotorista()) ? this.motoristaService.obterPorCodigo(item.getCodigoMotorista()) : null;

                        final Veiculo placaCavalo = StringUtil.isNotNullEmpty(item.getCodigoPlacaCavalo()) ? this.veiculoService.obterPorCodigo(item.getCodigoPlacaCavalo()) : null;

                        final Veiculo placaCarretaUm = StringUtil.isNotNullEmpty(item.getPlacaCarretaUm()) ? this.veiculoService.obterPorCodigo(item.getPlacaCarretaUm()) : null;

                        final Veiculo placaCarretaDois = StringUtil.isNotNullEmpty(item.getPlacaCarretadois()) ? this.veiculoService.obterPorCodigo(item.getPlacaCarretadois()) : null;

                        return item.toEntidade(produtor, safra, material, deposito, transportadora, motorista, placaCavalo, placaCarretaUm, placaCarretaDois);

                    }).collect(Collectors.toSet());

                    final ItemNFPedido itemNFPedido = itens.stream().filter(i -> ObjetoUtil.isNotNull(i.getPlacaCavalo()) && ObjetoUtil.isNotNull(i.getSafra())).findFirst().get();

                    romaneio = romaneioRequestItem.toEntidade(usuarioArmazem, centro, operacao, itemNFPedido.getPlacaCavalo(), itemNFPedido.getSafra());

                    romaneio.setItens(new ArrayList(itens));

                    romaneio.setRomaneioArmazemTerceiros(this.romaneioArmazemTerceirosDAO.save(romaneio.getRomaneioArmazemTerceiros()));

                    romaneio.setPesagem(this.pesagemService.salvar(romaneio.getPesagem()));

                    romaneio.setClassificacao(this.classificacaoService.salvarClassificacaoDeArmazemTerceiros(romaneio.getClassificacao()));

                    if (operacao.isPossuiClassificacaoOrigem()) {

                        romaneio.setItensClassificacaoOrigem(romaneio.getItensClassificacaoOrigem());
                    }

                    this.romaneioService.salvarRomaneio(romaneio);

                });

                return Response.ok(new MensagemResponse("S", MessageSupport.getMessage("MSG001"))).build();
            }

        } catch (Exception e) {

            return Response.ok(new MensagemResponse("E", e.getMessage())).build();
        }

        return null;
    }

    @POST
    @PermitAll
    @Path("dm")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDadosMestres(@HeaderParam("usuario") final String usuario, final DMRequest request) {

        if (ObjetoUtil.isNotNull(request)) {

            final DMResponse response = new DMResponse();

            response.setFornecedores(this.produtorService.listarParaArmazemDeTerceiros(request.getFornecedores()));

            response.setTransportadoras(this.transportadoraService.listarParaArmazemDeTerceiros(request.getTransportadoras()));

            response.setMotoristas(this.motoristaService.listarParaArmazemTerceiros(request.getMotoristas()));

            response.setVeiculos(this.veiculoService.listarParaArmazemTerceiros(request.getVeiculos()));

            return Response.ok(response).build();
        }

        return null;
    }
}
