package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.dto.SelectItemDTO;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.rest.paginacao.Pagina;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.CampoOperacaoMaterialService;
import br.com.sjc.fachada.service.MaterialService;
import br.com.sjc.modelo.cfg.CampoOperacaoMaterial;
import br.com.sjc.modelo.dto.MaterialDTO;
import br.com.sjc.modelo.sap.Material;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Path("dados-mestres/material")
public class MaterialEndpoint extends ManutencaoEndpoint<Material> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private MaterialService servico;

    @Autowired
    private CampoOperacaoMaterialService campoOperacaoMaterialService;

    @GET
    @Path("/listarMateriaisAtivos")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<MaterialDTO> listarMateriaisAtivos() {

        return this.servico.listarMateriaisAtivos();
    }

    @GET
    @Path("/listar-materiais")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<MaterialDTO>> listarMateriais() {

        List<SelectItemDTO<MaterialDTO>> materiais = new ArrayList<>();
        materiais.add(new SelectItemDTO<MaterialDTO>("Selecione", null));
        materiais.addAll(this.servico.listarMateriaisAtivos().stream().map(i -> new SelectItemDTO<MaterialDTO>(i.toString(), i)).collect(Collectors.toList()));
        return materiais;
    }

    @POST
    @Path("/listar-materiais")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Pagina<?> listarComPaginacao(final Paginacao<MaterialDTO> paginacao) {

        return this.getService().listarComPaginacao(paginacao);
    }

    @POST
    @Path("/obter-campos-material")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<CampoOperacaoMaterial> obterCamposPorMaterial(Material material) {

        return campoOperacaoMaterialService.obterCamposPorMaterial(material);
    }

    @Override
    protected MaterialService getService() {

        return servico;
    }

}
