package br.com.sjc.rest.endpoint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FiltroAtualizacao {

    private Date dataAtualizacao;
}
