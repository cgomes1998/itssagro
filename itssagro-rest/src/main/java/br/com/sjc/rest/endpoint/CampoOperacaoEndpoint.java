package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.dto.SelectItemDTO;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.CampoOperacaoService;
import br.com.sjc.fachada.service.DadosSincronizacaoService;
import br.com.sjc.modelo.cfg.CampoOperacao;
import br.com.sjc.modelo.dto.CampoOperacaoDTO;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Path("configuracoes/campos")
public class CampoOperacaoEndpoint extends ManutencaoEndpoint<CampoOperacao> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private CampoOperacaoService servico;

    @Autowired
    private DadosSincronizacaoService dadosSincronizacaoService;

    @POST
    @Path("/salvar-todos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response salvarTodos(List<CampoOperacao> campos) {

        this.servico.salvar(campos);

        return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSG001")).build()).build();
    }

    @GET
    @Path(("/listar-operacoes"))
    @Produces(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<DadosSincronizacaoEnum>> listarOperacoes() {

        List<SelectItemDTO<DadosSincronizacaoEnum>> itens = new ArrayList<>();

        itens.add(new SelectItemDTO<>("Selecione", null));

        itens.addAll(this.dadosSincronizacaoService.listarOperacoes().stream().map(i -> new SelectItemDTO<DadosSincronizacaoEnum>(i.getOperacaoDescricao(), i.getEntidade())).collect(Collectors.toList()));

        return itens;
    }

    @GET
    @Path(("/listar-operacoes-config"))
    @Produces(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<DadosSincronizacaoEnum>> listarOperacoesConfiguraveis() {

        List<SelectItemDTO<DadosSincronizacaoEnum>> itens = new ArrayList<>();

        itens.add(new SelectItemDTO<>("Selecione", null));

        itens.addAll(Arrays.asList(new DadosSincronizacaoEnum[]{DadosSincronizacaoEnum.VEICULO, DadosSincronizacaoEnum.MOTORISTA, DadosSincronizacaoEnum.SENHA}).stream().map(i -> new SelectItemDTO<>(i.getDescricao(), i)).collect(Collectors.toList()));

        itens.addAll(this.dadosSincronizacaoService.listarOperacoes().stream().map(i -> new SelectItemDTO<DadosSincronizacaoEnum>(i.getOperacaoDescricao(), i.getEntidade())).collect(Collectors.toList()));

        return itens;
    }

    @GET
    @Path(("/obter-campos/{operacao}"))
    @Produces(MediaType.APPLICATION_JSON)
    public List<CampoOperacaoDTO> obterCampos(@PathParam("operacao") DadosSincronizacaoEnum operacao) {

        return this.servico.obterCamposPorOperacao(operacao);
    }

    @Override
    protected CampoOperacaoService getService() {

        return servico;
    }

}
