package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.ClassificacaoService;
import br.com.sjc.fachada.service.RomaneioService;
import br.com.sjc.modelo.Classificacao;
import br.com.sjc.modelo.sap.ItemNFPedido;
import br.com.sjc.modelo.sap.ItensClassificacao;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.TabelaClassificacao;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Path("fluxo/classificacao")
public class ClassificacaoEndpoint extends ManutencaoEndpoint<Classificacao> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private ClassificacaoService classificacaoService;

    @Autowired
    private RomaneioService romaneioService;

    @GET
    @Path("/get-classificacao/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Romaneio dtoGetClassificacao(@PathParam("id") Long id) {

        final Romaneio entidadeSaved = this.romaneioService.get(id);

        if (ObjetoUtil.isNull(entidadeSaved.getClassificacao())) {

            entidadeSaved.setClassificacao(new Classificacao());

            if (entidadeSaved.getOperacao().isPossuiDivisaoCarga()) {

                entidadeSaved.getClassificacao().setPlacaCavalo(entidadeSaved.getItens().stream().filter(i -> ObjetoUtil.isNotNull(i.getPlacaCavalo()))
                        .map(ItemNFPedido::getPlacaCavalo)
                        .findFirst()
                        .orElse(null));

                entidadeSaved.getClassificacao().setSafra(entidadeSaved.getItens().stream().filter(i -> ObjetoUtil.isNotNull(i.getSafra()))
                        .map(ItemNFPedido::getSafra)
                        .findFirst()
                        .orElse(null));

            } else {

                entidadeSaved.getClassificacao().setPlacaCavalo(entidadeSaved.getPlacaCavalo());

                entidadeSaved.getClassificacao().setSafra(entidadeSaved.getSafra());
            }
        }

        return entidadeSaved;
    }

    @GET
    @Path("/obter-itens-classificacao/{codigoMaterial}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obterItensClassificacao(@PathParam("codigoMaterial") String codigoMaterial) {

        final List<TabelaClassificacao> itensSAP = this.classificacaoService.listarItensClassificacaoPorMaterial(codigoMaterial);

        final List<ItensClassificacao> itensRomaneio = itensSAP.stream().map(TabelaClassificacao::toItensClassificacao).collect(Collectors.toSet()).stream().collect(Collectors.toList());

        Collections.sort(itensRomaneio, (ItensClassificacao a1, ItensClassificacao a2) -> a1.getItemClassificao().compareTo(a2.getItemClassificao()));

        return Response.ok(MapBuilder.create("itensSAP", itensSAP).map("itensRomaneio", itensRomaneio).build()).build();
    }

    @POST
    @Path("/indiceInformadoExisteNaTabelaDeClassificacao")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public boolean indiceInformadoExisteNaTabelaDeClassificacao(ItensClassificacao itensClassificacao) {
        return classificacaoService.indiceInformadoExisteNaTabelaDeClassificacao(itensClassificacao);
    }

    @Override
    public ClassificacaoService getService() {

        return this.classificacaoService;
    }

}
