package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.dto.SelectItemDTO;
import br.com.sjc.fachada.rest.BaseEndpoint;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.TabelaClassificacaoService;
import br.com.sjc.modelo.dto.TabelaClassificacaoDTO;
import br.com.sjc.modelo.sap.TabelaClassificacao;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Path("dados-mestres/tabela-classificacao")
public class TabelaClassificacaoEndpoint extends ManutencaoEndpoint<TabelaClassificacao> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private TabelaClassificacaoService servico;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/listar-itens-descricao")
    public List<SelectItemDTO<String>> listarDescricaoItemClassificacao() {

        List<SelectItemDTO<String>> itens = new ArrayList<>();

        itens.add(new SelectItemDTO<String>("Selecione", null));

        itens.addAll(this.getService().listarTabelaOrdernandoPorItem().stream().map(i -> new SelectItemDTO<String>(i, i)).collect(Collectors.toList()));

        return itens;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/salvar-tabela")
    public Response salvarTabela(final TabelaClassificacao entidade) {

        this.getService().atualizarIndices(entidade.getItens());

        this.getService().adicionarItensParaCalculoDeIndice(entidade);

        this.getService().atualizarCodigoArmazemDeTerceiros(entidade);

        this.getService().atualizarCodigoDoLeitor(entidade);

        return Response.ok(MapBuilder.create(BaseEndpoint.RESPONSE_MENSAGEM, this.getMensagem("MSG001")).build()).build();
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("query")
    public TabelaClassificacaoDTO get(@QueryParam("itemClassificacao") String itemClassificacao, @QueryParam("codigoMaterial") String codigoMaterial) {
        return this.getService().listByItemClassificacao(itemClassificacao, codigoMaterial);
    }

    @Override
    protected TabelaClassificacaoService getService() {
        return servico;
    }
}
