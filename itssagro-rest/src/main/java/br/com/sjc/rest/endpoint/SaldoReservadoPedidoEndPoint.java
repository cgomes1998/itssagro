package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.SaldoReservadoPedidoService;
import br.com.sjc.modelo.SaldoReservadoPedido;
import br.com.sjc.modelo.request.SaldoPedidoReservadoRequest;
import br.com.sjc.modelo.response.SaldoPedidoReservadoResponse;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("fluxo/romaneio/saldo-reservado-pedido")
public class SaldoReservadoPedidoEndPoint extends ManutencaoEndpoint<SaldoReservadoPedido> {

    @Autowired
    private SaldoReservadoPedidoService servico;

    @POST
    @Path("/obter-saldo-reservado-pedido")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response obterSaldoReservadoPedido(SaldoPedidoReservadoRequest request) {

        try {

            SaldoPedidoReservadoResponse response = this.servico.obterSaldoReservadoPedido(request);

            return Response.ok().entity(response).build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @Override
    public SaldoReservadoPedidoService getService() {
        return this.servico;
    }
}
