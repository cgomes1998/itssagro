package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.rest.paginacao.Pagina;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.LacreService;
import br.com.sjc.fachada.service.LoteService;
import br.com.sjc.modelo.Lacre;
import br.com.sjc.modelo.Lote;
import br.com.sjc.modelo.dto.LacreCountDTO;
import br.com.sjc.modelo.enums.StatusLacreEnum;
import br.com.sjc.modelo.view.VwLacreDetalhe;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Component
@Path("cadastros/lacre")
public class LoteEndpoint extends ManutencaoEndpoint<Lote> {

    @Autowired
    private LoteService loteService;

    @Autowired
    private LacreService lacreService;

    @Override
    protected LoteService getService() {

        return loteService;
    }

    @PUT
    @Path("status-lacre")
    @PermitAll
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response statusLacre(Long id) {

        try {
            final Lacre lacre = this.lacreService.get(id);
            this.atribuirUsuario(lacre);
            lacre.setStatusLacre(StatusLacreEnum.INATIVO.equals(lacre.getStatusLacre()) ? StatusLacreEnum.DISPONIVEL : StatusLacreEnum.INATIVO);
            this.lacreService.alterarStatus(lacre);
            return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSG002")).build()).build();
        } catch (Exception e) {
            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @DELETE
    @Path("excluir-lacre/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response excluirLacre(@PathParam("id") Long id) {

        try {
            this.lacreService.excluirPorId(id);
            return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSG003")).build()).build();
        } catch (Exception e) {
            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("alterar-status-todos-lacres")
    @PermitAll
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response alterarTodosStatusLacre(Lote lote) {

        try {
            this.lacreService.alterarTodosStatusLacre(lote);
            return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSG002")).build()).build();
        } catch (Exception e) {
            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @GET
    @Path("buscar-lacres/{value}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response buscarLacresAutoComplete(@PathParam("value") String value) {

        try {
            List<Lacre> lacres = this.lacreService.buscarLacresAutoComplete(value);
            return Response.ok(lacres).build();
        } catch (Exception e) {
            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("/listar-lacres")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Pagina<Lacre> listarLacres(final Paginacao<Lacre> paginacao) {

        return this.lacreService.listarLacres(paginacao);
    }

    @GET
    @Path("/listar-lacres/{loteId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarLacres(@PathParam("loteId") final Long loteId) {

        try {
            return Response.ok(this.lacreService.listarLacres(loteId)).build();
        } catch (Exception e) {
            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @POST
    @Path("/obter-lacres-vinculados-romaneio")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<VwLacreDetalhe> obterLacreVinculadoARomaneio(final List<Lacre> lacres) {
        return lacreService.obterLacreVinculadoARomaneio(lacres);
    }

    @GET
    @Path("/obterTotalDeLacresPorStatus/{statusLacre}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<LacreCountDTO> obterTotalDeLacresPorStatus(@PathParam("statusLacre") StatusLacreEnum statusLacre) {
        return lacreService.obterTotalDeLacresPorStatus(statusLacre);
    }

    @GET
    @Path("/obterTotalDeLacres")
    @Produces(MediaType.APPLICATION_JSON)
    public List<LacreCountDTO> obterTotalDeLacres() {
        return lacreService.obterTotalDeLacresPorStatus(null);
    }

    @POST
    @Path("/inutilizarLacres")
    @Consumes(MediaType.APPLICATION_JSON)
    public void inutilizarLacres(List<Lacre> lacres) {
        lacreService.inutilizarLacres(lacres);
    }

    @POST
    @Path("/status-lacres")
    @Consumes(MediaType.APPLICATION_JSON)
    public void alterarStatusLacres(List<Lacre> lacres) {
        lacreService.alterarStatusDosLacres(lacres);
    }
}
