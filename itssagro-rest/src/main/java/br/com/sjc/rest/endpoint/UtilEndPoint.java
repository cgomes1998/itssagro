package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.dto.SelectItemDTO;
import br.com.sjc.fachada.rest.BaseEndpoint;
import br.com.sjc.fachada.service.AmbienteService;
import br.com.sjc.fachada.service.CidadeService;
import br.com.sjc.fachada.service.SAPService;
import br.com.sjc.fachada.service.TabelaClassificacaoService;
import br.com.sjc.modelo.cfg.Ambiente;
import br.com.sjc.modelo.enums.TipoParametroEnum;
import br.com.sjc.modelo.enums.UFEnum;
import br.com.sjc.modelo.sap.TabelaClassificacao;
import br.com.sjc.modelo.sap.sync.SAPSync;
import br.com.sjc.util.rest.MapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Path("util")
public class UtilEndPoint {

    @Autowired
    private CidadeService cidadeService;

    @Autowired
    private AmbienteService ambienteService;

    @Autowired
    private SAPService sapService;

    @Autowired
    private TabelaClassificacaoService tabelaClassificacaoService;

    @Autowired
    private RestTemplate restTemplate;

    @GET
    @Path("/leitor-umidade/obter-peso/{idClassificacao}")
    @Produces({"application/json"})
    public Response obterPeso(@PathParam("idClassificacao") Long idClassificacao) {

        TabelaClassificacao tabelaClassificacao = this.tabelaClassificacaoService.get(idClassificacao);

        if(tabelaClassificacao != null){
            // URL do endpoint externo
            String url = String.format("http://%s:%s/leitorUmidade/api/leitor/capturarIndice/%s", tabelaClassificacao.getHostLeitor(), tabelaClassificacao.getPortaLeitor(), tabelaClassificacao.getCodigoLeitor());

            // Fazendo a chamada GET e recebendo a resposta
            ResponseEntity<Double> response = restTemplate.getForEntity(url, Double.class);

            // Verificando se a resposta foi bem-sucedida
            if (response.getStatusCode().is2xxSuccessful()) {
                Double result = response.getBody();

                return Response.ok(result).build();
            } else {
                return Response.serverError().build();
            }
        }

        return Response.ok(BaseEndpoint.ERRO, "Não foi encontrada a configuração da Classificação.").build();
    }


    @GET
    @Path("/ping-sap")
    @Produces({"application/json"})
    @Scheduled(fixedDelay = 300000)
    public Response realizarPingSAP() {

        final Ambiente ambiente = this.ambienteService.obterAmbienteLogado();

        try {

            this.sapService.realizarPing(ambiente);

            SAPSync.ONLINE = true;

        } catch (Exception e) {

            SAPSync.ONLINE = false;
        }

        return Response.ok(MapBuilder.create("sapOnline", SAPSync.ONLINE).map("ambiente", ambiente.getDescricao()).build()).build();
    }

    @GET
    @Path("/sap-online")
    @Produces({"application/json"})
    public Response isSapOnline() {

        final Ambiente ambiente = this.ambienteService.obterAmbienteLogado();

        return Response.ok(MapBuilder.create("sapOnline", SAPSync.ONLINE).map("ambiente", ambiente.getDescricao()).build()).build();
    }

    @GET
    @Path("/tipos-parametros")
    @Produces({"application/json"})
    public List<SelectItemDTO<TipoParametroEnum>> listarTiposParametros() {

        List<SelectItemDTO<TipoParametroEnum>> tipos = new ArrayList<>();

        tipos.add(new SelectItemDTO<>("Selecione", null));

        List<TipoParametroEnum> tipoParametroEnums = Arrays.asList(TipoParametroEnum.values());

        Collections.sort(tipoParametroEnums, (TipoParametroEnum t1, TipoParametroEnum t2) -> t1.getDescricao().compareTo(t2.getDescricao()));

        tipos.addAll(tipoParametroEnums.stream().map((i) -> new SelectItemDTO<TipoParametroEnum>(i.getDescricao(), i)).collect(Collectors.toList()));

        return tipos;
    }

    /**
     * Metodo responsavel por listar todos as UFs
     */
    @GET
    @Path("/ufs")
    @Produces({"application/json"})
    public List<SelectItemDTO<UFEnum>> listarUFs() {

        List<SelectItemDTO<UFEnum>> uf = new ArrayList();

        uf.add(new SelectItemDTO<>("Selecione", null));

        uf.addAll(Arrays.asList(UFEnum.values()).stream().map(i -> new SelectItemDTO<UFEnum>(i.getDescricao(), i)).collect(Collectors.toList()));

        return uf;
    }

    @GET
    @Path("/cidades/{uf}")
    @Produces({"application/json"})
    public List<SelectItemDTO<String>> listarCidadesUF(@PathParam("uf") String uf) {

        List<SelectItemDTO<String>> cidades = new ArrayList();

        cidades.add(new SelectItemDTO<>("Selecione", null));

        cidades.addAll(this.getServico().listarCidadeUF(UFEnum.valueOf(uf)).stream().map(i -> new SelectItemDTO<String>(i.getNome(), i.getNome())).collect(Collectors.toList()));

        return cidades;
    }

    protected CidadeService getServico() {
        return cidadeService;
    }

}
