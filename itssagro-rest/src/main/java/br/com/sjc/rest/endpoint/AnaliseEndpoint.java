package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.BaseEndpoint;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.AnaliseService;
import br.com.sjc.fachada.service.LogConversaoLitragemService;
import br.com.sjc.modelo.Analise;
import br.com.sjc.modelo.LogConversaoLitragem;
import br.com.sjc.modelo.dto.AnaliseFiltroDTO;
import br.com.sjc.util.ItssAgroThreadPool;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.rest.MapBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Component
@Path("dados-mestres/analiseItem")
@Slf4j
public class AnaliseEndpoint extends ManutencaoEndpoint<Analise> {

    private static final long serialVersionUID = 1L;

    @Autowired
    private AnaliseService servico;

    @Autowired
    private LogConversaoLitragemService logConversaoLitragemService;

    @GET
    @Path("/material/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Analise> listarPorMaterial(@PathParam("id") Long id) {

        return this.getService().listarPorMaterial(id);
    }

    @POST
    @Path("/obter-por-material")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Analise obterAnalisePorMaterial(AnaliseFiltroDTO filtroDTO) {

        return this.getService().obterAnalisePorMaterial(filtroDTO);
    }

    @POST
    @Path("/salvarLogConversaoLitragem")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void salvarLogConversaoLitragem(LogConversaoLitragem logConversaoLitragem) {

        ItssAgroThreadPool.executarTarefa(() -> logConversaoLitragemService.salvar(logConversaoLitragem));
    }

    @Override
    public Response salvar(Analise entidade) {
        log.info("entidade: {}", entidade);

        try {

            this.servico.validar(entidade);

            return super.salvar(entidade);

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(BaseEndpoint.ERRO, StringUtil.isNotNullEmpty(e.getMessage()) ? e.getMessage() : "NULLPOINTER EXCEPTION").build()).build();
        }
    }

    @Override
    protected AnaliseService getService() {

        return servico;
    }

}
