package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.TabelaValorService;
import br.com.sjc.modelo.Arquivo;
import br.com.sjc.modelo.TabelaValor;
import br.com.sjc.modelo.TabelaValorItem;
import br.com.sjc.modelo.sap.ConversaoLitragem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Component
@Path("cadastros/tabela-valor")
public class TabelaValorEndpoint extends ManutencaoEndpoint<TabelaValor> {

    @Autowired
    private TabelaValorService tabelaValorService;

    @Override
    protected TabelaValorService getService() {

        return this.tabelaValorService;
    }

    @POST
    @Path("/arquivo")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<TabelaValorItem> importarArquivo(Arquivo arquivo) {

        return tabelaValorService.importarArquivo(arquivo);
    }

    @GET
    @Path("/obter-por-material/{material}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TabelaValor obterPorMaterial(@PathParam("material") Long materialId) {

        return this.getService().obterPorMaterial(materialId);
    }

    @POST
    @Path("/obter-itens-para-conversao")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TabelaValorItem obterItensParaConversao(final ConversaoLitragem item) {

        return getService().obterItensParaConversao(item);
    }

}
