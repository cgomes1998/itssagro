package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.dto.SelectItemDTO;
import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.PortariaMotivoService;
import br.com.sjc.modelo.cfg.PortariaMotivo;
import br.com.sjc.modelo.dto.PortariaDTO;
import br.com.sjc.modelo.dto.PortariaMotivoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Path( "cadastros/portaria-motivo" )
public class PortariaMotivoEndPoint extends ManutencaoEndpoint<PortariaMotivo> {
    @Autowired
    private PortariaMotivoService portariaMotivoService;

    @Override
    protected PortariaMotivoService getService() {

        return portariaMotivoService;
    }

    @GET
    @Path("/listar-motivos")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<SelectItemDTO<PortariaMotivoDTO>> listarMotivos() {

        List<SelectItemDTO<PortariaMotivoDTO>> portarias = new ArrayList<>();
        portarias.add(new SelectItemDTO<PortariaMotivoDTO>("Selecione", null));
        portarias.addAll(this.portariaMotivoService.listarMotivosAtivos().stream().map(i -> new SelectItemDTO<PortariaMotivoDTO>(i.getDescricao().toString(), i)).collect(Collectors.toList()));
        return portarias;
    }
}
