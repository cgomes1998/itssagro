package br.com.sjc.rest.endpoint;

import br.com.sjc.fachada.rest.ManutencaoEndpoint;
import br.com.sjc.fachada.service.LogSenhaService;
import br.com.sjc.modelo.LogSenha;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Path;

@Component
@Path("cadastros/log-senha")
public class LogSenhaEndPoint extends ManutencaoEndpoint<LogSenha> {

    @Autowired
    private LogSenhaService logSenhaService;

    @Override
    protected LogSenhaService getService() {
        return logSenhaService;
    }

}
