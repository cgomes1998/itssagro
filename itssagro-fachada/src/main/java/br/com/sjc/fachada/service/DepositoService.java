package br.com.sjc.fachada.service;

import br.com.sjc.modelo.sap.Deposito;

import java.util.List;

public interface DepositoService extends Servico<Long, Deposito> {

    Deposito obterPorCodigo(String codigo);

    Deposito obterUnicoPorCodigo(String codigo);

    List<Deposito> obterDepositosPorCentroId(Long idCentro);

    List<Deposito> obterDepositosPorCentroLogado();
}
