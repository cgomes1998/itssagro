package br.com.sjc.fachada.servico.impl;

import br.com.sjc.fachada.excecoes.ServicoException;
import br.com.sjc.fachada.service.LogAtividadeService;
import br.com.sjc.fachada.service.Servico;
import br.com.sjc.modelo.*;
import br.com.sjc.modelo.enums.EntidadeTipo;
import br.com.sjc.modelo.enums.LogAtividadeAcao;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.util.*;
import br.com.sjc.util.bundle.MessageSupport;
import br.com.sjc.util.transformers.TransformerTuple;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.stream.Collectors;

public abstract class ServicoGenerico<ID extends Serializable, E extends ObjectID<ID>> extends ServicoGenericoConsultar<ID, E> implements Servico<ID, E> {

    private Usuario usuarioLogado;

    @Autowired
    private LogAtividadeService logAtividadeService;

    @Override
    public Usuario getUsuarioLogado() {

        return usuarioLogado;
    }

    @Override
    public void setUsuarioLogado(final Usuario usuarioLogado) {

        this.usuarioLogado = usuarioLogado;
    }

    public String nomeUsuarioLogado() {

        if (usuarioLogado != null) {

            return String.format("%s - %s", this.getUsuarioLogado().getLogin(), this.getUsuarioLogado().getNome());

        }

        return "";

    }

    @Override
    public E salvar(E entidade) {

        this.preSalvar(entidade);

        Boolean cadastro = Boolean.FALSE;

        Usuario usuarioLogado = null;

        if (entidade instanceof EntidadeGenerica) {
            EntidadeGenerica entidadeGenerica = (EntidadeGenerica) entidade;

            usuarioLogado = entidadeGenerica.getUsuarioLogado();

            if (entidadeGenerica.getId() == null) {

                cadastro = Boolean.TRUE;

                entidadeGenerica.setDataCadastro(DateUtil.hoje());

            } else {

                entidadeGenerica.setDataAlteracao(DateUtil.hoje());
            }

        }

        entidade = this.getDAO().save(entidade);

        if (getRegistrarLogAtividade()) {

            if (ObjetoUtil.isNotNull(usuarioLogado)) {

                ((EntidadeGenerica) entidade).setUsuarioLogado(usuarioLogado);
            }

            registrarLogAtividade(entidade, cadastro);
        }

        this.posSalvar(entidade);

        return entidade;
    }

    public void registrarLogAtividade(E entidade, Boolean... cadastro) {

        EntidadeGenerica entidadeGenerica = (EntidadeGenerica) entidade;

        registrarLogAtividade(entidade, ObjetoUtil.isNotNull(cadastro) && cadastro[0] ? LogAtividadeAcao.CADASTRO : LogAtividadeAcao.getLogAtividadeAcao(entidadeGenerica));
    }

    public void registrarLogAtividade(E entidade, LogAtividadeAcao logAtividadeAcao) {

        EntidadeGenerica entidadeGenerica = (EntidadeGenerica) entidade;

        if (ObjetoUtil.isNotNull(entidadeGenerica.getUsuarioLogado())) {

            LogAtividade logAtividade =
                    LogAtividade.logAtividadeBuilder().entidadeTipo(EntidadeTipo.getEntidadeTipo(entidade)).logAtividadeAcao(logAtividadeAcao).usuario(entidadeGenerica.getUsuarioLogado()).entidadeGenerica(entidadeGenerica).build();

            if (ObjetoUtil.isNotNull(logAtividade.getEntidadeTipo()) && ObjetoUtil.isNotNull(logAtividade.getEntidadeId())) {

                logAtividadeService.getDAO().save(logAtividade);
            }
        }
    }

    public void posSalvar(E entidade) {

    }

    @Override
    public void salvar(final List<E> lista) {

        lista.stream().forEach(e -> this.salvar(e));

    }

    @Override
    @Transactional
    public void alterarStatus(final E entidade) {

        if (entidade instanceof EntidadeAutenticada) {

            ((EntidadeAutenticada) entidade).setStatus(StatusEnum.ATIVO.equals(((EntidadeAutenticada) entidade).getStatus()) ? StatusEnum.INATIVO : StatusEnum.ATIVO);

            alterarAtributos(entidade, EntidadeAutenticada_.STATUS);

        } else {

            throw new ServicoException(entidade.getClass().getSimpleName() + " não é uma EntidadeAutenticada");
        }

    }

    @Override
    public void validarInativacao(E entidade) {

    }

    @Override
    public void excluir(E entidade) {

        preRemover(entidade);

        this.getDAO().delete(entidade);
    }

    @Override
    public void excluirPorId(ID id) {

        try {

            this.getDAO().deleteById(id);

        } catch (DataIntegrityViolationException e) {

            throw new ServicoException(MessageSupport.getMessage("MSG_EXCLUSAO"));
        }

    }

    protected <T> List<T> executeQueryAndTransforResult(TypedQuery<Tuple> typedQuery, Class<T> clazz) {

        final List<Tuple> tuples = typedQuery.getResultList();

        return tuples.stream()

                .map(tuple -> TransformerTuple.transformer(tuple, clazz))

                .collect(Collectors.toList());

    }

    public void preSalvar(E entidade) {

        validarRegrasPersistencia(entidade);

        final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

        final Validator validator = factory.getValidator();

        final Set<ConstraintViolation<E>> validate = validator.validate(entidade);

        if (validate.size() > 0) {

            throw new ServicoException(validate.iterator().next().getMessage());

        }

        try {

            if (entidade instanceof EntidadeAutenticada) {
                EntidadeAutenticada entidadeAutenticada = (EntidadeAutenticada) entidade;

                if (entidadeAutenticada.getId() == null) {

                    entidadeAutenticada.setIdUsuarioAutorCadastro(entidadeAutenticada.getUsuarioLogado().getId());

                } else {

                    entidadeAutenticada.setIdUsuarioAutorAlteracao(entidadeAutenticada.getUsuarioLogado().getId());

                }
            }

        } catch (final Exception e) {

        }

    }

    @SuppressWarnings("unchecked")
    protected void validarRegrasPersistencia(E entidade) {

        getRegrasPersistencia(entidade).forEach(regraPersistencia -> {
            CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
            Root<E> root = (Root<E>) criteriaQuery.from(getClassEntityInference());

            if (regraPersistencia.hasSpecification()) {
                List<Predicate> predicates = new ArrayList<>();

                if (Objects.nonNull(entidade.getId())) {
                    predicates.add(criteriaBuilder.notEqual(root.get(getNameIdProperty()), entidade.getId()));
                }

                predicates.add(regraPersistencia.getPredicate(criteriaBuilder, criteriaQuery, root));

                criteriaQuery.where(predicates.toArray(new Predicate[0]));
                criteriaQuery.select(criteriaBuilder.count(root.get(getNameIdProperty())));

                Long qtdeRegistros = getEntityManager().createQuery(criteriaQuery).getSingleResult();

                if (qtdeRegistros > 0) {
                    throw new RuntimeException(MessageSupport.getMessage(regraPersistencia.getKey()));
                }

            } else {
                throw new RuntimeException(MessageSupport.getMessage(regraPersistencia.getKey()));

            }
        });
    }

    protected RegraPersistenciaList getRegrasPersistencia(E entidade) {

        return RegraPersistenciaList.build();
    }

    public void preRemover(E entidade) {

    }

    protected boolean isBlank(final String value) {

        return value == null || value.trim().isEmpty();
    }
    
    public void executeQuery(final DataSource dataSource, final String query) throws Exception {
        
        Connection con = null;
        
        Statement st = null;
        
        try {
            
            con = dataSource.getConnection();
            
            st = con.createStatement();
            
            st.executeUpdate(query);
            
        } catch (final Exception e) {
            
            throw e;
            
        } finally {
            
            if (st != null) {
                
                try {
                    
                    st.close();
                    
                } catch (SQLException e) {
                    
                    e.printStackTrace();
                }
            }
            
            if (con != null) {
                
                try {
                    
                    con.close();
                    
                } catch (SQLException e) {
                    
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    @Transactional
    public void alterarAtributos(E entidade, String... atributos) {

        StringBuilder sqlUpdate = new StringBuilder();

        Table table = entidade.getClass().getAnnotation(Table.class);

        sqlUpdate.append("update ");
        if (!StringUtils.isEmpty(table.schema())) {
            sqlUpdate.append(table.schema()).append(".");
        }
        sqlUpdate.append(table.name()).append(" set ");

        Map<String, Object> valoresPorParametro = new HashMap<>();

        Arrays.stream(atributos).forEach(atributo -> {
            Field field = FieldUtils.getField(entidade.getClass(), atributo, true);

            String nomeColune;
            if (field.isAnnotationPresent(JoinColumn.class)) {
                JoinColumn joinColumn = field.getAnnotation(JoinColumn.class);
                nomeColune = joinColumn.name();

            } else if (field.isAnnotationPresent(Column.class)) {
                Column column = field.getAnnotation(Column.class);
                nomeColune = column.name().length() > 0 ? column.name() : field.getName();

            } else {
                nomeColune = field.getName();

            }

            Object valor = Util.getValorFromField(entidade, field);

            sqlUpdate.append(nomeColune).append(" = ").append(Objects.isNull(valor) ? "null" : ":" + atributo).append(", ");

            if (!Objects.isNull(valor)) {
                if (valor.getClass().isEnum()) {
                    Enumerated enumerated = field.getAnnotation(Enumerated.class);

                    Enum valorEnum = (Enum) valor;
                    valor = enumerated.value() == EnumType.ORDINAL ? valorEnum.ordinal() : valorEnum.name();

                } else if (valor instanceof ObjectID) {
                    valor = ((ObjectID) valor).getId();

                }

                valoresPorParametro.put(atributo, valor);
            }
        });

        sqlUpdate.delete(sqlUpdate.length() - 2, sqlUpdate.length());
        sqlUpdate.append(" where ").append(getNameIdProperty()).append(" = :id");
        valoresPorParametro.put("id", entidade.getId());

        Query query = getEntityManager().createNativeQuery(sqlUpdate.toString());
        valoresPorParametro.forEach(query::setParameter);
        query.executeUpdate();
    }

    public Boolean getRegistrarLogAtividade() {

        return Boolean.FALSE;
    }

}
