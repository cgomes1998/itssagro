package br.com.sjc.fachada.service;

import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.enums.ModuloOperacaoEnum;

import java.util.List;

public interface DadosSincronizacaoService extends Servico<Long, DadosSincronizacao> {

    List<DadosSincronizacao> listarOperacoes();

    DadosSincronizacao obterPorOperacao(String operacao);

    DadosSincronizacao obterPorModuloEOperacao(ModuloOperacaoEnum moduloOperacao, String operacao);

    DadosSincronizacao obterPorEntidade(DadosSincronizacaoEnum entidade);
}
