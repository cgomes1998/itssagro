package br.com.sjc.fachada.rest.paginacao;

import br.com.sjc.util.customPredicates.DateCustonPredicate;
import br.com.sjc.util.customPredicates.ILikeWithTranslate;
import org.hibernate.criterion.MatchMode;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

public enum Operacao {
    IN {
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            return path.in(valor);
        }

        @Override
        public boolean necessitaDeValor() {

            return true;
        }
    },

    NOT_IN {
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            return criteriaBuilder.not(path.in(valor));
        }

        @Override
        public boolean necessitaDeValor() {

            return true;
        }
    },

    EQ {
        @Override
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            return criteriaBuilder.equal(path, valor[0]);
        }

        @Override
        public boolean necessitaDeValor() {

            return true;
        }
    },

    NOT_EQ {
        @Override
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            return criteriaBuilder.notEqual(path, valor[0]);
        }

        @Override
        public boolean necessitaDeValor() {

            return true;
        }
    },

    GE {
        @Override
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            if (valor[0] instanceof Date) {
                return criteriaBuilder.greaterThanOrEqualTo(path, (Date) valor[0]);

            } else if (valor[0] instanceof Integer) {
                return criteriaBuilder.greaterThanOrEqualTo(path, (Integer) valor[0]);

            } else if (valor[0] instanceof Long) {
                return criteriaBuilder.greaterThanOrEqualTo(path, (Long) valor[0]);

            } else if (valor[0] instanceof Float) {
                return criteriaBuilder.greaterThanOrEqualTo(path, (Float) valor[0]);

            } else if (valor[0] instanceof Double) {
                return criteriaBuilder.greaterThanOrEqualTo(path, (Double) valor[0]);

            } else {
                return criteriaBuilder.greaterThanOrEqualTo(path, (BigDecimal) valor[0]);

            }
        }

        @Override
        public boolean necessitaDeValor() {

            return true;
        }
    },

    LE {
        @Override
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            if (valor[0] instanceof Date) {
                return criteriaBuilder.lessThanOrEqualTo(path, (Date) valor[0]);

            } else if (valor[0] instanceof Integer) {
                return criteriaBuilder.lessThanOrEqualTo(path, (Integer) valor[0]);

            } else if (valor[0] instanceof Long) {
                return criteriaBuilder.lessThanOrEqualTo(path, (Long) valor[0]);

            } else if (valor[0] instanceof Float) {
                return criteriaBuilder.lessThanOrEqualTo(path, (Float) valor[0]);

            } else if (valor[0] instanceof Double) {
                return criteriaBuilder.lessThanOrEqualTo(path, (Double) valor[0]);

            } else {
                return criteriaBuilder.lessThanOrEqualTo(path, (BigDecimal) valor[0]);

            }
        }

        @Override
        public boolean necessitaDeValor() {

            return true;
        }
    },

    GT {
        @Override
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            if (valor[0] instanceof Date) {
                return criteriaBuilder.greaterThan(path, (Date) valor[0]);

            } else if (valor[0] instanceof Integer) {
                return criteriaBuilder.greaterThan(path, (Integer) valor[0]);

            } else if (valor[0] instanceof Long) {
                return criteriaBuilder.greaterThan(path, (Long) valor[0]);

            } else if (valor[0] instanceof Float) {
                return criteriaBuilder.greaterThan(path, (Float) valor[0]);

            } else if (valor[0] instanceof Double) {
                return criteriaBuilder.greaterThan(path, (Double) valor[0]);

            } else {
                return criteriaBuilder.greaterThan(path, (BigDecimal) valor[0]);

            }
        }

        @Override
        public boolean necessitaDeValor() {

            return true;
        }
    },

    LT {
        @Override
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            if (valor[0] instanceof Date) {
                return criteriaBuilder.lessThan(path, (Date) valor[0]);

            } else if (valor[0] instanceof Integer) {
                return criteriaBuilder.lessThan(path, (Integer) valor[0]);

            } else if (valor[0] instanceof Long) {
                return criteriaBuilder.lessThan(path, (Long) valor[0]);

            } else if (valor[0] instanceof Float) {
                return criteriaBuilder.lessThan(path, (Float) valor[0]);

            } else if (valor[0] instanceof Double) {
                return criteriaBuilder.lessThan(path, (Double) valor[0]);

            } else {
                return criteriaBuilder.lessThan(path, (BigDecimal) valor[0]);

            }
        }

        @Override
        public boolean necessitaDeValor() {

            return true;
        }
    },

    LIKE {
        @Override
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            return criteriaBuilder.like(path, "%" + valor[0].toString() + "%");
        }

        @Override
        public boolean necessitaDeValor() {

            return true;
        }
    },

    ILIKE {
        @Override
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            return criteriaBuilder.like(criteriaBuilder.lower(path), valor[0].toString().toLowerCase());
        }

        @Override
        public boolean necessitaDeValor() {

            return true;
        }
    },

    ILIKE_ANYWHERE {
        @Override
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            return criteriaBuilder.like(criteriaBuilder.lower(path), "%" + valor[0].toString().toLowerCase() + "%");
        }

        @Override
        public boolean necessitaDeValor() {

            return true;
        }
    },

    ILIKE_ANYWHERE_IGNORE_SPECIAL_CARACTERS {
        @Override
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            return ILikeWithTranslate.build(criteriaBuilder, path, valor[0].toString().toLowerCase(), MatchMode.ANYWHERE);
        }

        @Override
        public boolean necessitaDeValor() {

            return true;
        }
    },

    BETWEEN_STRING_DATES {
        @Override
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            return DateCustonPredicate.build(criteriaBuilder, path, (Date) valor[0], (Date) valor[1]);
        }

        @Override
        public boolean necessitaDeValor() {

            return true;
        }
    },

    IS_EMPTY {
        @Override
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            return criteriaBuilder.isEmpty(path);
        }

        @Override
        public boolean necessitaDeValor() {

            return false;
        }
    },

    IS_NOT_EMPTY {
        @Override
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            return criteriaBuilder.isNotEmpty(path);
        }

        @Override
        public boolean necessitaDeValor() {

            return false;
        }
    },

    IS_NULL {
        @Override
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            return criteriaBuilder.isNull(path);
        }

        @Override
        public boolean necessitaDeValor() {

            return false;
        }
    },

    IS_NOT_NULL {
        @Override
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            return criteriaBuilder.isNotNull(path);
        }

        @Override
        public boolean necessitaDeValor() {

            return false;
        }
    },

    IS_NOT_EMPTY_NULL {
        @Override
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            return criteriaBuilder.and(criteriaBuilder.isNotNull(path), criteriaBuilder.notEqual(path, ""));
        }

        @Override
        public boolean necessitaDeValor() {

            return false;
        }
    },

    BETWEEN {
        @Override
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            if (valor[0] instanceof Date) {
                return criteriaBuilder.between(path, (Date) valor[0], (Date) valor[1]);

            } else if (valor[0] instanceof LocalDateTime) {
                return criteriaBuilder.between(path, (LocalDateTime) valor[0], (LocalDateTime) valor[1]);

            } else if (valor[0] instanceof Integer) {
                return criteriaBuilder.between(path, (Integer) valor[0], (Integer) valor[1]);

            } else if (valor[0] instanceof Long) {
                return criteriaBuilder.between(path, (Long) valor[0], (Long) valor[1]);

            } else if (valor[0] instanceof Float) {
                return criteriaBuilder.between(path, (Float) valor[0], (Float) valor[1]);

            } else if (valor[0] instanceof Double) {
                return criteriaBuilder.between(path, (Double) valor[0], (Double) valor[1]);

            } else {
                return criteriaBuilder.between(path, (BigDecimal) valor[0], (BigDecimal) valor[1]);

            }
        }

        @Override
        public boolean necessitaDeValor() {

            return true;
        }
    },

    BUSCA_RAPIDA {
        @Override
        public Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor) {

            return null;
        }

        @Override
        public boolean necessitaDeValor() {

            return true;
        }
    };

    public abstract Predicate restricao(CriteriaBuilder criteriaBuilder, Path path, Object[] valor);

    public abstract boolean necessitaDeValor();
}
