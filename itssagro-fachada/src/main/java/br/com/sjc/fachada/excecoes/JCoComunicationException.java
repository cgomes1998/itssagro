package br.com.sjc.fachada.excecoes;

import br.com.sjc.util.bundle.MessageSupport;

public class JCoComunicationException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public JCoComunicationException() {
        super(MessageSupport.getMessage("MSGE006"));
    }

    public JCoComunicationException(String mensagem) {
        super(mensagem);
    }
}
