package br.com.sjc.fachada.service;

import br.com.sjc.modelo.PortariaTicket;
import br.com.sjc.modelo.response.FileResponse;

public interface PortariaTicketService extends Servico<Long, PortariaTicket> {
    PortariaTicket informarSaida(Long portariaTicket);

    PortariaTicket informarEntrada(Long portariaTicket);

    void imprimirTicketMarcacao(Long id);

    void imprimirTicketEntrada(Long id);

    void imprimirTicketSaida(Long id);

    FileResponse obterRelatorioPortariaTicket(PortariaTicket filtro);
}
