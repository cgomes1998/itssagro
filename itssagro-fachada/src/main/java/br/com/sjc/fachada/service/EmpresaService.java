package br.com.sjc.fachada.service;

import br.com.sjc.modelo.cfg.Empresa;

public interface EmpresaService extends Servico<Long, Empresa> {

}
