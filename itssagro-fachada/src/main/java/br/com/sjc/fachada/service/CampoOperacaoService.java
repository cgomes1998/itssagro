package br.com.sjc.fachada.service;

import br.com.sjc.modelo.cfg.CampoOperacao;
import br.com.sjc.modelo.dto.CampoOperacaoDTO;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;

import java.util.List;

public interface CampoOperacaoService extends Servico<Long, CampoOperacao> {

    List<CampoOperacaoDTO> obterCamposPorOperacao ( DadosSincronizacaoEnum dadosSincronizacaoEnum );
}
