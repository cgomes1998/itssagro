package br.com.sjc.fachada.rest;

import br.com.sjc.modelo.Arquivo;
import br.com.sjc.modelo.Usuario;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.Base64;
import java.util.ResourceBundle;

public class BaseEndpoint implements Serializable {

    public static final String TOKEN_USER = "TOKEN.USER";

    public static final String TOKEN_USER_AUTHORIZATION = "TOKEN.USER.AUTHORIZATION";

    public static final String AUTHORIZATION_PROPERTY = "Authorization";

    public static final String RESPONSE_MENSAGEM = "mensagem";

    public static final String ENTIDADE = "entidade";

    public static final String ERRO = "erro";

    private static final long serialVersionUID = 1L;

    @Context
    protected HttpServletRequest request;

    public Usuario getUsuarioToken() {

        return (Usuario) this.request.getAttribute(TOKEN_USER);
    }

	public HttpServletRequest getRequest() {

        return this.request;
    }

    protected String getMensagem(String mensagem) {

        try {

            return getBundle().getString(mensagem);

        } catch (Exception e) {

            return mensagem;
        }
    }

    protected ResourceBundle getBundle() {

        return ResourceBundle.getBundle("message");
    }

    public Response downloadArquiboBase64(String nome, String mimeType,
                                          ByteArrayOutputStream byteArrayOutputStream) {

        return downloadArquiboBase64(
                nome,
                mimeType,
                byteArrayOutputStream.toByteArray()
        );
    }

    public Response downloadArquiboBase64(String nome, String mimeType, byte[] bytes) {

        Arquivo arquivo = new Arquivo();
        arquivo.setNome(nome);
        arquivo.setMimeType(mimeType);
        arquivo.setBase64(Base64.getEncoder().encodeToString(bytes));

        return Response.ok(arquivo).build();
    }

}
