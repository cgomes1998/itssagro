package br.com.sjc.fachada.rest.paginacao;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Sort.Direction;

import java.io.Serializable;

@Getter
@Setter
public class Ordenacao implements Serializable {

    private static final long serialVersionUID = 1L;

    private String campo;

    private Direction ordenacao;

    public Ordenacao() {

    }

    @Builder(builderMethodName = "ordenacaoBuilder")
    public Ordenacao(String campo, Direction ordenacao) {

        this.campo = campo;

        this.ordenacao = ordenacao;
    }

}
