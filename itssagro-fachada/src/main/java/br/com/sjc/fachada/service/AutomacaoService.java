package br.com.sjc.fachada.service;

import br.com.sjc.modelo.dto.NrCartaoStatusDTO;
import br.com.sjc.modelo.dto.PreCadastroVeiculoDTO;
import br.com.sjc.modelo.dto.VeiculoBalancaDTO;

public interface AutomacaoService {

    VeiculoBalancaDTO obterPesoBalanca(int bal);

    NrCartaoStatusDTO alterarStatusPesagem(int tag, int status, String msg1, String msg2, String msg3, int peso_ordem, String obs);

    PreCadastroVeiculoDTO realizarPreCadastroVeiculo(int tag, String placa_cavalo, String placa_carreta, String transportador, String motorista, String razaosocial, String endereco, String municipio, String produto, String data_ordem, String nr_ordem);

}
