package br.com.sjc.fachada.service;

import br.com.sjc.modelo.dto.NfeDepositoDTO;
import br.com.sjc.modelo.dto.RetornoNfeDTO;
import br.com.sjc.modelo.dto.RomaneioDepositoDTO;
import br.com.sjc.modelo.sap.RetornoNFe;

import java.util.List;
import java.util.Set;

public interface RetornoNFeService extends Servico<Long, RetornoNFe> {
    
    void salvarListaRetornoNfe(List<RetornoNFe> retornoNFes);
    
    List<RetornoNFe> obterEntidadeProjetantoApenasId(Set<String> notas);

    List<RetornoNFe> listarPorNfeTransporte(Set<String> nfesTranporte);

    RetornoNFe obterNotaPorNumero(String numero);

    boolean notaEstaSendoUtilizada(String numeroNota, Long idRomaneio);

    List<RetornoNFe> listarNfePorRomaneio ( Long idRomaneio );

    List<NfeDepositoDTO> obterDadosDeNotaParaRelatorioDeDeposito(List<RomaneioDepositoDTO> romaneioDepositoDTOS);

	List<RetornoNfeDTO> obterRetornoNFePorRomaneios(List<String> numRomaneios);

}
