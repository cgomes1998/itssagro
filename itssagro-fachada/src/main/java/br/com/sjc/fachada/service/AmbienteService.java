package br.com.sjc.fachada.service;

import br.com.sjc.modelo.cfg.Ambiente;

public interface AmbienteService extends Servico<Long, Ambiente> {
    Ambiente obterPorHostEMandante(String host, String mandante);

    Ambiente obterAmbienteLogado();
}
