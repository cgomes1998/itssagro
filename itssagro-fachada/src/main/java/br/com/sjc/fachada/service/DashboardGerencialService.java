package br.com.sjc.fachada.service;

import br.com.sjc.modelo.dto.DashboardGerencialDTO;
import br.com.sjc.modelo.dto.DashboardGerencialFiltroDTO;
import br.com.sjc.modelo.dto.DashboardGerencialFiltroEtapaDTO;

import java.util.List;

public interface DashboardGerencialService {

    List<DashboardGerencialDTO> obterFilas(DashboardGerencialFiltroDTO filtro);

    DashboardGerencialDTO obterFilas(DashboardGerencialFiltroEtapaDTO filtroEtapa);

    void atualizarDashboard();

}
