package br.com.sjc.fachada.servico.impl;

import br.com.sjc.fachada.service.*;

public class ServiceInject {

    public static MotoristaService motoristaService;

    public static VeiculoService veiculoService;

    public static AmbienteSincronizacaoService ambienteSincronizacaoService;

    public static RomaneioRequestKeyService romaneioRequestKeyService;

    public static RomaneioService romaneioService;

    public static PesagemService pesagemService;

    public static DadosSincronizacaoService dadosSincronizacaoService;

    public static AmbienteService ambienteService;

    public static ParametroService parametroService;

    public static UsuarioService usuarioService;

    public static LaudoService laudoService;

    public static TabelaValorService tabelaValorService;

    public static SAPService sapService;
}
