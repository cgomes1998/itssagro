package br.com.sjc.fachada.rest.paginacao;

import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.GenericPropertyMatcher;

public enum OperacaoEnum {

    EQ {
        @Override
        public GenericPropertyMatcher obterOperacao() {
        	
            return ExampleMatcher.GenericPropertyMatchers.exact();
        }
    },

    LIKE {
        @Override
        public GenericPropertyMatcher obterOperacao() {

            return ExampleMatcher.GenericPropertyMatchers.contains();
            
        }
    },

    ILIKE {
        @Override
        public GenericPropertyMatcher obterOperacao() {

        	return ExampleMatcher.GenericPropertyMatchers.startsWith();
        }
    };

	public abstract GenericPropertyMatcher obterOperacao();
}
