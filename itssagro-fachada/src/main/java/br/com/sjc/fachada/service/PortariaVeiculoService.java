package br.com.sjc.fachada.service;

import br.com.sjc.modelo.cfg.PortariaVeiculo;
import br.com.sjc.modelo.dto.PortariaMotivoDTO;
import br.com.sjc.modelo.dto.PortariaVeiculoDTO;

import java.util.List;

public interface PortariaVeiculoService extends Servico<Long, PortariaVeiculo> {
    List<PortariaVeiculoDTO> listarVeiculosAtivos();
}
