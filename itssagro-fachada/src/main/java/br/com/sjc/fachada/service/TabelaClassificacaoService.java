package br.com.sjc.fachada.service;

import br.com.sjc.modelo.dto.TabelaClassificacaoDTO;
import br.com.sjc.modelo.dto.TabelaClassificacaoItemDTO;
import br.com.sjc.modelo.sap.TabelaClassificacao;
import br.com.sjc.modelo.sap.request.EventOutRequest;

import java.util.Date;
import java.util.List;

public interface TabelaClassificacaoService extends Servico<Long, TabelaClassificacao> {

    void adicionarItensParaCalculoDeIndice(TabelaClassificacao entidade);

    void atualizarIndices(List<TabelaClassificacaoItemDTO> itens);

    void atualizarCodigoDoLeitor(TabelaClassificacao entidade);

    void atualizarCodigoArmazemDeTerceiros(TabelaClassificacao itens);

    List<String> listarTabelaOrdernandoPorItem();

    void salvarSAP(EventOutRequest request, String host, String mandante);

    List<TabelaClassificacao> listarSincronizados(final Date data);

    void sincronizar(List<TabelaClassificacao> entidades);

    TabelaClassificacaoDTO listByItemClassificacao(String itemClassificacao, String codigoMaterial);

    List<TabelaClassificacao> getLimiteTodosIndices();
}
