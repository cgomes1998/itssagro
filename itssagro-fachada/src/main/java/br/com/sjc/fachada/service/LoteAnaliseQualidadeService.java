package br.com.sjc.fachada.service;

import br.com.sjc.modelo.LoteAnaliseQualidade;
import br.com.sjc.modelo.dto.LoteAnaliseQualidadeMotivoInativacaoDTO;
import br.com.sjc.modelo.response.FileResponse;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;

public interface LoteAnaliseQualidadeService extends Servico<Long, LoteAnaliseQualidade> {

    @Transactional
    void alterarStatusLoteAnaliseQualidade(LoteAnaliseQualidade loteAnaliseQualidade);

    LoteAnaliseQualidadeMotivoInativacaoDTO obterMotivoInativacaoDoLote(LoteAnaliseQualidade loteAnaliseQualidade);

    LoteAnaliseQualidade obterLote(LoteAnaliseQualidade entidadeFiltro);

    FileResponse obterRelatorioLotes(LoteAnaliseQualidade entidadeFiltro);

}
