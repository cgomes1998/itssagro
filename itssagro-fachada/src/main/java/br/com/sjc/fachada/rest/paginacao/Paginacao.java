package br.com.sjc.fachada.rest.paginacao;

import br.com.sjc.modelo.Usuario;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
public class Paginacao<E> implements Pageable {

    private int pageSize;

    private int pageNumber;

    private long offset;

    private E entidade;

    private Sort sort;

    private Ordenacao ordenacao;

    private boolean listarTodos = false;

    private List<String> groupBy = new ArrayList<>();

    private List<Filtro> filtros = new ArrayList<>();

    private Specification<E> specification;

    private Usuario usuarioLogado;

    public Paginacao() {

    }

    @Override
    public Sort getSort() {

        if (Objects.nonNull(sort)) {
            return sort;

        } else if (Objects.isNull(ordenacao) || (Objects.isNull(ordenacao.getCampo()) && Objects.isNull(ordenacao.getOrdenacao()))) {
            return Sort.by(Direction.DESC, "dataCadastro");

        }

        return Sort.by(ordenacao.getOrdenacao(), ordenacao.getCampo());
    }

    @Override
    public Pageable next() {

        return null;
    }

    @Override
    public Pageable previousOrFirst() {

        return null;
    }

    @Override
    public Pageable first() {

        return null;
    }

    @Override
    public boolean hasPrevious() {

        return false;
    }

    public boolean possuiFiltros() {

        return Objects.nonNull(filtros) && !filtros.isEmpty();
    }

}
