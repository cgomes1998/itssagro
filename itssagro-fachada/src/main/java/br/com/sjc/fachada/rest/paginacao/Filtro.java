package br.com.sjc.fachada.rest.paginacao;

import br.com.sjc.util.JoinCriteria;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Filtro {

    private String campo;

    private Operacao operacao;

    private String[] valor;

    private TipoFiltro tipo;

    private List<JoinCriteria> joins = new ArrayList<>();

    private List<Filtro> filtrosOr = new ArrayList<>();

    private List<Filtro> filtrosAnd = new ArrayList<>();

    public boolean hasFiltrosOr() {

        return !filtrosOr.isEmpty();
    }

    public boolean hasFiltrosAnd() {

        return !filtrosAnd.isEmpty();
    }

    public boolean hasValor() {

        return !Objects.isNull(valor) && Arrays.stream(valor).anyMatch(valorTemp -> valorTemp != null && !valorTemp.isEmpty());
    }

    public boolean hasJoins() {

        return !joins.isEmpty();
    }

    public Object[] getValorTransformado() {

        Object[] valor = new Object[this.valor.length];

        if (operacao.necessitaDeValor()) {
            int indice = 0;

            for (final String string : this.valor) {
                valor[indice++] = tipo.converter(string);
            }
        }

        return valor;
    }

    public static Filtro build(TipoFiltro tipo, Operacao operacao, String campo, String[] valor) {

        Filtro filtro = new Filtro();
        filtro.setTipo(tipo);
        filtro.setOperacao(operacao);
        filtro.setCampo(campo);
        filtro.setValor(valor);

        return filtro;
    }

}
