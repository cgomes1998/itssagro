package br.com.sjc.fachada.rest.paginacao;

import java.util.Collection;

public class Pagina<E> {

	public Object contentSummary;

	private Collection<E> content;

    private long totalElements;
    
    public Pagina() {

    }
    
	public Pagina(Collection<E> content, long totalElements) {
		this.content = content;
		this.totalElements = totalElements;
	}

	public Object getContentSummary() {

		return contentSummary;
	}

	public void setContentSummary(Object contentSummary) {

		this.contentSummary = contentSummary;
	}

	public Collection<E> getContent() {
		return content;
	}

	public void setContent(Collection<E> content) {
		this.content = content;
	}

	public long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(long totalElements) {
		this.totalElements = totalElements;
	}

}
