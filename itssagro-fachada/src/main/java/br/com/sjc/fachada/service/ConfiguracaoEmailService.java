package br.com.sjc.fachada.service;

import br.com.sjc.modelo.ConfiguracaoEmail;

/**
 * Created by julio.bueno on 23/07/2019.
 */
public interface ConfiguracaoEmailService extends Servico<Long, ConfiguracaoEmail> {

    ConfiguracaoEmail get();

}
