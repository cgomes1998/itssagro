package br.com.sjc.fachada.service;

import br.com.sjc.modelo.Documento;

/**
 * Created by julio.bueno on 03/07/2019.
 */
public interface DocumentoService extends Servico<Long, Documento> {

    Documento obterAssinatura();

}
