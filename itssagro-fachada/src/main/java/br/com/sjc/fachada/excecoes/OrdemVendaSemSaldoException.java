package br.com.sjc.fachada.excecoes;

public class OrdemVendaSemSaldoException extends Exception {

    private static final long serialVersionUID = 1L;

    public OrdemVendaSemSaldoException() {

        super("Ordem de venda não tem saldo suficiente para romaneio ser integrado ao SAP");
    }

}
