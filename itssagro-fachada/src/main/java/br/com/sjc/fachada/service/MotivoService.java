package br.com.sjc.fachada.service;

import br.com.sjc.modelo.Motivo;
import br.com.sjc.modelo.enums.MotivoTipoEnum;

import java.util.List;

/**
 * Created by julio.bueno on 24/06/2019.
 */
public interface MotivoService extends Servico<Long, Motivo> {
    List<Motivo> getMotivosPorTipo(MotivoTipoEnum motivoTipoEnum);
}
