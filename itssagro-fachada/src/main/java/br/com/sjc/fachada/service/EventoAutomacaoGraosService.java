package br.com.sjc.fachada.service;

import br.com.sjc.modelo.EventoAutomacaoGraos;
import br.com.sjc.modelo.enums.CriterioTipoEnum;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.sap.Romaneio;

import java.util.Optional;

public interface EventoAutomacaoGraosService extends Servico<Long, EventoAutomacaoGraos> {

	Optional<EventoAutomacaoGraos> obterEventoASerExecutado(
		   Romaneio romaneio, EtapaEnum etapaAtual, CriterioTipoEnum criterioDaEtapaAtual
	);

}
