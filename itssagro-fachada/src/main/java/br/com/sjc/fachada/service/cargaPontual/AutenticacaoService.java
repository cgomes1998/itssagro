package br.com.sjc.fachada.service.cargaPontual;

import br.com.sjc.modelo.pojo.cargaPontual.autenticacao.Autenticacao;
import br.com.sjc.modelo.pojo.cargaPontual.autenticacao.RetornoGerarToken;
import br.com.sjc.modelo.pojo.cargaPontual.autenticacao.RetornoValidadeToken;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.ws.rs.core.MediaType;

@FeignClient(url = "${cargaPontual.servico.endpoint}", name = "autenticacaoService")
public interface AutenticacaoService {

    @RequestMapping(method = RequestMethod.POST, value = "autenticacao/token/", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    RetornoGerarToken gerarToken(Autenticacao usuario);

    @RequestMapping(method = RequestMethod.GET, value = "autenticacao/token/validade/{token}", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    RetornoValidadeToken consultarValidadeToken(@PathVariable("token") String token);

}
