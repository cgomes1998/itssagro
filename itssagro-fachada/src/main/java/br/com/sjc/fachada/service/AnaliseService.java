package br.com.sjc.fachada.service;

import br.com.sjc.modelo.Analise;
import br.com.sjc.modelo.dto.AnaliseFiltroDTO;

import java.util.List;

public interface AnaliseService extends Servico<Long, Analise> {

    Analise obterAnalisePorMaterial ( AnaliseFiltroDTO filtroDTO );

    List<Analise> listarPorMaterial ( Long id );

    void validar ( Analise entidade ) throws Exception;
}
