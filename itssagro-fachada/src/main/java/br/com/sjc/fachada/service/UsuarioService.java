package br.com.sjc.fachada.service;

import br.com.sjc.modelo.Usuario;
import br.com.sjc.modelo.dto.aprovacaoDocumento.UsuarioAprovacaoDocumento;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;

public interface UsuarioService extends Servico<Long, Usuario> {

    Collection<UsuarioAprovacaoDocumento> listarUsuariosComPermissaoParaAprovarIndices();

    Usuario autenticarUsuario (
            final String login,
            final String senha,
            final boolean senhaJaCriptografa
    );

    String obterLoginPorId ( Long id );

    Usuario obterPorLogin ( String login );

    boolean existeUsuarioComLoginESenha (
            String login,
            String senha
    );

    void atualizarDataUltimoAcesso (
            Long id,
            Date data
    );

    @Transactional(readOnly = true)
    String obterNomePorId( Long id );
}
