package br.com.sjc.fachada.service;

import br.com.sjc.modelo.sap.Iva;

import java.util.List;

public interface IvaService extends Servico<Long, Iva> {

    List<Iva> listarIvasOrdernandoPorCodigoAcrescente();

    void sincronizar();

}
