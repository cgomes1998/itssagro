package br.com.sjc.fachada.service;

import br.com.sjc.modelo.Laudo;
import br.com.sjc.modelo.dto.AnaliseFiltroDTO;

public interface LaudoService extends Servico<Long, Laudo> {

    Laudo obterCertificadoPorMaterialETanque(AnaliseFiltroDTO filtroDTO);

    Laudo obterCertificadoPorMaterial(Long idMaterial);

    String obterNumeroDoCertificadoPorMaterial(Long idMaterial, Long tanque);

}
