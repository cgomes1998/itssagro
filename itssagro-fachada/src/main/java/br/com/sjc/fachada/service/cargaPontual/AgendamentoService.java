package br.com.sjc.fachada.service.cargaPontual;

import br.com.sjc.modelo.pojo.cargaPontual.agendamento.trocaStatus.Body;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.ws.rs.core.MediaType;

@FeignClient(url = "${cargaPontual.servico.endpoint}", name = "senhaIntegracaoService")
public interface AgendamentoService {

    @RequestMapping(method = RequestMethod.POST, value = "agendamento/trocastatus/", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    br.com.sjc.modelo.pojo.cargaPontual.agendamento.trocaStatus.Retorno trocaStatus(@RequestHeader("tokenCombinado") String tokenCombinado, Body body);

    @RequestMapping(method = RequestMethod.GET, value = "agendamento/{filtros}", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento.Retorno consultarAgendamento(@RequestHeader("tokenCombinado") String tokenCombinado, @PathVariable("filtros") String filtros);

}
