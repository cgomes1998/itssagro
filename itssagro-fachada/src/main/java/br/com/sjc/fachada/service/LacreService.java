package br.com.sjc.fachada.service;

import br.com.sjc.fachada.rest.paginacao.Pagina;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.modelo.Lacre;
import br.com.sjc.modelo.Lote;
import br.com.sjc.modelo.dto.LacreCountDTO;
import br.com.sjc.modelo.dto.RelatorioLacreDTO;
import br.com.sjc.modelo.dto.RelatorioLacreFiltroDTO;
import br.com.sjc.modelo.enums.StatusLacreEnum;
import br.com.sjc.modelo.view.VwLacreDetalhe;

import java.util.List;

public interface LacreService extends Servico<Long, Lacre> {

    void inutilizarLacres(List<Lacre> lacres);

    void alterarStatusDosLacres(List<Lacre> lacres);

    List<Lacre> buscarLacresAutoComplete(String value);

    Pagina<Lacre> listarLacres(Paginacao<Lacre> paginacao);

    List<Lacre> listarLacres(Long loteId);

    void alterarTodosStatusLacre(Lote lote);

    List<VwLacreDetalhe> obterLacreVinculadoARomaneio(List<Lacre> lacres);

    List<LacreCountDTO> obterTotalDeLacresPorStatus(StatusLacreEnum statusLacre);

    List<RelatorioLacreDTO> obterRelatorioDeLacresVinculadosARomaneio(RelatorioLacreFiltroDTO relatorioLacreFiltro);
}
