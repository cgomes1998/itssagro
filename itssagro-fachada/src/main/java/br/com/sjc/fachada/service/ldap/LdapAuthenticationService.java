package br.com.sjc.fachada.service.ldap;

import br.com.sjc.fachada.service.ParametroService;
import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.enums.TipoParametroEnum;
import br.com.sjc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

/**
 * Classe que realiza a conexão e autenticação ao LDAP
 *
 */
public class LdapAuthenticationService {
    /*
     * Singleton
     */
    protected static LdapAuthenticationService instanceLdap;

    /*
     * Implementação do Initial context para LDAP
     */
    public static String INITIAL_CTX = "com.sun.jndi.ldap.LdapCtxFactory";

    /*
     * Servidor LDAP
     */
    public static String SERVIDOR_LDAP; //Defina aqui o ip onde está o ldap. o que vem após os ":" é a porta do ldap

    /*
     * Dominio LDAP
     */
    public static String DOMINIO_LDAP;

    /*
     * Diretório Base LDAP
     */
    public static String BASE_DN_LDAP;

    /*
     * Tipo de conexão realizada
     */
    public static String CONNECTION_TYPE = "simple";

    /*
     * Mensagem de Erro de Conexão ao Ldap
     */
    public static String MSG_ERROR_LDAP_CONNECTION = "Não foi possível obter um contexto LDAP";

    /*
     * Mensagem de Erro sobre Validação do Login e Password
     */
    public static String MSG_ERROR_LDAP_VALIDATION_USER = "Username ou Password Inválida";

    private LdapAuthenticationService() {
        super();
    }

    /**
     * Obtém a mesma instância de LdapAuthentication para todas as chamadas
     *
     * @return um objeto LdapAuthentication
     */
    public static LdapAuthenticationService getInstance() {

        if (instanceLdap == null) {

            instanceLdap = new LdapAuthenticationService();
        }

        return instanceLdap;
    }

    /**
     * Método responsável por realizar a chamada para autenticação via ldap do
     * login e password passados como parâmetros.
     *
     */
    public boolean authentication(String login, String password) {

        DirContext ctx = null;
        SearchControls sc = null;
        String filtro = null;
        NamingEnumeration cursor = null;
        boolean bResult = false;

        /*
         * Cria conexão padrão com LDAP
         */
        ctx = createLdapConnection(login, password);

        if (ctx != null) {
            return true;
        }

        return bResult;
    }

    /**
     * Método responsável por realizar a conexão padrão com o Ldap.
     *
     */
    @SuppressWarnings("unchecked")
    private DirContext createLdapConnection(String username, String password) {

        DirContext ctx = null;
        Hashtable env = new Hashtable();

        // Especifica INITIAL CONTEXT
        env.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CTX);

        // Especifica o IP/Nome e a porta do servidor LDAP
        env.put(Context.PROVIDER_URL, SERVIDOR_LDAP);

        // Usuário ADMIN
        env.put(Context.SECURITY_PRINCIPAL, String.format("%s@%s", username, DOMINIO_LDAP));

        // Senha ADMIN
        env.put(Context.SECURITY_CREDENTIALS, password);

        // Tipo de Conexão
        env.put(Context.SECURITY_AUTHENTICATION, CONNECTION_TYPE);

        try {
            // Cria um Initial Context
            ctx = new InitialDirContext(env);

        } catch (NamingException e) {
            System.out.println(MSG_ERROR_LDAP_CONNECTION);
            e.printStackTrace();
        }

        return ctx;
    }

    /**
     * Método responsável por realizar a validação do login no Ldap. O campo dn
     * é distinguished name formado anteriormente a partir da consulta do login
     * no Ldap.
     */
    @SuppressWarnings("unchecked")
    private boolean validateUser(String dn, String senha) {

        DirContext ldapCtx = null;
        boolean bResult = false;

        Hashtable env = new Hashtable();

        // Especifica INITIAL CONTEXT
        env.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CTX);

        // Especifica o IP/Nome e a porta do servidor LDAP
        env.put(Context.PROVIDER_URL, SERVIDOR_LDAP);

        // Ldap Distingued Name
        env.put(Context.SECURITY_PRINCIPAL, dn);

        // Senha Usuário
        env.put(Context.SECURITY_CREDENTIALS, senha);

        // Tipo de Conexão
        env.put(Context.SECURITY_AUTHENTICATION, CONNECTION_TYPE);

        try {
            // Cria um Initial Context
            ldapCtx = new InitialDirContext(env);


        } catch (AuthenticationException auEx) {
            System.out.println(MSG_ERROR_LDAP_VALIDATION_USER);
            auEx.printStackTrace();
        } catch (NamingException ne) {
            System.out.println(MSG_ERROR_LDAP_CONNECTION);
            ne.printStackTrace();
        } finally {

            if (ldapCtx != null) {
                bResult = true;
            }
        }

        return bResult;
    }
}
