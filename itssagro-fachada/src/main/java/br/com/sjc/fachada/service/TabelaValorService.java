package br.com.sjc.fachada.service;

import br.com.sjc.modelo.Arquivo;
import br.com.sjc.modelo.TabelaValor;
import br.com.sjc.modelo.TabelaValorItem;
import br.com.sjc.modelo.sap.ConversaoLitragem;
import br.com.sjc.modelo.sap.ConversaoLitragemItem;

import javax.transaction.Transactional;
import java.util.List;

public interface TabelaValorService extends Servico<Long, TabelaValor> {

    TabelaValor obterPorMaterial(Long material);

    @Transactional
    TabelaValorItem obterItensParaConversao (
            ConversaoLitragem item
    );

    List<TabelaValorItem> importarArquivo( Arquivo arquivo);

}
