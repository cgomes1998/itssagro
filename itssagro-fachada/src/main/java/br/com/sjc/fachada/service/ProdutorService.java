package br.com.sjc.fachada.service;

import br.com.sjc.modelo.endpoint.request.item.FornecedorRequest;
import br.com.sjc.modelo.endpoint.response.item.FornecedorResponse;
import br.com.sjc.modelo.sap.Produtor;
import br.com.sjc.modelo.sap.response.item.RfcProdutorResponseItem;

import java.util.Date;
import java.util.List;

public interface ProdutorService extends Servico<Long, Produtor> {

    void salvarSAP(List<RfcProdutorResponseItem> data);

    List<Produtor> listarSincronizados(Date data);

    void sincronizar(List<Produtor> data);

    List<Produtor> buscarProdutoresAutoComplete(String value);

    List<Produtor> buscarArmazemTerceirosAutoComplete(String value);

    List<FornecedorResponse> listarParaArmazemDeTerceiros(List<FornecedorRequest> requests);

    Produtor obterPorCodigo(String codigo);

    String gerarSenha(Produtor produtor) throws Exception;

    Produtor autenticarFornecedor(String login, String senha);
}
