package br.com.sjc.fachada.excecoes;

import br.com.sjc.util.bundle.MessageSupport;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ItensClassificacaoException extends Exception {

    private static final long serialVersionUID = 1L;

    private List<String> indices;

    public ItensClassificacaoException(final List<String> indices) {
        super(ItensClassificacaoException.getMessage(indices));
        this.indices = indices;
    }

    public static String getMessage(final List<String> itens) {

        if (itens.size() > 1) {

            String itensConcat = itens.stream().map(Objects::toString).collect(Collectors.joining(", "));

            return MessageFormat.format(MessageSupport.getMessage("MSGE007"), itensConcat);
        }

        return MessageFormat.format(MessageSupport.getMessage("MSGE008"), itens.get(0));
    }

    public List<String> getIndices() {
        if(indices == null){
            indices = new ArrayList<>();
        }

        return indices;
    }
}
