package br.com.sjc.fachada.service;

import br.com.sjc.modelo.LogRomaneio;
import br.com.sjc.modelo.Usuario;
import br.com.sjc.modelo.dto.LogRomaneioDTO;
import br.com.sjc.modelo.sap.Romaneio;

import java.util.List;

public interface LogRomaneioService extends Servico<Long, LogRomaneio> {

    void salvar(Romaneio romaneio, Usuario usuarioToken);

    List<LogRomaneioDTO> obterLogPorRomaneio(Long idRomaneio);
}
