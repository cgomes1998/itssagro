package br.com.sjc.fachada.service;

import br.com.sjc.modelo.Senha;
import br.com.sjc.modelo.SenhaEtapa;
import br.com.sjc.modelo.dto.FluxoEtapaDTO;
import br.com.sjc.modelo.dto.SenhaEtapaDTO;
import br.com.sjc.modelo.dto.SenhaEtapaRelatorioDTO;

import java.util.Date;
import java.util.List;

public interface SenhaEtapaService extends Servico<Long, SenhaEtapa> {
    List<SenhaEtapaRelatorioDTO> obterSenhasEtapasPorFluxoEtapas(Date data, Date dataFim, List<FluxoEtapaDTO> fluxoEtapas);
}