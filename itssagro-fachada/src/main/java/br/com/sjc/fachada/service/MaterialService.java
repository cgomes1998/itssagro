package br.com.sjc.fachada.service;

import br.com.sjc.fachada.rest.paginacao.Pagina;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.modelo.dto.MaterialDTO;
import br.com.sjc.modelo.sap.ConfiguracaoUnidadeMedidaMaterial;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.modelo.sap.request.EventOutRequest;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface MaterialService extends Servico<Long, Material> {

    @Transactional(readOnly = true)
    List<ConfiguracaoUnidadeMedidaMaterial> obterConfiguracoesUnidadesMedidas(String codigoMaterial);

    void salvarSAP(EventOutRequest request, String host, String mandante);

    List<Material> listarSincronizados(final Date data);

    void sincronizar(List<Material> data);

    List<MaterialDTO> listarMateriaisAtivos();

    Material obterPorCodigo(String codigo);

    Pagina<?> listarComPaginacao(Paginacao<MaterialDTO> paginacao);
}
