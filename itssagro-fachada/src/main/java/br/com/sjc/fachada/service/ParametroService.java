package br.com.sjc.fachada.service;

import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.enums.TipoParametroEnum;

import java.util.List;

public interface ParametroService extends Servico<Long, Parametro> {

    Parametro obterPorTipoParametro(TipoParametroEnum tipoParametroEnum);

    List<Parametro> obterPorTipoParametro(List<TipoParametroEnum> tipoParametros);
}
