package br.com.sjc.fachada.service;

import br.com.sjc.modelo.Classificacao;
import br.com.sjc.modelo.sap.ItensClassificacao;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.TabelaClassificacao;
import net.sf.jasperreports.engine.JRException;

import java.util.List;

public interface ClassificacaoService extends Servico<Long, Classificacao> {

    Classificacao salvarClassificacaoDeArmazemTerceiros(Classificacao entidade);

    byte[] imprimirPdf(Romaneio romaneioSaved, Long idRomaneio) throws JRException;

    List<TabelaClassificacao> listarItensClassificacaoPorMaterial(String codigoMaterial);

    boolean indiceInformadoExisteNaTabelaDeClassificacao(ItensClassificacao itensClassificacao);
}
