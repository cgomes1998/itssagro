package br.com.sjc.fachada.service;

import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.sap.ConversaoLitragem;
import br.com.sjc.modelo.sap.Romaneio;

import java.math.BigDecimal;

public interface ConversaoLitragemService extends Servico<Long, ConversaoLitragem> {

    ConversaoLitragem salvar(Romaneio entidade, Parametro parametro);

    BigDecimal obterDiferencaLitros(Romaneio romaneio);

}
