package br.com.sjc.fachada.service;

import br.com.sjc.modelo.Hardware;

import java.util.Arrays;
import java.util.List;

/**
 * Created by julio.bueno on 02/07/2019.
 */
public interface HardwareService extends Servico<Long, Hardware> {
    List<Hardware> listarHardwaresAtivos();
}
