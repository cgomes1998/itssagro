package br.com.sjc.fachada.dto;

public class SelectItemDTO<E> {

    private String label;

    private E value;

    public SelectItemDTO(String label, E value) {

        this.label = label;
        this.value = value;
    }

    public String getLabel() {

        return label;
    }

    public void setLabel(String label) {

        this.label = label;
    }

    public E getValue() {

        return value;
    }

    public void setValue(E value) {

        this.value = value;
    }

}
