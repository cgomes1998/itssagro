package br.com.sjc.fachada.servico.impl;

import br.com.sjc.fachada.modelo.Entidade;
import br.com.sjc.fachada.rest.paginacao.Filtro;
import br.com.sjc.fachada.rest.paginacao.Operacao;
import br.com.sjc.fachada.rest.paginacao.Pagina;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.fachada.service.ServicoConsultar;
import br.com.sjc.modelo.ObjectID;
import br.com.sjc.util.AliasToBeanNestedResultTransformer;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.anotation.EntityProperties;
import br.com.sjc.util.anotation.ProjectionConfigurationDTO;
import br.com.sjc.util.anotation.ProjectionProperty;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.hibernate.property.access.internal.PropertyAccessStrategyFieldImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class ServicoGenericoConsultar<ID extends Serializable, E extends ObjectID<ID>> implements ServicoConsultar<ID, E> {

    @Autowired
    private EntityManager entityManager;

    @Override
    public E get(Serializable id) {
        return get(getNameIdProperty(), id);
    }

    @Override
    public E get(String atributo, Object valor) {
        return get((Specification<E>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(atributo), valor));
    }

    public E get(Specification<E> specification) {
        return get(entidadeClass(), specification);
    }

    public <T extends ObjectID> T get(Class<T> classe, Specification<T> specification) {
        T entidade;

        if (utilizarConsultarComProjecao()) {
            entidade = getWithProjections(classe, specification);

        } else {
            CriteriaBuilder criteriaBuilder = criteriaBuilder();
            CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(classe);
            Root<T> root = criteriaQuery.from(classe);

            List<Predicate> predicates = getPredicatesDefault(root, criteriaQuery, criteriaBuilder);
            predicates.add(specification.toPredicate(root, criteriaQuery, criteriaBuilder));

            criteriaQuery.where(predicates.toArray(new Predicate[0]));

            entidade = getSingleResult(criteriaQuery);
        }

        return entidade;
    }

    public E get(List<Selection<?>> selections, Specification<E> specification) {
        CriteriaBuilder criteriaBuilder = criteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
        Root<E> root = criteriaQuery.from(entidadeClass());

        List<Predicate> predicates = getPredicatesDefault(root, criteriaQuery, criteriaBuilder);
        predicates.add(specification.toPredicate(root, criteriaQuery, criteriaBuilder));

        criteriaQuery.where(predicates.toArray(new Predicate[]{}));

        criteriaQuery.multiselect(selections);

        return getSingleResult(criteriaQuery, entidadeClass());
    }

    public <D> D dtoGet(Class<D> dtoClass, Specification<E> specification) {
        return getSingleResult(dtoCriteriQuery(dtoClass, specification), dtoClass);
    }

    public <T> T getValorAtributo(String atributo, Serializable id) {
        return getValorAtributo(atributo, (Specification<E>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(getNameIdProperty()), id));
    }

    public <T> T getValorAtributo(String atributo, Specification<E> specification) {
        CriteriaBuilder criteriaBuilder = criteriaBuilder();
        CriteriaQuery<Object> criteriaQuery = criteriaBuilder.createQuery();
        Root<E> root = criteriaQuery.from(entidadeClass());

        criteriaQuery.select(getPathByValue(root, atributo));
        criteriaQuery.where(specification.toPredicate(root, criteriaQuery, criteriaBuilder));

        TypedQuery<Object> typedQuery = getEntityManager().createQuery(criteriaQuery);
        typedQuery.setMaxResults(1);

        T valorAtributo = null;

        try {
            valorAtributo = (T) typedQuery.getSingleResult();

        } catch (NoResultException ignored) {
        }

        return valorAtributo;
    }

    public <T> T getValoresAtributos(List<Selection> selecions, Specification<T> specification) {
        CriteriaBuilder criteriaBuilder = criteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
        Root<T> root = criteriaQuery.from((Class<T>) entidadeClass());

        criteriaQuery.where(specification.toPredicate(root, criteriaQuery, criteriaBuilder));
        criteriaQuery.multiselect(selecions.toArray(new Selection[0]));

        TypedQuery<Tuple> typedQuery = getEntityManager().createQuery(criteriaQuery);
        typedQuery.setMaxResults(1);

        T valorAtributo = null;

        try {
            Tuple tuple = typedQuery.getSingleResult();
            valorAtributo = AliasToBeanNestedResultTransformer.transformTuple(tuple, (Class<T>) entidadeClass());

        } catch (NoResultException ignored) {
        }

        return valorAtributo;
    }

    @SuppressWarnings("Duplicates")
    public boolean existe(Specification<E> specification) {
        CriteriaBuilder criteriaBuilder = criteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root root = criteriaQuery.from(entidadeClass());

        criteriaQuery.select(criteriaBuilder.countDistinct(root.get(getNameIdProperty())));
        criteriaQuery.where(specification.toPredicate(root, criteriaQuery, criteriaBuilder));

        return getEntityManager().createQuery(criteriaQuery).getSingleResult() > 0;
    }

    @Override
    public List<E> listar() {
        CriteriaBuilder criteriaBuilder = criteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
        Root<E> root = criteriaQuery.from(entidadeClass());

        criarProjectionsConsultar(criteriaBuilder, criteriaQuery, root, entidadeClass());

        List<Tuple> tuples = getEntityManager().createQuery(criteriaQuery).getResultList();

        return tuples
                .stream()
                .map(tuple -> AliasToBeanNestedResultTransformer.transformTuple(tuple, entidadeClass())).peek(entidade -> {
                    preencherCollectionsWithProjections(entidade);
                    preencherInnerCollectionsWithProjections(entidade);
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<E> listar(Specification<E> specification) {
        CriteriaBuilder criteriaBuilder = criteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
        Root<E> root = criteriaQuery.from(entidadeClass());

        criarProjectionsConsultar(criteriaBuilder, criteriaQuery, root, entidadeClass());

        criteriaQuery.where(specification.toPredicate(root, criteriaQuery, criteriaBuilder));

        List<Tuple> tuples = getEntityManager().createQuery(criteriaQuery).getResultList();

        return tuples
                .stream()
                .map(tuple -> AliasToBeanNestedResultTransformer.transformTuple(tuple, entidadeClass())).peek(entidade -> {
                    preencherCollectionsWithProjections(entidade);
                    preencherInnerCollectionsWithProjections(entidade);
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<E> listar(List<Selection<?>> selections, Specification<E> specification) {
        CriteriaBuilder criteriaBuilder = criteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
        Root<E> root = criteriaQuery.from(entidadeClass());

        criteriaQuery.where(specification.toPredicate(root, criteriaQuery, criteriaBuilder));
        criteriaQuery.multiselect(selections);

        TypedQuery<Tuple> typedQuery = getEntityManager().createQuery(criteriaQuery);

        return typedQuery.getResultList().stream().map(tuple -> AliasToBeanNestedResultTransformer.transformTuple(tuple, entidadeClass())).collect(Collectors.toList());
    }

    @Override
    public Pagina<E> listar(Paginacao paginacao) {
        Pagina<E> Pagina;

        boolean executarCount = true;
        long contar = contar(paginacao);

        if (contar > 0 || !executarCount) {
            CriteriaBuilder criteriaBuilder = criteriaBuilder();
            CriteriaQuery<E> criteriaQuery = criteriaBuilder.createQuery(entidadeClass());
            Root<E> root = criteriaQuery.from(entidadeClass());

            addWhereInCriteriaQuery(paginacao, criteriaBuilder, criteriaQuery, root);
            addOrdenacaoPorPaginacao(paginacao, criteriaBuilder, criteriaQuery, root);

            TypedQuery<E> typedQuery = getEntityManager().createQuery(criteriaQuery);
            addPaginacaoInTypeQuery(paginacao, typedQuery);

            Pagina = new Pagina<>(typedQuery.getResultList(), contar);

        } else {
            Pagina = new Pagina<>();
        }

        return Pagina;
    }

    @Override
    public <T> List<T> dtoListar() {
        Paginacao paginacao = new Paginacao();
        paginacao.setListarTodos(true);

        TypedQuery<Tuple> typedQuery = getEntityManager().createQuery(dtoCriteriQuery(paginacao));

        return typedQuery.getResultList().stream().map(tuple -> AliasToBeanNestedResultTransformer.transformTuple(tuple, (Class<T>) getDtoListagem())).collect(Collectors.toList());
    }

    @Override
    public Pagina<?> dtoListar(Paginacao paginacao) {
        Pagina<?> Pagina;

        try {
            Pagina = dtoListar(paginacao, getDtoListagem());

        } catch (Exception e) {

            e.printStackTrace();

            Pagina = new Pagina<>();

        }

        return Pagina;
    }

    @Override
    public <D> Collection<D> dtoListar(Specification specification, Class<D> dtoClass) {

        Paginacao paginacao = new Paginacao();

        paginacao.setListarTodos(true);

        paginacao.setSpecification(specification);

        return dtoListar(paginacao, dtoClass).getContent();
    }

    @Override
    public <D> Pagina<D> dtoListar(Paginacao paginacao, Class<D> dtoClass) {
        Pagina<D> Pagina;

        boolean possuiCountProperty = Arrays.stream(FieldUtils.getAllFields(dtoClass)).anyMatch(field -> {
            ProjectionProperty property = field.getAnnotation(ProjectionProperty.class);
            return !Objects.isNull(property) && property.count();
        });

        if (!paginacao.isListarTodos() && !possuiCountProperty) {
            long contar = contar(paginacao, dtoClass);

            if (contar > 0) {
                TypedQuery<Tuple> typedQuery = getEntityManager().createQuery(dtoCriteriQuery(paginacao, dtoClass));
                addPaginacaoInTypeQuery(paginacao, typedQuery);

                List<D> dtos = mapTypeQueryList(typedQuery, dtoClass);

                Pagina = new Pagina<>(dtos, contar);

            } else {
                Pagina = new Pagina<>();
            }

        } else {
            TypedQuery<Tuple> typedQuery = getEntityManager().createQuery(dtoCriteriQuery(paginacao, dtoClass));
            List<D> dtos = mapTypeQueryList(typedQuery, dtoClass);
            Pagina = new Pagina<>(dtos, dtos.size());

        }

        return Pagina;
    }

    public <T> List<T> mapTypeQueryList(TypedQuery<Tuple> typedQuery, Class<T> classe) {
        return typedQuery.getResultList().stream().map(tuple -> AliasToBeanNestedResultTransformer.transformTuple(tuple, classe)).collect(Collectors.toList());
    }

    @Override
    public long contar(Paginacao paginacao) {
        return contar(paginacao, getDtoListagem());
    }

    @Override
    public long contar(Paginacao paginacao, Class<?> dtoClass) {
        CriteriaBuilder criteriaBuilder = criteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root root = criteriaQuery.from(entidadeClass());

        createJoinsFromAnottation(root, dtoClass);

        // Adiciona os selecions do DTO principal para garantir que vai ter todos os joins não especifíciados
        addSelecionsByDTO(criteriaBuilder, criteriaQuery, root, dtoClass);

        criteriaQuery.groupBy(new ArrayList<>());

        Field fieldForCountProperty = Arrays.stream(FieldUtils.getAllFields(dtoClass))
                .filter(field -> field.isAnnotationPresent(ProjectionProperty.class) && field.getAnnotation(ProjectionProperty.class).useForCountTotal())
                .findFirst().orElse(null);

        String propertyCount = Objects.isNull(fieldForCountProperty) ? getNameIdProperty() : fieldForCountProperty.getAnnotation(ProjectionProperty.class).values()[0];

        criteriaQuery.select(criteriaBuilder.countDistinct(getPathByValue(root, propertyCount)));

        addWhereInCriteriaQuery(paginacao, criteriaBuilder, criteriaQuery, root);

        criteriaQuery.orderBy();

        return getEntityManager().createQuery(criteriaQuery).getSingleResult();
    }

    @SuppressWarnings("Duplicates")
    public Long contar(Specification<E> specification) {
        CriteriaBuilder criteriaBuilder = criteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root root = criteriaQuery.from(entidadeClass());

        criteriaQuery.select(criteriaBuilder.countDistinct(root.get(getNameIdProperty())));

        criteriaQuery.where(specification.toPredicate(root, criteriaQuery, criteriaBuilder));

        return getEntityManager().createQuery(criteriaQuery).getSingleResult();
    }

    protected final Class<E> entidadeClass() {
        return getClassEntityInference();
    }

    protected final CriteriaBuilder criteriaBuilder() {
        return getEntityManager().getCriteriaBuilder();
    }

    protected final CriteriaQuery<Tuple> dtoCriteriQuery(Paginacao paginacao) {
        return dtoCriteriQuery(paginacao, getDtoListagem());
    }

    protected final <D> CriteriaQuery<Tuple> dtoCriteriQuery(Paginacao paginacao, Class<D> dtoClass) {
        CriteriaBuilder criteriaBuilder = criteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
        Root root = criteriaQuery.from(entidadeClass());

        if (!isDTOPossuiGroupProperties(dtoClass)) {
            criteriaQuery.distinct(true);
        }

        createJoinsFromAnottation(root, dtoClass);
        addSelecionsByDTO(criteriaBuilder, criteriaQuery, root, dtoClass);
        addWhereInCriteriaQuery(paginacao, criteriaBuilder, criteriaQuery, root);
        addOrdenacaoPorPaginacao(paginacao, criteriaBuilder, criteriaQuery, root);

        return criteriaQuery;
    }

    protected final <D> CriteriaQuery<Tuple> dtoCriteriQuery(Class<D> dtoClass, Specification<E> specification) {
        CriteriaBuilder criteriaBuilder = criteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
        Root root = criteriaQuery.from(entidadeClass());

        createJoinsFromAnottation(root, dtoClass);
        addSelecionsByDTO(criteriaBuilder, criteriaQuery, root, dtoClass);

        criteriaQuery.where(specification.toPredicate(root, criteriaQuery, criteriaBuilder));

        return criteriaQuery;
    }

    protected void addPaginacaoInTypeQuery(Paginacao paginacao, TypedQuery typedQuery) {
        if (!paginacao.isListarTodos()) {
            typedQuery.setFirstResult(paginacao.getPageNumber() * paginacao.getPageSize());

            typedQuery.setMaxResults(paginacao.getPageSize());
        }
    }

    protected void addOrdenacaoPorPaginacao(Paginacao paginacao, CriteriaBuilder criteriaBuilder, CriteriaQuery criteriaQuery, Root root) {
        List<Order> orders = new ArrayList<>();

        if (ObjetoUtil.isNotNull(paginacao.getOrdenacao())) {

            switch (paginacao.getOrdenacao().getOrdenacao()) {
                case ASC:
                    orders.add(criteriaBuilder.asc(getPathByValue(root, paginacao.getOrdenacao().getCampo())));
                    break;
                case DESC:
                    orders.add(criteriaBuilder.desc(getPathByValue(root, paginacao.getOrdenacao().getCampo())));
                    break;
            }
        }

        if (!orders.isEmpty()) {
            criteriaQuery.orderBy(orders);
        }
    }

    @Override
    public Specification<E> obterEspecification(Paginacao<E> paginacao) {

        return null;
    }

    protected void addWhereInCriteriaQuery(Paginacao paginacao, CriteriaBuilder criteriaBuilder, CriteriaQuery criteriaQuery, Root root) {

        if (ObjetoUtil.isNull(paginacao.getSpecification())) {

            paginacao.setSpecification(this.obterEspecification(paginacao));
        }

        if (Objects.nonNull(paginacao.getSpecification()) && paginacao.possuiFiltros()) {
            List<Predicate> predicates = new ArrayList<>();

            predicates.add(paginacao.getSpecification().toPredicate(root, criteriaQuery, criteriaBuilder));

            criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[0])));

        } else if (Objects.nonNull(paginacao.getSpecification())) {
            criteriaQuery.where(paginacao.getSpecification().toPredicate(root, criteriaQuery, criteriaBuilder));
        }
    }

    private Predicate getPredicateFromFiltro(CriteriaBuilder criteriaBuilder, Root root, Filtro filtro) {
        Operacao operacao = filtro.getOperacao();
        Object[] valor = filtro.getValorTransformado();

        return operacao.restricao(criteriaBuilder, getPathByValue(root, filtro.getCampo()), valor);
    }

    private boolean isDTOPossuiGroupProperties(Class<?> dtoCLass) {
        boolean possuiGroupProperties = false;

        if (dtoCLass.isAnnotationPresent(ProjectionConfigurationDTO.class)) {
            ProjectionConfigurationDTO configurationDTO = dtoCLass.getAnnotation(ProjectionConfigurationDTO.class);
            possuiGroupProperties = configurationDTO.groupProperties();
        }

        return possuiGroupProperties;
    }

    protected void addSelecionsByDTO(CriteriaBuilder criteriaBuilder, CriteriaQuery criteriaQuery, Root root, Class<?> dtoCLass) {
        createJoinsFromAnottation(root, dtoCLass);

        List<Expression> groupBy = new ArrayList<>();

        List<Selection> selecions = new ArrayList<>();

        boolean utilizerGroupBy = false;
        if (dtoCLass.isAnnotationPresent(ProjectionConfigurationDTO.class)) {
            ProjectionConfigurationDTO configurationDTO = dtoCLass.getAnnotation(ProjectionConfigurationDTO.class);
            utilizerGroupBy = configurationDTO.groupProperties();
        }

        for (final Field field : FieldUtils.getAllFields(dtoCLass)) {
            if (field.isAnnotationPresent(ProjectionProperty.class)) {
                ProjectionProperty property = field.getAnnotation(ProjectionProperty.class);

                if (property.values().length <= 1) {
                    String value = property.values().length == 1 ? property.values()[0] : field.getName();
                    String alias = field.getName();
                    if (Entidade.class.isAssignableFrom(field.getType()) && !value.contains(".")) {
                        value = field.getName() + "." + value;
                        alias = value;
                    }

                    Path path = getPathByValue(root, value);

                    if (property.sum()) {

                        if (property.orderDesc()) {
                            criteriaQuery.orderBy(criteriaBuilder.desc(criteriaBuilder.sum(getPathByValue(root, value))));

                        }

                        selecions.add(criteriaBuilder.sum(getPathByValue(root, value)).alias(alias));

                    } else if (property.count()) {

                        if (property.orderDesc()) {
                            criteriaQuery.orderBy(criteriaBuilder.desc(criteriaBuilder.count(getPathByValue(root, value))));

                        }

                        selecions.add(criteriaBuilder.count(getPathByValue(root, value)).alias(alias));

                    } else {
                        path.alias(alias);
                        selecions.add(path);
                        if (utilizerGroupBy) {
                            groupBy.add(path);
                        }
                    }

                } else {
                    for (String valueTemp : property.values()) {
                        String value = field.getName() + "." + valueTemp;

                        Path path = getPathByValue(root, value);
                        path.alias(value);
                        selecions.add(path.alias(value));

                        if (utilizerGroupBy) {
                            groupBy.add(path);
                        }
                    }
                }
            }
        }

        if (!groupBy.isEmpty()) {
            criteriaQuery.groupBy(groupBy.toArray(new Expression[0]));
        }

        criteriaQuery.select(criteriaBuilder.tuple(selecions.toArray(new Selection[0])));
    }

    protected Path getPathByValue(Path root, String value) {
        return getPathByValue(root, value, false);
    }

    protected Path getPathByValue(Path root, String value, boolean juntarAliasRootNoJoin) {
        Path path;

        if (value.contains(".")) {
            From from = (From) root;

            String[] campos = value.split("\\.");
            String ultimoCampo = campos[campos.length - 1];

            for (int index = 0; index < campos.length - 1; index++) {
                String joinAlias = juntarAliasRootNoJoin ? from.getAlias() + campos[index] : campos[index];
                from = createJoin(from, campos[index], joinAlias, JoinType.LEFT);
            }

            path = from.get(ultimoCampo);
        } else {

            path = root.get(value);

        }

        return path;
    }

    protected void createJoinsFromAnottation(Root root, Class dtoCLass) {
        if (!Objects.isNull(dtoCLass) && dtoCLass.isAnnotationPresent(ProjectionConfigurationDTO.class)) {
            ProjectionConfigurationDTO configuration = (ProjectionConfigurationDTO) dtoCLass.getAnnotation(ProjectionConfigurationDTO.class);

            for (br.com.sjc.util.anotation.Join join : configuration.joins()) {

                if (join.associacao().contains(".")) {
                    String[] objetos = join.associacao().split("\\.");

                    Join jpaJoin = null;
                    for (int index = 0; index < objetos.length; index++) {
                        if (index == objetos.length - 1) {
                            createJoin(jpaJoin, objetos[index], join.apelido(), join.tipoJuncao());

                        } else {
                            jpaJoin = createJoin(Objects.isNull(jpaJoin) ? root : jpaJoin, objetos[index]);
                        }
                    }

                } else {
                    createJoin(root, join.associacao(), join.apelido(), join.tipoJuncao());

                }
            }

            createJoinsFromAnottation(root, dtoCLass.getSuperclass());
        }
    }

    /**
     * Se o join para o field especificado não existir, cria um join para o field do tipo Inner.<br/>
     * O nome do field será considerado como alias do join.
     *
     * @param from  um From
     * @param field um Field
     * @return Um Join
     */
    protected final Join createJoin(From from, String field) {
        return createJoin(from, field, field);
    }

    /**
     * Se o join para o alias não existir, cria um join para o field com o alias especificado do tipo Inner.
     *
     * @param from  um From
     * @param field um String
     * @param alias um String
     * @return Um Join
     */
    protected final Join createJoin(From from, String field, String alias) {
        return createJoin(from, field, alias, javax.persistence.criteria.JoinType.INNER);
    }

    /**
     * Se o join para o field especificado não existir, cria um join para o field.<br/>
     * O nome do field será considerado como alias do join.
     *
     * @param from     um From
     * @param field    um String
     * @param joinType um JoinType
     * @return um Join
     */
    protected final Join createJoin(From from, String field, JoinType joinType) {
        return createJoin(from, field, field, joinType);
    }

    /**
     * Se o join para o alias não existir, cria um join para o field com o alias especificado.
     *
     * @param from     um From
     * @param field    um String
     * @param alias    um String
     * @param joinType um JoinType
     * @return um Join
     */
    protected final Join createJoin(From from, String field, String alias, JoinType joinType) {
        String[] fields = field.split("\\.");

        From nextFrom = from;

        if (fields.length > 0) {
            for (int index = 0; index < fields.length - 1; index++) {
                nextFrom = createJoin(nextFrom, fields[index]);
            }
            field = fields[fields.length - 1];
        }

        Join join = getJoinByAlias(nextFrom, alias);

        if (Objects.isNull(join)) {
            join = nextFrom.join(field, joinType);
            join.alias(alias);
        }

        return join;
    }

    protected Join getJoinByAlias(From from, String alias) {
        Join join = null;

        Set<Join> joins = from.getJoins();

        if (!Objects.isNull(joins) && !joins.isEmpty()) {
            for (Join joinTemp : joins) {
                if (alias.equals(joinTemp.getAlias())) {
                    join = joinTemp;

                } else {
                    join = getJoinByAlias(joinTemp, alias);

                }

                if (!Objects.isNull(join)) {
                    break;
                }
            }
        }

        return join;
    }

    public E getWithProjections(Serializable id) {
        return getWithProjections(getNameIdProperty(), id);
    }

    public E getWithProjections(String atributo, Object valor) {
        return getWithProjections((Specification<E>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(atributo), valor));
    }

    public E getWithProjections(Specification<E> specification) {
        return getWithProjections(entidadeClass(), specification);
    }

    public <T extends ObjectID> T getWithProjections(Class<T> classe, Specification<T> specification) {
        CriteriaBuilder criteriaBuilder = criteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
        Root<T> root = criteriaQuery.from(classe);

        criarProjectionsConsultar(criteriaBuilder, criteriaQuery, root, classe);

        List<Predicate> predicates = getPredicatesDefault(root, criteriaQuery, criteriaBuilder);
        predicates.add(specification.toPredicate(root, criteriaQuery, criteriaBuilder));

        criteriaQuery.where(predicates.toArray(new Predicate[0]));

        T entidade = getSingleResult(criteriaQuery, classe);

        preencherCollectionsWithProjections(entidade);
        preencherInnerCollectionsWithProjections(entidade);

        return entidade;
    }

    private <T extends ObjectID> void preencherCollectionsWithProjections(T entidade) {
        if (!Objects.isNull(entidade)) {
            preencherCollectionsWithProjections(entidade, FieldUtils.getAllFieldsList(entidade.getClass()).stream().filter(this::fieldsPermitidosCollections).collect(Collectors.toList()));
        }
    }

    private <T extends ObjectID> void preencherInnerCollectionsWithProjections(T entidade) {
        if (!Objects.isNull(entidade)) {
            FieldUtils.getFieldsListWithAnnotation(entidade.getClass(), EntityProperties.class).stream()
                    .filter(field -> {
                        boolean isNotCollection = !Collection.class.isAssignableFrom(field.getType());
                        boolean hasCollections = field.getAnnotation(EntityProperties.class).collecions().length > 0;
                        return isNotCollection && hasCollections;

                    }).forEach(field -> {
                        ObjectID entidadeTemp = (ObjectID) PropertyAccessStrategyFieldImpl.INSTANCE.buildPropertyAccess(entidade.getClass(), field.getName()).getGetter().get(entidade);
                        preencherCollectionsWithProjections(entidadeTemp);
                    });
        }
    }

    private <T extends ObjectID> void preencherCollectionsWithProjections(T entidade, List<Field> fields) {
        fields.forEach(field -> {
            EntityProperties entityProperties = field.getAnnotation(EntityProperties.class);

            CriteriaBuilder criteriaBuilder = criteriaBuilder();
            CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
            Root root = criteriaQuery.from(entidade.getClass());

            createJoin(root, field.getName(), JoinType.INNER);

            criteriaQuery.where(criteriaBuilder.equal(root.get(getNameIdProperty(entidade)), entidade.getId()));

            List<Selection> selections = getProjectionsDoEntityProperties(field, root, false);
            criteriaQuery.select(criteriaBuilder.tuple(selections.toArray(new Selection[0])));

            TypedQuery<Tuple> typedQuery = getEntityManager().createQuery(criteriaQuery);
            Stream stream = typedQuery.getResultList().stream()
                    .map(tuple -> AliasToBeanNestedResultTransformer.transformTuple(tuple, entityProperties.targetEntity()));

            Collection<T> valor = (Collection<T>) (field.getType().equals(Set.class) ? stream.collect(Collectors.toSet()) : stream.collect(Collectors.toList()));

            if (entityProperties.collecions().length > 0) {
                List<String> collecionsFields = Arrays.asList(entityProperties.collecions());
                valor.forEach(valorTemp -> {
                    preencherCollectionsWithProjections(valorTemp, Arrays.stream(FieldUtils.getAllFields(valorTemp.getClass())).filter(fieldTemp -> collecionsFields.contains(fieldTemp.getName())).collect(Collectors.toList()));
                });
            }

            PropertyAccessStrategyFieldImpl.INSTANCE
                    .buildPropertyAccess(entidade.getClass(), field.getName())
                    .getSetter()
                    .set(entidade, valor, null);
        });
    }

    protected void criarProjectionsConsultar(CriteriaBuilder criteriaBuilder, CriteriaQuery<Tuple> criteriaQuery, From root, Class classeFields) {
        List<Selection> selecions = new ArrayList<>();

        FieldUtils.getAllFieldsList(classeFields).stream()
                .filter(this::fieldsPermitidosConsultarProjections)
                .forEach(field -> {
                    if (field.isAnnotationPresent(EntityProperties.class)) {
                        selecions.addAll(getProjectionsDoEntityProperties(field, root));

                    } else {
                        selecions.add(root.get(field.getName()).alias(field.getName()));
                    }
                });


        criteriaQuery.select(criteriaBuilder.tuple(selecions.toArray(new Selection[0])));
    }

    private List<Selection> getProjectionsDoEntityProperties(Field field, From root) {
        return getProjectionsDoEntityProperties(field, root, true);
    }

    private List<Selection> getProjectionsDoEntityProperties(Field field, From root, boolean adicionarFieldNameAoAlias) {
        List<Selection> selecions = new ArrayList<>();

        getPropertiesFromField(field).forEach(property -> {
            From join = createJoin(root, field.getName(), JoinType.LEFT);

            if (property.contains(".")) {
                selecions.add(getPathByValue(join, property, true).alias(adicionarFieldNameAoAlias ? field.getName() + "." + property : property));

            } else {
                Class<?> classType = field.getType();
                if (field.isAnnotationPresent(EntityProperties.class)) {
                    EntityProperties entityProperties = field.getAnnotation(EntityProperties.class);
                    if (entityProperties.targetEntity() != void.class) {
                        classType = entityProperties.targetEntity();
                    }
                }

                Field propertyField = FieldUtils.getField(classType, property, true);
                boolean isEntidade = propertyField != null && ObjectID.class.isAssignableFrom(propertyField.getType());
                if (isEntidade) {
                    From innerRoot = createJoin(join, property, JoinType.LEFT);
                    selecions.addAll(getProjectionsDoEntityProperties(field, innerRoot));

                } else {
                    selecions.add(join.get(property).alias(adicionarFieldNameAoAlias ? field.getName() + "." + property : property));
                }
            }
        });

        return selecions;
    }

    private List<String> getPropertiesFromField(Field field) {
        EntityProperties entityProperties = field.getAnnotation(EntityProperties.class);

        List<String> properties = new ArrayList<>();

        if (entityProperties.value().length > 0) {
            properties.addAll(Arrays.asList(entityProperties.value()));

        } else {
            Class<?> classe = Collection.class.isAssignableFrom(field.getType()) ? entityProperties.targetEntity() : field.getType();

            FieldUtils.getAllFieldsList(classe).forEach(fieldTemp -> {
                if (fieldTemp.isAnnotationPresent(EntityProperties.class) && !Collection.class.isAssignableFrom(fieldTemp.getType())) {
                    EntityProperties fieldTempEntityProperty = fieldTemp.getAnnotation(EntityProperties.class);
                    if (fieldTempEntityProperty.value().length > 0) {
                        Arrays.stream(fieldTempEntityProperty.value()).forEach(propertyTemp -> properties.add(fieldTemp.getName() + "." + propertyTemp));

                    } else {
                        FieldUtils.getAllFieldsList(fieldTemp.getType()).stream()
                                .filter(this::fieldsPermitidosConsultarProjections)
                                .forEach(item -> {

                                    if (ObjectID.class.isAssignableFrom(item.getType())) {
                                        getPropertiesFromField(item).forEach(innerFiel -> {
                                            properties.add(fieldTemp.getName() + "." + item.getName() + "." + innerFiel);
                                        });
                                    } else {
                                        properties.add(fieldTemp.getName() + "." + item.getName());
                                    }

                                });
                    }
                } else if (fieldsPermitidosConsultarProjections(fieldTemp)) {
                    properties.add(fieldTemp.getName());

                }
            });
        }

        return properties;
    }

    private boolean fieldsPermitidosConsultarProjections(Field field) {
        boolean isCollection = Collection.class.isAssignableFrom(field.getType());
        boolean isTransient = field.isAnnotationPresent(Transient.class);
        boolean isEntityWithoutEntityProperties = field.getType().isAnnotationPresent(Entity.class) && !field.isAnnotationPresent(EntityProperties.class);
        boolean isSerialVersionUID = field.getName().equals("serialVersionUID");
        boolean isStatic = Modifier.isStatic(field.getModifiers());

        return !isCollection && !isTransient && !isEntityWithoutEntityProperties && !isSerialVersionUID && !isStatic;
    }

    private boolean fieldsPermitidosCollections(Field field) {
        boolean isTransient = field.isAnnotationPresent(Transient.class);
        boolean isCollection = Collection.class.isAssignableFrom(field.getType());
        boolean isAnnotationEntityPropertiesPresent = field.isAnnotationPresent(EntityProperties.class);

        return !isTransient && isCollection && isAnnotationEntityPropertiesPresent;
    }

    protected final <T extends ObjectID> T getSingleResult(CriteriaQuery<T> criteriaQuery) {
        return getSingleResult(getEntityManager().createQuery(criteriaQuery));
    }

    protected final <T> T getSingleResult(CriteriaQuery<Tuple> criteriaQuery, Class<T> classe) {
        T t = null;

        Tuple tuple = getSingleResult(getEntityManager().createQuery(criteriaQuery));

        if (!Objects.isNull(tuple)) {
            t = AliasToBeanNestedResultTransformer.transformTuple(tuple, classe);
        }

        return t;
    }

    protected final <T> T getSingleResult(TypedQuery<T> typedQuery) {
        T t;

        try {
            typedQuery.setMaxResults(1);
            t = typedQuery.getSingleResult();
        } catch (NoResultException e) {
            t = null;
        }

        return t;
    }

    protected boolean utilizarConsultarComProjecao() {
        return true;
    }

    protected List<Predicate> getPredicatesDefault(Root<?> root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
        return new ArrayList<>();
    }

    protected String getNameIdProperty() {
        return getNameIdProperty(entidadeClass());
    }

    protected String getNameIdProperty(Object entidade) {
        return getNameIdProperty(entidade.getClass());
    }

    protected String getNameIdProperty(Class<?> classeEntidade) {
        Field field = FieldUtils.getFieldsWithAnnotation(classeEntidade, Id.class)[0];

        return field.getName();
    }

    protected Class<?> getDtoListagem() {
        return null;
    }

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    protected Class<E> getClassEntityInference() {

        try {

            final Type className = ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];

            return (Class<E>) Class.forName(className.getTypeName());

        } catch (ClassNotFoundException e) {

            throw new RuntimeException(e);

        }

    }
}

