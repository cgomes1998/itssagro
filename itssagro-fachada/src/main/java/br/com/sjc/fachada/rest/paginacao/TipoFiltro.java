package br.com.sjc.fachada.rest.paginacao;

import br.com.sjc.util.DateUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Objects;

/**
 * Created by julio.bueno on 25/03/2019.
 */
public enum TipoFiltro {
    INTEIRO {
        @Override
        public Integer converter(String valor) {

            return Integer.parseInt(valor);
        }
    },

    LONG {
        @Override
        public Long converter(String valor) {

            return Long.parseLong(valor);
        }
    },

    DECIMAL {
        @Override
        public Double converter(String valor) {

            return Double.parseDouble(valor);
        }
    },

    DATA {
        @Override
        public Date converter(String valor) {

            return DateUtil.toDate(new Long(valor));
        }
    },

    DATASTRING {
        @Override
        public String converter(String date) {

            try {
                Date dateParse = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX").parse(date);
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                return dateFormat.format(dateParse);
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    },

    LOCAL_DATE_TIME {
        @Override
        public LocalDateTime converter(String valor) {

            try {
                Date dateParse = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX").parse(valor);
                return dateParse.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

            } catch (ParseException e) {
                throw new RuntimeException(e.getMessage(), e);

            }
        }
    },

    DATA_HORA {
        @Override
        public Date converter(String valor) {

            try {
                return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(valor);

            } catch (ParseException e) {
                throw new RuntimeException(e.getMessage(), e);

            }
        }
    },

    STRING {
        @Override
        public String converter(String valor) {

            return valor;
        }

    },

    BOOLEAN {
        public Boolean converter(String valor) {

            return Boolean.valueOf(valor);
        }
    },

    ENUM {
        @Override
        public Object converter(String valor) {

            Enum<?> enumerate = null;

            if (!Objects.isNull(valor)) {
                try {
                    int ultimoIndexPonto = valor.lastIndexOf(".");

                    String classEnum = valor.substring(0, ultimoIndexPonto);
                    valor = valor.substring(ultimoIndexPonto + 1);

                    Class<? extends Enum> clazz = (Class<? extends Enum>) Class.forName(classEnum);
                    enumerate = Enum.valueOf(clazz, valor);

                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    enumerate = null;

                }
            }

            return enumerate;
        }
    },

    UUID {
        @Override
        public Object converter(String valor) {

            return java.util.UUID.fromString(valor);
        }
    };

    public abstract Object converter(String valor);
}
