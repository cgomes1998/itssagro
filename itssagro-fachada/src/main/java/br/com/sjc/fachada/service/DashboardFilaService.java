package br.com.sjc.fachada.service;

import br.com.sjc.modelo.FluxoEtapa;
import br.com.sjc.modelo.Senha;
import br.com.sjc.modelo.SenhaEtapa;
import br.com.sjc.modelo.TipoFluxoEnum;
import br.com.sjc.modelo.dto.*;
import br.com.sjc.modelo.enums.CriterioTipoEnum;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.modelo.sap.Romaneio;

import java.util.List;

public interface DashboardFilaService {

    List<EtapaFilaDTO> obterFilas(DashboardFilaFiltroDTO filtro);

    List<SenhaEtapaRelatorioDTO> obterFilasRelatorio(RelatorioFilaFiltroDTO filtro);

    void validarEventosDaAutomacao(Romaneio romaneio, EtapaEnum etapaAtualDoRomaneioEnum, CriterioTipoEnum criterioTipoEnum);

    void validarCondicaoSaidaEtapa(EtapaSenhaFilaDTO etapaSenhaFila);

    void executarEventosAutomacao();

    void estornar(EtapaSenhaFilaDTO etapaSenhaFila);

    FluxoEtapa obterFluxoEtapaPorMaterialEEtapa(TipoFluxoEnum tipoFluxo, Material material, EtapaEnum etapaEnum);

    void atualizarDashboard();

    void alterarStatusPesagem(Senha senha, FluxoEtapa fluxoEtapa);

}
