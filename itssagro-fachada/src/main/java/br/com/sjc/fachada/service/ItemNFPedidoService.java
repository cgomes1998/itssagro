package br.com.sjc.fachada.service;

import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.sap.ItemNFPedido;

import java.util.Date;

public interface ItemNFPedidoService extends Servico<Long, ItemNFPedido> {

    void atualizarRequestKey(String requestKey, Long id);

    void atualizarDocumentoFaturamento(Long documentoFaturamento, String requestKey, String numeroRomaneio);

    Boolean existeNotaSerieDataInicioDataFimProdutorSafraMaterialStatusSapStatusRomaneioIdItem(String nota, String serie, Date dataInicio, Date dataFim, Long idProdutor, Long idSafra, Long idMaterial, StatusIntegracaoSAPEnum status,
                                                                                               StatusRomaneioEnum statusRomaneio, Long idItem);

    Boolean existePlacaCavaloStatusRomaneioRomaneioDiferenteDe(Long idPlaca, StatusRomaneioEnum statusRomaneio, Long idRomaneio);

    Boolean existePlacaCavaloStatusRomaneio(Long idPlaca, StatusRomaneioEnum statusRomaneio);

}
