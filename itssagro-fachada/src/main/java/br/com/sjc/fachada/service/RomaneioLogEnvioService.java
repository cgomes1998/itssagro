package br.com.sjc.fachada.service;

import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.RomaneioLogEnvio;

public interface RomaneioLogEnvioService extends Servico<Long, RomaneioLogEnvio> {
    RomaneioLogEnvio obterPorRomaneio(Romaneio romaneio);

    RomaneioLogEnvio obterPorRomaneioId(Long id);
}
