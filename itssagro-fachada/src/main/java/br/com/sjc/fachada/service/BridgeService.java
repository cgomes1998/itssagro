package br.com.sjc.fachada.service;

import br.com.sjc.modelo.sap.Bridge;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;

import java.util.List;

public interface BridgeService extends Servico<Long, Bridge> {

    void salvar(List<EventInRequestItem> items, Romaneio romaneio);

    boolean existeBridgeParaRomaneio(Romaneio romaneio);

    List<Bridge> listarBridges(final Long idRomaneio);
}
