package br.com.sjc.fachada.service;

import br.com.sjc.modelo.Perfil;
import br.com.sjc.modelo.PerfilFuncionalidade;
import br.com.sjc.modelo.PerfilOperacao;
import br.com.sjc.modelo.enums.StatusEnum;

import java.util.List;

public interface PerfilService extends Servico<Long, Perfil> {

    List<Perfil> findByStatus(StatusEnum status);

    List<PerfilFuncionalidade> listarFuncionalidadesPorPerfilId(Long id);

    List<PerfilOperacao> listarOperacoesPorPerfilId(Long id);
}
