package br.com.sjc.fachada.service;

import br.com.sjc.modelo.RecebimentoNfeSapCargaPontual;

public interface RecebimentoNfeSapCargaPontualService extends Servico<Long, RecebimentoNfeSapCargaPontual> {

}
