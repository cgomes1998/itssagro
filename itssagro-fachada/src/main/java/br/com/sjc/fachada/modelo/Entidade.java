package br.com.sjc.fachada.modelo;

public interface Entidade {

    Long getId();

}
