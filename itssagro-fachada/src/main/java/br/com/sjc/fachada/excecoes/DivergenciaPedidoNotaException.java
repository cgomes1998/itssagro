package br.com.sjc.fachada.excecoes;

import br.com.sjc.util.bundle.MessageSupport;

public class DivergenciaPedidoNotaException extends Exception {

    private static final long serialVersionUID = 1L;

    public DivergenciaPedidoNotaException(final Exception e) {

        super(e);
    }

    public DivergenciaPedidoNotaException() {

        super(MessageSupport.getMessage("MSGE005"));
    }

}