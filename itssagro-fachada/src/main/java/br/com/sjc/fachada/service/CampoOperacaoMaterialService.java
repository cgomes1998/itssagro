package br.com.sjc.fachada.service;

import br.com.sjc.modelo.cfg.Campo;
import br.com.sjc.modelo.cfg.CampoOperacaoMaterial;
import br.com.sjc.modelo.sap.Material;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CampoOperacaoMaterialService extends Servico<Long, CampoOperacaoMaterial> {

    void salvar(List<CampoOperacaoMaterial> lista);

    Campo obterCampo(String variavel);

    @Transactional(readOnly = true)
    List<CampoOperacaoMaterial> obterCamposPorMaterial(Material material);
}
