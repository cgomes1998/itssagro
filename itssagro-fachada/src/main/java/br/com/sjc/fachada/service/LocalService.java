package br.com.sjc.fachada.service;

import br.com.sjc.modelo.Local;

import java.util.List;

/**
 * Created by julio.bueno on 03/07/2019.
 */
public interface LocalService extends Servico<Long, Local> {
    List<Local> listarLocaisAtivos();
}
