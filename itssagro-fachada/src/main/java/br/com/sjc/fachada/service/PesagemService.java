package br.com.sjc.fachada.service;

import br.com.sjc.modelo.Balanca;
import br.com.sjc.modelo.Pesagem;
import br.com.sjc.modelo.dto.PesagemHistoricoDTO;
import br.com.sjc.modelo.dto.PesagemManualDTO;
import br.com.sjc.modelo.sap.ItemNFPedido;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface PesagemService extends Servico<Long, Pesagem> {

	PesagemHistoricoDTO obterSegundaPesagemDoRomaneio(Long idPesagem);

	PesagemHistoricoDTO obterPrimeiraPesagemDoRomaneio(Long idPesagem);

	PesagemHistoricoDTO obterSegundaPesagemDoRomaneio(List<PesagemHistoricoDTO> pesagensHistorico, Long idPesagem);

	PesagemHistoricoDTO obterPrimeiraPesagemDoRomaneio(List<PesagemHistoricoDTO> pesagensHistorico, Long idPesagem);

	List<PesagemHistoricoDTO> obterSegundaPesagemDeRomaneios(List<Long> idPesagens);

	List<PesagemHistoricoDTO> obterPrimeiraPesagemDeRomaneios(List<Long> idPesagens);

	List<PesagemManualDTO> obterPesagensManuais(ItemNFPedido filtro);

	Map<String, Date> getDatasDePesagem(
		   List<PesagemHistoricoDTO> historicosPrimeiraPesagem,
		   List<PesagemHistoricoDTO> historicosSegundaPesagem,
		   Long idPesagem
	);

}
