package br.com.sjc.fachada.service;

import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.modelo.cfg.Ambiente;
import br.com.sjc.modelo.dto.LogIntegracaoDTO;
import br.com.sjc.modelo.dto.OrdemVendaDTO;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.response.DadosNfeTransferenciaResponse;
import br.com.sjc.modelo.response.SaldoPedidoEToleranciaPedidoTransferenciaResponse;
import br.com.sjc.modelo.sap.ConfiguracaoUnidadeMedidaMaterial;
import br.com.sjc.modelo.sap.ItemNFPedido;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.response.*;
import org.quartz.JobDataMap;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface SAPService {

    void initRFCServer();

    void realizarPing(Ambiente ambienteLogado) throws Exception;

    void enviarCadastro(DadosSincronizacaoEnum entidade, JobDataMap jobDataMap);

    List<LogIntegracaoDTO> obterEtapasDoRomaneio(String numeroRomaneio);

    void atualizarRomaneios(Long idRomaneio, Date dataUltimaSincronizacao);

    void sincronizarRFC(DadosSincronizacaoEnum entidade, Date ultimaSincronizacao);

    void sincronizar(DadosSincronizacaoEnum entidade, Date ultimaSincronizacao);

    boolean enviarRomaneio(Romaneio romaneio, boolean reenvio);

    List<ConfiguracaoUnidadeMedidaMaterial> obterMedidasParaConversao(Material material);

    BigDecimal converterQuantidade(List<ConfiguracaoUnidadeMedidaMaterial> unidades, String unidadeDescricao, Double quantidade);

    BigDecimal converterQuantidade(List<ConfiguracaoUnidadeMedidaMaterial> unidades, String unidadeBalanca, String unidadeASerEnviadaAoSAP, Double quantidade);

    void reenviarRomaneioPorId(Long id);

    void reenviarRomaneioPorData(Date data);

    Romaneio registrarLogIntegracao(Romaneio romaneio, StatusRomaneioEnum statusRomaneio, StatusIntegracaoSAPEnum statusSap, boolean enviado, String log, String exception);

    RfcSaldoOrdemPedidoResponse buscarSaldoPedido(String pedido, String item, String operacao) throws Exception;

    RfcToleranciaOrdemPedidoResponse buscarToleranciaPedido(String pedido, String item, String operacao) throws Exception;

    RfcObterNfeResponse buscarDadosNfe(String chave) throws Exception;

    DadosNfeTransferenciaResponse buscarDadosNfeTransferencia(String chave, String remessa) throws Exception;

    RfcObterCteResponse buscarDadosCte(String chave) throws Exception;

    OrdemVendaDTO buscarOrdemVenda(String codigoOrdemVenda) throws Exception;

    RfcSaldoOrdemVendaResponse buscarSaldoOrdemVenda(String ordemVenda) throws Exception;

    RfcSaldoEstoqueProdutoAcabadoResponse buscarSaldoEstoqueProdutoAcabado(String material, String centro);

    void criarOuModificarDocumentoTransporte(Romaneio entidade);

    void criarRemessa(Romaneio entidade);

    void estornarRemessa(Romaneio romaneio, final boolean deletarItensEstornados);

    RfcOrdensVendaResponse buscarOrdens(Paginacao<ItemNFPedido> filtro);

    void validarStatusOrdemDeVenda(Romaneio entidade);

    SaldoPedidoEToleranciaPedidoTransferenciaResponse buscarSaldoPedidoEToleranciaPedidoTransferencia(String pedido, String item, String operacao) throws Exception;
}
