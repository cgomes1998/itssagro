package br.com.sjc.fachada.dao;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import java.util.List;

public interface DAOConsultar<ID, E> extends JpaSpecificationExecutor<E>, QueryByExampleExecutor<E>, PagingAndSortingRepository<E, ID> {

    List<E> findAll();

    List<E> findAll(Sort var1);

    List<E> findAllById(Iterable<ID> var1);

    <S extends E> List<S> findAll(Example<S> var1);

    <S extends E> List<S> findAll(Example<S> var1, Sort var2);

}
