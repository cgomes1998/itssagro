package br.com.sjc.fachada.service;

import br.com.sjc.modelo.endpoint.request.item.MotoristaRequest;
import br.com.sjc.modelo.endpoint.response.item.MotoristaResponse;
import br.com.sjc.modelo.sap.Motorista;
import br.com.sjc.modelo.sap.request.EventOutRequest;
import br.com.sjc.modelo.sap.response.item.RfcMotoristaResponseItem;

import java.util.Date;
import java.util.List;

public interface MotoristaService extends Servico<Long, Motorista> {

    List<MotoristaResponse> listarParaArmazemTerceiros(List<MotoristaRequest> requests);

    void salvarSAP(EventOutRequest request, String host, String mandante);

    void salvarSAP(List<RfcMotoristaResponseItem> retorno);

    List<Motorista> listarPreCadastro();

    List<Motorista> listarSincronizados(Date data);

    Motorista obterPorCodigo(String codigo);

    void transformarToUpperCase(Motorista entidade);

    List<Motorista> buscarMotoristasAutoComplete(String value);

    Motorista getMorotistaPorVeiculo(Long veiculoId);
}
