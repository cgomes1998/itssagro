package br.com.sjc.fachada.service;

import br.com.sjc.modelo.LogAtividade;
import br.com.sjc.modelo.dto.LogAtividadeDTO;
import br.com.sjc.modelo.enums.EntidadeTipo;

import java.util.List;

public interface LogAtividadeService extends Servico<Long, LogAtividade> {

    List<LogAtividadeDTO> getLogAtividades(EntidadeTipo entidadeTipo, Long entidadeId);

}
