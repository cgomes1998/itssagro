package br.com.sjc.fachada.service;

import br.com.sjc.modelo.dto.RomaneioRequestKeyDTO;
import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.RomaneioRequestKey;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;

import java.util.List;

public interface RomaneioRequestKeyService extends Servico<Long, RomaneioRequestKey> {

    RomaneioRequestKey obterPorRequestEOperacao(String request, String operacao);

    List<RomaneioRequestKeyDTO> listarRomaneiosRequestsPorId(List<Long> idsRomaneio);

    void atualizarStatusSAPRomaneioPorOperacaoERequest(String chave, String operacao, StatusIntegracaoSAPEnum statusIntegracaoSAP);
}
