package br.com.sjc.fachada.service;

import br.com.sjc.modelo.Fluxo;
import br.com.sjc.modelo.Senha;
import br.com.sjc.modelo.dto.FluxoDTO;
import br.com.sjc.modelo.dto.MaterialDTO;
import br.com.sjc.modelo.sap.Centro;

import java.util.List;

public interface FluxoService extends Servico<Long, Fluxo> {

    List<FluxoDTO> listarOrdernandoDeFormaCrescentePorCodigo();

    List<MaterialDTO> listarMateriaisDoFluxo(List<Long> idFluxos);

    Fluxo obterPorCentroMaterial(Centro centro, Senha senha);
}
