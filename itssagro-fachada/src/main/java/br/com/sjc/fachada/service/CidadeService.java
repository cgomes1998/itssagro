package br.com.sjc.fachada.service;

import br.com.sjc.modelo.Cidade;
import br.com.sjc.modelo.enums.UFEnum;

import java.util.List;

public interface CidadeService extends Servico<Long, Cidade>{

    List<Cidade> listarCidadeUF(UFEnum uf);

}
