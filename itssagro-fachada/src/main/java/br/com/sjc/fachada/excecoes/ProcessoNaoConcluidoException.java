package br.com.sjc.fachada.excecoes;

import br.com.sjc.util.bundle.MessageSupport;

public class ProcessoNaoConcluidoException extends Exception {

    private static final long serialVersionUID = 1L;

    public ProcessoNaoConcluidoException(final Exception e) {

        super(e);
    }

    public ProcessoNaoConcluidoException() {

        super(MessageSupport.getMessage("MSGE028"));
    }

}
