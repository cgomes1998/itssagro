package br.com.sjc.fachada.rest;

import br.com.sjc.modelo.EntidadeAutenticada;
import br.com.sjc.modelo.EntidadeGenerica;
import br.com.sjc.util.rest.MapBuilder;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.Objects;

public abstract class ManutencaoEndpoint<E> extends ConsultaEndpoint<E> {

    private static final long serialVersionUID = 1L;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response salvar(E entidade) {

        try {

            this.atribuirUsuario(entidade);

            this.getService().salvar(entidade);

            return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSG001")).build()).build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response alterar(E entidade) {

        try {

            this.atribuirUsuario(entidade);

            this.getService().salvar(entidade);

            return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSG002")).build()).build();

        } catch (Exception e) {

            return Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }
    }

    @PUT
    @Path("status")
    @PermitAll
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response status(Long id) {

        final E entidade = this.getService().get(id);

        this.atribuirUsuario(entidade);

        this.getService().alterarStatus(entidade);

        return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSG002")).build()).build();
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response excluir(@PathParam("id") Long id) {

        this.getService().excluirPorId(id);

        return Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem("MSG003")).build()).build();
    }

    protected void atribuirUsuario(final Object entidade) {

        try {

            ((EntidadeGenerica) entidade).setUsuarioLogado(this.getUsuarioToken());

        } catch (final Exception ignored) {

            ignored.printStackTrace();
        }

    }

    public Response onExecuteService(E entidade, OnExecuteServiceCallBack callBack) {

        Response response;

        try {
            this.atribuirUsuario(entidade);

            callBack.executar();
            response = callBack.getResponse();

            if (Objects.isNull(response)) {
                response = Response.ok(MapBuilder.create(RESPONSE_MENSAGEM, this.getMensagem(callBack.getMensagem())).build()).build();
            }

        } catch (Exception e) {

            response = Response.ok(MapBuilder.create(ERRO, e.getMessage()).build()).build();
        }

        return response;
    }

    protected interface OnExecuteServiceCallBack {

        void executar() throws Exception;

        default Response getResponse() {

            return null;
        }

        default String getMensagem() {

            return "MSG001";
        }

    }

}

