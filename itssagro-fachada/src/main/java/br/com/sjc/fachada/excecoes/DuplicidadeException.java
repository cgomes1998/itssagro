package br.com.sjc.fachada.excecoes;

import br.com.sjc.util.bundle.MessageSupport;

public class DuplicidadeException extends Exception {

    private static final long serialVersionUID = 1L;

    public DuplicidadeException(final Exception e) {

        super(e);
    }

    public DuplicidadeException(String... args) {

        super(MessageSupport.getMessage("MSGE029", args));
    }

}
