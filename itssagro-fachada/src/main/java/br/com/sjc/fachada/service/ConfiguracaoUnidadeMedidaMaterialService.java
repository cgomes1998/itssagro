package br.com.sjc.fachada.service;

import br.com.sjc.modelo.sap.ConfiguracaoUnidadeMedidaMaterial;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ConfiguracaoUnidadeMedidaMaterialService extends Servico<Long, ConfiguracaoUnidadeMedidaMaterial> {

    @Transactional(readOnly = true)
    List<ConfiguracaoUnidadeMedidaMaterial> obterConfiguracoesUnidadesMedidas(
            String codigoMaterial
    );
}
