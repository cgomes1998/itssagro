package br.com.sjc.fachada.service;

import br.com.sjc.modelo.pojo.cargaPontual.autenticacao.RetornoGerarToken;

public interface AutenticacaoCargaPontualService {

    void gerarTokenAutenticacao() throws Exception;

    RetornoGerarToken getRetornoGerarToken();

}
