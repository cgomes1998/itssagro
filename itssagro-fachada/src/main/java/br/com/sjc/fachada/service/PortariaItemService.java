package br.com.sjc.fachada.service;

import br.com.sjc.modelo.cfg.PortariaItem;
import br.com.sjc.modelo.dto.PortariaItemDTO;

import java.util.List;

public interface PortariaItemService extends Servico<Long, PortariaItem> {
    List<PortariaItemDTO> listarItensAtivos();
}
