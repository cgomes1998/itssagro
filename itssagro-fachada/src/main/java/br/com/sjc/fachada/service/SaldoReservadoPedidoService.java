package br.com.sjc.fachada.service;

import br.com.sjc.modelo.SaldoReservadoPedido;
import br.com.sjc.modelo.request.SaldoPedidoReservadoRequest;
import br.com.sjc.modelo.response.SaldoPedidoReservadoResponse;

import java.math.BigDecimal;

public interface SaldoReservadoPedidoService extends Servico<Long, SaldoReservadoPedido> {
    void reservarSaldoPedido(String numeroRomaneioConsumidor, String numeroPedido, String itemPedido, BigDecimal saldoReservado);

    void liberarSaldoPedido(String numeroRomaneioConsumidor, String numeroPedido, String itemPedido);

    SaldoPedidoReservadoResponse obterSaldoReservadoPedido(SaldoPedidoReservadoRequest request);
}
