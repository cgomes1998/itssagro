package br.com.sjc.fachada.service;

import br.com.sjc.modelo.Formulario;
import br.com.sjc.modelo.FormularioItem;

import java.io.OutputStream;
import java.util.List;

/**
 * Created by julio.bueno on 04/07/2019.
 */
public interface FormularioService extends Servico<Long, Formulario> {

    Boolean verificarSeFormularioEstaEmUso(Long id);

    void publicar(Formulario formulario);

    void descontinuar(Formulario formulario);

    void clonar(Long Id);

    List<FormularioItem> listarItens(Long formularioId);

    void gerarPdfFormulario(Long id, OutputStream output);

    void gerarPdfFormularioRomaneio (
            Long idFormulario,
            Long idRomaneio,
            OutputStream outputStream
    );

    List<Formulario> listarPublicados();
}
