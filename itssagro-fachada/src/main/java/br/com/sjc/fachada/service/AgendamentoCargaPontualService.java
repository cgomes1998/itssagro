package br.com.sjc.fachada.service;

import br.com.sjc.modelo.AgendamentoCargaPontual;
import br.com.sjc.modelo.FluxoEtapa;
import br.com.sjc.modelo.Senha;
import br.com.sjc.modelo.enums.TipoParametroEnum;

import java.util.Date;

public interface AgendamentoCargaPontualService extends Servico<Long, AgendamentoCargaPontual> {

    void trocaStatusEntradaMotorista(Senha senha) throws Exception;

    void trocaStatus(Senha senha, FluxoEtapa fluxoEtapa, Boolean estorno) throws Exception;

    void gerarSenhaCargaPontual(Date... dataFiltro);

}
