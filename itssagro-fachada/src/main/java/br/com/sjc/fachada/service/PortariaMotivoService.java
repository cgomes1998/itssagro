package br.com.sjc.fachada.service;

import br.com.sjc.modelo.cfg.PortariaMotivo;
import br.com.sjc.modelo.dto.PortariaMotivoDTO;

import java.util.List;

public interface PortariaMotivoService extends Servico<Long, PortariaMotivo> {
    List<PortariaMotivoDTO> listarMotivosAtivos();
}
