package br.com.sjc.fachada.service;

import br.com.sjc.fachada.modelo.EmailSenderConfiguration;
import br.com.sjc.util.ItssAgroThreadPool;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.Util;
import br.com.sjc.util.email.Email;
import br.com.sjc.util.email.EmailBodyBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.util.StringUtils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
public abstract class EmailSenderService {

    public void sendEmail(Email email) {
        if (!email.getDestinatario().isEmpty()) {
            ItssAgroThreadPool.enfileirarTarefaFixed(() -> {
                EmailSenderConfiguration configuracao = getConfiguracao();

                if (configuracao.possuiTodasInformacoess()) {
                    List<String> logs = new ArrayList<>();

                    try {
                        Session mailSession = createMailSession(configuracao, logs);

                        String assunto = substituirParametros(email, email.getAssunto(), false);
                        String textoEmailHtml = getTextoEmailPadronizado(email);
                        String destinatarios = getDestinatarios(email.getDestinatario());

                        MimeMessage message = new MimeMessage(mailSession);
                        message.setFrom(new InternetAddress(configuracao.getRemetente()));
                        message.addRecipients(Message.RecipientType.CC, getDestinatarios(email.getCC()));
                        message.addRecipients(Message.RecipientType.BCC, getDestinatarios(email.getBCC()));
                        message.addRecipients(Message.RecipientType.TO, destinatarios);
                        message.setSubject(assunto, "UTF-8");

                        Multipart corpoEmail = new MimeMultipart();
                        addTextoEmail(corpoEmail, textoEmailHtml);
                        addAnexos(corpoEmail, email.getAnexos());
                        message.setContent(corpoEmail);

                        logs.add("---- Enviando e-mail ----");
                        logs.add("From: " + configuracao.getRemetente());
                        logs.add("To: " + destinatarios);
                        logs.add("Subject: " + assunto);
                        logs.add("Text: " + textoEmailHtml);
                        logs.add("Anexos: " + getNomesAnexos(email));

                        if (!configuracao.getAmbienteDev()) {
                            Transport.send(message);
                            apagarArquivosAnexos(email);
                        }

                        logs.add("Status: Email Enviado");
                        logs.add("Ambiente de Desenvolvimento: " + configuracao.getAmbienteDev() + " (se igual a 'true' os e-mails não são enviados de verdade)");
                    } catch (Exception e) {
                        logs.add("Status: Email Não Enviado:");
                        log.error("Erro ao Enviar Email:", e);
                        pringLogs(logs);
                        throw new RuntimeException(e);

                    }
                    logs.add("-------------------------");
                    pringLogs(logs);

                } else {
                    log.warn("---- O sistema não possui todas as configurações de e-mail ----");
                }
            });
        }
    }

    private synchronized void pringLogs(List<String> logs) {
        logs.forEach(log::info);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void apagarArquivosAnexos(Email email) {
        if (!email.getAnexos().isEmpty()) {
            email.getAnexos().forEach(file -> {
                if (file.exists()) {
                    file.delete();
                }
            });
        }
    }

    private String getNomesAnexos(Email email) {
        String nomesAnexos = "-";

        if (!email.getAnexos().isEmpty()) {
            nomesAnexos = email.getAnexos().stream().map(File::getName).reduce("", (nomeConcatenado, nome) -> {
                if (nomeConcatenado.equals("")) {
                    return nome;
                } else {
                    return nomeConcatenado + ", " + nome;
                }
            });
        }

        return nomesAnexos;
    }

    private void addTextoEmail(Multipart corpoEmail, String textoEmailHtml) throws Exception {
        MimeBodyPart textoEmail = new MimeBodyPart();
        textoEmail.setContent(textoEmailHtml, "text/html");

        corpoEmail.addBodyPart(textoEmail);
    }

    private void addAnexos(Multipart corpoEmail, List<File> anexos) throws Exception {
        AtomicBoolean falhaAoAnexarArquivos = new AtomicBoolean(false);

        anexos.forEach(file -> {
            try {
                MimeBodyPart anexo = new MimeBodyPart();
                anexo.attachFile(file);

                corpoEmail.addBodyPart(anexo);

            } catch (Exception e) {
                falhaAoAnexarArquivos.set(true);
                log.error("Falha ao anexos o arquivo:" + file.getName(), e);

            }
        });

        if (falhaAoAnexarArquivos.get()) {
            throw new Exception("Não foi possível anexos todos os arquivos no email.");
        }
    }

    private String getDestinatarios(List<String> destinatarios) {
        return destinatarios.stream().reduce("", (destinatario_1, destinatario_2) -> destinatario_2 + "," + destinatario_1);
    }

    private String getTextoEmailPadronizado(Email email) {
        return "<font color='#000000' size='2' face='Verdana'>" + substituirParametros(email, email.getTextoEmail()) + getRodape() + "</font>";
    }

    private String substituirParametros(Email email, String valor) {
        return substituirParametros(email, valor, true);
    }

    private String substituirParametros(Email email, String valor, boolean escapeHtml) {
        StringBuilder novoValor = new StringBuilder(valor);

        Map<String, String> parametros = email.getParametros();
        parametros.put(EmailBodyBuilder.PARAMETRO_URL_SISTEMA, getConfiguracao().getUrlSistema());

        parametros.forEach((key, value) -> {
            try {
                Util.replaceAll(novoValor, key, escapeHtml ? StringEscapeUtils.escapeHtml4(value) : value);
            } catch (NullPointerException e) {
                log.error("Não foi possível substituir o parametro " + key + " do email " + email.getClass().getSimpleName(), e);
            }
        });

        return novoValor.toString();
    }

    private String getRodape() {
        return EmailBodyBuilder
                .build()
                .addLineBreak(2)
                .addLineDivider()
                .open_P_element("text-align: right")
                .addDataHora(new Date())
                .close_P_element()
                .toString();
    }

    private Session createMailSession(EmailSenderConfiguration configuracao, List<String> logs) {
        Session mailSession;

        Properties properties = new Properties();
        properties.put("mail.smtp.host", configuracao.getHost());
        properties.put("mail.smtp.port", configuracao.getPorta());
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.debug", "true");

        if (!StringUtils.isEmpty(configuracao.getPorta())) {
            properties.put("mail.smtp.port", configuracao.getPorta());
        }

        if (configuracao.isProtocoloSSL()) {
            properties.put("mail.smtp.ssl.enable", true);
            properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            properties.put("mail.smtp.socketFactory.fallback", "false");
            properties.put("mail.smtp.starttls.enable", "true");

            if (!StringUtils.isEmpty(configuracao.getPorta())) {
                properties.put("mail.smtp.ssl.socketFactory.port", configuracao.getPorta());

            } else {
                properties.put("mail.smtp.port", "465");
                properties.put("mail.smtp.socketFactory.port", "465");

            }

        } else {
            properties.put("mail.smtp.socketFactory.fallback", "false");
            properties.put("mail.smtp.starttls.enable", "true");
        }

        if (StringUtil.isNotNullEmpty(configuracao.getSenha())) {
            mailSession = Session.getInstance(properties, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(configuracao.getUsuario(), configuracao.getSenha());
                }
            });
        } else {
            mailSession = Session.getInstance(properties);
        }

        return mailSession;
    }

    public abstract EmailSenderConfiguration getConfiguracao();
}
