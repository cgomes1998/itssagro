package br.com.sjc.fachada.service;

import br.com.sjc.modelo.sap.Centro;
import br.com.sjc.modelo.sap.request.EventOutRequest;

import java.util.Date;
import java.util.List;

public interface CentroService extends Servico<Long, Centro> {

    void salvarSAP(EventOutRequest request, String host, String mandante);

    void sincronizar(List<Centro> entidades);

    List<Centro> listarSincronizados(final Date data);

    Centro obterPorCodigo(String codigo);
}
