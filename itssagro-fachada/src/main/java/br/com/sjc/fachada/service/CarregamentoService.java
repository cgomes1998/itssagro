package br.com.sjc.fachada.service;

import br.com.sjc.modelo.Carregamento;
import br.com.sjc.modelo.sap.Material;

public interface CarregamentoService extends Servico<Long, Carregamento> {


    Integer obterProximoCertificado(Long material, String tanque);
}
