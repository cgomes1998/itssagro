package br.com.sjc.fachada.service;

import br.com.sjc.modelo.LogConversaoLitragem;

public interface LogConversaoLitragemService extends Servico<Long, LogConversaoLitragem> {}
