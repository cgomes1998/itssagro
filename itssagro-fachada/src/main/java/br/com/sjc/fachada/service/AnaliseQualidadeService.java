package br.com.sjc.fachada.service;

import br.com.sjc.modelo.AnaliseQualidade;
import br.com.sjc.modelo.dto.AnaliseFiltroDTO;
import br.com.sjc.modelo.sap.Romaneio;

import java.util.List;

public interface AnaliseQualidadeService extends Servico<Long, AnaliseQualidade> {

    byte[] imprimirLaudo ( Long idLaudo ) throws Exception;

    byte[] imprimirRomaneio ( Long idRomaneio, Romaneio romaneioConsultado) throws Exception;

    List<AnaliseQualidade> obterAnaliseQualidadeParaCarregamento(AnaliseFiltroDTO filtroDTO);

    boolean laudoEmUso(Long idLaudo);

}
