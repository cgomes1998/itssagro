package br.com.sjc.fachada.service;

import br.com.sjc.modelo.dto.TipoVeiculoDTO;
import br.com.sjc.modelo.dto.TipoVeiculoSetaDTO;
import br.com.sjc.modelo.sap.TipoVeiculo;
import br.com.sjc.modelo.sap.request.EventOutRequest;

import java.util.Date;
import java.util.List;

public interface TipoVeiculoService extends Servico<Long, TipoVeiculo> {

    List<TipoVeiculoSetaDTO> listarSetasDtoPorTipoVeiculo ( Long id );

    void salvarSAP( EventOutRequest request, String host, String mandante);

    List<TipoVeiculo> listarSincronizados(final Date data);

    void sincronizar(List<TipoVeiculo> entidades);
}
