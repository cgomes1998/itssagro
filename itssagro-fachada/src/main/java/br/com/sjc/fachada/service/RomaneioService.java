package br.com.sjc.fachada.service;

import br.com.sjc.fachada.excecoes.*;
import br.com.sjc.fachada.rest.paginacao.Pagina;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.modelo.Motivo;
import br.com.sjc.modelo.Senha;
import br.com.sjc.modelo.Usuario;
import br.com.sjc.modelo.dto.*;
import br.com.sjc.modelo.dto.aprovacaoDocumento.RomaneioAprovacaoDocumento;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.enums.StatusPedidoEnum;
import br.com.sjc.modelo.response.*;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.modelo.sap.request.item.RfcStatusRomaneioRequestItem;
import br.com.sjc.modelo.sap.response.item.RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_DADOS_Item;
import net.sf.jasperreports.engine.JRException;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface RomaneioService extends Servico<Long, Romaneio> {

    RomaneioLogEnvio obterRomaneioLogEnvioPorId(Long id);

    Centro obterCentroLogado();

    void atualizarDadosAtendimento(String numeroRomaneio, Integer numeroCartaoAcesso, Boolean atendido, Date dataAtendimento);

    List<ItemNFPedidoDTO> obterRomaneiosParaAutoatendimento();

    List<ItemNFPedidoDTO> obterItensDeRomaneioParaEntradaLiberada();

    void simplesPersistencia(Romaneio romaneio);

    Optional<Romaneio> obterRomaneioParaAutoatendimento(AutoAtendimentoMotorista autoAtendimentoMotorista);

    List<RfcStatusRomaneioRequestItem> listarRequestKeysDeRomaneiosPorId(List<Long> idRomaneios);

    List<RomaneioConversaoLitragemDTO> obterRomaneiosComConversaoLitragem(List<String> romaneios);

    List<RfcStatusRomaneioRequestItem> obterRequestsDeRomaneiosSemNota(List<Long> idRomaneios);

    void atualizarRequest(Long romaneioId);

    List<PesagemManualDTO> obterPesagensManuais(ItemNFPedido filtro);

    @Transactional
    List<RelatorioComercialDTO> obterRelatorioComercial(RelatorioComercialFiltroDTO filtro);

    @Transactional
    List<RelComercialNFeDTO> getRelatorioComercialNFeDTOS(RelatorioComercialFiltroDTO filtro);

    List<RelatorioComercial_V2_DTO> obterRelatorioComercial_V2(RelatorioComercialFiltroDTO filtro);

    @Transactional
    List<RelatorioFluxoDeCarregamentoDTO> obterRelatorioFluxoDeCarregamento(RelatorioFluxoDeCarregamentoFiltroDTO filtro);

    List<Romaneio> obterRomaneioProjetandoApenasIdEOperacao(Set<String> numeroRomaneios);

    void validarEtapas(final Romaneio romaneio) throws ServicoException;

    List<RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_DADOS_Item> filtrarRelatorioFornecedorGraosPagamento(RelatorioDepositoFiltroDTO filtro);

    Object filtrarDepositosFornecedorGraosEntregas(RelatorioDepositoFiltroDTO filtro);

    FileResponse obterRelatorioDepositos(RelatorioDepositoFiltroDTO filtro);

    FileResponse obterRelatorioDeDepositosFornecedorGraosEntregas(RelatorioDepositoFiltroDTO filtro);

    FileResponse obterRelatorioDeDepositosFornecedorGraosPagamento(RelatorioDepositoFiltroDTO filtro);

    FileResponse obterRelatorioDeLacresVinculadosARomaneio(RelatorioLacreFiltroDTO relatorioLacreFiltro);

    void validarChaveDeAcesso(String chave) throws Exception;

    Romaneio obterSimplesEntrada(String numero);

    byte[] imprimirPdf(Romaneio romaneio, Long id) throws JRException;

    List<RequestItemDTO> obterRequestsSemFaturas(List<Long> idsRomaneio);

    void updateDocumentoFaturamento(Long documentoFaturamento, String requestKey, String numeroRomaneio);

    void validarItensClassificacaoRestritos(Romaneio entidade, Usuario usuarioToken) throws ItensClassificacaoException;

    void validarEtapasDoRomaneioObrigatorias(Romaneio entidade) throws ProcessoNaoConcluidoException;

    void validarValorUnitarioPedidoENota(Romaneio entidade) throws DivergenciaPedidoNotaException;

    List<Long> listarIdDeRomaneiosIntegradosComSAP(Date dataUltimaSincronizacao);

    Romaneio obterRomaneioPorTicketOrigem(String ticket) throws Exception;

    Romaneio obterRomaneioPorNumeroNotaFiscal(String numero, Long idRomaneio) throws Exception;

    Romaneio obterRomaneioArmazemDeTerceiros(String numero) throws Exception;

    Romaneio obterRomaneioParaDevolucao(String numero, DadosSincronizacaoEnum operacao) throws Exception;

    Romaneio obterPorNumeroRomaneio(String numeroRomaneio);

    ObterEntidadeResponse obterEntidade(Long id);

    Romaneio obterRomaneioParaReferenciar(String numeroRomaneio);

    ObterRomaneioSemPesagemResponse obterRomaneioSemPesagem(String numero) throws Exception;

    ObterRomaneioEstornadoResponse obterRomaneioEstornado(String numero) throws Exception;

    Romaneio salvarRomaneio(Romaneio entidade);

    Romaneio salvarSemIntegracaoComSap(Romaneio entidade) throws Exception;

    Romaneio salvarComRetorno(Romaneio entidade) throws Exception;

    void enviarRomaneio(Romaneio entidade) throws Exception;

    SalvarRomaneioV2Response salvarRomaneioV2(Romaneio entidade, boolean validarEtapas, boolean salvarPesagem, boolean validarConversao) throws Exception;

    ObterOperacaoResponse obterOperacao(String operacao);

    ObterRomaneioPorSenhaResponse obterRomaneioPorSenha(Long senha);

    ObterRomaneioPorIdEEtapaResponse obterRomaneioPorIdEEtapa(Long id, EtapaEnum etapa);

    void atualizarSaldoReservadoPedido(Romaneio entidade);

    void inicializarLacreStatusLacre(Romaneio entidade);

    void validarSaldoOrdemDeVenda(Romaneio entidade) throws OrdemVendaSemSaldoException;

    void validarSaldoProdutoAcabado(Romaneio entidade);

    void validarEscrituracaoDeNota(String nota, String serie, Date dataNota, Long idProdutor, Long idSafra, Long idMaterial, Long idItem) throws Exception;

    void estornarRomaneioSemIntegracaoComAutomacao(Romaneio entidade);

    void estornar(Romaneio romaneio, boolean estornoEReferenciamento);

    void tornarPendente(Romaneio dto);

    void tornarEmProcessamento(Romaneio romaneio);

    List<Romaneio> listarRomaneiosComErroDeIntegracaoPorData(Date data);

    void atualizarStatusPedido(String romaneio, String pedido, StatusPedidoEnum statusPedido);

    Romaneio obterDadosRomaneioEstornado(DadosSincronizacaoEnum operacaoEntidade, String numero) throws ServicoException;

    ListaRomaneiosPendenteAprovacaoIndicesResponse listarRomaneiosPendenteAprovacaoIndices();

    byte[] imprimirPdfClassificacao(Long id) throws Exception;

    Arquivo imprimirPdfClassificacaoPorNumeroRomaneio(String numeroRomaneio) throws Exception;

    Arquivo imprimirPdfPorNumeroRomaneio(String numeroRomaneio) throws Exception;

    void enviarEmailRomaneioBloqueadoPorIndiceClassificacao(Romaneio romaneio);

    Pagina<ItemNFPedidoDTO> listarPaginadoItemNFPedido(Paginacao<ItemNFPedido> paginacao);

    void validacoesRomaneio(Romaneio entidade) throws Exception;

    Romaneio obterPorSenha(Senha senha);

    RomaneioDTO obterPorSenha(Long senhaId);

    void setarConversaoLitragem(Long idConversao, String numeroRomaneio);

    boolean existeItemNFPedidoPorTipoVeiculoSeta(Long tipoVeiculoSetaId);

    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    String obterMotivoEstorno(Long id);

    BigDecimal obterSaldoReservado(String pedido, String item);

    BigDecimal obterSaldoOrdemVendaBloqueado(String ordemVenda, String romaneio);

    boolean existeNumeroRomaneio(String numeroRomaneio);

    List<Cliente> listarClientesPorRomaneio(Long idRomaneio);

    List<RetornoNFe> listarNfePorRomaneio(Long idRomaneio);

    void validarSePlacaEstaSendoUtilizada(final Veiculo veiculo, final Long idRomaneio, final Long idSenha) throws ServicoException;

    void aprovarRomaneioComItensRestritos(RomaneioAprovacaoDocumento romaneioAprovacaoDocumento);

    void aprovarRomaneioComItensRestritos(Romaneio romaneio);

    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    List<Motivo> buscarMotivosPorId(Long id);

    List<RelatorioAnaliseConversaoDTO> obterRelatorioAnaliseConversao(RelatorioAnaliseConversaoFiltroDTO filtro);

    List<RelatorioLaudoSintetico> obterRelatorioLaudoSintetico(RelatorioLaudoFiltroDTO relatorioLaudoFiltro);

    List<RelatorioLaudoAnalitico> obterRelatorioLaudoAnalitico(RelatorioLaudoFiltroDTO relatorioLaudoFiltro);

    List<RomaneioGestaoItssAgroDTO> listarRomaneioGestaoItssAgroPorData(final Date dataInicial, Date dataFinal);

    Pagina<RomaneioClassificacaoListagemDTO> listarClassificacaoDTO(Paginacao<Romaneio> paginacao);

    Pagina<RomaneioPesagemListagemDTO> listarPesagemDTO(Paginacao<Romaneio> paginacao);

    Pagina<RomaneioLacreListagemDTO> listarLacreDTO(Paginacao paginacao);

}
