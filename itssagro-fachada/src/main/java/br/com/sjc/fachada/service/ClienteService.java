package br.com.sjc.fachada.service;

import br.com.sjc.modelo.sap.Cliente;
import br.com.sjc.modelo.sap.response.item.RfcClienteResponseItem;

import java.util.List;

/**
 * Created by julio.bueno on 27/06/2019.
 */
public interface ClienteService extends Servico<Long, Cliente> {

    void salvarSAP(List<RfcClienteResponseItem> retorno);

    List<Cliente> buscarClientesAutoComplete(String value);

    Cliente obterPorCodigo(String codigoCliente);
}
