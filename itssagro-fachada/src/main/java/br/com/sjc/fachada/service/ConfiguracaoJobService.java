package br.com.sjc.fachada.service;

import br.com.sjc.modelo.cfg.ConfiguracaoJob;

public interface ConfiguracaoJobService extends Servico<Long, ConfiguracaoJob> {

    ConfiguracaoJob obterPorNome(String nome);
}
