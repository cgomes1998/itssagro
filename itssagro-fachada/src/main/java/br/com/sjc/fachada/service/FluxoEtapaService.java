package br.com.sjc.fachada.service;

import br.com.sjc.modelo.Fluxo;
import br.com.sjc.modelo.FluxoEtapa;
import br.com.sjc.modelo.TipoFluxoEnum;
import br.com.sjc.modelo.dto.EtapaCampoDTO;
import br.com.sjc.modelo.dto.FluxoEtapaDTO;
import br.com.sjc.modelo.dto.ProximoFluxoEtapaDTO;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.sap.Centro;
import br.com.sjc.modelo.sap.Material;

import java.util.List;

public interface FluxoEtapaService extends Servico<Long, FluxoEtapa> {

    List<EtapaCampoDTO> listarCampos(EtapaEnum etapa);

    FluxoEtapaDTO obterPorCentroEtapaMaterial(Centro centro, EtapaEnum etapa, String codigoMaterial, String descricaoMaterial, TipoFluxoEnum tipoFluxo);

    List<FluxoEtapaDTO> listarDtoPorFluxo(List<Long> idFluxos);

    List<FluxoEtapa> listarPorFluxo(Long fluxoId);

    FluxoEtapa obterFluxoEtapaPorMaterialEEtapa(TipoFluxoEnum tipoFluxo, Material material, EtapaEnum etapaEnum);

    List<ProximoFluxoEtapaDTO> listarProximasEtapasDoFluxo(Fluxo fluxo);

}
