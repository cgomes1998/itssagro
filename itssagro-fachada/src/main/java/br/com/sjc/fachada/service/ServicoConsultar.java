package br.com.sjc.fachada.service;

import br.com.sjc.fachada.dao.DAOConsultar;
import br.com.sjc.fachada.rest.paginacao.Pagina;
import br.com.sjc.fachada.rest.paginacao.Paginacao;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Selection;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public interface ServicoConsultar<ID, O> {

    O get(Serializable id);

    O get(String atributo, Object valor);

    O get(Specification<O> specification);

    List<O> listar();

    List<O> listar(Specification<O> specification);

    List<O> listar(List<Selection<?>> selections, Specification<O> specification);

    Pagina<O> listar(Paginacao paginacao);

    <T>List<T> dtoListar();

    Pagina<?> dtoListar(Paginacao paginacao);

    <D> Collection<D> dtoListar(Specification specification, Class<D> dtoClass);

    <D> Pagina<D> dtoListar(Paginacao paginacao, Class<D> dtoClass);

    long contar(Paginacao paginacao);

    long contar(Paginacao paginacao, Class<?> dtoClass);

    boolean existe(Specification<O> specification);

    <T> T getValorAtributo(String atributo, Serializable id);

    <T> T getValorAtributo(String atributo, Specification<O> specification);

    Specification<O> obterEspecification(Paginacao<O> paginacao);

}
