package br.com.sjc.fachada.service;

import br.com.sjc.modelo.cfg.Ambiente;
import br.com.sjc.modelo.cfg.AmbienteSincronizacao;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoCallerService;

public interface AmbienteSincronizacaoService extends Servico<Long, AmbienteSincronizacao> {

    AmbienteSincronizacao obterPorDadosSincronizacao(DadosSincronizacao dadosSincronizacao);

    void updateRequestCount(DadosSincronizacao dadosSincronizacao, JCoCallerService sap);

    void updateRequestCount(AmbienteSincronizacao sincronizacao);

    AmbienteSincronizacao obterAmbienteSincronizacao(DadosSincronizacao dadosSincronizacao, JCoCallerService sap);

    AmbienteSincronizacao obterPorAmbienteESincronizacao(DadosSincronizacao dadosSincronizacao, Ambiente ambiente);
}
