package br.com.sjc.fachada.service;

import br.com.sjc.modelo.endpoint.request.item.TransportadoraRequest;
import br.com.sjc.modelo.endpoint.response.item.TransportadoraResponse;
import br.com.sjc.modelo.sap.Transportadora;
import br.com.sjc.modelo.sap.request.EventOutRequest;

import java.util.Date;
import java.util.List;

public interface TransportadoraService extends Servico<Long, Transportadora> {

    void salvarSAP(EventOutRequest request, String host, String mandante);

    List<Transportadora> listarSincronizados(final Date data);

    void sincronizar(List<Transportadora> entidades);

    List<Transportadora> buscarTransportadorasAutoComplete(String value);

    List<TransportadoraResponse> listarParaArmazemDeTerceiros(List<TransportadoraRequest> requests);

    Transportadora obterPorCodigo(String codigo);
}
