package br.com.sjc.fachada.service;


import br.com.sjc.modelo.Balanca;
import br.com.sjc.modelo.dto.CapturaBalanca;

import java.util.List;


public interface BalancaService extends Servico<Long, Balanca>{
    List<Balanca> obterBalancasPorDirecaoETipoPeso(CapturaBalanca entidade);
}
