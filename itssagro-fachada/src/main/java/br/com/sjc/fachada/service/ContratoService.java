package br.com.sjc.fachada.service;

import br.com.sjc.modelo.sap.CondicaoPagamento;
import br.com.sjc.modelo.sap.GrupoCompradores;
import br.com.sjc.modelo.sap.OrganizacaoCompra;
import br.com.sjc.modelo.sap.TipoContrato;

import java.util.List;

public interface ContratoService extends Servico<Long, TipoContrato> {

    List<TipoContrato> listarTipoContratos();

    List<CondicaoPagamento> listarCondicoesPagamento();

    List<GrupoCompradores> listarGrupoCompradores();

    List<OrganizacaoCompra> listarOrganizacaoCompras();

    void salvarTipoContrato(List<TipoContrato> tipoContratos);

    void salvarCondicaoPagamento(List<CondicaoPagamento> entidades);

    void salvarGrupoCompradores(List<GrupoCompradores> entidades);

    void salvarOrganizacaoCompras(List<OrganizacaoCompra> entidades);
}
