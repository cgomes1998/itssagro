package br.com.sjc.fachada.service;

import br.com.sjc.modelo.cfg.Portaria;
import br.com.sjc.modelo.dto.PortariaDTO;

import java.util.List;

public interface PortariaService extends Servico<Long, Portaria> {
    List<PortariaDTO> listarPortariasAtivas();
}
