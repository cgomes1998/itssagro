package br.com.sjc.fachada.service;

import br.com.sjc.modelo.endpoint.request.item.VeiculoRequest;
import br.com.sjc.modelo.endpoint.response.item.VeiculoResponse;
import br.com.sjc.modelo.sap.Veiculo;
import br.com.sjc.modelo.sap.request.EventOutRequest;
import br.com.sjc.modelo.sap.response.item.RfcVeiculoResponseItem;

import java.util.Date;
import java.util.List;

public interface VeiculoService extends Servico<Long, Veiculo> {

    List<VeiculoResponse> listarParaArmazemTerceiros(List<VeiculoRequest> requests);

    void salvarSAP(List<RfcVeiculoResponseItem> retorno);

    void salvarSAP(EventOutRequest request, String host, String mandante);

    void sincronizar(List<Veiculo> entidades);

    List<Veiculo> listarPreCadastro();

    List<Veiculo> listarSincronizados(final Date data);

    Veiculo obterPorCodigo(String codigo);

    List<Veiculo> buscarVeiculosAutoComplete(String value);

    List<Veiculo> buscarVeiculosAtivosAutoComplete(String value);
}