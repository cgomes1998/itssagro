package br.com.sjc.fachada.excecoes;

public class PersistenciaException extends EndpointException {

	private static final long serialVersionUID = 1L;

	public PersistenciaException(final String mensagem, final Object... argumentos) {

        super(mensagem, argumentos);
    }
}