package br.com.sjc.fachada.modelo;


import br.com.sjc.util.email.ConfiguracaoEmailProtocolo;
import org.springframework.util.StringUtils;

import java.util.Objects;

public interface EmailSenderConfiguration {

    String getHost();
    String getPorta();
    String getRemetente();
    String getUsuario();
    String getSenha();
    String getUrlSistema();
    Boolean getAmbienteDev();
    ConfiguracaoEmailProtocolo getProtocolo();

    default boolean possuiTodasInformacoess(){
        return !StringUtils.isEmpty(getHost())
                && !StringUtils.isEmpty(getRemetente())
                && !StringUtils.isEmpty(getUsuario())
                && !Objects.isNull(getAmbienteDev())
                && !Objects.isNull(getProtocolo());
    }

    default boolean isProtocoloSSL(){
        return ConfiguracaoEmailProtocolo.SSL == getProtocolo();
    }

}
