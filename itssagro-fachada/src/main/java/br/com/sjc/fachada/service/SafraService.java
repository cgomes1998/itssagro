package br.com.sjc.fachada.service;

import br.com.sjc.modelo.sap.Safra;
import br.com.sjc.modelo.sap.request.EventOutRequest;

import java.util.Date;
import java.util.List;

public interface SafraService extends Servico<Long, Safra> {

    void salvarSAP(EventOutRequest request, String host, String mandante);

    List<Safra> listarSincronizados(final Date data);

    void sincronizar(List<Safra> entidades);

    List<Safra> buscarSafrasAutoComplete(String value);

    Safra obterPorCodigo(String codigo);
}
