package br.com.sjc.fachada.service;

import br.com.sjc.modelo.FluxoEtapa;
import br.com.sjc.modelo.Motivo;
import br.com.sjc.modelo.Senha;
import br.com.sjc.modelo.SenhaFormulario;
import br.com.sjc.modelo.dto.*;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.enums.StatusSenhaEnum;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.modelo.sap.Romaneio;
import net.sf.jasperreports.engine.JRException;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface SenhaService extends Servico<Long, Senha> {

    void gerar(Senha senha);

    void adicionarEtapa(Senha senha, FluxoEtapa etapa);

    byte[] imprimirPdf(Long idSenha) throws JRException;

    void alterarStatusSenha(Senha senha);

    Senha obterPorSenha(String senha);

    void enviarEmailLiberacao(Senha senha);

    Senha obterUltimaSenhaChamada();

    List<PainelSenhaDTO> obterUltimasSenhasChamadas(Long idSenha);

    Senha salvarFormulario(SenhaFormulario entidade);

    void executarCancelamentoAutomatico();

    List<TempoPorEtapaDTO> obterTempoMedioPorEtapaESenhas(List<Long> ids, List<FluxoEtapa> etapas);

    List<TempoPorEtapaDTO> obterTempoMedioPorEtapaEnumESenhas(List<Long> ids, List<EtapaEnum> etapas);

    boolean existeSenhaPorTipoVeiculoSeta(Long tipoVeiculoSetaId);

    Senha obterPorIdAgendamentoOuPlaca(ConsultaSenhaMotoristaEntrada idAgendamento);

    List<Senha> buscarSenhasAutoComplete(final String value);

    List<SenhaDTO> listarPorMaterialEPlacaENumeroRomaneioENumeroCartaoAcesso(
            List<Material> materiais,
            String placa,
            String numeroRomaneio,
            String numeroCartaoAcesso,
            Date data,
            List<FluxoEtapaDTO> fluxoEtapas,
            List<StatusSenhaEnum> statusSenha
    );

    @Transactional(readOnly = true)
    List<Motivo> obterMotivoPorId(Long id);

    Optional<Senha> obterPorPlacaENumeroCartaoAcessoEEtapaContem(String placa, Integer nrCartao, EtapaEnum... etapas);

    boolean existsByVeiculoIdAndStatusSenhaNotInAndIdNot(Long id, List<StatusSenhaEnum> asList, Long idSenha);

    boolean existsByVeiculoIdAndStatusSenhaNotIn(Long id, List<StatusSenhaEnum> asList);

    void estornarRomaneioNaAutomacao(Romaneio romaneio);

}
