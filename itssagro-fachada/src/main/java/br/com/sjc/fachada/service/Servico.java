package br.com.sjc.fachada.service;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Usuario;

import java.util.List;

public interface Servico<ID, E> extends ServicoConsultar<ID, E> {

    E salvar(E entidade);

    void salvar(List<E> lista);

    void alterarStatus(E entidade);

    void validarInativacao(E entidade);

    void excluir(E entidade);

    void excluirPorId(ID id);

    DAO<ID, E> getDAO();

    Usuario getUsuarioLogado();

    void setUsuarioLogado(final Usuario usuarioLogado);

    void alterarAtributos(E entidade, String... atributos);

}
