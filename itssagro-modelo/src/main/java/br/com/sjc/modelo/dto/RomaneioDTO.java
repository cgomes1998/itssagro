package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.AnaliseQualidade;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusPedidoEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.sap.LocalCarregamento;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class RomaneioDTO extends DataDTO {

    private String produtor;

    private String cliente;

    private String placaCavalo;

    private String material;

    private String numeroRomaneio;

    private String ticketOrigem;

    private String numeroNfeESerie;

    private String stNfe;

    private String numeroPedido;

    private String motorista;

    private String transportadora;

    private Long idUsuarioAprovador;

    private DadosSincronizacao operacao;

    private StatusIntegracaoSAPEnum statusIntegracaoSAP;

    private StatusRomaneioEnum statusRomaneio;

    private StatusPedidoEnum statusPedido;

    private boolean maisDeUmCliente;

    private boolean maisDeUmProdutor;

    private boolean maisDeUmaNota;

    private boolean classificacaoRealizada;

    private BigDecimal pesoBruto;

    private BigDecimal pesoTara;

    private BigDecimal totalDescontos;

    private String botaoAcionadoConfirmacaoPeso;

    private LocalCarregamento localCarregamento;

    private List<AnaliseQualidade> analises;

    private List<ItemNFPedidoDTO> itens;

    // Dados CTE
    private BigDecimal valorTotalCte;

    private String ufEmissoraCte;

    private String ufTomadoraCte;

    private String pedidoCte;

    private String itemPedidoCte;

    private String ivaCte;

    private String parceiroCte;

    private String chaveAcessoCte;

    private String numeroCte;

    private String serieCte;

    private String logCte;

    private String numeroAleatorioCte;

    private String digitoVerificadorCte;

    private Date dataCte;

    public RomaneioDTO() {
        super();
    }

    public RomaneioDTO(Long id) {

        this.setId(id);
    }

    public RomaneioDTO(Romaneio romaneio) {
        setId(romaneio.getId());
        setNumeroRomaneio(romaneio.getNumeroRomaneio());
        setStatusRomaneio(romaneio.getStatusRomaneio());
    }

    public RomaneioDTO(
            Long id,
            String numeroRomaneio,
            Date dataCadastro,
            DadosSincronizacao operacao,
            String codigoProdutorCabecalho,
            String descricaoProdutorCabecalho,
            String codigoProdutorItem,
            String descricaoProdutorItem,
            String codigoCliente,
            String descricaoCliente,
            String placaCavalo,
            String material,
            StatusRomaneioEnum statusRomaneio,
            StatusIntegracaoSAPEnum statusIntegracaoSAP,
            String numeroNfeESerie,
            String serie,
            String stNfe,
            String ticketOrigem,
            String numeroPedido,
            StatusPedidoEnum statusPedido,
            Long totalCliente,
            Long totalProdutor,
            Long totalNota,
            Long idClassificacao,
            BigDecimal pesoBruto,
            BigDecimal pesoTara,
            BigDecimal totalDescontos,
            String motorista,
            String transportadora
    ) {

        this.setId(id);

        this.setNumeroRomaneio(numeroRomaneio);

        this.setDataCadastro(dataCadastro);

        this.setOperacao(operacao);

        this.setStatusRomaneio(statusRomaneio);

        this.setStatusIntegracaoSAP(statusIntegracaoSAP);

        if (StringUtil.isNotNullEmpty(codigoProdutorCabecalho)) {
            this.setProdutor(MessageFormat.format("{0} - {1}", codigoProdutorCabecalho, descricaoProdutorCabecalho));
        } else if (StringUtil.isNotNullEmpty(codigoProdutorItem)) {
            this.setProdutor(MessageFormat.format("{0} - {1}", codigoProdutorItem, descricaoProdutorItem));
        }

        this.setCliente(MessageFormat.format("{0} - {1}", codigoCliente, descricaoCliente));

        if (StringUtil.isNotNullEmpty(placaCavalo)) {

            this.setPlacaCavalo(StringUtil.toPlaca(placaCavalo));
        }

        this.setMaterial(material);

        if (StringUtil.isNotNullEmpty(numeroNfeESerie)) {

            this.setNumeroNfeESerie(numeroNfeESerie.concat(StringUtil.isNotNullEmpty(serie) ? String.format(" %s", serie) : StringUtil.empty()));

            this.setStNfe(stNfe);
        }

        this.setTicketOrigem(ticketOrigem);

        this.setNumeroPedido(numeroPedido);

        this.setStatusPedido(statusPedido);

        this.setMaisDeUmCliente(totalCliente > 1);

        this.setMaisDeUmProdutor(totalProdutor > 1);

        this.setMaisDeUmaNota(totalNota > 1);

        this.setClassificacaoRealizada(ObjetoUtil.isNotNull(idClassificacao) && idClassificacao > 0);

        this.setPesoBruto(pesoBruto);

        this.setPesoTara(pesoTara);

        this.setTotalDescontos(totalDescontos);

        this.setMotorista(motorista);

        this.setTransportadora(transportadora);
    }

    public RomaneioDTO(Long id, String numeroRomaneio, StatusRomaneioEnum statusRomaneio) {
        setId(id);
        setNumeroRomaneio(numeroRomaneio);
        setStatusRomaneio(statusRomaneio);
    }

    public BigDecimal getPesoLiquido() {

        try {

            if (ObjetoUtil.isNull(this.getPesoBruto()) || ObjetoUtil.isNull(this.getPesoTara())) {
                throw new Exception();
            }

            return new BigDecimal(this.getPesoBruto().doubleValue() - this.getPesoTara().doubleValue());

        } catch (Exception e) {

            return BigDecimal.ZERO;
        }
    }

    public BigDecimal getPesoLiquidoSeco() {

        try {

            if (ObjetoUtil.isNull(this.getTotalDescontos())) {
                throw new Exception();
            }

            return new BigDecimal(this.getPesoLiquido().doubleValue() - this.getTotalDescontos().doubleValue());

        } catch (Exception e) {

            return BigDecimal.ZERO;
        }
    }

    public String getStatusRomaneioDescricao() {

        return ObjetoUtil.isNotNull(this.getStatusRomaneio()) ? this.getStatusRomaneio().getDescricao() : "***";
    }

    public String getStatusSapDescricao() {

        return ObjetoUtil.isNotNull(this.getStatusIntegracaoSAP()) ? this.getStatusIntegracaoSAP().getDescricao() : "***";
    }

    public String getStatusPedidoDescricao() {

        return ObjetoUtil.isNotNull(this.getStatusPedido()) ? this.getStatusPedido().getDescricao() : "***";
    }
}
