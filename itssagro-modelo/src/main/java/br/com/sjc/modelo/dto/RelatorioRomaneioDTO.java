package br.com.sjc.modelo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RelatorioRomaneioDTO implements Cloneable {

    private Date dataPortaria;

    private Date dataCriacao;

    private Date dataSaida;

    private String numero;

    private String numeroPedido;

    private String numeroNotaFiscalTransporte;

    private String serieNotaFiscalTransporte;

    private String produtor;

    private String cpfCnpj;

    private String endereco;

    private String operacao;

    private String material;

    private String transportadora;

    private String statusRomaneio;

    private String safra;

    private String centroDestino;

    private String centroOrigem;

    private String motorista;

    private String veiculo;

    private String observacao;

    private String observacaoNota;

    private String deposito;

    private String usuario;

    private String fazenda;

    private String inscricaoEstadual;

    private String pesoBruto;

    private String pesoTara;

    private String pesoUmido;

    private String pesoLiquidoSeco;

    private String descontos;

    private String descontosOrigem;

    private String pesoOrigemTara;

    private String pesoOrigemBruto;

    private String pesoOrigemUmido;

    private String pesoOrigemSeco;

    private String lacres;

    private String nfes;

    private boolean possuiDadosNfe;

    private boolean possuiDivisaoCarga;

    private boolean possuiClassificacaoOrigem;

    private boolean possuiClassificacaoDestino;

    private boolean possuiLacragem;

    private boolean possuiConversao;

    private boolean possuiAnalise;

    private ConversaoLitragemDTO conversao;

    private List<AnaliseQualidadeDTO> analises;

    private List<RelatorioClassificacaoDTO> classificacaoOrigem;

    private List<RelatorioClassificacaoDTO> classificacaoDestino;

    private List<DivisaoCargaDTO> divisoesCarga;

    private List<ItemOrdemVendaDTO> itensOrdemVenda;

    private String numeroCartaoAcesso;

    public RelatorioRomaneioDTO getClone () {

        try {

            return ( RelatorioRomaneioDTO ) super.clone ();

        } catch ( CloneNotSupportedException e ) {

            return this;
        }
    }
}
