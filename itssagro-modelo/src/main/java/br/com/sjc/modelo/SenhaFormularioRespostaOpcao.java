package br.com.sjc.modelo;

import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_senha_formulario_resposta_opcao", schema = "public")
public class SenhaFormularioRespostaOpcao extends EntidadeAutenticada {

    private static final long serialVersionUID = 1L;

    @EntityProperties(value = {"id"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_senha_formulario_resposta", nullable = false)
    @JsonIgnore
    private SenhaFormularioResposta senhaFormularioResposta;

    @EntityProperties(value = {"id", "formularioItem.id", "titulo", "ordem"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_formulario_item_opcao", nullable = false)
    private FormularioItemOpcao formularioItemOpcao;

    @Transient
    private boolean marcado = false;

}
