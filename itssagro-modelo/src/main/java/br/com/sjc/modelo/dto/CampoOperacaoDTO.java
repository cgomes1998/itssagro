package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.cfg.Campo;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CampoOperacaoDTO extends DataDTO {

    @ProjectionProperty
    private DadosSincronizacaoEnum operacao;

    @ProjectionProperty
    private boolean obrigatorio;

    @ProjectionProperty
    private boolean somenteLeitura;

    @ProjectionProperty
    private boolean divisaoCarga;

    @ProjectionProperty
    private boolean permitirValorDefault;

    @ProjectionProperty
    private boolean valorDefaultEEnum;

    @ProjectionProperty
    private String display;

    @ProjectionProperty
    private String valorDefault;

    @ProjectionProperty(values = {"id", "descricao"})
    private Campo campo;

    public CampoOperacaoDTO(boolean obrigatorio, boolean somenteLeitura, boolean divisaoCarga, boolean permitirValorDefault, boolean valorDefaultEEnum, String display, String valorDefault, String nomeVariavel) {

        this.setObrigatorio(obrigatorio);

        this.setSomenteLeitura(somenteLeitura);

        this.setDivisaoCarga(divisaoCarga);

        this.setPermitirValorDefault(permitirValorDefault);

        this.setValorDefaultEEnum(valorDefaultEEnum);

        this.setDisplay(display);

        this.setValorDefault(valorDefault);

        this.setCampo(new Campo());

        this.getCampo().setDescricao(nomeVariavel);
    }

}
