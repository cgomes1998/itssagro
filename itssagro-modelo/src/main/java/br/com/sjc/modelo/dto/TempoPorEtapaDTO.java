package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.util.ObjetoUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class TempoPorEtapaDTO {

    private Long idSenha;

    private Long sequencia;

    private EtapaEnum etapa;

    private Long duracao;

    private Date horaSaida;

    public TempoPorEtapaDTO(
            Long idSenha,
            Long sequencia,
            EtapaEnum etapa,
            Date horaEntrada,
            Date horaSaida
    ) {

        this.setIdSenha(idSenha);

        this.setSequencia(sequencia);

        this.setEtapa(etapa);

        this.setHoraSaida(horaSaida);

        this.setDuracao((ObjetoUtil.isNotNull(horaSaida) ? horaSaida.getTime() : new Date().getTime()) - horaEntrada.getTime());
    }
}
