package br.com.sjc.modelo.dto.aprovacaoDocumento;

import br.com.sjc.util.anotation.ProjectionConfigurationDTO;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ProjectionConfigurationDTO
public class UsuarioAprovacaoDocumento {

    @ProjectionProperty
    private Long id;

    @ProjectionProperty
    private String login;

    @ProjectionProperty
    private String nome;

}
