package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.Fluxo;
import br.com.sjc.modelo.FluxoEtapa;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.sap.Material;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DashboardGerencialFiltroEtapaDTO {

    private Fluxo fluxo;

    private List<Material> materiais;

    private List<FluxoEtapa> etapas;

}
