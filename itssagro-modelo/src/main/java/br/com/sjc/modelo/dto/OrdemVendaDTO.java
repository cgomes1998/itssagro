package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.TipoFreteEnum;
import br.com.sjc.modelo.sap.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrdemVendaDTO {

    private Cliente cliente;

    private Cliente operadorLogistica;

    private Transportadora transportadora;

    private Transportadora transportadoraSub;

    private Deposito deposito;

    private Material material;

    private Centro centro;

    private TipoFreteEnum tipoFrete;

    private String numeroItem;

    private String empresa;

    private String localExpedicao;

    private String unidadeMedidaOrdem;

    private String numeroPedido;

    private BigDecimal saldo;

    private BigDecimal saldoOrdemVendaBloqueado;
}
