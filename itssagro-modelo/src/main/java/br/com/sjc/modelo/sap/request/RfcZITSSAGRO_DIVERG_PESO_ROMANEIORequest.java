package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.*;
import br.com.sjc.modelo.sap.arquitetura.jco.*;
import br.com.sjc.modelo.sap.request.item.RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_FORNECEDOR_Item;
import br.com.sjc.modelo.sap.request.item.RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_MATERIAL_Item;
import br.com.sjc.modelo.sap.request.item.StageDataItem;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@SAPParameter(function = "ZITSSAGRO_DIVERG_PESO_ROMANEIO")
public class RfcZITSSAGRO_DIVERG_PESO_ROMANEIORequest extends JCoRequest {

    @SAPColumn(name = "I_CENTRO")
    private String centro;

    @SAPColumn(name = "I_DEPOSITO")
    private String deposito;

    @SAPColumn(name = "I_SAFRA")
    private String safra;

    @SAPColumn(name = "I_DATA_PEDIDO_DE")
    private String dataPedidoDe;

    @SAPColumn(name = "I_DATA_PEDIDO_ATE")
    private String dataPedidoAte;

    @SAPColumn(name = "I_DATA_VENCTO_DE")
    private String dataVencimentoDe;

    @SAPColumn(name = "I_DATA_VENCTO_ATE")
    private String dataVencimentoAte;

    @SAPColumn(name = "I_DATA_NF_DE")
    private String dataNfeDe;

    @SAPColumn(name = "I_DATA_NF_ATE")
    private String dataNfeAte;

    @SAPTable( name = "T_FORNECEDOR", item = RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_FORNECEDOR_Item.class )
    private List<RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_FORNECEDOR_Item> t_fornecedor = new ArrayList<>();

    @SAPTable( name = "T_MATERIAL", item = RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_MATERIAL_Item.class )
    private List<RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_MATERIAL_Item> t_material = new ArrayList<>();

    @Builder(builderMethodName = "builderRfcZitssAgroDiverPesoRomaneioRequest")
    public RfcZITSSAGRO_DIVERG_PESO_ROMANEIORequest(Centro centro, Deposito deposito, Safra safra, Produtor fornecedor, Material material, Date dataPedidoDe, Date dataPedidoAte, Date dataVencimentoDe, Date dataVencimentoAte, Date dataNfeDe, Date dataNfeAte) {

        if (ObjetoUtil.isNotNull(centro)) {
            setCentro(StringUtil.completarZeroAEsquerda(4, centro.getCodigo()));
        }

        if (ObjetoUtil.isNotNull(deposito)) {
            setDeposito(StringUtil.completarZeroAEsquerda(4, deposito.getCodigo()));
        }

        if (ObjetoUtil.isNotNull(safra)) {
            setSafra(safra.getCodigo());
        }

        if (ObjetoUtil.isNotNull(fornecedor)) {
            setT_fornecedor(new ArrayList<>());
            RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_FORNECEDOR_Item item = new RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_FORNECEDOR_Item();
            item.setLifnr(StringUtil.completarZeroAEsquerda(10, fornecedor.getCodigo()));
            getT_fornecedor().add(item);
        }

        if (ObjetoUtil.isNotNull(material)) {
            setT_material(new ArrayList<>());
            RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_MATERIAL_Item item = new RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_MATERIAL_Item();
            item.setMatnr(StringUtil.completarZeroAEsquerda(18, material.getCodigo()));
            getT_material().add(item);
        }

        if (ObjetoUtil.isNotNull(dataPedidoDe)) {
            setDataPedidoAte(DateUtil.formatToSAP(dataPedidoDe));
        }

        if (ObjetoUtil.isNotNull(dataPedidoAte)) {
            setDataPedidoAte(DateUtil.formatToSAP(dataPedidoAte));
        }

        if (ObjetoUtil.isNotNull(dataVencimentoDe)) {
            setDataVencimentoDe(DateUtil.formatToSAP(dataVencimentoDe));
        }

        if (ObjetoUtil.isNotNull(dataVencimentoAte)) {
            setDataVencimentoAte(DateUtil.formatToSAP(dataVencimentoAte));
        }

        if (ObjetoUtil.isNotNull(dataNfeDe)) {
            setDataNfeDe(DateUtil.formatToSAP(dataNfeDe));
        }

        if (ObjetoUtil.isNotNull(dataNfeAte)) {
            setDataNfeAte(DateUtil.formatToSAP(dataNfeAte));
        }
    }

    @Override
    public int getTableRows ( String name ) {

        final Field[] fields = this.getClass ().getDeclaredFields ();
        List<?> table = Arrays.asList ( fields ).stream ().filter (f -> ObjetoUtil.isNotNull ( f.getAnnotation ( SAPTable.class ) ) && f.getAnnotation ( SAPTable.class ).name ().equals ( name ) ).map (f -> {
            try {
                return f.get ( this );
            } catch ( IllegalAccessException e ) {
                e.printStackTrace ();
            }
            return null;
        } ).collect ( Collectors.toList () );

        return ( ( List<?> ) table.get ( 0 ) ).size ();
    }

    @Override
    public List<?> getTableItens ( String name ) {

        final Field[] fields = this.getClass ().getDeclaredFields ();
        List<?> table = Arrays.asList ( fields ).stream ().filter ( f -> ObjetoUtil.isNotNull ( f.getAnnotation ( SAPTable.class ) ) && f.getAnnotation ( SAPTable.class ).name ().equals ( name ) ).map ( f -> {
            try {
                return f.get ( this );
            } catch ( IllegalAccessException e ) {
                e.printStackTrace ();
            }
            return null;
        } ).collect ( Collectors.toList () );
        return ( List<?> ) table.get ( 0 );
    }

    @Override
    public int getStructureRows ( String name ) {

        return 1;
    }

    @Override
    public List<?> getStructureItens (
            String name
    ) {

        final Field[] fields = this.getClass ().getDeclaredFields ();
        return Arrays.asList ( fields ).stream ().filter ( f -> ObjetoUtil.isNotNull ( f.getAnnotation ( SAPStructure.class ) ) && f.getAnnotation ( SAPStructure.class ).name ().equals ( name ) ).map (f -> {
            try {
                return f.get ( this );
            } catch ( IllegalAccessException e ) {
                e.printStackTrace ();
            }
            return null;
        } ).collect ( Collectors.toList () );
    }
}
