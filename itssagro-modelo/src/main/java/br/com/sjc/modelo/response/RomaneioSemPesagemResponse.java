package br.com.sjc.modelo.response;

import br.com.sjc.modelo.AnaliseQualidade;
import br.com.sjc.modelo.Carregamento;
import br.com.sjc.modelo.Motivo;
import br.com.sjc.modelo.Usuario;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEtapaEnum;
import br.com.sjc.modelo.sap.Centro;
import br.com.sjc.modelo.sap.ConversaoLitragem;
import br.com.sjc.modelo.sap.ItemNFPedido;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
public class RomaneioSemPesagemResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private Centro centro;
    private Usuario usuarioAprovador;
    private ConversaoLitragem conversaoLitragem;
    private String observacao;
    private String observacaoNota;
    private List<ItemNFPedido> itens;
    private List<AnaliseQualidade> analises;
    private List<Motivo> motivos;
    private Integer numeroCartaoAcesso;
    private Carregamento carregamento;
    private DadosSincronizacao operacao;
    private StatusRomaneioEnum statusRomaneio;
    private StatusRomaneioEtapaEnum statusEtapa;
}
