package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.response.item.RfcMotoristaMensagemItem;
import br.com.sjc.modelo.sap.response.item.RfcMotoristaResponseItem;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@SAPParameter(function = "ZITSSAGRO_FM_DM_MOTORISTA")
public class RfcMotoristaResponse extends JCoResponse {

    @SAPTable(name = "MOTORISTA_RET", item = RfcMotoristaResponseItem.class)
    private List<RfcMotoristaResponseItem> retorno;

    @SAPTable(name = "MENSAGENS", item = RfcMotoristaMensagemItem.class)
    private List<RfcMotoristaMensagemItem> mensagens;
}
