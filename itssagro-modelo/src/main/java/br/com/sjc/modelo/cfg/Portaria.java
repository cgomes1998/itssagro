package br.com.sjc.modelo.cfg;

import br.com.sjc.modelo.EntidadeAutenticada;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Time;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_portaria", schema = "cfg")
public class Portaria extends EntidadeAutenticada {
    private static final long serialVersionUID = 1L;

    @Column(name = "sigla", length = 3, nullable = false)
    private String sigla;

    @Column(name = "descricao", length = 20, nullable = false)
    private String descricao;

    @Column(name = "ip_impressora", length = 20, nullable = false)
    private String ipImpressora;

    @Column(name = "porta_impressora", length = 10, nullable = false)
    private String portaImpressora;

    @Column(name = "hora_inicio_marcacao")
    private Time horaInicioMarcacao;

    @Column(name = "hora_fim_marcacao")
    private Time horaFimMarcacao;

    @Column(name = "validar_horario_marcacao", nullable = false, columnDefinition = "boolean default false")
    private boolean validarHorarioMarcacao;

    @Column(name = "ativo", nullable = false, columnDefinition = "boolean default false")
    private boolean ativo;
}
