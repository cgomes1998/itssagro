package br.com.sjc.modelo.dto;


import br.com.sjc.util.BigDecimalUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class RelatorioLaudoSintetico {

    private String ordemVenda;

    private Long idMaterial;

    private Long idRomaneio;

    private Long idAnalise;

    private Long idLaudo;

    private String codigoMaterial;

    private String material;

    private boolean buscarCertificadoQualidade;

    private String codigoCliente;

    private String cliente;

    private String numeroRomaneio;

    private String placaVeiculo;

    private BigDecimal pesoTara;

    private BigDecimal pesoBruto;

    private BigDecimal pesoLiquido;

    private String numeroNfe;

    private String unidadeMedida;

    private Date dataNfe;

    private Date dataCadastro;

    private String lote;

    private String lacre;

    private String certificado;

    public RelatorioLaudoSintetico(
            Long idRomaneio,
            String ordemVenda,
            Long idMaterial,
            String codigoMaterial,
            String material,
            boolean buscarCertificadoQualidade,
            String codigoCliente,
            String cliente,
            String numeroRomaneio,
            String placaVeiculo,
            BigDecimal pesoTara,
            BigDecimal pesoBruto,
            String numeroNfe,
            String unidadeMedida,
            Date dataNfe,
            Long idAnalise,
            String lote,
            String lacreAmostra,
            Long idLaudo
    ) {

        setIdRomaneio(ObjetoUtil.isNotNull(idRomaneio) ? idRomaneio : 0l);

        setOrdemVenda(ObjetoUtil.isNotNull(ordemVenda) ? ordemVenda : StringUtil.empty());

        setIdMaterial(ObjetoUtil.isNotNull(idMaterial) ? idMaterial : 0l);

        setCodigoMaterial(ObjetoUtil.isNotNull(codigoMaterial) ? codigoMaterial : StringUtil.empty());

        setMaterial(ObjetoUtil.isNotNull(material) ? material : StringUtil.empty());

        setBuscarCertificadoQualidade(buscarCertificadoQualidade);

        setCliente(ObjetoUtil.isNotNull(codigoCliente) ? codigoCliente : "00");

        setCliente(ObjetoUtil.isNotNull(cliente) ? cliente : StringUtil.empty());

        setNumeroRomaneio(ObjetoUtil.isNotNull(numeroRomaneio) ? numeroRomaneio : StringUtil.empty());

        setPlacaVeiculo(ObjetoUtil.isNotNull(placaVeiculo) ? placaVeiculo : StringUtil.empty());

        if (!BigDecimalUtil.isMaiorQueZero(pesoBruto)) {

            pesoBruto = BigDecimal.ZERO;
        }

        if (!BigDecimalUtil.isMaiorQueZero(pesoTara)) {

            pesoTara = BigDecimal.ZERO;
        }

        setPesoLiquido(ObjetoUtil.isNotNull(pesoBruto) ? pesoBruto.subtract(pesoTara) : BigDecimal.ZERO);

        setNumeroNfe(ObjetoUtil.isNotNull(numeroNfe) ? numeroNfe : StringUtil.empty());

        setUnidadeMedida(ObjetoUtil.isNotNull(unidadeMedida) ? unidadeMedida : StringUtil.empty());

        if (ObjetoUtil.isNotNull(dataNfe)) {

            setDataNfe(dataNfe);
        }

        setLote(ObjetoUtil.isNotNull(lote) ? lote : StringUtil.empty());

        setLacre(ObjetoUtil.isNotNull(lacreAmostra) ? lacreAmostra : StringUtil.empty());

        setIdLaudo(idLaudo);
    }
}
