package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.request.item.EventOutRequestItem;
import br.com.sjc.modelo.sap.request.item.EventOutReturnItem;
import com.sap.conn.jco.JCoMetaData;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * <p>
 * <b>Title:</b> EventOutRequest.java
 * </p>
 *
 * <p>
 * <b>Description:</b> Parâmetros de entrada da RFC criada <code>ZITSSAGRO_FM_EVENT_OUT</code>
 * </p>
 *
 * <p>
 * <b>Company: </b> ITSS Soluções em Tecnologia
 * </p>
 *
 * @author Bruno Zafalão
 * @version 1.0.0
 */
@Getter
@Setter
@SAPParameter(function = "ZITSSAGRO_FM_EVENT_OUT")
public class EventOutRequest extends JCoRequest {

    @SAPColumn(name = "OPERA", size = 6, type = JCoMetaData.TYPE_CHAR)
    private String operacao;

    @SAPColumn(name = "MODLO", size = 3, type = JCoMetaData.TYPE_CHAR)
    private String modulo;

    @SAPColumn(name = "EVENT", size = 3, type = JCoMetaData.TYPE_CHAR)
    private String evento;

    @SAPColumn(name = "WERKS", size = 5, type = JCoMetaData.TYPE_CHAR)
    private String centro;

    @SAPColumn(name = "INTSY", size = 32, type = JCoMetaData.TYPE_CHAR)
    private String programID;

    @SAPColumn(name = "INTPR", size = 10, type = JCoMetaData.TYPE_CHAR)
    private String numeroRomaneio;

    @SAPColumn(name = "CHAVE", size = 20, type = JCoMetaData.TYPE_CHAR)
    private String requestKey;

    @SAPColumn(name = "STAGRO", size = 1, type = JCoMetaData.TYPE_CHAR)
    private String statusSAP;

    @SAPTable(name = "MSGDATA", item = EventOutRequestItem.class)
    private List<EventOutRequestItem> data;

    @SAPTable(name = "FLOWOUT", item = EventOutFlowOut.class)
    private List<EventOutFlowOut> flowOut;

    @SAPTable(name = "RETURN", item = EventOutReturnItem.class)
    private List<EventOutReturnItem> logRetorno;
}
