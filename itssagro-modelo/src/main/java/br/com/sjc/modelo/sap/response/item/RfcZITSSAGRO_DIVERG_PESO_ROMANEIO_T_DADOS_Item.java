package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@SAPItem
public class RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_DADOS_Item extends JCoItem {

    @SAPColumn(name = "LIFNR")
    private String lifnr;

    @SAPColumn(name = "NAME1")
    private String name1;

    @SAPColumn(name = "KONNR")
    private String konnr;

    @SAPColumn(name = "MATNR")
    private String matnr;

    @SAPColumn(name = "CLASF")
    private String clasf;

    @SAPColumn(name = "EBELN")
    private String ebeln;

    @SAPColumn(name = "BEDAT")
    private Date bedat;

    @SAPColumn(name = "ZFBDT")
    private Date zfbdt;

    @SAPColumn(name = "DOCDAT")
    private Date docdat;

    @SAPColumn(name = "NFE_SERIE")
    private String nfe_serie;

    @SAPColumn(name = "PESO_NFE")
    private BigDecimal peso_nfe;

    @SAPColumn(name = "TRANSPORTADORA")
    private String transportadora;

    @SAPColumn(name = "MOTORISTA")
    private String motorista;

    @SAPColumn(name = "PLACA")
    private String placa;

    @SAPColumn(name = "PESO_BRUTO_ORIGEM")
    private BigDecimal peso_bruto_origem;

    @SAPColumn(name = "PESO_TARA_ORIGEM")
    private BigDecimal peso_tara_origem;

    @SAPColumn(name = "DESCONTO_ORIGEM")
    private BigDecimal desconto_origem;

    @SAPColumn(name = "PESO_LIQ_ORIGEM")
    private BigDecimal peso_liq_origem;

    @SAPColumn(name = "ROMANEIO")
    private String romaneio;

    @SAPColumn(name = "DOC_DATE")
    private Date doc_date;

    @SAPColumn(name = "PESO_BRUTO_DESTINO")
    private BigDecimal peso_bruto_destino;

    @SAPColumn(name = "PESO_TARA_DESTINO")
    private BigDecimal peso_tara_destino;

    @SAPColumn(name = "DESCONTO_DESTINO")
    private BigDecimal desconto_destino;

    @SAPColumn(name = "PESO_LIQ_DESTINO")
    private BigDecimal peso_liq_destino;

    @SAPColumn(name = "DIF_ORIGEM_DESTINO")
    private BigDecimal dif_origem_destino;

    @SAPColumn(name = "NFE_SERIE_QUEBRADA")
    private String nfe_serie_quebrada;

    @SAPColumn(name = "QTD_NFE_QUEBRADA")
    private BigDecimal qtd_nfe_quebrada;

    @SAPColumn(name = "PAGAMENTO")
    private String pagamento;

    @SAPColumn(name = "DT_PGTO")
    private Date dt_pgto;

    @SAPColumn(name = "TX_ZLSCH")
    private String tx_zlsch;

    @SAPColumn(name = "OPERACAO")
    private String operacao;

    @SAPColumn(name = "VLR_TOTAL_NF")
    private BigDecimal vlr_total_nf;

    @SAPColumn(name = "IMP_SEN_FPAS")
    private BigDecimal imp_sen_fpas;

    @SAPColumn(name = "VLR_RETENCAO")
    private BigDecimal vlr_retencao;

    @SAPColumn(name = "VLR_LIQ_NF_APG")
    private BigDecimal vlr_liq_nf_apg;

    private String centro;
}
