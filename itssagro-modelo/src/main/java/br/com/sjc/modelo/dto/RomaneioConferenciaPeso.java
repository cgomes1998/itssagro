package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.Pesagem;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class RomaneioConferenciaPeso {

    private Pesagem pesagem;

    public RomaneioConferenciaPeso(BigDecimal pesoInicial, BigDecimal pesoFinal) {
        this.pesagem = new Pesagem();
        this.pesagem.setPesoInicial(pesoInicial);
        this.pesagem.setPesoFinal(pesoFinal);
    }
}
