package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.sap.Iva;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import br.com.sjc.util.ObjetoUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class RfcIvaResponseItem extends JCoItem {

    @SAPColumn(name = "MWSKZ")
    private String codigo;

    @SAPColumn(name = "TEXT1")
    private String descricao;

    public Iva toEntidade(Iva entidade) {

        if (ObjetoUtil.isNull(entidade)) {

            entidade = new Iva();

            entidade.setCodigo(this.getCodigo());
        }

        entidade.setDescricao(this.getDescricao());

        return entidade;
    }
}
