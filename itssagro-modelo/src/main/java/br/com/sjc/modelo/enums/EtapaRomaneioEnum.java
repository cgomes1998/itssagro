package br.com.sjc.modelo.enums;

import lombok.Getter;

@Getter
public enum EtapaRomaneioEnum {

    ABA_ROMANEIO,

    ABA_PESAGEM,

    ABA_CLASSIFICACAO,

    ABA_CARREGAMENTO;
}
