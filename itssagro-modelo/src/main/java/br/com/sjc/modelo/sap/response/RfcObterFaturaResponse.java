package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPParameter(function = "ZITSSAGRO_GET_FATURA")
public class RfcObterFaturaResponse extends JCoResponse {

    @SAPColumn(name = "E_FATURA")
    private String fatura;

}
