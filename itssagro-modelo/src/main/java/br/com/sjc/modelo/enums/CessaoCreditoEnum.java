package br.com.sjc.modelo.enums;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Created by julio.bueno on 18/07/2019.
 */
@Getter
@RequiredArgsConstructor
public enum CessaoCreditoEnum {
    PENHOR("PE", "Penhor"),
    CESSAO_DE_CREDITO("CE", "Cessão de Crédito"),
    PENHOR_CESSAO_DE_CREDITO("PC", "Penhor / Cessão de Crédito");

    @NonNull
    private String valor;

    @NonNull
    private String descricao;
}
