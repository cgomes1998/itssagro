package br.com.sjc.modelo.enums;

import lombok.Getter;

@Getter
public enum StatusRegistroEnum {

    ATIVO("Ativo"),

    BLOQUEADO("Bloqueado"),

    ELIMINADO("Eliminado"),

    BLOQUEADO_E_ELIMINADO("Bloqueado e Eliminado");

    private String descricao;

    StatusRegistroEnum(final String descricao) {

        this.descricao = descricao;
    }
}
