package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.StatusAnaliseQualidadeEnum;
import br.com.sjc.modelo.sap.AnaliseQualidadeItem;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_analise_qualidade", schema = "sap")
@AllArgsConstructor
public class AnaliseQualidade extends EntidadeGenerica {

    @Column(name = "id_laudo")
    private Long idLaudo;

    @Column(name = "lacre_amostra")
    private String lacreAmostra;

    @EntityProperties(value = {"id", "codigo", "dataFabricacao", "dataVencimento", "status", "material.id", "material.codigo", "material.descricao"})
    @ManyToOne
    @JoinColumn(name = "id_lote_analise_qualidade")
    private LoteAnaliseQualidade loteAnaliseQualidade;

    @Column(name = "status_analise_qualidade", length = 10)
    @Enumerated(EnumType.STRING)
    private StatusAnaliseQualidadeEnum statusAnaliseQualidade = StatusAnaliseQualidadeEnum.AGUARDANDO;

    @EntityProperties(value = {"id"})
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_romaneio")
    private Romaneio romaneio;

    @EntityProperties(value = {"id", "nome", "login", "cpf"})
    @ManyToOne
    @JoinColumn(name = "id_usuario_responsavel")
    private Usuario usuarioResponsavel;

    @EntityProperties(value = {"id", "codigo", "descricao", "dataValidade", "assinatura", "arquivo.id", "arquivo.nome", "arquivo.mimeType", "arquivo.base64"})
    @ManyToOne
    @JoinColumn(name = "id_assinatura", referencedColumnName = "id")
    private Documento assinatura;

    @EntityProperties(value = {"id", "analiseItem.id", "analiseItem.analise.id", "analiseItem.analise.codigo", "analiseItem.analise.material.id", "analiseItem.analise.material.codigo", "analiseItem.analise.material.descricao",
            "analiseItem.ordem", "analiseItem.grausInpm", "analiseItem.caracteristica", "analiseItem.metodologia", "analiseItem.tipoValidacao", "analiseItem.especificacao", "analiseItem.unidade", "analiseItem.subCaracteristica",
            "analiseItem.condicao", "analiseItem.valorInicial", "analiseItem.valorFinal", "analiseItem.laboratorio", "analiseItem.boletim", "resultado",}, targetEntity = AnaliseQualidadeItem.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "id_analise_qualidade")
    private List<AnaliseQualidadeItem> itens;

    @EntityProperties(value = {"id", "descricao", "tipo", "conferenciaPeso", "buscarDadosRomaneio"})
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "tb_analise_qualidade_motivo", schema = "public", joinColumns = @JoinColumn(name = "id_analise_qualidade"), inverseJoinColumns = @JoinColumn(name = "id_motivo"))
    private List<Motivo> motivos = new ArrayList<>();

    @Transient
    private InputStream assinaturaStream;

    @Transient
    private String certificado;

    @Transient
    private Date dataCertificado;

    @Transient
    private String volumeCertificado;

    @Transient
    private String materiaPrima;

    @Transient
    private String observacao;

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;
        AnaliseQualidade that = (AnaliseQualidade) o;
        return Objects.equals(idLaudo, that.idLaudo) && Objects.equals(lacreAmostra, that.lacreAmostra) && Objects.equals(loteAnaliseQualidade, that.loteAnaliseQualidade) && statusAnaliseQualidade == that.statusAnaliseQualidade && Objects.equals(romaneio, that.romaneio) && Objects.equals(usuarioResponsavel, that.usuarioResponsavel) && Objects.equals(assinatura, that.assinatura) && Objects.equals(itens, that.itens) && Objects.equals(motivos, that.motivos) && Objects.equals(assinaturaStream, that.assinaturaStream) && Objects.equals(certificado, that.certificado) && Objects.equals(dataCertificado, that.dataCertificado);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), idLaudo, lacreAmostra, loteAnaliseQualidade, statusAnaliseQualidade, romaneio, usuarioResponsavel, assinatura, itens, motivos, assinaturaStream, certificado, dataCertificado);
    }

    public AnaliseQualidade() {

    }

    public AnaliseQualidade(Long idLaudo, String lacreAmostra, LoteAnaliseQualidade loteAnaliseQualidade, StatusAnaliseQualidadeEnum statusAnaliseQualidade, Usuario usuarioResponsavel, Documento assinatura,
                            InputStream assinaturaStream, List<AnaliseQualidadeItem> itens, List<Motivo> motivos) {

        this.idLaudo = idLaudo;
        this.lacreAmostra = lacreAmostra;
        this.loteAnaliseQualidade = loteAnaliseQualidade;
        this.statusAnaliseQualidade = statusAnaliseQualidade;
        this.usuarioResponsavel = usuarioResponsavel;
        this.assinatura = assinatura;
        this.assinaturaStream = assinaturaStream;
        if (ColecaoUtil.isNotEmpty(itens)) {
            this.itens = itens.stream().map(a -> new AnaliseQualidadeItem(a.getAnaliseItem(), a.getResultado())).collect(Collectors.toList());
        }
        if (ColecaoUtil.isNotEmpty(motivos)) {
            this.motivos = motivos.stream().map(m -> new Motivo(m.getDescricao(), m.getTipo())).collect(Collectors.toList());
        }
    }

}
