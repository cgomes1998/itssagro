package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusIntegracaoEnum;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class LogIntegracaoDTO implements Serializable, Comparable<LogIntegracaoDTO> {

    private static final long serialVersionUID = 7572924040339186512L;

    private Long id;

    private String denominacao;

    private StatusIntegracaoEnum status;

    private String chave;

    private List<LogIntegracaoDTO> subLogs;

    private String etapa;

    private List<DetalheLogDTO> detalhes;

    private String produtor;

    public String getStatusDescricao() {
        return this.status != null ? this.status.getDescricao() : "";
    }

    public String get_class() {
        return this.status != null ? this.status.get_class() : "";
    }

    @Override
    public int compareTo(LogIntegracaoDTO logIntegracaoDTO) {

        try {

            return ((Integer) Integer.parseInt(this.etapa)).compareTo(Integer.parseInt(logIntegracaoDTO.getEtapa()));

        } catch (Exception ex) {

            return this.etapa.compareTo(logIntegracaoDTO.getEtapa());
        }
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + ((denominacao == null) ? 0 : denominacao.hashCode());
        result = prime * result + ((etapa == null) ? 0 : etapa.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LogIntegracaoDTO other = (LogIntegracaoDTO) obj;
        if (denominacao == null) {
            if (other.denominacao != null)
                return false;
        } else if (!denominacao.equals(other.denominacao))
            return false;
        if (etapa == null) {
            if (other.etapa != null)
                return false;
        } else if (!etapa.equals(other.etapa))
            return false;
        if (status != other.status)
            return false;
        return true;
    }

}
