package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LoteAnaliseQualidadeMotivoInativacaoDTO extends DataDTO {

    @ProjectionProperty
    private String codigo;

    @ProjectionProperty
    private String motivoInativacao;

    @ProjectionProperty
    private StatusEnum status;
}
