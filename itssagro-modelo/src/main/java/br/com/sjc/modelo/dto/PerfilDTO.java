package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.Perfil;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PerfilDTO extends DataDTO {

    @ProjectionProperty
    private String nome;

    @ProjectionProperty
    private StatusEnum status;

    public PerfilDTO(final Perfil perfil) {
        setId(perfil.getId());
        setNome(perfil.getNome());
        setStatus(perfil.getStatus());
    }
}
