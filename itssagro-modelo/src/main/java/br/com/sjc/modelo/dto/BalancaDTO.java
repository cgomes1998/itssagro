package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.DirecaoEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BalancaDTO extends DataDTO {

    @ProjectionProperty
    private String nome;

    @ProjectionProperty
    private String ip;

    @ProjectionProperty
    private DirecaoEnum direcao;

    @ProjectionProperty
    private StatusEnum status;

    public String getDirecaoDescricao() {

        return this.direcao != null ? this.direcao.getDescricao() : "***";
    }

}
