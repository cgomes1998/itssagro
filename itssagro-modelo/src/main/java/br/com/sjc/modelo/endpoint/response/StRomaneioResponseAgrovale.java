package br.com.sjc.modelo.endpoint.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StRomaneioResponseAgrovale {

    private String statusRomaneio;

    private String numeroRomaneio;

    private String chaveNfRemessa;
}