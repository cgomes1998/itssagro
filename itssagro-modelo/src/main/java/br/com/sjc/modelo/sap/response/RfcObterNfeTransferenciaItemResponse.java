package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SAPParameter(function = "/ITSSAGRO/XML_NFE_TRANSF_HD")
public class RfcObterNfeTransferenciaItemResponse extends JCoItem {

    @SAPColumn(name = "MATNR")
    private String matnr;

    @SAPColumn(name = "SAFRA")
    private String safra;

    @SAPColumn(name = "LGORT_O")
    private String lgort_o;

    @SAPColumn(name = "WERKS_O")
    private String werks;

    @SAPColumn(name = "LGORT_D")
    private String lgort_d;

    @SAPColumn(name = "WERKS_D")
    private String werks_d;

    @SAPColumn(name = "EBELN")
    private String ebeln;

    @SAPColumn(name = "EBELP")
    private String ebelp;

    @SAPColumn(name = "MENGE")
    private BigDecimal menge;

    @SAPColumn(name = "MEINS")
    private String meins;

    @SAPColumn(name = "VBELN")
    private String vbeln;

    @SAPColumn(name = "POSNR")
    private String posnr;

}
