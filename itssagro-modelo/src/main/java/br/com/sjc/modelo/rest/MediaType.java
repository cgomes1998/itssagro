package br.com.sjc.modelo.rest;

/**
 * Created by julio.bueno on 10/07/2019.
 */
public interface MediaType {

    String APPLICATION_PDF = "application/pdf";
    String APPLICATION_XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

}
