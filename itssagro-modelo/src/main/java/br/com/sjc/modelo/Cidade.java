package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.UFEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_cidade", schema = "public")
public class Cidade extends EntidadeAutenticada {

    private static final long serialVersionUID = 1L;

    @Column(name = "nome", length = 100)
    private String nome;

    @Enumerated(EnumType.STRING)
    @Column(name = "uf")
    private UFEnum estado;

}