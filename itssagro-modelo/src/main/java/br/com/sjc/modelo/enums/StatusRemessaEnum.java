package br.com.sjc.modelo.enums;

public enum StatusRemessaEnum {

    CRIADA,
    ESTORNADA,
    ERRO_AO_CRIAR,
    ERRO_AO_ESTORNAR;
}
