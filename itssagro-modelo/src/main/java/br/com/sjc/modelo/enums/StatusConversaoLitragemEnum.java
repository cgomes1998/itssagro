package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusConversaoLitragemEnum {

    AGUARDANDO("Aguardando"),
    APROVADO("Aprovado"),
    CANCELADO("Cancelado"),
    REPROVADO("Reprovado"),
    PENDENTE("Pendente");

    private String descricao;
}
