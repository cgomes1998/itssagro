package br.com.sjc.modelo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LaudoClassificacao {

    private String numero;

    private String material;

    private String veiculo;

    private String numeroNfe;

    private String serieNfe;

    private String observacao;

    private Date dataCriacao;

    private List<RelatorioClassificacaoDTO> itensClassificacao;

    public LaudoClassificacao getClone() {

        try {

            return (LaudoClassificacao) super.clone();

        } catch (CloneNotSupportedException e) {

            return this;
        }
    }
}
