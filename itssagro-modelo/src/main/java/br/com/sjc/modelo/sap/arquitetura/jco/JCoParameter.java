package br.com.sjc.modelo.sap.arquitetura.jco;

/**
 * <p>
 * <b>Title:</b> JCoParameter
 * </p>
 *
 * <p>
 * <b>Description:</b> Classe responsável por definir uma classe como parâmetro utilizado na função SAP.
 * </p>
 *
 * @author Bruno Zafalão
 * @version 1.0.0
 */
public abstract class JCoParameter {

    /**
     * Método responsável por obter o ktokk de parâmetro utilizado.
     *
     * @return <code>JCoParameterType</code>
     * @author Bruno Zafalão
     */
    public abstract JCoParameterType type();

    /**
     * Método responsável por obter o nome da função que será executada no SAP.
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    public String function() {

        return this.getClass().getAnnotation(SAPParameter.class).function();
    }
}
