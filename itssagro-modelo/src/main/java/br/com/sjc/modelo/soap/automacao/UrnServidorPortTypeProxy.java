package br.com.sjc.modelo.soap.automacao;

import java.rmi.RemoteException;

public class UrnServidorPortTypeProxy implements UrnServidorPortType {

    private String _endpoint = null;
    private UrnServidorPortType urnServidorPortType = null;

    public UrnServidorPortTypeProxy() {
        _initUrnServidorPortTypeProxy();
    }

    public UrnServidorPortTypeProxy(String endpoint) {
        _endpoint = endpoint;
        _initUrnServidorPortTypeProxy();
    }

    private void _initUrnServidorPortTypeProxy() {
        try {
            urnServidorPortType = (new UrnServidorLocator()).getUrnServidorPort();
            if (urnServidorPortType != null) {
                if (_endpoint != null)
                    ((javax.xml.rpc.Stub) urnServidorPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
                else
                    _endpoint = (String) ((javax.xml.rpc.Stub) urnServidorPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
            }

        } catch (javax.xml.rpc.ServiceException serviceException) {
        }
    }

    public String getEndpoint() {
        return _endpoint;
    }

    public void setEndpoint(String endpoint) {
        _endpoint = endpoint;
        if (urnServidorPortType != null)
            ((javax.xml.rpc.Stub) urnServidorPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);

    }

    public UrnServidorPortType getUrnServidorPortType() {
        if (urnServidorPortType == null)
            _initUrnServidorPortTypeProxy();
        return urnServidorPortType;
    }


    @Override
    public String exemplo(String nome, int idade) throws RemoteException {
        return null;
    }

    @Override
    public String consulta_automatiza(int bal) throws RemoteException {
        return null;
    }

    @Override
    public String trocastatus_automatiza(int tag, int status, String msg1, String msg2, String msg3, int peso_ordem, String obs) throws RemoteException {
        return null;
    }

    @Override
    public String precad_automatiza(int tag, String placa_cavalo, String placa_carreta, String transportador, String motorista, String razaosocial, String endereco, String municipio, String produto, String data_ordem, String nr_ordem) throws RemoteException {
        return null;
    }

}