package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.arquitetura.jco.*;
import br.com.sjc.modelo.sap.request.item.BapiShipmentHeader;
import br.com.sjc.modelo.sap.request.item.BapiShipmentHeaderActionHeader;
import br.com.sjc.modelo.sap.request.item.BapiShipmentItem;
import br.com.sjc.modelo.sap.request.item.BapiShipmentItemAction;
import br.com.sjc.util.ObjetoUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SAPParameter( function = "/ITSSAGRO/BAPI_SHIPMENT_CHANGE" )
public class RfcChangeDocTransporteRequest extends JCoRequest {

    @SAPStructure( name = "HEADERDATA", item = BapiShipmentHeader.class )
    private BapiShipmentHeader bapiShipmentHeader;

    @SAPStructure( name = "HEADERDATAACTION", item = BapiShipmentHeaderActionHeader.class )
    private BapiShipmentHeaderActionHeader bapiShipmentHeaderActionHeader;

    @SAPTable( name = "ITEMDATA", item = BapiShipmentItem.class )
    private List<BapiShipmentItem> bapiShipmentItems;

    @SAPTable( name = "ITEMDATAACTION", item = BapiShipmentItemAction.class )
    private List<BapiShipmentItemAction> bapiShipmentItemActions;

    @SAPColumn( name = "JAVA_ACTION" )
    private String acao;

    @Override
    public int getTableRows ( String name ) {

        final Field[] fields = this.getClass ().getDeclaredFields ();
        List<?> table = Arrays.asList ( fields ).stream ().filter ( f -> ObjetoUtil.isNotNull ( f.getAnnotation ( SAPTable.class ) ) && f.getAnnotation ( SAPTable.class ).name ().equals ( name ) ).map ( f -> {
            try {
                return f.get ( this );
            } catch ( IllegalAccessException e ) {
                e.printStackTrace ();
            }
            return null;
        } ).collect ( Collectors.toList () );

        return ( ( List<?> ) table.get ( 0 ) ).size ();
    }

    @Override
    public List<?> getTableItens ( String name ) {

        final Field[] fields = this.getClass ().getDeclaredFields ();
        List<?> table = Arrays.asList ( fields ).stream ().filter ( f -> ObjetoUtil.isNotNull ( f.getAnnotation ( SAPTable.class ) ) && f.getAnnotation ( SAPTable.class ).name ().equals ( name ) ).map ( f -> {
            try {
                return f.get ( this );
            } catch ( IllegalAccessException e ) {
                e.printStackTrace ();
            }
            return null;
        } ).collect ( Collectors.toList () );
        return ( List<?> ) table.get ( 0 );
    }

    @Override
    public int getStructureRows ( String name ) {

        return 1;
    }

    @Override
    public List<?> getStructureItens (
            String name
    ) {

        final Field[] fields = this.getClass ().getDeclaredFields ();
        return Arrays.asList ( fields ).stream ().filter ( f -> ObjetoUtil.isNotNull ( f.getAnnotation ( SAPStructure.class ) ) && f.getAnnotation ( SAPStructure.class ).name ().equals ( name ) ).map ( f -> {
            try {
                return f.get ( this );
            } catch ( IllegalAccessException e ) {
                e.printStackTrace ();
            }
            return null;
        } ).collect ( Collectors.toList () );
    }
}
