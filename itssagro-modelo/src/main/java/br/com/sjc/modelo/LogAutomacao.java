package br.com.sjc.modelo;

import br.com.sjc.modelo.deserializer.StringRawDeserializer;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_log_automacao")
public class LogAutomacao extends EntidadeGenerica {

	@JsonRawValue
	@JsonDeserialize(using = StringRawDeserializer.class)
	@Column(columnDefinition = "TEXT")
	private String conteudoEnviado;

	@JsonRawValue
	@JsonDeserialize(using = StringRawDeserializer.class)
	@Column(columnDefinition = "TEXT")
	private String conteudoRetornado;

	@Column()
	private String acao;

	@Column(columnDefinition = "TEXT")
	private String excecao;

}
