package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.Safra;
import br.com.sjc.modelo.sap.Veiculo;
import br.com.sjc.util.BigDecimalUtil;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PesagemEClassificacaoDTO extends DataDTO {

    private String numeroRomaneio;

    private String descricaoOperacao;

    private BigDecimal pesoBruto;

    private BigDecimal pesoTara;

    private BigDecimal totalDescontos;

    private BigDecimal pesoLiquido;

    private Veiculo placaCavalo;

    private Safra safra;

    private StatusRomaneioEnum statusRomaneio;

    private StatusIntegracaoSAPEnum statusIntegracaoSAP;

    private boolean classificacaoRealizada;

    private Date dtCriacaoRomaneio;

    private String descricaoMaterial;

    private String nomeCliente;

    public PesagemEClassificacaoDTO(final Romaneio entidade) {
        this.setId(entidade.getId());
        this.setNumeroRomaneio(entidade.getNumeroRomaneio());
        this.setDescricaoOperacao(entidade.getOperacao().getDescricao());
        this.setDtCriacaoRomaneio(entidade.getDataCadastro());

        if (ObjetoUtil.isNotNull(entidade.getMaterial())) {
            this.setDescricaoMaterial(entidade.getMaterial().getDescricao());
        }
        if (ObjetoUtil.isNotNull(entidade.getCliente())) {
            this.setNomeCliente(entidade.getCliente().getNome());
        }

        if (ObjetoUtil.isNotNull(entidade.getPesagem())) {
            this.setPesoBruto(entidade.getPesagem().getPesoInicial());
            this.setPesoTara(entidade.getPesagem().getPesoFinal());
            BigDecimal descontos = BigDecimal.ZERO;

            if (ObjetoUtil.isNotNull(entidade.getClassificacao()) && ColecaoUtil.isNotEmpty(entidade.getClassificacao().getItens())) {
                descontos = entidade.getClassificacao().getItens().parallelStream().map(i -> ObjetoUtil.isNotNull(i.getDesconto()) ? i.getDesconto() : BigDecimal.ZERO).reduce(BigDecimal.ZERO, BigDecimal::add);
                this.setTotalDescontos(descontos);
            }

            final BigDecimal bruto = ObjetoUtil.isNotNull(entidade.getPesagem().getPesoInicial()) ? entidade.getPesagem().getPesoInicial() : BigDecimal.ZERO;
            final BigDecimal tara = ObjetoUtil.isNotNull(entidade.getPesagem().getPesoFinal()) ? entidade.getPesagem().getPesoFinal() : BigDecimal.ZERO;
            final BigDecimal seco = (bruto.subtract(tara)).subtract(descontos);
            this.setPesoLiquido(BigDecimalUtil.isMaiorZero(seco) ? seco : null);
        }

        if (entidade.getOperacao().isPossuiDivisaoCarga()) {
            this.setPlacaCavalo(entidade.getItens().stream().filter(i -> ObjetoUtil.isNotNull(i.getPlacaCavalo())).map(i -> i.getPlacaCavalo()).findFirst().orElse(null));
            this.setSafra(entidade.getItens().stream().filter(i -> ObjetoUtil.isNotNull(i.getSafra())).map(i -> i.getSafra()).findFirst().orElse(null));
        } else {
            this.setPlacaCavalo(entidade.getPlacaCavalo());
            this.setSafra(entidade.getSafra());
        }

        this.setClassificacaoRealizada(ObjetoUtil.isNotNull(entidade.getClassificacao()) && ColecaoUtil.isNotEmpty(entidade.getClassificacao().getItens()));
        this.setStatusRomaneio(entidade.getStatusRomaneio());
        this.setStatusIntegracaoSAP(entidade.getStatusIntegracaoSAP());
    }

    public String getStatusIntegracaoDescricao() {
        return ObjetoUtil.isNotNull(this.statusIntegracaoSAP) ? this.statusIntegracaoSAP.getDescricao() : "***";
    }

    public String getStatusRomaneioDescricao() {
        return this.statusRomaneio.getDescricao();
    }

    public String getPlacaCavaloDescricao() {
        return ObjetoUtil.isNull(this.placaCavalo) ? "***" : StringUtil.toPlaca(this.placaCavalo.getPlaca1());
    }

    public String getSafraDescricao() {
        return ObjetoUtil.isNull(this.safra) ? "***" : this.safra.getDescricao();
    }
}
