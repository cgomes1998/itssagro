package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.endpoint.response.item.TransportadoraResponse;
import br.com.sjc.modelo.enums.TipoTransporteEnum;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_transportadora", schema = "sap")
public class Transportadora extends EntidadeSAPSync {

    @Column(name = "desc_transportadora")
    private String descricao;

    @Column(name = "cnpj_transportadora")
    private String cnpj;

    @Column(name = "cpf_transportadora")
    private String cpf;

    @Enumerated(EnumType.STRING)
    @Column(name = "tp_transporte")
    private TipoTransporteEnum tipoTransporte;

    public TransportadoraResponse toResponse() {

        final TransportadoraResponse response = new TransportadoraResponse();

        response.setCodigo(this.getCodigo());

        response.setNome(this.getDescricao());

        return response;
    }

    public Transportadora() {

        super();
    }

    public Transportadora(final String codigo, final String descricao) {

        this.setCodigo(codigo);

        this.descricao = descricao;
    }

    public String getDescricaoFormatada() {

        return this.toString();
    }

    public String getCpfCnpj() {

        if (StringUtil.isNotNullEmpty(this.cpf)) {
            return StringUtil.format(this.cpf, "###.###.###-##");
        } else if (StringUtil.isNotNullEmpty(this.cnpj)) {
            return StringUtil.format(this.cnpj, "###.###.###/####-##");
        }
        return this.getCodigo();
    }

    @Override
    public String toString() {

        if (StringUtil.isNotNullEmpty(this.getCodigo())) {

            return this.getCodigo() + " - " + this.descricao;
        }

        if (StringUtil.isNotNullEmpty(this.cpf)) {

            return StringUtil.format(this.cpf, "###.###.###-##") + " - " + this.descricao;
        }

        if (StringUtil.isNotNullEmpty(this.cnpj)) {

            return StringUtil.format(this.cnpj, "###.###.###/####-##") + " - " + this.descricao;
        }

        return this.descricao;
    }

}
