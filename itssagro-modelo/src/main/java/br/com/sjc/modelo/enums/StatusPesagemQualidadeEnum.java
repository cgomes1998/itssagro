package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusPesagemQualidadeEnum {

    AGUARDANDO("Aguardando"),
    CHAMADO("Chamado"),
    EM_PESAGEM("Em Pesagem"),
    CAPTURADO("Capturado"),
    NAO_CAPTURADO("Não Capturado"),
    PENDENTE("Pendente");

    private String descricao;
}
