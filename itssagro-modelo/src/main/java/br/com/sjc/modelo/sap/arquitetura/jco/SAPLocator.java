package br.com.sjc.modelo.sap.arquitetura.jco;

import br.com.sjc.util.StringUtil;
import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoDestinationManager;
import com.sap.conn.jco.JCoException;
import com.sap.conn.jco.JCoRepository;
import com.sap.conn.jco.ext.DestinationDataProvider;
import com.sap.conn.jco.ext.Environment;
import com.sap.conn.jco.ext.ServerDataProvider;
import com.sap.conn.jco.server.JCoServer;
import com.sap.conn.jco.server.JCoServerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Properties;

/**
 * <p>
 * <b>Title:</b>SAPLocator
 * </p>
 *
 * <p>
 * <b>Description:</b>Classe responsável por localizar o serviço de integração com SAP e inicializar a comunicação via JCO.
 * </p>
 *
 * @author Bruno Zafalão
 * @version 1.0.0
 */
public final class SAPLocator {

    /**
     * Atributo provider.
     */
    private static DestinationDataProvider provider;

    /**
     * Responsável pela criação de novas instâncias desta classe.
     */
    private SAPLocator() {

        super();
    }

    /**
     * Método responsável por localizar os serviços de integração com SAP.
     *
     * @param configuration
     * @return <code>JCoDestination</code>
     * @author Bruno Zafalão
     */
    public static JCoDestination locale(final SAPConfiguration configuration) {

        final Properties connectProperties = new Properties();

        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_ALIAS_USER, configuration.aliasuser());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_ASHOST, configuration.ashost());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_CLIENT, configuration.client());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_CODEPAGE, configuration.codepage());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_DEST, configuration.dest());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_GETSSO2, configuration.getsso2());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_GROUP, configuration.group());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_GWHOST, configuration.gwhost());

        if (configuration.saprouter() != null && !configuration.saprouter().isEmpty()) {

            SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_SAPROUTER, "/H/" + configuration.saprouter());
        }

        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_GWSERV, configuration.gwserv());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_LANG, configuration.lang());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_LCHECK, configuration.lcheck());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_MSHOST, configuration.mshost());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_MYSAPSSO2, configuration.mysapsso2());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_PASSWD, configuration.passwd());

        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_R3NAME, configuration.r3name());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_SNC_LIBRARY, configuration.snclib());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_SNC_MODE, configuration.sncmode());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_SNC_MYNAME, configuration.sncmyname());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_SNC_PARTNERNAME, configuration.sncpartnername());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_SNC_QOP, configuration.sncqop());

        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_SYSNR, configuration.sysnr());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_TPHOST, configuration.tphost());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_TPNAME, configuration.tpname());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_TYPE, configuration.type());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_USE_SAPGUI, configuration.usesapgui());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_USER, configuration.user());
        SAPLocator.addProperty(connectProperties, DestinationDataProvider.JCO_X509CERT, configuration.x509cert());

        JCoDestination destinationService = null;

        try {

            if (Environment.isDestinationDataProviderRegistered()) {

                Environment.unregisterDestinationDataProvider(SAPLocator.provider);

                SAPLocator.provider = null;
            }

            SAPLocator.provider = new RFCDestinationDataProvider(connectProperties);

            try {

                Environment.registerDestinationDataProvider(SAPLocator.provider);

            } catch (final Exception e) {

                e.printStackTrace();
            }

            destinationService = JCoDestinationManager.getDestination(configuration.destinationName());

            destinationService.ping();

        } catch (final JCoException e) {

            e.printStackTrace();

            throw new RuntimeException(e);
        }

        return destinationService;
    }

    /**
     * Método responsável por localizar os serviços de integração com SAP.
     *
     * @param destinationConfiguration
     * @param serverConfiguration
     * @return <code>JCoDestination</code>
     * @author Bruno Zafalão
     */
    public static JCoServer locale(final SAPConfiguration destinationConfiguration, final SAPConfiguration serverConfiguration) {

        try {

            final Properties properties = new Properties();

            SAPLocator.addProperty(properties, ServerDataProvider.JCO_CONNECTION_COUNT, serverConfiguration.conectioncount());

            if (serverConfiguration.saprouter() != null && !serverConfiguration.saprouter().isEmpty()) {

                SAPLocator.addProperty(properties, ServerDataProvider.JCO_GWHOST, "/H/" + serverConfiguration.saprouter() + "/H/" + serverConfiguration.gwhost());

            } else {

                SAPLocator.addProperty(properties, ServerDataProvider.JCO_GWHOST, serverConfiguration.gwhost());
            }

            SAPLocator.addProperty(properties, ServerDataProvider.JCO_PROGID, serverConfiguration.progid());

            SAPLocator.addProperty(properties, ServerDataProvider.JCO_GWSERV, serverConfiguration.gwserv());

            final ServerDataProvider provider = new RFCServerDataProvider(properties);

            final JCoDestination destinationService = SAPLocator.locale(destinationConfiguration);

            if (!Environment.isServerDataProviderRegistered()) {

                Environment.registerServerDataProvider(provider);
            }

            final JCoRepository repository = destinationService.getRepository();

            final JCoServer server = JCoServerFactory.getServer(serverConfiguration.serverName());

            server.setRepository(repository);

            return server;

        } catch (final Exception e) {

            e.printStackTrace();
        }

        return null;
    }

    /**
     * Método responsável por criar o arquivo destination utilizado nos serviços de integração com o SAP.
     *
     * @param destinationName
     * @param connectProperties
     * @author Bruno Zafalão
     */
    protected static void createDestinationFile(final String destinationName, final Properties connectProperties) {

        final File destination = new File(destinationName.concat(SAPConfiguration.SUFFIXX_DESTINATION));

        try {

            final FileOutputStream fos = new FileOutputStream(destination, false);

            connectProperties.store(fos, new StringBuilder(destinationName).toString());

            fos.close();

        } catch (final Exception e) {

            throw new RuntimeException(e);
        }
    }

    /**
     * Método responsável por adicionar os valores informados na configuração do JCO.
     *
     * @param connectProperties
     * @param provider
     * @param value
     * @author Bruno Zafalão
     */
    private static void addProperty(final Properties connectProperties, final String provider, final String value) {

        if (StringUtil.isNotNullEmpty(value)) {

            connectProperties.setProperty(provider, value);
        }
    }
}
