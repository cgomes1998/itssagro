package br.com.sjc.modelo.response;

import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.dto.CampoOperacaoDTO;
import br.com.sjc.modelo.sap.Produtor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
public class ObterOperacaoResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private DadosSincronizacao operacao;
    private List<CampoOperacaoDTO> campos;
    private Produtor armazemFilial;
}
