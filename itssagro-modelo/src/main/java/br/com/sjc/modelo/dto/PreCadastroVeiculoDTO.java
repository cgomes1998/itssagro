package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusAutomacaoEnum;
import lombok.Data;

@Data
public class PreCadastroVeiculoDTO {

    private String placaVeiculo;
    private StatusAutomacaoEnum status;

    public PreCadastroVeiculoDTO(String retorno) {
        String[] retornos = retorno.split(",");
        if (retornos.length >= 2) {
            this.placaVeiculo = retornos[0];
            this.status = StatusAutomacaoEnum.valueOf(Integer.parseInt(retornos[1]));
        }
    }

}
