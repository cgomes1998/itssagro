package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.sap.TipoVeiculo;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TipoVeiculoDTO extends DataDTO {

    @ProjectionProperty
    private String codigo;

    @ProjectionProperty
    private String descricao;

    @ProjectionProperty
    private String tipo;

    @ProjectionProperty
    private String textoTipo;

    @ProjectionProperty
    private StatusCadastroSAPEnum statusCadastroSap;

    @ProjectionProperty
    private StatusEnum status;

    @ProjectionProperty
    private BigDecimal peso;

    @ProjectionProperty
    private BigDecimal pesoTara;

    @ProjectionProperty
    private BigDecimal pesoLiquido;

    public TipoVeiculoDTO(Long id, String codigo, String descricao, String tipo, String textoTipo, BigDecimal peso, BigDecimal pesoTara, BigDecimal pesoLiquido) {

        this.setId(id);
        this.codigo = codigo;
        this.descricao = descricao;
        this.tipo = tipo;
        this.textoTipo = textoTipo;
        this.peso = peso;
        this.pesoTara = pesoTara;
        this.pesoLiquido = pesoLiquido;
    }

    public TipoVeiculoDTO(final TipoVeiculo entidade) {

        setId(entidade.getId());
        setCodigo(entidade.getCodigo());
        setDescricao(entidade.getDescricao());
        setTipo(entidade.getTipo());
        setTextoTipo(entidade.getTextoTipo());
        setStatusCadastroSap(entidade.getStatusCadastroSap());
        setStatus(entidade.getStatus());
        setPeso(entidade.getPeso());
        setPesoLiquido(entidade.getPesoLiquido());
        setPesoTara(entidade.getPesoTara());
    }

    public TipoVeiculoDTO(Long idTipoVeiculo) {

        this.setId(idTipoVeiculo);
    }

    public String getStatusSapDescricao() {

        return ObjetoUtil.isNotNull(this.statusCadastroSap) ? this.statusCadastroSap.getDescricao() : null;
    }

    public String getStatusRegistroDescricao() {

        return ObjetoUtil.isNotNull(this.status) ? this.status.getDescricao() : "***";
    }

    @Override
    public String toString() {

        if ((this.tipo != null && !this.tipo.trim().equals("")) && (this.textoTipo != null && !this.textoTipo.trim().equals(""))) {

            return this.descricao + " - " + this.toMilhar(this.peso) + " / " + this.tipo + " - " + this.textoTipo;
        }

        return this.descricao + " - " + this.toMilhar(this.peso);
    }

    private String toMilhar(final BigDecimal valor) {

        if (ObjetoUtil.isNull(valor))
            return "";

        Locale brasil = new Locale("pt", "BR");

        DecimalFormat df = new DecimalFormat("#,###.######", new DecimalFormatSymbols(brasil));

        return df.format(valor);
    }

}
