package br.com.sjc.modelo.enums;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public enum StatusRomaneioEnum {

    CONCLUIDO("Concluído"),

    AGUARDANDO("Aguardando"),

    CRIADO("Criado"),

    CANCELADO("Cancelado"),

    EM_PROCESSAMENTO("Em Processamento"),

    EM_CONTIGENCIA("Em Contigência"),

    ESTORNADO("Estornado"),

    ENVIADO("Enviado"),

    PENDENTE("Pendente"),

    EFETIVO("Efetivo"),

    PENDENTE_INDICES_CLASSIFICACAO("Pendente (Aprovação de índices de classificação)");

    private String descricao;

    StatusRomaneioEnum(final String descricao) {

        this.descricao = descricao;
    }

    public static List<StatusRomaneioEnum> listarTodosExceto(StatusRomaneioEnum... status) {

        return Arrays.asList(StatusRomaneioEnum.values()).stream().filter(i -> Arrays.asList(status).stream().filter(s -> s.equals(i)).count() <= 0).collect(Collectors.toList());
    }

    public boolean isConcluido() {

        return this.equals(CONCLUIDO);
    }

    public static StatusRomaneioEnum get(String str) {

        return Arrays.asList(values()).stream().filter(statusRomaneioEnum -> statusRomaneioEnum.getDescricao().equals(str)).findFirst().orElse(null);
    }
}
