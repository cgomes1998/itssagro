package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.Senha;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RecebimentoSapNfeCargaPontualDTO extends DataDTO {

    @ProjectionProperty(values = {"id", "cargaPontualIdAgendamento"})
    private Senha senha;

}
