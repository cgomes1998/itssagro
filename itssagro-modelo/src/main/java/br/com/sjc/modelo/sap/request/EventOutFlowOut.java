package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import com.sap.conn.jco.JCoMetaData;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * <b>Title:</b> FM_EVENT_OUT_FLOW_OUT.java
 * </p>
 *
 * <p>
 * <b>Description:</b> Classe de retorno de log.
 * </p>
 *
 * <p>
 * <b>Company: </b> Fábrica de Software -ITSS Tecnologia
 * </p>
 *
 * @author Silvânio Júnior
 * @version 1.0.0
 */
@Getter
@Setter
public class EventOutFlowOut extends JCoItem {

    @SAPColumn(name = "FLOWD", size = 3, type = JCoMetaData.TYPE_CHAR, ordinal = 0)
    private String etapa;

    @SAPColumn(name = "DENOM", size = 40, type = JCoMetaData.TYPE_CHAR, ordinal = 1)
    private String denominacao;

    @SAPColumn(name = "FLWST", size = 1, type = JCoMetaData.TYPE_CHAR, ordinal = 2)
    private String status;

    @SAPColumn(name = "DOCUM", size = 50, type = JCoMetaData.TYPE_CHAR, ordinal = 3)
    private String documento;

}
