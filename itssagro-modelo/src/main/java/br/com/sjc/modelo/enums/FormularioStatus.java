package br.com.sjc.modelo.enums;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Created by julio.bueno on 04/07/2019.
 */
@Getter
@RequiredArgsConstructor
public enum FormularioStatus {
    EM_CRIACAO("Em Criação"),
    PUBLICADO("Publicado"),
    DESCONTINUADO("Descontinuado");

    @NonNull
    private String descricao;
}
