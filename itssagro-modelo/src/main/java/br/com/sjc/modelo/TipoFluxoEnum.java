package br.com.sjc.modelo;

import lombok.Getter;

@Getter
public enum TipoFluxoEnum {

    RECEBIMENTO("Recebimento"),
    EXPEDICAO("Expedição");

    private String descricao;

    TipoFluxoEnum(String descricao) {

        this.descricao = descricao;
    }
}
