package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.TipoFluxoEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_balanca_fluxo_pesagem", schema = "public")
public class BalancaFluxoPesagem implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_fluxo")
    private TipoFluxoEnum tipoFluxo;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_balanca")
    private Balanca balanca;
}
