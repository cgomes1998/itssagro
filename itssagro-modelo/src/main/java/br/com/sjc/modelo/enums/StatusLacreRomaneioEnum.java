package br.com.sjc.modelo.enums;

import lombok.Getter;

@Getter
public enum StatusLacreRomaneioEnum {

    VINCULADO ( "Vinculado" ),

    INUTILIZADO ( "Inutilizado" );

    private String descricao;

    StatusLacreRomaneioEnum ( final String descricao ) {

        this.descricao = descricao;
    }

}
