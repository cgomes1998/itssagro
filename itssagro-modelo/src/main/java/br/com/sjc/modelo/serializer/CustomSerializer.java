package br.com.sjc.modelo.serializer;

import br.com.sjc.modelo.EntidadeGenerica;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by julio.bueno on 25/03/2019.
 */
public abstract class CustomSerializer<E extends EntidadeGenerica> extends JsonSerializer<E> {

    private JsonGenerator gen;

    @Override
    public void serialize(E entidade, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        this.gen = gen;

        if (!Objects.isNull(entidade)) {
            gen.writeStartObject();
            writeSerializableField("id", entidade.getId());
            serializeAttributes(entidade);
            gen.writeEndObject();
        }
    }

    protected void writeNumberField(String field, Long value) throws IOException {
        if (!Objects.isNull(value)) {
            gen.writeNumberField(field, value);
        }
    }

    protected void writeStringField(String field, String value) throws IOException {
        if (!Objects.isNull(value)) {
            gen.writeStringField(field, value);
        }
    }

    protected void writeSerializableField(String field, Serializable value) throws IOException {
        if (!Objects.isNull(value)) {
            gen.writeStringField(field, value.toString());
        }
    }

    protected void writeObjectField(String field, Object value) throws IOException {
        if (!Objects.isNull(value)) {
            gen.writeObjectField(field, value);
        }
    }

    protected abstract void serializeAttributes(E entidade) throws IOException;

}
