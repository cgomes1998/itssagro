package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class RfcOrdemVendaCabecalhoSaidaResponse extends JCoItem {

    @SAPColumn(name = "OPERATION")
    private String operacao;

    @SAPColumn(name = "DOC_NUMBER")
    private String numeroDocumentoVenda;

    @SAPColumn(name = "CO_AREA")
    private String empresa;

    @SAPColumn(name = "BILL_BLOCK")
    private String status;

    @SAPColumn(name = "DOC_TYPE")
    private String tipoOrdem;

    @SAPColumn(name = "PURCH_NO")
    private String numeroPedido;

}
