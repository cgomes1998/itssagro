package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusAutomacaoEnum;
import br.com.sjc.util.ObjetoUtil;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class VeiculoBalancaDTO {

    private Integer balanca;
    private Integer nrCartao;
    private String placaVeiculo;
    private BigDecimal peso1;
    private BigDecimal peso2;
    private StatusAutomacaoEnum status;

    public VeiculoBalancaDTO() {
        super();
    }

    public VeiculoBalancaDTO(String retorno) {
        String[] retornos = retorno.split(",");
        if (retornos.length >= 6) {
            this.balanca = Integer.valueOf(retornos[0]);
            this.nrCartao = Integer.valueOf(retornos[1]);
            this.placaVeiculo = retornos[2];
            if (ObjetoUtil.isNotNull(retornos[3])) {
                this.peso1 = new BigDecimal(retornos[3]);
            }
            if (ObjetoUtil.isNotNull(retornos[4])) {
                this.peso2 = new BigDecimal(retornos[4]);
            }
            this.status = StatusAutomacaoEnum.valueOf(Integer.parseInt(retornos[5]));
        }
    }

}
