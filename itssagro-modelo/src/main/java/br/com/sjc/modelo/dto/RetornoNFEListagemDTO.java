package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.ObjectID;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.anotation.ProjectionConfigurationDTO;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ProjectionConfigurationDTO
public class RetornoNFEListagemDTO implements ObjectID<Long> {

    @ProjectionProperty
    private Long id;

    @ProjectionProperty
    private String nfeTransporte;

    @ProjectionProperty
    private String nfe;

    @ProjectionProperty
    private String serie;

    @ProjectionProperty
    private String status;

    @ProjectionProperty
    private String requestKey;

    @ProjectionProperty(values = {"id", "numeroRomaneio", "requestKey"})
    private Romaneio romaneio;

    public String getNotaCompleta() {

        String valor = "";

        if (StringUtil.isNotNullEmpty(this.nfe)) {

            valor = this.nfe;

        } else if (StringUtil.isNotNullEmpty(this.nfeTransporte)) {

            valor = this.nfeTransporte;
        }

        if (StringUtil.isNotNullEmpty(valor) && StringUtil.isNotNullEmpty(this.serie)) {

            valor += " - " + this.serie;
        }

        return valor;
    }

}
