package br.com.sjc.modelo.dto;

import br.com.sjc.util.BigDecimalUtil;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ItemClassificacaoDepositoDTO {

    private BigDecimal desconto;

    private BigDecimal indice;

    private BigDecimal percentualDesconto;

    private String descricaoItemClassificacao;

    private String codigoItemClassificacao;

    private Long idClassificacao;

    private String numeroRomaneio;

    public ItemClassificacaoDepositoDTO() {

    }

    public ItemClassificacaoDepositoDTO(
            BigDecimal desconto,
            BigDecimal indice,
            BigDecimal percentualDesconto,
            String descricaoItemClassificacao,
            String codigoItemClassificacao,
            String numeroRomaneio
    ) {

        this.desconto = BigDecimalUtil.isMaiorZero(desconto) ? desconto : BigDecimal.ZERO;
        this.indice = BigDecimalUtil.isMaiorZero(indice) ? indice : BigDecimal.ZERO;
        this.percentualDesconto = BigDecimalUtil.isMaiorZero(percentualDesconto) ? percentualDesconto : BigDecimal.ZERO;
        this.descricaoItemClassificacao = descricaoItemClassificacao;
        this.codigoItemClassificacao = codigoItemClassificacao;
        this.numeroRomaneio = numeroRomaneio;
    }

    public ItemClassificacaoDepositoDTO(
            BigDecimal desconto,
            BigDecimal indice,
            BigDecimal percentualDesconto,
            String descricaoItemClassificacao,
            String codigoItemClassificacao,
            Long idClassificacao
    ) {

        this.desconto = BigDecimalUtil.isMaiorZero(desconto) ? desconto : BigDecimal.ZERO;
        this.indice = BigDecimalUtil.isMaiorZero(indice) ? indice : BigDecimal.ZERO;
        this.percentualDesconto = BigDecimalUtil.isMaiorZero(percentualDesconto) ? percentualDesconto : BigDecimal.ZERO;
        this.descricaoItemClassificacao = descricaoItemClassificacao;
        this.codigoItemClassificacao = codigoItemClassificacao;
        this.idClassificacao = idClassificacao;
    }

}
