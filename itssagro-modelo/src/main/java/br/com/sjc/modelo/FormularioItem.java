package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.FormularioItemTipo;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by julio.bueno on 04/07/2019.
 */
@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_formulario_item")
public class FormularioItem extends EntidadeGenerica {

    @EntityProperties(value = {"id"})
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_formulario", nullable = false)
    private Formulario formulario;

    @Column
    private String titulo;

    @Column
    private Boolean required = false;

    @Column
    private Integer ordem;

    @Column
    private Integer tamanho;

    @Column
    private String valorEsperado;

    @Enumerated(EnumType.STRING)
    private FormularioItemTipo tipo;

    @EntityProperties(value = {"id", "formularioItem.id", "titulo", "ordem"}, targetEntity = FormularioItemOpcao.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "formularioItem", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    private List<FormularioItemOpcao> opcoes = new ArrayList<>();

    public void addOpcao(FormularioItemOpcao opcao) {

        opcoes.add(opcao);
    }

    public List<FormularioItemOpcao> getOpcoes() {

        opcoes.sort((opcao_1, opcao_2) -> opcao_1.getOrdem().compareTo(opcao_2.getOrdem()));

        return opcoes;
    }

    public boolean isSessao() {

        return FormularioItemTipo.SESSAO == tipo;
    }

    public boolean isTextArea() {

        return FormularioItemTipo.TEXT_AREA == tipo;
    }

    public boolean isData() {

        return FormularioItemTipo.DATA == tipo;
    }

    public boolean isHora() {

        return FormularioItemTipo.HORA == tipo;
    }

}
