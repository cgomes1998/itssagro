package br.com.sjc.modelo.cfg;

import br.com.sjc.modelo.EntidadeGenerica;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_campo_operacao_material", schema = "cfg")
public class CampoOperacaoMaterial extends EntidadeGenerica {

    private static final long serialVersionUID = 1L;

    @Column(name = "operacao", updatable = false)
    @Enumerated(EnumType.STRING)
    private DadosSincronizacaoEnum operacao;

    @Column(name = "obrigatorio", nullable = false, columnDefinition = "boolean default false")
    private boolean obrigatorio;

    @Column(name = "somente_leitura", nullable = false, columnDefinition = "boolean default false")
    private boolean somenteLeitura;

    @Column(name = "descricao_display")
    private String display;

    @EntityProperties(value = {"id", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_campo", updatable = false)
    private Campo campo;
    
    @EntityProperties(value = {"id"})
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_material")
    private Material material;

    @Transient
    private Long idCampo;

    public CampoOperacaoMaterial() {
    }

    public CampoOperacaoMaterial(DadosSincronizacaoEnum operacao, boolean somenteLeitura, boolean obrigatorio, String display, Campo campo, Material material) {
        this.somenteLeitura = somenteLeitura;
        this.operacao = operacao;
        this.obrigatorio = obrigatorio;
        this.display = display;
        this.campo = campo;
        this.material = material;
    }

    public CampoOperacaoMaterial(DadosSincronizacaoEnum operacao, boolean obrigatorio, String display, Campo campo, Material material) {
        this.operacao = operacao;
        this.obrigatorio = obrigatorio;
        this.display = display;
        this.campo = campo;
        this.material = material;
    }
}
