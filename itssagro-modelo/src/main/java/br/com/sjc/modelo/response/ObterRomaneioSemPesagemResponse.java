package br.com.sjc.modelo.response;

import br.com.sjc.modelo.sap.Romaneio;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class ObterRomaneioSemPesagemResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private RomaneioSemPesagemResponse entidade;
    private boolean habilitarLacragem;
    private boolean habilitarConversao;
    private boolean habilitarPesagem;
    private boolean habilitarClassificacao;
}
