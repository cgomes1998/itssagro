package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPStructure;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@SAPParameter(function = "/ITSSAGRO/XML_CTE_TCIB_T_HD")
public class RfcObterCteResponse extends JCoResponse {

    @SAPStructure(name = "W_HEADER", item = RfcObterCteHeaderResponse.class )
    private RfcObterCteHeaderResponse header;

}
