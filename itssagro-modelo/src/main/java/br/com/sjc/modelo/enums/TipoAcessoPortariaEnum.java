package br.com.sjc.modelo.enums;

import lombok.Getter;

@Getter
public enum TipoAcessoPortariaEnum {

    DIRETO("Direto"),

    MARCACAO_VEZ("Marcação de Vez");

    private String descricao;

    TipoAcessoPortariaEnum(String descricao) {
        this.descricao = descricao;
    }
}
