package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.EntidadeGenerica;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_romaneio_armazem_terceiros", schema = "sap")
public class RomaneioArmazemTerceiros extends EntidadeGenerica {

    private static final long serialVersionUID = -7939466771665487472L;

    @Column(name = "nr_romaneio", unique = true, updatable = true, insertable = true)
    private String ticketOrigem;

    @Column(name = "cnpj_armazem")
    private String cnpjArmazem;

    @Column(name = "nome_armazem")
    private String nomeArmazem;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_entrada_armazem")
    private Date dataEntradaArmazem;
}
