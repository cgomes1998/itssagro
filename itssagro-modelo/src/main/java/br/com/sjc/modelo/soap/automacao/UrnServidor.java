/**
 * UrnServidor.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.sjc.modelo.soap.automacao;

public interface UrnServidor extends javax.xml.rpc.Service {

    String getUrnServidorPortAddress();

    UrnServidorPortType getUrnServidorPort() throws javax.xml.rpc.ServiceException;

    UrnServidorPortType getUrnServidorPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;

}
