package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.StatusConferenciaEnum;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_conferencia", schema = "sap")
public class Conferencia extends EntidadeGenerica {

    @Column(name = "status_conferencia")
    @Enumerated(EnumType.STRING)
    private StatusConferenciaEnum statusConferencia = StatusConferenciaEnum.AGUARDANDO;

    @EntityProperties(value = {"id", "descricao", "tipo", "conferenciaPeso", "buscarDadosRomaneio"}, targetEntity = Motivo.class)
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "tb_conferencia_motivo", schema = "public", joinColumns = @JoinColumn(name = "id_conferencia"), inverseJoinColumns = @JoinColumn(name = "id_motivo"))
    private List<Motivo> motivos = new ArrayList<>();

}
