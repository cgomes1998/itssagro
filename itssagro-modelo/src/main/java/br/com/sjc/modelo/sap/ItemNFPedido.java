package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.EntidadeGenerica;
import br.com.sjc.modelo.dto.DivisaoCargaDTO;
import br.com.sjc.modelo.dto.ItemOrdemVendaDTO;
import br.com.sjc.modelo.dto.NFeDTO;
import br.com.sjc.modelo.enums.StatusDocTransporteEnum;
import br.com.sjc.modelo.enums.StatusRemessaEnum;
import br.com.sjc.modelo.enums.TipoFreteEnum;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_item_nf_pedido", schema = "sap")
public class ItemNFPedido extends EntidadeGenerica implements Serializable, Comparable<ItemNFPedido> {

    @EntityProperties(value = {"id", "cpfCnpj", "codigo", "nome", "tipoTransporte"})
    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id")
    private Cliente cliente;

    @EntityProperties(value = {"id", "cpfCnpj", "codigo", "nome"})
    @ManyToOne
    @JoinColumn(name = "id_operador_logistico", referencedColumnName = "id")
    private Cliente operadorLogistico;

    @EntityProperties(value = {"id", "cpf", "cnpj", "codigo", "nome", "fazenda", "endFazenda", "bairro", "municipio", "estado", "cep", "inscricaoEstadual", "statusInscEstadual"})
    @ManyToOne
    @JoinColumn(name = "id_produtor", referencedColumnName = "id")
    private Produtor produtor;
    
    @EntityProperties(value = {"id", "cpf", "cnpj", "codigo", "nome", "fazenda", "endFazenda", "bairro", "municipio", "estado", "cep", "inscricaoEstadual", "statusInscEstadual"})
    @ManyToOne
    @JoinColumn(name = "id_produtor_destino", referencedColumnName = "id")
    private Produtor produtorDestino;

    @EntityProperties(value = {"id", "cpf", "cnpj", "codigo", "nome", "fazenda", "endFazenda", "bairro", "municipio", "estado", "cep", "inscricaoEstadual", "statusInscEstadual"})
    @ManyToOne
    @JoinColumn(name = "id_deposito_entrada", referencedColumnName = "id")
    private Produtor depositoEntrada;

    @EntityProperties(value = {"id", "cpf", "cnpj", "codigo", "nome", "fazenda", "endFazenda", "bairro", "municipio", "estado", "cep", "inscricaoEstadual", "statusInscEstadual"})
    @ManyToOne
    @JoinColumn(name = "id_deposito_saida", referencedColumnName = "id")
    private Produtor depositoSaida;

    @EntityProperties(value = {"id", "codigo", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_safra", referencedColumnName = "id")
    private Safra safra;

    @EntityProperties(value = {"id", "codigo", "descricao", "unidadeMedida", "valorUnitario", "realizarConversaoLitragem", "necessitaTreinamentoMotorista", "lacrarCarga", "realizarAnaliseQualidade", "buscarCertificadoQualidade",
            "necessitaMaisDeUmaAnalise", "possuiEtapaCarregamento", "gerarNumeroCertificado", "possuiResolucaoAnp", "exibirEspecificacaoNaImpressaoDoLaudo", "toleranciaEmKg", "tipoTransporte"})
    @ManyToOne
    @JoinColumn(name = "id_material", referencedColumnName = "id")
    private Material material;

    @EntityProperties(value = {"id", "codigo", "nome", "cpf"})
    @ManyToOne
    @JoinColumn(name = "id_motorista", referencedColumnName = "id")
    private Motorista motorista;

    @EntityProperties(value = {"id", "codigo", "placa1", "placa2", "placa3", "municipio", "renavam", "chassi", "marca", "tipo.id", "tipo.descricao", "tipo.peso", "tipo.pesoTara", "tipo.pesoLiquido", "tipo.tipo", "tipo.textoTipo", "cor",
            "eixos", "ufPlaca1", "ufPlaca2", "ufPlaca3", "dataVencimentoPlaca1", "dataVencimentoPlaca2", "dataVencimentoPlaca3", "dataCivPlaca1", "dataCivPlaca2", "dataCivPlaca3", "dataCalibragemPlaca1", "dataCalibragemPlaca2",
            "dataCalibragemPlaca3", "dataCipp1", "dataCipp2", "dataCipp3", "dataVencimentoCr", "dataVencimentoAatipp", "antt"}, collecions = {"tipo.setas"})
    @ManyToOne
    @JoinColumn(name = "id_placa_cavalo", referencedColumnName = "id")
    private Veiculo placaCavalo;

    @EntityProperties(value = {"id", "codigo", "placa1", "placa2", "placa3", "municipio", "renavam", "chassi", "marca", "tipo.id", "tipo.descricao", "tipo.peso", "tipo.pesoTara", "tipo.pesoLiquido", "tipo.tipo", "tipo.textoTipo", "cor",
            "eixos", "ufPlaca1", "ufPlaca2", "ufPlaca3", "dataVencimentoPlaca1", "dataVencimentoPlaca2", "dataVencimentoPlaca3", "dataCivPlaca1", "dataCivPlaca2", "dataCivPlaca3", "dataCalibragemPlaca1", "dataCalibragemPlaca2",
            "dataCalibragemPlaca3", "dataCipp1", "dataCipp2", "dataCipp3", "dataVencimentoCr", "dataVencimentoAatipp", "antt"}, collecions = {"tipo.setas"})
    @ManyToOne
    @JoinColumn(name = "id_placa_cavalo_um", referencedColumnName = "id")
    private Veiculo placaCavaloUm;

    @EntityProperties(value = {"id", "codigo", "placa1", "placa2", "placa3", "municipio", "renavam", "chassi", "marca", "tipo.id", "tipo.descricao", "tipo.peso", "tipo.pesoTara", "tipo.pesoLiquido", "tipo.tipo", "tipo.textoTipo", "cor",
            "eixos", "ufPlaca1", "ufPlaca2", "ufPlaca3", "dataVencimentoPlaca1", "dataVencimentoPlaca2", "dataVencimentoPlaca3", "dataCivPlaca1", "dataCivPlaca2", "dataCivPlaca3", "dataCalibragemPlaca1", "dataCalibragemPlaca2",
            "dataCalibragemPlaca3", "dataCipp1", "dataCipp2", "dataCipp3", "dataVencimentoCr", "dataVencimentoAatipp", "antt"}, collecions = {"tipo.setas"})
    @ManyToOne
    @JoinColumn(name = "id_placa_cavalo_dois", referencedColumnName = "id")
    private Veiculo placaCavaloDois;

    @EntityProperties(value = {"id", "codigo", "descricao", "cnpj", "cpf", "tipoTransporte"})
    @ManyToOne
    @JoinColumn(name = "id_transportadora", referencedColumnName = "id")
    private Transportadora transportadora;

    @EntityProperties(value = {"id", "codigo", "descricao", "cnpj", "cpf"})
    @ManyToOne
    @JoinColumn(name = "id_transportadora_subcontratada", referencedColumnName = "id")
    private Transportadora transportadoraSubcontratada;

    @EntityProperties(value = {"id", "codigo", "descricao", "peso", "pesoTara", "pesoLiquido", "tipo", "textoTipo"})
    @ManyToOne
    @JoinColumn(name = "id_tipo_veiculo", referencedColumnName = "id")
    private TipoVeiculo tipoVeiculo;

    @EntityProperties(value = {"id", "tipoVeiculo.id", "descricao", "qtd", "padrao"})
    @ManyToOne
    @JoinColumn(name = "id_tipo_seta", referencedColumnName = "id")
    private TipoVeiculoSeta tipoSeta;

    @EntityProperties(value = {"id", "codigo", "descricao", "statusCadastroSap", "statusRegistro", "centro.id", "centro.codigo", "centro.descricao"})
    @ManyToOne
    @JoinColumn(name = "id_deposito", referencedColumnName = "id")
    private Deposito deposito;

    @EntityProperties(value = {"id", "codigo", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_centro_destino", referencedColumnName = "id")
    private Centro centroDestino;

    @EntityProperties(value = {"id"})
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_romaneio")
    private Romaneio romaneio;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_frete")
    private TipoFreteEnum tipoFrete;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_remessa")
    private StatusRemessaEnum statusRemessa;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_doc_transporte")
    private StatusDocTransporteEnum statusDocTransporte;

    @Column(name = "nr_remessa")
    private String numeroRemessa;

    @Column(name = "nr_ordem_transporte")
    private String numeroOrdemTransporte;

    @Column(name = "codigo_ordem_venda")
    private String codigoOrdemVenda;

    @Column(name = "item_ordem")
    private String itemOrdem;

    @Column(name = "numero_pedido")
    private String numeroPedido;

    @Column(name = "item_pedido")
    private String itemPedido;

    @Column(name = "numero_nf_transporte")
    private String numeroNotaFiscalTransporte;

    @Column(name = "serie_nota_fiscal_transporte")
    private String serieNotaFiscalTransporte;

    @Column(name = "remessa_nfe")
    private String remessaNfe;

    @Column(name = "chave_acesso_nfe")
    private String chaveAcessoNfe;

    @Column(name = "numero_nfe")
    private String numeroNfe;

    @Column(name = "serie_nfe")
    private String serieNfe;

    @Column(name = "numero_aleatorio_chave_acesso")
    private String numeroAleatorioChaveAcesso;

    @Column(name = "numero_log_nfe")
    private String numeroLog;

    @Column(name = "digito_verificador_nfe")
    private String digitoVerificadorNfe;

    @Column(name = "armazem_terceiros_referencia")
    private String armazemTerceirosReferencia;

    @Column(name = "medida_nfe")
    private String medidaNfe;

    @Column(name = "empresa_ordem")
    private String empresaOrdem;

    @Column(name = "local_expedicao")
    private String localExpedicao;

    @Column(name = "placa_carreta_um")
    private String placaCarretaUm;

    @Column(name = "placa_carreta_dois")
    private String placaCarretaDois;

    @Column(name = "unidade_medida_ordem")
    private String unidadeMedidaOrdem;

    @Column(name = "request_key")
    @Setter(AccessLevel.NONE)
    private String requestKey;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_nfe")
    private Date dataNfe;

    @Column(name = "valor_unitario_pedido", nullable = false, columnDefinition = "numeric(19,6) default 0")
    private BigDecimal valorUnitarioPedido;

    @Column(name = "valor_unitario_nfe", nullable = false, columnDefinition = "numeric(19,6) default 0")
    private BigDecimal valorUnitarioNfe;

    @Column(name = "valor_total_nfe")
    private BigDecimal valorTotalNfe;

    @Column(name = "peso_total_nfe")
    private BigDecimal pesoTotalNfe;

    @Column(name = "vlr_saldo_pedido")
    private BigDecimal saldoPedido;

    @Column(name = "vlr_tolerancia_pedido")
    private BigDecimal toleranciaPedido;

    @Column(name = "vlr_rateio")
    private BigDecimal rateio;

    @Column(name = "vlr_quebra")
    private BigDecimal quebra;

    @Column(name = "quantidade_carregar")
    private BigDecimal quantidadeCarregar;

    @Column(name = "quantidade_prevista")
    private BigDecimal quantidadePrevista;

    @Column(name = "saldo_ordem_venda")
    private BigDecimal saldoOrdemVenda;

    @Column(name = "vlr_peso_liquido_umido")
    private BigDecimal pesoLiquidoUmido;

    @Column(name = "saldo_estoque_produto_acabado")
    private BigDecimal saldoEstoqueProdutoAcabado;

    @Column(name = "item_transporte")
    private Long itemTransporte;

    @Column(name = "documento_faturamento")
    private Long documentoFaturamento;

    @Column(name = "transportadora_buscada_pela_ordem", nullable = false, columnDefinition = "boolean default false")
    private boolean transportadoraBuscadaPelaOrdem;

    @Column(name = "transportadora_sub_buscada_pela_ordem", nullable = false, columnDefinition = "boolean default false")
    private boolean transportadoraSubBuscadaPelaOrdem;

    @Transient
    private BigDecimal rateioConvertido;

    @Transient
    private Date dataFiltroInicio;

    @Transient
    private Date dataFiltroFim;

    public void setRequestKey(String requestKey) {

        if (StringUtil.isNotNullEmpty(requestKey)) {
            this.requestKey = requestKey;
        }
    }

    public ItemNFPedido() {

    }

    public ItemNFPedido(Produtor depositoEntrada, Produtor depositoSaida, Produtor produtor, Safra safra, Material material, Motorista motorista, Veiculo placaCavalo, Veiculo placaCavaloUm, Veiculo placaCavaloDois,
                        Transportadora transportadora, Transportadora transportadoraSubcontratada, TipoVeiculo tipoVeiculo, TipoVeiculoSeta tipoSeta, Deposito deposito, TipoFreteEnum tipoFrete, String numeroPedido, String itemPedido,
                        String numeroNotaFiscalTransporte, String serieNotaFiscalTransporte, String remessaNfe, String chaveAcessoNfe, String numeroNfe, String serieNfe, String numeroAleatorioChaveAcesso, String numeroLog, String digitoVerificadorNfe,
                        String armazemTerceirosReferencia, String empresaOrdem, String localExpedicao, String placaCarretaUm, String placaCarretaDois, String unidadeMedidaOrdem, Date dataNfe, BigDecimal valorUnitarioPedido,
                        BigDecimal valorUnitarioNfe, BigDecimal valorTotalNfe, BigDecimal pesoTotalNfe, BigDecimal saldoPedido, BigDecimal rateio, BigDecimal quebra, BigDecimal toleranciaPedido
    ) {
        this.setDepositoEntrada(depositoEntrada);
        this.setDepositoSaida(depositoSaida);
        this.setProdutor(produtor);
        this.setSafra(safra);
        this.setMaterial(material);
        this.setMotorista(motorista);
        this.setPlacaCavalo(placaCavalo);
        this.setPlacaCavaloUm(placaCavaloUm);
        this.setPlacaCavaloDois(placaCavaloDois);
        this.setTransportadora(transportadora);
        this.setTransportadoraSubcontratada(transportadoraSubcontratada);
        this.setTipoVeiculo(tipoVeiculo);
        this.setTipoSeta(tipoSeta);
        this.setDeposito(deposito);
        this.setTipoFrete(tipoFrete);
        this.setNumeroPedido(numeroPedido);
        this.setItemPedido(itemPedido);
        this.setNumeroNotaFiscalTransporte(numeroNotaFiscalTransporte);
        this.setSerieNotaFiscalTransporte(serieNotaFiscalTransporte);
        this.setRemessaNfe(remessaNfe);
        this.setChaveAcessoNfe(chaveAcessoNfe);
        this.setNumeroNfe(numeroNfe);
        this.setSerieNfe(serieNfe);
        this.setNumeroAleatorioChaveAcesso(numeroAleatorioChaveAcesso);
        this.setNumeroLog(numeroLog);
        this.setDigitoVerificadorNfe(digitoVerificadorNfe);
        this.setArmazemTerceirosReferencia(armazemTerceirosReferencia);
        this.setEmpresaOrdem(empresaOrdem);
        this.setLocalExpedicao(localExpedicao);
        this.setPlacaCarretaUm(placaCarretaUm);
        this.setPlacaCarretaDois(placaCarretaDois);
        this.setUnidadeMedidaOrdem(unidadeMedidaOrdem);
        this.setDataNfe(dataNfe);
        this.setValorUnitarioPedido(valorUnitarioPedido);
        this.setValorUnitarioNfe(valorUnitarioNfe);
        this.setValorTotalNfe(valorTotalNfe);
        this.setPesoTotalNfe(pesoTotalNfe);
        this.setSaldoPedido(saldoPedido);
        this.setRateio(rateio);
        this.setQuebra(quebra);
        this.setToleranciaPedido(toleranciaPedido);
    }

    public ItemNFPedido(Safra safra, Material material, Motorista motorista, Veiculo placaCavalo, Veiculo placaCavaloUm, Veiculo placaCavaloDois, Transportadora transportadora, Transportadora transportadoraSubcontratada,
                        TipoVeiculo tipoVeiculo, TipoVeiculoSeta tipoSeta, Deposito deposito, TipoFreteEnum tipoFrete, String codigoOrdemVenda, String itemOrdem, String numeroPedido, String itemPedido, String empresaOrdem,
                        String localExpedicao, String placaCarretaUm, String placaCarretaDois, String unidadeMedidaOrdem, Date dataNfe, BigDecimal rateio, BigDecimal quebra, BigDecimal quantidadeCarregar, BigDecimal saldoOrdemVenda,
                        BigDecimal quantidadePrevista, BigDecimal saldoEstoqueProdutoAcabado, Cliente cliente) {

        this.setSafra(safra);
        this.setMaterial(material);
        this.setMotorista(motorista);
        this.setPlacaCavalo(placaCavalo);
        this.setPlacaCavaloUm(placaCavaloUm);
        this.setPlacaCavaloDois(placaCavaloDois);
        this.setTransportadora(transportadora);
        this.setTransportadoraSubcontratada(transportadoraSubcontratada);
        this.setTipoVeiculo(tipoVeiculo);
        this.setTipoSeta(tipoSeta);
        this.setDeposito(deposito);
        this.setTipoFrete(tipoFrete);
        this.setCodigoOrdemVenda(codigoOrdemVenda);
        this.setItemOrdem(itemOrdem);
        this.setNumeroPedido(numeroPedido);
        this.setItemPedido(itemPedido);
        this.setEmpresaOrdem(empresaOrdem);
        this.setLocalExpedicao(localExpedicao);
        this.setPlacaCarretaUm(placaCarretaUm);
        this.setPlacaCarretaDois(placaCarretaDois);
        this.setUnidadeMedidaOrdem(unidadeMedidaOrdem);
        this.setDataNfe(dataNfe);
        this.setRateio(rateio);
        this.setQuebra(quebra);
        this.setQuantidadeCarregar(quantidadeCarregar);
        this.setSaldoOrdemVenda(saldoOrdemVenda);
        this.setSaldoEstoqueProdutoAcabado(saldoEstoqueProdutoAcabado);
        this.setCliente(cliente);
        this.setQuantidadePrevista(quantidadePrevista);
    }

    public BigDecimal getDivPeso() {

        return this.rateio;
    }

    public NFeDTO toNFe() {

        return new NFeDTO(this);
    }

    public DivisaoCargaDTO toRelatorioDto() {

        final DivisaoCargaDTO dto = new DivisaoCargaDTO();

        dto.setNumeroPedido(this.getNumeroPedido());

        dto.setItemPedido(this.getItemPedido());

        dto.setNfe(this.getNumeroNfe());

        dto.setSerieNfe(this.getSerieNfe());

        dto.setDataNfe(this.getDataNfe());

        dto.setValorUnitarioNfe(StringUtil.toMilhar(ObjetoUtil.isNotNull(this.getValorUnitarioNfe()) ? this.getValorUnitarioNfe() : BigDecimal.ZERO, 6));

        dto.setSaldoPedido(StringUtil.toMilhar(ObjetoUtil.isNotNull(this.getSaldoPedido()) ? this.getSaldoPedido() : BigDecimal.ZERO));

        dto.setToleranciaPedido(StringUtil.toMilhar(ObjetoUtil.isNotNull(this.getToleranciaPedido()) ? this.getToleranciaPedido() : BigDecimal.ZERO));

        return dto;
    }

    public ItemOrdemVendaDTO toItemOrdemVendaDTO() {

        final ItemOrdemVendaDTO dto = new ItemOrdemVendaDTO();

        dto.setCodigoOrdemVenda(this.getCodigoOrdemVenda());

        dto.setItemOrdemVenda(this.getItemOrdem());

        dto.setDataNfe(this.getDataNfe());

        dto.setValorUnitarioNfe(StringUtil.toMilhar(ObjetoUtil.isNotNull(this.getValorUnitarioNfe()) ? this.getValorUnitarioNfe() : BigDecimal.ZERO, 6));

        dto.setSaldoOrdemVenda(StringUtil.toMilhar(ObjetoUtil.isNotNull(this.getSaldoOrdemVenda()) ? this.getSaldoOrdemVenda() : BigDecimal.ZERO));

        dto.setQntdCarregar(this.getQuantidadeCarregar());

        dto.setCliente(ObjetoUtil.isNotNull(this.getCliente()) ? this.getCliente().toString() : StringUtil.empty());

        dto.setOperadorLogistico(ObjetoUtil.isNotNull(this.getOperadorLogistico()) ? this.getOperadorLogistico().toString() : StringUtil.empty());

        dto.setNumeroRemessa(this.getNumeroRemessa());

        dto.setNumeroOrdemTransporte(this.getNumeroOrdemTransporte());

        return dto;
    }

    @Override
    public int compareTo(ItemNFPedido o) {

        if (ObjetoUtil.isNotNull(o.getId()) && ObjetoUtil.isNotNull(getId())) {

            return o.getId().compareTo(getId());
        }

        return o.getUuid().compareTo(getUuid());
    }

}
