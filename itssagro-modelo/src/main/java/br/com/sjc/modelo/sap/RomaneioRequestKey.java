package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.EntidadeGenerica;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_romaneio_request_key", schema = "sap")
public class RomaneioRequestKey extends EntidadeGenerica {

    private static final long serialVersionUID = -3351987708785118658L;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_romaneio")
    private Romaneio romaneio;

    @Column(name = "requestKey")
    private String requestKey;

}
