package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.enums.TipoFreteEnum;
import br.com.sjc.modelo.sap.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class ItemNFPedidoDTO extends DataDTO {

    private Cliente cliente;

    private String numeroPedido;

    private Material material;

    private BigDecimal valorUnitarioPedido;

    private BigDecimal saldoPedido;

    private Produtor produtor;

    private Safra safra;

    private Motorista motorista;

    private Veiculo placaCavalo;

    private Veiculo placaCavaloUm;

    private Veiculo placaCavaloDois;

    private Transportadora transportadora;

    private Transportadora transportadoraSubcontratada;

    private TipoVeiculo tipoVeiculo;

    private Deposito deposito;

    private Romaneio romaneio;

    private TipoFreteEnum tipoFrete;

    private String itemPedido;

    private String numeroNotaFiscalTransporte;

    private String serieNotaFiscalTransporte;

    private String remessaNfe;

    private String chaveAcessoNfe;

    private String numeroNfe;

    private String serieNfe;

    private String numeroAleatorioChaveAcesso;

    private String numeroLog;

    private String digitoVerificadorNfe;

    private String armazemTerceirosReferencia;

    private Date dataNfe;

    private BigDecimal valorUnitarioNfe;

    private BigDecimal valorTotalNfe;

    private BigDecimal pesoTotalNfe;

    private BigDecimal rateio;

    private BigDecimal quebra;

    public ItemNFPedidoDTO(ItemNFPedido item) {
        
        this();
        setId(item.getId());
        setCliente(item.getCliente());
        setNumeroPedido(item.getNumeroPedido());
        setMaterial(item.getMaterial());
        setValorUnitarioPedido(item.getValorUnitarioPedido());
        setSaldoPedido(item.getSaldoPedido());
        setProdutor(item.getProdutor());
        setSafra(item.getSafra());
        setMotorista(item.getMotorista());
        setPlacaCavalo(item.getPlacaCavalo());
        setPlacaCavaloUm(item.getPlacaCavaloUm());
        setPlacaCavaloDois(item.getPlacaCavaloDois());
        setTransportadora(item.getTransportadora());
        setTransportadoraSubcontratada(item.getTransportadoraSubcontratada());
        setTipoVeiculo(item.getTipoVeiculo());
        setDeposito(item.getDeposito());
        setRomaneio(item.getRomaneio());
        setTipoVeiculo(item.getTipoVeiculo());
        setItemPedido(item.getItemPedido());
        setNumeroNotaFiscalTransporte(item.getNumeroNotaFiscalTransporte());
        setSerieNotaFiscalTransporte(item.getSerieNotaFiscalTransporte());
        setRemessaNfe(item.getRemessaNfe());
        setChaveAcessoNfe(item.getChaveAcessoNfe());
        setNumeroNfe(item.getNumeroNfe());
        setSerieNfe(item.getSerieNfe());
        setNumeroAleatorioChaveAcesso(item.getNumeroAleatorioChaveAcesso());
        setNumeroLog(item.getNumeroLog());
        setDigitoVerificadorNfe(item.getDigitoVerificadorNfe());
        setArmazemTerceirosReferencia(item.getArmazemTerceirosReferencia());
        setDataNfe(item.getDataNfe());
        setValorUnitarioPedido(item.getValorUnitarioPedido());
        setValorTotalNfe(item.getValorTotalNfe());
        setPesoTotalNfe(item.getPesoTotalNfe());
        setRateio(item.getRateio());
        setQuebra(item.getQuebra());
    }

    public ItemNFPedidoDTO(String numeroRomaneio, Integer numeroCartaoAcesso, Date dataAlteracao, String placa, String motoristaCodigo, String motoristaNome) {

        this.setRomaneio(new Romaneio());
        this.getRomaneio().setDataAlteracao(dataAlteracao);
        this.getRomaneio().setNumeroRomaneio(numeroRomaneio);
        this.getRomaneio().setNumeroCartaoAcesso(numeroCartaoAcesso);
        this.setPlacaCavalo(new Veiculo());
        this.getPlacaCavalo().setPlaca1(placa);
        this.setMotorista(new Motorista());
        this.getMotorista().setCodigo(motoristaCodigo);
        this.getMotorista().setNome(motoristaNome);
    }

}
