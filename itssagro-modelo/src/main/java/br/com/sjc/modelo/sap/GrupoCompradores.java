package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.EntidadeGenerica;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_grupo_compradores", schema = "sap")
public class GrupoCompradores extends EntidadeGenerica {

    @Column
    private String codigo;

    @Column
    private String descricao;

    @Override
    public String toString() {

        return this.codigo + " - " + this.descricao;
    }

}
