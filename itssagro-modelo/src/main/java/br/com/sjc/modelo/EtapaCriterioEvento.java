package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.EventoAutomacaoEnum;
import br.com.sjc.modelo.enums.StatusAutomacaoEnum;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_etapa_criterio_evento")
public class EtapaCriterioEvento extends EntidadeAutenticada {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_etapa_criterio")
    @EntityProperties(value = {"id", "email", "numeroCriterio", "repetirEtapa", "tipo", "fluxoEtapa.id", "fluxoEtapa.sequencia", "fluxoEtapa.etapa", "proximaEtapa.id", "proximaEtapa.sequencia", "proximaEtapa.etapa"}, collecions = {
            "condicoes", "acoes", "eventos"})
    @JsonIgnore
    private EtapaCriterio etapaCriterio;

    @EntityProperties(value = {"id", "eventoAutomacaoGraos.id", "eventoAutomacaoGraos.descricao", "sequencia", "statusEstornoAutomacao", "mensagemEstornoAutomacao", "etapa"}, collecions = {"eventos"})
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_evento_automacao_graos_etapa")
    @JsonIgnore
    private EventoAutomacaoGraosEtapa eventoAutomacaoGraosEtapa;

    @EntityProperties(value = {"id", "codigo", "codigoFormatado", "descricao"}, collecions = {"centros", "hardwares"})
    @ManyToOne
    @JoinColumn(name = "id_local")
    private Local local;

    @EntityProperties(value = {"id", "codigo", "codigoFormatado", "descricao", "endereco", "identificacao", "porta"}, collecions = {"centros"})
    @ManyToOne
    @JoinColumn(name = "id_hardware")
    private Hardware hardware;

    @Column(name = "numero_criterio")
    private Integer numeroEvento;

    @Enumerated(EnumType.STRING)
    private EventoAutomacaoEnum evento;

    @Column(name = "status_automacao")
    @Enumerated(EnumType.STRING)
    private StatusAutomacaoEnum statusAutomacao;

    @Column(name = "exibir_mensagem")
    private boolean exibirMensagem = false;

    @Column(name = "criterio_tipo")
    private String criterioTipo;

    @Column(name = "descricao_status")
    private String descricaoStatus;

    @Column(name = "mensagem_um")
    private String mensagemUm;

    @Column(name = "mensagem_dois")
    private String mensagemDois;

    @Column(name = "mensagem_tres")
    private String mensagemTres;

}
