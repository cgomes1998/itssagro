package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.StatusLaudoEnum;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_laudo")
@SuppressWarnings("all")
public class Laudo extends EntidadeAutenticada {

    @EntityProperties(value = {"id", "codigo", "descricao", "possuiResolucaoAnp"})
    @ManyToOne
    @JoinColumn(name = "id_material", referencedColumnName = "id")
    private Material material;

    @EntityProperties(value = {"id", "laudo.id", "grausInpm",
            "caracteristica", "metodologia", "tipoValidacao", "especificacao",
            "unidade", "subCaracteristica", "condicao", "valorInicial", "valorFinal", "laboratorio",
            "boletim", "resultado", "mostrarEspecificacao"}, targetEntity = LaudoItem.class)
    @OrderBy("index asc")
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "laudo", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    private List<LaudoItem> itens = new ArrayList<>();

    @EntityProperties(value = {"id", "nome", "login", "cpf"})
    @ManyToOne
    @JoinColumn(name = "id_usuario_autor_cadastro", referencedColumnName = "id", updatable = false, insertable = false)
    private Usuario usuario;

    @Column(name = "tanque")
    private String tanque;

    @Column(name = "numero_certificado")
    private String numeroCertificado;

    @Column(name = "lacre_amostra")
    private String lacreAmostra;

    @EntityProperties(value = {"id", "codigo", "dataFabricacao", "dataVencimento", "status", "material.id", "material.codigo", "material.descricao"})
    @ManyToOne
    @JoinColumn(name = "id_lote_analise_qualidade")
    private LoteAnaliseQualidade loteAnaliseQualidade;

    @Column(name = "status_laudo")
    @Enumerated(EnumType.STRING)
    private StatusLaudoEnum statusLaudo;

    @EntityProperties(value = {"id", "codigo", "descricao", "dataValidade", "assinatura", "arquivo.id", "arquivo.nome", "arquivo.mimeType", "arquivo.base64"})
    @ManyToOne
    @JoinColumn(name = "id_assinatura", referencedColumnName = "id")
    private Documento assinatura;

    @Column(name = "volume_certificado")
    private String volumeCertificado;

    @Column(name = "unidade_medida_certificado")
    private String unidadeMedidaCertificado;

    @EntityProperties(value = {"id", "codigo", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_materia_prima", referencedColumnName = "id")
    private MateriaPrima materiaPrima;

    @Column(name = "observacao", columnDefinition = "text")
    private String observacao;

    public Laudo() {

    }

    public Laudo(Long id, String numeroCertificado) {

        this.setId(id);
        this.numeroCertificado = numeroCertificado;
    }

}
