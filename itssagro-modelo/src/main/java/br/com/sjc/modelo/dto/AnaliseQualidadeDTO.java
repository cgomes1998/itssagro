package br.com.sjc.modelo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AnaliseQualidadeDTO {

    private String lacreAmostra;

    private String lote;
}
