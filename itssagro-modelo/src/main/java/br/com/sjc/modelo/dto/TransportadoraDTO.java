package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransportadoraDTO extends DataDTO {

    @ProjectionProperty
    private String codigo;

    @ProjectionProperty
    private String descricao;

    @ProjectionProperty
    private String cpf;

    @ProjectionProperty
    private String cnpj;

    @ProjectionProperty
    private StatusCadastroSAPEnum statusCadastroSap;

    @ProjectionProperty
    private StatusRegistroEnum statusRegistro;

    public String getCnpjCpf() {

        return StringUtil.isNotNullEmpty(cnpj) ? cnpj : cpf;
    }

    public String getStatusSapDescricao() {

        return this.statusCadastroSap.getDescricao();
    }

    public String getStatusRegistroDescricao() {

        return this.statusRegistro.getDescricao();
    }

}
