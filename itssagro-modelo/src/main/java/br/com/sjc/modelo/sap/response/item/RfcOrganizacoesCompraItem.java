package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.sap.OrganizacaoCompra;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import br.com.sjc.util.DateUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class RfcOrganizacoesCompraItem extends JCoItem {

    @SAPColumn(name = "EKORG")
    private String codigo;

    @SAPColumn(name = "EKOTX")
    private String descricao;

    public OrganizacaoCompra toEntidade() {

        OrganizacaoCompra entidade = new OrganizacaoCompra();

        entidade.setDataCadastro(DateUtil.hoje());

        entidade.setCodigo(codigo);

        entidade.setDescricao(descricao);

        return entidade;
    }
}
