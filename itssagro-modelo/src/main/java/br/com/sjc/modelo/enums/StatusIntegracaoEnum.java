package br.com.sjc.modelo.enums;

import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;

import java.util.Arrays;

@Getter
public enum StatusIntegracaoEnum {

    W(false, "Aguardando", "loadingEtapa"),

    M(true, "Concluído Manualmente", "fas fa-check-circle verde"),

    A(true, "Concluído Automaticamente", "fas fa-check-circle verde"),

    E(false, "Erro", "fas fa-exclamation-triangle vermelho"),

    C(false, "Cancelada", "fas fa-ban"),

    N(true, "Estornado", "fas fa-trash-alt vermelho-escuro"),

    I(true, "Ignorada", "fas fa-arrow-down");

    private boolean concluido;

    private String descricao;

    private String _class;

    private StatusIntegracaoEnum(final boolean concluido, final String descricao, final String _class) {

        this.concluido = concluido;

        this.descricao = descricao;

        this._class = _class;
    }

    public static StatusIntegracaoEnum get(final String status) {

        if (ObjetoUtil.isNotNull(status)) {

            return Arrays.asList(StatusIntegracaoEnum.values()).stream().filter(i -> i.toString().equals(status.toUpperCase())).findFirst().orElse(null);
        }

        return null;
    }

    public static StatusIntegracaoEnum getByName(String str) {

        if (StringUtil.isNotNullEmpty(str)) {

            return Arrays.asList(values()).stream().filter(i -> i.name().equals(str)).findFirst().orElse(null);
        }

        return null;
    }
}
