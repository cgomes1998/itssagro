package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.MotivoTipoEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by julio.bueno on 24/06/2019.
 */
@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_motivo")
public class Motivo extends EntidadeAutenticada {

    @Column
    private String descricao;

    @Enumerated(EnumType.STRING)
    @Column
    private MotivoTipoEnum tipo;

    @Column(name = "conferencia_peso", nullable = false, columnDefinition = "boolean default false")
    private Boolean conferenciaPeso = Boolean.FALSE;

    @Column(name = "buscar_dados_romaneio", nullable = false, columnDefinition = "boolean default false")
    private Boolean buscarDadosRomaneio = Boolean.FALSE;

    public Motivo() {
        super();
    }

    public Motivo(
            String descricao,
            MotivoTipoEnum tipo
    ) {
        this.descricao = descricao;
        this.tipo = tipo;
    }
}
