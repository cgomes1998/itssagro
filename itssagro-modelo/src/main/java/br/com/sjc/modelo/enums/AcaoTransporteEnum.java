package br.com.sjc.modelo.enums;

import lombok.Getter;

@Getter
public enum AcaoTransporteEnum {

    A ( "Inserir" ),
    C ( "Modificar" ),
    D ( "Eliminar" );

    private String descricao;

    AcaoTransporteEnum ( String descricao ) {

        this.descricao = descricao;
    }
}
