package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.sap.*;
import br.com.sjc.util.anotation.ProjectionConfigurationDTO;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ProjectionConfigurationDTO
public class ItemNFPedidoListagemDTO extends DataDTO {

    @ProjectionProperty(values = {"id", "numeroRomaneio"})
    private Romaneio romaneio;

    @ProjectionProperty(values = {"id", "placa1"})
    private Veiculo placaCavalo;

    @ProjectionProperty(values = {"id", "codigo", "nome", "cpf", "cnpj"})
    private Produtor produtor;

    @ProjectionProperty(values = {"id", "codigo", "descricao"})
    private Material material;

    @ProjectionProperty(values = {"id", "codigo", "descricao"})
    private Safra safra;

    @ProjectionProperty(values = {"id", "codigo", "descricao", "cnpj", "cpf"})
    private Transportadora transportadora;

    @ProjectionProperty(values = {"id", "codigo", "nome", "cpf"})
    private Motorista motorista;

    @ProjectionProperty(values = {"id", "codigo", "nome", "cpfCnpj"})
    private Cliente cliente;

}
