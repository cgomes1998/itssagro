package br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento;

import br.com.sjc.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Conteudo2 {

    private int idAgendamento;

    private String dataAgendamento;

    private String status;

    private String tipoOperacaoAgendamento;

    private String periodoInicialFinal;

    private String nomeTransportador;

    private String cnpjCpfTransportador;

    private int idPlantaCargaDescarga;

    private String codigoPlantaCargaDescarga;

    private String plantaCargaDescarga;

    private String tipoAgendamento;

    private int statusAtualAgendamento;

    private String statusAtualDescricaoAgendamento;

    private Object numeroNota;

    private Object observacaoAgendamento;

    private Veiculos veiculos;

    private Motorista motorista;

//    private Compartimentosseta compartimentosseta;

    private List<Orden> ordens;

    private int conteudoQuantidade;

    public Date getDataAgendamentoInicialDate() {

        Date format = DateUtil.format(this.dataAgendamento, "yyyy-MM-dd");

        String[] splitPeriodoInicialFinal = periodoInicialFinal.split(" - ");

        String[] splitPeriodoInicial = splitPeriodoInicialFinal[0].split(":");

        DateUtil.setarHora(format, Integer.valueOf(splitPeriodoInicial[0].trim()), Integer.valueOf(splitPeriodoInicial[1].trim()));

        return format;
    }

}
