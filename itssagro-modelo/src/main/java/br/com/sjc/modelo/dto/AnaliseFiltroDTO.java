package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.sap.ItemNFPedido;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import lombok.Data;

import java.util.Objects;

@Data
public class AnaliseFiltroDTO {

    private Long idRomaneio;

    private Material material;

    private String tanque;

    public AnaliseFiltroDTO () {

        super ();
    }

    public AnaliseFiltroDTO (
            final
            Romaneio filtro
    ) {

        if ( ObjetoUtil.isNotNull ( filtro ) && ColecaoUtil.isNotEmpty ( filtro.getItens () ) ) {

            this.setMaterial ( filtro.getItens ().stream ().map ( ItemNFPedido::getMaterial ).filter ( Objects::nonNull ).findFirst ().orElse ( null ) );
        }

        if ( ObjetoUtil.isNotNull ( filtro ) && ObjetoUtil.isNotNull ( filtro.getConversaoLitragem () ) && ObjetoUtil.isNotNull ( filtro.getConversaoLitragem ().getTanque () ) ) {

            this.setTanque ( filtro.getConversaoLitragem ().getTanque ().toString () );
        }
    }
}
