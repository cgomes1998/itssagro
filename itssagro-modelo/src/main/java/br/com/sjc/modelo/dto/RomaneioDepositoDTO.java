package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.DirecaoOperacaoDepositoEnum;
import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.util.BigDecimalUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
@Setter
public class RomaneioDepositoDTO {

    private String operacao;

    private String depositoEntrada;

    private String depositoSaida;

    private DirecaoOperacaoDepositoEnum direcaoOperacaoDeposito;

    private String numeroRomaneio;

    private StatusIntegracaoSAPEnum statusIntegracaoSAP;

    private Date dataCriacaoRomaneio;

    private String veiculo;

    private String depositante;

    private String material;

    private Date dtSimplesEntradaReferencia;

    private Long idClassificacao;

    private Date dataClassificacao;

    private Date dataPrimeiraPesagem;

    private Date dataSegundaPesagem;

    private Long idPesagem;

    private BigDecimal pesoBruto;

    private BigDecimal pesoTara;

    private BigDecimal pesoLiquidoUmido;

    private BigDecimal pesoBrutoOrigem;

    private BigDecimal pesoTaraOrigem;

    private BigDecimal pesoLiquidoUmidoOrigem;

    private BigDecimal pesoLiquidoSecoOrigem;

    private BigDecimal rateio;

    private BigDecimal quantidadeNfe;

    private String requestKey;

    public RomaneioDepositoDTO() {

    }

    public RomaneioDepositoDTO(
            String operacao,
            String codigoDepositoEntradaCabecalho,
            String nomeDepositoEntradaCabecalho,
            String codigoDepositoEntradaItem,
            String nomeDepositoEntradaItem,
            String codigoDepositoSaidaCabecalho,
            String nomeDepositoSaidaCabecalho,
            String codigoDepositoSaidaItem,
            String nomeDepositoSaidaItem,
            DirecaoOperacaoDepositoEnum direcaoOperacaoDeposito,
            String numeroRomaneio,
            StatusIntegracaoSAPEnum statusIntegracaoSAP,
            Date dataCadastroRomaneio,
            String placa1VeiculoCabecalho,
            String placa1VeiculoItem,
            String codigoProdutorCabecalho,
            String nomeProdutorCabecalho,
            String codigoProdutorItem,
            String nomeProdutorItem,
            String codigoMaterialCabecalho,
            String descricaoMaterialCabecalho,
            String codigoMaterialItem,
            String descricaoMaterialItem,
            Date dtSimplesEntradaReferencia,
            Long idClassificacao,
            Date dataClassificacao,
            Long idPesagem,
            BigDecimal pesoInicial,
            BigDecimal pesoFinal,
            BigDecimal pesoBrutoOrigem,
            BigDecimal pesoTaraOrigem,
            BigDecimal pesoLiquidoUmidoOrigem,
            BigDecimal pesoLiquidoSecoOrigem,
            BigDecimal rateio,
            String requestKeyRomaneio,
            String requestKeyItem,
            BigDecimal pesoTotalNfe,
            BigDecimal quebra
    ) {

        this.setOperacao(operacao);

        this.setDepositoEntrada(concatenarItensNaoNulos(codigoDepositoEntradaCabecalho, nomeDepositoEntradaCabecalho, codigoDepositoEntradaItem, nomeDepositoEntradaItem));

        this.setDepositoSaida(concatenarItensNaoNulos(codigoDepositoSaidaCabecalho, nomeDepositoSaidaCabecalho, codigoDepositoSaidaItem, nomeDepositoSaidaItem));

        this.setDirecaoOperacaoDeposito(direcaoOperacaoDeposito);

        this.setNumeroRomaneio(numeroRomaneio);

        this.setStatusIntegracaoSAP(statusIntegracaoSAP);

        this.setDataCriacaoRomaneio(dataCadastroRomaneio);

        this.setVeiculo(StringUtil.isNotNullEmpty(placa1VeiculoCabecalho) ? StringUtil.toPlaca(placa1VeiculoCabecalho) : StringUtil.isNotNullEmpty(placa1VeiculoItem) ? StringUtil.toPlaca(placa1VeiculoItem) : StringUtil.empty());

        this.setDepositante(concatenarItensNaoNulos(codigoProdutorCabecalho, nomeProdutorCabecalho, codigoProdutorItem, nomeProdutorItem));

        this.setMaterial(concatenarItensNaoNulos(codigoMaterialCabecalho, descricaoMaterialCabecalho, codigoMaterialItem, descricaoMaterialItem));

        this.setDtSimplesEntradaReferencia(dtSimplesEntradaReferencia);

        this.setIdClassificacao(idClassificacao);

        this.setDataClassificacao(dataClassificacao);

        this.setIdPesagem(idPesagem);

        this.setPesoBruto(BigDecimalUtil.isMaiorZero(pesoInicial) ? pesoInicial : BigDecimal.ZERO);

        this.setPesoTara(BigDecimalUtil.isMaiorZero(pesoFinal) ? pesoFinal : BigDecimal.ZERO);

        this.setPesoLiquidoUmido(this.getPesoBruto().subtract(this.getPesoTara()));

        this.setRateio(BigDecimalUtil.isMaiorZero(rateio) ? rateio : BigDecimal.ZERO);

        this.setPesoBrutoOrigem(BigDecimalUtil.isMaiorZero(pesoBrutoOrigem) ? pesoBrutoOrigem : BigDecimal.ZERO);

        this.setPesoTaraOrigem(BigDecimalUtil.isMaiorZero(pesoTaraOrigem) ? pesoTaraOrigem : BigDecimal.ZERO);

        this.setPesoLiquidoUmidoOrigem(BigDecimalUtil.isMaiorZero(pesoLiquidoUmidoOrigem) ? pesoLiquidoUmidoOrigem : BigDecimal.ZERO);

        this.setPesoLiquidoSecoOrigem(BigDecimalUtil.isMaiorZero(pesoLiquidoSecoOrigem) ? pesoLiquidoSecoOrigem : BigDecimal.ZERO);

        this.setRequestKey(StringUtil.isNotNullEmpty(requestKeyRomaneio) ? requestKeyRomaneio : requestKeyItem);

        if (BigDecimalUtil.isMaiorZero(pesoTotalNfe)) {

            this.setQuantidadeNfe(pesoTotalNfe);

        } else if (BigDecimalUtil.isMaiorZero(quebra)) {

            this.setQuantidadeNfe(quebra);

        } else {

            this.setQuantidadeNfe(BigDecimal.ZERO);
        }
    }

    private String concatenarItensNaoNulos(String... valores) {

        List<String> objects = Arrays.asList(valores).stream().filter(Objects::nonNull).collect(Collectors.toList());

        return objects.stream().collect(Collectors.joining(" - "));
    }

}
