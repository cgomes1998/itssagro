package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.CondicaoEnum;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_analise_item", schema = "public")
public class AnaliseItem extends EntidadeAutenticada {

    @EntityProperties(value = {"id", "codigo", "material.id", "material.codigo", "material.descricao"}, collecions = {"itens"})
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_analise", nullable = false)
    @JsonIgnore
    private Analise analise;

    @Column
    private Integer ordem;

    @Column(name = "graus_inpm", nullable = false, columnDefinition = "boolean default false")
    private boolean grausInpm;

    @Column(name = "caracteristica")
    private String caracteristica;

    @Column(name = "metodologia")
    private String metodologia;

    @Column(name = "tipo_validacao")
    private String tipoValidacao;

    @Column(name = "especificacao")
    private String especificacao;

    @Column(name = "unidade")
    private String unidade;

    @Column(name = "sub_caracteristica")
    private String subCaracteristica;

    @Enumerated(EnumType.STRING)
    @Column(name = "condicao")
    private CondicaoEnum condicao;

    @Column(name = "valor_inicial")
    private String valorInicial;

    @Column(name = "valor_final")
    private String valorFinal;

    @Column(name = "laboratorio")
    private String laboratorio;

    @Column(name = "boletim")
    private String boletim;

    @Column(name = "mostrar_especificacao", nullable = false, columnDefinition = "boolean default true")
    private boolean mostrarEspecificacao;

    @Transient
    private String condicaoStr;

    @Transient
    private String resultado;

    public AnaliseItem() {

        super();
    }

    public AnaliseItem(final LaudoItem item) {

        this.setCaracteristica(item.getCaracteristica());

        this.setSubCaracteristica(item.getSubCaracteristica());

        if (ObjetoUtil.isNotNull(item.getCondicao())) {
            this.setCondicaoStr(item.getCondicao().getDescricao());
        }

        if (ObjetoUtil.isNull(item.getEspecificacao())) {
            if (ObjetoUtil.isNull(item.getCondicao())) {
                this.setEspecificacao(String.join(" - ", item.getValorInicial(), item.getValorFinal()));
            } else {
                this.setEspecificacao(String.join(" ", this.getCondicaoStr(), item.getValorInicial()));
            }
        }

        this.setUnidade(item.getUnidade());

        this.setLaboratorio(item.getLaboratorio());

        this.setBoletim(item.getBoletim());

        this.setMetodologia(item.getMetodologia());

        this.setGrausInpm(item.isGrausInpm());

        this.setMostrarEspecificacao(item.isMostrarEspecificacao());
    }

    @PostLoad
    public void postLoad() {

        if (ObjetoUtil.isNotNull(this.condicao)) {
            this.condicaoStr = this.condicao.getDescricao();
        }
        if (ObjetoUtil.isNull(this.especificacao)) {
            if (ObjetoUtil.isNull(this.condicaoStr)) {
                this.especificacao = String.join(" - ", this.valorInicial, this.valorFinal);
            } else {
                this.especificacao = String.join(" ", this.condicaoStr, this.valorInicial);
            }
        }
    }

    public String getDescricaoCondicao() {

        String valor = "";
        if (ObjetoUtil.isNotNull(this.getCondicao())) {
            valor += this.getCondicao().getDescricao();
            valor += " ";
            valor += this.getValorInicial();
        } else {
            valor += this.getValorInicial();
            valor += " - ";
            valor += this.getValorFinal();
        }
        return valor;
    }

}
