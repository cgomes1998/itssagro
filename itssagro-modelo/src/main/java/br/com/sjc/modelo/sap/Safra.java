package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.enums.StatusEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_safra", schema = "sap")
public class Safra extends EntidadeSAPSync {

    @Column(name = "desc_safra")
    private String descricao;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "status")
    private StatusEnum status;

    public Safra() {

        super();
    }

    public Safra(final Long id, final String descricao) {

        this.setId(id);

        this.descricao = descricao;
    }

    public Safra(final String codigo, final String descricao, final StatusEnum status) {

        this.setCodigo(codigo);

        this.descricao = descricao;

        this.status = status;
    }

}
