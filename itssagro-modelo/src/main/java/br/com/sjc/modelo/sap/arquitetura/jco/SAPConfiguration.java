package br.com.sjc.modelo.sap.arquitetura.jco;

/**
 * <p>
 * <b>Title:</bSAPConfiguration
 * </p>
 *
 * <p>
 * <b>Description:</bInterface responsável por especificar os valores informados para a conexão com o ERP SAP via JCO.
 * </p>
 *
 * @author Bruno Zafalão
 * @version 1.0.0
 */
public interface SAPConfiguration {

    /**
     * Constante SUFFIXX_DESTINATION.
     */
    String SUFFIXX_DESTINATION = ".jcoDestination";

    /**
     * Método responsável por obter o valor da propriedade <code>ashost</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String ashost();

    /**
     * Método responsável por obter o valor da propriedade <code>saprouter</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String saprouter();

    /**
     * Método responsável por obter o valor da propriedade <code>mshost</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String mshost();

    /**
     * Método responsável por obter o valor da propriedade <code>gwhost</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String gwhost();

    /**
     * Método responsável por obter o valor da propriedade <code>tphost</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String tphost();

    /**
     * Método responsável por obter o valor da propriedade <code>type</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String type();

    /**
     * Método responsável por obter o valor da propriedade <code>gwserv</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String gwserv();

    /**
     * Método responsável por obter o valor da propriedade <code>useguihost</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String useguihost();

    /**
     * Método responsável por obter o valor da propriedade <code>useguiserv</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String useguiserv();

    /**
     * Método responsável por obter o valor da propriedade <code>useguiprogid</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String useguiprogid();

    /**
     * Método responsável por obter o valor da propriedade <code>sncmode</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String sncmode();

    /**
     * Método responsável por obter o valor da propriedade <code>sncpartnername</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String sncpartnername();

    /**
     * Método responsável por obter o valor da propriedade <code>sncqop</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String sncqop();

    /**
     * Método responsável por obter o valor da propriedade <code>sncmyname</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String sncmyname();

    /**
     * Método responsável por obter o valor da propriedade <code>snclib</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String snclib();

    /**
     * Método responsável por obter o valor da propriedade <code>dest</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String dest();

    /**
     * Método responsável por obter o valor da propriedade <code>dsr</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String dsr();

    /**
     * Método responsável por obter o valor da propriedade <code>saplogonid</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String saplogonid();

    /**
     * Método responsável por obter o valor da propriedade <code>extiddata</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String extiddata();

    /**
     * Método responsável por obter o valor da propriedade <code>extidtype</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String extidtype();

    /**
     * Método responsável por obter o valor da propriedade <code>client</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String client();

    /**
     * Método responsável por obter o valor da propriedade <code>user</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String user();

    /**
     * Método responsável por obter o valor da propriedade <code>codepage</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String codepage();

    /**
     * Método responsável por obter o valor da propriedade <code>abapdebug</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String abapdebug();

    /**
     * Método responsável por obter o valor da propriedade <code>usesapgui</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String usesapgui();

    /**
     * Método responsável por obter o valor da propriedade <code>getsso2</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String getsso2();

    /**
     * Método responsável por obter o valor da propriedade <code>mysapsso2</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String mysapsso2();

    /**
     * Método responsável por obter o valor da propriedade <code>x509cert</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String x509cert();

    /**
     * Método responsável por obter o valor da propriedade <code>lcheck</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String lcheck();

    /**
     * Método responsável por obter o valor da propriedade <code>grtdata</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String grtdata();

    /**
     * Método responsável por obter o valor da propriedade <code>passwd</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String passwd();

    /**
     * Método responsável por obter o valor da propriedade <code>sysnr</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String sysnr();

    /**
     * Método responsável por obter o valor da propriedade <code>r3name</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String r3name();

    /**
     * Método responsável por obter o valor da propriedade <code>group</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String group();

    /**
     * Método responsável por obter o valor da propriedade <code>trace</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String trace();

    /**
     * Método responsável por obter o valor da propriedade <code>aliasuser</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String aliasuser();

    /**
     * Método responsável por obter o valor da propriedade <code>lang</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String lang();

    /**
     * Método responsável por obter o valor da propriedade <code>idletimeout</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String idletimeout();

    /**
     * Método responsável por obter o valor da propriedade <code>tpname</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String tpname();

    /**
     * Método responsável por obter o valor da propriedade <code>conection_count</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String conectioncount();

    /**
     * Método responsável por obter o valor da propriedade <code>progid</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String progid();

    /**
     * Método responsável por obter o valor da propriedade <code>endpointname</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String endpointname();

    /**
     * Método responsável por obter o valor da propriedade <code>destinationPath</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String destinationName();

    /**
     * Método responsável por obter o valor da propriedade <code>servername</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String serverName();

    /**
     * Método responsável por obter o valor da propriedade <code>repositoryname</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String repositoryName();

    /**
     * Método responsável por obter o valor da propriedade <code>functionname</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    String functionName();

    /**
     * Método responsável por obter o valor da propriedade <code>codigoconexao</code>
     *
     * @return <code>String</code>
     * @author Ezequiel Bispo Nunes
     */
    String codigoConexao();
}
