package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.StatusLacragemEnum;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
@Entity
@Table(name = "tb_lacragem", schema = "sap")
public class Lacragem extends EntidadeAutenticada {

    @Column(name = "status_lacragem")
    @Enumerated(EnumType.STRING)
    private StatusLacragemEnum statusLacragem = StatusLacragemEnum.AGUARDANDO;

    @EntityProperties(value = {"id", "dataUtilizacao", "usuarioQueUtilizou.id", "usuarioQueUtilizou.nome", "usuarioQueUtilizou.login", "usuarioQueUtilizou.cpf", "lacragem.id", "lacre.id", "lacre.lote", "lacre.codigo", "lacre" +
            ".observacaoStatusLacreAlterar", "lacre.statusLacre", "lacre.dataInutilizacao", "lacre.usuarioQueInutilizou.id", "lacre.usuarioQueInutilizou.login", "lacre.usuarioQueInutilizou.nome", "lacre.usuarioQueInutilizou.cpf", "lacre" +
            ".motivoInutilizacao.id", "lacre.motivoInutilizacao.descricao", "lacre.motivoInutilizacao.tipo", "statusLacreRomaneio"}, targetEntity = LacreRomaneio.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lacragem", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<LacreRomaneio> lacres;
    
    @EntityProperties(value = {"id", "descricao", "tipo", "conferenciaPeso", "buscarDadosRomaneio"}, targetEntity = Motivo.class)
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "tb_lacragem_motivo", schema = "public", joinColumns = @JoinColumn(name = "id_lacragem"), inverseJoinColumns = @JoinColumn(name = "id_motivo"))
    private List<Motivo> motivos = new ArrayList<>();

    public Lacragem() {

        super();
    }

    public Lacragem(StatusLacragemEnum statusLacragem, List<LacreRomaneio> lacres, List<Motivo> motivos) {

        this.statusLacragem = statusLacragem;
        this.lacres = lacres.stream().map(l -> new LacreRomaneio(l.getLacre(), l.getStatusLacreRomaneio())).collect(Collectors.toList());
        if (ColecaoUtil.isNotEmpty(motivos)) {
            this.motivos = motivos.stream().map(m -> new Motivo(m.getDescricao(), m.getTipo())).collect(Collectors.toList());
        }
    }
}
