package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.request.item.RfcCriarRemessaItemRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SAPParameter( function = "ZBAPI_OUTB_DELIVERY_CREATE_STO" )
public class RfcCriarRemessaRequest extends JCoRequest {

    @SAPColumn( name = "SHIP_POINT", size = 4 )
    private String localExpedicao;

    @SAPColumn( name = "DUE_DATE", size = 8 )
    private String dataCriacao;

    @SAPColumn( name = "NO_DEQUEUE", size = 1 )
    private String desbloquear;

    @SAPTable( name = "STOCK_TRANS_ITEMS", item = RfcCriarRemessaItemRequest.class )
    private List<RfcCriarRemessaItemRequest> itens;

    @Override
    public int getTableRows () {

        return this.getItens ().size ();
    }

    @Override
    public List<RfcCriarRemessaItemRequest> getTableItens () {

        return this.getItens ();
    }
}
