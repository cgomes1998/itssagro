package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.Fluxo;
import br.com.sjc.modelo.FluxoMaterial;
import br.com.sjc.modelo.sap.Material;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FluxoMaterialDTO extends DataDTO {

    private Fluxo fluxo;

    private Material material;

    public FluxoMaterialDTO(FluxoMaterial fluxoMaterial) {
        setId(fluxoMaterial.getId());
        setFluxo(fluxoMaterial.getFluxo());
        setMaterial(fluxoMaterial.getMaterial());
    }

}
