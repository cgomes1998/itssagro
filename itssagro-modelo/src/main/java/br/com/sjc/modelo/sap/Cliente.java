package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.enums.TipoPessoaEnum;
import br.com.sjc.modelo.enums.TipoTransporteEnum;
import br.com.sjc.modelo.enums.UFEnum;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by julio.bueno on 26/06/2019.
 */
@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_cliente", schema = "sap")
public class Cliente extends EntidadeSAPPre {

    @Column
    private String nome;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_pessoa")
    private TipoPessoaEnum tipoPessoa;

    @Column(name = "cpj_cnpj")
    private String cpfCnpj;

    @Column(name = "inscricao_estadual")
    private String inscricaoEstadual;

    @Column
    private String cep;

    @Enumerated(EnumType.STRING)
    @Column(name = "uf")
    private UFEnum uf;

    @Enumerated(EnumType.STRING)
    @Column(name = "tp_transporte")
    private TipoTransporteEnum tipoTransporte;

    @Column
    private String municipio;

    @Column
    private String numero;

    @Column
    private String endereco;

    @Column
    private String bairro;

    @Column
    private String telefone;

    public Cliente() {
        super();
    }

    public Cliente(
            final String codigo,
            final String cpfCnpj,
            final String nome
    ) {
        this.setCodigo(codigo);
        this.setCpfCnpj(cpfCnpj);
        this.setNome(nome);
    }

    public String getEnderecoFormatado() {
        List<String> text = new ArrayList<>();
        if (StringUtil.isNotNullEmpty(this.getMunicipio())) {
            text.add(this.getMunicipio());
        }
        if (ObjetoUtil.isNotNull(this.getUf())) {
            text.add(this.getUf().getSigla());
        }
        if (ColecaoUtil.isNotEmpty(text)) {
            return text.stream().collect(Collectors.joining(" - "));
        }
        return StringUtil.empty();
    }

    public String getDescricaoFormatada() {

        return this.toString();
    }

    @Override
    public String toString() {

        if (StringUtil.isNotNullEmpty(this.getCodigo())) {

            return MessageFormat.format("{0} - {1}", this.getCodigo(), this.nome);
        }

        if (StringUtil.isNotNullEmpty(this.cpfCnpj) && this.cpfCnpj.length() <= 11) {

            return MessageFormat.format("{0} - {1}", StringUtil.format(this.cpfCnpj, "###.###.###-##"), this.nome);
        }

        if (StringUtil.isNotNullEmpty(this.cpfCnpj)) {

            return MessageFormat.format("{0} - {1}", StringUtil.format(this.cpfCnpj, "###.###.###/####-##"), this.nome);
        }

        return this.nome;
    }
}
