package br.com.sjc.modelo;

import br.com.sjc.util.StringUtil;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Base64;

/**
 * Created by julio.bueno on 03/07/2019.
 */
@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_arquivo")
public class Arquivo extends EntidadeGenerica {

    @Column
    private String nome;

    @Column(name = "mime_type")
    private String mimeType;

    @Column(name = "base_64", columnDefinition = "TEXT")
    private String base64;

    public Arquivo() {

    }

    @Builder(builderMethodName = "arquivoBuilder")
    public Arquivo(String nome, String base64, byte[] buffer) {

        this.nome = nome;

        if (StringUtil.isNotNullEmpty(base64)) {

            this.base64 = base64;

        } else {

            this.base64 = Base64.getEncoder().encodeToString(buffer);
        }
    }

}
