package br.com.sjc.modelo.sap.request.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import com.sap.conn.jco.JCoMetaData;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * <b>Title:</b> EventOutRequestItem.java
 * </p>
 *
 * <p>
 * <b>Description:</b> Registros da tabela <code>MSGDATA</code> da função <code>ZITSSAGRO_FM_EVENT_OUT</code>
 * </p>
 *
 * <p>
 * <b>Company: </b> ITSS Soluções em Tecnologia
 * </p>
 *
 * @author Bruno Zafalão
 * @version 1.0.0
 */
@Getter
@Setter
@SAPItem
public class EventOutRequestItem extends JCoItem {

    @SAPColumn(name = "CHAVE", size = 20, type = JCoMetaData.TYPE_CHAR, ordinal = 0)
    private String requestKey;

    @SAPColumn(name = "FIELD", size = 50, type = JCoMetaData.TYPE_CHAR, ordinal = 1)
    private String campo;

    @SAPColumn(name = "SEQNC", size = 10, type = JCoMetaData.TYPE_CHAR, ordinal = 2)
    private String sequencia;

    @SAPColumn(name = "GRCOD", size = 10, type = JCoMetaData.TYPE_CHAR, ordinal = 3)
    private String grupo;

    @SAPColumn(name = "GRITM", size = 10, type = JCoMetaData.TYPE_CHAR, ordinal = 4)
    private String itemDocumento;

    @SAPColumn(name = "VALUE", size = 200, type = JCoMetaData.TYPE_CHAR, ordinal = 5)
    private String valor;

}
