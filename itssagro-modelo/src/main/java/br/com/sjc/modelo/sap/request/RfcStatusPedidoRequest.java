package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.request.item.RfcStatusPedidoRequestItem;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@SAPParameter(function = "ZITSSAGRO_FM_LIB_PEDIDO")
public class RfcStatusPedidoRequest extends JCoRequest {

    @SAPTable(name = "T_EBELN", item = RfcStatusPedidoRequestItem.class)
    private List<RfcStatusPedidoRequestItem> tEbeln;

    @Override
    public int getTableRows() {

        return this.getTEbeln().size();
    }

    @Override
    public List<RfcStatusPedidoRequestItem> getTableItens() {

        return this.getTEbeln();
    }
}
