package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPStructure;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPParameter(function = "/ITSSAGRO/XML_NFE_TRANSF_HD")
public class RfcObterNfeTransferenciaResponse extends JCoResponse {

    @SAPStructure(name = "W_HEADER", item = RfcObterNfeTransferenciaHeaderResponse.class )
    private RfcObterNfeTransferenciaHeaderResponse header;

    @SAPStructure( name = "W_ITEM", item = RfcObterNfeTransferenciaItemResponse.class )
    private RfcObterNfeTransferenciaItemResponse item;

    @SAPColumn(name = "E_CHAVE")
    private String chave;

    @SAPColumn(name = "E_REMESSA")
    private String remessa;

    @SAPColumn(name = "E_DOCSTA")
    private String docStatus;

    @SAPColumn(name = "E_REMESSAAPTA")
    private String remessaApta;
}
