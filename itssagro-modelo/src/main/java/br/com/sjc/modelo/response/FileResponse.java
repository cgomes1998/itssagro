package br.com.sjc.modelo.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class FileResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private byte[] buffer;
}
