package br.com.sjc.modelo;

import br.com.sjc.modelo.sap.Centro;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_fluxo")
public class Fluxo extends EntidadeAutenticada {

    @Column
    private Long codigo;

    @Column
    private String descricao;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_fluxo")
    private TipoFluxoEnum tipoFluxo;

    @EntityProperties(value = {"id", "fluxo.id", "centro.id", "centro.codigo", "centro.descricao"}, targetEntity = FluxoCentro.class)
    @OrderBy("id")
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "fluxo", cascade = CascadeType.ALL)
    private List<FluxoCentro> centros = new ArrayList<>();

    @EntityProperties(value = {"id", "fluxo.id", "material.id", "material.codigo", "material.descricao"}, targetEntity = FluxoMaterial.class)
    @OrderBy("id")
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "fluxo", cascade = CascadeType.ALL)
    private List<FluxoMaterial> materiais = new ArrayList<>();

    @EntityProperties(value = {"id", "fluxo.id", "sequencia", "statusEstornoAutomacao", "statusCargaPontualDestinoSucesso", "statusCargaPontualDestinoEstorno", "mensagemEstornoAutomacao", "etapa", "medirTempo", "etapaFinal", "avancoAutomatico", "formulario.id", "local.id", "hardware.id"},
            collecions = "criterios", targetEntity = FluxoEtapa.class)
    @OrderBy("sequencia")
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "fluxo", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<FluxoEtapa> etapas = new ArrayList<>();

    @Transient
    private Centro centro;

    @Transient
    private Material material;

}
