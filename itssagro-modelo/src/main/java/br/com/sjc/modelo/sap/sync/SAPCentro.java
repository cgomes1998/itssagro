package br.com.sjc.modelo.sap.sync;

import br.com.sjc.modelo.sap.Centro;
import br.com.sjc.modelo.sap.Deposito;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class SAPCentro extends SAPEntidade {

    @SAPColumn(name = "COD_CENTRO")
    private String codigo;

    @SAPColumn(name = "DES_CENTRO")
    private String descricao;

    @SAPColumn(name = "COD_DEPOSITO")
    private String codigoDeposito;

    @SAPColumn(name = "DES_DEPOSITO")
    private String descricaoDeposito;

    /**
     * Enum COLUMN_INPUT
     */
    public enum COLUMN_INPUT {

        DT_ULT_INF;
    }

    public static SAPCentro newInstance() {

        return new SAPCentro();
    }

    public Centro copy(Centro entidadeSaved) {

        if (ObjetoUtil.isNull(entidadeSaved)) {

            entidadeSaved = new Centro();

        } else {

            entidadeSaved.setId(entidadeSaved.getId());
        }

        entidadeSaved.setDataCadastro(new Date());

        entidadeSaved.setStatusCadastroSap(StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO);

        entidadeSaved.setStatusRegistro(StatusRegistroEnum.ATIVO);

        entidadeSaved.setCodigo(StringUtil.isNotNullEmpty(this.getCodigo()) ? this.getCodigo().trim() : null);

        entidadeSaved.setDescricao(StringUtil.isNotNullEmpty(this.getDescricao()) ? this.getDescricao().trim() : null);

        return entidadeSaved;
    }

    public Deposito copyDeposito(Deposito entidadeSaved) {

        if (ObjetoUtil.isNull(entidadeSaved)) {

            entidadeSaved = new Deposito();

        } else {

            entidadeSaved.setId(entidadeSaved.getId());
        }

        entidadeSaved.setDataCadastro(new Date());

        entidadeSaved.setStatusCadastroSap(StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO);

        entidadeSaved.setStatusRegistro(StatusRegistroEnum.ATIVO);

        entidadeSaved.setCodigo(StringUtil.isNotNullEmpty(this.getCodigoDeposito()) ? this.getCodigoDeposito().trim() : null);

        entidadeSaved.setDescricao(StringUtil.isNotNullEmpty(this.getDescricaoDeposito()) ? this.getDescricaoDeposito().trim() : null);

        return entidadeSaved;
    }
}
