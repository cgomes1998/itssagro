package br.com.sjc.modelo.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class DashboardGerencialDTO {

    private FluxoDTO fluxo;

    private long totalFila = 0;

    private DashboardGerencialSenhaDTO ultimaChamado;

    private List<DashboardGerencialSenhaDTO> senhas = new ArrayList<>();

    private List<DashboardGerencialEtapaDTO> etapas = new ArrayList<>();

}
