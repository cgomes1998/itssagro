package br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Motorista {

    private int id;

    private String nome;

    private String cpf;

    private String rg;

    private String cnh;

    private String data_validade_cnh;

    private String data_validade_mopp;

    private List<Telefone> telefones;

    private Endereco endereco;

}
