package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusEmissaoDocumentoEnum {

    AGUARDANDO("Aguardando"),
    CHAMADA("Chamada"),
    EM_EMISSAO("Em Emissão"),
    CANCELADO("Cancelado"),
    EMITIDOS("Emitidos"),
    PENDENTE("Pendente");

    private String descricao;
}
