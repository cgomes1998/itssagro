package br.com.sjc.modelo.enums;

import lombok.Getter;

@Getter
public enum OperacaoSenhaEnum {

    ENTRADA("Entrada"),
    SAIDA("Saída");

    private String descricao;

    OperacaoSenhaEnum(String descricao) {
        this.descricao = descricao;
    }
}
