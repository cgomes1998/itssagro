package br.com.sjc.modelo.pojo.cargaPontual.agendamento.trocaStatus;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class RetornoConteudo {

    private int status;

    private String statusDescricao;

    private String mensagem;

    private Conteudo conteudo;

}
