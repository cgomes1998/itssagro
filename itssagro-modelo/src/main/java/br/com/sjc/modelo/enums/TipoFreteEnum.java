package br.com.sjc.modelo.enums;

import lombok.Getter;

@Getter
public enum TipoFreteEnum {

    CIF("1 - CIF", 1),

    FOB("2 - FOB", 2);

    private String descricao;

    private Integer codigo;

    TipoFreteEnum(String descricao, Integer codigo) {

        this.codigo = codigo;

        this.descricao = descricao;
    }
}
