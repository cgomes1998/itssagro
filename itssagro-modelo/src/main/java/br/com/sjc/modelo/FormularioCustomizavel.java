package br.com.sjc.modelo;

import java.util.List;

/**
 * Created by julio.bueno on 10/07/2019.
 */
public interface FormularioCustomizavel {

    Formulario getFormulario();
    void setFormulario(Formulario formulario);

    List<FormularioResposta> getRespostas();
    void setRespostas(List<FormularioResposta> respostas);

}
