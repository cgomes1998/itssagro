package br.com.sjc.modelo;

import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_usuario")
public class Usuario extends EntidadeAutenticada {

    private static final long serialVersionUID = 1L;

    @Column(name = "nome")
    private String nome;

    @Column(name = "login", length = 100, unique = true)
    private String login;

    @Column(name = "cpf", length = 11)
    private String cpf;

    @Column(name = "senha", length = 100)
    private String senha;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dt_ultimo_acesso")
    private Date ultimoAcesso;

    @Column(name = "permite_acesso_externo", nullable = false, columnDefinition = "boolean default false")
    private boolean permiteAcessoExterno;

    @EntityProperties(value = {"id", "nome", "pesagemManual", "aplicarIndiceRestrito", "permitirLiberacoes"}, collecions = {"funcionalidades", "operacoes"}, targetEntity = Perfil.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    @JoinTable(name = "tb_usuario_perfil", joinColumns = {@JoinColumn(name = "id_usuario")}, inverseJoinColumns = {@JoinColumn(name = "id_perfil")})
    private Collection<Perfil> perfis;

    @Transient
    private Perfil perfil;

    @Transient
    private String novaSenha;

}
