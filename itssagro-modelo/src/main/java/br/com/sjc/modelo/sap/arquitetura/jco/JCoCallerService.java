package br.com.sjc.modelo.sap.arquitetura.jco;

import br.com.sjc.util.ObjetoUtil;
import com.sap.conn.jco.*;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@SuppressWarnings( { "rawtypes", "unchecked" } )
public final class JCoCallerService {

    private final JCoDestination service;

    private static Map<String, JCoCallerService> mapServices;

    private String rfcProgramID;

    private JCoFunction function;

    private static Logger log = Logger.getLogger ( JCoCallerService.class.getName () );

    static {
        JCoCallerService.mapServices = new HashMap<> ();
    }

    private JCoCallerService ( final SAPConfiguration configuration ) {

        this.service = SAPLocator.locale ( configuration );
    }

    private JCoCallerService ( final JCoDestination destinationService ) {

        this.service = destinationService;
    }

    private JCoCallerService (
            final JCoDestination destinationService,
            final String rfcProgramID
    ) {

        this.service = destinationService;

        this.rfcProgramID = rfcProgramID;
    }

    public static JCoCallerService getInstance ( final JCoDestination service ) {

        return new JCoCallerService ( service );
    }

    public void call (
            final JCoRequest request,
            final JCoResponse response
    ) {

        if ( ObjetoUtil.isNotNull ( request ) ) {

            try {

                final JCoFunction jcoFunction = this.getFunction ( request.function () );

                this.initRequestParameters ( jcoFunction, request );

                jcoFunction.execute ( this.getService () );

                function = jcoFunction;

                this.initResponseParameters ( jcoFunction, response );

            } catch ( final JCoException | IllegalAccessException
                    | IllegalArgumentException | InstantiationException e ) {

                JCoCallerService.log.warning ( e.getMessage () );

                throw new RuntimeException ( e );
            }
        }
    }

    private void initResponseParameters (
            final JCoFunction jcoFunction,
            final JCoResponse response
    ) throws InstantiationException, IllegalAccessException {

        final Field[] fields = response.getClass ().getDeclaredFields ();

        for ( int i = 0; i < fields.length; i++ ) {

            this.initSimpleResponseParameter ( fields[i], jcoFunction, response );

            this.initTableResponseParameter ( fields[i], jcoFunction, response );

            this.initStructureResponseParameter ( fields[i], jcoFunction, response );
        }
    }

    private void initStructureResponseParameter (
            Field field,
            JCoFunction jcoFunction,
            JCoResponse response
    ) throws InstantiationException, IllegalAccessException {

        final SAPStructure structure = field.getAnnotation ( SAPStructure.class );

        if ( structure != null ) {

            final JCoStructure jCoStructure = jcoFunction.getExportParameterList ().getStructure ( structure.name () );

            field.setAccessible ( true );

            final Field[] itemfields = structure.item ().getDeclaredFields ();

            final JCoItem item = structure.item ().newInstance ();

            for ( int i = 0; i < itemfields.length; i++ ) {

                final SAPColumn itemColumn = itemfields[i].getAnnotation ( SAPColumn.class );

                itemfields[i].setAccessible ( true );

                if ( ObjetoUtil.isNotNull ( itemColumn ) ) {

                    final Object value = jCoStructure.getValue ( itemColumn.name () );

                    itemfields[i].set ( item, value );
                }
            }

            field.set ( response, item );
        }
    }

    private void initTableResponseParameter (
            final Field field,
            final JCoFunction jcoFunction,
            final JCoResponse response
    ) throws InstantiationException, IllegalAccessException {

        final SAPTable table = field.getAnnotation ( SAPTable.class );

        if ( table != null ) {

            final JCoTable jcoTable = jcoFunction.getTableParameterList ().getTable ( table.name () );

            field.setAccessible ( true );

            final List itens = new LinkedList ();

            final Field[] itemfields = table.item ().getDeclaredFields ();

            for ( int row = 0; row < jcoTable.getNumRows (); row++ ) {

                final JCoItem item = table.item ().newInstance ();

                for ( int i = 0; i < itemfields.length; i++ ) {

                    final SAPColumn itemColumn = itemfields[i].getAnnotation ( SAPColumn.class );

                    itemfields[i].setAccessible ( true );

                    if ( ObjetoUtil.isNotNull ( itemColumn ) ) {

                        final Object value = jcoTable.getValue ( itemColumn.name () );

                        itemfields[i].set ( item, value );
                    }
                }

                itens.add ( item );

                jcoTable.nextRow ();
            }

            field.set ( response, itens );
        }
    }

    private void initSimpleResponseParameter (
            final Field field,
            final JCoFunction jcoFunction,
            final JCoResponse response
    ) throws InstantiationException, IllegalAccessException {

        final SAPColumn column = field.getAnnotation ( SAPColumn.class );

        if ( column != null && jcoFunction.getExportParameterList () != null ) {

            field.setAccessible ( true );

            final Object value = jcoFunction.getExportParameterList ().getValue ( column.name () );

            field.set ( response, value );
        }
    }

    private void initRequestParameters (
            final JCoFunction jcoFunction,
            final JCoRequest request
    )
            throws IllegalArgumentException, IllegalAccessException, InstantiationException {

        final Field[] fields = request.getClass ().getDeclaredFields ();

        for ( int i = 0; i < fields.length; i++ ) {

            if ( fields[i].isAnnotationPresent ( SAPColumn.class ) ) {

                final SAPColumn column = fields[i].getAnnotation ( SAPColumn.class );

                if ( column != null ) {

                    fields[i].setAccessible ( true );

                    if ( fields[i].get ( request ) instanceof String && ObjetoUtil.isNotNull ( fields[i].get ( request ) ) ) {

                        jcoFunction.getImportParameterList ().setValue ( column.name (), String.valueOf ( fields[i].get ( request ) ) );

                    } else {

                        jcoFunction.getImportParameterList ().setValue ( column.name (), fields[i].get ( request ) );

                    }

                }

            } else if ( fields[i].isAnnotationPresent ( SAPTable.class ) ) {

                final SAPTable table = fields[i].getAnnotation ( SAPTable.class );

                if ( table != null ) {

                    final JCoTable jcoTable = jcoFunction.getTableParameterList ().getTable ( table.name () );

                    fields[i].setAccessible ( true );

                    final Field[] itemfields = table.item ().getDeclaredFields ();

                    for ( int row = 0; row < request.getTableRows ( table.name () ); row++ ) {

                        jcoTable.insertRow ( row );

                        for ( int indice = 0; indice < itemfields.length; indice++ ) {

                            itemfields[indice].setAccessible ( true );

                            jcoTable.setValue ( itemfields[indice].getAnnotation ( SAPColumn.class ).name (), itemfields[indice].get ( request.getTableItens ( table.name () ).get ( row ) ) );
                        }
                    }
                }

            } else if ( fields[i].isAnnotationPresent ( SAPStructure.class ) ) {

                final SAPStructure structure = fields[i].getAnnotation ( SAPStructure.class );

                if ( structure != null ) {

                    final JCoStructure jcoStructure = jcoFunction.getImportParameterList ().getStructure ( structure.name () );

                    fields[i].setAccessible ( true );

                    final Field[] itemfields = structure.item ().getDeclaredFields ();

                    for ( int row = 0; row < request.getStructureRows ( structure.name () ); row++ ) {

                        for ( int indice = 0; indice < itemfields.length; indice++ ) {

                            itemfields[indice].setAccessible ( true );

                            try {

                                jcoStructure.setValue ( itemfields[indice].getAnnotation ( SAPColumn.class ).name (), itemfields[indice].get ( request.getStructureItens ( structure.name () ).get ( row ) ) );

                            } catch ( NullPointerException e ) {

                                log.info ( "Structure nula" );
                            }

                        }
                    }
                }
            }
        }
    }

    private JCoFunction getFunction ( final String function ) throws JCoException {

        final JCoFunction jcoFunction = this.getService ().getRepository ().getFunction ( function );

        if ( ObjetoUtil.isNull ( jcoFunction ) ) {

            throw new IllegalArgumentException ( "error.jco.function.invalid" );
        }

        return jcoFunction;
    }

    public static JCoCallerService getInstance ( final SAPConfiguration configuration ) {

        JCoCallerService.mapServices.put ( configuration.codigoConexao (), new JCoCallerService ( configuration ) );

        return JCoCallerService.mapServices.get ( configuration.codigoConexao () );
    }

    public static JCoCallerService getInstance (
            final String key,
            final JCoDestination destinationService,
            final String rfcProgramID
    ) {

        JCoCallerService.mapServices.put ( key, new JCoCallerService ( destinationService, rfcProgramID ) );

        return JCoCallerService.mapServices.get ( key );
    }

    public JCoDestination getService () {

        return this.service;
    }

    public String getRfcProgramID () {

        return this.rfcProgramID;
    }

    public void setRfcProgramID ( final String rfcProgramID ) {

        this.rfcProgramID = rfcProgramID;
    }

    public JCoFunction getFunction () {

        return function;
    }

    public void setFunction ( JCoFunction function ) {

        this.function = function;
    }

}
