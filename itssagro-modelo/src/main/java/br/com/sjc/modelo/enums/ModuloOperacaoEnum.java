package br.com.sjc.modelo.enums;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum ModuloOperacaoEnum {

    DADOS_MESTRES("DM", "Dado Mestre"),

    ROMANEIO("RM", "Romaneio"),

    COMPRAS("CP", "Contrato/Pedido de Compra");

    private String codigo;

    private String descricao;

    private ModuloOperacaoEnum(final String codigo, final String descricao) {

        this.codigo = codigo;

        this.descricao = descricao;
    }

    public static ModuloOperacaoEnum get(final String descricao) {

        return Arrays.asList(ModuloOperacaoEnum.values()).stream().filter((m) -> m.getCodigo().equals(descricao)).findFirst().orElse(null);
    }
}
