package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.*;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProdutorDTO extends DataDTO {

    @ProjectionProperty
    private String codigo;

    @ProjectionProperty
    private String nome;

    @ProjectionProperty
    private String cpf;

    @ProjectionProperty
    private String cnpj;

    @ProjectionProperty
    private String municipio;

    @ProjectionProperty
    private UFEnum estado;

    @ProjectionProperty
    private TipoPessoaEnum tipo;

    @ProjectionProperty
    private StatusInscEstadualEnum statusInscEstadual;

    @ProjectionProperty
    private StatusCadastroSAPEnum statusCadastroSap;

    @ProjectionProperty
    private StatusRegistroEnum statusRegistro;

    public String getCpfCnpj() {

        return StringUtil.isNotNullEmpty(this.getCnpj()) ? this.getCnpj() : this.getCpf();
    }

    public String getStatusSapDescricao() {

        return this.statusCadastroSap.getDescricao();
    }

    public String getStatusRegistroDescricao() {

        return this.statusRegistro.getDescricao();
    }

    public String getTipoPessoaDescricao() {

        return this.tipo.getValue();
    }

    public String getStatusInscEstadualDescricao() {

        return this.statusInscEstadual.getDescricao();
    }

    public String getEstadoDescricao() {

        return ObjetoUtil.isNull(this.estado) ? "***" : this.estado.getDescricao();
    }

}
