package br.com.sjc.modelo.enums;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public enum DadosSincronizacaoEnum {
    
    AGROVALE_PARA_DEP_FECHADO_GO206("RT - Agrovale para Dep. Fech. GO206", "170"),
    ANALISE("Análise", ""),
    CENTRO("Centro", "100"),
    CLIENTE("Cliente", "190"),
    CONTRATO_PEDIDO_COMPRAS("Contrato/Pedido de Compras", "050"),
    DADOS_CONTRATO("Dados de Contrato", ""),
    EM_POS_DEVOLUCAO_PF("Em Pós Devolução Pessoa Física", ""),
    EM_POS_DEVOLUCAO_PF_DIRETO_NA_AGROVALE("", ""),
    EM_POS_DEVOLUCAO_PF_DIRETO_NA_CARAMURU("", ""),
    EM_POS_DEVOLUCAO_PF_DIRETO_NA_GO206("", ""),
    EM_POS_DEVOLUCAO_PJ("Em Pós Devolução Pessoa Jurídica", ""),
    ENTRADA_ARMAZEM_DE_TERCEIROS("Entrada Armazém de terceiros", "800"),
    IVA("Ivas", ""),
    MATERIAL("Material", "120"),
    MOTORISTA("Motorista", "150"),
    NF_COMP_PRODUTOR_PJ_DIRETO_AGROVALE("NF COMP - Produtor PJ direto na Agrovale", "360"),
    NF_COMP_PRODUTOR_PJ_DIRETO_GO206("NF COMP - Produtor PJ direto na GO206", "340"),
    NF_COMP_PRODUTOR_PJ_DIRETO_UPG("NF COMP - Produtor PJ direto na UPG", "320"),
    NF_TRANSPORTE("Nota fiscal de transporte", "100"),
    NFE_NOTA_ST_ROMANEIO("Status do Romaneio, Nota Fiscal e Pedidos de Compras", ""),
    OPERACAO_SIMPLES_PESAGEM("520", "Operação de Simples Pesagem"),
    PRODUTOR("Fornecedor", "140"),
    PRODUTOR_PF_DIRETO_AGROVALE("EM - Produtor PF direto na Agrovale", "150"),
    PRODUTOR_PF_DIRETO_CARAMURU("EM - Produtor PF direto na Caramuru", "155"),
    PRODUTOR_PF_DIRETO_EM_ARMAZEM_TERC("EM - Produtor PF direto em Armazém Terc.", "157"),
    PRODUTOR_PF_DIRETO_GO206("Produtor PF direto na GO206", "130"),
    PRODUTOR_PF_DIRETO_UPG("Produtor PF direto na UPG", "110"),
    PRODUTOR_PJ_DIRETO_AGROVALE("EM - Produtor PJ direto na Agrovale", "160"),
    PRODUTOR_PJ_DIRETO_CARAMURU("EM - Produtor PJ direto na Caramuru", "165"),
    PRODUTOR_PJ_DIRETO_GO206("Produtor PJ direto na GO206", "140"),
    PRODUTOR_PJ_DIRETO_UPG("Produtor PJ direto na UPG", "120"),
    REMESSA_AGROVALE_UPG("RT - Remessa da Agrovale para UPG", "190"),
    REMESSA_GO206_PARA_UPG("RT- Remessa da GO206 para UPG", "200"),
    ENTRADA_ARMAZENAGEM_GO206("Entrada para armazenagem GO206", "220"),
    RETORNO_ARMAZEM_TERCEIROS_PARA_UPG("RT - Armazém de terceiros para UPG", "197"),
    RETORNO_CARAMURU_UPG("RT - Caramuru para UPG", "195"),
    ROMANEIO("Romaneio", ""),
    RT_COMP_AGROVALE_PARA_DEP_FECHADO_GO206("370", "RT COMP - Agrovale para Dep. Fech. GO206"),
    RT_COMP_AGROVALE_PARA_UPG("390", "RT COMP - Agrovale para UPG"),
    RT_COMP_CARAMURU_PARA_UPG("395", "RT COMP - Caramuru para UPG"),
    SAFRA("Safra", "130"),
    SENHA("Cancelamento de senhas", ""),
    SIMPLES_ENTRADA("Simples Entrada", "900"),
    SIMPLES_PESAGEM("Simples Pesagem", "210"),
    SM_OPERACAO_SAIDA("510", "SM - Operação de Saída"),
    TABELA_CLASSIFICACAO("Tabela de Classificação", "180"),
    TIPO_VEICULO("Tipo de Veículo", "170"),
    TRANS_PROP_MERC_DEPOSITADA("RT - Trans. de Prop. de Merc. Depositada", "180"),
    TRANSPORTADORA("Transportadora", "110"),
    VEICULO("Veículo", "160"),
    XML_NFE("Xml Nota fiscal", ""),
    OBTER_PESO_BALANCA("Obter peso da balança", ""),
    AGENDAMENTO_CARGA_PONTUAL("Agendamento Carga Pontual", ""),
    RT_TERCEIRO_TRANSF_TERCEIROS("RT - Terceiro para transf. de Terceiros", "175"),
    RT_BAIXA_QUEBRA_ARMAZEM_TERCEIROS("RT - Baixa de quebra Armazém de terceiros", "199"),
    OPERACAO_TRANSFERENCIA("STO - Transferência de Centros com Laudo", "550"),
    TRANSFERENCIA_GERAIS("STO - Transferência de Centros sem Laudo", "560"),
    ENTRADA_FISICA_TRANSFERENCIA("Entrada Física Transferência", "600");
    
    private String descricao;
    
    private String operacao;
    
    DadosSincronizacaoEnum(final String descricao, final String operacao) {
        
        this.descricao = descricao;
        
        this.operacao = operacao;
    }
    
    public static boolean possuiClassificacaoDependenteDeRomaneioDeOrigem(DadosSincronizacaoEnum operacao) {
        
        List<DadosSincronizacaoEnum> operacoesComClassificacaoDeRomaneioDeOrigem = new ArrayList<>();
        operacoesComClassificacaoDeRomaneioDeOrigem.add(EM_POS_DEVOLUCAO_PF);
        operacoesComClassificacaoDeRomaneioDeOrigem.add(EM_POS_DEVOLUCAO_PJ);
        operacoesComClassificacaoDeRomaneioDeOrigem.add(EM_POS_DEVOLUCAO_PF_DIRETO_NA_GO206);
        operacoesComClassificacaoDeRomaneioDeOrigem.add(EM_POS_DEVOLUCAO_PF_DIRETO_NA_AGROVALE);
        operacoesComClassificacaoDeRomaneioDeOrigem.add(EM_POS_DEVOLUCAO_PF_DIRETO_NA_CARAMURU);
        
        return operacoesComClassificacaoDeRomaneioDeOrigem.contains(operacao);
    }
}
