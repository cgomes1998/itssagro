package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.sap.GrupoCompradores;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import br.com.sjc.util.DateUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class RfcGruposCompradoresItem extends JCoItem {

    @SAPColumn(name = "EKGRP")
    private String codigo;

    @SAPColumn(name = "EKNAM")
    private String descricao;

    public GrupoCompradores toEntidade() {

        GrupoCompradores entidade = new GrupoCompradores();

        entidade.setDataCadastro(DateUtil.hoje());

        entidade.setCodigo(codigo);

        entidade.setDescricao(descricao);

        return entidade;
    }
}
