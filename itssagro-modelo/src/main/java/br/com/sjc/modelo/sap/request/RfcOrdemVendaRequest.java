package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPStructure;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.request.item.RfcOrdemVendaItemRequest;
import br.com.sjc.modelo.sap.request.item.RfcOrdemVendaDocumentoVendaRequest;
import lombok.Getter;
import lombok.Setter;

import java.util.Collections;
import java.util.List;

@Getter
@Setter
@SAPParameter( function = "BAPISDORDER_GETDETAILEDLIST" )
public class RfcOrdemVendaRequest extends JCoRequest {

    @SAPStructure( name = "I_BAPI_VIEW", item = RfcOrdemVendaItemRequest.class )
    private RfcOrdemVendaItemRequest item;

    @SAPTable( name = "SALES_DOCUMENTS", item = RfcOrdemVendaDocumentoVendaRequest.class )
    private List<RfcOrdemVendaDocumentoVendaRequest> ordensVenda;

    @Override
    public int getTableRows () {

        return this.getOrdensVenda ().size ();
    }

    @Override
    public List<RfcOrdemVendaDocumentoVendaRequest> getTableItens () {

        return this.getOrdensVenda ();
    }

    @Override
    public int getStructureRows ( String name ) {

        return 1;
    }

    @Override
    public List<?> getStructureItens ( String name ) {

        return Collections.singletonList ( item );
    }
}
