package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.EntidadeAutenticada;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
@AllArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_conversao_litragem_item", schema = "sap")
public class ConversaoLitragemItem extends EntidadeAutenticada {

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_conversao_litragem", nullable = false)
    private ConversaoLitragem conversaoLitragem;

    @Column
    private Long tanque;

    @Column(name = "temp_tanque", precision = 19, scale = 2)
    private BigDecimal tempTanque;

    @Column(name = "tempo_carreta", precision = 19, scale = 2)
    private BigDecimal tempCarreta;

    @Column(name = "grau_inpm", precision = 19, scale = 2)
    private BigDecimal grauInpm;

    @Column(precision = 19, scale = 2)
    private BigDecimal acidez;

    @Column(name = "volume_seta_vinte_graus")
    private BigDecimal volumeSetaVinteGraus;

    @Column(name = "volume_densidade_l_vinte_graus")
    private BigDecimal volumeDensidadeLVinteGraus;

    @Column(name = "diferenca_litros")
    private BigDecimal diferencaLitros;

    public ConversaoLitragemItem() {
    }

    public ConversaoLitragemItem(
            Date dataCadastro,
            Long tanque,
            BigDecimal tempTanque,
            BigDecimal tempCarreta,
            BigDecimal grauInpm,
            BigDecimal acidez,
            BigDecimal volumeSetaVinteGraus,
            BigDecimal volumeDensidadeLVinteGraus,
            BigDecimal diferencaLitros
    ) {
        this.setDataCadastro(dataCadastro);
        this.tanque = tanque;
        this.tempTanque = tempTanque;
        this.tempCarreta = tempCarreta;
        this.grauInpm = grauInpm;
        this.acidez = acidez;
        this.volumeSetaVinteGraus = volumeSetaVinteGraus;
        this.volumeDensidadeLVinteGraus = volumeDensidadeLVinteGraus;
        this.diferencaLitros = diferencaLitros;
    }
}
