package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.sap.Motorista;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import br.com.sjc.modelo.enums.UFEnum;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

import java.text.MessageFormat;

@Getter
@Setter
@SAPItem
public class RfcMotoristaResponseItem extends JCoItem {

    @SAPColumn(name = "KUNNR")
    private String codigo;

    @SAPColumn(name = "NAME1")
    private String nome;

    @SAPColumn(name = "STCD2")
    private String cpf;

    @SAPColumn(name = "STCD3")
    private String cnh;

    @SAPColumn(name = "STREET")
    private String logradouro;

    @SAPColumn(name = "HOUSE_NO")
    private String numero;

    @SAPColumn(name = "ORT02")
    private String bairro;

    @SAPColumn(name = "POST_CODE1")
    private String cep;

    @SAPColumn(name = "CITY1")
    private String cidade;

    @SAPColumn(name = "REGION")
    private String regiao;

    @SAPColumn(name = "KTOKD")
    private String grupoContas;

    @SAPColumn(name = "TEL_NUMBER")
    private String telefone;

    @SAPColumn(name = "BLOQUEADO")
    private String bloqueado;

    @SAPColumn(name = "MARC_ELIM")
    private String eliminado;

    private StatusRegistroEnum statusRegistroSAP;

    public StatusRegistroEnum getStatusRegistroSAP() {

        this.statusRegistroSAP = StatusRegistroEnum.ATIVO;

        if (StringUtil.isNotNullEmpty(this.getBloqueado())) {

            this.statusRegistroSAP = StatusRegistroEnum.BLOQUEADO;
        }

        if (StringUtil.isNotNullEmpty(this.getEliminado())) {

            this.statusRegistroSAP = StatusRegistroEnum.ELIMINADO;
        }

        if (StringUtil.isNotNullEmpty(this.getBloqueado()) && StringUtil.isNotNullEmpty(this.getEliminado())) {

            this.statusRegistroSAP = StatusRegistroEnum.BLOQUEADO_E_ELIMINADO;
        }

        return this.statusRegistroSAP;
    }

    public Motorista copy(Motorista entidadeSaved) {

        if (ObjetoUtil.isNull(entidadeSaved)) {

            entidadeSaved = new Motorista();

            entidadeSaved.setDataCadastro(DateUtil.hoje());

        } else {

            entidadeSaved.setDataAlteracao(DateUtil.hoje());
        }

        entidadeSaved.setNome(StringUtil.isNotNullEmpty(this.getNome()) ? this.getNome().trim() : entidadeSaved.getNome());

        entidadeSaved.setCpf(StringUtil.isNotNullEmpty(this.getCpf()) ? this.getCpf().trim() : entidadeSaved.getCpf());

        entidadeSaved.setCnh(StringUtil.isNotNullEmpty(this.getCnh()) ? this.getCnh().trim() : entidadeSaved.getCnh());

        entidadeSaved.setCep(StringUtil.isNotNullEmpty(this.getCep()) ? this.getCep().trim() : entidadeSaved.getCep());

        entidadeSaved.setRua(StringUtil.isNotNullEmpty(this.getLogradouro()) ? this.getLogradouro().trim() : entidadeSaved.getRua());

        entidadeSaved.setNumero(StringUtil.isNotNullEmpty(this.getNumero()) ? this.getNumero().trim() : entidadeSaved.getNumero());

        entidadeSaved.setMunicipio(StringUtil.isNotNullEmpty(this.getCidade()) ? this.getCidade().trim() : entidadeSaved.getMunicipio());

        entidadeSaved.setCodigo(StringUtil.isNotNullEmpty(this.getCodigo()) ? this.getCodigo().trim() : entidadeSaved.getCodigo());

        entidadeSaved.setBairro(StringUtil.isNotNullEmpty(this.getBairro()) ? this.getBairro().trim() : entidadeSaved.getBairro());

        entidadeSaved.setUf(StringUtil.isNotNullEmpty(this.getRegiao()) ? UFEnum.valueOf(this.getRegiao()) : entidadeSaved.getUf());

        entidadeSaved.setLogSAP(MessageFormat.format("MOTORISTA SINCRONIZADO COM O SAP EM {0}", DateUtil.format("dd/MM/yyyy HH:mm:ss", DateUtil.hoje())));

        if (StringUtil.isNotNullEmpty(this.getCodigo())) {

            entidadeSaved.setLogSAP(entidadeSaved.getLogSAP() + "\nCÓDIGO: " + this.getCodigo());
        }

        entidadeSaved.setStatusCadastroSap(StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO);

        entidadeSaved.setStatus(StatusEnum.ATIVO);

        entidadeSaved.setStatusRegistro(this.getStatusRegistroSAP());

        return entidadeSaved;
    }
}
