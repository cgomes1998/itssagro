package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EventoAutomacaoEnum {

    PRECAD_AUTOMATIZA,
    TROCASTATUS_AUTOMATIZA,
    CONSULTA_AUTOMATIZA
    
}
