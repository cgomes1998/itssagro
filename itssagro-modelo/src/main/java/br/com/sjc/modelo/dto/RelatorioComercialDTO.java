package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.util.ObjetoUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@EqualsAndHashCode(of = {"numeroRomaneio", "codigoOrdemVenda"})
public class RelatorioComercialDTO {

    private String codigoOrdemVenda;

    private String pedido;

    private String material;

    private String cliente;

    private String transportadora;

    private String transportadoraSubcontratada;

    private String numeroRomaneio;

    private String placa;

    private String motorista;

    private StatusRomaneioEnum statusRomaneio;

    private StatusIntegracaoSAPEnum statusSap;

    private BigDecimal pesoTara;

    private BigDecimal pesoBruto;

    private BigDecimal pesoLiquido;

    private String nota;

    private String remessa;

    private String numeroOrdemTransporte;

    private BigDecimal qtdKgL;

    private String descricaoMotivo;

    private String romaneioVinculadoPorMotivo;

    private Long idItem;

    private String restricoes;

    private Long quantidadePesagensBrutas;

    private Date dataCadastro;
    
    private Date dataCriacaoNota;
    
    private Long idPesagem;
    
	private Date dataPrimeiraPesagem;

	private Date dataSegundaPesagem;

    public RelatorioComercialDTO() {

    }

    public RelatorioComercialDTO(String codigoOrdemVenda, String pedido, String material, String cliente, String transportadora, String transportadoraSubcontratada, String numeroRomaneio, String placa, String motorista,
                                 StatusRomaneioEnum statusRomaneio, StatusIntegracaoSAPEnum statusSap, BigDecimal pesoTara, BigDecimal pesoBruto, BigDecimal pesoLiquido, String nota, String remessa, String numeroOrdemTransporte,
                                 BigDecimal qtdKgL, String descricaoMotivo, String romaneioVinculadoPorMotivo, Long idItem, Date dataCadastro, Long idPesagem) {

        this.codigoOrdemVenda = codigoOrdemVenda;
        this.pedido = pedido;
        this.material = material;
        this.cliente = cliente;
        this.transportadora = transportadora;
        this.transportadoraSubcontratada = transportadoraSubcontratada;
        this.numeroRomaneio = numeroRomaneio;
        this.placa = placa;
        this.motorista = motorista;
        this.statusRomaneio = statusRomaneio;
        this.statusSap = statusSap;
        this.pesoTara = pesoTara;
        this.pesoBruto = pesoBruto;
        this.pesoLiquido = pesoLiquido;
        this.nota = nota;
        this.remessa = remessa;
        this.numeroOrdemTransporte = numeroOrdemTransporte;
        this.qtdKgL = qtdKgL;
        this.descricaoMotivo = descricaoMotivo;
        this.romaneioVinculadoPorMotivo = romaneioVinculadoPorMotivo;
        this.idItem = idItem;
        this.dataCadastro = dataCadastro;
        this.idPesagem = idPesagem;
    }

    public BigDecimal getPesoLiquidoValido() {

        if (ObjetoUtil.isNull(this.getPesoLiquido()) || this.getPesoLiquido().doubleValue() <= 0)
            return BigDecimal.ZERO;
        return this.getPesoLiquido();
    }

    public BigDecimal getQtdKgLValido() {

        if (ObjetoUtil.isNull(this.getQtdKgL()) || this.getQtdKgL().doubleValue() <= 0)
            return BigDecimal.ZERO;
        return this.getQtdKgL().setScale(0, BigDecimal.ROUND_HALF_DOWN);
    }

}
