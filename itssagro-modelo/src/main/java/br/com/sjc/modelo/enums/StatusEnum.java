package br.com.sjc.modelo.enums;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum StatusEnum {

    ATIVO("Ativo", "01"),

    INATIVO("Inativo", "02");

    private String descricao;

    private String codigoSAP;

    private StatusEnum(final String descricao, final String codigoSAP) {

        this.descricao = descricao;

        this.codigoSAP = codigoSAP;
    }

    public static StatusEnum getCodigoSAP(final String codigoSAP) {

        return Arrays.asList(StatusEnum.values()).stream().filter((s) -> s.getCodigoSAP().equals(codigoSAP)).findFirst().orElse(null);
    }
}
