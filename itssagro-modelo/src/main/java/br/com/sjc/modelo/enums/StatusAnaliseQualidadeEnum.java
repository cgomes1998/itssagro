package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusAnaliseQualidadeEnum {

    AGUARDANDO("Aguardando"),
    EM_ANALISE("Em Análise"),
    APROVADO("Aprovado"),
    CANCELADO("Cancelado"),
    REPROVADO("Reprovado"),
    PENDENTE("Pendente");

    private String descricao;
}
