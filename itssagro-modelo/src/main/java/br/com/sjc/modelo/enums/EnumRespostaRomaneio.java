package br.com.sjc.modelo.enums;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum EnumRespostaRomaneio {

    NUMERO_NFE("J_1BNFE_ACTIVE-NFNUM9"),

    SERIE_NFE("J_1BNFE_ACTIVE-SERIE"),

    CODIGO_NFE("J_1BNFE_ACTIVE-CODE"),

    NR_PEDIDO("B_EBELN"),

    NR_CONTRATO("B_KONNR"),

    NR_DOC_FATURA("B_FATURA");

    private String descricao;

    private EnumRespostaRomaneio(String descricao) {

        this.descricao = descricao;
    }

    public static EnumRespostaRomaneio obterValor(String campo) {

        return Arrays.asList(EnumRespostaRomaneio.values()).stream().filter(i -> i.getDescricao().equalsIgnoreCase(campo)).findFirst().orElse(null);
    }

}
