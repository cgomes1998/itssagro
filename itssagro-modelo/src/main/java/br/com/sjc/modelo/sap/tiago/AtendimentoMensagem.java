package br.com.sjc.modelo.sap.tiago;

import br.com.sjc.modelo.Usuario;

import java.util.Date;

public class AtendimentoMensagem {

    private Atendimento atendimento;

    private Usuario remetente;

    private Usuario destinatario;

    private String conteudo;

    private Date dataEnvio;

    private Date dataLeitura;

    private Date dataVisualizacao;
}
