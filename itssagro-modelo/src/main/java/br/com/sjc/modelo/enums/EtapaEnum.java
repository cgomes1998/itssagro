package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EtapaEnum {

    DISTRIBUICAO_SENHA("Distribuição de Senha"),
    CHECKLIST("Checklist"),
    CRIAR_ROMANEIO("Criar Romaneio"),
    PESAGEM_TARA("Pesagem (Tara)"),
    PESAGEM_BRUTO("Pesagem (Bruto)"),
    LACRAGEM("Lacragem"),
    CONVERSAO_LITRAGEM("Conversão de Litragem"),
    ANALISE_QUALIDADE("Análise de Qualidade"),
    EMITIR_DOCUMENTO("Emitir Documento"),
    CONFERENCIA_GERAIS("Conferência (Caso Geral)"),
    CANCELAMENTO_ORDEM("Cancelamento de Ordem"),
    DESCARGA("Descarga"),
    PENDENCIA_COMERCIAL("Pendência (Comercial)"),
    SAIDA("Saída"),
    CONFIRMACAO_PESO("Confirmação de Peso"),
    CARREGAMENTO("Carregamento"),
    AGUARDANDO_CHAMADA_CARREGAMENTO("Aguardando Chamada para Entrada"),
    EM_FILA_CARGA_DESCARGA("Em Fila de Carga ou Descarga"),
    AGUARDANDO_CHEGADA_AMOSTRA("Aguardando Chegada Amostra");

    private String descricao;

}
