package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.Classificacao;
import br.com.sjc.modelo.Pesagem;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.sap.Safra;
import br.com.sjc.modelo.sap.Veiculo;
import br.com.sjc.util.BigDecimalUtil;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.anotation.ProjectionConfigurationDTO;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ProjectionConfigurationDTO(groupProperties = true)
public class RomaneioClassificacaoListagemDTO extends DataDTO {

    @ProjectionProperty(values = {"numeroRomaneio"}, useForCountTotal = true)
    private String numeroRomaneio;

    @ProjectionProperty(values = {"id", "operacao", "entidade"})
    private DadosSincronizacao operacao;

    @ProjectionProperty(values = {"id", "placa1"})
    private Veiculo placaCavalo;

    @ProjectionProperty(values = {"id", "codigo", "descricao"})
    private Safra safra;

    @ProjectionProperty(values = {"id", "pesoInicial", "pesoFinal", "placaCavalo.id", "placaCavalo.placa1"})
    private Pesagem pesagem;

    @ProjectionProperty(values = {"id", "dataCadastro"})
    private Classificacao classificacao;

    @ProjectionProperty
    private StatusIntegracaoSAPEnum statusIntegracaoSAP;

    @ProjectionProperty
    private StatusRomaneioEnum statusRomaneio;

    @ProjectionProperty(values = {"classificacao.itens.desconto"}, sum = true)
    private BigDecimal totalDescontos;

    private List<ItemNFPedidoListagemDTO> itens = new ArrayList<>();

    public Boolean getPossuiItens() {

        return ColecaoUtil.isNotEmpty(itens);
    }

    public BigDecimal getPesoLiquido() {

        if (ObjetoUtil.isNull(this.getPesagem()) || ObjetoUtil.isNull(this.getPesagem().getPesoInicial()) || ObjetoUtil.isNull(this.getPesagem().getPesoFinal())) {
            return BigDecimal.ZERO;
        }

        return new BigDecimal(this.getPesagem().getPesoInicial().doubleValue() - this.getPesagem().getPesoFinal().doubleValue());
    }

    public BigDecimal getPesoLiquidoSeco() {

        if (BigDecimalUtil.isMaiorQueZero(this.getPesoLiquido()) && BigDecimalUtil.isMaiorQueZero(this.getTotalDescontos())) {

            return this.getPesoLiquido().subtract(this.getTotalDescontos());

        } else if (BigDecimalUtil.isMaiorQueZero(this.getPesoLiquido())) {

            return this.getPesoLiquido();
        }

        return BigDecimal.ZERO;
    }

}
