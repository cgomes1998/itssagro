package br.com.sjc.modelo.sap.email;

import br.com.sjc.modelo.sap.ItemNFPedido;
import br.com.sjc.modelo.sap.Motorista;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.Veiculo;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.email.Email;
import br.com.sjc.util.email.EmailBodyBuilder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by julio.bueno on 24/07/2019.
 */
@RequiredArgsConstructor
public class EmailRomaneioBloqueadoPorIndiceClassificacao implements Email {

    @NonNull
    private Romaneio romaneio;

    @Override
    public String getAssunto() {
        return "Romaneio ${romaneio.numeroRomaneio} - Bloqueado por Índice de Classificação";
    }

    @Override
    public String getTextoEmail() {
        return EmailBodyBuilder
                .build()
                .addText("O Romaneio ${romaneio.numeroRomaneio} referente o(s) Fornecedo(res): ${fornecedores}; Veículo ${romaneio.placaCavalo} e Motorista: ${romaneio.motorista}, está com os seguintes itens pendentes de aprovação:")
                .addLineBreak(2)
                .addHtml(montarTabelaIndicesClassificacao())
                .addLineBreak(2)
                .addText("Podemos seguir com o fluxo?")
                .toString();
    }

    @Override
    public List<String> getDestinatario() {
        return romaneio.getEmails();
    }

    @Override
    public Map<String, String> getParametros() {
        Map<String, String> parametros = new HashMap<>();
        String placaCavalo = "";
        String motorista = "";
        if (romaneio.getOperacao().isPossuiDivisaoCarga()) {
            placaCavalo = romaneio.getItens().stream().map(ItemNFPedido::getPlacaCavalo).filter(Objects::nonNull).map(Veiculo::getPlaca1).findFirst().orElse("");
            motorista = romaneio.getItens().stream().map(ItemNFPedido::getMotorista).filter(Objects::nonNull).map(Motorista::getNome).findFirst().orElse("");
        } else {
            placaCavalo = ObjetoUtil.isNotNull(romaneio.getPlacaCavalo()) ? romaneio.getPlacaCavalo().getPlaca1() : "";
            motorista = ObjetoUtil.isNotNull(romaneio.getMotorista()) ? romaneio.getMotorista().getNome() : "";
        }
        parametros.put("${romaneio.numeroRomaneio}", romaneio.getNumeroRomaneio());
        parametros.put("${romaneio.placaCavalo}", placaCavalo);
        parametros.put("${romaneio.motorista}", motorista);
        parametros.put("${fornecedores}", getFornecedores());

        return parametros;
    }

    private String getFornecedores() {
        StringBuilder fornecedores = new StringBuilder();

        if (romaneio.getItens().isEmpty()) {
            if (ObjetoUtil.isNotNull(romaneio.getProdutor())) {
                fornecedores.append(romaneio.getProdutor().getNome());
            }
        } else {
            fornecedores.append(romaneio.getItens().stream().map(item -> item.getProdutor().getNome()).reduce("", (valorAcumulado, valor) -> {
                if (valorAcumulado.equalsIgnoreCase("")) {
                    valorAcumulado = valor;

                } else {
                    valorAcumulado += ", " + valor;

                }
                return valorAcumulado;
            }));

        }

        return fornecedores.toString();
    }

    private String montarTabelaIndicesClassificacao() {
        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMinimumFractionDigits(2);

        EmailBodyBuilder emailBodyBuilder = EmailBodyBuilder.build();

        String styleColuna = "style=\"border: 1px solid #000; min-width: 200px; font-size: 12px;\"";
        String styleColunaNumero = "style=\"border: 1px solid #000; min-width: 200px; font-size: 12px; text-align: right;\"";

        emailBodyBuilder.addHtml("<table style=\"border-collapse: collapse;\">");
        emailBodyBuilder.addHtml("<tr>");
        emailBodyBuilder.addHtml("<th colspan=\"3\" ").addHtml(styleColuna).addHtml(">");
        emailBodyBuilder.addText("Classificação");
        emailBodyBuilder.addHtml("</th>");
        emailBodyBuilder.addHtml("</tr>");
        emailBodyBuilder.addHtml("<tr>");
        emailBodyBuilder.addHtml("<th ").addHtml(styleColuna).addHtml(">");
        emailBodyBuilder.addText("Índice");
        emailBodyBuilder.addHtml("</th>");
        emailBodyBuilder.addHtml("<th ").addHtml(styleColuna).addHtml(">");
        emailBodyBuilder.addText("Limíte");
        emailBodyBuilder.addHtml("</th>");
        emailBodyBuilder.addHtml("<th ").addHtml(styleColuna).addHtml(">");
        emailBodyBuilder.addText("Valor Atual");
        emailBodyBuilder.addHtml("</th>");
        emailBodyBuilder.addHtml("<tr>");
        emailBodyBuilder.addHtml("<tr>");

        romaneio.getIndicesBloqueados().forEach(indicebloqueado -> {
            emailBodyBuilder.addHtml("<td ").addHtml(styleColuna).addHtml(">");
            emailBodyBuilder.addText(indicebloqueado.getDescricaoItem());
            emailBodyBuilder.addHtml("</td>");
            emailBodyBuilder.addHtml("<td ").addHtml(styleColunaNumero).addHtml(">");
            emailBodyBuilder.addText(numberFormat.format(indicebloqueado.getLimite()) + "%");
            emailBodyBuilder.addHtml("</td>");
            emailBodyBuilder.addHtml("<td ").addHtml(styleColunaNumero).addHtml(">");
            emailBodyBuilder.addText(numberFormat.format(indicebloqueado.getIndice()) + "%");
            emailBodyBuilder.addHtml("</td>");
        });

        emailBodyBuilder.addHtml("</tr>");
        emailBodyBuilder.addHtml("</table>");

        return emailBodyBuilder.toString();
    }
}
