package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@SAPParameter(function = "/ITSSAGRO/SALDO_ORDEM_COMPRA4")
public class RfcToleranciaOrdemPedidoResponse extends JCoResponse {

    @SAPColumn(name = "E_QTD")
    private BigDecimal saldo;

    @SAPColumn(name = "E_MESSAGE")
    private String mensagemException;
}
