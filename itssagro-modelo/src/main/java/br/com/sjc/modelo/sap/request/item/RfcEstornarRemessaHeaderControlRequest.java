package br.com.sjc.modelo.sap.request.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SAPParameter( function = "BAPIOBDLVHDRCTRLCHG" )
public class RfcEstornarRemessaHeaderControlRequest extends JCoItem {

    @SAPColumn( name = "DELIV_NUMB", size = 10 )
    private String numeroRemessa;

    @SAPColumn( name = "DLV_DEL", size = 1 )
    private String eliminar;

}
