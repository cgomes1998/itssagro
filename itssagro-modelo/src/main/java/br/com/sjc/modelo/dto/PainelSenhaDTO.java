package br.com.sjc.modelo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PainelSenhaDTO extends DataDTO {

    private MaterialDTO material;

    private List<SenhaDTO> senhaList;

}
