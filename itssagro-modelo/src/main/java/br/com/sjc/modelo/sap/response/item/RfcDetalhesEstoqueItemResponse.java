package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SAPParameter(function = "BAPI_MATERIAL_STOCK_REQ_LIST")
public class RfcDetalhesEstoqueItemResponse extends JCoItem {

    @SAPColumn(name = "UNRESTRICTED_STCK")
    private BigDecimal saldo;

    @SAPColumn(name = "RESERVATIONS")
    private BigDecimal qntdReservada;

}
