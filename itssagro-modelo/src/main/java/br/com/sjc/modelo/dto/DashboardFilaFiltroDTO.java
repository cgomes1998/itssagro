package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.Fluxo;
import br.com.sjc.modelo.sap.Material;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DashboardFilaFiltroDTO {

    private Fluxo fluxo;

    private Material material;

    private String placa;

    private String numeroRomaneio;

    private String numeroCartaoAcesso;

}
