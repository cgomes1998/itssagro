package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.response.item.RfcVeiculoResponseItem;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@SAPParameter(function = "ZITSSAGRO_FM_DM_VEICULO")
public class RfcVeiculoResponse extends JCoResponse {

    @SAPTable(name = "VEICULO", item = RfcVeiculoResponseItem.class)
    private List<RfcVeiculoResponseItem> entrada;

    @SAPTable(name = "VEICULO_RET", item = RfcVeiculoResponseItem.class)
    private List<RfcVeiculoResponseItem> retorno;

}
