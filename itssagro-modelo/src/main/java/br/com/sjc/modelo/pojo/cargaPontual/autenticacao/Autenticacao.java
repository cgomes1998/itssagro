package br.com.sjc.modelo.pojo.cargaPontual.autenticacao;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Autenticacao {

    private String tokenCombinado;

    private int identificadorEmpresa;

    private String loginUsuario;

    private String loginUsuarioSenha;

}
