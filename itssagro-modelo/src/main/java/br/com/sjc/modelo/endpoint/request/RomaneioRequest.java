package br.com.sjc.modelo.endpoint.request;

import br.com.sjc.modelo.endpoint.request.item.RomaneioRequestItem;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RomaneioRequest {

    private List<RomaneioRequestItem> romaneio;
}