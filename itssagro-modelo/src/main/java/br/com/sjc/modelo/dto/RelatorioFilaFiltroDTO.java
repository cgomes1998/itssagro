package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.Fluxo;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.util.ObjetoUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RelatorioFilaFiltroDTO {

    private Fluxo fluxo;

    private Date dataInicio;

    private Date dataFim;

    public boolean isNotNull () {
        return ObjetoUtil.isNotNull ( this.getFluxo () ) || ObjetoUtil.isNotNull ( this.getDataInicio () ) || ObjetoUtil.isNotNull ( this.getDataFim () );
    }

}
