package br.com.sjc.modelo.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum TipoParceiroSAPEnum {

    TRANSPORTADORA("SP"),
    CLIENTE("AG"),
    TRANSPORTADORA_SUB("ZS"),
    OPERADOR_LOGISTICO("ZL");

    private String codigo;

    TipoParceiroSAPEnum(final String codigo) {
        this.codigo = codigo;
    }
}
