package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.TabelaValorItem;
import br.com.sjc.modelo.sap.ConversaoLitragem;
import br.com.sjc.modelo.sap.ConversaoLitragemItem;
import lombok.Data;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Data
public class ConversaoLitragemDTO {

    private BigDecimal grauInpm;

    private BigDecimal tempTanque;

    private BigDecimal massaEspec;

    private BigDecimal massaEspecificaVinteGraus;

    private BigDecimal fatorReducao;

    private BigDecimal volumeSetaVinteGraus;

    private BigDecimal volumeDensidadeLVinteGraus;

    private Long nrTanque;

    public ConversaoLitragemDTO (
            ConversaoLitragemItem itemConversao,
            TabelaValorItem tabelaValor,
            ConversaoLitragem conversaoLitragem
    ) {

        this.setTempTanque ( conversaoLitragem.getTempTanque () );

        this.setMassaEspec ( tabelaValor.getMassaEspecifica () );

        this.setMassaEspecificaVinteGraus ( tabelaValor.getMassaEspecificaVinteGraus () );

        this.setFatorReducao ( tabelaValor.getFatorReducao() );

        this.setVolumeSetaVinteGraus ( itemConversao.getVolumeSetaVinteGraus ().setScale(0, RoundingMode.HALF_UP) );

        this.setVolumeDensidadeLVinteGraus ( itemConversao.getVolumeDensidadeLVinteGraus () );

        this.setNrTanque ( conversaoLitragem.getTanque () );

        this.setGrauInpm ( conversaoLitragem.getGrauInpm () );
    }
}
