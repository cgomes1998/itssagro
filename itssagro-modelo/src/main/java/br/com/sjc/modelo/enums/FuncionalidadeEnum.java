package br.com.sjc.modelo.enums;

import br.com.sjc.modelo.PerfilFuncionalidade;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum FuncionalidadeEnum {

    MANTER_USUARIO("Usuários", ModuloEnum.CADASTRO, true, true, true, true, true),

    MANTER_AMBIENTE("Ambientes SAP", ModuloEnum.CADASTRO, true, true, true, true, true),

    MANTER_PERFIL("Perfis", ModuloEnum.CADASTRO, true, true, true, true, true),

    MANTER_BALANCA("Balanças", ModuloEnum.CADASTRO, true, true, true, true, true),

    MANTER_EMPRESA("Empresa", ModuloEnum.CADASTRO, true, true, true, true, true),

    MANTER_PORTARIA("Portaria", ModuloEnum.CADASTRO, true, true, true, true, true),

    MANTER_MOTIVO("Motivos", ModuloEnum.CADASTRO, true, true, true, true, true),

    MANTER_HARDWARE("Hardwares", ModuloEnum.CADASTRO, true, true, true, true, true),

    MANTER_LOCAL("Locais", ModuloEnum.CADASTRO, true, true, true, true, true),

    MANTER_DOCUMENTO("Documentos", ModuloEnum.CADASTRO, true, true, true, true, true),

    MANTER_FORMULARIO("Formulários", ModuloEnum.CADASTRO, true, true, true, true, true),

    MANTER_CARTAO_ACESSO("Cartões de Acesso", ModuloEnum.CADASTRO, true, true, true, true, false),

    MANTER_LOTE("Lotes", ModuloEnum.CADASTRO, true, true, true, true, false),

    MANTER_MOTORISTA("Motorista", ModuloEnum.DADOS_MESTRES, true, true, true, true, true),

    MANTER_CLIENTE("Cliente", ModuloEnum.DADOS_MESTRES, true, true, false, true, false),

    MANTER_VEICULO("Veículo", ModuloEnum.DADOS_MESTRES, true, true, true, true, true),

    MANTER_CENTRO("Centro", ModuloEnum.DADOS_MESTRES, true, true, false, false, false),

    MANTER_MATERIAL("Material", ModuloEnum.DADOS_MESTRES, true, true, false, false, false),

    MANTER_PRODUTOR("Produtor", ModuloEnum.DADOS_MESTRES, true, true, false, false, false),

    MANTER_SAFRA("Safra", ModuloEnum.DADOS_MESTRES, true, true, false, false, false),

    MANTER_TRANSPORTADORA("Transportadora", ModuloEnum.DADOS_MESTRES, true, true, false, true, false),

    MANTER_TABELA_CLASSIFICACAO("Tabela de Classificação", ModuloEnum.DADOS_MESTRES, true, true, true, true, true),

    MANTER_TIPO_VEICULO("Tipo de Veículo", ModuloEnum.DADOS_MESTRES, true, true, false, true, false),

    MANTER_ANALISE("Análise", ModuloEnum.DADOS_MESTRES, true, true, true, true, true),

    MANTER_TABELA_VALOR("Tabela de Valores", ModuloEnum.CADASTRO, true, true, true, true, true),

    MANTER_LACRE("Lacre", ModuloEnum.CADASTRO, true, true, true, true, true),

    CONFIG_JOBS("Configurar Job´s", ModuloEnum.CONFIGURACAO, true, true, true, true, true),

    CONFIG_CAMPOS("Configuração de Campos", ModuloEnum.CONFIGURACAO, true, true, true, true, true),

    DADOS_SINCRONIZACAO("Dados Sincronização", ModuloEnum.CONFIGURACAO, true, true, true, true, true),

    MANTER_PARAMETRO("Parâmetros de Integração", ModuloEnum.CONFIGURACAO, true, true, true, true, true),

    CONFIGURACAO_FLUXO("Configuração de Fluxo", ModuloEnum.CONFIGURACAO, true, true, true, true, true),

    CLASSIFICACAO("Classificação", ModuloEnum.FLUXO, true, true, true, true, true),

    PORTARIA_EMISSAO_TICKET("Portaria Emissão Ticket", ModuloEnum.FLUXO, true, true, true, true, true),

    ATRIBUIR_LACRE_ROMANEIO("Atribuir Lacres a Romaneios", ModuloEnum.FLUXO, true, true, false, true, false),

    ROMANEIO("Ordem de Carregamento", ModuloEnum.FLUXO, true, true, true, true, true),

    PESAGEM("Pesagem", ModuloEnum.FLUXO, true, true, true, true, true),

    CONVERSAO_LITRAGEM("Conversão de Litragem", ModuloEnum.FLUXO, true, true, true, true, true),

    PAINEL_CHAMADAS("Painel de Chamadas", ModuloEnum.FLUXO, true, true, true, true, true),

    SENHA("Senha", ModuloEnum.FLUXO, true, true, true, true, true),

    DASHBOARD_FILAS("Dashboard de Filas", ModuloEnum.FLUXO, true, true, true, true, true),

    CARREGAMENTO("Carregamento", ModuloEnum.FLUXO, true, true, true, true, true),

    CONFIG_EMAIL("Configuração E-mail", ModuloEnum.CONFIGURACAO, true, true, true, true, true),

    DASHBOARD_GERENCIAL("Dashboard Gerencial", ModuloEnum.FLUXO, true, true, true, true, true),

    CHECKLIST("Checklist", ModuloEnum.FLUXO, true, false, false, true, false),

    LAUDO("Laudo", ModuloEnum.FLUXO, true, true, true, true, true),

    RELATORIO_COMERCIAL("Relatório de Romaneios", ModuloEnum.RELATORIOS, true, false, false, false, false),

    RELATORIO_LAUDO("Relatório de Laudos", ModuloEnum.RELATORIOS, true, false, false, false, false),

    RELATORIO_ROMANEIO_COMPRA_VENDA("Relatório de Romaneios de Compra e Venda", ModuloEnum.RELATORIOS, true, false, false, false, false),

    RELATORIO_ORDEM("Relatório Ordens de venda", ModuloEnum.RELATORIOS, true, false, false, false, false),

    RELATORIO_PESAGEM_MANUAL("Relatório de Pesagens manuais", ModuloEnum.RELATORIOS, true, false, false, false, false),

    RELATORIO_ANALISE_CONVERSAO("Relatório de Análise de Conversão", ModuloEnum.RELATORIOS, true, false, false, false, false),

    RELATORIO_FLUXO_CARREGAMENTO("Relatório de Fluxo de Carregamento", ModuloEnum.RELATORIOS, true, false, false, false, false),

    RELATORIO_COMERCIAL_V2("Relatório Comercial", ModuloEnum.RELATORIOS, true, false, false, false, false),

    RELATORIO_DEPOSITO("Relatório de Romaneios Grãos", ModuloEnum.RELATORIOS, true, false, false, false, false),

    RELATORIO_PORTARIA_TICKET("Relatório Portaria", ModuloEnum.RELATORIOS, true, false, false, false, false),

    RELATORIO_LACRE("Relatório de Lacres", ModuloEnum.RELATORIOS, true, false, false, false, false),

    RELATORIO_LOTE("Relatório de Lotes", ModuloEnum.RELATORIOS, true, false, false, false, false),

    RELATORIO_FILA("Relatório de Filas", ModuloEnum.RELATORIOS, true, false, false, false, false),

    AUTOATENDIMENTO_MOTORISTA("Autoatendimento ao Motorista", ModuloEnum.AUTOATENDIMENTO, true, false, false, false, false),

    VEICULO_LIBERADO("Veículo Liberado", ModuloEnum.AUTOATENDIMENTO, true, false, false, false, false),

    ENTRADA_LIBERADA("Entrada Liberada", ModuloEnum.AUTOATENDIMENTO, true, false, false, false, false),

    ENTRADA_MOTORISTA("Autoatendimento Apresentação do Motorista na Planta", ModuloEnum.AUTOATENDIMENTO, true, false, false, false, false);

    @NonNull
    private String descricao;

    @NonNull
    private ModuloEnum modulo;

    @NonNull
    private Boolean permitirConsultar;

    @NonNull
    private Boolean permitirVisualizar;

    @NonNull
    private Boolean permitirIncluir;

    @NonNull
    private Boolean permitirAlterar;

    @NonNull
    private Boolean permitirExcluir;

    public PerfilFuncionalidade toEntidade() {

        PerfilFuncionalidade entidade = new PerfilFuncionalidade();
        entidade.setFuncionalidade(this);
        entidade.setAlterar(false);
        entidade.setExcluir(false);
        entidade.setIncluir(false);
        entidade.setVisualizar(false);
        entidade.setConsultar(false);

        entidade.setPermitirConsultar(permitirConsultar);
        entidade.setPermitirVisualizar(permitirVisualizar);
        entidade.setPermitirIncluir(permitirIncluir);
        entidade.setPermitirAlterar(permitirAlterar);
        entidade.setPermitirExcluir(permitirExcluir);

        return entidade;
    }

}
