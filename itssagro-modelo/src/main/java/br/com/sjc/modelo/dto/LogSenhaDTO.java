package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.LogSenha;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LogSenhaDTO extends DataDTO {

    private SenhaDTO senha;

    private String statusSenhaDescricao;

    private Long idUsuarioAutorCadastro;

    public LogSenhaDTO(LogSenha logSenha) {
        setId(logSenha.getId());
        setSenha(new SenhaDTO(logSenha.getSenha()));
        setDataCadastro(logSenha.getDataCadastro());
        setStatusSenhaDescricao(logSenha.getStatusSenha().getDescricao());
        setIdUsuarioAutorCadastro(logSenha.getIdUsuarioAutorCadastro());
    }

}
