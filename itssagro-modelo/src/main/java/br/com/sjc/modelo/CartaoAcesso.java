package br.com.sjc.modelo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_cartao_acesso", schema = "public")
public class CartaoAcesso extends EntidadeAutenticada {

	@Column(name = "codigo")
	private String codigo;

	@Column(name = "numero_cartao_acesso")
	private Integer numeroCartaoAcesso;

}
