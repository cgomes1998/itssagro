package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.StatusLacreEnum;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_lote")
public class Lote extends EntidadeAutenticada {

    @Column
    private String prefixo;

    @Column
    private Integer identInicial;

    @Column
    private Integer identFinal;

    @Column
    private Integer quantidade;

    @EntityProperties(value = {"id", "lote.id", "codigo", "observacaoStatusLacreAlterar", "statusLacre", "dataInutilizacao", "usuarioQueInutilizou.id", "motivoInutilizacao.id"}, targetEntity = Lacre.class)
    @OrderBy("id")
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "lote", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    private List<Lacre> lacres = new ArrayList<>();

    @Transient
    private String codigo;

    @Transient
    private StatusLacreEnum statusLacre;

    @Transient
    private Date dataCadastroDe;

    @Transient
    private Date dataCadastroAte;

}
