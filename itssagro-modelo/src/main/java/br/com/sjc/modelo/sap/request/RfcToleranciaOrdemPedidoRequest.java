package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPParameter(function = "/ITSSAGRO/SALDO_ORDEM_COMPRA4")
public class RfcToleranciaOrdemPedidoRequest extends JCoRequest {

    @SAPColumn(name = "I_EBELN")
    private String pedido;

    @SAPColumn(name = "I_EBELP")
    private String itemPedido;

    @SAPColumn(name = "I_OPER")
    private String operacao;
}
