package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.EventoAutomacaoGraos;
import br.com.sjc.modelo.Fluxo;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventoAutomacaoGraosDTO extends DataDTO {

	@ProjectionProperty
	private String descricao;

	@ProjectionProperty
	private StatusEnum status;
}
