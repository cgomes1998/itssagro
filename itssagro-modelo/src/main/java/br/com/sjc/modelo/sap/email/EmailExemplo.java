package br.com.sjc.modelo.sap.email;

import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.util.Email;
import br.com.sjc.util.EmailBodyBuilder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by julio.bueno on 23/07/2019.
 */
@RequiredArgsConstructor
public class EmailExemplo implements Email {

    @NonNull
    private Romaneio romaneio;

    @Override
    public String getAssunto() {
        return "Texto Exibido no assunto ${romaneio.fazenda}";
    }

    @Override
    public String getTextoEmail() {
        return EmailBodyBuilder
                .build()
                .addText("Texto do email ${romaneio.fazenda}")
                .addLineBreak(2)
                .addTextStrong("Texto em negrito ${romaneio.numero}")
                .open_P_element("style=\"text-align: right\"")
                    .addText("Mais texto do email")
                .close_P_element()
                .toString();
    }

    @Override
    public List<String> getDestinatario() {
        List<String> destinatarios = new ArrayList<>();
        destinatarios.add("destinario1@mail.com");
        destinatarios.add("destinario2@mail.com");
        // .......

        return destinatarios;
    }

    @Override
    public Map<String, String> getParametros() {
        Map<String, String> parametros = new HashMap<>();
        parametros.put("${romaneio.fazenda}", romaneio.getFazenda());
        parametros.put("${romaneio.numero}", romaneio.getNumeroRomaneio());

        return parametros;
    }
}
