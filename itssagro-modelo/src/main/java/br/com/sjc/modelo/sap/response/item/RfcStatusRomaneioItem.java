package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class RfcStatusRomaneioItem extends JCoItem {

    @SAPColumn(name = "MANDT")
    private String mandante;

    @SAPColumn(name = "MODLO")
    private String modulo;

    @SAPColumn(name = "OPERA")
    private String operacao;

    @SAPColumn(name = "CHAVE")
    private String chave;

    @SAPColumn(name = "STAGRO")
    private Integer status;
}
