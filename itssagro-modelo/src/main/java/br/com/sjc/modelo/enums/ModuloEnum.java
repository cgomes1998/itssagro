package br.com.sjc.modelo.enums;

public enum ModuloEnum {

    DASHBOARD,

    CONFIGURACAO,

    DADOS_MESTRES,

    CADASTRO,

    FLUXO,

    RELATORIOS,

    AUTOATENDIMENTO;
}
