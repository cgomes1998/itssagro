package br.com.sjc.modelo.cfg;

import br.com.sjc.modelo.EntidadeAutenticada;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_empresa", schema = "cfg")
public class Empresa extends EntidadeAutenticada {
    private static final long serialVersionUID = 1L;

    @Column(name = "razao_social")
    private String razaoSocial;

    @Column(name = "cnpj", length = 20)
    private String cnpj;

    @Column(name = "endereco")
    private String endereco;

    @Column(name = "cidade")
    private String cidade;

    @Column(name = "estado")
    private String estado;

    @Column(name = "cep", length = 10)
    private String cep;

    @Column(name = "codigo_anp")
    private String codigoAnp;

}
