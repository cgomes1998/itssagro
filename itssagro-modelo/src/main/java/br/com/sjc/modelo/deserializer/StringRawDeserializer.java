package br.com.sjc.modelo.deserializer;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

/**
 * Created by Claudio Gomes on 19/08/2020
 */
public class StringRawDeserializer extends JsonDeserializer<String> {

	@Override
	public String deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException {

		JsonNode jsonNode = jsonParser.getCodec().readTree(jsonParser);

		return jsonNode.toString();
	}

}
