package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@SAPParameter(function = "/ITSSAGRO/SALDO_ORDEM_COMPRA3")
public class RfcSaldoOrdemPedidoResponse extends JCoResponse {

    @SAPColumn(name = "E_QTD")
    private BigDecimal saldo;

    @SAPColumn(name = "E_UNIT")
    private BigDecimal valorUnitario;

    @SAPColumn(name = "E_SAFRA")
    private String safra;

    @SAPColumn(name = "E_LIFNR")
    private String produtor;

    @SAPColumn(name = "E_WERKS")
    private String centro;

    @SAPColumn(name = "E_DEPOSITO")
    private String deposito;

    @SAPColumn(name = "E_LOCALPESAGEM")
    private String localPesagem;

    @SAPColumn(name = "E_MATNR")
    private String material;

    @SAPColumn(name = "E_MESSAGE")
    private String mensagemException;
}
