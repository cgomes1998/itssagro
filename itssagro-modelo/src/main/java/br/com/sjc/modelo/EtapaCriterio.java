package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.CriterioTipoEnum;
import br.com.sjc.modelo.serializer.FluxoEtapaSerializer;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_etapa_criterio")
public class EtapaCriterio extends EntidadeAutenticada {

    private String email;

    @Column(name = "numero_criterio")
    private Integer numeroCriterio;

    //criterio falha
    @Column(name = "repetir_etapa")
    private Integer repetirEtapa = 0;

    @Enumerated(EnumType.STRING)
    private CriterioTipoEnum tipo;

    @EntityProperties(value = {"id", "sequencia", "etapa", "statusCargaPontualDestinoSucesso", "statusCargaPontualDestinoEstorno"})
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_fluxo_etapa", nullable = false)
    @JsonIgnore
    private FluxoEtapa fluxoEtapa;

    @EntityProperties(value = {"id", "sequencia", "etapa", "statusCargaPontualDestinoSucesso", "statusCargaPontualDestinoEstorno"})
    @ManyToOne
    @JoinColumn(name = "id_proxima_etapa", referencedColumnName = "id")
    @JsonSerialize(using = FluxoEtapaSerializer.class)
    private FluxoEtapa proximaEtapa;

    //condicao
    @EntityProperties(value = {"id", "etapaCriterio.id", "numeroCondicao", "etapa", "campo", "condicao", "valor", "operadorEntrada", "operadorSaida"},
            targetEntity = EtapaCriterioCondicao.class)
    @OrderBy("id")
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "etapaCriterio", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<EtapaCriterioCondicao> condicoes = new ArrayList<>();

    //acao
    @EntityProperties(value = {"id", "etapaCriterio.id", "numero", "etapa", "campo", "valor"}, targetEntity = EtapaCriterioAcao.class)
    @OrderBy("id")
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "etapaCriterio", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<EtapaCriterioAcao> acoes = new ArrayList<>();

    //automacao
    @EntityProperties(value = {"id", "etapaCriterio.id", "eventoAutomacaoGraosEtapa.id", "numeroEvento", "criterioTipo", "evento", "local.id", "local.codigo", "local.codigoFormatado", "local.descricao", "hardware.id", "hardware.codigo", "hardware.codigoFormatado", "hardware.descricao", "hardware.endereco", "hardware.identificacao", "hardware.porta", "statusAutomacao", "descricaoStatus", "exibirMensagem", "mensagemUm", "mensagemDois",
            "mensagemTres"}, targetEntity = EtapaCriterioEvento.class)
    @OrderBy("id")
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "etapaCriterio", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<EtapaCriterioEvento> eventos = new ArrayList<>();

}
