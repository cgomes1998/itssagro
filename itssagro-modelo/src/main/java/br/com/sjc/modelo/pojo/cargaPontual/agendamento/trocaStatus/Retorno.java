package br.com.sjc.modelo.pojo.cargaPontual.agendamento.trocaStatus;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Retorno {

    private RetornoConteudo retorno;

}
