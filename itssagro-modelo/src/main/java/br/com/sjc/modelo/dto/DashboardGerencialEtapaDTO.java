package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.FluxoEtapa;
import br.com.sjc.modelo.enums.EtapaEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DashboardGerencialEtapaDTO {

    private FluxoEtapa etapa;

    private boolean etapaFinal;

    private long totalFila;

    private double duracaoMedia;

}
