package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.StatusCarregamentoEnum;
import br.com.sjc.modelo.enums.StatusDescarregamentoEnum;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_carregamento", schema = "sap")
public class Carregamento extends EntidadeGenerica {

    @Column(name = "tanque")
    private String tanque;

    @Column(name = "temperatura_tanque", scale = 4)
    private BigDecimal temperaturaTanque;

    @Column(name = "temperatura_carreta", scale = 4)
    private BigDecimal temperaturaCarreta;

    @Column(name = "grau_inpm", scale = 4)
    private BigDecimal grauINPM;

    @Column(name = "massa_especifica_temperatura_ensaio", scale = 4)
    private BigDecimal massaEspecificaTemperaturaEnsaio;

    @Column(name = "massa_especifica_vintegraus", scale = 4)
    private BigDecimal massaEspecificaVinteGraus;

    @Column(name = "lacre_amostra")
    private String lacreAmostra;

    @Column(name = "certificado")
    private Integer certificado;

    @Column(name = "dt_carregamento")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dataCarregamento;

    @Column(name = "dt_descarregamento")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dataDescarregamento;

    @Column(name = "status_carregamento")
    @Enumerated(EnumType.STRING)
    private StatusCarregamentoEnum statusCarregamento = StatusCarregamentoEnum.AGUARDANDO;

    @Column(name = "status_descarregamento")
    @Enumerated(EnumType.STRING)
    private StatusDescarregamentoEnum statusDescarregamento = StatusDescarregamentoEnum.AGUARDANDO;

    @EntityProperties(value = {"id", "descricao", "tipo", "conferenciaPeso", "buscarDadosRomaneio"}, targetEntity = Motivo.class)
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "tb_carregamento_motivo", schema = "public", joinColumns = @JoinColumn(name = "id_carregamento"), inverseJoinColumns = @JoinColumn(name = "id_motivo"))
    private List<Motivo> motivos = new ArrayList<>();

    public Carregamento() {

        super();
    }

    public Carregamento(String tanque, BigDecimal temperaturaTanque, BigDecimal temperaturaCarreta, BigDecimal grauINPM, BigDecimal massaEspecificaTemperaturaEnsaio, BigDecimal massaEspecificaVinteGraus, String lacreAmostra,
                        Integer certificado, Date dataCarregamento, Date dataDescarregamento, StatusCarregamentoEnum statusCarregamento, StatusDescarregamentoEnum statusDescarregamento, List<Motivo> motivos) {

        this.tanque = tanque;
        this.temperaturaTanque = temperaturaTanque;
        this.temperaturaCarreta = temperaturaCarreta;
        this.grauINPM = grauINPM;
        this.massaEspecificaTemperaturaEnsaio = massaEspecificaTemperaturaEnsaio;
        this.massaEspecificaVinteGraus = massaEspecificaVinteGraus;
        this.lacreAmostra = lacreAmostra;
        this.certificado = certificado;
        this.dataCarregamento = dataCarregamento;
        this.dataDescarregamento = dataDescarregamento;
        this.statusCarregamento = statusCarregamento;
        this.statusDescarregamento = statusDescarregamento;
        if (ColecaoUtil.isNotEmpty(motivos)) {
            this.motivos = motivos.stream().map(m -> new Motivo(m.getDescricao(), m.getTipo())).collect(Collectors.toList());
        }
    }

}
