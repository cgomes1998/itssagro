package br.com.sjc.modelo.enums;

import lombok.Getter;

@Getter
public enum StatusInscEstadualEnum {

    ATIVO("Ativo"),

    INATIVO("Inativa"),

    PENDENTE("Pendente");

    private String descricao;

    private StatusInscEstadualEnum(String descricao) {

        this.descricao = descricao;
    }
}
