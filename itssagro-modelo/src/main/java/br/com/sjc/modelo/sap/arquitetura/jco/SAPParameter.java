package br.com.sjc.modelo.sap.arquitetura.jco;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * <b>Title:</b> SAPParameter
 * </p>
 * 
 * <p>
 * <b>Description:</b> Annotation responsável por definir uma classe como parâmetro de entrada ou saída de uma função SAP.
 * </p>
 * 
 * @author Bruno Zafalão
 * 
 * @version 1.0.0
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SAPParameter {

	/**
	 * Propriedade que identifica o nome da função que será utilizada.
	 * 
	 * @author Bruno Zafalão
	 */
	String function() default "";
}
