package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusAutomacaoEnum;
import lombok.Data;

@Data
public class NrCartaoStatusDTO {

    private Integer nrCartao;
    private StatusAutomacaoEnum status;

    public NrCartaoStatusDTO(String retorno) {
        String[] retornos = retorno.split(",");
        if (retornos.length >= 2) {
            this.nrCartao = Integer.valueOf(retornos[0]);
            this.status = StatusAutomacaoEnum.valueOf(Integer.parseInt(retornos[1]));
        }
    }

}
