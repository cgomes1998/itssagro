package br.com.sjc.modelo.sap.arquitetura.jco;

import br.com.sjc.util.StringUtil;

import java.util.ResourceBundle;

/**
 * <p>
 * <b>Title:</bSAPConfigurationAdapter
 * </p>
 *
 * <p>
 * <b>Description:</bClasse responsável por adaptar os métodos de configuração da comunicação com o SAP.
 * </p>
 *
 * @author Bruno Zafalão
 * @version 1.0.0
 */
public class SAPConfigurationAdapter implements SAPConfiguration {

    /**
     * Atributo bundle.
     */
    private static ResourceBundle bundle;

    /**
     * Responsável pela criação de novas instâncias desta classe.
     *
     * @param bundle
     */
    protected SAPConfigurationAdapter(final ResourceBundle bundle) {

        SAPConfigurationAdapter.bundle = bundle;
    }

    /**
     * Responsável pela criação de novas instâncias desta classe.
     */
    protected SAPConfigurationAdapter() {

        SAPConfigurationAdapter.bundle = null;
    }

    /**
     * Método responsável por obter o valor do atributo de configuração informado.
     *
     * @param key
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    public static String get(final String key) {

        return SAPConfigurationAdapter.getProperties().getString(key);
    }

    /**
     * Método responsável por obter o arquivo de configurações do sistema.
     *
     * @return <code>ResourceBundle</code>
     * @author Bruno Zafalão
     */
    public static ResourceBundle getProperties() {

        return SAPConfigurationAdapter.bundle;
    }

    /**
     * Método responsável por obter o valor da propriedade <code>ashost</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String ashost() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>mshost</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String mshost() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>gwhost</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String gwhost() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>tphost</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String tphost() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>type</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String type() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>gwserv</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String gwserv() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>useguihost</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String useguihost() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>useguiserv</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String useguiserv() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>useguiprogid</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String useguiprogid() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>sncmode</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String sncmode() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>sncpartnername</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String sncpartnername() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>sncqop</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String sncqop() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>sncmyname</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String sncmyname() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>snclib</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String snclib() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>dest</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String dest() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>dsr</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String dsr() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>saplogonid</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String saplogonid() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>extiddata</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String extiddata() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>extidtype</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String extidtype() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>client</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String client() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>user</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String user() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>codepage</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String codepage() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>abapdebug</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String abapdebug() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>usesapgui</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String usesapgui() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>getsso2</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String getsso2() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>mysapsso2</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String mysapsso2() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>x509cert</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String x509cert() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>lcheck</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String lcheck() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>grtdata</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String grtdata() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>passwd</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String passwd() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>sysnr</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String sysnr() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>r3name</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String r3name() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>group</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String group() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>trace</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String trace() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>aliasuser</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String aliasuser() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>lang</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String lang() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>idletimeout</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String idletimeout() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>tpname</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String tpname() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>endpointname</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String endpointname() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>destinationName</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String destinationName() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>saprouter</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String saprouter() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>conection_count</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String conectioncount() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>progid</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String progid() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>servername</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String serverName() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>repositoryname</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String repositoryName() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>functionname</code>
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    @Override
    public String functionName() {

        return StringUtil.empty();
    }

    /**
     * Método responsável por obter o valor da propriedade <code>codigoconexao</code>
     *
     * @return <code>String</code>
     * @author Ezequiel Bispo Nunes
     */
    @Override
    public String codigoConexao() {

        return StringUtil.empty();
    }
}
