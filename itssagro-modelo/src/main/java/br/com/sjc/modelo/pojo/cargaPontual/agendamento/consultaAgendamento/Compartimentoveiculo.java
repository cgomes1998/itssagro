package br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Compartimentoveiculo {

    private List<Compartimento1> compartimento1;

}
