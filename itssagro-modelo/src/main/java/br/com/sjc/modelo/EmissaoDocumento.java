package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.StatusEmissaoDocumentoEnum;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_emissao_documento", schema = "sap")
public class EmissaoDocumento extends EntidadeGenerica {

    @Column(name = "status_emissao_documento")
    @Enumerated(EnumType.STRING)
    private StatusEmissaoDocumentoEnum statusEmissaoDocumento = StatusEmissaoDocumentoEnum.AGUARDANDO;

    @Column(name = "documento_impresso")
    private boolean documentoImpresso;

    @Column(name = "nf_impresso")
    private boolean nfImpresso;

    @Column(name = "laudo_analise_impresso")
    private boolean laudoAnaliseImpresso;

    @Column(name = "romaneio_impresso")
    private boolean romaneioImpresso;

    @Column(name = "formulario_impresso")
    private boolean formularioImpresso;

    @EntityProperties(value = {"id", "descricao", "tipo", "conferenciaPeso", "buscarDadosRomaneio"}, targetEntity = Motivo.class)
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "tb_emissao_documento_motivo", schema = "public", joinColumns = @JoinColumn(name = "id_emissao_documento"), inverseJoinColumns = @JoinColumn(name = "id_motivo"))
    private List<Motivo> motivos = new ArrayList<>();

    public EmissaoDocumento() {

        super();
    }

    public EmissaoDocumento(StatusEmissaoDocumentoEnum statusEmissaoDocumento, boolean documentoImpresso, boolean nfImpresso, boolean laudoAnaliseImpresso, boolean romaneioImpresso, List<Motivo> motivos) {

        this.statusEmissaoDocumento = statusEmissaoDocumento;
        this.documentoImpresso = documentoImpresso;
        this.nfImpresso = nfImpresso;
        this.laudoAnaliseImpresso = laudoAnaliseImpresso;
        this.romaneioImpresso = romaneioImpresso;
        if (ColecaoUtil.isNotEmpty(motivos)) {
            this.motivos = motivos.stream().map(m -> new Motivo(m.getDescricao(), m.getTipo())).collect(Collectors.toList());
        }
    }

}
