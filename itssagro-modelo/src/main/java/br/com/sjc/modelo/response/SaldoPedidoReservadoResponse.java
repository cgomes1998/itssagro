package br.com.sjc.modelo.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
public class SaldoPedidoReservadoResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private BigDecimal saldoReservado;
}
