package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.Classificacao;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_itens_classificacao", schema = "sap")
public class ItensClassificacao implements Serializable, Comparable<ItensClassificacao> {

    private static final long serialVersionUID = -3351987708785118658L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "item_classificacao")
    private String itemClassificao;

    @Column(name = "desc_item_classificacao")
    private String descricaoItem;

    @Column(name = "codigo_tabela")
    private String codigoTabela;

    private BigDecimal indice;

    @Column(name = "percentual_desconto")
    private BigDecimal percentualDesconto;

    @Column(name = "desconto")
    private BigDecimal desconto;

    @Column(name = "desconto_classificacao")
    private BigDecimal descontoClassificacao;

    @EntityProperties(value = {"id"})
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_classificacao")
    private Classificacao classificacao;

    @EntityProperties(value = {"id"})
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_romaneio")
    private Romaneio romaneio;

    @Transient
    private String codigoLeitor;

    @Transient
    private String hostLeitor;

    @Transient
    private String portaLeitor;

    @Transient
    private String codigoMaterial;

    @Transient
    private String codigoArmazemTerceiros;

    @Transient
    private Double indiceNumerico;

    @Transient
    private Double limite;

    public ItensClassificacao() {

        super();
    }

    public ItensClassificacao(String itemClassificao, String descricaoItem, String codigoTabela, BigDecimal indice, BigDecimal percentualDesconto, BigDecimal desconto, BigDecimal descontoClassificacao) {

        this.itemClassificao = itemClassificao;
        this.descricaoItem = descricaoItem;
        this.codigoTabela = codigoTabela;
        this.indice = indice;
        this.percentualDesconto = percentualDesconto;
        this.desconto = desconto;
        this.descontoClassificacao = descontoClassificacao;
    }

    public BigDecimal getPercentualDescontoValido() {

        return this.getPercentualDesconto() != null ? this.getPercentualDesconto() : BigDecimal.ZERO;
    }

    public BigDecimal getDescontoValido() {

        return this.getDesconto() != null ? this.getDesconto() : BigDecimal.ZERO;
    }

    public BigDecimal getDescontoClassificacaoValido() {

        return this.getDescontoClassificacao() != null ? this.getDescontoClassificacao() : BigDecimal.ZERO;
    }

    public Long getItemClassificacaoNumerico() {

        try {

            return Long.valueOf(itemClassificao);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return 0l;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        ItensClassificacao that = (ItensClassificacao) o;
        return Objects.equals(descricaoItem, that.descricaoItem);
    }

    @Override
    public int hashCode() {

        return Objects.hash(descricaoItem);
    }

    @Override
    public int compareTo(ItensClassificacao o) {

        return new Long(o.itemClassificao).compareTo(new Long(itemClassificao));
    }

}
