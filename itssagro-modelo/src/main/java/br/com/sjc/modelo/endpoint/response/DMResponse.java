package br.com.sjc.modelo.endpoint.response;

import br.com.sjc.modelo.endpoint.response.item.FornecedorResponse;
import br.com.sjc.modelo.endpoint.response.item.MotoristaResponse;
import br.com.sjc.modelo.endpoint.response.item.TransportadoraResponse;
import br.com.sjc.modelo.endpoint.response.item.VeiculoResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DMResponse {

    private List<FornecedorResponse> fornecedores;

    private List<TransportadoraResponse> transportadoras;

    private List<VeiculoResponse> veiculos;

    private List<MotoristaResponse> motoristas;
}