package br.com.sjc.modelo.enums;

import br.com.sjc.modelo.Senha;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.util.ObjetoUtil;

public enum LogAtividadeAcao {
    CADASTRO,
    EDICAO,
    CANCELAR_MARCACAO,
    SOLICITAR_LIBERACAO,
    TORNAR_PENDENTE,
    LIBERAR_MARCACAO,
    REPROVAR_FORMULARIO,
    APROVAR_FORMULARIO,
    INATIVAR,
    ATIVAR,
    ENVIAR_AO_SAP,
    LIBERAR_ORDEM,
    ESTORNAR,
    REPROVAR,
    APROVAR;

    public static <E> LogAtividadeAcao getLogAtividadeAcao(E entidade) {

        LogAtividadeAcao logAtividadeAcao = LogAtividadeAcao.EDICAO;

        if (entidade instanceof Senha) {

            Senha senha = (Senha) entidade;

            if (ObjetoUtil.isNotNull(senha.getLogAtividadeAcao())) {

                logAtividadeAcao = senha.getLogAtividadeAcao();
            }
        }

        if (entidade instanceof Romaneio) {

            Romaneio romaneio = (Romaneio) entidade;

            if (ObjetoUtil.isNotNull(romaneio.getLogAtividadeAcao())) {

                logAtividadeAcao = romaneio.getLogAtividadeAcao();
            }
        }

        return logAtividadeAcao;
    }
}
