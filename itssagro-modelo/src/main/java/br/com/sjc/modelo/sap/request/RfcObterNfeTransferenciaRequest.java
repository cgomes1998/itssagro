package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@SAPParameter(function = "/ITSSAGRO/XML_NFE_TRANSF_HD")
@NoArgsConstructor
@AllArgsConstructor
public class RfcObterNfeTransferenciaRequest extends JCoRequest {

    @SAPColumn(name = "I_CHNFE")
    private String chave;

    @SAPColumn(name = "I_REMESSA")
    private String remessa;

}
