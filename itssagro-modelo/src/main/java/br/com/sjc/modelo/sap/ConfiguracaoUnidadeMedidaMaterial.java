package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.EntidadeGenerica;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "tb_configuracao_unidade_medida_material", schema = "sap")
public class ConfiguracaoUnidadeMedidaMaterial extends EntidadeGenerica {

    @Column
    private BigDecimal quantidadeASerCalculada;

    @Column
    private String unidadeMedidaASerCalculada;

    @Column
    private BigDecimal quantidadeEquivalente;

    @Column
    private String unidadeMedidaEquivalente;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private boolean utilizarCasasDecimais;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_material")
    private Material material;

    public ConfiguracaoUnidadeMedidaMaterial() {
    }

    public ConfiguracaoUnidadeMedidaMaterial(
            String unidadeMedidaASerCalculada,
            String unidadeMedidaEquivalente,
            String codigoMaterial) {
        this.quantidadeASerCalculada = BigDecimal.ZERO;
        this.quantidadeEquivalente = BigDecimal.ZERO;
        this.unidadeMedidaASerCalculada = unidadeMedidaASerCalculada;
        this.unidadeMedidaEquivalente = unidadeMedidaEquivalente;
        this.material = new Material();
        this.material.setCodigo(codigoMaterial);
    }
}
