package br.com.sjc.modelo.sap.sync;

import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import br.com.sjc.modelo.enums.UFEnum;
import br.com.sjc.modelo.sap.Motorista;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SAPMotorista extends SAPEntidade {

    @SAPColumn(name = "ID_MOTORISTA")
    private String idMotorista;

    @SAPColumn(name = "CODIGO_MOT")
    private String codigo;

    @SAPColumn(name = "GRUPO_CONTA")
    private String grupoContas;

    @SAPColumn(name = "NOME_MOTORISTA")
    private String nome;

    @SAPColumn(name = "CPF_MOTORISTA")
    private String cpf;

    @SAPColumn(name = "CNH_MOTORISTA")
    private String cnh;

    @SAPColumn(name = "RUA_MOTORISTA")
    private String logradouro;

    @SAPColumn(name = "CEP_MOTORISTA")
    private String cep;

    @SAPColumn(name = "CIDADE_MOTORISTA")
    /** Atributo municipio. */
    private String cidade;

    @SAPColumn(name = "FONE_MOTORISTA")
    private String telefone;

    @SAPColumn(name = "NRO_MOTORISTA")
    private String numero;

    @SAPColumn(name = "BLOQUEADO")
    private String bloqueado;

    @SAPColumn(name = "BAIRO_MOTORISTA")
    private String bairro;

    @SAPColumn(name = "MARC_ELIM")
    private String eliminado;

    @SAPColumn(name = "REGIAO_MOTORISTA")
    private String regiao;

    public Long getIdMotorista() {
        return idMotorista != null ? Long.valueOf(idMotorista) : null;
    }

    private StatusRegistroEnum statusRegistroSAP;

    public enum COLUMN_INPUT {

        DT_ULT_INF,

        COD_MOT,

        PRE_CAD,

        NOME_MOTORISTA,

        CPF_MOTORISTA,

        CNH_MOTORISTA,

        RUA_MOTORISTA,

        CEP_MOTORISTA,

        CIDADE_MOTORISTA,

        FONE_MOTORISTA,

        FONE_MOTORISTA2,

        NRO_MOTORISTA,

        ID_MOTORISTA,

        BAIRRO_MOTORISTA,

        REGIAO_MOTORISTA;
    }

    public static SAPMotorista newInstance() {

        return new SAPMotorista();
    }

    /**
     * Retorna o valor do atributo <code>statusRegistroSAP</code>
     *
     * @return <code>StatusRegistroEnum</code>
     */
    public StatusRegistroEnum getStatusRegistroSAP() {

        this.statusRegistroSAP = StatusRegistroEnum.ATIVO;

        if (StringUtil.isNotNullEmpty(this.getBloqueado())) {

            this.statusRegistroSAP = StatusRegistroEnum.BLOQUEADO;
        }

        if (StringUtil.isNotNullEmpty(this.getEliminado())) {

            this.statusRegistroSAP = StatusRegistroEnum.ELIMINADO;
        }

        if (StringUtil.isNotNullEmpty(this.getBloqueado()) && StringUtil.isNotNullEmpty(this.getEliminado())) {

            this.statusRegistroSAP = StatusRegistroEnum.BLOQUEADO_E_ELIMINADO;
        }

        return this.statusRegistroSAP;
    }

    public Motorista copy(Motorista entidadeSaved) {

        if (ObjetoUtil.isNull(entidadeSaved)) {

            entidadeSaved = new Motorista();

            entidadeSaved.setId(this.getIdMotorista());

            entidadeSaved.setDataCadastro(DateUtil.hoje());
        }

        entidadeSaved.setNome(StringUtil.isNotNullEmpty(this.getNome()) ? this.getNome().trim() : entidadeSaved.getNome());

        entidadeSaved.setCpf(StringUtil.isNotNullEmpty(this.getCpf()) ? this.getCpf().trim() : entidadeSaved.getCpf());

        entidadeSaved.setCnh(StringUtil.isNotNullEmpty(this.getCnh()) ? this.getCnh().trim() : entidadeSaved.getCnh());

        entidadeSaved.setCep(StringUtil.isNotNullEmpty(this.getCep()) ? this.getCep().trim() : entidadeSaved.getCep());

        entidadeSaved.setRua(StringUtil.isNotNullEmpty(this.getLogradouro()) ? this.getLogradouro().trim() : entidadeSaved.getRua());

        entidadeSaved.setNumero(StringUtil.isNotNullEmpty(this.getNumero()) ? this.getNumero().trim() : entidadeSaved.getNumero());

        entidadeSaved.setMunicipio(StringUtil.isNotNullEmpty(this.getCidade()) ? this.getCidade().trim() : entidadeSaved.getMunicipio());

        entidadeSaved.setCodigo(StringUtil.isNotNullEmpty(this.getCodigo()) ? new Long(this.getCodigo().trim()).toString() : entidadeSaved.getCodigo());

        entidadeSaved.setBairro(StringUtil.isNotNullEmpty(this.getBairro()) ? this.getBairro().trim() : entidadeSaved.getBairro());

        entidadeSaved.setUf(StringUtil.isNotNullEmpty(this.getRegiao()) ? UFEnum.valueOf(this.getRegiao()) : entidadeSaved.getUf());

        entidadeSaved.setLogSAP("MOTORISTA SINCRONIZADO COM O SAP EM " + DateUtil.format("dd/MM/yyyy HH:mm:ss", DateUtil.hoje()));

        if (StringUtil.isNotNullEmpty(this.getCodigo())) {

            entidadeSaved.setLogSAP(entidadeSaved.getLogSAP() + "\nCÓDIGO: " + this.getCodigo());
        }

        entidadeSaved.setStatusCadastroSap(StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO);

        entidadeSaved.setStatus(StatusEnum.ATIVO);

        entidadeSaved.setStatusRegistro(this.getStatusRegistroSAP());

        return entidadeSaved;
    }
}
