package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPParameter(function = "/ITSSAGRO/DADOS_CONTRATO")
public class RfcDadosContratoRequest extends JCoRequest {
}
