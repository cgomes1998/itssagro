package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CondicaoEnum {

    IGUAL("==", "Igual a"),
    DIFERENTE("!=", "Diferente de"),
    MAIOR(">", "Maior que"),
    MENOR("<", "Menor que"),
    MAIOR_IGUAL(">=", "Maior ou igual a"),
    MENOR_IGUAL("<=", "Menor ou igual a");

    private String operador;

    private String descricao;

}
