package br.com.sjc.modelo.enums;

/**
 * Created by julio.bueno on 04/07/2019.
 */
public enum FormularioItemTipo {
    SESSAO,
    TEXT,
    TEXT_AREA,
    RADIO,
    CHECKBOX,
    SELECT,
    DATA,
    HORA
}
