package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class MateriaPrimaDTO extends DataDTO {
    @ProjectionProperty
    private String codigo;

    @ProjectionProperty
    private String descricao;

    @ProjectionProperty
    private StatusEnum status;
}
