package br.com.sjc.modelo.enums;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum StatusOrdemVendaEnum {

    BLOQUEIO_INTERNO("01", "Bloqueio Interno"),
    BLOQ_SINTEGRA("02", "Bloq. Sintegra"),
    BLOQ_RECEITA("03", "Bloq. Receita"),
    BLQ_RECEITA_SINTEGRA("04", "Blq. Receita/Sintegra"),
    APROV_N1("N1", "Aprov N1"),
    APROV_N2("N2", "Aprov N2"),
    APROV_N3("N3", "Aprov N3");

    private String codigoSap;

    private String descricao;

    StatusOrdemVendaEnum(String codigoSap, String descricao) {
        this.codigoSap = codigoSap;
        this.descricao = descricao;
    }

    public static StatusOrdemVendaEnum obterDescricaoPorCodigoSap(final String codigoSap) {

        return Arrays.asList(StatusOrdemVendaEnum.values())
                .stream()
                .filter(statusOrdemVendaEnum -> statusOrdemVendaEnum.getCodigoSap().equals(codigoSap))
                .findFirst()
                .orElse(null);
    }
}
