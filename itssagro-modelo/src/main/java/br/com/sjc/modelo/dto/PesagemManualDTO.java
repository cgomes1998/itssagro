package br.com.sjc.modelo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PesagemManualDTO {

    private String numeroRomaneio;

    private Date dataCadastro;

    private String material;

    private String placa;

    private String motorista;

    private String motivo;
}
