package br.com.sjc.modelo;

import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by julio.bueno on 04/07/2019.
 */
@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_formulario_item_opcao")
public class FormularioItemOpcao extends EntidadeGenerica {

    @JsonIgnore
    @EntityProperties(value = {"id"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_formulario_item", nullable = false)
    private FormularioItem formularioItem;

    @Column
    private String titulo;

    @Column
    private Integer ordem;

}
