package br.com.sjc.modelo.dto.aprovacaoDocumento;

import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LogConversaoLitragemListagemDTO extends DataDTO {

    @ProjectionProperty
    private String numeroRomaneio;

    @ProjectionProperty
    private String conteudo;

}
