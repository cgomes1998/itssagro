package br.com.sjc.modelo.pojo.cargaPontual.agendamento.trocaStatus;

import br.com.sjc.modelo.FluxoEtapa;
import br.com.sjc.modelo.Senha;
import br.com.sjc.util.DateUtil;
import lombok.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
public class Body {

    private List<AgendamentoProdutoDocaItemStatus> agendamentoprodutodocaitemstatus = new ArrayList<>();

    public Body() {

    }

    @Builder
    public Body(Senha senha, FluxoEtapa fluxoEtapa, Boolean estorno) {

        agendamentoprodutodocaitemstatus.add(AgendamentoProdutoDocaItemStatus.builder()
                .condicao(estorno ? "2" : "1")
                .dataTrocaStatus(DateUtil.format(DateUtil.PATTERN_DATA + " " + DateUtil.PATTERN_HORA, new Date()))
                .statusDestino((estorno ? fluxoEtapa.getStatusCargaPontualDestinoEstorno() : fluxoEtapa.getStatusCargaPontualDestinoSucesso()).toString())
                .idAgendamento(senha.getCargaPontualIdAgendamento())
                .quantidadeRealizada(senha.getQuantidadeCarregar().toString())
                .build());
    }
}
