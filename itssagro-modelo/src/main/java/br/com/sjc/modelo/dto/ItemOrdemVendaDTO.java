package br.com.sjc.modelo.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class ItemOrdemVendaDTO {

    private Date dataNfe;

    private String cliente;

    private String operadorLogistico;

    private String codigoOrdemVenda;

    private String itemOrdemVenda;

    private String nfe;

    private String serieNfe;

    private String valorUnitarioNfe;

    private String saldoOrdemVenda;

    private BigDecimal qntdCarregar;

    private String numeroRemessa;

    private String numeroOrdemTransporte;
}
