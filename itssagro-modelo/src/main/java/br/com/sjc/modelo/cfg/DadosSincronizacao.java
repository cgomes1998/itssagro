package br.com.sjc.modelo.cfg;

import br.com.sjc.modelo.EntidadeAutenticada;
import br.com.sjc.modelo.PerfilOperacao;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.enums.DirecaoOperacaoEnum;
import br.com.sjc.modelo.enums.ModuloOperacaoEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_dados_sincronizacao", schema = "cfg")
@SuppressWarnings("all")
public class DadosSincronizacao extends EntidadeAutenticada {

    private static final long serialVersionUID = 8347066693634178861L;

    @Enumerated(EnumType.STRING)
    private DadosSincronizacaoEnum entidade;

    private String descricao;

    @Enumerated(EnumType.STRING)
    private ModuloOperacaoEnum modulo;

    @Column(name = "direcao_operacao")
    @Enumerated(EnumType.STRING)
    private DirecaoOperacaoEnum direcaoOperacao;

    private String operacao;

    @Column(name = "evento_entrada")
    private String eventoEntrada;

    @Column(name = "evento_saida")
    private String eventoSaida;

    @Column(name = "possui_divisao_carga", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiDivisaoCarga;

    @Column(name = "possui_pesagem", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiPesagem;

    @Column(name = "possui_classificacao", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiClassificacao;

    @Column(name = "pesagem_manual", nullable = false, columnDefinition = "boolean default false")
    private boolean pesagemManual;

    @Column(name = "aplica_classificao", nullable = false, columnDefinition = "boolean default false")
    private boolean aplicaClassificacao;

    @Column(name = "simples_pesagem", nullable = false, columnDefinition = "boolean default false")
    private boolean simplesPesagem;

    @Column(name = "possui_consulta_saldo_disponivel", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiConsultaSaldoDisponivel;

    @Column(name = "possui_busca_nfe_transporte", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiBuscaNFeTransporte;

    @Column(name = "possui_classificacao_origem", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiClassificacaoOrigem;

    @Column(name = "possui_consulta_contrato", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiConsultaContrato;

    @Column(name = "possui_dados_nfe", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiDadosNfe;

    @Column(name = "possui_retorno_nfe_sap", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiRetornoNfeSAP;

    @Column(name = "possui_retorno_nfe_transporte_sap", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiRetornoNfeTransporteSAP;

    @Column(name = "possui_validacao_entre_vlr_unit_nota_e_pedido", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiValidacaoEntreVlrUniNotaEPedido;

    @Column(name = "possui_validacao_para_escriturar_nota", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiValidacaoParaEscriturarNota;

    @Column(name = "possui_impressao_por_etapa", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiImpressaoPorEtapa;

    @Column(name = "possui_validacao_chave_acesso", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiValidacaoChaveAcesso;

    @Column(name = "possui_rateio", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiRateio;

    @Column(name = "possui_validacao_divergencia_produtor_nota_e_pedido", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiValidacaoDivergenciaProdutorNotaEPedido;

    @Column(name = "possui_validacao_chave_e_fornecedor", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiValidacaoChaveEFornecedor;

    @Column(name = "utiliza_obter_dados_nfe", nullable = false, columnDefinition = "boolean default false")
    private boolean utilizaObterDadosNfe;

    @Column(name = "utiliza_obter_dados_cte", nullable = false, columnDefinition = "boolean default false")
    private boolean utilizaObterDadosCte;

    @Column(name = "enviar_email_pesagem_manual", nullable = false, columnDefinition = "boolean default false")
    private boolean enviarEmailPesagemManual;

    @Column(name = "gestao_patio", nullable = false, columnDefinition = "boolean default false")
    private boolean gestaoPatio;

    @Column(name = "criar_remessa", nullable = false, columnDefinition = "boolean default false")
    private boolean criarRemessa;

    @Column(name = "criar_documento_transporte", nullable = false, columnDefinition = "boolean default false")
    private boolean criarDocumentoTransporte;

    @Column(name = "possui_validacao_conversao_litragem", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiValidacaoConversaoLitragem;

    @Column(name = "umidade_informada_manualmente", nullable = false, columnDefinition = "boolean default false")
    private boolean umidadeInformadaManualmente;

    @Column(name = "possui_regra_complemento", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiRegraComplemento;

    @Column(name = "count_requisicao")
    private long requestCount;

    @Column(name = "tempo_pre_cadastro")
    private Integer tempoPreCadastro;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sincronizado_em")
    private Date ultimaSincronizacao;

    @Column(name = "tolerancia_entre_vlr_uni_nota_e_pedido", precision = 19, scale = 2)
    private BigDecimal toleranciaEntreVlrUniNotaEPedido;

    @Transient
    private AmbienteSincronizacao ambienteSincronizacao;

    public String getOperacaoDescricao() {

        return this.operacao + " - " + this.descricao;
    }

    public PerfilOperacao toPerfilOperacao() {

        PerfilOperacao entidade = new PerfilOperacao();

        entidade.setOperacao(this);

        entidade.setOperacaoAtribuidaAoPerfil(false);

        return entidade;
    }

}
