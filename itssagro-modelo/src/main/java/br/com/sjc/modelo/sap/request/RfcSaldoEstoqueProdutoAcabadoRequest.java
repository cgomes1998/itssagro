package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SAPParameter(function = "BAPI_MATERIAL_STOCK_REQ_LIST")
public class RfcSaldoEstoqueProdutoAcabadoRequest extends JCoRequest {

    @SAPColumn(name = "MATERIAL")
    private String material;

    @SAPColumn(name = "PLANT")
    private String centro;

}
