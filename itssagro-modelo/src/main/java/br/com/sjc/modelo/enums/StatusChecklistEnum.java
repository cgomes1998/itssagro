package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusChecklistEnum {

    AGUARDANDO("Aguardando"),
    EM_ANALISE("Em Análise"),
    APROVADO("Aprovado"),
    REPROVADO("Reprovado"),
    PENDENTE("Pendente"),
    CANCELAMENTO_ORDEM("Cancelamento de Ordem");

    private String descricao;

}
