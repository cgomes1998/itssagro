package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.ObjectID;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
public abstract class DataDTO implements ObjectID<Long> {

    @ProjectionProperty
    private Long id;

    private StatusEnum status;

    private Date dataCadastro;

    public String getStatusDescricao() {

        return this.status != null ? this.status.getDescricao() : "";
    }

    public String getDataCadastroFormatada() {

        return this.dataCadastro != null ? new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(this.dataCadastro) : "";
    }
}
