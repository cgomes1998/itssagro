package br.com.sjc.modelo.sap.sync;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;

@Getter
@Setter
public class SAPProdutorRural extends SAPEntidade {

    public static String CODIGO_ARMAZEM_TERCEIRO = "Z010";

    public static List<String> CODIGOS_PRODUTORES = Arrays.asList("Z019", "Z020");

    public enum COLUMN_INPUT {

        DT_ULT_INF,

        COD_PRODUTOR,

        PRE_CAD,

        NOME_PROD,

        CNPJ_PROD,

        CPF_PROD,

        IE_PROD,

        NOMEFAZ_PROD,

        RUA_PROD,

        CEP_PROD,

        CIDADE_PROD,

        SETOR_PROD,

        REGIAO_PROD,

        TEL_PROD,

        NRO_PROD,

        ID_PRODUTOR;
    }

    public static SAPProdutorRural newInstance() {

        return new SAPProdutorRural();
    }
}
