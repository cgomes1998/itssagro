package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class RfcOrdemVendaParceiroSaidaResponse extends JCoItem {

    @SAPColumn(name = "PARTN_ROLE")
    private String tipoParceiro;

    @SAPColumn(name = "CUSTOMER")
    private String codigoCliente;

    @SAPColumn(name = "VENDOR_NO")
    private String codigoTransportadora;

}
