package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.EntidadeTipo;
import br.com.sjc.modelo.enums.LogAtividadeAcao;
import br.com.sjc.modelo.enums.OperacaoSenhaEnum;
import br.com.sjc.modelo.enums.StatusSenhaEnum;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_senha", schema = "public")
public class Senha extends EntidadeAutenticada {

    private static final long serialVersionUID = 1L;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dt_carga_pontual_agendamento_inicial")
    private Date cargaPontualDataAgendamentoInicial;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dt_carga_pontual_agendamento_final")
    private Date cargaPontualDataAgendamentoFinal;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dt_chegada_motorista")
    private Date dataChegadaMotorista;

    @Column(name = "carga_pontual_id_agendamento")
    private String cargaPontualIdAgendamento;

    @Column(name = "senha")
    private String senha;

    @Column(name = "codigo_ordem_venda")
    private String codigoOrdemVenda;

    @Column(name = "numero_pedido")
    private String numeroPedido;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_fluxo")
    private TipoFluxoEnum tipoFluxo;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_senha")
    private StatusSenhaEnum statusSenha;

    @Enumerated(EnumType.STRING)
    @Column(name = "operacao")
    private OperacaoSenhaEnum operacao;

    @Enumerated(EnumType.STRING)
    @Column(name = "entidade_tipo")
    private EntidadeTipo entidadeTipo;

    @Column(name = "numero_cartao_acesso")
    private Integer numeroCartaoAcesso;

    @Column(name = "quantidade_carregar")
    private BigDecimal quantidadeCarregar;

    @Column(name = "saldo_ordem_venda")
    private BigDecimal saldoOrdemVenda;

    @EntityProperties(value = {"id", "cpfCnpj", "codigo", "nome"})
    @ManyToOne
    @JoinColumn(name = "id_cliente")
    private Cliente cliente;

    @EntityProperties(value = {"id", "cpfCnpj", "codigo", "nome"})
    @ManyToOne
    @JoinColumn(name = "id_operador_logistico")
    private Cliente operadorLogistico;

    @EntityProperties(value = {"id", "codigo", "descricao", "unidadeMedida", "valorUnitario", "realizarConversaoLitragem", "necessitaTreinamentoMotorista", "lacrarCarga", "realizarAnaliseQualidade", "buscarCertificadoQualidade",
            "necessitaMaisDeUmaAnalise", "possuiEtapaCarregamento", "gerarNumeroCertificado", "possuiResolucaoAnp", "exibirEspecificacaoNaImpressaoDoLaudo", "toleranciaEmKg", "tipoTransporte",})
    @ManyToOne
    @JoinColumn(name = "id_material")
    private Material material;

    @EntityProperties(value = {"id", "status", "nome", "cpf", "cnh", "cep", "rua", "bairro", "numero", "municipio", "telefone", "celular", "uf", "dataValidadeCNH", "dataTreinamento", "dataValidadeTreinamento", "dataInicioBloqueio",
            "dataFimBloqueio", "veiculo.id", "veiculo.placa1", "veiculo.codigo"})
    @ManyToOne
    @JoinColumn(name = "id_motorista")
    private Motorista motorista;

    @EntityProperties(value = {"id", "status", "codigo", "placa1", "placa2", "placa3", "municipio", "renavam", "chassi", "marca", "tipo.id", "tipo.descricao", "tipo.peso", "tipo.pesoTara", "tipo.pesoLiquido", "tipo.tipo", "tipo.textoTipo", "cor",
            "eixos", "ufPlaca1", "ufPlaca2", "ufPlaca3", "dataVencimentoPlaca1", "dataVencimentoPlaca2", "dataVencimentoPlaca3", "dataCivPlaca1", "dataCivPlaca2", "dataCivPlaca3", "dataCalibragemPlaca1", "dataCalibragemPlaca2",
            "dataCalibragemPlaca3", "dataCipp1", "dataCipp2", "dataCipp3", "dataVencimentoCr", "dataVencimentoAatipp", "antt"}, collecions = {"tipo.setas"})
    @ManyToOne
    @JoinColumn(name = "id_veiculo")
    private Veiculo veiculo;

    @EntityProperties(value = {"id", "codigo", "descricao", "cnpj", "cpf"})
    @ManyToOne
    @JoinColumn(name = "id_transportadora")
    private Transportadora transportadora;

    @EntityProperties(value = {"id", "etapa.id", "etapa.fluxo.id", "etapa.fluxo.codigo", "etapa.fluxo.descricao", "etapa.fluxo.tipoFluxo", "etapa.sequencia", "etapa.statusEstornoAutomacao", "etapa.mensagemEstornoAutomacao", "etapa.etapa"
            , "etapa.medirTempo", "etapa.etapaFinal", "etapa.avancoAutomatico", "etapa.", "qtdRepeticao", "qtdChamada", "horaEntrada", "horaSaida", "duracao", "statusEtapa"}, collecions = {"etapa.criterios"})
    @ManyToOne
    @JoinColumn(name = "id_senha_etapa_atual")
    private SenhaEtapa etapaAtual;

    @EntityProperties(value = {"id", "statusChecklist", "senha.id", "formulario.id", "formulario.descricao", "formulario.formularioStatus", "formulario.cabecalhoDadosMotorista", "formulario.cabecalhoDadosVeiculo", "formulario" +
            ".cabecalhoDadosCliente", "formulario.cabecalhoDadosTransportadora"}, collecions = {"respostas", "motivos"})
    @ManyToOne
    @JoinColumn(name = "id_senha_formulario_atual")
    private SenhaFormulario formularioAtual;

    @EntityProperties(value = {"id", "tipoVeiculo.id", "descricao", "qtd", "padrao"})
    @ManyToOne
    @JoinColumn(name = "id_tipo_veiculo_seta")
    private TipoVeiculoSeta seta;

    @EntityProperties(value = {"id", "descricao", "tipo", "conferenciaPeso", "buscarDadosRomaneio"}, targetEntity = Motivo.class)
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "tb_senha_motivo", schema = "public", joinColumns = @JoinColumn(name = "id_senha"), inverseJoinColumns = @JoinColumn(name = "id_motivo"))
    private List<Motivo> motivos = new ArrayList<>();

    @EntityProperties(value = {"id", "etapa.id", "etapa.fluxo.id", "etapa.fluxo.codigo", "etapa.fluxo.descricao", "etapa.fluxo.tipoFluxo", "etapa.sequencia", "etapa.statusEstornoAutomacao", "etapa.mensagemEstornoAutomacao", "etapa.etapa"
            , "etapa.medirTempo", "etapa.etapaFinal", "etapa.avancoAutomatico", "etapa.", "qtdRepeticao", "qtdChamada", "horaEntrada", "horaSaida", "duracao", "statusEtapa"}, targetEntity = SenhaEtapa.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "senha", cascade = CascadeType.ALL)
    private List<SenhaEtapa> etapas = new ArrayList<>();

    @EntityProperties(value = {"id", "statusChecklist", "senha.id", "formulario.id", "formulario.descricao", "formulario.formularioStatus", "formulario.cabecalhoDadosMotorista", "formulario.cabecalhoDadosVeiculo", "formulario" +
            ".cabecalhoDadosCliente", "formulario.cabecalhoDadosTransportadora"}, targetEntity = SenhaFormulario.class)
    @OrderBy("id desc")
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "senha", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    private List<SenhaFormulario> formularios = new ArrayList<>();

    @Transient
    private String botaoAcionadoAguardandoChamadaCarregamento;

    @Transient
    private Usuario usuarioCadastro;

    @Transient
    private List<StatusSenhaEnum> statusSenhaList;

    @Transient
    private LogAtividadeAcao logAtividadeAcao;

    public Senha() {

    }

    public Senha(final Long id) {

        this.setId(id);
    }

    public String getDescricaoFormatada() {

        String descricaoFormatada = this.senha;

        if (ObjetoUtil.isNotNull(material) && StringUtil.isNotNullEmpty(material.getDescricao())) {
            descricaoFormatada += " - " + material.getDescricao();
        }

        if (ObjetoUtil.isNotNull(motorista) && StringUtil.isNotNullEmpty(motorista.getNome())) {
            descricaoFormatada += " - " + motorista.getNome();
        }

        return descricaoFormatada;
    }

}
