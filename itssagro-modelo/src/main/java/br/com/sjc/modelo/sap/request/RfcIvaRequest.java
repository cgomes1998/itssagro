package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;

@SAPParameter(function = "ZITSSAGRO_FM_IVAS")
public class RfcIvaRequest extends JCoRequest {
}
