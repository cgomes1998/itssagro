package br.com.sjc.modelo.sap.request.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SAPParameter( function = "ORDER_VIEW" )
public class RfcOrdemVendaItemRequest extends JCoItem {

    @SAPColumn( name = "HEADER" )
    private String cabecalho;

    @SAPColumn( name = "ITEM" )
    private String item;

    @SAPColumn( name = "SDSCHEDULE" )
    private String agendamento;

    @SAPColumn( name = "BUSINESS" )
    private String negocio;

    @SAPColumn( name = "PARTNER" )
    private String parceiro;

    @SAPColumn( name = "ADDRESS" )
    private String endereco;

    @SAPColumn( name = "STATUS_H" )
    private String statusH;

    @SAPColumn( name = "STATUS_I" )
    private String statusI;

    @SAPColumn( name = "SDCOND" )
    private String condicao;

    @SAPColumn( name = "SDCOND_ADD" )
    private String condicaoAdd;

    @SAPColumn( name = "CONTRACT" )
    private String contrato;

    @SAPColumn( name = "TEXT" )
    private String texto;

    @SAPColumn( name = "FLOW" )
    private String fluxo;

    @SAPColumn( name = "BILLPLAN" )
    private String planoPagamento;

    @SAPColumn( name = "CONFIGURE" )
    private String configuracao;

    @SAPColumn( name = "CREDCARD" )
    private String cartaoCredito;

    @SAPColumn( name = "INCOMP_LOG" )
    private String log;

}
