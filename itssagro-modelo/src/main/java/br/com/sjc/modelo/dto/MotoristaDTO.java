package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MotoristaDTO extends DataDTO {

    @ProjectionProperty
    private String codigo;

    @ProjectionProperty
    private String nome;

    @ProjectionProperty
    private String cpf;

    @ProjectionProperty
    private String cnh;

    @ProjectionProperty
    private String cep;

    @ProjectionProperty
    private String rua;

    @ProjectionProperty
    private String numero;

    @ProjectionProperty
    private String municipio;

    @ProjectionProperty
    private StatusEnum status;

}
