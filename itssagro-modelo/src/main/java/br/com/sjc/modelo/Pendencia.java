package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.StatusPendenciaEnum;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_pendencia", schema = "sap")
public class Pendencia extends EntidadeGenerica {

    @Column(name = "status_pendencia")
    @Enumerated(EnumType.STRING)
    private StatusPendenciaEnum statusPendencia = StatusPendenciaEnum.AGUARDANDO;

    @EntityProperties(value = {"id", "descricao", "tipo", "conferenciaPeso", "buscarDadosRomaneio"}, targetEntity = Motivo.class)
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "tb_pendencia_motivo", schema = "public", joinColumns = @JoinColumn(name = "id_pendencia"), inverseJoinColumns = @JoinColumn(name = "id_motivo"))
    private List<Motivo> motivos = new ArrayList<>();

}
