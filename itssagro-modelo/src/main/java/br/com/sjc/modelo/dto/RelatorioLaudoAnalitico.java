package br.com.sjc.modelo.dto;


import br.com.sjc.modelo.AnaliseItem;
import br.com.sjc.modelo.AnaliseQualidade;
import br.com.sjc.modelo.LoteAnaliseQualidade;
import br.com.sjc.modelo.Pesagem;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.util.BigDecimalUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class RelatorioLaudoAnalitico {

    private Romaneio romaneio;

    private ItemNFPedido itemNFPedido;

    private RetornoNFe retornoNFe;

    private AnaliseQualidade analiseQualidade;

    private AnaliseQualidadeItem analiseQualidadeItem;

    public RelatorioLaudoAnalitico(
            Long idRomaneio,
            String ordemVenda,
            Long idMaterial,
            String codigoMaterial,
            String material,
            boolean buscarCertificadoQualidade,
            boolean necessitaMaisDeUmaAnalise,
            String codigoCliente,
            String cliente,
            String numeroRomaneio,
            String placaVeiculo,
            BigDecimal pesoTara,
            BigDecimal pesoBruto,
            String numeroNfe,
            String unidadeMedida,
            Date dataNfe,
            Long idAnalise,
            String lote,
            String lacreAmostra,
            Long idLaudo,
            String caracteristica,
            String especificacao,
            String unidade,
            String subCaracteristica,
            String resultado
    ) {

        this.setAnaliseQualidadeItem(new AnaliseQualidadeItem());

        this.analiseQualidadeItem.setResultado(resultado);

        this.analiseQualidadeItem.setAnaliseItem(new AnaliseItem());

        this.analiseQualidadeItem.getAnaliseItem().setCaracteristica(caracteristica);

        this.analiseQualidadeItem.getAnaliseItem().setEspecificacao(especificacao);

        this.analiseQualidadeItem.getAnaliseItem().setUnidade(unidade);

        this.analiseQualidadeItem.getAnaliseItem().setSubCaracteristica(subCaracteristica);

        this.retornoNFe = new RetornoNFe();

        this.retornoNFe.setNfe(numeroNfe);

        this.retornoNFe.setDataCadastro(dataNfe);

        this.analiseQualidade = new AnaliseQualidade();

        this.analiseQualidade.setId(idAnalise);

        this.analiseQualidade.setLoteAnaliseQualidade(LoteAnaliseQualidade.builder().codigo(lote).build());

        this.analiseQualidade.setLacreAmostra(lacreAmostra);

        this.analiseQualidade.setIdLaudo(idLaudo);

        this.romaneio = new Romaneio();

        this.romaneio.setId(idRomaneio);

        this.romaneio.setNumeroRomaneio(numeroRomaneio);

        this.romaneio.setPesagem(new Pesagem());

        this.romaneio.getPesagem().setPesoInicial(BigDecimalUtil.isMaiorQueZero(pesoTara) ? pesoTara : BigDecimal.ZERO);

        this.romaneio.getPesagem().setPesoFinal(BigDecimalUtil.isMaiorQueZero(pesoBruto) ? pesoBruto : BigDecimal.ZERO);

        this.itemNFPedido = new ItemNFPedido();

        this.itemNFPedido.setCodigoOrdemVenda(ordemVenda);

        this.itemNFPedido.setUnidadeMedidaOrdem(unidadeMedida);

        this.itemNFPedido.setCliente(new Cliente());

        this.itemNFPedido.getCliente().setCodigo(codigoCliente);

        this.itemNFPedido.getCliente().setNome(cliente);

        this.itemNFPedido.setPlacaCavalo(new Veiculo());

        this.itemNFPedido.getPlacaCavalo().setPlaca1(placaVeiculo);

        this.itemNFPedido.setMaterial(new Material());

        this.itemNFPedido.getMaterial().setId(idMaterial);

        this.itemNFPedido.getMaterial().setCodigo(codigoMaterial);

        this.itemNFPedido.getMaterial().setDescricao(material);

        this.itemNFPedido.getMaterial().setBuscarCertificadoQualidade(buscarCertificadoQualidade);

        this.itemNFPedido.getMaterial().setNecessitaMaisDeUmaAnalise(necessitaMaisDeUmaAnalise);
    }
}
