package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.EtapaEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProximoFluxoEtapaDTO {

    private Long id;

    private Long sequencia;

    private EtapaEnum etapa;

    public ProximoFluxoEtapaDTO(Long id, Long sequencia, EtapaEnum etapa) {
        this.id = id;
        this.sequencia = sequencia;
        this.etapa = etapa;
    }
}
