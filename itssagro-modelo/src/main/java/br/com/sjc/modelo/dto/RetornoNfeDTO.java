package br.com.sjc.modelo.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RetornoNfeDTO {

    private String nfe;

    private String requestKey;
    
    private Date dataCriacaoNotaMastersaf;

}
