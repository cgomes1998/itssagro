package br.com.sjc.modelo.sap.request.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SAPItem
public class RfcCriarRemessaItemRequest extends JCoItem {

    @SAPColumn( name = "REF_DOC" )
    private String codigoOrdemVenda;

    @SAPColumn( name = "REF_ITEM" )
    private String itemOrdemVenda;

    @SAPColumn( name = "SALES_UNIT" )
    private String unidadeVenda;

    @SAPColumn( name = "LGORT" )
    private String codigoDeposito;

    @SAPColumn( name = "DLV_QTY" )
    private BigDecimal quantidadeRemessa;
}
