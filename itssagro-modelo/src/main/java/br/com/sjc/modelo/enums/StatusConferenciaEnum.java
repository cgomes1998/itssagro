package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusConferenciaEnum {

    AGUARDANDO("Aguardando"),
    EM_ANALISE("Em Análise"),
    CANCELADO("Cancelado"),
    LIBERADO("Liberado"),
    PENDENTE("Pendente");

    private String descricao;
}
