package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@SAPItem
public class RfcStatusNfeItem extends JCoItem implements Comparable<RfcStatusNfeItem> {

    @SAPColumn(name = "ROMANEIO")
    private String romaneio;

    @SAPColumn(name = "NFNUM9")
    private String nfe;

    @SAPColumn(name = "SERIE")
    private String serieNfe;

    @SAPColumn(name = "CODE")
    private String status;

    @SAPColumn(name = "AUTHDATE")
    private Date dataCriacao;

    @SAPColumn(name = "AUTHTIME")
    private Date horaCriacao;

    @SAPColumn(name = "CHAVE")
    private String chave;

    @Override
    public int compareTo(RfcStatusNfeItem o) {
        if (StringUtil.isEmpty(o.getRomaneio()) || StringUtil.isEmpty(getRomaneio())) {
            return 0;
        }
        return new Long(getRomaneio()).compareTo(new Long(o.getRomaneio()));
    }
}
