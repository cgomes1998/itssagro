package br.com.sjc.modelo.sap.sync;

import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;

import java.lang.reflect.Field;

/**
 * <p>
 * <b>Title:</b> EntidadeSAP
 * </p>
 *
 * <p>
 * <b>Description:</b> Entidade responsável por prover funcionalidades comuns às entidades de sincronização.
 * </p>
 *
 * <p>
 * <b>Company: </b> ITSS Soluções em Tecnologia
 * </p>
 *
 * @author Bruno Zafalão
 * @version 1.0.0
 */
public class SAPEntidade {

    /**
     * Método responsável por preencher os valores de uma entidade.
     *
     * @param field
     * @param value
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @author Bruno Zafalão
     */
    public void update(final String field, final String value) throws IllegalArgumentException, IllegalAccessException {

        final Field[] fields = this.getClass().getDeclaredFields();

        for (int i = 0; i < fields.length; i++) {

            if (fields[i].isAnnotationPresent(SAPColumn.class)) {

                final SAPColumn column = fields[i].getAnnotation(SAPColumn.class);

                if (column != null && column.name().equals(field)) {

                    fields[i].setAccessible(true);

                    fields[i].set(this, value);
                }
            }
        }
    }
}
