package br.com.sjc.modelo.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class LogRomaneioDTO extends DataDTO {

    private String login;

    private String nomeUsuario;

    private String motivo;

    private Date dataLiberacao;

    private Long idLogRomaneioAcoes;

    private List<String> restricoes;

    public LogRomaneioDTO(String login, String nomeUsuario, String motivo, Date dataLiberacao, Long idLogRomaneioAcoes) {
        this.login = login;
        this.nomeUsuario = nomeUsuario;
        this.motivo = motivo;
        this.dataLiberacao = dataLiberacao;
        this.idLogRomaneioAcoes = idLogRomaneioAcoes;
    }
}
