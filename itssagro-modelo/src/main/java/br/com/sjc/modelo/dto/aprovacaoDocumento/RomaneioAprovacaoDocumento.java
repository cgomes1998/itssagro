package br.com.sjc.modelo.dto.aprovacaoDocumento;

import br.com.sjc.modelo.Classificacao;
import br.com.sjc.modelo.Usuario;
import br.com.sjc.modelo.sap.Motorista;
import br.com.sjc.modelo.sap.Produtor;
import br.com.sjc.modelo.sap.TabelaClassificacao;
import br.com.sjc.modelo.sap.Veiculo;
import br.com.sjc.util.anotation.ProjectionConfigurationDTO;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ProjectionConfigurationDTO
public class RomaneioAprovacaoDocumento {

    @ProjectionProperty
    private Long id;

    @ProjectionProperty
    private String numeroRomaneio;

    @ProjectionProperty(values = {"id", "codigo", "placa1"})
    private Veiculo placaCavalo;

    @ProjectionProperty(values = {"id", "codigo", "nome", "cpf"})
    private Motorista motorista;

    @ProjectionProperty(values = {"id", "nome"})
    private Produtor produtor;

    @ProjectionProperty(values = {"id", "dataCadastro"})
    private Classificacao classificacao;

    private Usuario usuarioAprovador;

    private List<ItemNFPedidoAprovacaoDocumento> itemNFPedidoAprovacaoDocumentos = new ArrayList<>();

    private List<TabelaClassificacaoAprovacaoDocumento> limitesIndices = new ArrayList<>();

}
