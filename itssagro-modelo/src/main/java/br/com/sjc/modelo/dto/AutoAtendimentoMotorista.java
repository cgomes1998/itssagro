package br.com.sjc.modelo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AutoAtendimentoMotorista {

	private String numeroRomaneio;

	private String numeroCartaoDeAcesso;

}
