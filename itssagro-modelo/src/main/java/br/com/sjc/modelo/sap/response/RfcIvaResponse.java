package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.response.item.RfcIvaResponseItem;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@SAPParameter(function = "ZITSSAGRO_FM_IVAS")
public class RfcIvaResponse extends JCoResponse {

    @SAPTable(name = "T_IVAS", item = RfcIvaResponseItem.class)
    private List<RfcIvaResponseItem> ivas;

}
