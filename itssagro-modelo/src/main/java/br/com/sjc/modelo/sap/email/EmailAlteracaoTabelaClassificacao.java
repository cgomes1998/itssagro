package br.com.sjc.modelo.sap.email;

import br.com.sjc.modelo.ConfiguracaoEmail;
import br.com.sjc.modelo.sap.TabelaClassificacao;
import br.com.sjc.util.email.Email;
import br.com.sjc.util.email.EmailBodyBuilder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by julio.bueno on 24/07/2019.
 */
@RequiredArgsConstructor
public class EmailAlteracaoTabelaClassificacao implements Email {

    @NonNull
    private TabelaClassificacao tabelaClassificacaoNova;

    @NonNull
    private ConfiguracaoEmail configuracaoEmail;

    @NonNull
    private String percentualAnterior;

    @Override
    public String getAssunto() {
        return "Alteração na Tabela de Classificação";
    }

    @Override
    public String getTextoEmail() {
        return EmailBodyBuilder
                .build()
                .addTextStrong("O percentual de desconto do item ${item} foi alterado.")
                .addLineBreak()
                .addText("Percentual anterior: ${percentualAnterior} %")
                .addLineBreak()
                .addText("Percentual alterado: ${percentualAlterado} %")
                .toString();
    }

    @Override
    public List<String> getDestinatario() {
        return configuracaoEmail.getEmailsTabelaClassificacao();
    }

    @Override
    public Map<String, String> getParametros() {
        Map<String, String> parametros = new HashMap<>();
        parametros.put("${item}", tabelaClassificacaoNova.getItemClassificacao() + " - " + tabelaClassificacaoNova.getDescricaoItemClassificacao());
        parametros.put("${percentualAnterior}", percentualAnterior);
        parametros.put("${percentualAlterado}", tabelaClassificacaoNova.getPercentualDesconto());
        return parametros;
    }
}
