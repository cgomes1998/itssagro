package br.com.sjc.modelo.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class ObterRomaneioEstornadoResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private RomaneioEstornadoResponse entidade;
    private boolean habilitarLacragem;
    private boolean habilitarConversao;
    private boolean habilitarPesagem;
    private boolean habilitarClassificacao;
}
