package br.com.sjc.modelo;

import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_tabela_valor_item")
public class TabelaValorItem extends EntidadeAutenticada {

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tabela_valor", nullable = false)
    @EntityProperties(value = {"id"})
    private TabelaValor tabelaValor;

    @Column(name = "temperatura", precision = 19, scale = 1)
    private BigDecimal temperatura;

    @Column(name = "massa_especifica", precision = 19, scale = 4)
    private BigDecimal massaEspecifica;

    @Column(name = "massa_especifica_vinte_graus", precision = 19, scale = 4)
    private BigDecimal massaEspecificaVinteGraus;

    @Column(name = "grau_alcoolico", precision = 19, scale = 4)
    private BigDecimal grauAlcoolico;

    @Column(name = "grau_alcoolico_vinte_graus", precision = 19, scale = 4)
    private BigDecimal grauAlcoolicoVinteGraus;

    @Column(name = "fator_reducao", precision = 19, scale = 4)
    private BigDecimal fatorReducao;

}
