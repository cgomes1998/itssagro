package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class RfcReturnItem extends JCoItem {

    @SAPColumn(name = "TYPE")
    private String tipoMensagem;

    @SAPColumn(name = "MESSAGE")
    private String mensagem;

    @SAPColumn(name = "MESSAGE_V1")
    private String mensagemUm;

    @SAPColumn(name = "MESSAGE_V2")
    private String mensagemDois;

    @SAPColumn(name = "MESSAGE_V3")
    private String mensagemTres;

    @SAPColumn(name = "MESSAGE_V4")
    private String mensagemQuatro;

}
