package br.com.sjc.modelo.endpoint.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
public class MensagemResponse {

    private String tipo;

    private String texto;

    public MensagemResponse(final String tipo, final String mensagem) {

        this.tipo = tipo;

        this.texto = mensagem;
    }
}
