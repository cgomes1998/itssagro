package br.com.sjc.modelo;

import br.com.sjc.util.email.ConfiguracaoEmailProtocolo;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by julio.bueno on 23/07/2019.
 */
@Setter
@Getter
@Entity
@Table(name = "tb_configuracao_email")
public class ConfiguracaoEmail extends EntidadeAutenticada {

    @Column(name = "server")
    private String server;

    @Column(name = "porta")
    private String porta;

    @Column(name = "usuario")
    private String usuario;

    @Column(name = "senha")
    private String senha;

    @Enumerated(EnumType.STRING)
    @Column(name = "protocolo")
    private ConfiguracaoEmailProtocolo protocolo;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
    @CollectionTable(name = "tb_configuracao_email_emails_tabela_classificacao", joinColumns = @JoinColumn(name = "id_configuracao_email"))
    private List<String> emailsTabelaClassificacao = new ArrayList<>();

    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
    @CollectionTable(name = "tb_configuracao_email_emails_classificacao", joinColumns = @JoinColumn(name = "id_configuracao_email"))
    private List<String> emailsClassificacao = new ArrayList<>();

    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
    @CollectionTable(name = "tb_configuracao_email_emails_pesagem", joinColumns = @JoinColumn(name = "id_configuracao_email"))
    private List<String> emailsPesagem = new ArrayList<>();

}
