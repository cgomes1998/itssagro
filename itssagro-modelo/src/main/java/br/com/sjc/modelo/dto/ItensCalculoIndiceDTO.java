package br.com.sjc.modelo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ItensCalculoIndiceDTO {

    private BigInteger idTabelaClassificacao;

    private String descricao;
}
