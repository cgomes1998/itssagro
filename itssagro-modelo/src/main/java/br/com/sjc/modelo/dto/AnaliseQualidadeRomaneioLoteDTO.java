package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.LoteAnaliseQualidade;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AnaliseQualidadeRomaneioLoteDTO {

    @ProjectionProperty(values = {"id", "codigo"})
    private LoteAnaliseQualidade loteAnaliseQualidade;

    @ProjectionProperty(values = {"id", "numeroRomaneio"})
    private Romaneio romaneio;

}
