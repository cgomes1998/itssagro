package br.com.sjc.modelo.sap.request.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class RfcStatusPedidoRequestItem extends JCoItem {

    @SAPColumn(name = "EBELN")
    private String pedido;

    public RfcStatusPedidoRequestItem(final String pedido) {

        this.setPedido(pedido);
    }
}
