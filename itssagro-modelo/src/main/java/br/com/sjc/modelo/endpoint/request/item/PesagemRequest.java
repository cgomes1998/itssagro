package br.com.sjc.modelo.endpoint.request.item;

import br.com.sjc.modelo.Pesagem;
import br.com.sjc.modelo.Usuario;
import br.com.sjc.modelo.sap.Safra;
import br.com.sjc.modelo.sap.Veiculo;
import br.com.sjc.util.ObjetoUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PesagemRequest {

	private double pesoBruto;

	private double pesoLiquidoUmido;

	private double descontos;

	private double pesoLiquidoSeco;

	public Pesagem toEntidade(final Veiculo placaCavalo, final Safra safra, final Usuario balanceiro) {

		Pesagem entidade = new Pesagem();

		entidade.setPlacaCavalo(placaCavalo);

		entidade.setSafra(safra);

		entidade.setUsuarioPesoInicial(balanceiro);

		entidade.setUsuarioPesoFinal(balanceiro);

		final BigDecimal pesoUmido = ObjetoUtil.isNotNull(this.pesoLiquidoUmido) ? BigDecimal.valueOf(this.pesoLiquidoUmido) : BigDecimal.ZERO;

		entidade.setPesoInicial(ObjetoUtil.isNotNull(this.pesoBruto) ? BigDecimal.valueOf(this.pesoBruto) : BigDecimal.ZERO);

		entidade.setPesoFinal(entidade.getPesoInicial().subtract(pesoUmido).setScale(
			   0,
			   RoundingMode.HALF_EVEN
		));

		return entidade;
	}

}