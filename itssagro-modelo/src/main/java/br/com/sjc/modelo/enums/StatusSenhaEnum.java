package br.com.sjc.modelo.enums;

import lombok.Getter;

@Getter
public enum StatusSenhaEnum {

    AGUARDANDO_CHAMADA("Aguardando Chamada"),
    CHAMADA("Chamada"),
    COMPLETA("Completa"),
    PENDENTE("Pendente"),
    CANCELADA("Cancelada"),
    AGUARDANDO_ENTRADA("Aguardando Chegada na Planta");

    private String descricao;

    StatusSenhaEnum(String descricao) {

        this.descricao = descricao;
    }
}
