package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.enums.MotivoRecusaEnum;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@SAPItem
public class RfcOrdensVendaItemResponse extends JCoItem {

    @SAPColumn(name = "SD_DOC")
    private String ordemVenda;

    @SAPColumn(name = "PLANT")
    private String centro;

    @SAPColumn(name = "SOLD_TO")
    private String codigoCliente;

    @SAPColumn(name = "NAME")
    private String descricaoCliente;

    @SAPColumn(name = "MATERIAL")
    private String codigoMaterial;

    @SAPColumn(name = "SHORT_TEXT")
    private String descricaoMaterial;

    @SAPColumn(name = "PURCH_NO")
    private String numeroPedido;

    @SAPColumn(name = "REASON_REJ")
    private String motivoRecusa;

    @SAPColumn(name = "DOC_STATUS")
    private String descricaoStatusOrdem;

    @SAPColumn(name = "DOC_TYPE")
    private String tipoOrdem;

    @SAPColumn(name = "NET_VAL_HD")
    private BigDecimal preco;

    @SAPColumn(name = "REQ_QTY")
    private BigDecimal qtdInicial;

    @SAPColumn(name = "DLV_QTY")
    private BigDecimal qtdEmb;

    @SAPColumn(name = "CREATION_DATE")
    private Date dataOrdem;

    public BigDecimal getSaldo() {

        return this.getQtdInicial().subtract(this.getQtdEmb());
    }

    public String getMotivoRecusaDescricao() {
        return MotivoRecusaEnum.obterDescricaoPorCodigo(this.getMotivoRecusa());
    }
}
