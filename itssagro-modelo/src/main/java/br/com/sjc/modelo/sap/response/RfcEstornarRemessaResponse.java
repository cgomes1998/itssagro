package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.response.item.RfcReturnItem;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@SAPParameter(function = "ZITSSAGRO_OUTB_DELIVERY_CHANGE")
public class RfcEstornarRemessaResponse extends JCoResponse {

    @SAPTable(name = "RETURN", item = RfcReturnItem.class)
    private List<RfcReturnItem> tReturn;
}
