package br.com.sjc.modelo.cfg;

import br.com.sjc.modelo.EntidadeGenerica;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Inheritance( strategy = InheritanceType.JOINED )
@Table( name = "tb_campo", schema = "cfg" )
public class Campo extends EntidadeGenerica {

    @Column( name = "descricao_campo" )
    private String descricao;
}
