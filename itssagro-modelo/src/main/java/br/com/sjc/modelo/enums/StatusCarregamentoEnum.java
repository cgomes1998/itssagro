package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusCarregamentoEnum {

    AGUARDANDO("Aguardando"),
    EM_CARREGAMENTO("Em Carregamento"),
    CARREGADO("Carregado"),
    CANCELADO("Cancelado"),
    PENDENTE("Pendente");

    private String descricao;

}
