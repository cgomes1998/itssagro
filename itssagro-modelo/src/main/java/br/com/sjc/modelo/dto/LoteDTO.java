package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.Lote;
import br.com.sjc.modelo.enums.StatusLacreEnum;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class LoteDTO extends DataDTO {

    @ProjectionProperty
    private String prefixo;

    @ProjectionProperty
    private Integer identInicial;

    @ProjectionProperty
    private Integer identFinal;

    @ProjectionProperty
    private Integer quantidade;

    private Boolean todosLacresDisponiveis = false;

    public LoteDTO(Long id) {

        setId(id);
    }

}
