package br.com.sjc.modelo;

import br.com.sjc.modelo.sap.Material;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_analise", schema = "public")
public class Analise extends EntidadeAutenticada {

    @Column(name = "codigo")
    private String codigo;

    @EntityProperties(value = {"id", "codigo", "descricao"})
    @OneToOne
    @JoinColumn(name = "id_material", referencedColumnName = "id")
    private Material material;

    @EntityProperties(value = {"id", "status", "analise.id", "ordem",
            "grausInpm", "caracteristica", "metodologia",
            "tipoValidacao", "especificacao", "unidade", "subCaracteristica",
            "condicao", "valorInicial", "valorFinal", "laboratorio", "boletim",
            "mostrarEspecificacao"},
            targetEntity = AnaliseItem.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "analise", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<AnaliseItem> itens = new ArrayList<>();

    @Transient
    private Long idLaudo;

    @Transient
    private String lacreAmostra;

    @Transient
    private Documento assinatura;

    public List<AnaliseItem> getItens() {

        return ColecaoUtil.isNotEmpty(itens) ? itens.stream().sorted(Comparator.comparing(AnaliseItem::getOrdem, Comparator.nullsLast(Comparator.naturalOrder()))).collect(Collectors.toList()) : new ArrayList<>();
    }
}
