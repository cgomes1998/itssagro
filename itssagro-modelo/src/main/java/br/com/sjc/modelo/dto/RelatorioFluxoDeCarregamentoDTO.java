package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.enums.UFEnum;
import br.com.sjc.modelo.sap.Cliente;
import br.com.sjc.util.BigDecimalUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class RelatorioFluxoDeCarregamentoDTO implements Comparable<RelatorioFluxoDeCarregamentoDTO> {

    private String statusCarregamento;

    private String statusRomaneio;

    private String statusSAP;

    private String codigoOrdemVenda;

    private String numeroRomaneio;

    private String material;

    private String cliente;

    private String transportadora;

    private String transportadoraSubcontratada;

    private String placa;

    private String motorista;

    private String dtEmissaoSenha;

    private String horaEmissaoSenha;

    private Date dtPrimeiraPesagem;

    private Date dtSegundaPesagem;

    private String nota;

    private BigDecimal qtdKgL;

    private Boolean realizarConversaoLitragem;

    private Long idSenha;

    private Long idPesagem;

    private Date dataSenha;

    private Date dataRomaneio;

    private Date dataAnaliseQualidade;

    private Date dataEmissaoNfe;

    private Date cargaPontualDataAgendamentoInicial;

    private Date cargaPontualDataAgendamentoFinal;

    private Cliente operadorLogistico;

    private boolean romaneioTemRateio = false;

    public RelatorioFluxoDeCarregamentoDTO() {
    }

    public RelatorioFluxoDeCarregamentoDTO(
            String numeroOrdem,
            String codigoMaterial,
            String descricaoMaterial,
            String codigoCliente,
            String nomeCliente,
            String codigoOperadorLogistico,
            String nomeOperadorLogistico,
            UFEnum ufOperadorLogistico,
            String municipioOperadorLogistico,
            String codigoTransportadora,
            String nomeTransportadora,
            String placaVeiculo,
            String codigoMotorista,
            String nomeMotorista,
            Date dataSenha,
            Date dataChegadaMotorista,
            Date cargaPontualDataAgendamentoInicial,
            Date cargaPontualDataAgendamentoFinal
    ) {

        this.setCodigoOrdemVenda(StringUtil.isNotNullEmpty(numeroOrdem) ? numeroOrdem : StringUtil.empty());

        this.setStatusCarregamento("No pátio");

        if (StringUtil.isNotNullEmpty(codigoOperadorLogistico) || StringUtil.isNotNullEmpty(nomeOperadorLogistico)
                || ObjetoUtil.isNotNull(ufOperadorLogistico) || StringUtil.isNotNullEmpty(municipioOperadorLogistico)) {
            this.setOperadorLogistico(new Cliente());
            this.getOperadorLogistico().setCodigo(codigoOperadorLogistico);
            this.getOperadorLogistico().setNome(nomeOperadorLogistico);
            this.getOperadorLogistico().setUf(ufOperadorLogistico);
            this.getOperadorLogistico().setMunicipio(municipioOperadorLogistico);
        }

        if (ObjetoUtil.isNotNull(codigoMaterial, descricaoMaterial)) {

            this.setMaterial(String.format("%s - %s", codigoMaterial, descricaoMaterial));

            this.setRealizarConversaoLitragem(Boolean.FALSE);
        }

        if (ObjetoUtil.isNotNull(codigoCliente, nomeCliente)) {
            this.setCliente(String.format("%s - %s", codigoCliente, nomeCliente));
        }

        if (ObjetoUtil.isNotNull(codigoTransportadora, nomeTransportadora)) {
            this.setTransportadora(String.format("%s - %s", codigoTransportadora, nomeTransportadora));
        }

        if (StringUtil.isNotNullEmpty(placaVeiculo)) {
            this.setPlaca(StringUtil.toPlaca(placaVeiculo));
        }

        if (ObjetoUtil.isNotNull(codigoMotorista, nomeMotorista)) {
            this.setMotorista(String.format("%s - %s", codigoMotorista, nomeMotorista));
        }

        if (ObjetoUtil.isNotNull(dataChegadaMotorista)) {

            this.setDataSenha(dataChegadaMotorista);

            this.setDtEmissaoSenha(DateUtil.format("dd/MM/yyyy", dataChegadaMotorista));

            this.setHoraEmissaoSenha(DateUtil.format("HH:mm:ss", dataChegadaMotorista));

        } else if (ObjetoUtil.isNotNull(dataSenha)) {

            this.setDataSenha(dataSenha);

            this.setDtEmissaoSenha(DateUtil.format("dd/MM/yyyy", dataSenha));

            this.setHoraEmissaoSenha(DateUtil.format("HH:mm:ss", dataSenha));
        }

        this.setCargaPontualDataAgendamentoInicial(cargaPontualDataAgendamentoInicial);

        this.setCargaPontualDataAgendamentoFinal(cargaPontualDataAgendamentoFinal);
    }

    public RelatorioFluxoDeCarregamentoDTO(
            StatusRomaneioEnum statusRomaneio,
            StatusIntegracaoSAPEnum statusIntegracaoSAP,
            String numeroOrdem,
            String numeroRomaneio,
            Boolean realizarConversaoLitragem,
            String codigoMaterial,
            String descricaoMaterial,
            String codigoCliente,
            String nomeCliente,
            String codigoTransportadora,
            String nomeTransportadora,
            String codigoSubTransportadora,
            String nomeSubTransportadora,
            String placaVeiculo,
            String codigoMotorista,
            String nomeMotorista,
            String codigoOperadorLogistico,
            String nomeOperadorLogistico,
            UFEnum ufOperadorLogistico,
            String municipioOperadorLogistico,
            Date dataSenha,
            Date dataChegadaMotorista,
            Date cargaPontualDataAgendamentoInicial,
            Date cargaPontualDataAgendamentoFinal,
            Date dataRomaneio,
            Date dataAnaliseQualidade,
            String numeroNota,
            Date dataCriacaoNotaMastersaf,
            Long idSenha,
            Long idPesagem,
            BigDecimal pesoInicial,
            BigDecimal pesoFinal,
            BigDecimal rateio
    ) {

        this.setDataEmissaoNfe(dataCriacaoNotaMastersaf);

        if (StringUtil.isNotNullEmpty(codigoOperadorLogistico) || StringUtil.isNotNullEmpty(nomeOperadorLogistico)
                || ObjetoUtil.isNotNull(ufOperadorLogistico) || StringUtil.isNotNullEmpty(municipioOperadorLogistico)) {
            this.setOperadorLogistico(new Cliente());
            this.getOperadorLogistico().setCodigo(codigoOperadorLogistico);
            this.getOperadorLogistico().setNome(nomeOperadorLogistico);
            this.getOperadorLogistico().setUf(ufOperadorLogistico);
            this.getOperadorLogistico().setMunicipio(municipioOperadorLogistico);
        }

        if (BigDecimalUtil.isMaiorQueZero(pesoInicial) && BigDecimalUtil.isMaiorQueZero(pesoFinal) && ObjetoUtil.isNull(numeroNota)) {

            this.setStatusCarregamento("Carregado");

        } else if (BigDecimalUtil.isMaiorQueZero(pesoInicial) && BigDecimalUtil.isMaiorQueZero(pesoFinal) && ObjetoUtil.isNotNull(numeroNota)) {

            this.setStatusCarregamento("Nota emitida");

        } else if ((BigDecimalUtil.isMaiorQueZero(pesoFinal) && !BigDecimalUtil.isMaiorQueZero(pesoInicial))
                || BigDecimalUtil.isMaiorQueZero(pesoInicial) && !BigDecimalUtil.isMaiorQueZero(pesoFinal)) {

            this.setStatusCarregamento("Em carregamento");

        } else if (!BigDecimalUtil.isMaiorQueZero(pesoFinal) && !BigDecimalUtil.isMaiorQueZero(pesoInicial)) {

            this.setStatusCarregamento("No pátio");
        }

        if (ObjetoUtil.isNotNull(statusRomaneio)) {

            this.setStatusRomaneio(statusRomaneio.getDescricao());
        }

        if (ObjetoUtil.isNotNull(statusIntegracaoSAP)) {

            this.setStatusSAP(statusIntegracaoSAP.getDescricao());
        }

        this.setCodigoOrdemVenda(StringUtil.isNotNullEmpty(numeroOrdem) ? numeroOrdem : StringUtil.empty());

        this.setNumeroRomaneio(numeroRomaneio);

        if (ObjetoUtil.isNotNull(codigoMaterial, descricaoMaterial)) {

            this.setMaterial(String.format("%s - %s", codigoMaterial, descricaoMaterial));

            this.setRealizarConversaoLitragem(realizarConversaoLitragem);
        }

        if (ObjetoUtil.isNotNull(codigoCliente, nomeCliente)) {
            this.setCliente(String.format("%s - %s", codigoCliente, nomeCliente));
        }

        if (ObjetoUtil.isNotNull(codigoTransportadora, nomeTransportadora)) {
            this.setTransportadora(String.format("%s - %s", codigoTransportadora, nomeTransportadora));
        }

        if (ObjetoUtil.isNotNull(codigoSubTransportadora, nomeSubTransportadora)) {
            this.setTransportadoraSubcontratada(String.format("%s - %s", codigoSubTransportadora, nomeSubTransportadora));
        }

        if (StringUtil.isNotNullEmpty(placaVeiculo)) {
            this.setPlaca(StringUtil.toPlaca(placaVeiculo));
        }

        if (ObjetoUtil.isNotNull(codigoMotorista, nomeMotorista)) {
            this.setMotorista(String.format("%s - %s", codigoMotorista, nomeMotorista));
        }

        if (ObjetoUtil.isNotNull(dataChegadaMotorista)) {

            this.setDataSenha(dataChegadaMotorista);

            this.setDtEmissaoSenha(DateUtil.format("dd/MM/yyyy", dataChegadaMotorista));

            this.setHoraEmissaoSenha(DateUtil.format("HH:mm:ss", dataChegadaMotorista));

        } else if (ObjetoUtil.isNotNull(dataSenha)) {

            this.setDataSenha(dataSenha);

            this.setDtEmissaoSenha(DateUtil.format("dd/MM/yyyy", dataSenha));

            this.setHoraEmissaoSenha(DateUtil.format("HH:mm:ss", dataSenha));
        }

        this.setCargaPontualDataAgendamentoInicial(cargaPontualDataAgendamentoInicial);

        this.setCargaPontualDataAgendamentoFinal(cargaPontualDataAgendamentoFinal);

        this.setDataRomaneio(dataRomaneio);

        this.setDataAnaliseQualidade(dataAnaliseQualidade);

        if (BigDecimalUtil.isMaiorQueZero(rateio)) {

            this.setRomaneioTemRateio(true);

            this.setQtdKgL(rateio);

        } else if (!realizarConversaoLitragem && BigDecimalUtil.isMaiorQueZero(pesoInicial) && BigDecimalUtil.isMaiorQueZero(pesoFinal)) {

            this.setQtdKgL(pesoInicial.subtract(pesoFinal));
        }

        this.setIdPesagem(idPesagem);

        this.setNota(numeroNota);

        this.setIdSenha(idSenha);
    }

    public String getTempoGeracaoRomaneio() {
        String tempo = "-";
        if (ObjetoUtil.isNotNull(this.getDataSenha()) && ObjetoUtil.isNotNull(this.getDataRomaneio())) {
            tempo = DateUtil.horasEntreDatasNoFormato_hhmmss(this.getDataSenha(), this.getDataRomaneio());
        }
        return tempo;
    }

    public String getTempoPatioExterno() {
        String tempo = "-";
        if (ObjetoUtil.isNotNull(this.getDataSenha()) && ObjetoUtil.isNotNull(this.getDtPrimeiraPesagem())) {
            tempo = DateUtil.horasEntreDatasNoFormato_hhmmss(this.getDataSenha(), this.getDtPrimeiraPesagem());
        }
        return tempo;
    }

    public String getTempoEmCarregamento() {
        String tempo = "-";
        if (ObjetoUtil.isNotNull(this.getDtPrimeiraPesagem()) && ObjetoUtil.isNotNull(this.getDtSegundaPesagem())) {
            tempo = DateUtil.horasEntreDatasNoFormato_hhmmss(this.getDtPrimeiraPesagem(), this.getDtSegundaPesagem());
        }
        return tempo;
    }

    public String getTempoAnaliseQualidade() {
        String tempo = "-";
        if (ObjetoUtil.isNotNull(this.getDtSegundaPesagem()) && ObjetoUtil.isNotNull(this.getDataAnaliseQualidade())) {
            if (DateUtil.maiorQue(this.getDtSegundaPesagem(), this.getDataAnaliseQualidade())) {
                tempo = DateUtil.horasEntreDatasNoFormato_hhmmss(this.getDataAnaliseQualidade(), this.getDataAnaliseQualidade());
            } else {
                tempo = DateUtil.horasEntreDatasNoFormato_hhmmss(this.getDtSegundaPesagem(), this.getDataAnaliseQualidade());
            }
        }
        return tempo;
    }

    public String getTempoEmissaoNfe() {
        String tempo = "-";
        if (ObjetoUtil.isNotNull(this.getDataAnaliseQualidade()) && ObjetoUtil.isNotNull(this.getDataEmissaoNfe())) {
            tempo = DateUtil.horasEntreDatasNoFormato_hhmmss(this.getDataAnaliseQualidade(), this.getDataEmissaoNfe());
        }
        return tempo;
    }

    public String getTempoTotalNaEmpresa() {
        String tempo = "-";
        if (ObjetoUtil.isNotNull(this.getDataSenha()) && ObjetoUtil.isNotNull(this.getDataEmissaoNfe())) {
            tempo = DateUtil.horasEntreDatasNoFormato_hhmmss(this.getDataSenha(), this.getDataEmissaoNfe());
        }
        return tempo;
    }

    public String getTempoTotalHorasEstadiaAgendamento() {
        String tempo = "";
        if (ObjetoUtil.isNotNull(this.getCargaPontualDataAgendamentoInicial()) && ObjetoUtil.isNotNull(this.getDataEmissaoNfe())) {
            if (DateUtil.maiorQue(this.getCargaPontualDataAgendamentoInicial(), this.getDataEmissaoNfe())) {
                tempo = "-" + DateUtil.horasEntreDatasNoFormato_hhmmss(this.getDataEmissaoNfe(), this.getCargaPontualDataAgendamentoInicial());
            } else {
                tempo = DateUtil.horasEntreDatasNoFormato_hhmmss(this.getCargaPontualDataAgendamentoInicial(), this.getDataEmissaoNfe());
            }
        }
        return tempo;
    }

    public String getTempoTotalPatioExternoAgendamento() {
        String tempo = "";
        if (ObjetoUtil.isNotNull(this.getCargaPontualDataAgendamentoInicial()) && ObjetoUtil.isNotNull(this.getDtPrimeiraPesagem())) {
            if (DateUtil.maiorQue(this.getCargaPontualDataAgendamentoInicial(), this.getDtPrimeiraPesagem())) {
                tempo = "-" + DateUtil.horasEntreDatasNoFormato_hhmmss(this.getDtPrimeiraPesagem(), this.getCargaPontualDataAgendamentoInicial());
            } else {
                tempo = DateUtil.horasEntreDatasNoFormato_hhmmss(this.getCargaPontualDataAgendamentoInicial(), this.getDtPrimeiraPesagem());
            }
        }
        return tempo;
    }

    public String getTempoTotalVeiculoAgendamento() {
        String tempo = "";
        if (ObjetoUtil.isNotNull(this.getCargaPontualDataAgendamentoInicial()) && ObjetoUtil.isNotNull(this.getDataSenha())) {
            if (DateUtil.maiorQue(this.getCargaPontualDataAgendamentoInicial(), this.getDataSenha())) {
                tempo = "-" + DateUtil.horasEntreDatasNoFormato_hhmmss(this.getDataSenha(), this.getCargaPontualDataAgendamentoInicial());
            } else {
                tempo = DateUtil.horasEntreDatasNoFormato_hhmmss(this.getCargaPontualDataAgendamentoInicial(), this.getDataSenha());
            }
        }
        return tempo;
    }

    public BigDecimal getQtdKgLValido() {

        if (ObjetoUtil.isNull(this.getQtdKgL()) || this.getQtdKgL().doubleValue() <= 0)
            return BigDecimal.ZERO;
        return this.getQtdKgL().setScale(0, BigDecimal.ROUND_HALF_DOWN);
    }

    @Override
    public int compareTo(RelatorioFluxoDeCarregamentoDTO o) {
        if (ObjetoUtil.isNotNull(o.getDataSenha()) || ObjetoUtil.isNotNull((getDataSenha()))) {
            return 0;
        }
        return getDataSenha().compareTo(o.getDataSenha());
    }
}
