package br.com.sjc.modelo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TipoVeiculoSetaDTO extends DataDTO {

    private TipoVeiculoDTO tipoVeiculo;

    private String descricao;

    private Integer qtd;

    private Boolean padrao;

    public TipoVeiculoSetaDTO (
            Long id,
            Long idTipoVeiculo,
            String descricao,
            Integer qtd,
            Boolean padrao
    ) {

        this.setId ( id );
        this.tipoVeiculo = new TipoVeiculoDTO ( idTipoVeiculo );
        this.descricao = descricao;
        this.qtd = qtd;
        this.padrao = padrao;
    }

    public BigDecimal getQtdValida () {

        if ( this.qtd == null )
            return BigDecimal.ZERO;
        return new BigDecimal ( this.qtd );
    }
}
