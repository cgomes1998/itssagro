package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@MappedSuperclass
public class EntidadeAutenticada extends EntidadeGenerica {

    @Column(name = "id_usuario_autor_cadastro")
    private Long idUsuarioAutorCadastro;

    @Column(name = "id_usuario_autor_alteracao")
    private Long idUsuarioAutorAlteracao;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "status")
    private StatusEnum status;
    
    @Override
    public void preSalvar() {
    
        if (getId() == null) {
            setDataCadastro(DateUtil.hoje());
        }
    
        this.definirUsuarioAutor();
    }
    
    @PreUpdate
    public void preAlterar() {

        if (getId() != null) {

            setDataAlteracao(DateUtil.hoje());
        }

        this.definirUsuarioAutor();
    }

    @PreRemove
    public void preRemover() {

    }

    private void definirUsuarioAutor() {

        try {

            if (this.getId() == null) {

                this.setIdUsuarioAutorCadastro(this.getUsuarioLogado().getId());

            } else {

                this.setIdUsuarioAutorAlteracao(this.getUsuarioLogado().getId());

            }

        } catch (final Exception e) {

        }
    }

}
