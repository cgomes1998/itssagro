package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public enum EtapaCampoEnum {

    // Distribuição de Senha
    SENHA_SENHA(EtapaEnum.DISTRIBUICAO_SENHA, "senha", "Senha", null),
    SENHA_OPERACAO(EtapaEnum.DISTRIBUICAO_SENHA, "operacao", "Operação", OperacaoSenhaEnum.values()),
    SENHA_ORDEM_VENDA(EtapaEnum.DISTRIBUICAO_SENHA, "codigoOrdemVenda", "Ordem de Venda (SAP)", null),
    SENHA_MATERIAL(EtapaEnum.DISTRIBUICAO_SENHA, "material", "Material", null),
    SENHA_STATUS(EtapaEnum.DISTRIBUICAO_SENHA, "statusSenha", "Status", StatusSenhaEnum.values()),
    SENHA_MOTORISTA(EtapaEnum.DISTRIBUICAO_SENHA, "motorista", "Motorista", null),
    SENHA_SETA(EtapaEnum.DISTRIBUICAO_SENHA, "seta", "Seta", null),
    SENHA_MOTORISTA_NR_CNH(EtapaEnum.DISTRIBUICAO_SENHA, "motorista.cnh", "Nr. CNH", null),
    SENHA_MOTORISTA_DATA_VALIDADE_CNH(EtapaEnum.DISTRIBUICAO_SENHA, "motorista.dataValidadeCNH", "Data Validade CNH", null),
    SENHA_MOTORISTA_DATA_TREINAMENTO(EtapaEnum.DISTRIBUICAO_SENHA, "motorista.dataTreinamento", "Data Treinamento", null),
    SENHA_MOTORISTA_DATA_VALIDADE_TREINAMENTO(EtapaEnum.DISTRIBUICAO_SENHA, "motorista.dataValidadeTreinamento", "Data Validade Treinamento", null),
    SENHA_VEICULO_PLACA1(EtapaEnum.DISTRIBUICAO_SENHA, "motorista.veiculo.placa1", "Placa do Cavalo", null),
    SENHA_VEICULO_DATA_VENCIMENTO_PLACA1(EtapaEnum.DISTRIBUICAO_SENHA, "motorista.veiculo.dataVencimentoPlaca1", "Data Venc. IPVA Placa Cavalo", null),
    SENHA_VEICULO_DATA_CIV_PLACA1(EtapaEnum.DISTRIBUICAO_SENHA, "motorista.veiculo.dataCivPlaca1", "Data CIV Placa Cavalo", null),
    SENHA_VEICULO_DATA_CALIBRAGEM_PLACA1(EtapaEnum.DISTRIBUICAO_SENHA, "motorista.veiculo.dataCalibragemPlaca1", "Data Calibragem Placa Cavalo", null),
    SENHA_VEICULO_PLACA2(EtapaEnum.DISTRIBUICAO_SENHA, "motorista.veiculo.placa2", "Placa Carreta 1", null),
    SENHA_VEICULO_DATA_VENCIMENTO_PLACA2(EtapaEnum.DISTRIBUICAO_SENHA, "motorista.veiculo.dataVencimentoPlaca2", "Data Venc. IPVA Placa Carreta 1", null),
    SENHA_VEICULO_DATA_CIV_PLACA2(EtapaEnum.DISTRIBUICAO_SENHA, "motorista.veiculo.dataCivPlaca2", "Data CIV Placa Carreta 1", null),
    SENHA_VEICULO_DATA_CALIBRAGEM_PLACA2(EtapaEnum.DISTRIBUICAO_SENHA, "motorista.veiculo.dataCalibragemPlaca2", "Data Calibragem Placa Carreta 1", null),
    SENHA_VEICULO_PLACA3(EtapaEnum.DISTRIBUICAO_SENHA, "motorista.veiculo.placa3", "Placa Carreta 2", null),
    SENHA_VEICULO_DATA_VENCIMENTO_PLACA3(EtapaEnum.DISTRIBUICAO_SENHA, "motorista.veiculo.dataVencimentoPlaca3", "Data Venc. IPVA Placa Carreta 2", null),
    SENHA_VEICULO_DATA_CIV_PLACA3(EtapaEnum.DISTRIBUICAO_SENHA, "motorista.veiculo.dataCivPlaca3", "Data CIV Placa Carreta 2", null),
    SENHA_VEICULO_DATA_CALIBRAGEM_PLACA3(EtapaEnum.DISTRIBUICAO_SENHA, "motorista.veiculo.dataCalibragemPlaca3", "Data Calibragem Placa Carreta 2", null),
    SENHA_VEICULO_TIPO(EtapaEnum.DISTRIBUICAO_SENHA, "motorista.veiculo.tipo", "Tipo Veiculo", null),
    SENHA_VEICULO_RENAVAM(EtapaEnum.DISTRIBUICAO_SENHA, "motorista.veiculo.renavam", "Renavam", null),
    SENHA_VEICULO_CHASSI(EtapaEnum.DISTRIBUICAO_SENHA, "motorista.veiculo.chassi", "Chassi", null),
    SENHA_TRANSPORTADORA_CPFCNPJ(EtapaEnum.DISTRIBUICAO_SENHA, "transportadora.cpfCnpj", "CPF/CNPJ Transportadora", null),
    SENHA_TRANSPORTADORA_CODIGO(EtapaEnum.DISTRIBUICAO_SENHA, "transportadora.codigo", "Insc. Estadual Transportadora", null),
    SENHA_TRANSPORTADORA_DESCRICAO(EtapaEnum.DISTRIBUICAO_SENHA, "transportadora.descricao", "Descrição Transportadora", null),
    SENHA_CLIENTE_CPFCNPJ(EtapaEnum.DISTRIBUICAO_SENHA, "cliente.cpfCnpj", "CPF/CNPJ Cliente", null),
    SENHA_CLIENTE_INSCRICAO_ESTADUAL(EtapaEnum.DISTRIBUICAO_SENHA, "cliente.inscricaoEstadual", "Insc. Estadual Cliente", null),
    SENHA_CLIENTE_DESCRICAO(EtapaEnum.DISTRIBUICAO_SENHA, "cliente.descricao", "Descrição Cliente", null),
    SENHA_NUMERO_CARTAO_ACESSO(EtapaEnum.DISTRIBUICAO_SENHA, "numeroCartaoAcesso", "Número Cartão Acesso", null),
    SENHA_QUANTIDADE_CARREGAR(EtapaEnum.DISTRIBUICAO_SENHA, "quantidadeCarregar", "Qtd. a Carregar", null),
    SENHA_SALDO_ORDEM_VENDA(EtapaEnum.DISTRIBUICAO_SENHA, "saldoOrdemVenda", "Saldo da Ordem", null),

    // Checklist
    CHECKLIST_STATUS(EtapaEnum.CHECKLIST, "statusChecklist", "Status", StatusChecklistEnum.values()),

    // Criar Romaneio
    ROMANEIO_NUMERO_ROMANEIO(EtapaEnum.CRIAR_ROMANEIO, "numeroRomaneio", "Número da Ordem", null),
    ROMANEIO_SENHA(EtapaEnum.CRIAR_ROMANEIO, "senha.senha", "Senha", null),
    ROMANEIO_NUMERO_CARTAO_ACESSO(EtapaEnum.CRIAR_ROMANEIO, "numeroCartaoAcesso", "Nr Cartão de Acesso", null),
    ROMANEIO_CENTRO(EtapaEnum.CRIAR_ROMANEIO, "centro.descricao", "Centro", null),
    ROMANEIO_DATA_CADASTRO(EtapaEnum.CRIAR_ROMANEIO, "dataCadastro", "Data Emissão", null),
    ROMANEIO_STATUS_ROMANEIO(EtapaEnum.CRIAR_ROMANEIO, "statusEtapa", "Status", StatusRomaneioEtapaEnum.values()),
    ROMANEIO_MATERIAL(EtapaEnum.CRIAR_ROMANEIO, "material.descricao", "Material", null),

    // Pesagem (Tara)
    PESAGEM_TARA_PESO_TARA(EtapaEnum.PESAGEM_TARA, "pesoFinal", "Peso Tara (Kg)", null),
    PESAGEM_TARA_STATUS(EtapaEnum.PESAGEM_TARA, "statusPesagem", "Status", StatusPesagemQualidadeEnum.values()),

    // Pesagem (Bruto)
    PESAGEM_BRUTO_PESO_BRUTO(EtapaEnum.PESAGEM_BRUTO, "pesoInicial", "Peso Bruto (Kg)", null),
    PESAGEM_BRUTO_PESO_TIPO_VEICULO(EtapaEnum.PESAGEM_BRUTO, "pesoInicial", "Peso Tipo Veículo (Kg)", null),
    PESAGEM_PESO_LIQUIDO(EtapaEnum.PESAGEM_BRUTO, "pesoInicial", "Peso Líquido (Kg)", null),
    PESAGEM_BRUTO_CONVERSAO(EtapaEnum.PESAGEM_BRUTO, "statusPesagem", "Conversão", null),
    PESAGEM_BRUTO_STATUS(EtapaEnum.PESAGEM_BRUTO, "statusPesagem", "Status", StatusPesagemQualidadeEnum.values()),

    // Lacragem
    LACRAGEM_STATUS_LACRE(EtapaEnum.LACRAGEM, "statusLacragem", "Status", StatusLacragemEnum.values()),

    // Conversão Litragem
    CONVERSAO_LITRAGEM_TANQUE(EtapaEnum.CONVERSAO_LITRAGEM, "tanque", "Tanque", null),
    CONVERSAO_LITRAGEM_TEMP_TANQUE(EtapaEnum.CONVERSAO_LITRAGEM, "tempTanque", "Temp. Tanque", null),
    CONVERSAO_LITRAGEM_GRAUINPM(EtapaEnum.CONVERSAO_LITRAGEM, "grauInpm", "Grau INPM", null),
    CONVERSAO_LITRAGEM_ACIDEZ(EtapaEnum.CONVERSAO_LITRAGEM, "acidez", "Acidez", null),
    CONVERSAO_LITRAGEM_STATUS(EtapaEnum.CONVERSAO_LITRAGEM, "statusConversao", "Status", StatusConversaoLitragemEnum.values()),

    // Análise de Qualidade
    ANALISE_QUALIDADE_STATUS(EtapaEnum.ANALISE_QUALIDADE, "statusAnaliseQualidade", "Status", StatusAnaliseQualidadeEnum.values()),

    // Emitir Documento
    EMITIR_DOCUMENTO_STATUS_EMISSAO(EtapaEnum.EMITIR_DOCUMENTO, "statusEmissaoDocumento", "Status", StatusEmissaoDocumentoEnum.values()),
    EMITIR_DOCUMENTO_DOCUMENTO_IMPRESSO(EtapaEnum.EMITIR_DOCUMENTO, "documentoImpresso", "Documento Impresso", null),
    EMITIR_DOCUMENTO_NF_IMPRESSO(EtapaEnum.EMITIR_DOCUMENTO, "nfImpresso", "NF Impresso", null),
    EMITIR_DOCUMENTO_LAUDO_ANALISE_IMPRESSO(EtapaEnum.EMITIR_DOCUMENTO, "laudoAnaliseImpresso", "Laudo Analise Impresso", null),
    EMITIR_DOCUMENTO_ROMANEIO_IMPRESSO(EtapaEnum.EMITIR_DOCUMENTO, "romaneioImpresso", "Romaneio Impresso", null),

    // Descarga
    DESCARGA_STATUS_DESCARREGAMENTO(EtapaEnum.DESCARGA, "statusDescarregamento", "Status", StatusDescarregamentoEnum.values()),

    // Pendência (Comercial)
    PENDENCIA_COMERCIAL_STATUS_PENDENCIA(EtapaEnum.PENDENCIA_COMERCIAL, "statusPendencia", "Status", StatusPendenciaEnum.values()),

    // Conferência (Casos Gerais)
    CONFERENCIA_GERAIS_STATUS_CONFERENCIA(EtapaEnum.CONFERENCIA_GERAIS, "statusConferencia", "Status", StatusConferenciaEnum.values()),

    // Cancelamento de Ordem
    CANCELAMENTO_ORDEM_STATUS_CANCELAMENTO(EtapaEnum.CANCELAMENTO_ORDEM, "statusEtapa", "Status", StatusRomaneioEtapaEnum.values()),

    // Confirmação do Peso
    CONFIRMACAO_PESO(EtapaEnum.CONFIRMACAO_PESO, "botaoAcionadoConfirmacaoPeso", "Botão acionado", null),
    CONFIRMACAO_PESO_PESO_LIQUIDO(EtapaEnum.CONFIRMACAO_PESO, "pesoInicial", "Peso Líquido (Kg)", null),

    // Carregamento
    STATUS_LOCAL_CARREGAMENTO(EtapaEnum.CARREGAMENTO, "statusLocalCarregamento", "Status", StatusLocalCarregamentoPesoEnum.values()),

    // Aguardando Chamada para Carregamento
    AGUARDANDO_CHAMADA_CARREGAMENTO(EtapaEnum.AGUARDANDO_CHAMADA_CARREGAMENTO, "botaoAcionadoAguardandoChamadaCarregamento", "Botão acionado", null);

    private EtapaEnum etapa;

    //Deve representar a estrutura conforme entidade vinculada, pois é instanciado via reflexão
    private String campo;

    private String descricao;

    private Object[] valores;

    public static List<EtapaCampoEnum> obterPorEtapa(EtapaEnum etapa) {
        return Arrays.asList(EtapaCampoEnum.values()).stream().filter(
                etapaCampo -> etapaCampo.etapa.equals(etapa)).collect(Collectors.toList()
        );
    }

}
