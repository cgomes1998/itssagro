package br.com.sjc.modelo.pojo.cargaPontual.agendamento.trocaStatus;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AgendamentoProdutoDocaItemStatus {

    private String condicao;

    private String idAgendamento;

    private String statusDestino;

    private String quantidadeRealizada;

    private String dataTrocaStatus;

}
