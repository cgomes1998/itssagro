package br.com.sjc.modelo.sap.request.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SAPItem
public class BapiShipmentItemAction extends JCoItem {

    @SAPColumn( name = "DELIVERY" )
    private String delivery;

    @SAPColumn( name = "ITENERARY" )
    private String itenerary;
}
