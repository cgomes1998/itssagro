package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.*;

@Getter
@Setter
public class ItemNFNotaListagemDTO {

    @ProjectionProperty(values = {"id", "numeroRomaneio", "requestKey"})
    private Romaneio romaneio;

    @ProjectionProperty
    private String numeroNfe;

    @ProjectionProperty
    private String requestKey;

    @ProjectionProperty
    private String numeroNotaFiscalTransporte;

    public ItemNFNotaListagemDTO() {

    }

    @Builder(builderMethodName = "builderItemNfNotaListagemDto")
    public ItemNFNotaListagemDTO(Romaneio romaneio, String numeroNfe, String numeroNfTransporte, String requestKey) {

        this.romaneio = romaneio;
        this.numeroNfe = numeroNfe;
        this.numeroNotaFiscalTransporte = numeroNfTransporte;
        this.requestKey = requestKey;
    }

}
