package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.Motivo;
import br.com.sjc.modelo.endpoint.response.item.MotoristaResponse;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import br.com.sjc.modelo.enums.UFEnum;
import br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento.Conteudo2;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.anotation.EntityProperties;
import br.com.sjc.util.bundle.MessageSupport;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_motorista", schema = "sap")
@AllArgsConstructor
public class Motorista extends EntidadeSAPPre {

    @Column(name = "nome")
    private String nome;

    @Column(name = "cpf", length = 11)
    private String cpf;

    @Column(name = "cnh", length = 20)
    private String cnh;

    @Column(name = "cep", length = 9)
    private String cep;

    @Column(name = "rua")
    private String rua;

    @Column(name = "bairro")
    private String bairro;

    @Column(name = "numero", length = 10)
    private String numero;

    @Column(name = "municipio")
    private String municipio;

    @Column(name = "telefone", length = 14)
    private String telefone;

    @Column(name = "celular", length = 14)
    private String celular;

    @Enumerated(EnumType.STRING)
    @Column(name = "uf", length = 2)
    private UFEnum uf;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_validade_cnh")
    private Date dataValidadeCNH;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_treinamento")
    private Date dataTreinamento;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_validade_treinamento")
    private Date dataValidadeTreinamento;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_inicio_bloqueio")
    private Date dataInicioBloqueio;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_fim_bloqueio")
    private Date dataFimBloqueio;

    @ManyToOne
    @JoinColumn(name = "id_veiculo")
    private Veiculo veiculo;

    @EntityProperties(value = {"id", "descricao", "tipo", "conferenciaPeso", "buscarDadosRomaneio"}, targetEntity = Motivo.class)
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "tb_motorista_motivo", schema = "sap", joinColumns = @JoinColumn(name = "id_motorista"), inverseJoinColumns = @JoinColumn(name = "id_motivo"))
    private List<Motivo> motivos = new ArrayList<>();

    public Motorista() {

    }

    @Builder(builderMethodName = "builderCargaPontual")
    public Motorista(br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento.Motorista motorista) {

        setStatus(StatusEnum.ATIVO);

        setStatusRegistro(StatusRegistroEnum.ATIVO);

        setStatusCadastroSap(StatusCadastroSAPEnum.NAO_ENVIADO);

        if(StringUtil.isNotNullEmpty(motorista.getNome())){
            setNome(motorista.getNome());
        }

        if(StringUtil.isNotNullEmpty(motorista.getCpf())){
            setCpf(motorista.getCpf());
        }

        if(StringUtil.isNotNullEmpty(motorista.getCnh())){
            setCnh(motorista.getCnh());
        }

        if (StringUtil.isNotNullEmpty(motorista.getData_validade_cnh())) {
            setDataValidadeCNH(DateUtil.format(motorista.getData_validade_cnh(), "yyyy-MM-dd hh:mm:ss"));
        }

        if (StringUtil.isNotNullEmpty(motorista.getData_validade_mopp())) {
            setDataValidadeTreinamento(DateUtil.format(motorista.getData_validade_mopp(), "yyyy-MM-dd hh:mm:ss"));
        }

        inicializarMotoristaTelefone(motorista);

        if (ObjetoUtil.isNotNull(motorista.getEndereco())) {

            if(StringUtil.isNotNullEmpty(motorista.getEndereco().getCep())){
                setCep(motorista.getEndereco().getCep());
            }

            if(StringUtil.isNotNullEmpty(motorista.getEndereco().getRua())){
                setRua(motorista.getEndereco().getRua());
            }

            if(motorista.getEndereco().getNumero() > 0){
                setNumero(String.valueOf(motorista.getEndereco().getNumero()));
            }

            if(StringUtil.isNotNullEmpty(motorista.getEndereco().getBairro())){
                setBairro(motorista.getEndereco().getBairro());
            }

            if (StringUtil.isNotNullEmpty(motorista.getEndereco().getMunicipio())) {
                setMunicipio(motorista.getEndereco().getMunicipio().trim());
            }

            if (StringUtil.isNotNullEmpty(motorista.getEndereco().getUf())) {
                setUf(UFEnum.get(motorista.getEndereco().getUf().trim().toUpperCase()));
            }
        }
    }

    public void setarValoresCargaPontual(br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento.Motorista motorista) {

        setStatusCadastroSap(StatusCadastroSAPEnum.NAO_ENVIADO);

        setNome(motorista.getNome());

        if(StringUtil.isNotNullEmpty(motorista.getCpf())){
            setCpf(motorista.getCpf());
        }

        if(StringUtil.isNotNullEmpty(motorista.getCnh())){
            setCnh(motorista.getCnh());
        }

        if (StringUtil.isNotNullEmpty(motorista.getData_validade_cnh())) {

            setDataValidadeCNH(DateUtil.format(motorista.getData_validade_cnh(), "yyyy-MM-dd hh:mm:ss"));
        }

        if (StringUtil.isNotNullEmpty(motorista.getData_validade_mopp())) {

            setDataValidadeTreinamento(DateUtil.format(motorista.getData_validade_mopp(), "yyyy-MM-dd hh:mm:ss"));
        }

        inicializarMotoristaTelefone(motorista);

        if (ObjetoUtil.isNotNull(motorista.getEndereco())) {

            if(StringUtil.isNotNullEmpty(motorista.getEndereco().getCep())){
                setCep(motorista.getEndereco().getCep());
            }

            if(StringUtil.isNotNullEmpty(motorista.getEndereco().getRua())){
                setRua(motorista.getEndereco().getRua());
            }


            if(motorista.getEndereco().getNumero() > 0){
                setNumero(String.valueOf(motorista.getEndereco().getNumero()));
            }

            if(StringUtil.isNotNullEmpty(motorista.getEndereco().getBairro())){
                setBairro(motorista.getEndereco().getBairro());
            }

            if (StringUtil.isNotNullEmpty(motorista.getEndereco().getMunicipio())) {
                setMunicipio(motorista.getEndereco().getMunicipio().trim());
            }

            if (StringUtil.isNotNullEmpty(motorista.getEndereco().getUf())) {
                setUf(UFEnum.get(motorista.getEndereco().getUf().trim().toUpperCase()));
            }
        }
    }

    private void inicializarMotoristaTelefone(br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento.Motorista motorista) {

        if (ColecaoUtil.isNotEmpty(motorista.getTelefones())) {

            motorista.getTelefones().stream().filter(telefone -> telefone.getTipo().contains("TELEFONE")).findFirst().ifPresent(telefone -> {
                if(StringUtil.isNotNullEmpty(telefone.getNumero())){
                    setTelefone(telefone.getNumero().replace("-", ""));
                }
            });

            motorista.getTelefones().stream().filter(telefone -> telefone.getTipo().contains("CELULAR")).findFirst().ifPresent(telefone -> {
                if(StringUtil.isNotNullEmpty(telefone.getNumero())){
                    setCelular(telefone.getNumero().replace("-", ""));
                }
            });
        }
    }

    public String getDescricaoFormatada() {

        return this.toString();
    }

    public String getDescricaoNota() {

        return StringUtil.format(this.cpf, "###.###.###-##") + " - " + this.nome;
    }

    @Override
    public StatusEnum getStatus() {

        return super.getStatus();
    }

    @Override
    public void setStatus(StatusEnum status) {

        super.setStatus(status);
    }

    public MotoristaResponse toResponse() {

        MotoristaResponse response = new MotoristaResponse();

        response.setCodigo(this.getCodigo());

        response.setNome(this.getNome());

        return response;
    }

    public void verificarSeMotoristaEstaBloqueado() throws Exception {

        if (ObjetoUtil.isNotNull(this.getDataInicioBloqueio()) && ObjetoUtil.isNotNull(this.getDataFimBloqueio())) {
            if (DateUtil.estaEntre(DateUtil.hoje(), this.getDataInicioBloqueio(), this.getDataFimBloqueio())) {
                throw new Exception(MessageSupport.getMessage("MSGE027"));
            }
        }
    }

    @Override
    public String toString() {

        if (StringUtil.isNotNullEmpty(this.getCodigo())) {

            return this.getCodigo() + " - " + this.nome;
        }

        if (StringUtil.isNotNullEmpty(this.cpf)) {

            return StringUtil.format(this.cpf, "###.###.###-##") + " - " + this.nome;
        }

        return this.nome;
    }

}
