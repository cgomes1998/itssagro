package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class EventInResponseItem extends JCoItem {

    @SAPColumn(name = "RETNR", size = 5)
    private String codReturn;

    @SAPColumn(name = "DESCR", size = 150)
    private String descReturn;

}
