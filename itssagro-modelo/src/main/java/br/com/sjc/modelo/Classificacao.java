package br.com.sjc.modelo;

import br.com.sjc.modelo.sap.ItensClassificacao;
import br.com.sjc.modelo.sap.Safra;
import br.com.sjc.modelo.sap.Veiculo;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_classificacao", schema = "sap")
public class Classificacao extends EntidadeGenerica {

    @Column(columnDefinition = "text")
    private String observacao;

    @EntityProperties(value = {"id"})
    @ManyToOne
    @JoinColumn(name = "id_veiculo")
    private Veiculo placaCavalo;

    @EntityProperties(value = {"id"})
    @ManyToOne
    @JoinColumn(name = "id_safra")
    private Safra safra;

    @EntityProperties(value = {"id"})
    @ManyToOne
    @JoinColumn(name = "id_usuario_classificador")
    private Usuario usuarioClassificador;

    @EntityProperties(value = {"id", "itemClassificao", "descricaoItem", "codigoTabela", "indice", "percentualDesconto", "desconto", "descontoClassificacao", "classificacao.id", "romaneio.id"}, targetEntity = ItensClassificacao.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, mappedBy = "classificacao")
    @Getter(AccessLevel.NONE)
    private List<ItensClassificacao> itens;

    public List<ItensClassificacao> getItens() {

        return ColecaoUtil.isNotEmpty(itens) ? itens.stream().sorted(Comparator.comparingLong(ItensClassificacao::getItemClassificacaoNumerico)).collect(Collectors.toList()) : null;
    }

    public Classificacao() {

        super();
    }

    public Classificacao(String observacao, Veiculo placaCavalo, Safra safra, Usuario usuarioClassificador, List<ItensClassificacao> itens) {

        this.observacao = observacao;
        this.placaCavalo = placaCavalo;
        this.safra = safra;
        this.usuarioClassificador = usuarioClassificador;
        if (ColecaoUtil.isNotEmpty(itens)) {
            this.itens = new ArrayList(itens.stream().map(i -> new ItensClassificacao(i.getItemClassificao(), i.getDescricaoItem(), i.getCodigoTabela(), i.getIndice(), i.getPercentualDesconto(), i.getDesconto(),
                    i.getDescontoClassificacao())).collect(Collectors.toSet()));
        }
    }

}
