package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.DirecaoEnum;
import br.com.sjc.modelo.enums.TipoFluxoEnum;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class CapturaBalanca {

    private List<DirecaoEnum> direcoes;

    private TipoFluxoEnum tipoFluxo;
}
