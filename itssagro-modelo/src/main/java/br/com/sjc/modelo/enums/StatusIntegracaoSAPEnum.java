package br.com.sjc.modelo.enums;

import br.com.sjc.util.ObjetoUtil;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public enum StatusIntegracaoSAPEnum {

    COMPLETO("Completo", 1),

    AGUARDANDO("Aguardando", 2),

    ATENCAO("Atenção: Validação apresenta mensagens", 3),

    ERRO("Erro (Processamento)", 4),

    PAUSADO("Pausado", 5),

    ESTORNADO("Estornado", 6),

    ERRO_INTEGRACAO("Erro (Integração)", 7),

    ERRO_DIVERGENCIA_PEDIDO_NOTA("Erro (Divergência entre nota e pedido)", 8);

    private String descricao;

    private Integer codigo;

    private StatusIntegracaoSAPEnum(final String descricao, final Integer codigo) {

        this.descricao = descricao;

        this.codigo = codigo;
    }

    public static boolean contemCodigo(Integer status) {

        return get(status) != null;
    }

    public static List<StatusIntegracaoSAPEnum> listarTodosExceto(StatusIntegracaoSAPEnum... status) {

        return Arrays.asList(StatusIntegracaoSAPEnum.values()).stream().filter(i -> Arrays.asList(status).stream().filter(s -> s.equals(i)).count() <= 0).collect(Collectors.toList());
    }

    public static StatusIntegracaoSAPEnum get(final Integer codigo) {

        if (ObjetoUtil.isNotNull(codigo)) {

            return Arrays.asList(StatusIntegracaoSAPEnum.values()).stream().filter((statusSAP) -> statusSAP.getCodigo().equals(codigo)).findFirst().orElse(null);
        }

        return null;
    }

}
