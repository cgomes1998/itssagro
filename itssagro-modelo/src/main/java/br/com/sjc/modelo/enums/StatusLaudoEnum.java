package br.com.sjc.modelo.enums;

import lombok.Getter;
import lombok.AllArgsConstructor;

@Getter
@AllArgsConstructor
public enum StatusLaudoEnum {
    APROVADO("Aprovado"),
    REPROVADO("Reprovado");

    private String descricao;
}