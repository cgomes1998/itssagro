package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import br.com.sjc.modelo.enums.TipoPessoaEnum;
import br.com.sjc.modelo.enums.UFEnum;
import br.com.sjc.modelo.sap.Cliente;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.Objects;

/**
 * Created by julio.bueno on 26/06/2019.
 */
@Getter
@Setter
@NoArgsConstructor
public class ClienteDTO extends DataDTO {

    @ProjectionProperty
    private String codigo;

    @ProjectionProperty
    private String cpfCnpj;

    @ProjectionProperty
    private String nome;

    @ProjectionProperty
    private String municipio;

    @ProjectionProperty
    private TipoPessoaEnum tipoPessoa;

    @ProjectionProperty
    private UFEnum uf;

    @ProjectionProperty
    private StatusRegistroEnum statusRegistro;

    @ProjectionProperty
    private StatusEnum status;

    @ProjectionProperty
    private Date dataCadastro;

    public String getStatusRegistroDescricao() {

        return Objects.isNull(statusRegistro) ? "" : statusRegistro.getDescricao();
    }

}
