package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.EntidadeTipo;
import br.com.sjc.modelo.enums.LogAtividadeAcao;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by Claudio Gomes on 05/05/2021.
 */
@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_log_atividade")
public class LogAtividade extends EntidadeGenerica {

    @Enumerated(EnumType.STRING)
    @Column(name = "entidade_tipo")
    private EntidadeTipo entidadeTipo;

    @Enumerated(EnumType.STRING)
    @Column(name = "log_atividade_acao")
    private LogAtividadeAcao logAtividadeAcao;

    @Column(name = "usuario_autor")
    private Long usuarioAutor;

    @Column(name = "entidade_id")
    private Long entidadeId;

    public LogAtividade() {

    }

    @Builder(builderMethodName = "logAtividadeBuilder")
    public LogAtividade(EntidadeTipo entidadeTipo, EntidadeGenerica entidadeGenerica, Usuario usuario, LogAtividadeAcao logAtividadeAcao) {

        this.entidadeTipo = entidadeTipo;

        this.usuarioAutor = usuario.getId();

        this.entidadeId = entidadeGenerica.getId();

        this.logAtividadeAcao = logAtividadeAcao;
    }

}
