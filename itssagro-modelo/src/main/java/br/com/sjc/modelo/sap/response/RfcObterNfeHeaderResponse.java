package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SAPParameter(function = "/ITSSAGRO/XML_NFE_TCNH_T_HD")
public class RfcObterNfeHeaderResponse extends JCoItem {

    @SAPColumn(name = "NNF")
    private String nnf;

    @SAPColumn(name = "SERIE")
    private String serie;

    @SAPColumn(name = "DHEMI")
    private String dhemi;

    @SAPColumn(name = "NPROT")
    private String nprot;

    @SAPColumn(name = "DIG")
    private String dig;

    @SAPColumn(name = "NR_ALE")
    private String nrAle;

    @SAPColumn(name = "VNF")
    private BigDecimal vnf;

    @SAPColumn(name = "LAST_STEPSTATUS")
    private String status;
}
