package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.EntidadeGenerica;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Inheritance( strategy = InheritanceType.JOINED )
@Table( name = "tb_bridge", schema = "sap" )
public class Bridge extends EntidadeGenerica {

    private static final long serialVersionUID = -3351987708785118658L;

    @Column(length = 20)
    private String requestKey;

    private String campo;

    @Column(length = 3)
    private String sequencia;

    @Column(length = 2)
    private String itemDocumento;

    @Column(length = 2)
    private String grupo;

    private String valor;

    @JsonIgnore
    @ManyToOne
    @JoinColumn( name = "id_romaneio", referencedColumnName = "id" )
    private Romaneio romaneio;

}
