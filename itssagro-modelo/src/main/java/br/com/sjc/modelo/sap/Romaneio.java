package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.*;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.enums.*;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Setter
@Getter
@Entity
@Table(name = "tb_romaneio", schema = "sap")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Romaneio extends EntidadeAutenticada {

    private static final long serialVersionUID = -7939466771665487472L;

    @EntityProperties(value = {"id", "entidade", "descricao", "modulo", "direcaoOperacao", "operacao", "eventoEntrada", "eventoSaida", "possuiDivisaoCarga", "possuiPesagem", "possuiClassificacao", "pesagemManual", "aplicaClassificacao",
            "simplesPesagem", "possuiConsultaSaldoDisponivel", "possuiBuscaNFeTransporte", "possuiClassificacaoOrigem", "possuiConsultaContrato", "possuiDadosNfe", "possuiRetornoNfeSAP", "possuiRetornoNfeTransporteSAP",
            "possuiValidacaoEntreVlrUniNotaEPedido", "toleranciaEntreVlrUniNotaEPedido", "possuiValidacaoParaEscriturarNota", "possuiImpressaoPorEtapa", "possuiValidacaoChaveAcesso", "possuiRateio", "possuiValidacaoDivergenciaProdutorNotaEPedido",
            "possuiValidacaoChaveEFornecedor", "utilizaObterDadosNfe", "utilizaObterDadosCte", "enviarEmailPesagemManual", "gestaoPatio", "criarRemessa", "criarDocumentoTransporte", "possuiValidacaoConversaoLitragem", "umidadeInformadaManualmente", "possuiRegraComplemento",
            "requestCount", "tempoPreCadastro", "ultimaSincronizacao"})
    @ManyToOne
    @JoinColumn(name = "id_operacao", referencedColumnName = "id")
    private DadosSincronizacao operacao;

    @EntityProperties(value = {"id", "cpf", "cnpj", "codigo", "nome", "fazenda", "endFazenda", "bairro", "municipio", "estado", "cep", "inscricaoEstadual", "statusInscEstadual"})
    @ManyToOne
    @JoinColumn(name = "id_produtor", referencedColumnName = "id")
    private Produtor produtor;
    
    @EntityProperties(value = {"id", "cpf", "cnpj", "codigo", "nome", "fazenda", "endFazenda", "bairro", "municipio", "estado", "cep", "inscricaoEstadual", "statusInscEstadual"})
    @ManyToOne
    @JoinColumn(name = "id_produtor_destino", referencedColumnName = "id")
    private Produtor produtorDestino;

    @EntityProperties(value = {"id", "cpf", "cnpj", "codigo", "nome", "fazenda", "endFazenda", "bairro", "municipio", "estado", "cep", "inscricaoEstadual", "statusInscEstadual"})
    @ManyToOne
    @JoinColumn(name = "id_armazem_terceiros", referencedColumnName = "id")
    private Produtor armazemTerceiros;

    @EntityProperties(value = {"id", "cpf", "cnpj", "codigo", "nome", "fazenda", "endFazenda", "bairro", "municipio", "estado", "cep", "inscricaoEstadual", "statusInscEstadual"})
    @ManyToOne
    @JoinColumn(name = "id_deposito_entrada", referencedColumnName = "id")
    private Produtor depositoEntrada;

    @EntityProperties(value = {"id", "cpf", "cnpj", "codigo", "nome", "fazenda", "endFazenda", "bairro", "municipio", "estado", "cep", "inscricaoEstadual", "statusInscEstadual"})
    @ManyToOne
    @JoinColumn(name = "id_deposito_saida", referencedColumnName = "id")
    private Produtor depositoSaida;

    @EntityProperties(value = {"id", "codigo", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_safra", referencedColumnName = "id")
    private Safra safra;

    @EntityProperties(value = {"id", "cpfCnpj", "codigo", "nome"})
    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id")
    private Cliente cliente;

    @EntityProperties(value = {"id", "codigo", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_safra_plantil", referencedColumnName = "id")
    private Safra safraPlantil;

    @EntityProperties(value = {"id", "codigo", "descricao", "unidadeMedida", "valorUnitario", "realizarConversaoLitragem", "necessitaTreinamentoMotorista", "lacrarCarga", "realizarAnaliseQualidade", "buscarCertificadoQualidade",
            "necessitaMaisDeUmaAnalise", "possuiEtapaCarregamento", "gerarNumeroCertificado", "possuiResolucaoAnp", "exibirEspecificacaoNaImpressaoDoLaudo", "toleranciaEmKg", "tipoTransporte"})
    @ManyToOne
    @JoinColumn(name = "id_material", referencedColumnName = "id")
    private Material material;

    @EntityProperties(value = {"id", "codigo", "descricao", "statusCadastroSap", "statusRegistro"})
    @ManyToOne
    @JoinColumn(name = "id_deposito", referencedColumnName = "id")
    private Deposito deposito;

    @EntityProperties(value = {"id", "codigo", "nome", "cpf"})
    @ManyToOne
    @JoinColumn(name = "id_motorista", referencedColumnName = "id")
    private Motorista motorista;

    @EntityProperties(value = {"id", "codigo", "placa1", "placa2", "placa3", "municipio", "renavam", "chassi", "marca", "cor", "eixos", "ufPlaca1", "ufPlaca2", "ufPlaca3", "dataVencimentoPlaca1", "dataVencimentoPlaca2",
            "dataVencimentoPlaca3", "dataCivPlaca1", "dataCivPlaca2", "dataCivPlaca3", "dataCalibragemPlaca1", "dataCalibragemPlaca2", "dataCalibragemPlaca3", "dataCipp1", "dataCipp2", "dataCipp3", "dataVencimentoCr",
            "dataVencimentoAatipp", "antt", "tipo.id", "tipo.peso"})
    @ManyToOne
    @JoinColumn(name = "id_placa_cavalo", referencedColumnName = "id")
    private Veiculo placaCavalo;

    @EntityProperties(value = {"id", "codigo", "placa1", "placa2", "placa3", "municipio", "renavam", "chassi", "marca", "cor", "eixos", "ufPlaca1", "ufPlaca2", "ufPlaca3", "dataVencimentoPlaca1", "dataVencimentoPlaca2",
            "dataVencimentoPlaca3", "dataCivPlaca1", "dataCivPlaca2", "dataCivPlaca3", "dataCalibragemPlaca1", "dataCalibragemPlaca2", "dataCalibragemPlaca3", "dataCipp1", "dataCipp2", "dataCipp3", "dataVencimentoCr",
            "dataVencimentoAatipp", "antt"})
    @ManyToOne
    @JoinColumn(name = "id_placa_cavalo_um", referencedColumnName = "id")
    private Veiculo placaCavaloUm;

    @EntityProperties(value = {"id", "codigo", "placa1", "placa2", "placa3", "municipio", "renavam", "chassi", "marca", "cor", "eixos", "ufPlaca1", "ufPlaca2", "ufPlaca3", "dataVencimentoPlaca1", "dataVencimentoPlaca2",
            "dataVencimentoPlaca3", "dataCivPlaca1", "dataCivPlaca2", "dataCivPlaca3", "dataCalibragemPlaca1", "dataCalibragemPlaca2", "dataCalibragemPlaca3", "dataCipp1", "dataCipp2", "dataCipp3", "dataVencimentoCr",
            "dataVencimentoAatipp", "antt"})
    @ManyToOne
    @JoinColumn(name = "id_placa_cavalo_dois", referencedColumnName = "id")
    private Veiculo placaCavaloDois;

    @EntityProperties(value = {"id", "codigo", "descricao", "cnpj", "cpf"})
    @ManyToOne
    @JoinColumn(name = "id_transportadora", referencedColumnName = "id")
    private Transportadora transportadora;

    @EntityProperties(value = {"id", "codigo", "descricao", "cnpj", "cpf"})
    @ManyToOne
    @JoinColumn(name = "id_transportadora_subcontratada", referencedColumnName = "id")
    private Transportadora transportadoraSubcontratada;

    @EntityProperties(value = {"id", "codigo", "descricao", "peso", "pesoTara", "pesoLiquido", "tipo", "textoTipo"})
    @ManyToOne
    @JoinColumn(name = "id_tipo_veiculo", referencedColumnName = "id")
    private TipoVeiculo tipoVeiculo;

    @EntityProperties(value = {"id", "pesoInicial", "pesoFinal", "motivoAlteracao", "motivoAlteracaoBruta", "statusPesagem", "pesagemBrutaManual", "pesagemFinalManual", "usuarioPesoInicial.id", "usuarioPesoInicial.nome", "usuarioPesoInicial.login",
            "usuarioPesoInicial.cpf", "usuarioPesoFinal.id", "usuarioPesoFinal.nome", "usuarioPesoFinal.login", "usuarioPesoFinal.cpf", "placaCavalo.id", "placaCavalo.tipo.id", "placaCavalo.tipo.peso"}, collecions = {"motivos",
            "historicos"})
    @ManyToOne
    @JoinColumn(name = "id_pesagem", referencedColumnName = "id")
    private Pesagem pesagem;

    @EntityProperties(value = {"id", "observacao"}, collecions = {"itens"})
    @ManyToOne
    @JoinColumn(name = "id_classificacao", referencedColumnName = "id")
    private Classificacao classificacao;

    @EntityProperties(value = {"id", "codigo", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_centro", referencedColumnName = "id")
    private Centro centro;

    @EntityProperties(value = {"id", "codigo", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_centro_destino", referencedColumnName = "id")
    private Centro centroDestino;

    @EntityProperties(value = {"id", "codigo", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_tp_contrato", referencedColumnName = "id")
    private TipoContrato tipoContrato;

    @EntityProperties(value = {"id", "codigo", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_condicao_pag", referencedColumnName = "id")
    private CondicaoPagamento condicaoPag;

    @EntityProperties(value = {"id", "codigo", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_org_compras", referencedColumnName = "id")
    private OrganizacaoCompra orgCompra;

    @EntityProperties(value = {"id", "codigo", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_gp_compradores", referencedColumnName = "id")
    private GrupoCompradores gpCompradores;

    @EntityProperties(value = {"id", "nome", "login", "cpf"})
    @ManyToOne
    @JoinColumn(name = "id_usuario_aprovador", referencedColumnName = "id")
    private Usuario usuarioAprovador;

    @EntityProperties(value = {"id", "codigo", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_iva", referencedColumnName = "id")
    private Iva iva;

    @EntityProperties(value = {"id", "ticketOrigem", "cnpjArmazem", "nomeArmazem", "dataEntradaArmazem"})
    @ManyToOne
    @JoinColumn(name = "id_romaneio_armazem_terceiros", referencedColumnName = "id")
    private RomaneioArmazemTerceiros romaneioArmazemTerceiros;

    @EntityProperties(value = {"id", "tanque", "tempTanque", "grauInpm", "acidez", "statusConversao"}, collecions = {"conversoes"})
    @ManyToOne
    @JoinColumn(name = "id_conversao_litragem", referencedColumnName = "id", unique = true)
    private ConversaoLitragem conversaoLitragem;

    @Enumerated(EnumType.STRING)
    @Column(name = "local_pesagem")
    private OrigemClassificacaoEnum localPesagem;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_frete")
    private TipoFreteEnum tipoFrete;

    @Enumerated(EnumType.STRING)
    @Column(name = "incoterms_um")
    private TipoFreteEnum incotermsUm;

    @Enumerated(EnumType.STRING)
    @Column(name = "direcao_simples_pesagem")
    private DirecaoOperacaoEnum direcaoSimplesPesagem;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_sap")
    private StatusIntegracaoSAPEnum statusIntegracaoSAP;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_romaneio")
    private StatusRomaneioEnum statusRomaneio;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_etapa")
    private StatusRomaneioEtapaEnum statusEtapa;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_pedido")
    private StatusPedidoEnum statusPedido;

    @Enumerated(EnumType.STRING)
    @Column(name = "cessao_credito")
    private CessaoCreditoEnum cessaoCredito;

    @Enumerated(EnumType.STRING)
    @Column(name = "direcao_operacao_deposito")
    private DirecaoOperacaoDepositoEnum direcaoOperacaoDeposito;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_contrato")
    private StatusContratoEnum statusContrato;

    @Enumerated(EnumType.STRING)
    @Column(name = "entidade_tipo")
    private EntidadeTipo entidadeTipo;

    @Column(name = "nr_romaneio", unique = true, updatable = true, insertable = true)
    private String numeroRomaneio;

    @Column(name = "nr_romaneio_origem")
    private String numeroRomaneioOrigem;

    @Column(columnDefinition = "text")
    private String observacao;

    @Column(name = "observacao_nota", columnDefinition = "text")
    private String observacaoNota;

    @Column(name = "json_request", columnDefinition = "text")
    private String jsonRequest;

    @Column(name = "insc_estadual")
    private String inscricaoEstadual;

    private String fazenda;

    @Column(name = "incoterms_dois")
    private String incotermsDois;

    @Column(name = "nome_simples_pesagem")
    private String nomeSimplesPesagem;

    @Column(name = "material_simples_pesagem")
    private String materialSimplesPesagem;

    @Column(name = "motorista_simples_pesagem")
    private String motoristaSimplesPesagem;

    @Column(name = "placa_cavalo_simples_pesagem")
    private String placaCavaloSimplesPesagem;

    @Column(name = "motivo_estorno", columnDefinition = "text")
    private String motivoEstorno;

    @Column(name = "numero_pedido", length = 10)
    private String numeroPedido;

    @Column(name = "item_pedido")
    private String itemPedido;

    @Column(name = "numero_contrato", length = 10)
    private String numeroContrato;

    @Column(name = "chave_acesso_nfe")
    private String chaveAcessoNfe;

    @Column(name = "numero_nfe")
    private String numeroNfe;

    @Column(name = "serie_nfe")
    private String serieNfe;

    @Column(name = "simples_entrada_referencia", length = 10)
    private String simplesEntradaReferencia;

    @Column(name = "devolucao_referencia", length = 10)
    private String devolucaoReferencia;

    @Column
    private String recebedor;

    @Column(name = "romaneio_vinculado_por_motivo")
    private String romaneioVinculadoPorMotivo;

    @Column(name = "request_key")
    @Setter(AccessLevel.NONE)
    private String requestKey;

    @Column(name = "numero_contrato_djur", length = 30)
    private String numeroContratoDjur;

    @Column(name = "numero_log_nfe")
    private String numeroLog;

    @Column(name = "numero_cartao_acesso")
    private Integer numeroCartaoAcesso;

    @Column(name = "qntd_peso_origem")
    private BigDecimal quantidadePesoOrigem;

    @Column(name = "qntd_material")
    private BigDecimal quantidadeMaterial;

    @Column(name = "unidade_preco_material")
    private BigDecimal unidadePrecoMaterial;

    @Column(name = "vlr_material")
    private BigDecimal valorMaterial;

    @Column(name = "peso_bruto_origem")
    private BigDecimal pesoBrutoOrigem;

    @Column(name = "peso_tara_origem")
    private BigDecimal pesoTaraOrigem;

    @Column(name = "desconto_origem")
    private BigDecimal descontoOrigem;

    @Column(name = "peso_liquido_umido_origem")
    private BigDecimal pesoLiquidoUmidoOrigem;

    @Column(name = "peso_liquido_seco_origem")
    private BigDecimal pesoLiquidoSecoOrigem;

    @Column(name = "preco_frete")
    private BigDecimal precoFrete;

    @Column(name = "tolerancia_kg")
    private BigDecimal toleranciaKg;

    @Column(name = "vlr_saldo_pedido")
    private BigDecimal saldoPedido;

    @Column(name = "vlr_tolerancia_pedido")
    private BigDecimal toleranciaPedido;

    @Column(name = "peso_total_nfe")
    private BigDecimal pesoTotalNfe;

    @Column(name = "valor_total_nfe")
    private BigDecimal valorTotalNfe;

    @Column(name = "valor_unitario_nfe", columnDefinition = "numeric(19,6) default 0")
    private BigDecimal valorUnitarioNfe;

    @Column(name = "veiculo_longo", columnDefinition = "boolean default false")
    private boolean veiculoLongo;

    @Column(name = "flag_enviado", columnDefinition = "boolean default false")
    private boolean romaneioEnviado;

    @Column(name = "executar_fluxo", columnDefinition = "boolean default false")
    private boolean permitirExecutarFluxo;

    @Column(name = "possui_dados_origem", columnDefinition = "boolean default false")
    private boolean possuiDadosOrigem;

    @Column(name = "romaneio_liberado_impressao_auto_atendimento", columnDefinition = "boolean default false")
    private boolean romaneioLiberadoParaImpressaoAutoAtendimento;

    @Column(name = "romaneio_ja_imprimido_autoatendimento", columnDefinition = "boolean default false")
    private boolean romaneioJaImprimidoAutoatendimento;

    @Column(name = "validacao_conferencia_peso_realizada", columnDefinition = "boolean default false")
    private boolean validacaoConferenciaPesoRealizada;

    @Column(name = "data_fixa_pagamento")
    private Boolean dataFixaPagamento = Boolean.FALSE;

    @Column(name = "copiar_dados_de_romaneios_estornados")
    private Boolean copiarDadosDeRomaneiosEstornados = false;

    @Column(name = "passou_etapa_aguardando_chamada")
    private Boolean passouEtapaAguardandoChamada = Boolean.FALSE;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dt_saida_etapa_aguardando_chamada")
    private Date dataSaidaEtapaAguardandoChamada;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dt_portaria")
    private Date dataPortaria;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dt_impressao_autoatendimento")
    private Date dtImpressaoAutoatendimento;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dt_envio_romaneio")
    private Date dataEnvio;

    @Temporal(TemporalType.DATE)
    @Column(name = "dt_pagamento")
    private Date dtPagamento;

    @Temporal(TemporalType.DATE)
    @Column(name = "dt_inicio_entrega")
    private Date dtInicioEntrega;

    @Temporal(TemporalType.DATE)
    @Column(name = "dt_final_entrega")
    private Date dtFinalEntrega;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dt_simples_entrada_referencia")
    private Date dtSimplesEntradaReferencia;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_nfe")
    private Date dataNfe;

    // Dados CTE
    @Column(name = "valor_total_cte")
    private BigDecimal valorTotalCte;

    @Column(name = "uf_emissora_cte")
    private String ufEmissoraCte;

    @Column(name = "uf_tomadora_cte")
    private String ufTomadoraCte;

    @Column(name = "pedido_cte")
    private String pedidoCte;

    @Column(name = "item_pedido_cte")
    private String itemPedidoCte;

    @Column(name = "iva_cte")
    private String ivaCte;

    @Column(name = "parceiro_cte")
    private String parceiroCte;

    @Column(name = "chave_acesso_cte")
    private String chaveAcessoCte;

    @Column(name = "numero_cte")
    private String numeroCte;

    @Column(name = "serie_cte")
    private String serieCte;

    @Column(name = "log_cte")
    private String logCte;

    @Column(name = "numero_aleatorio_cte")
    private String numeroAleatorioCte;

    @Column(name = "digito_verificador_cte")
    private String digitoVerificadorCte;

    @Column(name = "digito_verificador_nfe")
    private String digitoVerificadorNfe;

    @Column(name = "medida_nfe")
    private String medidaNfe;

    @Column(name = "numero_aleatorio_chave_acesso")
    private String numeroAleatorioChaveAcesso;

    @Column(name = "remessa_nfe")
    private String remessaNfe;

    @Column(name = "data_cte")
    private Date dataCte;

    @EntityProperties(value = {"id", "itemClassificao", "descricaoItem", "codigoTabela", "indice", "percentualDesconto", "desconto", "descontoClassificacao", "classificacao.id", "romaneio.id"}, targetEntity = ItensClassificacao.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, mappedBy = "romaneio")
    @Getter(AccessLevel.NONE)
    private List<ItensClassificacao> itensClassificacaoOrigem;

    @EntityProperties(value = {"id", "codigoLeitor", "hostLeitor", "portaLeitor", "codigoMaterial", "codigoArmazemTerceiros", "itemClassificacao", "indice", "percentualDesconto", "descricaoItemClassificacao", "indiceNumerico",
            "indicePermitidoPorPerfil"}, targetEntity = TabelaClassificacao.class)
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "tb_itens_restritos_romaneio", schema = "sap", joinColumns = {@JoinColumn(name = "id_romaneio")}, inverseJoinColumns = {@JoinColumn(name = "id_item")})
    private List<TabelaClassificacao> itensRestritosPorPerfil;

    @EntityProperties(value = {
            "id",
            "cliente.id",
            "cliente.cpfCnpj",
            "cliente.codigo",
            "cliente.nome",
            "cliente.tipoTransporte",
            "operadorLogistico.id",
            "operadorLogistico.cpfCnpj",
            "operadorLogistico.codigo",
            "operadorLogistico.nome",
            "produtor.id",
            "produtor.cpf",
            "produtor.cnpj",
            "produtor.codigo",
            "produtor.nome",
            "produtor.fazenda",
            "produtor.endFazenda",
            "produtor.bairro",
            "produtor.municipio",
            "produtor.estado",
            "produtor.cep",
            "produtor.inscricaoEstadual",
            "produtor.statusInscEstadual",
            "produtorDestino.id",
            "produtorDestino.cpf",
            "produtorDestino.cnpj",
            "produtorDestino.codigo",
            "produtorDestino.nome",
            "produtorDestino.fazenda",
            "produtorDestino.endFazenda",
            "produtorDestino.bairro",
            "produtorDestino.municipio",
            "produtorDestino.estado",
            "produtorDestino.cep",
            "produtorDestino.inscricaoEstadual",
            "produtorDestino.statusInscEstadual",
            "depositoEntrada.id",
            "depositoEntrada.cpf",
            "depositoEntrada.cnpj",
            "depositoEntrada.codigo",
            "depositoEntrada.nome",
            "depositoEntrada.fazenda",
            "depositoEntrada.endFazenda",
            "depositoEntrada.bairro",
            "depositoEntrada.municipio",
            "depositoEntrada.estado",
            "depositoEntrada.cep",
            "depositoEntrada.inscricaoEstadual",
            "depositoEntrada.statusInscEstadual",
            "depositoSaida.id",
            "depositoSaida.cpf",
            "depositoSaida.cnpj",
            "depositoSaida.codigo",
            "depositoSaida.nome",
            "depositoSaida.fazenda",
            "depositoSaida.endFazenda",
            "depositoSaida.bairro",
            "depositoSaida.municipio",
            "depositoSaida.estado",
            "depositoSaida.cep",
            "depositoSaida.inscricaoEstadual",
            "depositoSaida.statusInscEstadual",
            "safra.id",
            "safra.codigo",
            "safra.descricao",
            "material.id",
            "material.codigo",
            "material.descricao",
            "material.unidadeMedida",
            "material.valorUnitario",
            "material.realizarConversaoLitragem",
            "material.necessitaTreinamentoMotorista",
            "material.lacrarCarga",
            "material.realizarAnaliseQualidade",
            "material.buscarCertificadoQualidade",
            "material.necessitaMaisDeUmaAnalise",
            "material.possuiEtapaCarregamento",
            "material.gerarNumeroCertificado",
            "material.possuiResolucaoAnp",
            "material.exibirEspecificacaoNaImpressaoDoLaudo",
            "material.toleranciaEmKg",
            "material.tipoTransporte",
            "motorista.id",
            "motorista.codigo",
            "motorista.nome",
            "motorista.cpf",
            "placaCavalo.id",
            "placaCavalo.codigo",
            "placaCavalo.placa1",
            "placaCavalo.ufPlaca1",
            "placaCavalo.tipo.id",
            "placaCavalo.tipo.peso",
            "placaCavalo.tipo.codigo",
            "placaCavaloUm.id",
            "placaCavaloUm.codigo",
            "placaCavaloUm.placa1",
            "placaCavaloDois.id",
            "placaCavaloDois.codigo",
            "placaCavaloDois.placa1",
            "transportadora.id",
            "transportadora.codigo",
            "transportadora.descricao",
            "transportadora.cnpj",
            "transportadora.cpf",
            "transportadora.tipoTransporte",
            "transportadoraSubcontratada.id",
            "transportadoraSubcontratada.codigo",
            "transportadoraSubcontratada.descricao",
            "transportadoraSubcontratada.cnpj",
            "transportadoraSubcontratada.cpf",
            "tipoVeiculo.id",
            "tipoVeiculo.codigo",
            "tipoVeiculo.descricao",
            "tipoVeiculo.peso",
            "tipoVeiculo.pesoTara",
            "tipoVeiculo.pesoLiquido",
            "tipoVeiculo.tipo",
            "tipoVeiculo.textoTipo",
            "tipoSeta.id",
            "tipoSeta.tipoVeiculo.id",
            "tipoSeta.descricao",
            "tipoSeta.qtd",
            "tipoSeta.padrao",
            "deposito.id",
            "deposito.codigo",
            "deposito.descricao",
            "deposito.statusCadastroSap",
            "deposito.statusRegistro",
            "centroDestino.id",
            "centroDestino.codigo",
            "centroDestino.descricao",
            "romaneio.id",
            "tipoFrete",
            "statusRemessa",
            "statusDocTransporte",
            "numeroRemessa",
            "numeroOrdemTransporte",
            "codigoOrdemVenda",
            "itemOrdem",
            "numeroPedido",
            "itemPedido",
            "numeroNotaFiscalTransporte",
            "serieNotaFiscalTransporte",
            "remessaNfe",
            "chaveAcessoNfe",
            "numeroNfe",
            "serieNfe",
            "numeroAleatorioChaveAcesso",
            "numeroLog",
            "digitoVerificadorNfe",
            "armazemTerceirosReferencia",
            "empresaOrdem",
            "localExpedicao",
            "placaCarretaUm",
            "placaCarretaDois",
            "unidadeMedidaOrdem",
            "requestKey",
            "dataNfe",
            "valorUnitarioPedido",
            "valorUnitarioNfe",
            "valorTotalNfe",
            "pesoTotalNfe",
            "saldoPedido",
            "toleranciaPedido",
            "rateio",
            "quebra",
            "quantidadeCarregar",
            "quantidadePrevista",
            "saldoOrdemVenda",
            "pesoLiquidoUmido",
            "saldoEstoqueProdutoAcabado",
            "itemTransporte",
            "documentoFaturamento",
            "transportadoraBuscadaPelaOrdem",
            "transportadoraSubBuscadaPelaOrdem",}, collecions = {"placaCavalo.tipo.setas"}, targetEntity =
            ItemNFPedido.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "romaneio", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<ItemNFPedido> itens;

    @EntityProperties(value = {"id", "nfeTransporte", "nfe", "serie", "status", "requestKey", "dataCriacaoNotaMastersaf", "romaneio.id"}, targetEntity = RetornoNFe.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "romaneio", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<RetornoNFe> retornoNfes;

    @EntityProperties(value = {"id", "mensagem", "codigoOrdemVenda", "tipoRestricao", "romaneio.id"}, targetEntity = RestricaoOrdem.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, mappedBy = "romaneio", fetch = FetchType.LAZY)
    private List<RestricaoOrdem> restricoes;

    @EntityProperties(value = {"id", "dataCadastro", "idLaudo", "lacreAmostra", "loteAnaliseQualidade.id", "loteAnaliseQualidade.codigo", "loteAnaliseQualidade.dataFabricacao", "loteAnaliseQualidade.dataVencimento", "loteAnaliseQualidade" +
            ".material.id", "loteAnaliseQualidade.material.codigo", "loteAnaliseQualidade.material.descricao", "statusAnaliseQualidade", "romaneio.id", "usuarioResponsavel.id", "usuarioResponsavel.nome", "usuarioResponsavel.login",
            "usuarioResponsavel.cpf", "assinatura.id", "assinatura.codigo", "assinatura.descricao", "assinatura.dataValidade", "assinatura.assinatura", "assinatura.arquivo.id", "assinatura.arquivo.nome", "assinatura.arquivo.mimeType",
            "assinatura.arquivo.base64"}, collecions = {"itens", "motivos"}, targetEntity = AnaliseQualidade.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "romaneio", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<AnaliseQualidade> analises = new ArrayList<>();

    @EntityProperties(value = {"id", "aprovador.id", "aprovador.nome", "aprovador.login", "aprovador.cpf", "romaneio.id"}, targetEntity = RomaneioHistoricoAprovacaoIndice.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "romaneio", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<RomaneioHistoricoAprovacaoIndice> romaneioHistoricoAprovacaoIndices = new ArrayList<>();

    @EntityProperties(value = {"id", "descricao", "tipo", "conferenciaPeso", "buscarDadosRomaneio"}, targetEntity = Motivo.class)
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "tb_romaneio_motivo", schema = "sap", joinColumns = @JoinColumn(name = "id_romaneio"), inverseJoinColumns = @JoinColumn(name = "id_motivo"))
    private List<Motivo> motivos = new ArrayList<>();

    @EntityProperties(value = {"id", "nome", "mimeType", "base64"}, targetEntity = Arquivo.class)
    @ManyToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "tb_romaneio_arquivos", schema = "sap", joinColumns = @JoinColumn(name = "id_romaneio"), inverseJoinColumns = @JoinColumn(name = "id_arquivo"))
    private List<Arquivo> arquivos;

    @EntityProperties(value = {"id", "dataCadastro", "cargaPontualDataAgendamentoInicial", "cargaPontualDataAgendamentoFinal", "cargaPontualIdAgendamento", "senha", "codigoOrdemVenda", "numeroPedido", "tipoFluxo", "statusSenha", "operacao", "entidadeTipo", "numeroCartaoAcesso",
            "quantidadeCarregar", "saldoOrdemVenda", "cliente.id", "cliente.nome", "cliente.cpfCnpj", "operadorLogistico.id", "operadorLogistico.nome", "operadorLogistico.cpfCnpj", "material.id", "material.codigo", "material.descricao",
            "motorista.id", "motorista.nome", "motorista.cpf", "veiculo.id", "veiculo.placa1", "veiculo.placa2", "veiculo.placa3", "transportadora.id", "transportadora.codigo", "transportadora.descricao", "transportadora.cpf",
            "transportadora.cnpj", "etapaAtual.id", "etapaAtual.horaEntrada", "etapaAtual.statusEtapa", "etapaAtual.etapa.id", "formularioAtual.id", "formularioAtual.statusChecklist", "formularioAtual.senha.id", "formularioAtual" +
            ".formulario.id", "formularioAtual.formulario.descricao", "formularioAtual.formulario.formularioStatus", "formularioAtual.formulario.cabecalhoDadosMotorista", "formularioAtual.formulario.cabecalhoDadosVeiculo",
            "formularioAtual.formulario" + ".cabecalhoDadosCliente", "formularioAtual.formulario.cabecalhoDadosTransportadora", "seta.id", "seta.tipoVeiculo.id", "seta.descricao", "seta.qtd", "seta.padrao"}, collecions = {
            "formularioAtual.respostas"})
    @ManyToOne
    @JoinColumn(name = "id_senha", referencedColumnName = "id")
    private Senha senha;

    @EntityProperties(value = {"id", "senha", "codigoOrdemVenda", "tipoFluxo", "statusSenha", "operacao", "numeroCartaoAcesso", "quantidadeCarregar", "saldoOrdemVenda", "material.id", "material.codigo", "material.descricao",})
    @ManyToOne
    @JoinColumn(name = "id_senha_estornada", referencedColumnName = "id")
    private Senha senhaEstornada;

    @EntityProperties(value = {"id", "descricao", "tipo", "conferenciaPeso", "buscarDadosRomaneio"})
    @ManyToOne
    @JoinColumn(name = "id_motivo", referencedColumnName = "id")
    private Motivo motivo;

    @EntityProperties(value = {"id", "statusEmissaoDocumento", "documentoImpresso", "nfImpresso", "laudoAnaliseImpresso", "romaneioImpresso", "formularioImpresso"})
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "id_emissao_documento", referencedColumnName = "id")
    private EmissaoDocumento emissaoDocumento;

    @EntityProperties(value = {"id", "tanque", "temperaturaTanque", "temperaturaCarreta", "grauINPM", "massaEspecificaTemperaturaEnsaio", "massaEspecificaVinteGraus", "lacreAmostra", "certificado", "dataCarregamento",
            "dataDescarregamento", "statusCarregamento", "statusDescarregamento"})
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "id_carregamento", referencedColumnName = "id")
    private Carregamento carregamento;

    @EntityProperties(value = {"id", "identificador", "statusLocalCarregamento", "dataInicioDoCarregamento", "dataFimDoCarregamento"})
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_local_carregamento", referencedColumnName = "id")
    private LocalCarregamento localCarregamento;

    @EntityProperties(value = {"id", "statusConferencia"})
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "id_conferencia", referencedColumnName = "id")
    private Conferencia conferencia;

    @EntityProperties(value = {"id", "statusPendencia"})
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "id_pendencia", referencedColumnName = "id")
    private Pendencia pendencia;

    @EntityProperties(value = {"id", "statusLacragem"}, collecions = {"lacres"})
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_lacragem", referencedColumnName = "id")
    private Lacragem lacragem;

    @Transient
    private Date dataInicioFiltro;

    @Transient
    private Date dataFimFiltro;

    @Transient
    private boolean romaneioComPesagem;

    @Transient
    private boolean romaneioComClassificacao;

    @Transient
    private boolean romaneioComAnalise;

    @Transient
    private boolean romaneioComConversao;

    @Transient
    private boolean romaneioComLacragem;

    @Transient
    private boolean romaneioComCarregamento;

    @Transient
    private String placaFiltro;

    @Transient
    private String produtorFiltro;

    @Transient
    private String materialFiltro;

    @Transient
    private String serieNotaFiscalTransporte;

    @Transient
    private String nomeCliente;

    @Transient
    private String alerta;

    @Transient
    private String motivoLiberacao;

    @Transient
    private String botaoAcionadoConfirmacaoPeso;

    @Transient
    private int indiceEtapaRomaneio;

    @Transient
    private boolean apenasOpGestao = false;

    @Transient
    private EtapaRomaneioEnum etapaRomaneio;

    @Transient
    private Cliente filtroOperadorLogistico;

    @Transient
    private List<String> emails = new ArrayList<>();

    @Transient
    private List<ItensClassificacao> indicesBloqueados = new ArrayList<>();

    @Transient
    private List<RomaneioRequestKey> requests;

    @Transient
    private FuncionalidadeEnum funcionalidade;

    @Transient
    private LogAtividadeAcao logAtividadeAcao;

    @Transient
    private String codigoOrdemVenda;

    @Transient
    private String numeroNotaFiscal;

    public List<ItensClassificacao> getItensClassificacaoOrigem() {

        return ColecaoUtil.isNotEmpty(itensClassificacaoOrigem) ? itensClassificacaoOrigem.stream().sorted(Comparator.comparingLong(ItensClassificacao::getItemClassificacaoNumerico)).collect(Collectors.toList()) : null;
    }

    public void setRequestKey(String requestKey) {

        if (StringUtil.isNotNullEmpty(requestKey)) {
            this.requestKey = requestKey;
        }
    }

    public Romaneio() {

        super();
    }

    public Romaneio(final Long idRomaneio, final Long idPesagem, final BigDecimal pesoInicial, final BigDecimal pesoFinal, final Long idClassificacao) {

        this.setId(idRomaneio);
        this.setPesagem(new Pesagem());
        this.getPesagem().setId(idPesagem);
        this.getPesagem().setPesoInicial(pesoInicial);
        this.getPesagem().setPesoFinal(pesoFinal);
        this.setClassificacao(new Classificacao());
        this.getClassificacao().setId(idClassificacao);
    }

    public Romaneio(Long id, Long idOperacao, String numeroRomaneio, DadosSincronizacaoEnum entidade) {

        this.setId(id);
        this.setOperacao(new DadosSincronizacao());
        this.getOperacao().setId(idOperacao);
        this.setNumeroRomaneio(numeroRomaneio);
        this.getOperacao().setEntidade(entidade);
    }

    public Romaneio(Produtor produtor, Produtor armazemTerceiros, Safra safra, Safra safraPlantil, Material material, Deposito deposito, Motorista motorista, Veiculo placaCavalo, Veiculo placaCavaloUm, Veiculo placaCavaloDois,
                    Transportadora transportadora, Transportadora transportadoraSubcontratada, TipoVeiculo tipoVeiculo, Pesagem pesagem, Classificacao classificacao, Centro centro, Centro centroDestino, TipoContrato tipoContrato,
                    CondicaoPagamento condicaoPag, OrganizacaoCompra orgCompra, GrupoCompradores gpCompradores, Usuario usuarioAprovador, Iva iva, RomaneioArmazemTerceiros romaneioArmazemTerceiros, OrigemClassificacaoEnum localPesagem,
                    TipoFreteEnum tipoFrete, TipoFreteEnum incotermsUm, DirecaoOperacaoEnum direcaoSimplesPesagem, StatusPedidoEnum statusPedido, CessaoCreditoEnum cessaoCredito, String numeroRomaneio, String numeroRomaneioOrigem,
                    String observacao, String observacaoNota, String jsonRequest, String inscricaoEstadual, String fazenda, String incotermsDois, String nomeSimplesPesagem, String materialSimplesPesagem, String motoristaSimplesPesagem,
                    String placaCavaloSimplesPesagem, String motivoEstorno, String numeroPedido, String itemPedido, String numeroContrato, String numeroNfe, String serieNfe, String simplesEntradaReferencia, String devolucaoReferencia,
                    String recebedor, BigDecimal quantidadePesoOrigem, BigDecimal quantidadeMaterial, BigDecimal unidadePrecoMaterial, BigDecimal valorMaterial, BigDecimal pesoBrutoOrigem, BigDecimal pesoTaraOrigem,
                    BigDecimal descontoOrigem, BigDecimal pesoLiquidoUmidoOrigem, BigDecimal pesoLiquidoSecoOrigem, BigDecimal precoFrete, BigDecimal toleranciaKg, BigDecimal saldoPedido, boolean veiculoLongo,
                    boolean permitirExecutarFluxo, boolean possuiDadosOrigem, Date dataEnvio, Date dtPagamento, Date dtInicioEntrega, Date dtFinalEntrega, Date dtSimplesEntradaReferencia, List<ItensClassificacao> itensClassificacaoOrigem
            , List<TabelaClassificacao> itensRestritosPorPerfil, List<ItemNFPedido> itens, BigDecimal toleranciaPedido, BigDecimal valorTotalCte, String ufEmissoraCte, String ufTomadoraCte, String pedidoCte, String itemPedidoCte, String ivaCte, String parceiroCte, String chaveAcessoCte, String numeroCte, String serieCte,
                    String logCte, String numeroAleatorioCte, String digitoVerificadorCte, Date dataCte) {

        this.produtor = produtor;
        this.armazemTerceiros = armazemTerceiros;
        this.safra = safra;
        this.safraPlantil = safraPlantil;
        this.material = material;
        this.deposito = deposito;
        this.motorista = motorista;
        this.placaCavalo = placaCavalo;
        this.placaCavaloUm = placaCavaloUm;
        this.placaCavaloDois = placaCavaloDois;
        this.transportadora = transportadora;
        this.transportadoraSubcontratada = transportadoraSubcontratada;
        this.tipoVeiculo = tipoVeiculo;
        if (ObjetoUtil.isNotNull(pesagem)) {
            this.pesagem = new Pesagem(pesagem.getSafra(), pesagem.getPlacaCavalo(), pesagem.getPesoInicial(), pesagem.getPesoFinal(), pesagem.getMotivoAlteracao(), pesagem.getMotivoAlteracaoBruta(), pesagem.getStatusPesagem(), pesagem.getMotivos());
        }
        if (ObjetoUtil.isNotNull(classificacao)) {
            this.classificacao = new Classificacao(classificacao.getObservacao(), classificacao.getPlacaCavalo(), classificacao.getSafra(), classificacao.getUsuarioClassificador(), classificacao.getItens());
        }
        this.centro = centro;
        this.centroDestino = centroDestino;
        this.tipoContrato = tipoContrato;
        this.condicaoPag = condicaoPag;
        this.orgCompra = orgCompra;
        this.gpCompradores = gpCompradores;
        this.usuarioAprovador = usuarioAprovador;
        this.iva = iva;
        this.romaneioArmazemTerceiros = romaneioArmazemTerceiros;
        this.localPesagem = localPesagem;
        this.tipoFrete = tipoFrete;
        this.incotermsUm = incotermsUm;
        this.direcaoSimplesPesagem = direcaoSimplesPesagem;
        this.statusPedido = statusPedido;
        this.cessaoCredito = cessaoCredito;
        this.numeroRomaneioOrigem = numeroRomaneioOrigem;
        this.observacao = observacao;
        this.observacaoNota = observacaoNota;
        this.jsonRequest = jsonRequest;
        this.inscricaoEstadual = inscricaoEstadual;
        this.fazenda = fazenda;
        this.incotermsDois = incotermsDois;
        this.nomeSimplesPesagem = nomeSimplesPesagem;
        this.materialSimplesPesagem = materialSimplesPesagem;
        this.motoristaSimplesPesagem = motoristaSimplesPesagem;
        this.placaCavaloSimplesPesagem = placaCavaloSimplesPesagem;
        this.motivoEstorno = motivoEstorno;
        this.numeroPedido = numeroPedido;
        this.itemPedido = itemPedido;
        this.numeroContrato = numeroContrato;
        this.numeroNfe = numeroNfe;
        this.serieNfe = serieNfe;
        this.simplesEntradaReferencia = simplesEntradaReferencia;
        this.devolucaoReferencia = devolucaoReferencia;
        this.recebedor = recebedor;
        this.quantidadePesoOrigem = quantidadePesoOrigem;
        this.quantidadeMaterial = quantidadeMaterial;
        this.unidadePrecoMaterial = unidadePrecoMaterial;
        this.valorMaterial = valorMaterial;
        this.pesoBrutoOrigem = pesoBrutoOrigem;
        this.pesoTaraOrigem = pesoTaraOrigem;
        this.descontoOrigem = descontoOrigem;
        this.pesoLiquidoUmidoOrigem = pesoLiquidoUmidoOrigem;
        this.pesoLiquidoSecoOrigem = pesoLiquidoSecoOrigem;
        this.precoFrete = precoFrete;
        this.toleranciaKg = toleranciaKg;
        this.saldoPedido = saldoPedido;
        this.veiculoLongo = veiculoLongo;
        this.permitirExecutarFluxo = permitirExecutarFluxo;
        this.possuiDadosOrigem = possuiDadosOrigem;
        this.dataEnvio = dataEnvio;
        this.dtPagamento = dtPagamento;
        this.dtInicioEntrega = dtInicioEntrega;
        this.dtFinalEntrega = dtFinalEntrega;
        this.dtSimplesEntradaReferencia = dtSimplesEntradaReferencia;
        this.toleranciaPedido = toleranciaPedido;

        // Dados CTE
        this.valorTotalCte = valorTotalCte;
        this.ufEmissoraCte = ufEmissoraCte;
        this.ufTomadoraCte = ufTomadoraCte;
        this.pedidoCte = pedidoCte;
        this.itemPedidoCte = itemPedidoCte;
        this.ivaCte = ivaCte;
        this.parceiroCte = parceiroCte;
        this.chaveAcessoCte = chaveAcessoCte;
        this.numeroCte = numeroCte;
        this.serieCte = serieCte;
        this.logCte = logCte;
        this.numeroAleatorioCte = numeroAleatorioCte;
        this.digitoVerificadorCte = digitoVerificadorCte;
        this.dataCte = dataCte;

        if (ColecaoUtil.isNotEmpty(itensClassificacaoOrigem)) {
            this.itensClassificacaoOrigem = new ArrayList(itensClassificacaoOrigem.stream().map(i -> new ItensClassificacao(i.getItemClassificao(), i.getDescricaoItem(), i.getCodigoTabela(), i.getIndice(), i.getPercentualDesconto(),
                    i.getDesconto(), i.getDescontoClassificacao())).collect(Collectors.toSet()));
        }
        if (ColecaoUtil.isNotEmpty(itensRestritosPorPerfil)) {
            this.itensRestritosPorPerfil = itensRestritosPorPerfil.stream().map(i -> new TabelaClassificacao(i.getCodigoMaterial(), i.getCodigoArmazemTerceiros(), i.getItemClassificacao(), i.getIndice(), i.getPercentualDesconto(),
                    i.getDescricaoItemClassificacao(), i.getIndiceNumerico(), i.isIndicePermitidoPorPerfil(), i.getItensParaCalculoIndice())).collect(Collectors.toList());
        }
        if (ColecaoUtil.isNotEmpty(itens)) {
            this.itens = new ArrayList(itens.stream().map(i -> new ItemNFPedido(i.getDepositoEntrada(), i.getDepositoSaida(), i.getProdutor(), i.getSafra(), i.getMaterial(), i.getMotorista(), i.getPlacaCavalo(), i.getPlacaCavaloUm(),
                    i.getPlacaCavaloDois(), i.getTransportadora(), i.getTransportadoraSubcontratada(), i.getTipoVeiculo(), i.getTipoSeta(), i.getDeposito(), i.getTipoFrete(), i.getNumeroPedido(), i.getItemPedido(), i.getNumeroNotaFiscalTransporte(),
                    i.getSerieNotaFiscalTransporte(), i.getRemessaNfe(), i.getChaveAcessoNfe(), i.getNumeroNfe(), i.getSerieNfe(), i.getNumeroAleatorioChaveAcesso(), i.getNumeroLog(), i.getDigitoVerificadorNfe(), i.getArmazemTerceirosReferencia(),
                    i.getEmpresaOrdem(), i.getLocalExpedicao(), i.getPlacaCarretaUm(), i.getPlacaCarretaDois(), i.getUnidadeMedidaOrdem(), i.getDataNfe(), i.getValorUnitarioPedido(), i.getValorUnitarioNfe(), i.getValorTotalNfe(),
                    i.getPesoTotalNfe(), i.getSaldoPedido(), i.getRateio(), i.getQuebra(), i.getToleranciaPedido())).collect(Collectors.toSet()));
        }
    }

    public Romaneio(Pesagem pesagem, Centro centro, Usuario usuarioAprovador, ConversaoLitragem conversaoLitragem, String observacao, String observacaoNota, List<ItemNFPedido> itens, List<AnaliseQualidade> analises, List<Motivo> motivos,
                    Senha senha, Integer numeroCartaoAcesso, Carregamento carregamento, Lacragem lacragem, DadosSincronizacao operacao, StatusRomaneioEnum statusRomaneio, StatusRomaneioEtapaEnum statusEtapa) {

        if (ObjetoUtil.isNotNull(pesagem)) {
            this.setPesagem(new Pesagem(pesagem.getSafra(), pesagem.getPlacaCavalo(), pesagem.getPesoInicial(), pesagem.getPesoFinal(), pesagem.getMotivoAlteracao(), pesagem.getMotivoAlteracaoBruta(), pesagem.getStatusPesagem(), pesagem.getMotivos()));
        }
        this.centro = centro;
        this.senha = senha;
        this.usuarioAprovador = usuarioAprovador;
        this.observacao = observacao;
        this.observacaoNota = observacaoNota;
        this.numeroCartaoAcesso = numeroCartaoAcesso;
        this.operacao = operacao;
        this.statusRomaneio = statusRomaneio;
        this.statusEtapa = statusEtapa;
        if (ObjetoUtil.isNotNull(conversaoLitragem)) {
            this.conversaoLitragem = new ConversaoLitragem(conversaoLitragem.getTanque(), conversaoLitragem.getTempTanque(), conversaoLitragem.getGrauInpm(), conversaoLitragem.getAcidez(), conversaoLitragem.getConversoes());
        }
        if (ObjetoUtil.isNotNull(carregamento)) {
            this.carregamento = new Carregamento(carregamento.getTanque(), carregamento.getTemperaturaTanque(), carregamento.getTemperaturaCarreta(), carregamento.getGrauINPM(), carregamento.getMassaEspecificaTemperaturaEnsaio(),
                    carregamento.getMassaEspecificaVinteGraus(), carregamento.getLacreAmostra(), carregamento.getCertificado(), carregamento.getDataCarregamento(), carregamento.getDataDescarregamento(),
                    carregamento.getStatusCarregamento(), carregamento.getStatusDescarregamento(), carregamento.getMotivos());
        }
        if (ObjetoUtil.isNotNull(lacragem) && ColecaoUtil.isNotEmpty(lacragem.getLacres())) {
            this.lacragem = new Lacragem(lacragem.getStatusLacragem(), lacragem.getLacres(), lacragem.getMotivos());
        }
        if (ColecaoUtil.isNotEmpty(analises)) {
            this.analises = analises.stream().map(a -> new AnaliseQualidade(a.getIdLaudo(), a.getLacreAmostra(), a.getLoteAnaliseQualidade(), a.getStatusAnaliseQualidade(), a.getUsuarioResponsavel(), a.getAssinatura(),
                    a.getAssinaturaStream(), a.getItens(), a.getMotivos())).collect(Collectors.toList());
        }
        if (ColecaoUtil.isNotEmpty(motivos)) {
            this.motivos = motivos.stream().map(m -> new Motivo(m.getDescricao(), m.getTipo())).collect(Collectors.toList());
        }
        if (ColecaoUtil.isNotEmpty(itens)) {
            this.itens = new ArrayList(itens.stream().map(i -> new ItemNFPedido(i.getSafra(), i.getMaterial(), i.getMotorista(), i.getPlacaCavalo(), i.getPlacaCavaloUm(), i.getPlacaCavaloDois(), i.getTransportadora(),
                    i.getTransportadoraSubcontratada(), i.getTipoVeiculo(), i.getTipoSeta(), i.getDeposito(), i.getTipoFrete(), i.getCodigoOrdemVenda(), i.getItemOrdem(), i.getNumeroPedido(), i.getItemPedido(), i.getEmpresaOrdem(),
                    i.getLocalExpedicao(), i.getPlacaCarretaUm(), i.getPlacaCarretaDois(), i.getUnidadeMedidaOrdem(), i.getDataNfe(), i.getRateio(), i.getQuebra(), i.getQuantidadeCarregar(), i.getSaldoOrdemVenda(),
                    i.getQuantidadeCarregar(), i.getSaldoEstoqueProdutoAcabado(), i.getCliente())).collect(Collectors.toSet()));
        }
    }

    public String getStatusRomaneioDescricao() {

        return this.statusRomaneio != null ? this.statusRomaneio.getDescricao() : "";
    }

    public BigDecimal getTolerancia() {

        if (ObjetoUtil.isNotNull(this.getQuantidadeMaterial()) && ObjetoUtil.isNotNull(this.getToleranciaKg())) {
            return (this.getToleranciaKg().multiply(new BigDecimal(100))).divide(this.getQuantidadeMaterial(), 2, BigDecimal.ROUND_HALF_DOWN);
        }
        return BigDecimal.ZERO;
    }

    public Romaneio newReferenceToDevolucao(String numero, DadosSincronizacao dadosSincronizacao) {

        this.setId(null);

        this.setStatusRomaneio(StatusRomaneioEnum.EM_PROCESSAMENTO);

        this.setStatusIntegracaoSAP(null);

        this.setDevolucaoReferencia(numero);

        this.setOperacao(dadosSincronizacao);

        if (ColecaoUtil.isNotEmpty(this.getRetornoNfes())) {
            this.setRetornoNfes(null);
        }

        if (ColecaoUtil.isNotEmpty(this.getItensClassificacaoOrigem())) {

            this.getItensClassificacaoOrigem().forEach(item -> item.setId(null));
        }

        if (ColecaoUtil.isNotEmpty(this.getItensRestritosPorPerfil())) {
            this.getItensRestritosPorPerfil().forEach(item -> item.setId(null));
        }

        if (ObjetoUtil.isNotNull(this.getPesagem())) {

            this.getPesagem().setId(null);
        }

        this.setClassificacao(null);

        if (ColecaoUtil.isNotEmpty(this.getItens())) {

            this.getItens().forEach(item -> item.setId(null));
        }

        return this;
    }

    public Boolean getCopiarDadosDeRomaneiosEstornados() {

        if (ObjetoUtil.isNull(copiarDadosDeRomaneiosEstornados)) {
            copiarDadosDeRomaneiosEstornados = false;
        }

        return copiarDadosDeRomaneiosEstornados;
    }

    @PreUpdate
    public void preAlterar() {

        super.preAlterar();

        if (ObjetoUtil.isNotNull(this.getStatusIntegracaoSAP())) {

            this.setStatusRomaneio(StatusRomaneioEnum.CONCLUIDO);
        }
    }

    @JsonIgnore
    public Veiculo getVeiculoDoRomaneio() {

        if (this.getOperacao().isPossuiDivisaoCarga()) {

            if (ColecaoUtil.isNotEmpty(this.getItens())) {

                return this.getItens().stream().map(ItemNFPedido::getPlacaCavalo).findFirst().orElse(null);
            }
        }

        return this.getPlacaCavalo();
    }

    @JsonIgnore
    public Transportadora getTransportadoraDoRomaneio() {

        if (this.getOperacao().isPossuiDivisaoCarga()) {

            if (ColecaoUtil.isNotEmpty(this.getItens())) {

                return this.getItens().stream().map(ItemNFPedido::getTransportadora).findFirst().orElse(null);
            }
        }

        return this.getTransportadora();
    }

    @JsonIgnore
    public Material getMaterialDoRomaneio() {

        if (this.getOperacao().isPossuiDivisaoCarga()) {

            if (ColecaoUtil.isNotEmpty(this.getItens())) {

                return this.getItens().stream().map(ItemNFPedido::getMaterial).findFirst().orElse(null);
            }
        }

        return this.getMaterial();
    }

    @JsonIgnore
    public Motorista getMotoristaDoRomaneio() {

        if (this.getOperacao().isPossuiDivisaoCarga()) {

            if (ColecaoUtil.isNotEmpty(this.getItens())) {

                return this.getItens().stream().map(ItemNFPedido::getMotorista).findFirst().orElse(null);
            }
        }

        return this.getMotorista();
    }

}
