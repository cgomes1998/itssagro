package br.com.sjc.modelo.response;

import br.com.sjc.modelo.sap.Centro;
import br.com.sjc.modelo.sap.Deposito;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.modelo.sap.Safra;
import br.com.sjc.modelo.sap.response.RfcObterNfeTransferenciaHeaderResponse;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
public class DadosNfeTransferenciaResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private String nnf;
    private String serie;
    private String dataNfe;
    private String nprot;
    private String dig;
    private String nrAle;
    private BigDecimal vnf;
    private String status;
    private String chave;
    private String remessa;
    private String medida;
    private String docStatus;
    private boolean remessaApta;
    private BigDecimal quantidade;
    private Safra safra;
    private Deposito deposito;
    private Material material;
    private Centro centro;
}
