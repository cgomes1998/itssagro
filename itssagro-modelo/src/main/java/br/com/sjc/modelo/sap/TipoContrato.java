package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.EntidadeGenerica;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_tipo_contrato", schema = "sap")
public class TipoContrato extends EntidadeGenerica {

    @Column
    private String codigo;

    @Column
    private String descricao;

    @Override
    public String toString() {

        return this.codigo + " - " + this.descricao;
    }

}
