package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OperadorEnum {

    ABRE_PARENTESES("(", "Abre Parênteses"),
    FECHA_PARENTESES(")", "Fecha Parênteses"),
    E("&&", "E"),
    OU("||", "Ou");

    private String operador;

    private String descricao;

}
