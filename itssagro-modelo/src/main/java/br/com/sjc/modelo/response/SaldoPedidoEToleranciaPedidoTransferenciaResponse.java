package br.com.sjc.modelo.response;

import br.com.sjc.modelo.enums.OrigemClassificacaoEnum;
import br.com.sjc.modelo.sap.*;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
public class SaldoPedidoEToleranciaPedidoTransferenciaResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private String unidadeMedida;
    private BigDecimal saldoPedido;
    private BigDecimal valorUnitarioPedido;
    private BigDecimal toleranciaPedido;
    private BigDecimal saldoReservado;
    private BigDecimal saldoDisponivel;
    private OrigemClassificacaoEnum localPesagem;
    private Produtor produtor;
    private Safra safra;
    private Centro centro;
    private Centro centroLogadoPermitido;
    private Deposito deposito;
    private Material material;
}
