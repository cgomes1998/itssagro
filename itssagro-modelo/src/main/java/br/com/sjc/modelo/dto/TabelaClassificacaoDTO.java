package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.ObjectID;
import br.com.sjc.modelo.sap.TabelaClassificacao;
import br.com.sjc.util.anotation.ProjectionConfigurationDTO;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ProjectionConfigurationDTO(groupProperties = true)
public class TabelaClassificacaoDTO implements ObjectID<Long> {

    private Long id;

    @ProjectionProperty(values = {"chave"}, useForCountTotal = true)
    private String chave;

    @ProjectionProperty
    private String descricaoItemClassificacao;

    @ProjectionProperty
    private String codigoMaterial;

    @ProjectionProperty
    private String itemClassificacao;

    private String codigo;

    private String codigoArmazemTerceiros;

    private String codigoLeitor;

    private String hostLeitor;

    private String portaLeitor;

    private List<TabelaClassificacaoItemDTO> itens = new ArrayList();

    private Set<String> itensParaCalculoIndice = new HashSet<>();

    public TabelaClassificacaoDTO(final List<TabelaClassificacao> entidades) {

        entidades.forEach(e -> this.itens.add(new TabelaClassificacaoItemDTO(e.getId(), e.getIndice(), e.getPercentualDesconto(), e.getIndiceNumerico(), e.isIndicePermitidoPorPerfil())));

        TabelaClassificacao entidade = entidades.get(0);

        setId(entidade.getId());

        setCodigo(entidade.getCodigo());

        setDescricaoItemClassificacao(entidade.getDescricaoItemClassificacao());

        setCodigoArmazemTerceiros(entidade.getCodigoArmazemTerceiros());

        setCodigoMaterial(entidade.getCodigoMaterial());

        setCodigoLeitor(entidade.getCodigoLeitor());

        setHostLeitor(entidade.getHostLeitor());

        setPortaLeitor(entidade.getPortaLeitor());

        setItemClassificacao(entidade.getItemClassificacao());

        setItensParaCalculoIndice(entidade.getItensParaCalculoIndice());
    }

    public List<TabelaClassificacaoItemDTO> getItens() {

        return itens;
    }

    public void setItens(List<TabelaClassificacaoItemDTO> itens) {

        this.itens = itens;
    }

}
