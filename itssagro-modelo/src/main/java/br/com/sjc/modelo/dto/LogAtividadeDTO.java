package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.LogAtividadeAcao;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LogAtividadeDTO {

    private Date dataCadastro;

    private LogAtividadeAcao logAtividadeAcao;

    private String usuarioAutor;

}
