package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum StatusAutomacaoEnum {

    /**
     * Gerado pelo ERP para informar que a placa estará liberada para realizar pesagem.
     */
    PLACA_LIBERADA_PARA_PESAGEM(1),

    /**
     * Gerado pela Conceitto para informar que o veículo entrou nas dependências da unidade.
     */
    VEICULO_DENTRO_UNIDADE(2),

    /**
     * Gerado pela Conceitto quando realizado a primeira pesagem, será preenchido as colunas.
     */
    REALIZADO_PRIMEIRA_PESAGEM(3),

    /**
     * Gerado pelo ERP quando o peso for aceito pelas regras de entrada do produto, caso haja necessidade de enviar
     * mensagem para o motorista deverá ser preenchida as colunas Mensagem_1 a 6.
     */
    PESAGEM_ACEITA_VALIDACAO_CARGA(4),

    /**
     * Gerado pelo ERP quando houver qualquer tipo de inconsistência que impeça o veículo de seguir o processo ou deva
     * descarregar em outra unidade, recomenda-se o preenchimento dos campos de mensagem para melhor entendimento do
     * motorista.
     */
    INCONSISTENCIA(5),

    /**
     * Gerado pela Conceitto quando realizado a segunda pesagem.
     */
    REALIZADO_SEGUNDA_PESAGEM(6),

    /**
     * Gerado pelo ERP quando o peso for aceito pelas regras de validação da carga, caso haja necessidade de enviar
     * mensagem para o motorista poderão ser utilizados os campos de mensagens.
     */
    VALIDACAO_CARGA_ACEITA(7),

    /**
     * Gerado pelo ERP quando o peso for negado pelas regras de validação da carga.
     */
    PESAGEM_NEGADA_VALIDACAO_CARGA(8),

    /**
     * Gerado pela conceito para caminhão sair da balança e aguardar o faturamento
     */
    SAIR_DA_BALANCA_E_AGUARDAR_OK_DO_FATURAMENTO(73),

    /**
     * Gerado pelo ERP se faturamento ok
     */
    PESAGEM_2_OK_FINALIZAR_PROCESSO(74),

    /**
     * Gerado pelo ERP se houver inconsistencia na pesagem
     */
    PESAGEM_COM_INCONSISTENCIA_REPESAR_VEICULO(75),

    /**
     * Status de limpeza de dados na tabela, caso haja algum problema físico que cause o não encerramento de pesagem
     * pela automação.
     */
    LIMPESA_DADOS_TABELA(98),

    /**
     * Cancelamento da Rota.
     */
    CANCELAMENTO_ROTA(99);

    private int codigo;

    public static StatusAutomacaoEnum valueOf(int codigo) {
        return Arrays.stream(StatusAutomacaoEnum.values()).filter(item -> item.getCodigo() == codigo).findFirst().orElse(null);
    }

}
