package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.AnaliseItem;
import br.com.sjc.modelo.EntidadeGenerica;
import br.com.sjc.modelo.LaudoItem;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_analise_qualidade_item", schema = "sap")
public class AnaliseQualidadeItem extends EntidadeGenerica {

    @EntityProperties(value = {
            "id",
            "analise.id",
            "analise.codigo",
            "analise.material.id",
            "analise.material.codigo",
            "analise.material.descricao",
            "ordem",
            "grausInpm",
            "caracteristica",
            "metodologia",
            "tipoValidacao",
            "especificacao",
            "unidade",
            "subCaracteristica",
            "condicao",
            "valorInicial",
            "valorFinal",
            "laboratorio",
            "boletim"
    })
    @ManyToOne
    @JoinColumn(name = "id_analise_item", referencedColumnName = "id")
    private AnaliseItem analiseItem;

    @Column(name = "resultado")
    private String resultado;

    @Transient
    private Long index;

    public AnaliseQualidadeItem() {

        super();
    }

    public AnaliseQualidadeItem(LaudoItem item) {

        this.setIndex(ObjetoUtil.isNotNull(item.getIndex()) ? item.getIndex() : new Long(0));

        this.setAnaliseItem(new AnaliseItem(item));

        this.setResultado(item.getResultado());
   }

    public AnaliseQualidadeItem(AnaliseItem analiseItem, String resultado) {
        this.analiseItem = analiseItem;
        this.resultado = resultado;
    }
}
