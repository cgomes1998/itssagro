package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.EntidadeGenerica;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.request.item.EventInRequestItem;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@SAPParameter(function = "/ITSSAGRO/FM_EVENT_IN")
public class EventInRequest extends JCoRequest {

    @SAPColumn(name = "OPERA")
    private String operacao;

    @SAPColumn(name = "MODLO")
    private String modulo;

    @SAPColumn(name = "EVENT")
    private String evento;

    @SAPColumn(name = "WERKS")
    private String centro;

    @SAPColumn(name = "INTSY")
    private String programID;

    @SAPColumn(name = "INTPR")
    private String numeroRomaneio;

    @SAPColumn(name = "SINCR")
    private String sincrono;

    @SAPTable(name = "MSGDATA", item = EventInRequestItem.class)
    private List<EventInRequestItem> data;

    private List<? extends EntidadeGenerica> entidades;

    private List<String> requestkeys;

    @Override
    public int getTableRows() {

        return this.getData().size();
    }

    @Override
    public List<EventInRequestItem> getTableItens() {

        return this.getData();
    }

    public static EventInRequest instance(final DadosSincronizacao dadosSincronizacao, final String centro, final String programID, final List<EventInRequestItem> data, final boolean sincrono, final List<? extends EntidadeGenerica> entidades) {

        final EventInRequest instance = new EventInRequest();

        instance.setModulo(dadosSincronizacao.getModulo().getCodigo());

        instance.setOperacao(dadosSincronizacao.getOperacao());

        instance.setCentro(centro);

        instance.setEvento(dadosSincronizacao.getEventoEntrada());

        instance.setProgramID(programID);

        instance.setData(data);

        instance.setSincrono(sincrono ? "X" : null);

        instance.setEntidades(entidades);

        return instance;
    }
}
