/**
 * UrnServidorLocator.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.sjc.modelo.soap.automacao;

public class UrnServidorLocator extends org.apache.axis.client.Service implements UrnServidor {

    // Use to get a proxy class for UrnServidorPort
    private String UrnServidorPort_address = "http://172.31.0.46:8090/ws/server/servidor.php";
    // The WSDD service name defaults to the port name.
    private String UrnServidorPortWSDDServiceName = "urn:ServidorPort";
    private java.util.HashSet ports = null;

    public UrnServidorLocator() {
    }

    public UrnServidorLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public UrnServidorLocator(String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    public String getUrnServidorPortAddress() {
        return UrnServidorPort_address;
    }

    public String getUrnServidorPortWSDDServiceName() {
        return UrnServidorPortWSDDServiceName;
    }

    public void setUrnServidorPortWSDDServiceName(String name) {
        UrnServidorPortWSDDServiceName = name;
    }

    public UrnServidorPortType getUrnServidorPort() throws javax.xml.rpc.ServiceException {
        java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(UrnServidorPort_address);
        } catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getUrnServidorPort(endpoint);
    }

    public UrnServidorPortType getUrnServidorPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            UrnServidorBindingStub _stub = new UrnServidorBindingStub(portAddress, this);
            _stub.setPortName(getUrnServidorPortWSDDServiceName());
            return _stub;
        } catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setUrnServidorPortEndpointAddress(String address) {
        UrnServidorPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (UrnServidorPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                UrnServidorBindingStub _stub = new UrnServidorBindingStub(new java.net.URL(UrnServidorPort_address), this);
                _stub.setPortName(getUrnServidorPortWSDDServiceName());
                return _stub;
            }
        } catch (Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        String inputPortName = portName.getLocalPart();
        if ("urn:ServidorPort".equals(inputPortName)) {
            return getUrnServidorPort();
        } else {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://172.31.0.46/soap/urn:Servidor", "urn:Servidor");
    }

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://172.31.0.46/soap/urn:Servidor", "urn:ServidorPort"));
        }
        return ports.iterator();
    }

    /**
     * Set the endpoint address for the specified port name.
     */
    public void setEndpointAddress(String portName, String address) throws javax.xml.rpc.ServiceException {

        if ("UrnServidorPort".equals(portName)) {
            setUrnServidorPortEndpointAddress(address);
        } else { // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
     * Set the endpoint address for the specified port name.
     */
    public void setEndpointAddress(javax.xml.namespace.QName portName, String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
