package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_evento_automacao_etapa")
public class EventoAutomacaoGraosEtapa extends EntidadeAutenticada {

    @EntityProperties(value = {"id", "descricao"})
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_evento_automacao_graos", nullable = false)
    @JsonIgnore
    private EventoAutomacaoGraos eventoAutomacaoGraos;

    @Column
    private Integer sequencia;

    @Column(name = "status_estorno_automacao")
    private Integer statusEstornoAutomacao;

    @Column(name = "mensagem_estorno_automacao")
    private String mensagemEstornoAutomacao;

    @Enumerated(EnumType.STRING)
    @Column
    private EtapaEnum etapa;

    @EntityProperties(value = {"id", "etapaCriterio.id", "eventoAutomacaoGraosEtapa.id", "local.id", "local.codigo", "local.codigoFormatado", "local.descricao", "hardware.id", "hardware.codigo", "hardware.codigoFormatado", "hardware" +
            ".descricao", "hardware.endereco", "hardware.identificacao", "hardware.porta", "numeroEvento", "evento", "statusAutomacao", "exibirMensagem", "criterioTipo", "descricaoStatus", "mensagemUm", "mensagemDois", "mensagemTres"},
            targetEntity = EtapaCriterioEvento.class)
    @OrderBy("id")
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "eventoAutomacaoGraosEtapa", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<EtapaCriterioEvento> eventos = new ArrayList<>();

}
