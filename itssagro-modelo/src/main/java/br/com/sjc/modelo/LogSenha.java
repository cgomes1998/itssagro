package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.StatusSenhaEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_log_senha", schema = "public")
public class LogSenha extends EntidadeAutenticada {

    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name = "id_senha")
    private Senha senha;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_senha")
    private StatusSenhaEnum statusSenha;

}
