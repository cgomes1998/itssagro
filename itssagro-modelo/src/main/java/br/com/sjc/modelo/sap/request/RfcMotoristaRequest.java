package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPParameter(function = "ZITSSAGRO_FM_DM_MOTORISTA")
public class RfcMotoristaRequest extends JCoRequest {

    @SAPColumn(name = "DT_ULT_INT")
    private String dataUltimaIntegracao;

    @SAPColumn(name = "PRE_CAD")
    private String preCadastro;

    @SAPColumn(name = "COD_MOT")
    private String codigo;
}
