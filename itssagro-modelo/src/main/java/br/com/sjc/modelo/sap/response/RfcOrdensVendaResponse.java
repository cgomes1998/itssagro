package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.response.item.RfcOrdensVendaItemResponse;
import br.com.sjc.modelo.sap.response.item.RfcOrdensVendaItemTxtResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@SAPParameter(function = "ZITSSAGRO_SALESORDER_GETLIST")
public class RfcOrdensVendaResponse extends JCoResponse {

    @SAPTable(name = "SALES_ORDERS", item = RfcOrdensVendaItemResponse.class)
    private List<RfcOrdensVendaItemResponse> ordens;

    @SAPTable(name = "ORDER_TEXTS", item = RfcOrdensVendaItemTxtResponse.class)
    private List<RfcOrdensVendaItemTxtResponse> textos;

}
