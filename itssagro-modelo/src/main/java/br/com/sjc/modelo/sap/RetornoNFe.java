package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.EntidadeGenerica;
import br.com.sjc.modelo.enums.OrigemClassificacaoEnum;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_retorno_nfe", schema = "sap")
public class RetornoNFe extends EntidadeGenerica {

    @Column(name = "nfe_transporte")
    private String nfeTransporte;

    @Column
    private String nfe;

    @Column
    private String serie;

    @Column
    private String status;

    @Column(name = "request_key")
    private String requestKey;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_criacao_nota_mastersaf")
    private Date dataCriacaoNotaMastersaf;

    @EntityProperties(value = {"id"})
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_romaneio")
    private Romaneio romaneio;

    @Transient
    private String itemDocumento;

    public RetornoNFe() {
    }
    
    @Builder
    public RetornoNFe(String nfeTransporte, String nfe, String serie, String status, String requestKey, Date dataCriacaoNotaMastersaf, Long idRomaneio) {
        
        this.nfeTransporte = nfeTransporte;
        this.nfe = nfe;
        this.serie = serie;
        this.status = status;
        this.requestKey = requestKey;
        this.dataCriacaoNotaMastersaf = dataCriacaoNotaMastersaf;
        if (Objects.nonNull(idRomaneio)) {
            this.setRomaneio(new Romaneio());
            this.getRomaneio().setId(idRomaneio);
        }
    }
    
    public RetornoNFe(
            final Long id,
            final String nfe,
            final String nfeTransporte
    ) {
        this.setId(id);
        this.setNfe(nfe);
        this.setNfeTransporte(nfeTransporte);
    }

    public RetornoNFe(String nfe, Date dataCriacaoNotaMastersaf, String romaneio) {
        this.nfe = nfe;
        this.dataCriacaoNotaMastersaf = dataCriacaoNotaMastersaf;
        this.romaneio = new Romaneio();
        this.romaneio.setNumeroRomaneio(romaneio);
    }

    public String getKey() {
        return StringUtil.isNotNullEmpty(this.nfe) ? this.nfe : this.nfeTransporte;
    }

    public String getNumeroAndSerieConcat() {
        List<String> valores = new ArrayList<>();
        if (StringUtil.isNotNullEmpty(this.nfe)) {
            valores.add(this.nfe);
        } else if (StringUtil.isNotNullEmpty(this.nfeTransporte)) {
            valores.add(this.nfeTransporte);
        }
        valores.add(this.serie);
        return valores.stream().filter(StringUtil::isNotNullEmpty).collect(Collectors.joining(" - "));
    }

    public RetornoNFe toRetornoSAP(final RetornoNFe retorno) {

        if (StringUtil.isNotNullEmpty(retorno.getNfeTransporte())) {

            this.setNfeTransporte(retorno.getNfeTransporte());
        }

        if (StringUtil.isNotNullEmpty(retorno.getNfe())) {

            this.setNfe(retorno.getNfe());
        }

        if (StringUtil.isNotNullEmpty(retorno.getStatus())) {

            this.setStatus(retorno.getStatus());
        }

        if (StringUtil.isNotNullEmpty(retorno.getSerie())) {

            this.setSerie(retorno.getSerie());
        }

        if (StringUtil.isNotNullEmpty(retorno.getItemDocumento())) {

            this.setItemDocumento(retorno.getItemDocumento());
        }

        return this;
    }

    public Romaneio toRomaneio() {

        Romaneio newEntidade = new Romaneio();

        newEntidade.setSerieNotaFiscalTransporte(this.getSerie());

        newEntidade.setProdutor(this.getRomaneio().getProdutor());

        newEntidade.setSafra(this.getRomaneio().getSafra());

        newEntidade.setMaterial(this.getRomaneio().getMaterial());

        newEntidade.setMotorista(this.getRomaneio().getMotorista());

        newEntidade.setPlacaCavalo(this.getRomaneio().getPlacaCavalo());

        newEntidade.setTransportadora(this.getRomaneio().getTransportadora());

        newEntidade.setTipoVeiculo(this.getRomaneio().getTipoVeiculo());

        newEntidade.setTipoFrete(this.getRomaneio().getTipoFrete());

        if (OrigemClassificacaoEnum.ORIGEM.equals(this.getRomaneio().getLocalPesagem())) {

            newEntidade.setPossuiDadosOrigem(true);

            newEntidade.setQuantidadePesoOrigem(this.getRomaneio().getQuantidadePesoOrigem());
        }

        return newEntidade;
    }

    public boolean isNotNull() {

        return StringUtil.isNotNullEmpty(this.getNfe()) || StringUtil.isNotNullEmpty(this.getNfeTransporte());
    }
    
    @Override
    public String toString() {
        
        return "RetornoNFe{" + "nfeTransporte='" + nfeTransporte + '\'' + ", nfe='" + nfe + '\'' + ", serie='" + serie + '\'' + ", status='" + status + '\'' + ", requestKey='" + requestKey + '\'' + ", itemDocumento='" + itemDocumento + '\'' + '}';
    }
    
    public String queryInsert() {
        
        StringBuilder stringBuilder = new StringBuilder();
        
        stringBuilder.append("insert\r\n");
        stringBuilder.append("	into\r\n");
        stringBuilder.append("	sap.tb_retorno_nfe (\r\n");
        stringBuilder.append("	dt_cadastro,\r\n");
        stringBuilder.append("	nfe,\r\n");
        stringBuilder.append("	nfe_transporte,\r\n");
        stringBuilder.append("	serie,\r\n");
        stringBuilder.append("	status,\r\n");
        stringBuilder.append("	id_romaneio,\r\n");
        stringBuilder.append("	data_criacao_nota_mastersaf,\r\n");
        stringBuilder.append("	request_key\r\n");
        stringBuilder.append("	)\r\n");
        stringBuilder.append("values(\r\n");
        stringBuilder.append("'" + DateUtil.format("yyyy-MM-dd HH:mm:ss", this.getDataCadastro()) + "',\r\n");
        stringBuilder.append("'" + this.nfe + "',\r\n");
        stringBuilder.append("'" + this.nfeTransporte + "',\r\n");
        stringBuilder.append("'" + this.serie + "',\r\n");
        stringBuilder.append("'" + this.status + "',\r\n");
        stringBuilder.append("" + this.romaneio.getId() + ",\r\n");
        stringBuilder.append("'" + DateUtil.format("yyyy-MM-dd HH:mm:ss", this.dataCriacaoNotaMastersaf) + "',\r\n");
        stringBuilder.append("'" + this.requestKey + "'\r\n");
        stringBuilder.append(");\r\n");
        
        return stringBuilder.toString().replaceAll("'null'", "null").replaceAll("''", "null");
    }
    
    public String queryUpdate() {
        
        StringBuilder stringBuilder = new StringBuilder();
        
        stringBuilder.append("update\r\n");
        stringBuilder.append("	sap.tb_retorno_nfe\r\n");
        stringBuilder.append("set\r\n");
        stringBuilder.append("	nfe = '" + this.nfe + "',\r\n");
        stringBuilder.append("	nfe_transporte = '" + this.nfeTransporte + "',\r\n");
        stringBuilder.append("	serie = '" + this.serie + "',\r\n");
        stringBuilder.append("	status = '" + this.status + "',\r\n");
        stringBuilder.append("	id_romaneio = " + this.romaneio.getId() + ",\r\n");
        stringBuilder.append("	data_criacao_nota_mastersaf = '" + DateUtil.format("yyyy-MM-dd HH:mm:ss", this.dataCriacaoNotaMastersaf) + "',\r\n");
        stringBuilder.append("	request_key = '" + this.requestKey + "',\r\n");
        stringBuilder.append("	dt_alteracao = '" + DateUtil.format("yyyy-MM-dd HH:mm:ss", DateUtil.hoje()) + "'\r\n");
        stringBuilder.append("where\r\n");
        stringBuilder.append("	id = "+this.getId()+";\r\n");
        
        return stringBuilder.toString().replaceAll("'null'", "null").replaceAll("''", "null");
    }
}
