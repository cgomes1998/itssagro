package br.com.sjc.modelo.sap.sync;

import br.com.sjc.modelo.sap.TabelaClassificacao;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SAPTabelaClassificacao extends SAPEntidade {

    @SAPColumn(name = "C_TABELA")
    private String codigoTabela;

    @SAPColumn(name = "C_MATER")
    private String codigoMaterial;

    @SAPColumn(name = "C_ITEM")
    private String itemClassificacao;

    @SAPColumn(name = "C_INDICE")
    private String indice;

    @SAPColumn(name = "C_PERCD")
    private String percentualDesconso;

    @SAPColumn(name = "C_DESCITEM")
    private String descricaoItemClassificacao;

    public enum COLUMN_INPUT {

        DT_ULT_INF;
    }

    public static SAPTabelaClassificacao newInstance() {

        return new SAPTabelaClassificacao();
    }

    public TabelaClassificacao copy(TabelaClassificacao entidadeSaved) {

        if (ObjetoUtil.isNull(entidadeSaved)) {

            entidadeSaved = new TabelaClassificacao();

        } else {

            entidadeSaved.setId(entidadeSaved.getId());
        }

        entidadeSaved.setCodigo(StringUtil.isNotNullEmpty(this.getCodigoTabela()) ? this.getCodigoTabela().trim() : null);

        entidadeSaved.setCodigoMaterial(StringUtil.isNotNullEmpty(this.getCodigoMaterial()) ? this.getCodigoMaterial().trim() : null);

        entidadeSaved.setDescricaoItemClassificacao(StringUtil.isNotNullEmpty(this.getDescricaoItemClassificacao()) ? this.getDescricaoItemClassificacao().trim() : null);

        entidadeSaved.setIndice(StringUtil.isNotNullEmpty(this.getIndice()) ? this.getIndice().trim() : null);

        entidadeSaved.setItemClassificacao(StringUtil.isNotNullEmpty(this.getItemClassificacao()) ? this.getItemClassificacao().trim() : null);

        entidadeSaved.setPercentualDesconto(StringUtil.isNotNullEmpty(this.getPercentualDesconso()) ? this.getPercentualDesconso().trim() : null);

        entidadeSaved.setIndiceNumerico(StringUtil.isNotNullEmpty(this.getIndice()) ? Double.valueOf(this.getIndice().trim()) : null);

        entidadeSaved.setIndicePermitidoPorPerfil(entidadeSaved.isIndicePermitidoPorPerfil());

        return entidadeSaved;
    }
}
