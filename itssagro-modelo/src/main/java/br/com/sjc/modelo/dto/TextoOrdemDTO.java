package br.com.sjc.modelo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TextoOrdemDTO {

    private String ordem;

    private String texto;
}
