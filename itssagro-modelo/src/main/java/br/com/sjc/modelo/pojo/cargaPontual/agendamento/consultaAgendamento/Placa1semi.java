package br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Placa1semi {

    private String placa;

    private String cidade;

    private String uf;

    private String validadeCIV;

    private String validadeCIPP;

    private String validadeCR;

    private String validadeAATIPP;

    private String validadeCalibragem;

    private String validadeAET;

    private String validadeLicencaAmbiental;

    private String validadeCRLV;

    private Compartimentoveiculo compartimentoveiculo;

    private Compartimentoveiculoagendamento compartimentoveiculoagendamento;

}
