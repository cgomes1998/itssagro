package br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Produto {

    private String natureza;

    private String produto;

    private String unidadeMedida;

    private String quantidadeAgendada;

}
