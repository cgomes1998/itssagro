package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.EntidadeGenerica;
import br.com.sjc.modelo.enums.TipoRestricaoEnum;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_restricao_ordem", schema = "sap")
@NoArgsConstructor
@AllArgsConstructor
public class RestricaoOrdem extends EntidadeGenerica {

    @Lob
    @Type(type = "text")
    private String mensagem;

    @Column(name = "codigo_ordem_venda")
    private String codigoOrdemVenda;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_restricao")
    private TipoRestricaoEnum tipoRestricao;

    @EntityProperties(value = {"id"})
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_romaneio")
    private Romaneio romaneio;

}
