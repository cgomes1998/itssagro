package br.com.sjc.modelo.enums;

import lombok.Getter;

@Getter
public enum TipoTransporteEnum {

    MI("Z010"),
    ME("Z011"),
    ME_RODOVIARIO("Z070");

    private String codigo;

    TipoTransporteEnum(String codigo) {

        this.codigo = codigo;
    }
}
