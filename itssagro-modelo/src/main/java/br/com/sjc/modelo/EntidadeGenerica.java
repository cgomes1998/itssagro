package br.com.sjc.modelo;

import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
public class EntidadeGenerica implements ObjectID<Long> {

    @Transient
    private String uuid = UUID.randomUUID().toString();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "dt_cadastro")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dataCadastro;

    @Column(name = "dt_alteracao")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dataAlteracao;

    @Transient
    private String buscaRapida;

    @JsonIgnore
    @Transient
    protected volatile transient Usuario usuarioLogado;

    @PrePersist
    public void preSalvar() {

        if (this.isNew() && ObjetoUtil.isNull(getDataCadastro())) {
            setDataCadastro(DateUtil.hoje());
        }
    }

    public boolean isNew() {

        return Objects.isNull(id);
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        EntidadeGenerica that = (EntidadeGenerica) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

}
