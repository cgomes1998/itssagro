package br.com.sjc.modelo.enums;

import br.com.sjc.modelo.*;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;

import java.util.Comparator;
import java.util.Optional;

public enum EntidadeTipo {
    CLIENTE,
    ANALISE,
    CARREGAMENTO,
    CHECKLIST,
    CONVERSAO_LITRAGEM,
    CLASSIFICACAO,
    DASHBOARD_FILAS,
    DASHBOARD_GERENCIAL,
    ATRIBUIR_LACRES_ROMANEIOS,
    PESAGEM,
    ROMANEIO,
    SENHA,
    LAUDO,
    MOTIVO,
    HARDWARE,
    LOCAL,
    ARQUIVO,
    FORMULARIO,
    TABELA_VALORES,
    LACRE,
    CARTAO_ACESSO,
    CONFIGURACAO_FLUXO,
    MOTORISTA,
    TIPO_VEICULO,
    VEICULO,
    MATERIAL,
    LOTE;
    
    public static <E> EntidadeTipo getEntidadeTipo(E entidade) {
        
        EntidadeTipo entidadeTipo = null;
        
        if (entidade instanceof LoteAnaliseQualidade) {
            
            entidadeTipo = EntidadeTipo.LOTE;
        }
        
        if (entidade instanceof Romaneio) {
            
            Romaneio romaneio = (Romaneio) entidade;
            
            entidadeTipo = ObjetoUtil.isNotNull(romaneio.getEntidadeTipo()) ? romaneio.getEntidadeTipo() : EntidadeTipo.ROMANEIO;
            
            atualizarDataCadastroUltimaAnaliseQualidade(entidadeTipo, (Romaneio) entidade);
        }
        
        if (entidade instanceof Senha) {
            
            Senha senha = (Senha) entidade;
            
            entidadeTipo = ObjetoUtil.isNotNull(senha.getEntidadeTipo()) ? senha.getEntidadeTipo() : EntidadeTipo.SENHA;
        }
        
        if (entidade instanceof SenhaFormulario) {
            
            entidadeTipo = EntidadeTipo.CHECKLIST;
        }
        
        if (entidade instanceof Analise) {
            
            entidadeTipo = EntidadeTipo.ANALISE;
        }
        
        if (entidade instanceof Laudo) {
            
            entidadeTipo = EntidadeTipo.LAUDO;
        }
        
        if (entidade instanceof Motivo) {
            
            entidadeTipo = EntidadeTipo.MOTIVO;
        }
        
        if (entidade instanceof Hardware) {
            
            entidadeTipo = EntidadeTipo.HARDWARE;
        }
        
        if (entidade instanceof Local) {
            
            entidadeTipo = EntidadeTipo.LAUDO;
        }
        
        if (entidade instanceof Documento) {
            
            entidadeTipo = EntidadeTipo.ARQUIVO;
        }
        
        if (entidade instanceof Formulario) {
            
            entidadeTipo = EntidadeTipo.FORMULARIO;
        }
        
        if (entidade instanceof TabelaValor) {
            
            entidadeTipo = EntidadeTipo.TABELA_VALORES;
        }
        
        if (entidade instanceof Lacre) {
            
            entidadeTipo = EntidadeTipo.LACRE;
        }
        
        if (entidade instanceof CartaoAcesso) {
            
            entidadeTipo = EntidadeTipo.CARTAO_ACESSO;
        }
        
        if (entidade instanceof Fluxo) {
            
            entidadeTipo = EntidadeTipo.CONFIGURACAO_FLUXO;
        }
        
        if (entidade instanceof Motorista) {
            
            entidadeTipo = EntidadeTipo.MOTORISTA;
        }
        
        if (entidade instanceof TipoVeiculo) {
            
            entidadeTipo = EntidadeTipo.TIPO_VEICULO;
        }
        
        if (entidade instanceof Veiculo) {
            
            entidadeTipo = EntidadeTipo.VEICULO;
        }
        
        if (entidade instanceof Material) {
            
            entidadeTipo = EntidadeTipo.MATERIAL;
        }
        
        if (entidade instanceof Pesagem) {
            
            entidadeTipo = EntidadeTipo.PESAGEM;
        }
        
        return entidadeTipo;
    }
    
    /*
        Método responsável por verificar se o evento está sendo acionado na tela de analise de qualidade e se possui analise.
        Caso true, verifica se existe novas analises.
        
        Caso false, obtem a última analise realizada e atualiza a data de cadastro dela para data atual.
        Caso true, não tem nenhuma ação, porque a classe AnaliseQualidade extende Entidade, e nela temos um método com a anotação @PreSave que seta
        a data de cadastro como a data atual.
        
        Regra solicitada pela key user Ana Reis, pois temos relatórios com a data da última analise feita para ser feito a média de tempo do
        caminhão em cada etapa, e como tem cenário em que a etapa pode ser refeita N vezes, a key user solicitou a alteração da data mesmo que
        não aja alteração.
     */
    private static void atualizarDataCadastroUltimaAnaliseQualidade(EntidadeTipo entidadeTipo, Romaneio romaneio) {
        
        if (EntidadeTipo.CLASSIFICACAO.equals(entidadeTipo) && ColecaoUtil.isNotEmpty(romaneio.getAnalises())) {
            
            boolean possuiNovaAnalise = romaneio.getAnalises().stream().anyMatch(analiseQualidade -> ObjetoUtil.isNull(analiseQualidade.getId()));
            
            if (!possuiNovaAnalise) {
                
                Optional<Long> ultimaAnalise = romaneio.getAnalises().stream().map(AnaliseQualidade::getId).max(Comparator.naturalOrder());
                
                if (ObjetoUtil.isNotNull(ultimaAnalise) && ultimaAnalise.isPresent()) {
                    
                    romaneio.getAnalises().stream().filter(analiseQualidade -> analiseQualidade.getId().equals(ultimaAnalise.get())).findFirst().ifPresent(analiseQualidade -> {
                        analiseQualidade.setDataCadastro(DateUtil.hoje());
                    });
                }
            }
        }
    }
}
