package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.EtapaCampoEnum;
import br.com.sjc.modelo.enums.EtapaEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EtapaCampoDTO extends DataDTO {

    private EtapaEnum etapa;

    private EtapaCampoEnum campo;

    private String descricao;

    public EtapaCampoDTO(EtapaCampoEnum etapaCampo) {
        setCampo(etapaCampo);
        setEtapa(etapaCampo.getEtapa());
        setDescricao(etapaCampo.getDescricao());
    }

}
