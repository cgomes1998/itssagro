package br.com.sjc.modelo;

import br.com.sjc.modelo.cfg.Portaria;
import br.com.sjc.modelo.cfg.PortariaItem;
import br.com.sjc.modelo.cfg.PortariaMotivo;
import br.com.sjc.modelo.cfg.PortariaVeiculo;
import br.com.sjc.modelo.enums.TipoAcessoPortariaEnum;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_portaria_ticket", schema = "public")
public class PortariaTicket extends EntidadeAutenticada {
    private static final long serialVersionUID = 1L;

    @EntityProperties(value = {"id", "descricao", "ipImpressora", "portaImpressora"})
    @ManyToOne
    @JoinColumn(name = "id_portaria")
    private Portaria portaria;

    @Column(name = "dt_marcacao")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dataMarcacao;

    @Column(name = "dt_entrada")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dataEntrada;

    @Column(name = "dt_saida")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dataSaida;

    @EntityProperties(value = {"id", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_portaria_motivo")
    private PortariaMotivo motivo;

    @Column(name = "motivo_complemento")
    private String motivoComplemento;

    @Column(name = "cnpj_empresa")
    private String cnpj;

    @Column(name = "empresa")
    private String empresa;

    @Column(name = "cpf_motorista")
    private String cpfMotorista;

    @Column(name = "nome_motorista")
    private String motorista;

    @Column(name = "id_sjc_motorista")
    private String idSjcMotorista;

    @EntityProperties(value = {"id", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_portaria_veiculo")
    private PortariaVeiculo veiculo;

    @Column(name = "placa_veiculo")
    private String placaVeiculo;

    @Column(name = "placa_carreta1")
    private String placaCarreta1;

    @Column(name = "placa_carreta2")
    private String placaCarreta2;

    @Column(name = "placa_carreta3")
    private String placaCarreta3;

    @EntityProperties(value = {"id", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_portaria_item")
    private PortariaItem produto;

    @Column(name = "observacao")
    private String observacao;

    @Column(name = "nome_colaborador")
    private String nomeColaborador;

    @Column(name = "id_colaborador")
    private String idColaborador;

    @Column(name = "senha")
    private String senha;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_acesso")
    private TipoAcessoPortariaEnum tipoAcesso;

    @Column(name = "nota_fiscal")
    private String notaFiscal;

    @Column(name = "estorno", nullable = false, columnDefinition = "boolean default false")
    private boolean estorno;

    @Column(name = "dt_estorno")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dataEstorno;

    @Column(name = "id_colaborador_estorno")
    private String idColaboradorEstorno;

    @Column(name = "motivo_estorno")
    private String motivoEstorno;
}
