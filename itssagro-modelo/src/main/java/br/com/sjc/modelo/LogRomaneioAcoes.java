package br.com.sjc.modelo;

import br.com.sjc.modelo.sap.LogRestricaoOrdem;
import br.com.sjc.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_log_romaneio_acoes", schema = "sap")
@SuppressWarnings("all")
public class LogRomaneioAcoes implements Serializable {

    @Transient
    private String uuid = UUID.randomUUID().toString();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dt_liberacao_romaneio")
    private Date dataLiberacao;

    @Lob
    @Type(type = "text")
    private String motivo;

    @ManyToOne
    @JoinColumn(name = "id_responsavel")
    private Usuario responsavel;

    @ManyToOne
    @JoinColumn(name = "id_log_romaneio")
    @JsonIgnore
    private LogRomaneio logRomaneio;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, mappedBy = "logRomaneioAcoes", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<LogRestricaoOrdem> restricoes;

    @PrePersist
    public void preSalvar() {
        this.setDataLiberacao(DateUtil.hoje());
    }
}
