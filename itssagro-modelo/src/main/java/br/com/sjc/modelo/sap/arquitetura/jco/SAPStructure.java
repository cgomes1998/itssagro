package br.com.sjc.modelo.sap.arquitetura.jco;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * <b>Title:</b> SAPStructure
 * </p>
 *
 * <p>
 * <b>Description:</b> Annotation responsável por definir uma classe como uma structure SAP.
 * </p>
 *
 * @author Bruno Zafalão
 * @version 1.0.0
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SAPStructure {

    /**
     * Propriedade que identifica o nome da tabela SAP.
     *
     * @author Bruno Zafalão
     */
    String name();

    /**
     * Propriedade que identifica o item que será registro da tabela SAP.
     *
     * @author Bruno Zafalão
     */
    Class<? extends JCoItem> item();
}
