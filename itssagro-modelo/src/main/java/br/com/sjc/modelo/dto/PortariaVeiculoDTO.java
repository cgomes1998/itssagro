package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.cfg.PortariaMotivo;
import br.com.sjc.modelo.cfg.PortariaVeiculo;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PortariaVeiculoDTO extends DataDTO {
    @ProjectionProperty
    private String descricao;

    @ProjectionProperty
    private Boolean ativo;

    public PortariaVeiculoDTO(PortariaVeiculo veiculo){
        this.setId(veiculo.getId());
        this.setDescricao(veiculo.getDescricao());
        this.setAtivo(veiculo.isAtivo());
    }
}
