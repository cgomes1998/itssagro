package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPParameter(function = "/ITSSAGRO/CDU008_CLIENTE")
public class RfcClienteRequest extends JCoRequest {

    @SAPColumn(name = "I_DT_ULT_INT")
    private String dataUltimaIntegracao;

}
