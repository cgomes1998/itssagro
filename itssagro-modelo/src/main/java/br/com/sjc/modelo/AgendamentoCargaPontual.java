package br.com.sjc.modelo;

import br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento.Conteudo2;
import br.com.sjc.util.StringUtil;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "tb_agendamento")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AgendamentoCargaPontual extends EntidadeGenerica {

    @Column(name = "id_agendamento")
    private int idAgendamento;

    @Column(columnDefinition = "text")
    private String conteudo;

    @Column(columnDefinition = "text")
    private String exception;

    @Builder
    public AgendamentoCargaPontual(Conteudo2 conteudo) {

        this.setIdAgendamento(conteudo.getIdAgendamento());

        this.setConteudo(StringUtil.toJson(conteudo));
    }

}
