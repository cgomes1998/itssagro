package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.response.item.RfcIvaResponseItem;
import br.com.sjc.modelo.sap.response.item.RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_DADOS_Item;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@SAPParameter(function = "ZITSSAGRO_DIVERG_PESO_ROMANEIO")
public class RfcZITSSAGRO_DIVERG_PESO_ROMANEIOResponse extends JCoResponse {

    @SAPTable(name = "T_DADOS", item = RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_DADOS_Item.class)
    private List<RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_DADOS_Item> t_dados;

}
