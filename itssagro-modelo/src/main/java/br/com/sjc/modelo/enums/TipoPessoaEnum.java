package br.com.sjc.modelo.enums;

import lombok.Getter;

@Getter
public enum TipoPessoaEnum {

    PF("Pessoa Física"),

    PJ("Pessoa Juridica");

    private String value;

    TipoPessoaEnum(final String value) {

        this.value = value;
    }
}
