package br.com.sjc.modelo.enums;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Created by julio.bueno on 24/06/2019.
 */
@Getter
@RequiredArgsConstructor
public enum MotivoTipoEnum {

    MOTORISTA("Motorista"),
    VEICULO("Veículo"),
    ORDEM("Ordem"),
    SENHA("Senha"),
    PESAGEM("Pesagem"),
    LACRAGEM("Lacragem"),
    CONVERSAO_LITRAGEM("Conversão de Litragem"),
    ANALISE_QUALIDADE("Análise de Qualidade"),
    EMISSAO_DOCUMENTOS("Emissão de Documentos"),
    CONFERENCIA("Conferência"),
    PENDENCIA_COMERCIAL("Pendência Comercial"),
    CHECKLIST("Checklist"),
    ROMANEIO_VINCULADO("Romaneio Vinculado");

    @NonNull
    private String value;
}
