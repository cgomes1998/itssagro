package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.enums.*;
import br.com.sjc.modelo.sap.Produtor;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import br.com.sjc.modelo.sap.sync.SAPProdutorRural;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class RfcProdutorResponseItem extends JCoItem {

    @SAPColumn(name = "NAME1")
    private String nomeProdutor;

    @SAPColumn(name = "STCD1")
    private String cnpj;

    @SAPColumn(name = "STCD2")
    private String cpf;

    @SAPColumn(name = "STCD3")
    private String inscricaoEstadual;

    @SAPColumn(name = "NAME_CO")
    private String nomeFazenda;

    @SAPColumn(name = "STREET")
    private String logradouro;

    @SAPColumn(name = "POST_CODE1")
    private String cep;

    @SAPColumn(name = "CITY1")
    private String cidade;

    @SAPColumn(name = "CITY2")
    private String setor;

    @SAPColumn(name = "REGION")
    private String regiao;

    @SAPColumn(name = "TEL_NUMBER")
    private String telefone;

    @SAPColumn(name = "LIFNR")
    private String codigoProdutor;

    @SAPColumn(name = "KTOKK")
    private String grpContaProdutor;

    @SAPColumn(name = "HOUSE_NO")
    private String numeroRua;

    @SAPColumn(name = "BLOQUEADO")
    private String bloqueado;

    @SAPColumn(name = "MARC_ELIM")
    private String eliminado;

    private StatusRegistroEnum statusRegistroSAP;

    public enum COLUMN_INPUT {

        DT_ULT_INF,

        COD_PRODUTOR,

        PRE_CAD,

        NOME_PROD,

        CNPJ_PROD,

        CPF_PROD,

        IE_PROD,

        NOMEFAZ_PROD,

        RUA_PROD,

        CEP_PROD,

        CIDADE_PROD,

        SETOR_PROD,

        REGIAO_PROD,

        TEL_PROD,

        NRO_PROD,

        ID_PRODUTOR;
    }

    public static SAPProdutorRural newInstance() {

        return new SAPProdutorRural();
    }

    public StatusRegistroEnum getStatusRegistroSAP() {

        this.statusRegistroSAP = StatusRegistroEnum.ATIVO;

        if (StringUtil.isNotNullEmpty(this.getBloqueado())) {

            this.statusRegistroSAP = StatusRegistroEnum.BLOQUEADO;
        }

        if (StringUtil.isNotNullEmpty(this.getEliminado())) {

            this.statusRegistroSAP = StatusRegistroEnum.ELIMINADO;
        }

        if (StringUtil.isNotNullEmpty(this.getBloqueado()) && StringUtil.isNotNullEmpty(this.getEliminado())) {

            this.statusRegistroSAP = StatusRegistroEnum.BLOQUEADO_E_ELIMINADO;
        }

        return this.statusRegistroSAP;
    }

    public Produtor copy(Produtor entidadeSaved) {

        if (ObjetoUtil.isNull(entidadeSaved)) {

            entidadeSaved = new Produtor();

        } else {

            entidadeSaved.setId(entidadeSaved.getId());
        }

        entidadeSaved.setTipo(StringUtil.isEmpty(this.getCpf()) ? TipoPessoaEnum.PJ : TipoPessoaEnum.PF);

        entidadeSaved.setNome(StringUtil.isNotNullEmpty(this.getNomeProdutor()) ? this.getNomeProdutor().trim() : entidadeSaved.getNome());

        entidadeSaved.setCodigo(StringUtil.isNotNullEmpty(this.getCodigoProdutor()) ? this.getCodigoProdutor().trim() : entidadeSaved.getCodigo());

        entidadeSaved.setCpf(StringUtil.isNotNullEmpty(this.getCpf()) ? this.getCpf().trim() : entidadeSaved.getCpf());

        entidadeSaved.setCnpj(StringUtil.isNotNullEmpty(this.getCnpj()) ? this.getCnpj().trim() : entidadeSaved.getCnpj());

        entidadeSaved.setFazenda(StringUtil.isNotNullEmpty(this.getNomeFazenda()) ? this.getNomeFazenda().trim() : entidadeSaved.getFazenda());

        entidadeSaved.setEndFazenda(StringUtil.isNotNullEmpty(this.getLogradouro()) ? this.getLogradouro().trim() : entidadeSaved.getEndFazenda());

        entidadeSaved.setBairro(StringUtil.isNotNullEmpty(this.getSetor()) ? this.getSetor().trim() : entidadeSaved.getBairro());

        entidadeSaved.setMunicipio(StringUtil.isNotNullEmpty(this.getCidade()) ? this.getCidade().trim() : entidadeSaved.getMunicipio());

        entidadeSaved.setEstado(StringUtil.isNotNullEmpty(this.getRegiao()) ? UFEnum.valueOf(this.getRegiao().trim()) : entidadeSaved.getEstado());

        entidadeSaved.setCep(StringUtil.isNotNullEmpty(this.getCep()) ? this.getCep().trim() : entidadeSaved.getCep());

        entidadeSaved.setTelefone(StringUtil.isNotNullEmpty(this.getTelefone()) ? this.getTelefone().trim() : entidadeSaved.getTelefone());

        entidadeSaved.setInscricaoEstadual(StringUtil.isNotNullEmpty(this.getInscricaoEstadual()) ? this.getInscricaoEstadual().trim() : entidadeSaved.getInscricaoEstadual());

        entidadeSaved.setGrpContaProdutor(StringUtil.isNotNullEmpty(this.getGrpContaProdutor()) ? this.getGrpContaProdutor() : entidadeSaved.getGrpContaProdutor());

        entidadeSaved.setStatusInscEstadual(StatusInscEstadualEnum.ATIVO);

        entidadeSaved.setLogSAP("PRODUTOR SINCRONIZADO COM O SAP EM " + DateUtil.hoje());

        if (StringUtil.isNotNullEmpty(this.getCodigoProdutor())) {

            entidadeSaved.setLogSAP(entidadeSaved.getLogSAP() + "\nCÓDIGO: " + this.getCodigoProdutor());
        }

        entidadeSaved.setStatusCadastroSap(StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO);

        entidadeSaved.setStatusRegistro(this.getStatusRegistroSAP());

        return entidadeSaved;
    }
}
