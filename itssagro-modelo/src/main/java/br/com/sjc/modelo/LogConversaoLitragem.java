package br.com.sjc.modelo;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "tb_log_conversao_litragem")
@AllArgsConstructor
@NoArgsConstructor
public class LogConversaoLitragem extends EntidadeGenerica {

    @Column(name = "numero_romaneio")
    private String numeroRomaneio;

    @Column(columnDefinition = "text")
    private String conteudo;


}
