package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.Fluxo;
import br.com.sjc.modelo.sap.Material;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DashboardGerencialFiltroDTO {

    private List<Fluxo> fluxos;

    private List<Material> materiais;

    private List<FluxoEtapaDTO> etapas;

}
