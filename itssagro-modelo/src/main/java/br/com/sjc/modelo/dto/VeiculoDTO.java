package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.enums.UFEnum;
import br.com.sjc.modelo.sap.TipoVeiculo;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VeiculoDTO extends DataDTO {

    @ProjectionProperty
    private UFEnum ufPlaca1;

    @ProjectionProperty
    private UFEnum ufPlaca2;

    @ProjectionProperty
    private UFEnum ufPlaca3;

    @ProjectionProperty
    private StatusEnum status;

    @ProjectionProperty
    private String codigo;

    @ProjectionProperty
    private String placa1;

    @ProjectionProperty
    private String placa2;

    @ProjectionProperty
    private String placa3;

    @ProjectionProperty
    private String renavam;

    @ProjectionProperty
    private String chassi;

    @ProjectionProperty
    private String marca;

    @ProjectionProperty
    private String cor;

    @ProjectionProperty
    private String municipio;

    @ProjectionProperty
    private Date dataVencimentoPlaca1;

    @ProjectionProperty
    private int eixos;
}
