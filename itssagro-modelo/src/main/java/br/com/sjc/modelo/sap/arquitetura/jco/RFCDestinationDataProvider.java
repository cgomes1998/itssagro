package br.com.sjc.modelo.sap.arquitetura.jco;

import com.sap.conn.jco.ext.DestinationDataEventListener;
import com.sap.conn.jco.ext.DestinationDataProvider;

import java.util.Properties;

/**
 * <p>
 * <b>Title:</b> ITSSDestinationDataProvider
 * </p>
 *
 * <p>
 * <b>Description:</b> Classe responsável por prover os dados de conexão com o SAP via RFC.
 * </p>
 *
 * <p>
 * <b>Company: </b> ITSS Soluções em Tecnologia
 * </p>
 *
 * @author Bruno Zafalão
 * @version 1.0.0
 */
public class RFCDestinationDataProvider implements DestinationDataProvider {

    /**
     * Atributo connectProperties.
     */
    private final Properties connectProperties;

    /**
     * Responsável pela criação de novas instâncias desta classe.
     *
     * @param connectProperties
     */
    public RFCDestinationDataProvider(final Properties connectProperties) {

        this.connectProperties = connectProperties;
    }

    /**
     * Método responsável por obter as propriedades de conexão.
     *
     * @param destinationName
     * @return <code>Properties</code>
     * @author Bruno Zafalão
     */
    public Properties getDestinationProperties(final String destinationName) {

        return this.connectProperties;
    }

    /**
     * Método responsável por definir o ouvinte.
     *
     * @param eventListener
     * @author Bruno Zafalão
     */
    public void setDestinationDataEventListener(final DestinationDataEventListener eventListener) {

        return;
    }

    /**
     * Método responsável por habilitar o suporte aos eventos.
     *
     * @return <code>boolean</code>
     * @author Bruno Zafalão
     */
    public boolean supportsEvents() {

        return false;
    }
}
