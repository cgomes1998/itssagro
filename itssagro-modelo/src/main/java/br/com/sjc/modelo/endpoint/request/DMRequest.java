package br.com.sjc.modelo.endpoint.request;

import br.com.sjc.modelo.endpoint.request.item.FornecedorRequest;
import br.com.sjc.modelo.endpoint.request.item.MotoristaRequest;
import br.com.sjc.modelo.endpoint.request.item.TransportadoraRequest;
import br.com.sjc.modelo.endpoint.request.item.VeiculoRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DMRequest {

    private List<FornecedorRequest> fornecedores;

    private List<TransportadoraRequest> transportadoras;

    private List<VeiculoRequest> veiculos;

    private List<MotoristaRequest> motoristas;
}

