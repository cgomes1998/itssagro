package br.com.sjc.modelo;

import br.com.sjc.modelo.dto.DataDTO;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.*;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaldoReservadoPedidoDTO extends DataDTO {

    @ProjectionProperty
    private String numeroRomaneioConsumidor;

    @ProjectionProperty
    private String numeroPedido;

    @ProjectionProperty
    private String itemPedido;

    @ProjectionProperty
    private BigDecimal saldoUtilizado;
}
