package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.cfg.Portaria;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import java.sql.Time;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PortariaDTO extends DataDTO {

    @ProjectionProperty
    private String sigla;

    @ProjectionProperty
    private String descricao;

    @ProjectionProperty
    private String ipImpressora;

    @ProjectionProperty
    private String portaImpressora;

    @ProjectionProperty
    private Boolean ativo;

    @ProjectionProperty
    private Time horaInicioMarcacao;

    @ProjectionProperty
    private Time horaFimMarcacao;

    @ProjectionProperty
    private boolean validarHorarioMarcacao;

    public PortariaDTO(Portaria portaria){
        this.setId(portaria.getId());
        this.setSigla(portaria.getSigla());
        this.setDescricao(portaria.getDescricao());
        this.setIpImpressora(portaria.getIpImpressora());
        this.setPortaImpressora(portaria.getPortaImpressora());
        this.setAtivo(portaria.isAtivo());
        this.setValidarHorarioMarcacao(portaria.isValidarHorarioMarcacao());

        if(portaria.isValidarHorarioMarcacao()){
            this.setHoraInicioMarcacao(portaria.getHoraInicioMarcacao());
            this.setHoraFimMarcacao(portaria.getHoraFimMarcacao());
        }
    }
}
