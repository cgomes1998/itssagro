package br.com.sjc.modelo.sap.arquitetura.jco;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.sap.conn.jco.JCoMetaData;

/**
 * <p>
 * <b>Title:</b> SAPColumn
 * </p>
 *
 * <p>
 * <b>Description:</b> Annotation responsável por definir um parâmetro utilizado
 * em determinada função SAP.
 * </p>
 *
 * @author Bruno Zafalão
 *
 * @version 1.0.0
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SAPColumn {

    /**
     * Propriedade que identifica o nome da coluna utilizada na função SAP.
     *
     * @author Bruno Zafalão
     */
    String name();

    /**
     * Propriedade que identifica o nome da coluna utilizada para o tamanho da
     * propriedade.
     *
     * @author Silvânio Júnior
     */
    int size() default 0;

    /**
     * Propriedade que identifica o nome da coluna utilizada para o tamanho da
     * propriedade.
     *
     * @author Silvânio Júnior
     */
    int type() default JCoMetaData.TYPE_STRING;

    int ordinal() default 0;
}
