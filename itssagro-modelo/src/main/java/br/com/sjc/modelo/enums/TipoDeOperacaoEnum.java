package br.com.sjc.modelo.enums;

import lombok.Getter;

@Getter
public enum TipoDeOperacaoEnum {

    MM("MM - Quadro de Pedidos"),

    SD("SD - Quadro de Ordens"),

    MM_SD("MM/SD - Quadro de Pedido + Ordens"),

    NAO_SE_APLICA("Sem Quadro de Divisões de Carga");

    private String descricao;

    TipoDeOperacaoEnum(final String descricao) {

        this.descricao = descricao;
    }

}
