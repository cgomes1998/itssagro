package br.com.sjc.modelo.sap.request.item;

import br.com.sjc.modelo.enums.ModuloOperacaoEnum;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class RfcStatusRomaneioRequestItem extends JCoItem {

	@SAPColumn(name = "MODLO")
	private String modulo;

	@SAPColumn(name = "OPERA")
	private String operacao;

	@SAPColumn(name = "CHAVE")
	private String chave;

	@SAPColumn(name = "ROMANEIO")
	private String romaneio;

	public RfcStatusRomaneioRequestItem() {

	}

	public RfcStatusRomaneioRequestItem(ModuloOperacaoEnum modulo, String operacao, String chave, String romaneio) {

		this.modulo = modulo.getCodigo();
		this.operacao = operacao;
		this.chave = chave;
		this.chave = StringUtil.completarZeroAEsquerda(
			   20,
			   chave
		);
		this.romaneio = romaneio;
	}

}
