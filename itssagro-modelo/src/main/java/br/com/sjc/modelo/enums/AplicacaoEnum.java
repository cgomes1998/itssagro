package br.com.sjc.modelo.enums;

import lombok.Getter;

@Getter
public enum AplicacaoEnum {

    GESTAO_PATIO("GESTÃO DE PÁTIO"),
    ITSSAGRO("ITSS AGRO"),
    ITSSCOMMONS("ITSS COMMONS");

    private String descricao;

    AplicacaoEnum(final String descricao) {
        this.descricao = descricao;
    }
}
