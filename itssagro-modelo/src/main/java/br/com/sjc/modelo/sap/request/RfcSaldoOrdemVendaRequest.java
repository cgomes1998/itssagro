package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SAPParameter(function = "/ITSSAGRO/SALDO_ORDEM_VENDA")
public class RfcSaldoOrdemVendaRequest extends JCoRequest {

    @SAPColumn(name = "I_VBELN")
    private String codigo;

}
