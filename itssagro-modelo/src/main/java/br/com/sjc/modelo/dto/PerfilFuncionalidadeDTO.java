package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.FuncionalidadeEnum;
import lombok.Data;

@Data
public class PerfilFuncionalidadeDTO {

    private FuncionalidadeEnum funcionalidade;

    private boolean visualizar;

    private boolean alterar;

    private boolean incluir;

    private boolean excluir;

    private boolean consultar;
}
