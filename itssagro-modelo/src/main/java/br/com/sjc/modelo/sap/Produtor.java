package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.endpoint.response.item.FornecedorResponse;
import br.com.sjc.modelo.enums.StatusInscEstadualEnum;
import br.com.sjc.modelo.enums.TipoPessoaEnum;
import br.com.sjc.modelo.enums.UFEnum;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.text.MessageFormat;
import java.util.Objects;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_produtor", schema = "sap")
public class Produtor extends EntidadeSAPSync {

    private static final long serialVersionUID = 5320421441043260436L;

    @Column(name = "nome")
    private String nome;

    @Column(name = "cpf")
    private String cpf;

    @Column(name = "cnpj")
    private String cnpj;

    @Column(name = "fazenda")
    private String fazenda;

    @Column(name = "endereco_fazenda")
    private String endFazenda;

    @Column(name = "bairro")
    private String bairro;

    @Column(name = "municipio")
    private String municipio;

    @Enumerated(EnumType.STRING)
    @Column(name = "estado")
    private UFEnum estado;

    @Column(name = "cep")
    private String cep;

    @Column(name = "telefone")
    private String telefone;

    @Column(name = "inscricao_estadual")
    private String inscricaoEstadual;

    @Column(name = "grp_conta_produtor")
    private String grpContaProdutor;

    @Column
    private String senha;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_insc_estadual")
    private StatusInscEstadualEnum statusInscEstadual;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo")
    private TipoPessoaEnum tipo;

    public String getDescricaoFormatada() {
        return this.toString();
    }

    public FornecedorResponse toResponse() {

        final FornecedorResponse response = new FornecedorResponse();

        response.setCodigo(this.getCodigo());

        response.setNome(this.getNome());

        response.setDocumento(StringUtil.isNotNullEmpty(this.getCnpj()) ? this.getCnpj() : this.getCpf());

        return response;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Produtor produtor = (Produtor) o;
        return Objects.equals(this.getCodigo(), produtor.getCodigo()) &&
                Objects.equals(cpf, produtor.cpf) &&
                Objects.equals(cnpj, produtor.cnpj);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getCodigo(), cpf, cnpj);
    }

    @Override
    public String toString() {

        if (StringUtil.isNotNullEmpty(this.getCodigo())) {

            return MessageFormat.format("{0} - {1}", this.getCodigo(), this.nome);
        }

        if (StringUtil.isNotNullEmpty(this.cpf)) {

            return MessageFormat.format("{0} - {1}", StringUtil.format(this.cpf, "###.###.###-##"), this.nome);
        }

        if (StringUtil.isNotNullEmpty(this.cnpj)) {

            return MessageFormat.format("{0} - {1}", StringUtil.format(this.cnpj, "###.###.###/####-##"), this.nome);
        }

        return this.nome;
    }
}
