package br.com.sjc.modelo.sap.sync;

import br.com.sjc.modelo.sap.Material;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SAPMaterial extends SAPEntidade {

    @SAPColumn(name = "COD_MAT")
    private String codigo;

    @SAPColumn(name = "DES_MAT")
    private String descricao;

    @SAPColumn(name = "UND_MED")
    private String unidadeMedida;

    @SAPColumn(name = "MARC_ELIM")
    private String eliminado;

    public enum COLUMN_INPUT {

        DT_ULT_INF;
    }

    public static SAPMaterial newInstance() {

        return new SAPMaterial();
    }

    public Material copy(Material entidadeSaved) {

        if (ObjetoUtil.isNull(entidadeSaved)) {

            entidadeSaved = new Material();

        } else {

            entidadeSaved.setId(entidadeSaved.getId());
        }

        entidadeSaved.setCodigo(StringUtil.isNotNullEmpty(this.getCodigo()) ? this.getCodigo().trim() : entidadeSaved.getCodigo());

        entidadeSaved.setDescricao(StringUtil.isNotNullEmpty(this.getDescricao()) ? this.getDescricao().trim() : entidadeSaved.getDescricao());

        entidadeSaved.setUnidadeMedida(StringUtil.isNotNullEmpty(this.getUnidadeMedida()) ? this.getUnidadeMedida().trim() : entidadeSaved.getUnidadeMedida());

        StatusRegistroEnum status = StatusRegistroEnum.ATIVO;

        if (StringUtil.isNotNullEmpty(this.getEliminado())) {

            status = StatusRegistroEnum.ELIMINADO;
        }

        entidadeSaved.setStatusRegistro(status);

        entidadeSaved.setStatusCadastroSap(StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO);

        return entidadeSaved;
    }
}
