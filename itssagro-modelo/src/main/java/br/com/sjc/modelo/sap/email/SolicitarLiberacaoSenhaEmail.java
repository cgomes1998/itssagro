package br.com.sjc.modelo.sap.email;

import br.com.sjc.modelo.Senha;
import br.com.sjc.modelo.Usuario;
import br.com.sjc.util.EmailBodyBuilder;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.email.Email;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by julio.bueno on 23/07/2019.
 */
@RequiredArgsConstructor
public class SolicitarLiberacaoSenhaEmail implements Email {

    @NonNull
    private Senha senha;

    @NonNull
    private Usuario usuario;

    @NonNull
    private String destinatario;

    @Override
    public String getAssunto () {

        return "Solicitação de Liberação de Senha";
    }

    @Override
    public String getTextoEmail () {

        return EmailBodyBuilder
                .build ()
                .addTextStrong ( "Motorista" )
                .addText ( "Nome: ${motorista.nome}" )
                .addText ( "CPF: ${motorista.cpf}" )
                .addLineBreak ( 1 )
                .addTextStrong ( "Veículo" )
                .addText ( "Placa: ${veiculo.placa}" )
                .addText ( "Tipo: ${veiculo.tipo}" )
                .addLineBreak ( 1 )
                .addTextStrong ( "Ordem de Venda" )
                .addText ( "Número: ${ordem.numero}" )
                .addText ( "Saldo: ${ordem.saldo}" )
                .addLineBreak ( 2 )
                .addText ( "Usuário Solicitação: ${usuario}" )
                .addText ( "Data Solicitação: ${data}" )
                .toString ();
    }

    @Override
    public List<String> getDestinatario () {

        return Arrays.asList ( destinatario );
    }

    @Override
    public Map<String, String> getParametros () {

        Map<String, String> parametros = new HashMap<> ();

        parametros.put ( "${motorista.nome}", ObjetoUtil.isNotNull ( senha.getMotorista () ) && ObjetoUtil.isNotNull ( senha.getMotorista ().getNome () ) ? senha.getMotorista ().getNome () : "" );
        parametros.put ( "${motorista.cpf}", ObjetoUtil.isNotNull ( senha.getMotorista () ) && ObjetoUtil.isNotNull ( senha.getMotorista ().getCpf () ) ? senha.getMotorista ().getCpf () : "" );

        parametros.put ( "${veiculo.placa}", ObjetoUtil.isNotNull ( senha.getVeiculo () ) && ObjetoUtil.isNotNull ( senha.getVeiculo ().getPlaca1 () ) ? senha.getVeiculo ().getPlaca1 () : "" );
        parametros.put ( "${veiculo.tipo}", ObjetoUtil.isNotNull ( senha.getVeiculo () ) && ObjetoUtil.isNotNull ( senha.getVeiculo ().getTipo () ) && ObjetoUtil.isNotNull ( senha.getVeiculo ().getTipo ().getDescricao () ) ? senha.getVeiculo ().getTipo ().getDescricao () : "" );

        parametros.put ( "${ordem.numero}", ObjetoUtil.isNotNull ( senha.getCodigoOrdemVenda () ) ? senha.getCodigoOrdemVenda () : "" );
        parametros.put ( "${ordem.saldo}", ObjetoUtil.isNotNull ( senha.getSaldoOrdemVenda () ) ? senha.getSaldoOrdemVenda ().toString () : "" );

        parametros.put ( "${usuario}", usuario.getNome () );
        parametros.put ( "${data}", new SimpleDateFormat ( "dd/MM/yyyy HH:mm" ).format ( senha.getDataAlteracao () ) );

        return parametros;
    }
}
