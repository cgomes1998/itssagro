package br.com.sjc.modelo.endpoint.request.item;

import br.com.sjc.modelo.Classificacao;
import br.com.sjc.modelo.Usuario;
import br.com.sjc.modelo.sap.ItensClassificacao;
import br.com.sjc.modelo.sap.Safra;
import br.com.sjc.modelo.sap.Veiculo;
import br.com.sjc.util.ObjetoUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClassificacaoRequest {

    private String codigoItem;

    private String descricaoItem;

    private double percentualIndice;

    private double percentualDesconto;

    private double desconto;

    public static Classificacao toClassificacao(final Veiculo placaCavalo, final Safra safra, final Usuario classificador, @NotNull final List<ClassificacaoRequest> itensRequest) {

        final Classificacao entidade = new Classificacao();

        entidade.setPlacaCavalo(placaCavalo);

        entidade.setSafra(safra);

        entidade.setUsuarioClassificador(classificador);

        entidade.setItens(new ArrayList(itensRequest.stream().map(ClassificacaoRequest::toItensClassificacao).collect(Collectors.toSet())));

        return entidade;
    }

    public ItensClassificacao toItensClassificacao() {

        final ItensClassificacao entidade = new ItensClassificacao();

        entidade.setCodigoArmazemTerceiros(this.codigoItem.trim());

        entidade.setDescricaoItem(this.descricaoItem.trim().toUpperCase());

        entidade.setPercentualDesconto(ObjetoUtil.isNotNull(this.percentualDesconto) ? BigDecimal.valueOf(this.percentualDesconto) : BigDecimal.ZERO);

        entidade.setIndice(ObjetoUtil.isNotNull(this.percentualIndice) ? BigDecimal.valueOf(this.percentualIndice) : BigDecimal.ZERO);

        entidade.setDesconto(ObjetoUtil.isNotNull(this.desconto) ? BigDecimal.valueOf(this.desconto) : BigDecimal.ZERO);

        return entidade;
    }
}
