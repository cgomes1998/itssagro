package br.com.sjc.modelo.sap.arquitetura.jco;

import java.util.LinkedList;
import java.util.List;

public class JCoRequest extends JCoParameter {

    @Override
    public JCoParameterType type() {
        return JCoParameterType.REQUEST;
    }

    public int getTableRows() {

        return 0;
    }

    public int getTableRows(String nameSapTable) {

        return getTableRows();
    }

    public List<?> getTableItens(String name) {

        return getTableItens();
    }

    public List<?> getTableItens() {

        return new LinkedList();
    }

    public int getStructureRows(String name) {
        return 0;
    }

    public List<?> getStructureItens(String name) {
        return new LinkedList();
    }

    @Override
    public String toString() {
        return "JCoRequest{ Type:" + JCoParameterType.REQUEST + "}";
    }

}
