package br.com.sjc.modelo;

import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_senha_formulario_resposta", schema = "public")
public class SenhaFormularioResposta extends EntidadeAutenticada {

    private static final long serialVersionUID = 1L;

    @EntityProperties(value = {"id"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_senha_formulario", nullable = false)
    @JsonIgnore
    private SenhaFormulario senhaFormulario;

    @EntityProperties(value = {"id", "formulario.id", "titulo", "required", "ordem", "tamanho", "valorEsperado", "tipo"}, collecions = {"opcoes"})
    @ManyToOne
    @JoinColumn(name = "id_formulario_item", nullable = false)
    private FormularioItem item;

    @EntityProperties(value = {"id", "senhaFormularioResposta.id", "formularioItemOpcao.id", "formularioItemOpcao.formularioItem.id", "formularioItemOpcao.titulo", "formularioItemOpcao.ordem"}, targetEntity =
            SenhaFormularioRespostaOpcao.class)
    @OrderBy("id desc")
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "senhaFormularioResposta", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    private List<SenhaFormularioRespostaOpcao> opcoes = new ArrayList<>();

    @Column
    private String resposta;

}
