package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.TipoParametroEnum;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ParametroDTO extends DataDTO {

    @ProjectionProperty
    private TipoParametroEnum tipoParametro;

    @ProjectionProperty
    private String valor;

    public String getTipoParametroDescricao() {

        return this.tipoParametro.getDescricao();
    }

}
