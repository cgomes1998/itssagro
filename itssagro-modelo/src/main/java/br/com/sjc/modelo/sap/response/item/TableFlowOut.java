package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class TableFlowOut extends JCoItem {

    @SAPColumn(name = "FLOWD")
    private String etapa;

    @SAPColumn(name = "DENOM")
    private String denominacao;

    @SAPColumn(name = "FLWST")
    private String status;

    @SAPColumn(name = "MODLO")
    private String modulo;

    @SAPColumn(name = "OPERA")
    private String operacao;

    @SAPColumn(name = "CHAVE")
    private String chave;

    @SAPColumn(name = "DOCUM")
    private String documentoGerado;

    @SAPColumn(name = "DESCR")
    private String produtor;
}
