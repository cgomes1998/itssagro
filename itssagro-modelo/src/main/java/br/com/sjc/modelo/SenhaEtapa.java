package br.com.sjc.modelo;

import br.com.sjc.modelo.serializer.FluxoEtapaSerializer;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_senha_etapa", schema = "public")
public class SenhaEtapa extends EntidadeAutenticada {

    @EntityProperties(value = {"id"})
    @ManyToOne
    @JoinColumn(name = "id_senha")
    @JsonBackReference
    private Senha senha;

    @EntityProperties(value = {"id", "fluxo.id", "fluxo.codigo", "fluxo.descricao", "fluxo.tipoFluxo", "sequencia", "statusCargaPontualDestinoSucesso", "statusCargaPontualDestinoEstorno", "statusEstornoAutomacao",
            "mensagemEstornoAutomacao", "etapa", "medirTempo", "etapaFinal", "avancoAutomatico"}, collecions = {"criterios"})
    @ManyToOne
    @JoinColumn(name = "id_etapa")
    @JsonSerialize(using = FluxoEtapaSerializer.class)
    private FluxoEtapa etapa;

    @Column(name = "qtd_repeticao")
    private Integer qtdRepeticao = 0;

    @Column(name = "qtd_chamada")
    private Integer qtdChamada = 0;

    @NonNull
    @Column(name = "hr_entrada")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date horaEntrada;

    @Column(name = "hr_saida")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date horaSaida;

    @Column(name = "duracao")
    private Long duracao;

    @NonNull
    @Column(name = "status_etapa")
    private String statusEtapa;

    public SenhaEtapa() {

    }

    public SenhaEtapa(FluxoEtapa etapa, Date horaEntrada, String statusEtapa) {

        this.etapa = etapa;
        this.horaEntrada = horaEntrada;
        this.statusEtapa = statusEtapa;
    }

    @PostLoad
    @PreUpdate
    @PrePersist
    public void calcularDuracao() {

        Date horaSaida = ObjetoUtil.isNotNull(this.horaSaida) ? this.horaSaida : new Date();
        this.duracao = horaSaida.getTime() - this.horaEntrada.getTime();
        verificarStatusEtapa();
    }

    private void verificarStatusEtapa() {

        if (ObjetoUtil.isNull(statusEtapa)) {
            setStatusEtapa(StringUtil.empty());
        }
    }

}
