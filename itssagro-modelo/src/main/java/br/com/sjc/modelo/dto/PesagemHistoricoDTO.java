package br.com.sjc.modelo.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class PesagemHistoricoDTO {

	private Long idPesagem;

	private Date dataCadastro;

	public PesagemHistoricoDTO() {

	}

	public PesagemHistoricoDTO(Long idPesagem, Date dataCadastro) {

		this.idPesagem = idPesagem;
		this.dataCadastro = dataCadastro;
	}

}
