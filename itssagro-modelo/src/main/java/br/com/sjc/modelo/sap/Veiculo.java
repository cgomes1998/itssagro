package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.Motivo;
import br.com.sjc.modelo.endpoint.response.item.VeiculoResponse;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import br.com.sjc.modelo.enums.UFEnum;
import br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento.Placa1semi;
import br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento.Placa2semi;
import br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento.Veiculos;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_veiculo", schema = "sap")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Veiculo extends EntidadeSAPPre {

    @Column(name = "placa_1", length = 7, unique = true)
    private String placa1;

    @Column(name = "placa_2", length = 7)
    private String placa2;

    @Column(name = "placa_3", length = 7)
    private String placa3;

    @Column(name = "municipio")
    private String municipio;

    @Column(name = "renavam", length = 20)
    private String renavam;

    @Column(name = "chassi", length = 30)
    private String chassi;

    @Column(name = "marca", length = 100)
    private String marca;

    @EntityProperties(value = {"id", "descricao", "peso", "pesoTara", "pesoLiquido", "tipo", "textoTipo"}, collecions = {"setas"})
    @ManyToOne
    @JoinColumn(name = "id_tipo_veiculo")
    private TipoVeiculo tipo;

    @Column(name = "cor", length = 100)
    private String cor;

    @Column(name = "eixos", length = 2)
    private int eixos;

    @Enumerated(EnumType.STRING)
    @Column(name = "uf_placa_1", length = 2)
    private UFEnum ufPlaca1;

    @Enumerated(EnumType.STRING)
    @Column(name = "uf_placa_2", length = 2)
    private UFEnum ufPlaca2;

    @Enumerated(EnumType.STRING)
    @Column(name = "uf_placa_3", length = 2)
    private UFEnum ufPlaca3;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_vencimento_placa_1")
    private Date dataVencimentoPlaca1;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_vencimento_placa_2")
    private Date dataVencimentoPlaca2;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_vencimento_placa_3")
    private Date dataVencimentoPlaca3;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_civ_placa_1")
    private Date dataCivPlaca1;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_civ_placa_2")
    private Date dataCivPlaca2;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_civ_placa_3")
    private Date dataCivPlaca3;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_calibragem_placa_1")
    private Date dataCalibragemPlaca1;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_calibragem_placa_2")
    private Date dataCalibragemPlaca2;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_calibragem_placa_3")
    private Date dataCalibragemPlaca3;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_cipp_1")
    private Date dataCipp1;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_cipp_2")
    private Date dataCipp2;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_cipp_3")
    private Date dataCipp3;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_vencimento_cr")
    private Date dataVencimentoCr;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_vencimento_aatipp")
    private Date dataVencimentoAatipp;

    @Column(name = "antt", length = 20)
    private String antt;

    @EntityProperties(value = {"id", "descricao", "tipo", "conferenciaPeso", "buscarDadosRomaneio"}, targetEntity = Motivo.class)
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "tb_veiculo_motivo", schema = "sap", joinColumns = @JoinColumn(name = "id_veiculo"), inverseJoinColumns = @JoinColumn(name = "id_motivo"))
    private List<Motivo> motivos = new ArrayList<>();

    @Transient
    private String codigoTipoVeiculo;

    public Veiculo() {

    }

    @Builder(builderMethodName = "builderCargaPontual")
    public Veiculo(br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento.Veiculos veiculos, TipoVeiculo tipoVeiculo) {

        setStatus(StatusEnum.ATIVO);

        setStatusRegistro(StatusRegistroEnum.ATIVO);

        setStatusCadastroSap(StatusCadastroSAPEnum.NAO_ENVIADO);

        inicializarValoresPlaca1(veiculos);

        inicializarValoresPlaca2(veiculos.getPlaca1semi());

        inicializarValoresPlaca3(veiculos.getPlaca2semi());

        setTipo(tipoVeiculo);
    }

    public void setarValoresCargaPontual(Veiculos veiculos, TipoVeiculo tipoVeiculo) {

        setStatusCadastroSap(StatusCadastroSAPEnum.NAO_ENVIADO);

        inicializarValoresPlaca1(veiculos);

        inicializarValoresPlaca2(veiculos.getPlaca1semi());

        inicializarValoresPlaca3(veiculos.getPlaca2semi());

        if(ObjetoUtil.isNotNull(tipoVeiculo)){
            setTipo(tipoVeiculo);
        }
    }

    private void inicializarValoresPlaca3(Placa2semi placa2semi) {

        if (ObjetoUtil.isNotNull(placa2semi)) {

            if(StringUtil.isNotNullEmpty(placa2semi.getPlaca())){
                setPlaca3(placa2semi.getPlaca());
            }

            if (StringUtil.isNotNullEmpty(placa2semi.getUf())) {
                setUfPlaca3(UFEnum.get(placa2semi.getUf().split(" \\| ")[1].trim()));
            }

            if (StringUtil.isNotNullEmpty(placa2semi.getValidadeCRLV())) {
                setDataVencimentoPlaca3(DateUtil.format(placa2semi.getValidadeCRLV(), "yyyy-MM-dd"));
            }

            if (StringUtil.isNotNullEmpty(placa2semi.getValidadeCIV())) {
                setDataCivPlaca3(DateUtil.format(placa2semi.getValidadeCIV(), "yyyy-MM-dd"));
            }

            if (StringUtil.isNotNullEmpty(placa2semi.getValidadeCIPP())) {
                setDataCipp3(DateUtil.format(placa2semi.getValidadeCIPP(), "yyyy-MM-dd"));
            }

            if (StringUtil.isNotNullEmpty(placa2semi.getValidadeCalibragem())) {
                setDataCalibragemPlaca3(DateUtil.format(placa2semi.getValidadeCalibragem(), "yyyy-MM-dd"));
            }
        }
    }

    private void inicializarValoresPlaca2(Placa1semi placa1semi) {

        if (ObjetoUtil.isNotNull(placa1semi)) {

            if(StringUtil.isNotNullEmpty(placa1semi.getPlaca())){
                setPlaca2(placa1semi.getPlaca());
            }

            if (StringUtil.isNotNullEmpty(placa1semi.getUf())) {
                setUfPlaca2(UFEnum.get(placa1semi.getUf().split(" \\| ")[1].trim()));
            }

            if (StringUtil.isNotNullEmpty(placa1semi.getValidadeCRLV())) {
                setDataVencimentoPlaca2(DateUtil.format(placa1semi.getValidadeCRLV(), "yyyy-MM-dd"));
            }

            if (StringUtil.isNotNullEmpty(placa1semi.getValidadeCIV())) {
                setDataCivPlaca2(DateUtil.format(placa1semi.getValidadeCIV(), "yyyy-MM-dd"));
            }

            if (StringUtil.isNotNullEmpty(placa1semi.getValidadeCIPP())) {
                setDataCipp2(DateUtil.format(placa1semi.getValidadeCIPP(), "yyyy-MM-dd"));
            }

            if (StringUtil.isNotNullEmpty(placa1semi.getValidadeCalibragem())) {
                setDataCalibragemPlaca2(DateUtil.format(placa1semi.getValidadeCalibragem(), "yyyy-MM-dd"));
            }
        }
    }

    private void inicializarValoresPlaca1(Veiculos veiculos) {

        if (ObjetoUtil.isNotNull(veiculos) && ObjetoUtil.isNotNull(veiculos.getPlacaCavalo())) {

            if(StringUtil.isNotNullEmpty(veiculos.getPlacaCavalo().getPlaca())){
                setPlaca1(veiculos.getPlacaCavalo().getPlaca());
            }

            if (StringUtil.isNotNullEmpty(veiculos.getPlacaCavalo().getCidade())) {
                setMunicipio(veiculos.getPlacaCavalo().getCidade().split(" \\| ")[1].trim());
            }

            if (StringUtil.isNotNullEmpty(veiculos.getPlacaCavalo().getUf())) {
                setUfPlaca1(UFEnum.get(veiculos.getPlacaCavalo().getUf().split(" \\| ")[1].trim()));
            }

            if (StringUtil.isNotNullEmpty(veiculos.getPlacaCavalo().getValidadeCRLV())) {
                setDataVencimentoPlaca1(DateUtil.format(veiculos.getPlacaCavalo().getValidadeCRLV(), "yyyy-MM-dd"));
            }

            if (StringUtil.isNotNullEmpty(veiculos.getPlacaCavalo().getValidadeCIV())) {
                setDataCivPlaca1(DateUtil.format(veiculos.getPlacaCavalo().getValidadeCIV(), "yyyy-MM-dd"));
            }

            if (StringUtil.isNotNullEmpty(veiculos.getPlacaCavalo().getValidadeCIPP())) {
                setDataCipp1(DateUtil.format(veiculos.getPlacaCavalo().getValidadeCIPP(), "yyyy-MM-dd"));
            }

            if (StringUtil.isNotNullEmpty(veiculos.getPlacaCavalo().getValidadeCR())) {
                setDataVencimentoCr(DateUtil.format(veiculos.getPlacaCavalo().getValidadeCR(), "yyyy-MM-dd"));
            }

            if (StringUtil.isNotNullEmpty(veiculos.getPlacaCavalo().getValidadeAATIPP())) {
                setDataVencimentoAatipp(DateUtil.format(veiculos.getPlacaCavalo().getValidadeAATIPP(), "yyyy-MM-dd"));
            }

            if (StringUtil.isNotNullEmpty(veiculos.getPlacaCavalo().getValidadeCalibragem())) {
                setDataCalibragemPlaca1(DateUtil.format(veiculos.getPlacaCavalo().getValidadeCalibragem(), "yyyy-MM-dd"));
            }
        }
    }

    public String getDescricaoFormatada() {

        return StringUtil.isEmpty(placa1) ? null : this.placa1.substring(0, 3).toUpperCase() + "-" + this.placa1.substring(3);
    }

    public String getPlacaMaisUf() {

        return StringUtil.isEmpty(placa1) || ObjetoUtil.isNull(ufPlaca1) ? null : this.placa1.substring(0, 3).toUpperCase() + "-" + this.placa1.substring(3) + " " + this.getUfPlaca1().getSigla();
    }

    public VeiculoResponse toResponse() {

        final VeiculoResponse response = new VeiculoResponse();

        response.setCodigo(this.getCodigo());

        response.setPlaca(this.getPlaca1());

        response.setTpVeiculo(this.getTipo() != null ? this.getTipo().toString() : "");

        return response;
    }

}
