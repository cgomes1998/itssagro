package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class RfcVeiculoResponseItem extends JCoItem {

    @SAPColumn(name = "PLACA")
    private String placa;

    @SAPColumn(name = "TP_VEI")
    private String tipoVeiculo;

    @SAPColumn(name = "TP_VEI_AGRO")
    private String codigo;

    @SAPColumn(name = "STATUS")
    private String status;

}
