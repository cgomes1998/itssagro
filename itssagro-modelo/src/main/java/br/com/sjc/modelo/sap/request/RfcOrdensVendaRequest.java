package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@SAPParameter( function = "ZITSSAGRO_SALESORDER_GETLIST" )
@NoArgsConstructor
@AllArgsConstructor
public class RfcOrdensVendaRequest extends JCoRequest {

    @SAPColumn( name = "CUSTOMER_NUMBER", size = 10 )
    private String codigoCliente;

    @SAPColumn( name = "SALES_ORGANIZATION", size = 4 )
    private String organizacaoVendas;

    @SAPColumn( name = "MATERIAL", size = 18 )
    private String codigoMaterial;
}
