package br.com.sjc.modelo.sap.sync;

import br.com.sjc.modelo.sap.Veiculo;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import br.com.sjc.modelo.enums.UFEnum;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SAPVeiculo extends SAPEntidade {

    @SAPColumn(name = "ID_VEICULO")
    private String idVeiculo;

    @SAPColumn(name = "C_PLACA1")
    private String placa1;

    @SAPColumn(name = "C_UF1")
    private String uf1;

    @SAPColumn(name = "C_PLACA2")
    private String placa2;

    @SAPColumn(name = "C_UF2")
    private String uf2;

    @SAPColumn(name = "C_PLACA3")
    private String placa3;

    @SAPColumn(name = "C_UF3")
    private String uf3;

    @SAPColumn(name = "C_MUNICIPIO")
    private String municipio;

    @SAPColumn(name = "C_RENAVAM")
    private String renvam;

    @SAPColumn(name = "C_CHASSI")
    private String chassi;

    @SAPColumn(name = "C_MARCA")
    private String marca;

    @SAPColumn(name = "C_TIPO")
    private String tipo;

    @SAPColumn(name = "C_COR")
    private String cor;

    @SAPColumn(name = "C_EIXOS")
    private String eixos;

    @SAPColumn(name = "C_CODVEI")
    private String codigo;

    @SAPColumn(name = "BLOQUEADO")
    private String bloqueado;

    @SAPColumn(name = "MARC_ELIM")
    private String eliminado;

    private StatusRegistroEnum statusRegistroSAP;

    public enum COLUMN_INPUT {

        C_DT_ULT_INF,

        C_COD_VEIC,

        C_PRE_CAD,

        C_PLACA1,

        C_UF1,

        C_PLACA2,

        C_UF2,

        C_PLACA3,

        C_UF3,

        C_MUNICIPIO,

        C_RENAVAM,

        C_CHASSI,

        C_MARCA,

        C_TIPO,

        C_TIPO1,

        C_COR,

        C_EIXOS,

        ID_VEICULO;
    }

    /**
     * Método responsável por criar instancias da entidade <code>SAPVeiculo</code>
     *
     * @return <code>SAPVeiculo</code>
     * @author Bruno Zafalão
     */
    public static SAPVeiculo newInstance() {

        return new SAPVeiculo();
    }

    /**
     * Retorna o valor do atributo <code>statusRegistroSAP</code>
     *
     * @return <code>StatusRegistroEnum</code>
     */
    public StatusRegistroEnum getStatusRegistroSAP() {

        this.statusRegistroSAP = StatusRegistroEnum.ATIVO;

        if (StringUtil.isNotNullEmpty(this.getBloqueado())) {

            this.statusRegistroSAP = StatusRegistroEnum.BLOQUEADO;
        }

        if (StringUtil.isNotNullEmpty(this.getEliminado())) {

            this.statusRegistroSAP = StatusRegistroEnum.ELIMINADO;
        }

        if (StringUtil.isNotNullEmpty(this.getBloqueado()) && StringUtil.isNotNullEmpty(this.getEliminado())) {

            this.statusRegistroSAP = StatusRegistroEnum.BLOQUEADO_E_ELIMINADO;
        }

        return this.statusRegistroSAP;
    }

    public Veiculo copy(Veiculo entidadeSaved) {

        if (ObjetoUtil.isNull(entidadeSaved)) {

            entidadeSaved = new Veiculo();

            entidadeSaved.setDataCadastro(DateUtil.hoje());
        }

        if (ObjetoUtil.isNull(entidadeSaved.getId())) {

            entidadeSaved.setId(StringUtil.isNotNullEmpty(this.getIdVeiculo()) ? Long.valueOf(this.getIdVeiculo().trim()) : null);
        }

        if (StringUtil.isNotNullEmpty(this.getUf1()) && StringUtil.isNotNullEmpty(this.getPlaca1())) {

            entidadeSaved.setCodigo(this.getUf1() + this.getPlaca1());
        }

        entidadeSaved.setPlaca1(StringUtil.isNotNullEmpty(this.getPlaca1()) ? this.getPlaca1().trim() : entidadeSaved.getPlaca1());

        entidadeSaved.setUfPlaca1(ObjetoUtil.isNotNull(this.getUf1()) ? UFEnum.get(this.getUf1()) : entidadeSaved.getUfPlaca1());

        entidadeSaved.setMunicipio(StringUtil.isNotNullEmpty(this.getMunicipio()) ? this.getMunicipio().trim() : entidadeSaved.getMunicipio());

        entidadeSaved.setRenavam(StringUtil.isNotNullEmpty(this.getRenvam()) ? this.getRenvam().trim() : entidadeSaved.getRenavam());

        entidadeSaved.setPlaca2(StringUtil.isNotNullEmpty(this.getPlaca2()) ? this.getPlaca2().trim() : entidadeSaved.getPlaca2());

        entidadeSaved.setUfPlaca2(UFEnum.get(this.getUf2()));

        entidadeSaved.setPlaca3(StringUtil.isNotNullEmpty(this.getPlaca3()) ? this.getPlaca3().trim() : entidadeSaved.getPlaca3());

        entidadeSaved.setUfPlaca3(UFEnum.get(this.getUf3()));

        entidadeSaved.setCor(StringUtil.isNotNullEmpty(this.getCor()) ? this.getCor().trim() : entidadeSaved.getCor());

        entidadeSaved.setChassi(StringUtil.isNotNullEmpty(this.getChassi()) ? this.getChassi().trim() : entidadeSaved.getChassi());

        entidadeSaved.setMarca(StringUtil.isNotNullEmpty(this.getMarca()) ? this.getMarca().trim() : entidadeSaved.getMarca());

        if (StringUtil.isNotNullEmpty(this.getEixos())) {

            entidadeSaved.setEixos(Integer.parseInt(this.getEixos()));
        }

        entidadeSaved.setCodigoTipoVeiculo(StringUtil.isNotNullEmpty(this.getTipo()) ? this.getTipo().trim() : entidadeSaved.getCodigoTipoVeiculo());

        entidadeSaved.setCodigo(StringUtil.isNotNullEmpty(this.getCodigo()) ? this.getCodigo().trim() : entidadeSaved.getCodigo());

        entidadeSaved.setLogSAP("VEÍCULO SINCRONIZADO COM O SAP EM " + DateUtil.format("dd/MM/yyyy HH:mm:ss", DateUtil.hoje()));

        if (StringUtil.isNotNullEmpty(this.getCodigo())) {

            entidadeSaved.setLogSAP(entidadeSaved.getLogSAP() + "\nCÓDIGO: " + this.getCodigo());
        }

        entidadeSaved.setStatusCadastroSap(StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO);

        entidadeSaved.setStatus(StatusEnum.ATIVO);

        entidadeSaved.setStatusRegistro(this.getStatusRegistroSAP());

        return entidadeSaved;
    }
}
