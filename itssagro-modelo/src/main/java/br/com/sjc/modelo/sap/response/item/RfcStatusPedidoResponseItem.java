package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.enums.StatusPedidoEnum;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class RfcStatusPedidoResponseItem extends JCoItem {

    @SAPColumn(name = "EBELN")
    private String numeroPedido;

    @SAPColumn(name = "FRGKE")
    private String codigoStatus;

    @SAPColumn(name = "ROMANEIO")
    private String romaneio;

    public StatusPedidoEnum getStatusPedido() {
        return StatusPedidoEnum.obterStatusPorCodigo(new Integer(codigoStatus));
    }
}
