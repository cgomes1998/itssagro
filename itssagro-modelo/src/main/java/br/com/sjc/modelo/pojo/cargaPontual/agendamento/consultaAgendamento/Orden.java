package br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Orden {

    private int idOrdem;

    private String numeroOrigem;

    private String documentoOrigem;

    private String tipoDocumento;

    private String serieOrigem;

    private String tipoFrete;

    private String cidadeDestino;

    private String agendadoPorto;

    private String trading;

    private String tipoDistribuicao;

    private String clienteFornecedor;

    private Object clienteVendaFornecedorCompra;

    private String localCargaDescarga;

    private Produto produtos;

}
