package br.com.sjc.modelo.dto;

import lombok.*;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by julio.bueno on 06/02/2020
 */
@Getter
@Setter
@Data
public class RelatorioAnaliseConversaoDTO {

    private Long tanque;
    private Long conversaoListagemId;
    private String numeroRomaneio;
    private Date dataConversao;
    private BigDecimal tempTanque;
    private BigDecimal grauInpm;
    private BigDecimal acidez;
    private BigDecimal volumeSetaVinteGraus;
    private BigDecimal volumeDensidadeLVinteGraus;
    private BigDecimal diferencaLitros;
    private Long quantidadeConversoes;

    public RelatorioAnaliseConversaoDTO() {
    }

    public RelatorioAnaliseConversaoDTO(Long tanque, Long conversaoListagemId, String numeroRomaneio, Date dataConversao, BigDecimal tempTanque, BigDecimal grauInpm, BigDecimal acidez, BigDecimal volumeSetaVinteGraus, BigDecimal volumeDensidadeLVinteGraus, BigDecimal diferencaLitros) {
        this.tanque = tanque;
        this.conversaoListagemId = conversaoListagemId;
        this.numeroRomaneio = numeroRomaneio;
        this.dataConversao = dataConversao;
        this.tempTanque = tempTanque;
        this.grauInpm = grauInpm;
        this.acidez = acidez;
        this.volumeSetaVinteGraus = volumeSetaVinteGraus;
        this.volumeDensidadeLVinteGraus = volumeDensidadeLVinteGraus;
        this.diferencaLitros = diferencaLitros;
    }
}
