package br.com.sjc.modelo.response;

import br.com.sjc.modelo.*;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEtapaEnum;
import br.com.sjc.modelo.sap.Centro;
import br.com.sjc.modelo.sap.ConversaoLitragem;
import br.com.sjc.modelo.sap.ItemNFPedido;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
public class RomaneioEstornadoResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private Pesagem pesagem;
    private Centro centro;
    private Usuario usuarioAprovador;
    private ConversaoLitragem conversaoLitragem;
    private String observacao;
    private String observacaoNota;
    private java.util.List<ItemNFPedido> itens;
    private List<AnaliseQualidade> analises;
    private List<Motivo> motivos;
    private Senha senha;
    private Integer numeroCartaoAcesso;
    private Carregamento carregamento;
    private Lacragem lacragem;
    private DadosSincronizacao operacao;
    private StatusRomaneioEnum statusRomaneio;
    private StatusRomaneioEtapaEnum statusEtapa;
}
