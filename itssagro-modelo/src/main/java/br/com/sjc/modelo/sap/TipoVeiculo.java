package br.com.sjc.modelo.sap;

import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_tipo_veiculo", schema = "sap")
public class TipoVeiculo extends EntidadeSAPPre {

    @Column(name = "desc_tipo")
    private String descricao;

    @Column(name = "peso_tipo")
    private BigDecimal peso;

    @Column(name = "peso_tara")
    private BigDecimal pesoTara;

    @Column(name = "peso_liquido")
    private BigDecimal pesoLiquido;

    @Column
    private String tipo;

    @Column(name = "txt_tipo")
    private String textoTipo;

    @EntityProperties(value = {"id", "tipoVeiculo.id", "descricao", "qtd", "padrao"}, targetEntity = TipoVeiculoSeta.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "tipoVeiculo", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    private List<TipoVeiculoSeta> setas = new ArrayList<>();

    public TipoVeiculo() {

        super();
    }

    public TipoVeiculo(Long idTipoVeiculo) {

        this.setId(idTipoVeiculo);
    }

    public TipoVeiculo(final String codigo, final String descricao, final BigDecimal peso, final String tipo, final String textoTipo) {

        this.setCodigo(codigo);

        this.setDescricao(descricao);

        this.setPeso(peso);

        this.setTipo(tipo);

        this.setTextoTipo(textoTipo);
    }

    public TipoVeiculo(final Long id, final String codigo, final String descricao, final String tipo, final String textoTipo, final BigDecimal peso, final BigDecimal pesoTara, final BigDecimal pesoLiquido) {

        this.setId(id);

        this.setCodigo(codigo);

        this.setDescricao(descricao);

        this.setTipo(tipo);

        this.setTextoTipo(textoTipo);

        this.setPeso(peso);

        this.setPesoTara(pesoTara);

        this.setPesoLiquido(pesoLiquido);
    }

    public String getLabel() {

        return this.toString();
    }

    @Override
    public String toString() {

        if ((this.tipo != null && !this.tipo.trim().equals("")) && (this.textoTipo != null && !this.textoTipo.trim().equals(""))) {

            return this.descricao + " - " + this.toMilhar(this.peso) + " / " + this.tipo + " - " + this.textoTipo;
        }

        return this.descricao + " - " + this.toMilhar(this.peso);
    }

    private String toMilhar(final BigDecimal valor) {

        if (ObjetoUtil.isNull(valor))
            return "";

        Locale brasil = new Locale("pt", "BR");

        DecimalFormat df = new DecimalFormat("#,###.######", new DecimalFormatSymbols(brasil));

        return df.format(valor);
    }

}
