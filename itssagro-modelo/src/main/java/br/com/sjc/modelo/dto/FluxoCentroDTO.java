package br.com.sjc.modelo.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FluxoCentroDTO extends DataDTO {

    private FluxoDTO fluxo;

    private CentroDTO centro;

}
