package br.com.sjc.modelo.enums;

public enum TipoRestricaoEnum {

    SENHA,
    CRIAR_REMESSA,
    ESTORNAR_REMESSA,
    ANALISE_QUALIDADE,
    CRIAR_DOC_TRANSPORTE,
    SALDO_ORDEM_VENDA,
    ORDEM_VENDA_BLOQUEADA,
    SALDO_PRODUTO_ACABADO,
    CONFERENCIA_PESO;
}
