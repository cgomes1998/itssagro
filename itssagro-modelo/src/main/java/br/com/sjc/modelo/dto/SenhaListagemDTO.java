package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusSenhaEnum;
import br.com.sjc.modelo.sap.Cliente;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.modelo.sap.Motorista;
import br.com.sjc.modelo.sap.Veiculo;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class SenhaListagemDTO extends DataDTO {

    @ProjectionProperty
    private String senha;

    @ProjectionProperty
    private StatusSenhaEnum statusSenha;

    @ProjectionProperty
    private Date dataCadastro;

    @ProjectionProperty
    private Date dataAlteracao;

    @ProjectionProperty(values = {"id", "codigo", "descricao"})
    private Material material;

    @ProjectionProperty(values = {"id", "placa1"})
    private Veiculo veiculo;

    @ProjectionProperty(values = {"id", "codigo", "cpfCnpj", "nome"})
    private Cliente cliente;

    @ProjectionProperty(values = {"id", "nome"})
    private Motorista motorista;

    private Boolean podeAlterar;
}
