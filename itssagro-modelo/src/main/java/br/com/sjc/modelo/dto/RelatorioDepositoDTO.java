package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.DirecaoOperacaoDepositoEnum;
import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.util.BigDecimalUtil;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
public class RelatorioDepositoDTO {

	private boolean possuiRateio;

	private String operacao;

	private String depositoEntrada;

	private String depositoSaida;

	private DirecaoOperacaoDepositoEnum direcaoOperacaoDeposito;

	private String numeroRomaneio;

	private StatusIntegracaoSAPEnum statusIntegracaoSAP;

	private Date dataCriacaoRomaneio;

	private String veiculo;

	private String depositante;

	private String material;

	private String nfe;

	private Date dtSimplesEntradaReferencia;

	private Date dataClassificacao;

	private Date dataPrimeiraPesagem;

	private Date dataSegundaPesagem;

	private BigDecimal pesoBruto;

	private BigDecimal pesoTara;

	private BigDecimal pesoLiquidoUmido;

	private BigDecimal pesoBrutoOrigem;

	private BigDecimal pesoTaraOrigem;

	private BigDecimal pesoLiquidoUmidoOrigem;

	private BigDecimal pesoLiquidoSecoOrigem;

	private BigDecimal rateio;

	private BigDecimal quantidadeNfe;

	private BigDecimal desconto;

	private BigDecimal pesoLiquidoSeco;

	private String requestKey;

	private Long idClassificacao;

	private Map<String, Double> valoresItensClassificacao;

	public RelatorioDepositoDTO() {

	}

	public RelatorioDepositoDTO(boolean possuiRateio, RomaneioDepositoDTO romaneioDepositoDTO, Collection<ItemNFNotaListagemDTO> notas) {

		this.setPossuiRateio(possuiRateio);

		this.setOperacao(romaneioDepositoDTO.getOperacao());

		this.setDepositoEntrada(romaneioDepositoDTO.getDepositoEntrada());

		this.setDepositoSaida(romaneioDepositoDTO.getDepositoSaida());

		this.setDirecaoOperacaoDeposito(romaneioDepositoDTO.getDirecaoOperacaoDeposito());

		this.setNumeroRomaneio(romaneioDepositoDTO.getNumeroRomaneio());

		this.setStatusIntegracaoSAP(romaneioDepositoDTO.getStatusIntegracaoSAP());

		this.setDataCriacaoRomaneio(romaneioDepositoDTO.getDataCriacaoRomaneio());

		this.setVeiculo(romaneioDepositoDTO.getVeiculo());

		this.setDepositante(romaneioDepositoDTO.getDepositante());

		this.setMaterial(romaneioDepositoDTO.getMaterial());

		if (ColecaoUtil.isNotEmpty(notas) && StringUtil.isNotNullEmpty(romaneioDepositoDTO.getRequestKey())) {

			ItemNFNotaListagemDTO nota = notas.stream().filter(n -> StringUtil.isNotNullEmpty(n.getRequestKey()) && n.getRequestKey().equals(romaneioDepositoDTO.getRequestKey())).findFirst().orElse(null);

			if (ObjetoUtil.isNotNull(nota)) {

				this.setNfe(StringUtil.isNotNullEmpty(nota.getNumeroNfe()) ? nota.getNumeroNfe() : nota.getNumeroNotaFiscalTransporte());
			}
		}

		this.setDtSimplesEntradaReferencia(romaneioDepositoDTO.getDtSimplesEntradaReferencia());

		this.setDataClassificacao(romaneioDepositoDTO.getDataClassificacao());

		this.setDataPrimeiraPesagem(romaneioDepositoDTO.getDataPrimeiraPesagem());

		this.setDataSegundaPesagem(romaneioDepositoDTO.getDataSegundaPesagem());

		this.setPesoBruto(romaneioDepositoDTO.getPesoBruto());

		this.setPesoTara(romaneioDepositoDTO.getPesoTara());

		this.setPesoLiquidoUmido(romaneioDepositoDTO.getPesoLiquidoUmido());

		this.setPesoBrutoOrigem(romaneioDepositoDTO.getPesoBrutoOrigem());

		this.setPesoTaraOrigem(romaneioDepositoDTO.getPesoTaraOrigem());

		this.setPesoLiquidoUmidoOrigem(romaneioDepositoDTO.getPesoLiquidoUmidoOrigem());

		this.setPesoLiquidoSecoOrigem(romaneioDepositoDTO.getPesoLiquidoSecoOrigem());

		this.setRateio(romaneioDepositoDTO.getRateio());

		this.setRequestKey(romaneioDepositoDTO.getRequestKey());

		this.setIdClassificacao(romaneioDepositoDTO.getIdClassificacao());

		this.setQuantidadeNfe(romaneioDepositoDTO.getQuantidadeNfe());
	}

}
