package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.ModuloOperacaoEnum;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RomaneioRequestKeyDTO {

	private String modulo;

	private String operacao;

	private String requestKey;

	private String numeroRomaneio;

	public RomaneioRequestKeyDTO(
		   final ModuloOperacaoEnum modulo,
		   final String operacao,
		   final String requestKey,
		   final String numeroRomaneio
	) {

		this.modulo = modulo.getCodigo();

		this.operacao = operacao;

		this.requestKey = StringUtil.completarZeroAEsquerda(
			   20,
			   requestKey
		);

		this.numeroRomaneio = numeroRomaneio;
	}

}
