package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.EtapaEnum;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class EtapaFilaDTO extends DataDTO {

    private Long sequencia;

    private EtapaEnum etapa;

    private List<EtapaSenhaFilaDTO> senhas;

}
