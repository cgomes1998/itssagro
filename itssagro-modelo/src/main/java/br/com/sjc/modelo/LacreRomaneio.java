package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.StatusLacreRomaneioEnum;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@Entity
@Table(name = "tb_lacragem_lacre", schema = "sap")
public class LacreRomaneio implements ObjectID<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "dt_utilizacao")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dataUtilizacao;

    @EntityProperties(value = {"id", "nome", "login", "cpf"})
    @ManyToOne
    @JoinColumn(name = "id_usuario_que_utilizou")
    private Usuario usuarioQueUtilizou;

    @EntityProperties(value = {"id"})
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_lacragem", referencedColumnName = "id")
    private Lacragem lacragem;

    @EntityProperties(value = {"id", "lote", "codigo", "observacaoStatusLacreAlterar", "statusLacre", "dataInutilizacao", "usuarioQueInutilizou.id", "usuarioQueInutilizou.login", "usuarioQueInutilizou.nome", "usuarioQueInutilizou.cpf",
            "motivoInutilizacao.id", "motivoInutilizacao.descricao", "motivoInutilizacao.tipo"})
    @ManyToOne
    @JoinColumn(name = "id_lacre", referencedColumnName = "id")
    private Lacre lacre;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_lacre")
    private StatusLacreRomaneioEnum statusLacreRomaneio;

    public LacreRomaneio() {

    }

    public LacreRomaneio(Lacre lacre, StatusLacreRomaneioEnum statusLacreRomaneio) {

        this.setLacre(lacre);
        this.setStatusLacreRomaneio(statusLacreRomaneio);
    }

}
