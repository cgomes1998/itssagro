package br.com.sjc.modelo.relatorio;

import br.com.sjc.modelo.AnaliseItem;
import br.com.sjc.modelo.AnaliseQualidade;
import br.com.sjc.modelo.PortariaTicket;
import br.com.sjc.modelo.dto.*;
import br.com.sjc.modelo.response.FileResponse;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.modelo.sap.response.item.RfcOrdensVendaItemResponse;
import br.com.sjc.modelo.sap.response.item.RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_DADOS_Item;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
class TabelaExcel {

    private String bridge;

    private String codigoGrupo;

    private String itemGrupo;

    private String valor;

}

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
class TabelaClassificacaoManual {

    private String tabela;

    private String material;

    private String item;

    private String indice;

    private String percentual;

    private String descricao;

}

/**
 * <p>
 * <b>Title:</b> GerarRelatorioExcel.java
 * </p>
 * <p>
 * <p>
 * <b>Description:</b>
 * </p>
 * <p>
 * <p>
 * <b>Company: </b> Fábrica de Software -ITSS Tecnologia
 * </p>
 *
 * @author Claudio Gomes
 * @version 1.0.0
 */
public class GerarRelatorioExcel {

    public static String converterParaString(Cell cell, boolean... itemGrupo) {

        String valor = "";

        switch (cell.getCellType()) {
            case NUMERIC:
                if (itemGrupo != null) {
                    valor = String.valueOf((int) cell.getNumericCellValue());
                } else {
                    valor = String.valueOf(cell.getNumericCellValue());
                }
                break;
            case STRING:
                valor = cell.getStringCellValue();
                break;
        }

        return valor;
    }

    public static FileResponse obterRelatorioLoteAnaliseQualidade(Collection<LoteAnaliseQualidadeDTO> lotes) {

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {

            XSSFSheet sheet = workbook.createSheet("Relatório de Lotes");

            GerarRelatorioExcel.montarRelatorioDeLotes(lotes, sheet, workbook);

            return escreverArquivo(workbook);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }

    public static FileResponse obterRelatorioDeLacresVinculadosARomaneio(List<RelatorioLacreDTO> lacres) {

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {

            XSSFSheet sheet = workbook.createSheet("Relatório de Lacres");

            GerarRelatorioExcel.montarRelatorioDeLacres(lacres, sheet, workbook);

            return escreverArquivo(workbook);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }

    public static FileResponse obterRelatorioDeDepositos(List<RelatorioDepositoDTO> depositos, List<ItemClassificacaoDepositoDTO> itensClassificacao, List<ItemClassificacaoDepositoDTO> itensClassificacaoOrigem) {

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {

            XSSFSheet sheet = workbook.createSheet("Relatório de Romaneios Grãos");

            GerarRelatorioExcel.montarRelatorioDeDepositos(depositos, itensClassificacao, itensClassificacaoOrigem, sheet, workbook);

            return escreverArquivo(workbook);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }

    public static FileResponse obterRelatorioDeDepositosFornecedorGraosPagamento(List<RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_DADOS_Item> t_dados) {

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {

            XSSFSheet sheet = workbook.createSheet("Relatório de Romaneios Grãos - Pagamento");

            GerarRelatorioExcel.montarRelatorioPortalFornecedorGraosPagamento(t_dados, sheet, workbook);

            return escreverArquivo(workbook);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }

    public static FileResponse obterRelatorioDeDepositosFornecedorGraosEntregas(List<RelatorioDepositoDTO> depositos, List<ItemClassificacaoDepositoDTO> itensClassificacao) {

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {

            XSSFSheet sheet = workbook.createSheet("Relatório de Romaneios Grãos - Entregas");

            GerarRelatorioExcel.montarRelatorioPortalFornecedorGraosEntregas(depositos, itensClassificacao, sheet, workbook);

            return escreverArquivo(workbook);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }

    public static FileResponse obterRelatorioPesagemManuais(List<PesagemManualDTO> pesagens) {

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {

            XSSFSheet sheet = workbook.createSheet("Relatório de Pesagens manuais");

            GerarRelatorioExcel.montarRelatorioPesagensManuais(pesagens, sheet, workbook);

            return escreverArquivo(workbook);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }

    public static FileResponse obterRelatorioOrdemVenda(Collection<RfcOrdensVendaItemResponse> ordens, Collection<TextoOrdemDTO> textosDasOrdens) {

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {

            XSSFSheet sheet = workbook.createSheet("Relatório Ordens de venda");

            GerarRelatorioExcel.montarRelatorioOrdemVenda(ordens, textosDasOrdens, sheet, workbook);

            return escreverArquivo(workbook);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }

    public static FileResponse obterRelatorioLaudoAnalitico(Collection<RelatorioLaudoAnalitico> laudos) {

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {

            XSSFSheet sheet = workbook.createSheet("Relatório Laudos Analíticos");

            GerarRelatorioExcel.montarRelatorioLaudoAnalitico(laudos, sheet, workbook);

            return escreverArquivo(workbook);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }

    public static FileResponse obterRelatorioLaudoSintetico(Collection<RelatorioLaudoSintetico> laudos) {

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {

            XSSFSheet sheet = workbook.createSheet("Relatório Laudos Sintéticos");

            GerarRelatorioExcel.montarRelatorioLaudoSintetico(laudos, sheet, workbook);

            return escreverArquivo(workbook);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }

    public static FileResponse obterRelatorioRomaneiosGestaoItssAgro(Collection<RomaneioGestaoItssAgroDTO> romaneios) {

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {

            XSSFSheet sheet = workbook.createSheet("Relatório de romaneios");

            GerarRelatorioExcel.montarRelatorioRomaneioGestaoItssAgro(romaneios, sheet, workbook);

            return escreverArquivo(workbook);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }

    public static FileResponse obterRelatorioFilas(Collection<SenhaEtapaRelatorioDTO> senhaEtapas) {

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {

            XSSFSheet sheet = workbook.createSheet("Relatório de Filas");

            GerarRelatorioExcel.montarRelatorioFilas(senhaEtapas, sheet, workbook);

            return escreverArquivo(workbook);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }

    public static FileResponse obterRelatorioPortariaTicket(Collection<PortariaTicket> portariaTicket) {

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {

            XSSFSheet sheet = workbook.createSheet("Relatório de Portaria Tickets");

            GerarRelatorioExcel.montarRelatorioPortariaTicket(portariaTicket, sheet, workbook);

            return escreverArquivo(workbook);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }

    private static void montarRelatorioRomaneioGestaoItssAgro(Collection<RomaneioGestaoItssAgroDTO> romaneios, XSSFSheet sheet, XSSFWorkbook workbook) {

        GerarRealtorioExcelStyles styles = GerarRealtorioExcelStyles.build(workbook);

        Row rowTituloTabela = sheet.createRow(1);

        rowTituloTabela.setHeight((short) 500);

        sheet.setColumnWidth(0, 500);

        GerarRelatorioExcel.criarCell(rowTituloTabela, 0, styles.getCellStyleLabel(), "Sistema");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 1, styles.getCellStyleLabel(), "Motorista");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 2, styles.getCellStyleLabel(), "Placa");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 3, styles.getCellStyleLabel(), "Nº Romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 4, styles.getCellStyleLabel(), "Data 1ª Pesagem");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 5, styles.getCellStyleLabel(), "Peso Bruto");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 6, styles.getCellStyleLabel(), "Peso Tara");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 7, styles.getCellStyleLabel(), "Data 2ª Pesagem");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 8, styles.getCellStyleLabel(), "Peso líquido");
        

        AtomicInteger linha = new AtomicInteger(2);

        romaneios.stream().forEach(romaneio -> {
            Row rowItem = sheet.createRow(linha.get());

            rowItem.setHeight((short) 300);

            GerarRelatorioExcel.criarCell(rowItem, 0, styles.getCellStyleValue(), romaneio.isGestaoPatio() ? "Gestão de pátio" : "Itss Agro");
            GerarRelatorioExcel.criarCell(rowItem, 1, styles.getCellStyleValue(), ObjetoUtil.isNotNull(romaneio.getMotorista()) ? romaneio.getMotorista() : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 2, styles.getCellStyleValue(), ObjetoUtil.isNotNull(romaneio.getPlaca()) ? romaneio.getPlaca() : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 3, styles.getCellStyleValue(), ObjetoUtil.isNotNull(romaneio.getNumRomaneio()) ? romaneio.getNumRomaneio() : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 4, styles.getCellStyleValue(), ObjetoUtil.isNotNull(romaneio.getDataPrimeiraPesagem()) ? DateUtil.format("dd/MM/yyyy HH:mm", romaneio.getDataPrimeiraPesagem()) : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 5, styles.getCellStyleValue(), ObjetoUtil.isNotNull(romaneio.getPesoBruto()) ? romaneio.getPesoBruto() : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 6, styles.getCellStyleValue(), ObjetoUtil.isNotNull(romaneio.getPesoTara()) ? romaneio.getPesoTara() : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 7, styles.getCellStyleValue(), ObjetoUtil.isNotNull(romaneio.getDataSegundaPesagem()) ? DateUtil.format("dd/MM/yyyy HH:mm", romaneio.getDataSegundaPesagem()) : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 8, styles.getCellStyleValue(), ObjetoUtil.isNotNull(romaneio.getPesoLiquido()) ? romaneio.getPesoLiquido() : StringUtil.empty());
            linha.set(linha.incrementAndGet());
        });

        for (int i = 0; i <= 28; i++) {

            sheet.autoSizeColumn(i);
        }
    }

    public static FileResponse obterRelatorioComercial_V2(final Collection<RelatorioComercial_V2_DTO> dtos) {

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {

            XSSFSheet sheet = workbook.createSheet("Relatório Comercial");

            GerarRelatorioExcel.montarRelatorioComercial_V2(dtos, sheet, workbook);

            return escreverArquivo(workbook);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }

    public static FileResponse obterRelatorioComercial(final Collection<RelatorioComercialDTO> dtos) {

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {

            XSSFSheet sheet = workbook.createSheet("Relatório Comercial");

            GerarRelatorioExcel.montarRelatorioComercial(dtos, sheet, workbook);

            return escreverArquivo(workbook);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }

    public static FileResponse obterRelatorioFluxoDeCarregamento(final Collection<RelatorioFluxoDeCarregamentoDTO> dtos) {

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {

            XSSFSheet sheet = workbook.createSheet("Relatório Fluxo de Carregamento");

            GerarRelatorioExcel.montarRelatorioFluxoDeCarregamento(dtos, sheet, workbook);

            return escreverArquivo(workbook);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }

    private static void montarRelatorioDeLotes(Collection<LoteAnaliseQualidadeDTO> lotes, XSSFSheet sheet, XSSFWorkbook workbook) {

        GerarRealtorioExcelStyles styles = GerarRealtorioExcelStyles.build(workbook);

        Row rowTituloTabela = sheet.createRow(1);

        rowTituloTabela.setHeight((short) 500);

        sheet.setColumnWidth(0, 500);

        GerarRelatorioExcel.criarCell(rowTituloTabela, 0, styles.getCellStyleLabel(), "Código");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 1, styles.getCellStyleLabel(), "Data de Fabricação");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 2, styles.getCellStyleLabel(), "Data de Vencimento");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 3, styles.getCellStyleLabel(), "Status");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 4, styles.getCellStyleLabel(), "Romaneios");

        AtomicInteger linha = new AtomicInteger(2);

        lotes.stream().forEach(item -> {

            Row rowItem = sheet.createRow(linha.get());

            rowItem.setHeight((short) 300);

            GerarRelatorioExcel.criarCell(rowItem, 0, styles.getCellStyleValue(), item.getCodigo());

            GerarRelatorioExcel.criarCell(rowItem, 1, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getDataFabricacao()) ? DateUtil.formatData(item.getDataFabricacao()) : StringUtil.empty());

            GerarRelatorioExcel.criarCell(rowItem, 2, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getDataVencimento()) ? DateUtil.formatData(item.getDataVencimento()) : StringUtil.empty());

            GerarRelatorioExcel.criarCell(rowItem, 3, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getStatus()) ? item.getStatus().getDescricao() : StringUtil.empty());

            if (ColecaoUtil.isNotEmpty(item.getRomaneios())) {

                GerarRelatorioExcel.criarCell(rowItem, 4, styles.getCellStyleValue(), item.getRomaneios().stream().map(Romaneio::getNumeroRomaneio).collect(Collectors.joining(", ")));

            } else {

                GerarRelatorioExcel.criarCell(rowItem, 4, styles.getCellStyleValue(), StringUtil.empty());
            }

            linha.incrementAndGet();
        });

        for (int i = 0; i <= 5; i++) {

            sheet.autoSizeColumn(i);
        }
    }

    private static void montarRelatorioDeLacres(List<RelatorioLacreDTO> lacres, XSSFSheet sheet, XSSFWorkbook workbook) {

        GerarRealtorioExcelStyles styles = GerarRealtorioExcelStyles.build(workbook);

        Row rowTituloTabela = sheet.createRow(1);

        rowTituloTabela.setHeight((short) 500);

        sheet.setColumnWidth(0, 500);

        GerarRelatorioExcel.criarCell(rowTituloTabela, 0, styles.getCellStyleLabel(), "Romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 1, styles.getCellStyleLabel(), "Status Romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 2, styles.getCellStyleLabel(), "Status Romaneio SAP");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 3, styles.getCellStyleLabel(), "Placa");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 4, styles.getCellStyleLabel(), "Tipo de Veículo");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 5, styles.getCellStyleLabel(), "Nº Nota Fiscal");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 6, styles.getCellStyleLabel(), "Data da Nota Fiscal");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 7, styles.getCellStyleLabel(), "Cliente");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 8, styles.getCellStyleLabel(), "Responsável");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 9, styles.getCellStyleLabel(), "Cód. Lacre");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 10, styles.getCellStyleLabel(), "Status do Lacre no Romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 11, styles.getCellStyleLabel(), "Data de Utilização");

        AtomicInteger linha = new AtomicInteger(2);

        lacres.stream().forEach(item -> {

            Row rowItem = sheet.createRow(linha.get());

            rowItem.setHeight((short) 300);

            GerarRelatorioExcel.criarCell(rowItem, 0, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getRomaneio()) ? item.getRomaneio().getNumeroRomaneio() : "-");

            GerarRelatorioExcel.criarCell(rowItem, 1, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getRomaneio()) && ObjetoUtil.isNotNull(item.getRomaneio().getStatusRomaneio()) ?
                    item.getRomaneio().getStatusRomaneio().getDescricao() : "-");

            GerarRelatorioExcel.criarCell(rowItem, 2, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getRomaneio()) && ObjetoUtil.isNotNull(item.getRomaneio().getStatusIntegracaoSAP()) ?
                    item.getRomaneio().getStatusIntegracaoSAP().getDescricao() : "-");

            GerarRelatorioExcel.criarCell(rowItem, 3, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getRomaneio()) ? item.getRomaneio().getPlacaCavalo().getPlaca1() : "-");

            GerarRelatorioExcel.criarCell(rowItem, 4, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getRomaneio()) ? item.getRomaneio().getPlacaCavalo().getTipo().getDescricao() : "-");

            GerarRelatorioExcel.criarCell(rowItem, 5, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getRetornoNFe()) ? item.getRetornoNFe().getNumeroAndSerieConcat() : "");

            GerarRelatorioExcel.criarCell(rowItem, 6, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getRetornoNFe()) ? DateUtil.formatData_ddMMyyyyHHmmss(item.getRetornoNFe().getDataCadastro()) : "");

            GerarRelatorioExcel.criarCell(rowItem, 7, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getRomaneio()) && ObjetoUtil.isNotNull(item.getRomaneio().getCliente()) ? item.getRomaneio().getCliente().toString() : "");

            GerarRelatorioExcel.criarCell(rowItem, 8, styles.getCellStyleValue(), StringUtil.isNotNullEmpty(item.getResponsavel()) ? item.getResponsavel() : "");

            GerarRelatorioExcel.criarCell(rowItem, 9, styles.getCellStyleValue(), item.getLacre().getCodigo());

            GerarRelatorioExcel.criarCell(rowItem, 10, styles.getCellStyleValue(), item.getStatusLacreApresentado());

            GerarRelatorioExcel.criarCell(rowItem, 11, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getLacre().getDataUtilizacao()) ? DateUtil.formatData_ddMMyyyyHHmmss(item.getLacre().getDataUtilizacao()) : "-");

            linha.incrementAndGet();
        });

        for (int i = 0; i <= 11; i++) {

            sheet.autoSizeColumn(i);
        }
    }

    private static void montarRelatorioPortalFornecedorGraosEntregas(List<RelatorioDepositoDTO> depositos, List<ItemClassificacaoDepositoDTO> itensClassificacao, XSSFSheet sheet, XSSFWorkbook workbook) {

        Set<ItemClassificacaoDTO> itensClassificacaoUnicos = itensClassificacao.stream().map(item -> new ItemClassificacaoDTO(item.getDescricaoItemClassificacao(), item.getCodigoItemClassificacao())).collect(Collectors.toSet());

        SortedSet<ItemClassificacaoDTO> itensClassificacaoUnicosOrdenados = new TreeSet<ItemClassificacaoDTO>(Comparator.comparing(ItemClassificacaoDTO::getCodigoItemClassificacao));

        itensClassificacaoUnicosOrdenados.addAll(itensClassificacaoUnicos);

        GerarRealtorioExcelStyles styles = GerarRealtorioExcelStyles.build(workbook);

        Row rowTituloTabela = sheet.createRow(1);

        rowTituloTabela.setHeight((short) 500);

        AtomicInteger columnTituloIndex = new AtomicInteger(0);

        sheet.setColumnWidth(0, 500);

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Data criação Romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Placa / Vagão");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Produtor/Depositante");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Produto");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Numero Nfe");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Quantidade NFE");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Peso Bruto");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Peso Tara");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Liq Úmido");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Desct");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Líquido Seco");

        AtomicInteger linha = new AtomicInteger(2);

        AtomicInteger indexRateio = new AtomicInteger(0);

        depositos.stream().sorted(Comparator.comparing(RelatorioDepositoDTO::getDataCriacaoRomaneio).reversed()).forEach(deposito -> {

            Row rowItem = sheet.createRow(linha.get());

            AtomicInteger columnValorIndex = new AtomicInteger(0);

            rowItem.setHeight((short) 300);

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), deposito.getNumeroRomaneio());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), DateUtil.formatData(deposito.getDataCriacaoRomaneio()));

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), deposito.getVeiculo());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), deposito.getDepositante());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), deposito.getMaterial());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), deposito.getNfe());

            AtomicInteger desconto = new AtomicInteger(0);

            itensClassificacaoUnicosOrdenados.forEach(itemClassificacao -> {

                ItemClassificacaoDepositoDTO itemClassificacaoDepositoDTO =
                        itensClassificacao.stream().filter(item -> item.getIdClassificacao().equals(deposito.getIdClassificacao()) && item.getCodigoItemClassificacao().equals(itemClassificacao.getCodigoItemClassificacao())).findFirst().orElse(null);

                boolean apresentarPercentual = ObjetoUtil.isNotNull(itemClassificacaoDepositoDTO) && !deposito.isPossuiRateio();

                if (apresentarPercentual) {

                    desconto.addAndGet(itemClassificacaoDepositoDTO.getDesconto().intValue());
                }
            });

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), !BigDecimalUtil.isMaiorQueZero(deposito.getQuantidadeNfe()) ?
                    deposito.getPesoLiquidoUmido().subtract(BigDecimal.valueOf(desconto.doubleValue())).doubleValue() : deposito.getQuantidadeNfe().doubleValue());

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), deposito.getPesoBruto().doubleValue());

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), deposito.getPesoTara().doubleValue());

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), deposito.getPesoLiquidoUmido().doubleValue());

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), desconto.doubleValue());

            if (deposito.isPossuiRateio()) {

                indexRateio.incrementAndGet();

                GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), deposito.getRateio().doubleValue());

            } else {

                indexRateio.set(0);

                GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), deposito.getPesoLiquidoUmido().subtract(BigDecimal.valueOf(desconto.doubleValue())).doubleValue());
            }

            linha.incrementAndGet();
        });

        for (int i = 0; i <= 50; i++) {

            sheet.autoSizeColumn(i);
        }
    }

    private static void montarRelatorioPortalFornecedorGraosPagamento(List<RfcZITSSAGRO_DIVERG_PESO_ROMANEIO_T_DADOS_Item> t_dados, XSSFSheet sheet, XSSFWorkbook workbook) {

        GerarRealtorioExcelStyles styles = GerarRealtorioExcelStyles.build(workbook);

        Row rowTituloTabela = sheet.createRow(1);

        rowTituloTabela.setHeight((short) 500);

        AtomicInteger columnTituloIndex = new AtomicInteger(0);

        sheet.setColumnWidth(0, 500);

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Local de Descarga");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Contrato");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Pedido");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Local Classif/Peso");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Data Romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Código Fornecedor");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Nome Fornecedor");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Nota Fiscal");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Status Pagamento");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Data pagto (realizado)");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Peso (KG)");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Valor Nota Fiscal");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Impostos %");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Valor Retenção Impostos");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Desconto Classificação (KG)");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Valor líquido a pagar");

        AtomicInteger linha = new AtomicInteger(2);

        t_dados.stream().forEach(item -> {

            Row rowItem = sheet.createRow(linha.get());

            AtomicInteger columnValorIndex = new AtomicInteger(0);

            rowItem.setHeight((short) 300);

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), item.getCentro());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), item.getKonnr());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), item.getEbeln());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), item.getClasf());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), item.getRomaneio());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), DateUtil.format("dd/MM/yyyy", item.getDoc_date()));

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), item.getLifnr());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), item.getName1());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), item.getNfe_serie());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), item.getPagamento());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), DateUtil.format("dd/MM/yyyy", item.getDt_pgto()));

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), item.getPeso_nfe());

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), item.getVlr_total_nf());

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), item.getImp_sen_fpas());

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), item.getVlr_retencao());

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), item.getDesconto_destino());

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), item.getVlr_liq_nf_apg());

            linha.incrementAndGet();
        });

        for (int i = 0; i <= 50; i++) {

            sheet.autoSizeColumn(i);
        }
    }

    private static void montarRelatorioDeDepositos(List<RelatorioDepositoDTO> depositos, List<ItemClassificacaoDepositoDTO> itensClassificacao, List<ItemClassificacaoDepositoDTO> itensClassificacaoOrigem, XSSFSheet sheet,
                                                   XSSFWorkbook workbook) {

        GerarRealtorioExcelStyles styles = GerarRealtorioExcelStyles.build(workbook);

        Row rowTituloTabela = sheet.createRow(1);

        rowTituloTabela.setHeight((short) 500);

        AtomicInteger columnTituloIndex = new AtomicInteger(0);

        sheet.setColumnWidth(0, 500);

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Operação");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Depósito entrada");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Depósito saida");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Tipo  de operação");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "St. Integração com o SAP");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Data criação Romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Placa / Vagão");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Produtor/Depositante");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Produto");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Numero Nfe");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Data Portaria");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Hora Portaria");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Data Classificação");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Hora Classificação");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Data 1º pesagem");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Hora 1º Pesagem");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Data 2º pesagem");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Hora 2º Pesagem");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Peso Bruto (Origem)");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Peso Tara (Origem)");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Liq. Úmido (Origem)");

        Set<ItemClassificacaoDTO> itensClassificacaoOrigemUnicos = itensClassificacaoOrigem.stream().map(item -> new ItemClassificacaoDTO(item.getDescricaoItemClassificacao(), item.getCodigoItemClassificacao())).collect(Collectors.toSet());

        SortedSet<ItemClassificacaoDTO> itensClassificacaoOrigemUnicosOrdenados = new TreeSet<ItemClassificacaoDTO>(Comparator.comparing(ItemClassificacaoDTO::getCodigoItemClassificacao));

        itensClassificacaoOrigemUnicosOrdenados.addAll(itensClassificacaoOrigemUnicos);

        itensClassificacaoOrigemUnicosOrdenados.forEach(itemClassificacao -> {

            GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), itemClassificacao.getDescricaoItemClassificacao() + " (Origem) ");

            GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), itemClassificacao.getDescricaoItemClassificacao() + " (Origem) Índice (%)");

            GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), itemClassificacao.getDescricaoItemClassificacao() + " (Origem) Perc. Desconto (%)");
        });

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Desct (Origem)");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Liq. Seco (Origem)");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Peso Bruto");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Peso Tara");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Liq. Úmido");

        Set<ItemClassificacaoDTO> itensClassificacaoUnicos = itensClassificacao.stream().map(item -> new ItemClassificacaoDTO(item.getDescricaoItemClassificacao(), item.getCodigoItemClassificacao())).collect(Collectors.toSet());

        SortedSet<ItemClassificacaoDTO> itensClassificacaoUnicosOrdenados = new TreeSet<ItemClassificacaoDTO>(Comparator.comparing(ItemClassificacaoDTO::getCodigoItemClassificacao));

        itensClassificacaoUnicosOrdenados.addAll(itensClassificacaoUnicos);

        itensClassificacaoUnicosOrdenados.forEach(itemClassificacao -> {

            GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), itemClassificacao.getDescricaoItemClassificacao());

            GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), itemClassificacao.getDescricaoItemClassificacao() + " Índice (%)");

            GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), itemClassificacao.getDescricaoItemClassificacao() + " Perc. Desconto (%)");
        });

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Desct");

        GerarRelatorioExcel.criarCell(rowTituloTabela, columnTituloIndex.getAndIncrement(), styles.getCellStyleLabel(), "Líquido Seco");

        AtomicInteger linha = new AtomicInteger(2);

        AtomicInteger indexRateio = new AtomicInteger(0);

        depositos.stream().sorted(Comparator.comparing(RelatorioDepositoDTO::getDataCriacaoRomaneio).reversed()).forEach(deposito -> {

            Row rowItem = sheet.createRow(linha.get());

            AtomicInteger columnValorIndex = new AtomicInteger(0);

            rowItem.setHeight((short) 300);

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), deposito.getOperacao());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), deposito.getDepositoEntrada());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), deposito.getDepositoSaida());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), ObjetoUtil.isNotNull(deposito.getDirecaoOperacaoDeposito()) ? deposito.getDirecaoOperacaoDeposito().getDescricao() :
                    StringUtil.empty());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), deposito.getNumeroRomaneio());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), ObjetoUtil.isNotNull(deposito.getStatusIntegracaoSAP()) ? deposito.getStatusIntegracaoSAP().getDescricao() : "***");

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), DateUtil.formatData(deposito.getDataCriacaoRomaneio()));

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), deposito.getVeiculo());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), deposito.getDepositante());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), deposito.getMaterial());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), deposito.getNfe());

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), DateUtil.formatData(deposito.getDtSimplesEntradaReferencia()));

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), DateUtil.formatHora(deposito.getDtSimplesEntradaReferencia()));

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), DateUtil.formatData(deposito.getDataClassificacao()));

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), DateUtil.formatHora(deposito.getDataClassificacao()));

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), DateUtil.formatData(deposito.getDataPrimeiraPesagem()));

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), DateUtil.formatHora(deposito.getDataPrimeiraPesagem()));

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), DateUtil.formatData(deposito.getDataSegundaPesagem()));

            GerarRelatorioExcel.criarCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValue(), DateUtil.formatHora(deposito.getDataSegundaPesagem()));

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), deposito.getPesoBrutoOrigem().doubleValue());

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), deposito.getPesoTaraOrigem().doubleValue());

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), deposito.getPesoLiquidoUmidoOrigem().doubleValue());

            AtomicInteger descontoOrigem = new AtomicInteger(0);

            itensClassificacaoOrigemUnicosOrdenados.forEach(itemClassificacao -> {

                ItemClassificacaoDepositoDTO itemClassificacaoDepositoDTO =
                        itensClassificacaoOrigem.stream().filter(item -> item.getNumeroRomaneio().equals(deposito.getNumeroRomaneio()) && item.getCodigoItemClassificacao().equals(itemClassificacao.getCodigoItemClassificacao())).findFirst().orElse(null);

                if (Objects.nonNull(itemClassificacaoDepositoDTO)) {

                    descontoOrigem.addAndGet(itemClassificacaoDepositoDTO.getDesconto().intValue());
                }

                GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), Objects.nonNull(itemClassificacaoDepositoDTO) ? itemClassificacaoDepositoDTO.getDesconto().doubleValue() :
                        new Double(0));

                GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePorcentagem(), Objects.nonNull(itemClassificacaoDepositoDTO) ?
                        itemClassificacaoDepositoDTO.getIndice().doubleValue() : new Double(0));

                GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePorcentagem(), Objects.nonNull(itemClassificacaoDepositoDTO) ?
                        itemClassificacaoDepositoDTO.getPercentualDesconto().doubleValue() : new Double(0));
            });

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), descontoOrigem.doubleValue());

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), deposito.getPesoLiquidoSecoOrigem().doubleValue());

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), deposito.getPesoBruto().doubleValue());

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), deposito.getPesoTara().doubleValue());

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), deposito.getPesoLiquidoUmido().doubleValue());

            AtomicInteger desconto = new AtomicInteger(0);

            itensClassificacaoUnicosOrdenados.forEach(itemClassificacao -> {

                ItemClassificacaoDepositoDTO itemClassificacaoDepositoDTO =
                        itensClassificacao.stream().filter(item -> item.getIdClassificacao().equals(deposito.getIdClassificacao()) && item.getCodigoItemClassificacao().equals(itemClassificacao.getCodigoItemClassificacao())).findFirst().orElse(null);

                boolean apresentarPercentual = ObjetoUtil.isNotNull(itemClassificacaoDepositoDTO) && !deposito.isPossuiRateio();

                if (apresentarPercentual) {

                    desconto.addAndGet(itemClassificacaoDepositoDTO.getDesconto().intValue());
                }

                GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), apresentarPercentual ? itemClassificacaoDepositoDTO.getDesconto().doubleValue() : new Double(0));

                GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePorcentagem(), apresentarPercentual ? itemClassificacaoDepositoDTO.getIndice().doubleValue() : new Double(0));

                GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePorcentagem(), apresentarPercentual ? itemClassificacaoDepositoDTO.getPercentualDesconto().doubleValue() :
                        new Double(0));
            });

            GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), desconto.doubleValue());

            if (deposito.isPossuiRateio()) {

                indexRateio.incrementAndGet();

                GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), deposito.getRateio().doubleValue());

            } else {

                indexRateio.set(0);

                GerarRelatorioExcel.criarDoubleCell(rowItem, columnValorIndex.getAndIncrement(), styles.getCellStyleValuePesos(), deposito.getPesoLiquidoUmido().subtract(BigDecimal.valueOf(desconto.doubleValue())).doubleValue());
            }

            boolean ultimoItemDoRateio = depositos.stream().filter(d -> d.getNumeroRomaneio().equals(deposito.getNumeroRomaneio())).count() == indexRateio.get();

            if (deposito.isPossuiRateio() && ultimoItemDoRateio) {

                Row rowSumarioRateio = sheet.createRow(linha.incrementAndGet());

                AtomicInteger posicaoItemClassificacaoValor = new AtomicInteger(51);

                GerarRelatorioExcel.criarCell(rowSumarioRateio, 0, styles.getCellStyleSummary(), "Total do Rateio");

                sheet.addMergedRegion(new CellRangeAddress(linha.get(), linha.get(), 0, 50));

                itensClassificacaoUnicosOrdenados.forEach(itemClassificacao -> {

                    ItemClassificacaoDepositoDTO itemClassificacaoDepositoDTO =
                            itensClassificacao.stream().filter(item -> item.getIdClassificacao().equals(deposito.getIdClassificacao()) && item.getCodigoItemClassificacao().equals(itemClassificacao.getCodigoItemClassificacao())).findFirst().orElse(null);

                    desconto.addAndGet(itemClassificacaoDepositoDTO.getDesconto().intValue());

                    GerarRelatorioExcel.criarDoubleCell(rowSumarioRateio, posicaoItemClassificacaoValor.getAndIncrement(), styles.getCellStyleValuePesos(), itemClassificacaoDepositoDTO.getDesconto().doubleValue());

                    GerarRelatorioExcel.criarDoubleCell(rowSumarioRateio, posicaoItemClassificacaoValor.getAndIncrement(), styles.getCellStyleValuePorcentagem(), itemClassificacaoDepositoDTO.getIndice().doubleValue());

                    GerarRelatorioExcel.criarDoubleCell(rowSumarioRateio, posicaoItemClassificacaoValor.getAndIncrement(), styles.getCellStyleValuePorcentagem(), itemClassificacaoDepositoDTO.getPercentualDesconto().doubleValue());
                });

                GerarRelatorioExcel.criarDoubleCell(rowSumarioRateio, posicaoItemClassificacaoValor.getAndIncrement(), styles.getCellStyleValuePesos(), desconto.doubleValue());

                BigDecimal somaRateio = depositos.stream().filter(d -> d.getNumeroRomaneio().equals(deposito.getNumeroRomaneio())).map(RelatorioDepositoDTO::getRateio).reduce(BigDecimal.ZERO, BigDecimal::add);

                GerarRelatorioExcel.criarDoubleCell(rowSumarioRateio, posicaoItemClassificacaoValor.getAndIncrement(), styles.getCellStyleValuePesos(), somaRateio.subtract(BigDecimal.valueOf(desconto.get())).doubleValue());
            }

            linha.incrementAndGet();
        });

        for (int i = 0; i <= 80; i++) {

            sheet.autoSizeColumn(i);
        }
    }

    private static void montarRelatorioPesagensManuais(List<PesagemManualDTO> pesagens, XSSFSheet sheet, XSSFWorkbook workbook) {

        GerarRealtorioExcelStyles styles = GerarRealtorioExcelStyles.build(workbook);

        Row rowTituloTabela = sheet.createRow(1);

        rowTituloTabela.setHeight((short) 500);

        sheet.setColumnWidth(0, 500);

        GerarRelatorioExcel.criarCell(rowTituloTabela, 0, styles.getCellStyleLabel(), "Número do Romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 1, styles.getCellStyleLabel(), "Data de Cadastro do Romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 2, styles.getCellStyleLabel(), "Produto");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 3, styles.getCellStyleLabel(), "Placa");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 4, styles.getCellStyleLabel(), "Motorista");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 5, styles.getCellStyleLabel(), "Motivo");

        AtomicInteger linha = new AtomicInteger(2);

        pesagens.stream().forEach(item -> {

            Row rowItem = sheet.createRow(linha.getAndIncrement());

            rowItem.setHeight((short) 300);

            GerarRelatorioExcel.criarCell(rowItem, 0, styles.getCellStyleValue(), item.getNumeroRomaneio());

            GerarRelatorioExcel.criarCell(rowItem, 1, styles.getCellStyleValue(), DateUtil.formatData_ddMMyyyyHHmmss(item.getDataCadastro()));

            GerarRelatorioExcel.criarCell(rowItem, 2, styles.getCellStyleValue(), item.getMaterial());

            GerarRelatorioExcel.criarCell(rowItem, 3, styles.getCellStyleValue(), item.getPlaca());

            GerarRelatorioExcel.criarCell(rowItem, 4, styles.getCellStyleValue(), item.getMotorista());

            GerarRelatorioExcel.criarCell(rowItem, 5, styles.getCellStyleValue(), item.getMotivo());
        });

        for (int i = 0; i <= 5; i++) {

            sheet.autoSizeColumn(i);
        }
    }

    private static void montarRelatorioLaudoAnalitico(Collection<RelatorioLaudoAnalitico> laudos, XSSFSheet sheet, XSSFWorkbook workbook) {

        GerarRealtorioExcelStyles styles = GerarRealtorioExcelStyles.build(workbook);

        Row rowTituloTabela = sheet.createRow(1);

        rowTituloTabela.setHeight((short) 500);

        sheet.setColumnWidth(0, 500);

        GerarRelatorioExcel.criarCell(rowTituloTabela, 0, styles.getCellStyleLabel(), "Ordem de Venda");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 1, styles.getCellStyleLabel(), "Material");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 2, styles.getCellStyleLabel(), "Cliente");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 3, styles.getCellStyleLabel(), "Numero do Romaneio");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 4, styles.getCellStyleLabel(), "Placa Veículo");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 5, styles.getCellStyleLabel(), "Peso Líquido");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 6, styles.getCellStyleLabel(), "Unidade de Medida");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 7, styles.getCellStyleLabel(), "Lacre");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 8, styles.getCellStyleLabel(), "Lote");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 9, styles.getCellStyleLabel(), "Certificado de Qualidade");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 10, styles.getCellStyleLabel(), "Nº Nota");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 11, styles.getCellStyleLabel(), "Dt. Emissão NFe");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 12, styles.getCellStyleLabel(), "Característica");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 13, styles.getCellStyleLabel(), "Sub Caracteristica");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 14, styles.getCellStyleLabel(), "Especificação");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 15, styles.getCellStyleLabel(), "Unidade");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 16, styles.getCellStyleLabel(), "Resultado");

        AtomicInteger linha = new AtomicInteger(2);

        laudos.stream().forEach(laudo -> {
            Row rowItem = sheet.createRow(linha.get());
            rowItem.setHeight((short) 300);
            Romaneio romaneio = laudo.getRomaneio();
            ItemNFPedido itemNFPedido = laudo.getItemNFPedido();
            Material material = itemNFPedido.getMaterial();
            Cliente cliente = itemNFPedido.getCliente();
            Veiculo placaCavalo = itemNFPedido.getPlacaCavalo();
            AnaliseQualidade analiseQualidade = laudo.getAnaliseQualidade();
            RetornoNFe retornoNFe = laudo.getRetornoNFe();
            AnaliseQualidadeItem analiseQualidadeItem = laudo.getAnaliseQualidadeItem();
            AnaliseItem analiseItem = ObjetoUtil.isNotNull(analiseQualidadeItem) ? analiseQualidadeItem.getAnaliseItem() : null;
            BigDecimal pesoLiquido = romaneio.getPesagem().getPesoFinal().subtract(romaneio.getPesagem().getPesoInicial());
            GerarRelatorioExcel.criarCell(rowItem, 0, styles.getCellStyleValue(), itemNFPedido.getCodigoOrdemVenda());
            GerarRelatorioExcel.criarCell(rowItem, 1, styles.getCellStyleValue(), ObjetoUtil.isNotNull(material) ? MessageFormat.format("{0} - {1}", material.getCodigo(), material.getDescricao()) : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 2, styles.getCellStyleValue(), ObjetoUtil.isNotNull(cliente) ? cliente.getDescricaoFormatada() : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 3, styles.getCellStyleValue(), romaneio.getNumeroRomaneio());
            GerarRelatorioExcel.criarCell(rowItem, 4, styles.getCellStyleValue(), ObjetoUtil.isNotNull(placaCavalo) ? StringUtil.toPlaca(placaCavalo.getPlaca1()) : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 5, styles.getCellStyleValue(), pesoLiquido.toString());
            GerarRelatorioExcel.criarCell(rowItem, 6, styles.getCellStyleValue(), itemNFPedido.getUnidadeMedidaOrdem());
            GerarRelatorioExcel.criarCell(rowItem, 7, styles.getCellStyleValue(), analiseQualidade.getLacreAmostra());
            GerarRelatorioExcel.criarCell(rowItem, 8, styles.getCellStyleValue(), ObjetoUtil.isNotNull(analiseQualidade.getLoteAnaliseQualidade()) ? analiseQualidade.getLoteAnaliseQualidade().getCodigo() : "");
            GerarRelatorioExcel.criarCell(rowItem, 9, styles.getCellStyleValue(), analiseQualidade.getCertificado());
            GerarRelatorioExcel.criarCell(rowItem, 10, styles.getCellStyleValue(), retornoNFe.getNfe());
            GerarRelatorioExcel.criarCell(rowItem, 11, styles.getCellStyleValue(), ObjetoUtil.isNotNull(retornoNFe.getDataCadastro()) ? DateUtil.format("dd/MM/yyyy", retornoNFe.getDataCadastro()) : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 12, styles.getCellStyleValue(), ObjetoUtil.isNotNull(analiseItem) ? analiseItem.getCaracteristica() : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 13, styles.getCellStyleValue(), ObjetoUtil.isNotNull(analiseItem) ? analiseItem.getSubCaracteristica() : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 14, styles.getCellStyleValue(), ObjetoUtil.isNotNull(analiseItem) ? analiseItem.getEspecificacao() : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 15, styles.getCellStyleValue(), ObjetoUtil.isNotNull(analiseItem) ? analiseItem.getUnidade() : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 16, styles.getCellStyleValue(), analiseQualidadeItem.getResultado());
            linha.set(linha.incrementAndGet());
        });

        for (int i = 0; i <= 16; i++) {

            sheet.autoSizeColumn(i);
        }
    }

    private static void montarRelatorioLaudoSintetico(Collection<RelatorioLaudoSintetico> laudos, XSSFSheet sheet, XSSFWorkbook workbook) {

        GerarRealtorioExcelStyles styles = GerarRealtorioExcelStyles.build(workbook);

        Row rowTituloTabela = sheet.createRow(1);

        rowTituloTabela.setHeight((short) 500);

        sheet.setColumnWidth(0, 500);

        GerarRelatorioExcel.criarCell(rowTituloTabela, 0, styles.getCellStyleLabel(), "Ordem de Venda");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 1, styles.getCellStyleLabel(), "Material");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 2, styles.getCellStyleLabel(), "Cliente");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 3, styles.getCellStyleLabel(), "Numero do Romaneio");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 4, styles.getCellStyleLabel(), "Placa Veículo");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 5, styles.getCellStyleLabel(), "Peso Líquido");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 6, styles.getCellStyleLabel(), "Unidade de Medida");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 7, styles.getCellStyleLabel(), "Lacre");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 8, styles.getCellStyleLabel(), "Lote");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 9, styles.getCellStyleLabel(), "Certificado de Qualidade");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 10, styles.getCellStyleLabel(), "Nº Nota");
        GerarRelatorioExcel.criarCell(rowTituloTabela, 11, styles.getCellStyleLabel(), "Dt. Emissão NFe");

        AtomicInteger linha = new AtomicInteger(2);

        laudos.stream().forEach(laudo -> {
            Row rowItem = sheet.createRow(linha.get());
            rowItem.setHeight((short) 300);
            GerarRelatorioExcel.criarCell(rowItem, 0, styles.getCellStyleValue(), ObjetoUtil.isNotNull(laudo.getOrdemVenda()) ? laudo.getOrdemVenda() : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 1, styles.getCellStyleValue(), ObjetoUtil.isNotNull(laudo.getCodigoMaterial()) ? MessageFormat.format("{0} - {1}", laudo.getCodigoMaterial(), laudo.getMaterial()) : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 2, styles.getCellStyleValue(), ObjetoUtil.isNotNull(laudo.getCodigoCliente()) ? MessageFormat.format("{0} - {1}", laudo.getCodigoCliente(), laudo.getCliente()) : laudo.getCliente());
            GerarRelatorioExcel.criarCell(rowItem, 3, styles.getCellStyleValue(), ObjetoUtil.isNotNull(laudo.getNumeroRomaneio()) ? laudo.getNumeroRomaneio() : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 4, styles.getCellStyleValue(), ObjetoUtil.isNotNull(laudo.getPlacaVeiculo()) ? laudo.getPlacaVeiculo() : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 5, styles.getCellStyleValue(), laudo.getPesoLiquido().toString());
            GerarRelatorioExcel.criarCell(rowItem, 6, styles.getCellStyleValue(), ObjetoUtil.isNotNull(laudo.getUnidadeMedida()) ? laudo.getUnidadeMedida() : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 7, styles.getCellStyleValue(), ObjetoUtil.isNotNull(laudo.getLacre()) ? laudo.getLacre() : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 8, styles.getCellStyleValue(), ObjetoUtil.isNotNull(laudo.getLote()) ? laudo.getLote() : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 9, styles.getCellStyleValue(), ObjetoUtil.isNotNull(laudo.getCertificado()) ? laudo.getCertificado() : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 10, styles.getCellStyleValue(), ObjetoUtil.isNotNull(laudo.getNumeroNfe()) ? laudo.getNumeroNfe() : StringUtil.empty());
            GerarRelatorioExcel.criarCell(rowItem, 11, styles.getCellStyleValue(), ObjetoUtil.isNotNull(laudo.getDataNfe()) ? DateUtil.format("dd/MM/yyyy", laudo.getDataNfe()) : "");
            linha.set(linha.incrementAndGet());
        });

        for (int i = 0; i <= 11; i++) {

            sheet.autoSizeColumn(i);
        }
    }

    private static void montarRelatorioOrdemVenda(Collection<RfcOrdensVendaItemResponse> ordens, Collection<TextoOrdemDTO> textosDasOrdens, XSSFSheet sheet, XSSFWorkbook workbook) {

        GerarRealtorioExcelStyles styles = GerarRealtorioExcelStyles.build(workbook);

        Row rowTituloTabela = sheet.createRow(1);

        rowTituloTabela.setHeight((short) 500);

        sheet.setColumnWidth(0, 500);

        GerarRelatorioExcel.criarCell(rowTituloTabela, 0, styles.getCellStyleLabel(), "Ordem Venda");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 1, styles.getCellStyleLabel(), "Data");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 2, styles.getCellStyleLabel(), "Cliente");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 3, styles.getCellStyleLabel(), "Produto");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 4, styles.getCellStyleLabel(), "Pedido");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 5, styles.getCellStyleLabel(), "Motivo da Recusa");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 6, styles.getCellStyleLabel(), "Preço");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 7, styles.getCellStyleLabel(), "Qt. Inicial");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 8, styles.getCellStyleLabel(), "Qt. Embq");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 9, styles.getCellStyleLabel(), "Saldo");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 10, styles.getCellStyleLabel(), "Situação");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 11, styles.getCellStyleLabel(), "Observação");

        AtomicInteger linha = new AtomicInteger(2);

        ordens.stream().forEach(item -> {

            Row rowItem = sheet.createRow(linha.get());

            rowItem.setHeight((short) 300);

            GerarRelatorioExcel.criarCell(rowItem, 0, styles.getCellStyleValue(), new Long(item.getOrdemVenda()).toString());

            GerarRelatorioExcel.criarCell(rowItem, 1, styles.getCellStyleValue(), DateUtil.format("dd/MM/yyyy", item.getDataOrdem()));

            GerarRelatorioExcel.criarCell(rowItem, 2, styles.getCellStyleValue(), MessageFormat.format("{0} - {1} - {2}", new Long(item.getCentro()).toString(), new Long(item.getCodigoCliente()).toString(), item.getDescricaoCliente()));

            GerarRelatorioExcel.criarCell(rowItem, 3, styles.getCellStyleValue(), MessageFormat.format("{0} - {1}", new Long(item.getCodigoMaterial()).toString(), item.getDescricaoMaterial()));

            GerarRelatorioExcel.criarCell(rowItem, 4, styles.getCellStyleValue(), item.getNumeroPedido());

            GerarRelatorioExcel.criarCell(rowItem, 5, styles.getCellStyleValue(), item.getMotivoRecusa());

            GerarRelatorioExcel.criarDoubleCell(rowItem, 6, styles.getCellStyleValuePreco(), item.getPreco().doubleValue());

            GerarRelatorioExcel.criarDoubleCell(rowItem, 7, styles.getCellStyleValuePesos(), item.getQtdInicial().doubleValue());

            GerarRelatorioExcel.criarDoubleCell(rowItem, 8, styles.getCellStyleValuePesos(), item.getQtdEmb().doubleValue());

            GerarRelatorioExcel.criarDoubleCell(rowItem, 9, styles.getCellStyleValuePesos(), item.getSaldo().doubleValue());

            GerarRelatorioExcel.criarCell(rowItem, 10, styles.getCellStyleValue(), item.getDescricaoStatusOrdem());

            GerarRelatorioExcel.criarCell(rowItem, 11, styles.getCellStyleValue(), textosDasOrdens.stream().filter(t -> t.getOrdem().equals(item.getOrdemVenda())).map(TextoOrdemDTO::getTexto).findFirst().orElse(StringUtil.empty()));

            linha.set(linha.incrementAndGet());
        });

        for (int i = 0; i <= 28; i++) {

            sheet.autoSizeColumn(i);
        }
    }

    private static void montarRelatorioComercial_V2(Collection<RelatorioComercial_V2_DTO> dtos, XSSFSheet sheet, XSSFWorkbook workbook) {

        GerarRealtorioExcelStyles styles = GerarRealtorioExcelStyles.build(workbook);

        Row rowTituloTabela = sheet.createRow(1);
        rowTituloTabela.setHeight((short) 500);

        sheet.setColumnWidth(0, 500);

        GerarRelatorioExcel.criarCell(rowTituloTabela, 0, styles.getCellStyleLabel(), "Status do romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 1, styles.getCellStyleLabel(), "Status do SAP");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 2, styles.getCellStyleLabel(), "Número da ordem");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 3, styles.getCellStyleLabel(), "Pedido");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 4, styles.getCellStyleLabel(), "Material");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 5, styles.getCellStyleLabel(), "Cliente");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 6, styles.getCellStyleLabel(), "Transportadora");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 7, styles.getCellStyleLabel(), "Número do romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 8, styles.getCellStyleLabel(), "Placa do veículo");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 9, styles.getCellStyleLabel(), "Motorista");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 10, styles.getCellStyleLabel(), "Data");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 11, styles.getCellStyleLabel(), "Hora");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 12, styles.getCellStyleLabel(), "Nr. nota");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 13, styles.getCellStyleLabel(), "Nr. remessa");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 14, styles.getCellStyleLabel(), "Nr. documento de transporte");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 15, styles.getCellStyleLabel(), "Nr. documento de faturamento");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 16, styles.getCellStyleLabel(), "Quantidade");

        AtomicInteger linha = new AtomicInteger(2);

        Map<String, List<RelatorioComercial_V2_DTO>> itensPorOrdem = dtos.stream().collect(Collectors.groupingBy(RelatorioComercial_V2_DTO::getCodigoOrdemVenda));

        Set<Long> idsItensAdicionados = new HashSet<>();

        AtomicLong quantidadeTotal = new AtomicLong(0);

        itensPorOrdem.keySet().forEach(ordem -> {

            final List<RelatorioComercial_V2_DTO> movimentacoes = itensPorOrdem.get(ordem);

            AtomicLong quantidadePorOrdem = new AtomicLong(0);

            movimentacoes.stream().forEach(item -> {

                Row rowItem = sheet.createRow(linha.get());

                rowItem.setHeight((short) 300);

                GerarRelatorioExcel.criarCell(rowItem, 0, styles.getCellStyleValue(), item.getStatusRomaneio().getDescricao());

                GerarRelatorioExcel.criarCell(rowItem, 1, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getStatusIntegracaoSAP()) ? item.getStatusIntegracaoSAP().getDescricao() : StringUtil.empty());

                GerarRelatorioExcel.criarCell(rowItem, 2, styles.getCellStyleValue(), item.getCodigoOrdemVenda());

                GerarRelatorioExcel.criarCell(rowItem, 3, styles.getCellStyleValue(), item.getNumeroPedido());

                GerarRelatorioExcel.criarCell(rowItem, 4, styles.getCellStyleValue(), item.getCodigoMaterial() + " - " + item.getDescricaoMaterial());

                GerarRelatorioExcel.criarCell(rowItem, 5, styles.getCellStyleValue(), item.getCodigoCliente() + " - " + item.getDescricaoCliente());

                GerarRelatorioExcel.criarCell(rowItem, 6, styles.getCellStyleValue(), item.getCodigoTransportadora() + " - " + item.getDescricaoTransportadora());

                GerarRelatorioExcel.criarCell(rowItem, 7, styles.getCellStyleValue(), item.getNumeroRomaneio());

                GerarRelatorioExcel.criarCell(rowItem, 8, styles.getCellStyleValue(), item.getPlacaUm());

                GerarRelatorioExcel.criarCell(rowItem, 9, styles.getCellStyleValue(), item.getCodigoMotorista() + " - " + item.getDescricaoMotorista());

                GerarRelatorioExcel.criarCell(rowItem, 10, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getDataRomaneio()) ? DateUtil.format("dd/MM/yyyy", item.getDataRomaneio()) : StringUtil.empty());

                GerarRelatorioExcel.criarCell(rowItem, 11, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getDataRomaneio()) ? DateUtil.format("HH:mm:ss", item.getDataRomaneio()) : StringUtil.empty());

                GerarRelatorioExcel.criarCell(rowItem, 12, styles.getCellStyleValue(), item.getNfe());

                GerarRelatorioExcel.criarCell(rowItem, 13, styles.getCellStyleValue(), item.getNumeroRemessa());

                GerarRelatorioExcel.criarCell(rowItem, 14, styles.getCellStyleValue(), item.getNumeroOrdemTransporte());

                GerarRelatorioExcel.criarCell(rowItem, 15, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getDocumentoFaturamento()) ? item.getDocumentoFaturamento().toString() : null);

                BigDecimal valor = BigDecimalUtil.isMaiorZero(item.getRateio()) ? item.getRateio() : item.getQtdKgLValido();

                quantidadePorOrdem.getAndAdd(valor.longValue());

                GerarRelatorioExcel.criarDoubleCell(rowItem, 16, styles.getCellStyleValuePesos(), valor.doubleValue());

                idsItensAdicionados.add(item.getIdItem().longValue());

                linha.set(linha.incrementAndGet());
            });

            Row rowSubTotal = sheet.createRow(linha.get());

            GerarRelatorioExcel.criarCell(rowSubTotal, 0, styles.getCellStyleSummary(), "Total");

            sheet.addMergedRegion(new CellRangeAddress(linha.get(), linha.get(), 0, 15));

            quantidadeTotal.getAndAdd(quantidadePorOrdem.longValue());

            GerarRelatorioExcel.criarDoubleCell(rowSubTotal, 16, styles.getCellStyleValuePesos(), quantidadePorOrdem.doubleValue());

            linha.set(linha.incrementAndGet());
        });

        Row rowTotal = sheet.createRow(linha.get());

        GerarRelatorioExcel.criarCell(rowTotal, 0, styles.getCellStyleSummary(), "Total");

        for (int i = 1; i <= 15; i++) {

            GerarRelatorioExcel.criarCell(rowTotal, i, styles.getCellStyleSummary(), "");
        }

        GerarRelatorioExcel.criarDoubleCell(rowTotal, 16, styles.getCellStyleValuePesos(), quantidadeTotal.doubleValue());

        sheet.addMergedRegion(new CellRangeAddress(linha.get(), linha.get(), 0, 15));

        linha.set(linha.incrementAndGet());

        for (int i = 0; i <= 16; i++) {

            sheet.autoSizeColumn(i);
        }
    }

    private static void montarRelatorioComercial(Collection<RelatorioComercialDTO> dtos, XSSFSheet sheet, XSSFWorkbook workbook) {

        GerarRealtorioExcelStyles styles = GerarRealtorioExcelStyles.build(workbook);

        Row rowTituloTabela = sheet.createRow(1);
        rowTituloTabela.setHeight((short) 500);
        sheet.setColumnWidth(0, 500);

        GerarRelatorioExcel.criarCell(rowTituloTabela, 0, styles.getCellStyleLabel(), "Número da ordem");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 1, styles.getCellStyleLabel(), "Pedido");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 2, styles.getCellStyleLabel(), "Material");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 3, styles.getCellStyleLabel(), "Cliente");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 4, styles.getCellStyleLabel(), "Transportadora");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 5, styles.getCellStyleLabel(), "Transportadora Subcontratada");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 6, styles.getCellStyleLabel(), "Número do romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 7, styles.getCellStyleLabel(), "Data Cadastro do Romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 8, styles.getCellStyleLabel(), "Placa do veículo");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 9, styles.getCellStyleLabel(), "Motorista");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 10, styles.getCellStyleLabel(), "Status do romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 11, styles.getCellStyleLabel(), "Status do SAP");
        
        
        GerarRelatorioExcel.criarCell(rowTituloTabela, 12, styles.getCellStyleLabel(), "Data Primeira Pesagem");
        
        GerarRelatorioExcel.criarCell(rowTituloTabela, 13, styles.getCellStyleLabel(), "Hora Primeira Pesagem");
        

        GerarRelatorioExcel.criarCell(rowTituloTabela, 14, styles.getCellStyleLabel(), "Peso tara");
        
        GerarRelatorioExcel.criarCell(rowTituloTabela, 15, styles.getCellStyleLabel(), "Data Segunda Pesagem");
        
        GerarRelatorioExcel.criarCell(rowTituloTabela, 16, styles.getCellStyleLabel(), "Hora Segunda Pesagem");
        

        GerarRelatorioExcel.criarCell(rowTituloTabela, 17, styles.getCellStyleLabel(), "Peso bruto");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 18, styles.getCellStyleLabel(), "Qtde Pesagens Brutas");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 19, styles.getCellStyleLabel(), "Peso líquido");
        
        
        GerarRelatorioExcel.criarCell(rowTituloTabela, 20, styles.getCellStyleLabel(), "Data Emissão da Nota Fiscal");
        
        GerarRelatorioExcel.criarCell(rowTituloTabela, 21, styles.getCellStyleLabel(), "Hora Emissão da Nota Fiscal");
        

        GerarRelatorioExcel.criarCell(rowTituloTabela, 22, styles.getCellStyleLabel(), "Nr. nota");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 23, styles.getCellStyleLabel(), "Nr. remessa");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 24, styles.getCellStyleLabel(), "Nr. documento de transporte");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 25, styles.getCellStyleLabel(), "Qtde KG/L");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 26, styles.getCellStyleLabel(), "Motivo Romaneio Vinculado");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 27, styles.getCellStyleLabel(), "Nr. Romaneio Vinculado");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 28, styles.getCellStyleLabel(), "Motivos Aba Restrições");
        

        AtomicInteger linha = new AtomicInteger(2);

        Map<String, List<RelatorioComercialDTO>> itensPorOrdem = dtos.stream().collect(Collectors.groupingBy(RelatorioComercialDTO::getCodigoOrdemVenda));

        Set<Long> idsItensAdicionados = new HashSet<>();

        itensPorOrdem.keySet().forEach(ordem -> {

            final List<RelatorioComercialDTO> movimentacoes = itensPorOrdem.get(ordem);

            movimentacoes.stream().forEach(item -> {

                Row rowItem = sheet.createRow(linha.get());

                rowItem.setHeight((short) 300);

                GerarRelatorioExcel.criarCell(rowItem, 0, styles.getCellStyleValue(), item.getCodigoOrdemVenda());

                GerarRelatorioExcel.criarCell(rowItem, 1, styles.getCellStyleValue(), item.getPedido());

                GerarRelatorioExcel.criarCell(rowItem, 2, styles.getCellStyleValue(), item.getMaterial());

                GerarRelatorioExcel.criarCell(rowItem, 3, styles.getCellStyleValue(), item.getCliente());

                GerarRelatorioExcel.criarCell(rowItem, 4, styles.getCellStyleValue(), item.getTransportadora());

                GerarRelatorioExcel.criarCell(rowItem, 5, styles.getCellStyleValue(), item.getTransportadoraSubcontratada());

                GerarRelatorioExcel.criarCell(rowItem, 6, styles.getCellStyleValue(), item.getNumeroRomaneio());

                GerarRelatorioExcel.criarCell(rowItem, 7, styles.getCellStyleValue(), DateUtil.formatData_ddMMyyyyHHmmss(item.getDataCadastro()));

                GerarRelatorioExcel.criarCell(rowItem, 8, styles.getCellStyleValue(), item.getPlaca());

                GerarRelatorioExcel.criarCell(rowItem, 9, styles.getCellStyleValue(), item.getMotorista());

                GerarRelatorioExcel.criarCell(rowItem, 10, styles.getCellStyleValue(), item.getStatusRomaneio().getDescricao());

                GerarRelatorioExcel.criarCell(rowItem, 11, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getStatusSap()) ? item.getStatusSap().getDescricao() : "-");
                
                
                GerarRelatorioExcel.criarCell(rowItem, 12, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getDataPrimeiraPesagem()) ? DateUtil.formatTo(item.getDataPrimeiraPesagem(), "dd/MM/yyyy") : "");

                GerarRelatorioExcel.criarCell(rowItem, 13, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getDataPrimeiraPesagem()) ? DateUtil.formatTo(item.getDataPrimeiraPesagem(), "HH:mm:ss") : "");
                

                if (!itemAdicionado(idsItensAdicionados, item.getIdItem())) {

                    GerarRelatorioExcel.criarDoubleCell(rowItem, 14, styles.getCellStyleValuePesos(), new Double(ObjetoUtil.isNotNull(item.getPesoTara()) ? item.getPesoTara().toString() : "0"));

                }
                
                GerarRelatorioExcel.criarCell(rowItem, 15, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getDataSegundaPesagem()) ? DateUtil.formatTo(item.getDataSegundaPesagem(), "dd/MM/yyyy") : "");

                GerarRelatorioExcel.criarCell(rowItem, 16, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getDataSegundaPesagem()) ? DateUtil.formatTo(item.getDataSegundaPesagem(), "HH:mm:ss") : "");
                
                
                
                if (!itemAdicionado(idsItensAdicionados, item.getIdItem())) {

                    GerarRelatorioExcel.criarDoubleCell(rowItem, 17, styles.getCellStyleValuePesos(), new Double(ObjetoUtil.isNotNull(item.getPesoBruto()) ? item.getPesoBruto().toString() : "0"));

                    GerarRelatorioExcel.criarDoubleCell(rowItem, 18, styles.getCellStyleValuePesos(), ObjetoUtil.isNotNull(item.getQuantidadePesagensBrutas()) ? item.getQuantidadePesagensBrutas().doubleValue() : 0);

                    GerarRelatorioExcel.criarDoubleCell(rowItem, 19, styles.getCellStyleValuePesos(), item.getPesoLiquidoValido().doubleValue());
                }
                
                GerarRelatorioExcel.criarCell(rowItem, 20, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getDataCriacaoNota()) ? DateUtil.formatTo(item.getDataCriacaoNota(), "dd/MM/yyyy") : "");

                GerarRelatorioExcel.criarCell(rowItem, 21, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getDataCriacaoNota()) ? DateUtil.formatTo(item.getDataCriacaoNota(), "HH:mm:ss") : "");
                

                GerarRelatorioExcel.criarCell(rowItem, 22, styles.getCellStyleValue(), item.getNota());

                GerarRelatorioExcel.criarCell(rowItem, 23, styles.getCellStyleValue(), item.getRemessa());

                GerarRelatorioExcel.criarCell(rowItem, 24, styles.getCellStyleValue(), item.getNumeroOrdemTransporte());

                if (!itemAdicionado(idsItensAdicionados, item.getIdItem())) {

                    GerarRelatorioExcel.criarDoubleCell(rowItem, 25, styles.getCellStyleValuePesos(), item.getQtdKgLValido().doubleValue());
                }

                GerarRelatorioExcel.criarCell(rowItem, 26, styles.getCellStyleValue(), item.getDescricaoMotivo());

                GerarRelatorioExcel.criarCell(rowItem, 27, styles.getCellStyleValue(), item.getRomaneioVinculadoPorMotivo());

                GerarRelatorioExcel.criarCell(rowItem, 28, styles.getCellStyleValue(), item.getRestricoes());
                

                idsItensAdicionados.add(item.getIdItem());

                linha.set(linha.incrementAndGet());
            });

            Row rowSubTotal = sheet.createRow(linha.get());

            GerarRelatorioExcel.criarCell(rowSubTotal, 0, styles.getCellStyleSummary(), "Total");

            sheet.addMergedRegion(new CellRangeAddress(linha.get(), linha.get(), 0, 18));

            GerarRelatorioExcel.criarDoubleCell(rowSubTotal, 19, styles.getCellStyleValuePesos(),
                    movimentacoes.stream().filter(distinctByKey(RelatorioComercialDTO::getIdItem)).map(RelatorioComercialDTO::getPesoLiquidoValido).reduce(BigDecimal.ZERO, BigDecimal::add).doubleValue());

            GerarRelatorioExcel.criarDoubleCell(rowSubTotal, 25, styles.getCellStyleValuePesos(),
                    movimentacoes.stream().filter(distinctByKey(RelatorioComercialDTO::getIdItem)).map(RelatorioComercialDTO::getQtdKgLValido).reduce(BigDecimal.ZERO, BigDecimal::add).doubleValue());

            GerarRelatorioExcel.criarCell(rowSubTotal, 20, styles.getCellStyleSummary(), "");

            sheet.addMergedRegion(new CellRangeAddress(linha.get(), linha.get(), 20, 24));

            linha.set(linha.incrementAndGet());
        });

        Row rowTotal = sheet.createRow(linha.get());

        GerarRelatorioExcel.criarCell(rowTotal, 0, styles.getCellStyleSummary(), "Total");

        for (int i = 1; i <= 18; i++) {

            GerarRelatorioExcel.criarCell(rowTotal, i, styles.getCellStyleSummary(), "");
        }

        for (int i = 20; i <= 24; i++) {

            GerarRelatorioExcel.criarCell(rowTotal, i, styles.getCellStyleSummary(), "");
        }

        GerarRelatorioExcel.criarCell(rowTotal, 19, styles.getCellStyleSummary(), dtos.stream().filter(distinctByKey(RelatorioComercialDTO::getIdItem)).map(RelatorioComercialDTO::getPesoLiquidoValido).reduce(BigDecimal.ZERO,
                BigDecimal::add).toString());

        GerarRelatorioExcel.criarCell(rowTotal, 25, styles.getCellStyleSummary(),
                dtos.stream().filter(distinctByKey(RelatorioComercialDTO::getIdItem)).map(RelatorioComercialDTO::getQtdKgLValido).reduce(BigDecimal.ZERO, BigDecimal::add).toString());

        sheet.addMergedRegion(new CellRangeAddress(linha.get(), linha.get(), 0, 18));

        linha.set(linha.incrementAndGet());

        for (int i = 0; i <= 27; i++) {

            sheet.setAutobreaks(true);

            sheet.autoSizeColumn(i);
        }
    }

    private static void montarRelatorioFluxoDeCarregamento(Collection<RelatorioFluxoDeCarregamentoDTO> dtos, XSSFSheet sheet, XSSFWorkbook workbook) {

        GerarRealtorioExcelStyles styles = GerarRealtorioExcelStyles.build(workbook);

        Row rowTituloTabela = sheet.createRow(1);

        rowTituloTabela.setHeight((short) 500);

        sheet.setColumnWidth(0, 500);

        GerarRelatorioExcel.criarCell(rowTituloTabela, 0, styles.getCellStyleLabel(), "Status do Carregamento");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 1, styles.getCellStyleLabel(), "Status do romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 2, styles.getCellStyleLabel(), "Status do SAP");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 3, styles.getCellStyleLabel(), "Número da ordem");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 4, styles.getCellStyleLabel(), "Número do romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 5, styles.getCellStyleLabel(), "Material");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 6, styles.getCellStyleLabel(), "Cliente");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 7, styles.getCellStyleLabel(), "Operador Logistico");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 8, styles.getCellStyleLabel(), "Cidade do Operador Logistico");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 9, styles.getCellStyleLabel(), "Transportadora");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 10, styles.getCellStyleLabel(), "Transportadora Subcontratada");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 11, styles.getCellStyleLabel(), "Placa do veículo");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 12, styles.getCellStyleLabel(), "Motorista");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 13, styles.getCellStyleLabel(), "Data Senha");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 14, styles.getCellStyleLabel(), "Hora Senha");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 15, styles.getCellStyleLabel(), "Tempo Geração de Romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 16, styles.getCellStyleLabel(), "Data Romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 17, styles.getCellStyleLabel(), "Hora Romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 18, styles.getCellStyleLabel(), "Tempo de Pátio Externo");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 19, styles.getCellStyleLabel(), "Data 1ª pesagem");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 20, styles.getCellStyleLabel(), "Hora 1ª pesagem");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 21, styles.getCellStyleLabel(), "Tempo em Carregamento");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 22, styles.getCellStyleLabel(), "Data 2ª pesagem");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 23, styles.getCellStyleLabel(), "Hora 2ª pesagem");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 24, styles.getCellStyleLabel(), "Tempo em Analise de Qualidade");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 25, styles.getCellStyleLabel(), "Data da Analise");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 26, styles.getCellStyleLabel(), "Hora da Analise");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 27, styles.getCellStyleLabel(), "Tempo de Emissão de Documentos");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 28, styles.getCellStyleLabel(), "Data Emissão NFe");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 29, styles.getCellStyleLabel(), "Hora Emissão NFe");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 30, styles.getCellStyleLabel(), "Nr. nota");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 31, styles.getCellStyleLabel(), "Qtde KG/L");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 32, styles.getCellStyleLabel(), "Tempo Total na Empresa");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 33, styles.getCellStyleLabel(), "Data Agendamento");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 34, styles.getCellStyleLabel(), "Hora Agendamento");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 35, styles.getCellStyleLabel(), "Total de Horas Estadia a partir do Agendamento");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 36, styles.getCellStyleLabel(), "Tempo Pátio Externo Agendamento");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 37, styles.getCellStyleLabel(), "Qdte. de Horas Adiantado Veículo chegou");

        AtomicInteger linha = new AtomicInteger(2);

        Map<String, List<RelatorioFluxoDeCarregamentoDTO>> itensPorOrdem = dtos.stream().collect(Collectors.groupingBy(RelatorioFluxoDeCarregamentoDTO::getCodigoOrdemVenda));

        itensPorOrdem.keySet().forEach(ordem -> {

            final List<RelatorioFluxoDeCarregamentoDTO> movimentacoes = itensPorOrdem.get(ordem);

            movimentacoes.stream().forEach(item -> {

                Row rowItem = sheet.createRow(linha.get());

                rowItem.setHeight((short) 300);

                GerarRelatorioExcel.criarCell(rowItem, 0, styles.getCellStyleValue(), item.getStatusCarregamento());

                GerarRelatorioExcel.criarCell(rowItem, 1, styles.getCellStyleValue(), item.getStatusRomaneio());

                GerarRelatorioExcel.criarCell(rowItem, 2, styles.getCellStyleValue(), item.getStatusSAP());

                GerarRelatorioExcel.criarCell(rowItem, 3, styles.getCellStyleValue(), item.getCodigoOrdemVenda());

                GerarRelatorioExcel.criarCell(rowItem, 4, styles.getCellStyleValue(), item.getNumeroRomaneio());

                GerarRelatorioExcel.criarCell(rowItem, 5, styles.getCellStyleValue(), item.getMaterial());

                GerarRelatorioExcel.criarCell(rowItem, 6, styles.getCellStyleValue(), item.getCliente());

                GerarRelatorioExcel.criarCell(rowItem, 7, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getOperadorLogistico()) ? item.getOperadorLogistico().getDescricaoFormatada() : StringUtil.empty());

                GerarRelatorioExcel.criarCell(rowItem, 8, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getOperadorLogistico()) ? item.getOperadorLogistico().getEnderecoFormatado() : StringUtil.empty());

                GerarRelatorioExcel.criarCell(rowItem, 9, styles.getCellStyleValue(), item.getTransportadora());

                GerarRelatorioExcel.criarCell(rowItem, 10, styles.getCellStyleValue(), item.getTransportadoraSubcontratada());

                GerarRelatorioExcel.criarCell(rowItem, 11, styles.getCellStyleValue(), item.getPlaca());

                GerarRelatorioExcel.criarCell(rowItem, 12, styles.getCellStyleValue(), item.getMotorista());

                GerarRelatorioExcel.criarCell(rowItem, 13, styles.getCellStyleValue(), item.getDtEmissaoSenha());

                GerarRelatorioExcel.criarCell(rowItem, 14, styles.getCellStyleValue(), item.getHoraEmissaoSenha());

                GerarRelatorioExcel.criarCell(rowItem, 15, styles.getCellStyleValue(), item.getTempoGeracaoRomaneio());

                GerarRelatorioExcel.criarCell(rowItem, 16, styles.getCellStyleValue(), DateUtil.formatData(item.getDataRomaneio()));

                GerarRelatorioExcel.criarCell(rowItem, 17, styles.getCellStyleValue(), DateUtil.formatHora(item.getDataRomaneio()));

                GerarRelatorioExcel.criarCell(rowItem, 18, styles.getCellStyleValue(), item.getTempoPatioExterno());

                GerarRelatorioExcel.criarCell(rowItem, 19, styles.getCellStyleValue(), DateUtil.formatData(item.getDtPrimeiraPesagem()));

                GerarRelatorioExcel.criarCell(rowItem, 20, styles.getCellStyleValue(), DateUtil.formatHora(item.getDtPrimeiraPesagem()));

                GerarRelatorioExcel.criarCell(rowItem, 21, styles.getCellStyleValue(), item.getTempoEmCarregamento());

                GerarRelatorioExcel.criarCell(rowItem, 22, styles.getCellStyleValue(), DateUtil.formatData(item.getDtSegundaPesagem()));

                GerarRelatorioExcel.criarCell(rowItem, 23, styles.getCellStyleValue(), DateUtil.formatHora(item.getDtSegundaPesagem()));

                GerarRelatorioExcel.criarCell(rowItem, 24, styles.getCellStyleValue(), item.getTempoAnaliseQualidade());

                GerarRelatorioExcel.criarCell(rowItem, 25, styles.getCellStyleValue(), DateUtil.formatData(item.getDataAnaliseQualidade()));

                GerarRelatorioExcel.criarCell(rowItem, 26, styles.getCellStyleValue(), DateUtil.formatHora(item.getDataAnaliseQualidade()));

                GerarRelatorioExcel.criarCell(rowItem, 27, styles.getCellStyleValue(), item.getTempoEmissaoNfe());

                GerarRelatorioExcel.criarCell(rowItem, 28, styles.getCellStyleValue(), DateUtil.formatData(item.getDataEmissaoNfe()));

                GerarRelatorioExcel.criarCell(rowItem, 29, styles.getCellStyleValue(), DateUtil.formatHora(item.getDataEmissaoNfe()));

                GerarRelatorioExcel.criarCell(rowItem, 30, styles.getCellStyleValue(), item.getNota());

                GerarRelatorioExcel.criarDoubleCell(rowItem, 31, styles.getCellStyleValuePesos(), item.getQtdKgLValido().doubleValue());

                GerarRelatorioExcel.criarCell(rowItem, 32, styles.getCellStyleValue(), item.getTempoTotalNaEmpresa());

                GerarRelatorioExcel.criarCell(rowItem, 33, styles.getCellStyleValue(), ObjetoUtil.isNotNull(item.getCargaPontualDataAgendamentoInicial()) ? DateUtil.formatData(item.getCargaPontualDataAgendamentoInicial()) : StringUtil.empty());

                String periodoAgendamento = StringUtil.empty();

                if (ObjetoUtil.isNotNull(item.getCargaPontualDataAgendamentoInicial()) && ObjetoUtil.isNotNull(item.getCargaPontualDataAgendamentoFinal())) {

                    periodoAgendamento = DateUtil.formatHora(item.getCargaPontualDataAgendamentoInicial()) + " - " + DateUtil.formatHora(item.getCargaPontualDataAgendamentoFinal());

                } else if (ObjetoUtil.isNotNull(item.getCargaPontualDataAgendamentoInicial())) {

                    periodoAgendamento = DateUtil.formatHora(item.getCargaPontualDataAgendamentoInicial());
                }

                GerarRelatorioExcel.criarCell(rowItem, 34, styles.getCellStyleValue(), periodoAgendamento);

                GerarRelatorioExcel.criarCell(rowItem, 35, styles.getCellStyleValue(), item.getTempoTotalHorasEstadiaAgendamento());

                GerarRelatorioExcel.criarCell(rowItem, 36, styles.getCellStyleValue(), item.getTempoTotalPatioExternoAgendamento());

                GerarRelatorioExcel.criarCell(rowItem, 37, styles.getCellStyleValue(), item.getTempoTotalVeiculoAgendamento());

                linha.set(linha.incrementAndGet());
            });

            Row rowSubTotal = sheet.createRow(linha.get());

            GerarRelatorioExcel.criarCell(rowSubTotal, 0, styles.getCellStyleSummary(), "Total");

            sheet.addMergedRegion(new CellRangeAddress(linha.get(), linha.get(), 0, 30));

            GerarRelatorioExcel.criarDoubleCell(rowSubTotal, 31, styles.getCellStyleValuePesos(), movimentacoes.stream().map(RelatorioFluxoDeCarregamentoDTO::getQtdKgLValido).reduce(BigDecimal.ZERO, BigDecimal::add).doubleValue());

            linha.set(linha.incrementAndGet());
        });

        Row rowTotal = sheet.createRow(linha.get());

        GerarRelatorioExcel.criarCell(rowTotal, 0, styles.getCellStyleSummary(), "Total");

        for (int i = 1; i <= 37; i++) {

            GerarRelatorioExcel.criarCell(rowTotal, i, styles.getCellStyleSummary(), "");
        }

        GerarRelatorioExcel.criarDoubleCell(rowTotal, 31, styles.getCellStyleValuePesos(), dtos.stream().map(RelatorioFluxoDeCarregamentoDTO::getQtdKgLValido).reduce(BigDecimal.ZERO, BigDecimal::add).doubleValue());

        sheet.addMergedRegion(new CellRangeAddress(linha.get(), linha.get(), 0, 30));

        linha.set(linha.incrementAndGet());

        for (int i = 0; i <= 37; i++) {

            sheet.autoSizeColumn(i);
        }
    }

    private static void montarRelatorioFilas(Collection<SenhaEtapaRelatorioDTO> dtos, XSSFSheet sheet, XSSFWorkbook workbook) {

        GerarRealtorioExcelStyles styles = GerarRealtorioExcelStyles.build(workbook);

        Row rowTituloTabela = sheet.createRow(1);

        rowTituloTabela.setHeight((short) 500);

        sheet.setColumnWidth(0, 500);

        GerarRelatorioExcel.criarCell(rowTituloTabela, 0, styles.getCellStyleLabel(), "Senha");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 1, styles.getCellStyleLabel(), "Ordem de Venda");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 2, styles.getCellStyleLabel(), "Material");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 3, styles.getCellStyleLabel(), "Cliente");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 4, styles.getCellStyleLabel(), "Número do romaneio");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 5, styles.getCellStyleLabel(), "Motorista");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 6, styles.getCellStyleLabel(), "Placa Veículo");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 7, styles.getCellStyleLabel(), "Nº Cartão");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 8, styles.getCellStyleLabel(), "Etapa");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 9, styles.getCellStyleLabel(), "Data Entrada");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 10, styles.getCellStyleLabel(), "Hora Entrada");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 11, styles.getCellStyleLabel(), "Data Saída");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 12, styles.getCellStyleLabel(), "Hora Saída");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 13, styles.getCellStyleLabel(), "Tempo (minutos)");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 14, styles.getCellStyleLabel(), "Status");

        /// GERAR OS CAMPOS DINAMICOS DAQUELE FLUXO
        Map<String, List<SenhaEtapaRelatorioDTO>> itensPorOrdem = new HashMap<String, List<SenhaEtapaRelatorioDTO>>();

        for (SenhaEtapaRelatorioDTO senhaEtapa : dtos) {
            if(itensPorOrdem.containsKey(senhaEtapa.getSenha())){
                itensPorOrdem.get(senhaEtapa.getSenha()).add(senhaEtapa);
            }
            else
            {
                List<SenhaEtapaRelatorioDTO> etapas = new ArrayList<SenhaEtapaRelatorioDTO>();
                etapas.add(senhaEtapa);

                itensPorOrdem.put(senhaEtapa.getSenha(), etapas);
            }
        }

        AtomicInteger linha = new AtomicInteger(2);
        itensPorOrdem.keySet().forEach(ordem -> {

            final List<SenhaEtapaRelatorioDTO> movimentacoes = itensPorOrdem.get(ordem).stream().sorted(Comparator.comparing(SenhaEtapaRelatorioDTO::getHoraEntrada).reversed()).collect(Collectors.toList());

            movimentacoes.stream().forEach(item -> {

                Row rowItem = sheet.createRow(linha.get());

                rowItem.setHeight((short) 300);

                GerarRelatorioExcel.criarCell(rowItem, 0, styles.getCellStyleValue(), item.getSenha());

                GerarRelatorioExcel.criarCell(rowItem, 1, styles.getCellStyleValue(), item.getOrdemVenda());

                GerarRelatorioExcel.criarCell(rowItem, 2, styles.getCellStyleValue(), item.getCodigoMaterial() + " - " + item.getNomeMaterial());

                GerarRelatorioExcel.criarCell(rowItem, 3, styles.getCellStyleValue(), item.getNomeCliente());

                GerarRelatorioExcel.criarCell(rowItem, 4, styles.getCellStyleValue(), item.getNumeroRomaneio());

                GerarRelatorioExcel.criarCell(rowItem, 5, styles.getCellStyleValue(), item.getNomeMotorista());

                GerarRelatorioExcel.criarCell(rowItem, 6, styles.getCellStyleValue(), item.getPlacaVeiculo());

                GerarRelatorioExcel.criarCell(rowItem, 7, styles.getCellStyleValue(), item.getNumeroCartaoAcesso() != null ? item.getNumeroCartaoAcesso().toString() : "");

                GerarRelatorioExcel.criarCell(rowItem, 8, styles.getCellStyleValue(), item.getEtapa().getDescricao());

                GerarRelatorioExcel.criarCell(rowItem, 9, styles.getCellStyleValue(), item.getDataEntradaFormatada());

                GerarRelatorioExcel.criarCell(rowItem, 10, styles.getCellStyleValue(), item.getHoraEntradaFormatada());

                GerarRelatorioExcel.criarCell(rowItem, 11, styles.getCellStyleValue(), item.getDataSaidaFormatada());

                GerarRelatorioExcel.criarCell(rowItem, 12, styles.getCellStyleValue(), item.getHoraSaidaFormatada());

                GerarRelatorioExcel.criarCell(rowItem, 13, styles.getCellStyleValue(), item.getTempoEsperaFormatado());

                GerarRelatorioExcel.criarCell(rowItem, 14, styles.getCellStyleValue(), item.getStatusEtapa());

                linha.set(linha.incrementAndGet());
            });

            linha.set(linha.incrementAndGet());
        });

        for (int i = 0; i <= 14; i++) {
            sheet.autoSizeColumn(i);
        }
    }

    private static void montarRelatorioPortariaTicket(Collection<PortariaTicket> dtos, XSSFSheet sheet, XSSFWorkbook workbook) {

        GerarRealtorioExcelStyles styles = GerarRealtorioExcelStyles.build(workbook);

        Row rowTituloTabela = sheet.createRow(1);

        rowTituloTabela.setHeight((short) 500);

        sheet.setColumnWidth(0, 500);

        GerarRelatorioExcel.criarCell(rowTituloTabela, 0, styles.getCellStyleLabel(), "Posição");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 1, styles.getCellStyleLabel(), "Portaria");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 2, styles.getCellStyleLabel(), "Data Marcação Vez");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 3, styles.getCellStyleLabel(), "Data Entrada");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 4, styles.getCellStyleLabel(), "Data Saída");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 5, styles.getCellStyleLabel(), "Motivo");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 6, styles.getCellStyleLabel(), "Motivo Complemento");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 7, styles.getCellStyleLabel(), "CNPJ / CPF");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 8, styles.getCellStyleLabel(), "Empresa");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 9, styles.getCellStyleLabel(), "CPF do Motorista / Condutor");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 10, styles.getCellStyleLabel(), "Nome do Motorista / Condutor");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 11, styles.getCellStyleLabel(), "ID SJC Motorista");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 12, styles.getCellStyleLabel(), "Veículo");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 13, styles.getCellStyleLabel(), "Placa Veículo / Cavalo");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 14, styles.getCellStyleLabel(), "Placa Carreta 1");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 15, styles.getCellStyleLabel(), "Placa Carreta 2");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 16, styles.getCellStyleLabel(), "Placa Carreta 3");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 17, styles.getCellStyleLabel(), "Produto / Item");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 18, styles.getCellStyleLabel(), "Observação");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 19, styles.getCellStyleLabel(), "Nome Colaborador Portaria");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 20, styles.getCellStyleLabel(), "ID Usuário Portaria");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 21, styles.getCellStyleLabel(), "Estornado");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 22, styles.getCellStyleLabel(), "Data Estorno / Cancelamento");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 23, styles.getCellStyleLabel(), "ID Usuário Portaria / Estorno / Cancelamento");

        GerarRelatorioExcel.criarCell(rowTituloTabela, 24, styles.getCellStyleLabel(), "Motivo / Estorno / Cancelamento");


        AtomicInteger linha = new AtomicInteger(2);

        dtos.stream().forEach(item -> {

            Row rowItem = sheet.createRow(linha.get());

            rowItem.setHeight((short) 300);

            GerarRelatorioExcel.criarCell(rowItem, 0, styles.getCellStyleValue(), item.getSenha());

            GerarRelatorioExcel.criarCell(rowItem, 1, styles.getCellStyleValue(), MessageFormat.format("{0} - {1}", item.getPortaria().getSigla(), item.getPortaria().getDescricao()));

            GerarRelatorioExcel.criarCell(rowItem, 2, styles.getCellStyleValue(), item.getDataMarcacao() != null ? DateUtil.format("dd/MM/yyyy HH:mm", item.getDataMarcacao()) : "");

            GerarRelatorioExcel.criarCell(rowItem, 3, styles.getCellStyleValue(), item.getDataEntrada() != null ? DateUtil.format("dd/MM/yyyy HH:mm", item.getDataEntrada()) : "");

            GerarRelatorioExcel.criarCell(rowItem, 4, styles.getCellStyleValue(), item.getDataSaida() != null ? DateUtil.format("dd/MM/yyyy HH:mm", item.getDataSaida()) : "");

            GerarRelatorioExcel.criarCell(rowItem, 5, styles.getCellStyleValue(), MessageFormat.format("{0} - {1}", item.getMotivo().getId(), item.getMotivo().getDescricao()));

            GerarRelatorioExcel.criarCell(rowItem, 6, styles.getCellStyleValue(), item.getMotivoComplemento());

            GerarRelatorioExcel.criarCell(rowItem, 7, styles.getCellStyleValuePreco(), item.getCnpj());

            GerarRelatorioExcel.criarCell(rowItem, 8, styles.getCellStyleValuePreco(), item.getEmpresa());

            GerarRelatorioExcel.criarCell(rowItem, 9, styles.getCellStyleValuePesos(), item.getCpfMotorista());

            GerarRelatorioExcel.criarCell(rowItem, 10, styles.getCellStyleValuePesos(), item.getMotorista());

            GerarRelatorioExcel.criarCell(rowItem, 11, styles.getCellStyleValuePesos(), item.getIdSjcMotorista());

            GerarRelatorioExcel.criarCell(rowItem, 12, styles.getCellStyleValue(), MessageFormat.format("{0} - {1}", item.getVeiculo().getId(), item.getVeiculo().getDescricao()));

            GerarRelatorioExcel.criarCell(rowItem, 13, styles.getCellStyleValuePesos(), item.getPlacaVeiculo());

            GerarRelatorioExcel.criarCell(rowItem, 14, styles.getCellStyleValuePesos(), item.getPlacaCarreta1());

            GerarRelatorioExcel.criarCell(rowItem, 15, styles.getCellStyleValuePesos(), item.getPlacaCarreta2());

            GerarRelatorioExcel.criarCell(rowItem, 16, styles.getCellStyleValuePesos(), item.getPlacaCarreta3());

            GerarRelatorioExcel.criarCell(rowItem, 17, styles.getCellStyleValue(), MessageFormat.format("{0} - {1}", item.getProduto().getId(), item.getProduto().getDescricao()));

            GerarRelatorioExcel.criarCell(rowItem, 18, styles.getCellStyleValuePesos(), item.getObservacao());

            GerarRelatorioExcel.criarCell(rowItem, 19, styles.getCellStyleValuePesos(), item.getNomeColaborador());

            GerarRelatorioExcel.criarCell(rowItem, 20, styles.getCellStyleValuePesos(), item.getIdColaborador());

            GerarRelatorioExcel.criarCell(rowItem, 21, styles.getCellStyleValue(), item.isEstorno() ? "Sim" : "Não");

            GerarRelatorioExcel.criarCell(rowItem, 22, styles.getCellStyleValue(), item.getDataEstorno() != null ? DateUtil.format("dd/MM/yyyy HH:mm", item.getDataEstorno()) : "");

            GerarRelatorioExcel.criarCell(rowItem, 23, styles.getCellStyleValue(), item.getIdColaboradorEstorno());

            GerarRelatorioExcel.criarCell(rowItem, 24, styles.getCellStyleValue(), item.getMotivoEstorno());

            linha.set(linha.incrementAndGet());
        });

        for (int i = 0; i <= 19; i++) {

            sheet.autoSizeColumn(i);
        }


    }

    private static void criarCell(Row row, int posicao, CellStyle cellStyle, final String valor) {

        Cell cell = row.createCell(posicao);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(StringUtil.isNotNullEmpty(valor) ? valor : StringUtil.empty());
    }

    private static void criarDoubleCell(Row row, int posicao, CellStyle cellStyle, final Double valor) {

        Cell cell = row.createCell(posicao);
        cell.setCellStyle(cellStyle);

        if (Objects.isNull(valor)) {
            cell.setCellValue("");

        } else {
            cell.setCellValue(valor);

        }
    }

    private static void criarDoubleCell(Row row, int posicao, CellStyle cellStyle, final BigDecimal valor) {

        Cell cell = row.createCell(posicao);
        cell.setCellStyle(cellStyle);

        if (Objects.isNull(valor)) {
            cell.setCellValue("");

        } else {
            cell.setCellValue(valor.doubleValue());

        }
    }

    private static FileResponse escreverArquivo(final XSSFWorkbook workbook) throws Exception {

        final String file = FileUtils.getTempDirectory().toPath() + File.separator + "relatório_" + DateUtil.format("ddMMyyyyHHmmss", DateUtil.hoje()) + ".xls";

        try (FileOutputStream fileOut = new FileOutputStream(file)) {
            workbook.write(fileOut);
            fileOut.flush();
        }

        byte[] fileBytes;
        try {
            fileBytes = Files.readAllBytes(Paths.get(file));
        } finally {
            FileUtils.deleteQuietly(new File(file));
        }

        return FileResponse.builder().name("relatório_" + DateUtil.format("ddMMyyyyHHmmss", DateUtil.hoje()) + ".xls")
                .buffer(fileBytes).build();
    }

    private static boolean itemAdicionado(Set<Long> list, Long id) {

        return list.stream().filter(Predicate.isEqual(id)).count() > 0;
    }

    /**
     * Método responsável por
     *
     * @param keyExtractor
     *
     * @return
     *
     * @author Claudio Gomes
     */
    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {

        Map<Object, Boolean> map = new ConcurrentHashMap<>();

        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    public static FileResponse obterRelatorioAnaliseConversao(List<RelatorioAnaliseConversaoDTO> dtos) {

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {
            XSSFSheet sheet = workbook.createSheet("Relatório Análise de Conversão");

            GerarRelatorioExcel.montarRelatorioAnaliseConversao(dtos, sheet, workbook);

            return escreverArquivo(workbook);

        } catch (Exception e) {
            e.printStackTrace();

        }

        return null;
    }

    private static void montarRelatorioAnaliseConversao(List<RelatorioAnaliseConversaoDTO> dtos, XSSFSheet sheet, XSSFWorkbook workbook) {

        final int COLUNA_A_NUMERO_ROMANEIO = 0;
        final int COLUNA_B_DATA_CONVERSAO = 1;
        final int COLUNA_C_TEMP_TANQUE = 2;
        final int COLUNA_D_GRAU_INPM = 3;
        final int COLUNA_E_ACIDEZ = 4;
        final int COLUNA_F_NR_TANQUE = 5;
        final int COLUNA_G_VOLUME_SETA = 6;
        final int COLUNA_H_VOLUME_DENSIDADE = 7;
        final int COLUNA_I_DIFERENCA_LITROS = 8;
        final int COLUNA_J_QUANTIDADE = 9;

        GerarRealtorioExcelStyles styles = GerarRealtorioExcelStyles.build(workbook);

        Row rowTituloTabela = sheet.createRow(1);
        rowTituloTabela.setHeight((short) 500);

        sheet.setColumnWidth(0, 500);

        GerarRelatorioExcel.criarCell(rowTituloTabela, COLUNA_A_NUMERO_ROMANEIO, styles.getCellStyleLabel(), "Número romaneio");
        GerarRelatorioExcel.criarCell(rowTituloTabela, COLUNA_B_DATA_CONVERSAO, styles.getCellStyleLabel(), "Data da conversão");
        GerarRelatorioExcel.criarCell(rowTituloTabela, COLUNA_C_TEMP_TANQUE, styles.getCellStyleLabel(), "Temp.do Tanque");
        GerarRelatorioExcel.criarCell(rowTituloTabela, COLUNA_D_GRAU_INPM, styles.getCellStyleLabel(), "Grau I.N.P.M.");
        GerarRelatorioExcel.criarCell(rowTituloTabela, COLUNA_E_ACIDEZ, styles.getCellStyleLabel(), "Acidez");
        GerarRelatorioExcel.criarCell(rowTituloTabela, COLUNA_F_NR_TANQUE, styles.getCellStyleLabel(), "Nr. Tanque");
        GerarRelatorioExcel.criarCell(rowTituloTabela, COLUNA_G_VOLUME_SETA, styles.getCellStyleLabel(), "Volume Seta (corrigido a 20ºC) litros");
        GerarRelatorioExcel.criarCell(rowTituloTabela, COLUNA_H_VOLUME_DENSIDADE, styles.getCellStyleLabel(), "Volume de acordo com a densidade a 20ºC");
        GerarRelatorioExcel.criarCell(rowTituloTabela, COLUNA_I_DIFERENCA_LITROS, styles.getCellStyleLabel(), "Diferença (litros)");
        GerarRelatorioExcel.criarCell(rowTituloTabela, COLUNA_J_QUANTIDADE, styles.getCellStyleLabel(), "Qntd. de Conversões");

        AtomicInteger linha = new AtomicInteger(2);

        dtos.forEach(dto -> {
            Row rowItem = sheet.createRow(linha.getAndIncrement());

            GerarRelatorioExcel.criarCell(rowItem, COLUNA_A_NUMERO_ROMANEIO, styles.getCellStyleValue(), dto.getNumeroRomaneio());
            GerarRelatorioExcel.criarCell(rowItem, COLUNA_B_DATA_CONVERSAO, styles.getCellStyleValue(), DateUtil.format("dd/MM/yyyy HH:mm", dto.getDataConversao()));
            GerarRelatorioExcel.criarDoubleCell(rowItem, COLUNA_C_TEMP_TANQUE, styles.getCellStyleValuePesos(), dto.getTempTanque());
            GerarRelatorioExcel.criarDoubleCell(rowItem, COLUNA_D_GRAU_INPM, styles.getCellStyleValuePesos(), dto.getGrauInpm());
            GerarRelatorioExcel.criarDoubleCell(rowItem, COLUNA_E_ACIDEZ, styles.getCellStyleValuePesos(), dto.getAcidez());
            GerarRelatorioExcel.criarCell(rowItem, COLUNA_F_NR_TANQUE, styles.getCellStyleValuePesos(), ObjetoUtil.isNotNull(dto.getTanque()) ? dto.getTanque().toString() : StringUtil.empty());
            GerarRelatorioExcel.criarDoubleCell(rowItem, COLUNA_G_VOLUME_SETA, styles.getCellStyleValuePesos(), dto.getVolumeSetaVinteGraus());
            GerarRelatorioExcel.criarDoubleCell(rowItem, COLUNA_H_VOLUME_DENSIDADE, styles.getCellStyleValuePesos(), dto.getVolumeDensidadeLVinteGraus());
            GerarRelatorioExcel.criarDoubleCell(rowItem, COLUNA_I_DIFERENCA_LITROS, styles.getCellStyleValuePesos(), dto.getDiferencaLitros());

            GerarRelatorioExcel.criarDoubleCell(rowItem, COLUNA_J_QUANTIDADE, styles.getCellStyleValuePesos(), Objects.isNull(dto.getQuantidadeConversoes()) ? 0 : dto.getQuantidadeConversoes().doubleValue());
        });

        for (int indexColuna = COLUNA_A_NUMERO_ROMANEIO; indexColuna <= COLUNA_J_QUANTIDADE; indexColuna++) {
            sheet.autoSizeColumn(indexColuna);
        }
    }

    @Getter
    private static class GerarRealtorioExcelStyles {

        private XSSFWorkbook workbook;

        private XSSFFont fontLabelTabela;

        private XSSFFont fontValueTabela;

        private XSSFFont fontLabelSummary;

        private XSSFCellStyle cellStyleLabel;

        private XSSFCellStyle cellStyleValue;

        private XSSFCellStyle cellStyleValuePorcentagem;

        private XSSFCellStyle cellStyleValuePesos;

        private XSSFCellStyle cellStyleValuePreco;

        private XSSFCellStyle cellStyleSummary;

        private GerarRealtorioExcelStyles(XSSFWorkbook workbook) {

            this.workbook = workbook;
        }

        public static GerarRealtorioExcelStyles build(XSSFWorkbook workbook) {

            GerarRealtorioExcelStyles styles = new GerarRealtorioExcelStyles(workbook);
            styles.criarFontes();
            styles.criarCellStyles();

            return styles;
        }

        private void criarFontes() {

            fontLabelTabela = fontStyleLabel(workbook);
            fontValueTabela = fontStyleValue(workbook);
            fontLabelSummary = fontStyleSummary(workbook);
        }

        private void criarCellStyles() {

            cellStyleLabel = cellStyleLabel(workbook, fontLabelTabela);
            cellStyleValue = cellStyleValue(workbook, fontValueTabela, false);
            cellStyleValuePorcentagem = cellStyleValue(workbook, fontValueTabela, true, "#,#0");
            cellStyleValuePesos = cellStyleValue(workbook, fontValueTabela, true, "#,##0");
            cellStyleValuePreco = cellStyleValue(workbook, fontValueTabela, true, "R$ #,##0");
            cellStyleSummary = cellStyleSummary(workbook, fontLabelSummary);
        }

        private XSSFFont fontStyleValue(XSSFWorkbook workbook) {

            XSSFFont font = workbook.createFont();
            font.setFontName(XSSFFont.DEFAULT_FONT_NAME);
            font.setFontHeightInPoints((short) 9);
            IndexedColorMap colorMap = workbook.getStylesSource().getIndexedColors();
            font.setColor(new XSSFColor(new java.awt.Color(0, 0, 0), colorMap));

            return font;
        }

        private XSSFFont fontStyleLabel(XSSFWorkbook workbook) {

            XSSFFont font = workbook.createFont();
            font.setFontName(XSSFFont.DEFAULT_FONT_NAME);
            IndexedColorMap colorMap = workbook.getStylesSource().getIndexedColors();
            font.setColor(new XSSFColor(new java.awt.Color(255, 255, 255), colorMap));
            font.setBold(true);

            return font;
        }

        private XSSFFont fontStyleSummary(XSSFWorkbook workbook) {

            XSSFFont font = workbook.createFont();
            font.setFontName(XSSFFont.DEFAULT_FONT_NAME);
            font.setFontHeightInPoints((short) 10);
            IndexedColorMap colorMap = workbook.getStylesSource().getIndexedColors();
            font.setColor(new XSSFColor(new java.awt.Color(255, 255, 255), colorMap));
            font.setBold(true);

            return font;
        }

        @SuppressWarnings("Duplicates")
        private static XSSFCellStyle cellStyleLabel(XSSFWorkbook workbook, XSSFFont fontLabelTabela) {

            XSSFCellStyle style = workbook.createCellStyle();
            IndexedColorMap colorMap = workbook.getStylesSource().getIndexedColors();
            style.setFillForegroundColor(new XSSFColor(new java.awt.Color(245, 123, 32), colorMap));
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            style.setFont(fontLabelTabela);
            style.setAlignment(HorizontalAlignment.CENTER);
            style.setVerticalAlignment(VerticalAlignment.CENTER);
            style.setBorderBottom(BorderStyle.THIN);
            style.setBorderTop(BorderStyle.THIN);
            style.setBorderRight(BorderStyle.THIN);
            style.setBorderLeft(BorderStyle.THIN);

            return style;
        }

        @SuppressWarnings("Duplicates")
        private static XSSFCellStyle cellStyleSummary(XSSFWorkbook workbook, XSSFFont fontLabel) {

            XSSFCellStyle style = workbook.createCellStyle();
            IndexedColorMap colorMap = workbook.getStylesSource().getIndexedColors();
            style.setFillForegroundColor(new XSSFColor(new java.awt.Color(245, 123, 32), colorMap));
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            style.setFont(fontLabel);
            style.setAlignment(HorizontalAlignment.RIGHT);
            style.setVerticalAlignment(VerticalAlignment.CENTER);
            style.setBorderBottom(BorderStyle.THIN);
            style.setBorderTop(BorderStyle.THIN);
            style.setBorderRight(BorderStyle.THIN);
            style.setBorderLeft(BorderStyle.THIN);

            return style;
        }

        private static XSSFCellStyle cellStyleValue(XSSFWorkbook workbook, XSSFFont fontValueTabela, boolean rigth, String... format) {

            XSSFCellStyle style = workbook.createCellStyle();
            style.setFont(fontValueTabela);
            if (rigth) {
                style.setAlignment(HorizontalAlignment.RIGHT);
            }
            style.setVerticalAlignment(VerticalAlignment.CENTER);
            style.setBorderBottom(BorderStyle.THIN);
            style.setBorderTop(BorderStyle.THIN);
            style.setBorderRight(BorderStyle.THIN);
            style.setBorderLeft(BorderStyle.THIN);
            if (format != null && format.length > 0) {
                style.setDataFormat(HSSFDataFormat.getBuiltinFormat(format[0]));
            }

            return style;
        }

    }

}
