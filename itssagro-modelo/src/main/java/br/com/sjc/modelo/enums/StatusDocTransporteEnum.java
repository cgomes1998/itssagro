package br.com.sjc.modelo.enums;

public enum StatusDocTransporteEnum {

    CRIADA,
    ERRO_AO_CRIAR,
    ERRO_AO_MODIFICAR,
    ERRO_AO_DELETAR,
    DELETADA;
}
