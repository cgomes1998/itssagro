package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.request.item.EventOutRequestItem;
import br.com.sjc.modelo.sap.response.item.EventInResponseItem;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@SAPParameter(function = "/ITSSAGRO/FM_EVENT_IN")
public class EventInResponse extends JCoResponse {

    @SAPTable(name = "RETURN", item = EventInResponseItem.class)
    private List<EventInResponseItem> retorno;

    @SAPTable(name = "MSSDATA", item = EventOutRequestItem.class)
    private List<EventOutRequestItem> dadosRetorno;

}
