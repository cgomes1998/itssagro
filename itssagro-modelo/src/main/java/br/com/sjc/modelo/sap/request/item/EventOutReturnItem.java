package br.com.sjc.modelo.sap.request.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import com.sap.conn.jco.JCoMetaData;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * <b>Title:</b> FM_EVENT_OUT_ReturnItem.java
 * </p>
 *
 * <p>
 * <b>Description:</b> Registros da tabela <code>RETURN</code> da função <code>ZITSSAGRO_FM_EVENT_OUT</code>
 * </p>
 *
 * <p>
 * <b>Company: </b> ITSS Soluções em Tecnologia
 * </p>
 *
 * @author Bruno Zafalão
 * @version 1.0.0
 */
@Getter
@Setter
@SAPItem
public class EventOutReturnItem extends JCoItem {

    @SAPColumn(name = "FLOWD", size = 3, type = JCoMetaData.TYPE_CHAR, ordinal = 0)
    private String etapasFluxo;

    @SAPColumn(name = "MESSAGE", size = 100, type = JCoMetaData.TYPE_CHAR, ordinal = 1)
    private String message;

    @SAPColumn(name = "LOGTY", size = 1, type = JCoMetaData.TYPE_CHAR, ordinal = 2)
    private String codMessage;

    @SAPColumn(name = "MODLO", size = 2, type = JCoMetaData.TYPE_CHAR, ordinal = 3)
    private String modulo;

    @SAPColumn(name = "OPERA", size = 5, type = JCoMetaData.TYPE_CHAR, ordinal = 4)
    private String operacao;

    @SAPColumn(name = "CHAVE", size = 20, type = JCoMetaData.TYPE_CHAR, ordinal = 5)
    private String requestKey;

}
