package br.com.sjc.modelo.enums;


import br.com.sjc.util.StringUtil;
import lombok.AllArgsConstructor;

import java.text.MessageFormat;
import java.util.Arrays;

@AllArgsConstructor
public enum MotivoRecusaEnum {
    ATRIBUIDO_PELO_SISTEMA__INTERNAMENTE("0", "Atribuído pelo sistema (internamente)"),
    DATA_REMESSA_TARDE_DEMAIS("1", "Data remessa tarde demais"),
    MA_QUALIDADE("2", "Má qualidade"),
    CARO_DEMAIS("3", "Caro demais"),
    CONCORRENTE_OFERECE_MELHOR_ATENDIMENTO("4", "Concorrente oferece melhor atendimento"),
    GARANTIA("5", "Garantia"),
    SOLICITACAO_DO_CLIENTE_NAO_JUSTIFICADA("10", "Solicitação do cliente não justificada"),
    CLIENTE_RECEBE_REMESSA_SUBSTIT("11", "Cliente recebe remessa substit."),
    QUESTAO_ESTA_SENDO_RESOLVIDA("50", "Questão está sendo resolvida"),
    BLOQUEIO_DE_CRDITO("Z0", "Bloqueio de crédito"),
    CREDITO("Z1", "Crédito"),
    CONDICAO_DE_PGTO("Z2", "Condição de Pgto"),
    PRECO_DIVERGENTE("Z3", "Preço Divergente"),
    CRDITO_CONDPGTO("Z4", "Crédito / CondPgto"),
    CRDITO_PRECO_DIV("Z5", "Crédito / Preço Div"),
    COND_PGTO_PR_DIV("Z6", "Cond Pgto / Pr Div"),
    CRD_COND_PGTO_PR_DIV("Z7", "Créd / Cond Pgto / Pr Div"),
    REPROVADO_WORKFLOW("Z8", "Reprovado Workflow"),
    EM_ANALISE_COMERCIAL__WORKFLOW("Z9", "Em Analise Comercial (Workflow)"),
    TAB_PRECO_INVALIDA("ZA", "Tab Preço Inválida"),
    CANCELAMENTO_DE_SALDO("ZB", "Cancelamento de Saldo"),
    CANCELAMENTO_DE_SALDO_DE_CONTRATO__DEV("ZC", "Cancelamento de Saldo de Contrato (Dev)"),
    AGUARDANDO_VALIDACAO("ZD", "Aguardando Validação"),
    ERRO_CONTINGÊNCIA_MANUAL_COBRANCA_MANUAL("ZX", "Erro Contingência Manual:Cobrança Manual");

    public String codigo;

    public String descricao;

    public String getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public static String obterDescricaoPorCodigo(final String codigo) {

        if (StringUtil.isEmpty(codigo))
            return null;

        return Arrays.asList(MotivoRecusaEnum.values())
                .stream()
                .filter(motivoRecusaEnum -> motivoRecusaEnum.getCodigo().equalsIgnoreCase(codigo))
                .map(MotivoRecusaEnum::getDescricao)
                .findFirst()
                .orElse(MessageFormat.format("Descrição do motivo de recusa com o código {0} não encontrada.", codigo));
    }
}
