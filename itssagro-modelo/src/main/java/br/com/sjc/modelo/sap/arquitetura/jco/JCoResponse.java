package br.com.sjc.modelo.sap.arquitetura.jco;

/**
 * <p>
 * <b>Title:</b> JCoResponse
 * </p>
 *
 * <p>
 * <b>Description:</b> Classe responsável por definir uma classe como parâmetro de saída de uma função SAP.
 * </p>
 *
 * @author Bruno Zafalão
 * @version 1.0.0
 */
public class JCoResponse extends JCoParameter {

    /**
     * Método responsável por obter o ktokk de parâmetro utilizado.
     *
     * @return <code>JCoParameterType</code>
     * @author Bruno Zafalão
     */
    @Override
    public JCoParameterType type() {

        return JCoParameterType.RESPONSE;
    }
}
