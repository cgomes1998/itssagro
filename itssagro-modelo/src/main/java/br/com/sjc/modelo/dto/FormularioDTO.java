package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.Formulario;
import br.com.sjc.modelo.enums.FormularioStatus;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by julio.bueno on 04/07/2019.
 */
@Getter
@Setter
@NoArgsConstructor
public class FormularioDTO extends DataDTO {

    @ProjectionProperty
    private String descricao;

    @ProjectionProperty
    private FormularioStatus formularioStatus;

    @ProjectionProperty
    private boolean cabecalhoDadosTransportadora;

    @ProjectionProperty
    private boolean cabecalhoDadosMotorista;

    @ProjectionProperty
    private boolean cabecalhoDadosCliente;

    @ProjectionProperty
    private boolean cabecalhoDadosVeiculo;

    public FormularioDTO(Formulario formulario) {

        setId(formulario.getId());
        setDescricao(formulario.getDescricao());
        setFormularioStatus(formulario.getFormularioStatus());
        setCabecalhoDadosTransportadora(formulario.getCabecalhoDadosTransportadora());
        setCabecalhoDadosMotorista(formulario.getCabecalhoDadosMotorista());
        setCabecalhoDadosCliente(formulario.getCabecalhoDadosCliente());
        setCabecalhoDadosVeiculo(formulario.getCabecalhoDadosVeiculo());
    }

}
