package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusIntegracaoEnum;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class DetalheLogDTO implements Serializable, Comparable<DetalheLogDTO> {

    private static final long serialVersionUID = 2837042754276315231L;

    private Long id;

    private StatusIntegracaoEnum status;

    private String etapa;

    private String mensagem;

    public String getStatusDescricao() {
        return this.status != null ? this.status.getDescricao() : "";
    }

    public String get_class() {
        return this.status != null ? this.status.get_class() : "";
    }

    @Override
    public int compareTo(DetalheLogDTO o) {

        try {

            return ((Integer) Integer.parseInt(this.etapa)).compareTo(Integer.parseInt(o.getEtapa()));

        } catch (Exception ex) {

            return this.etapa.compareTo(o.getEtapa());
        }
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + ((mensagem == null) ? 0 : mensagem.hashCode());
        result = prime * result + ((etapa == null) ? 0 : etapa.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DetalheLogDTO other = (DetalheLogDTO) obj;
        if (mensagem == null) {
            if (other.mensagem != null)
                return false;
        } else if (!mensagem.equals(other.mensagem))
            return false;
        if (etapa == null) {
            if (other.etapa != null)
                return false;
        } else if (!etapa.equals(other.etapa))
            return false;
        if (status != other.status)
            return false;
        return true;
    }

}
