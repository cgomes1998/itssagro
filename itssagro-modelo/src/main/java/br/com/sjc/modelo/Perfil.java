package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.AplicacaoEnum;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

@Setter
@Getter
@Entity
@Table(name = "tb_perfil")
public class Perfil extends EntidadeAutenticada {

    private static final long serialVersionUID = 1L;

    @Column(name = "nome")
    private String nome;

    @Column(name = "pesagem_manual", nullable = false, columnDefinition = "boolean default false")
    private boolean pesagemManual;

    @Column(name = "aplicar_indice_restrito", nullable = false, columnDefinition = "boolean default false")
    private boolean aplicarIndiceRestrito;

    @Column(name = "permitir_liberacoes", nullable = false, columnDefinition = "boolean default false")
    private boolean permitirLiberacoes;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection(targetClass = AplicacaoEnum.class)
    @JoinTable(name = "tb_perfil_aplicacao")
    @Column(name = "aplicacao", nullable = false)
    @Enumerated(EnumType.STRING)
    private Collection<AplicacaoEnum> aplicacoes;

    @EntityProperties(value = {"id", "funcionalidade", "visualizar", "alterar", "incluir", "excluir", "consultar", "perfil.id"}, targetEntity = PerfilFuncionalidade.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "perfil", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<PerfilFuncionalidade> funcionalidades;

    @EntityProperties(value = {"id", "operacao.id", "operacao.entidade", "operacao.descricao", "operacao.modulo", "operacao.direcaoOperacao", "operacao.operacao", "operacao.eventoEntrada", "operacao.eventoSaida", "operacao" +
            ".possuiDivisaoCarga", "operacao.possuiPesagem", "operacao.possuiClassificacao", "operacao.pesagemManual", "operacao.aplicaClassificacao", "operacao.simplesPesagem", "operacao.possuiConsultaSaldoDisponivel", "operacao" +
            ".possuiBuscaNFeTransporte", "operacao.possuiClassificacaoOrigem", "operacao.possuiConsultaContrato", "operacao.possuiDadosNfe", "operacao.possuiRetornoNfeSAP", "operacao.possuiRetornoNfeTransporteSAP", "operacao" +
            ".possuiValidacaoEntreVlrUniNotaEPedido", "operacao.toleranciaEntreVlrUniNotaEPedido", "operacao.possuiValidacaoParaEscriturarNota", "operacao.possuiImpressaoPorEtapa", "operacao.possuiValidacaoChaveAcesso", "operacao.possuiRateio", "operacao" +
            ".possuiValidacaoDivergenciaProdutorNotaEPedido", "operacao.possuiValidacaoChaveEFornecedor", "operacao.utilizaObterDadosNfe", "operacao.utilizaObterDadosCte", "operacao.enviarEmailPesagemManual", "operacao.gestaoPatio", "operacao.criarRemessa", "operacao.criarDocumentoTransporte",
            "operacao.possuiValidacaoConversaoLitragem", "operacao.umidadeInformadaManualmente", "operacao.requestCount", "operacao.tempoPreCadastro", "operacao.ultimaSincronizacao", "perfil.id"}, targetEntity = PerfilOperacao.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "perfil", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<PerfilOperacao> operacoes;

    @Transient
    private boolean atribuidoAoUsuario;

}
