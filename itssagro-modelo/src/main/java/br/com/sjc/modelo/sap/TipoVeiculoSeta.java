package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.EntidadeGenerica;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by julio.bueno on 27/06/2019.
 */
@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_tipo_veiculo_seta", schema = "sap")
public class TipoVeiculoSeta extends EntidadeGenerica {

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_tipo_veiculo")
    private TipoVeiculo tipoVeiculo;

    @Column
    private String descricao;

    @Column
    private Integer qtd;

    @Column
    private Boolean padrao;

    public TipoVeiculoSeta() {
        super();
    }

    public TipoVeiculoSeta(
            final Long id,
            final Long idTipoVeiculo,
            final String descricao,
            final Integer qtd,
            final Boolean padrao
    ) {
        this.setId(id);
        this.setTipoVeiculo(new TipoVeiculo(idTipoVeiculo));
        this.setDescricao(descricao);
        this.setQtd(qtd);
        this.setPadrao(padrao);
    }

    public BigDecimal getQtdValida() {

        if (this.qtd == null)
            return BigDecimal.ZERO;
        return new BigDecimal(this.qtd);
    }

}
