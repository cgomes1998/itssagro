package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.util.ObjetoUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Date;

@Data
@EqualsAndHashCode(of = {"numeroRomaneio", "codigoOrdemVenda"})
@NoArgsConstructor
@AllArgsConstructor
public class RelatorioComercial_V2_DTO {

    private StatusRomaneioEnum statusRomaneio;

    private StatusIntegracaoSAPEnum statusIntegracaoSAP;

    private String codigoOrdemVenda;

    private String numeroPedido;

    private String codigoMaterial;

    private String descricaoMaterial;

    private String codigoCliente;

    private String descricaoCliente;

    private String codigoTransportadora;

    private String descricaoTransportadora;

    private String numeroRomaneio;

    private String placaUm;

    private String codigoMotorista;

    private String descricaoMotorista;

    private Date dataRomaneio;

    private String nfe;

    private String numeroRemessa;

    private String numeroOrdemTransporte;

    private BigInteger documentoFaturamento;

    private BigDecimal qtdKg;

    private BigDecimal rateio;

    private BigInteger idItem;

    private String unidadeMedida;

    public BigDecimal getQtdKgLValido() {

        if (ObjetoUtil.isNull(this.getQtdKg()) || this.getQtdKg().doubleValue() <= 0)
            return BigDecimal.ZERO;

        return this.getQtdKg().setScale(0, RoundingMode.HALF_UP);
    }

    public BigDecimal getRateioValido() {

        if (ObjetoUtil.isNull(this.getRateio()) || this.getRateio().doubleValue() <= 0)
            return BigDecimal.ZERO;

        final boolean tonelada = Arrays.asList(new String[]{"TO"}).contains(this.getUnidadeMedida());

        return tonelada ? this.getRateio().divide(new BigDecimal(1000), 2, RoundingMode.HALF_UP) : this.getRateio().setScale(0, RoundingMode.HALF_UP);
    }

}
