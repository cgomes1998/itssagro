package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CriterioTipoEnum {

    SUCESSO("Sucesso"),
    FALHA("Falha");

    private String descricao;

}
