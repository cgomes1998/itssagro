package br.com.sjc.modelo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_materia_prima")
public class MateriaPrima extends EntidadeAutenticada {

    @Column
    private String codigo;

    @Column
    private String descricao;


}
