package br.com.sjc.modelo.dto;

import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AmbienteDTO extends DataDTO {

    @ProjectionProperty
    private String descricao;

    @ProjectionProperty
    private String host;

    @ProjectionProperty
    private String rfcPorta;

    @ProjectionProperty
    private String mandante;

    @ProjectionProperty
    private String numeroInstancia;

}
