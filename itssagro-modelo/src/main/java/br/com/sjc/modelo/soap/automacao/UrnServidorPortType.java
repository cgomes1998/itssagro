/**
 * UrnServidorPortType.java
 * <p>
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.sjc.modelo.soap.automacao;

public interface UrnServidorPortType extends java.rmi.Remote {

    /**
     * Apenas um exemplo utilizando modelo de ws.
     */
    String exemplo(String nome, int idade) throws java.rmi.RemoteException;

    /**
     * <br>Retorna o status e os dados do veiculo na balanca consultada,
     * caso exista, em uma string.<br>Retorna balanca,numero do tag, placa
     * veiculo, peso 1, peso 2 e status.
     */
    String consulta_automatiza(int bal) throws java.rmi.RemoteException;

    /**
     * <br>Altera o status de pesagem de um veiculo usando como referencia
     * o numero do TAG.
     * 		<br>Os campos de entrada sao :
     * 		<br>tag - numero da tag do veiculo em pesagem (integer);
     * 		<br>status - o status a ser atribuido a essa pesagem (string);
     * 		<br>msg1 - mensagem de 12 caracteres a ser mostrada no display (string);
     * 		<br>msg2 - mensagem de 12 caracteres a ser mostrada no display (string);
     * 		<br>msg3 - mensagem de 12 caracteres a ser mostrada no display (string);
     * 		<br>peso_ordem - peso final da ordem de carga, deve ser enviado
     * no momento da troca de status para a liberacao do veiculo na segunda
     * pesagem, antes da emissao do ticket (integer);
     * 		<br>obs - observacao a ser impressa no ticket (string);
     * 		<br><br>Retorna numero do tag e o novo status.
     */
    String trocastatus_automatiza(int tag, int status, String msg1, String msg2, String msg3, int peso_ordem, String obs) throws java.rmi.RemoteException;

    /**
     * <br>Faz o pre-cadastro de um veiculo usando como referencia
     * o numero da PLACA e a BALANCA.
     * 		<br>Os campos de entrada sao :
     * 		<br>tag - numero do TAG entregue ao motorista (integer);
     * 		<br>placa_cavalo - placa do cavalo do veiculo autorizado a entrar
     * na balanca (string);
     * 		<br>placa_carreta - placa da carreta do veiculo autorizado a entrar
     * na balanca (string);
     * 		<br>transportador - nome do transportador (string);
     * 		<br>motorista - nome do motorista do veiculo (string);
     * 		<br>razaosocial - codigo e razao social do remetente ou destinatario
     * (string);
     * 		<br>endereco - endereco do remetente ou destinatario (string);	
     *
     * 		<br>municipio - municipio do remetente ou destinatario (string);
     * 		<br>produto - codigo e nome do produto transportado (string);		
     * 		<br>data_ordem - data de emissao da ordem de carga (string);		
     * 		<br>nr_ordem - numero da ordem de carga (string);		
     * 		<br>Retorna numero da PLACA e o resultado (1 se OK, 0 se ocorreu
     * algum erro).
     */
    String precad_automatiza(int tag, String placa_cavalo, String placa_carreta, String transportador, String motorista, String razaosocial, String endereco, String municipio, String produto, String data_ordem, String nr_ordem) throws java.rmi.RemoteException;

}
