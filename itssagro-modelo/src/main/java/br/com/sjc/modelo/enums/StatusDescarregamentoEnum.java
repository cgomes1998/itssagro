package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusDescarregamentoEnum {

    AGUARDANDO("Aguardando"),
    EM_DESCARGA("Em Descarga"),
    DESCARREGADO("Descarregado"),
    CANCELADO("Cancelado"),
    PENDENTE("Pendente");

    private String descricao;

}
