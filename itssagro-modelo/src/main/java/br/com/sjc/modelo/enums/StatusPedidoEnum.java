package br.com.sjc.modelo.enums;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum StatusPedidoEnum {

    EM_APROVACAO(1, "Em Aprovação"),

    APROVADO(2, "Aprovado");

    private Integer codigo;

    private String descricao;

    StatusPedidoEnum(final Integer codigo, final String descricao) {

        this.codigo = codigo;

        this.descricao = descricao;
    }

    public static StatusPedidoEnum obterStatusPorCodigo(final Integer codigo) {
        return Arrays.asList(StatusPedidoEnum.values()).stream().filter(i -> i.getCodigo().equals(codigo)).findFirst().orElse(null);
    }
}
