package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.CondicaoEnum;
import br.com.sjc.modelo.enums.EtapaCampoEnum;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.enums.OperadorEnum;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_etapa_criterio_condicao")
public class EtapaCriterioCondicao extends EntidadeAutenticada {

    @EntityProperties(value = {"id"})
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_etapa_criterio", nullable = false)
    @JsonIgnore
    private EtapaCriterio etapaCriterio;

    @Column(name = "numero_condicao")
    private Integer numeroCondicao;

    @Enumerated(EnumType.STRING)
    private EtapaEnum etapa;

    @Enumerated(EnumType.STRING)
    private EtapaCampoEnum campo;

    @Enumerated(EnumType.STRING)
    private CondicaoEnum condicao;

    private String valor;

    @Enumerated(EnumType.STRING)
    @Column(name = "operador_entrada")
    private OperadorEnum operadorEntrada;

    @Enumerated(EnumType.STRING)
    @Column(name = "operador_saida")
    private OperadorEnum operadorSaida;

}
