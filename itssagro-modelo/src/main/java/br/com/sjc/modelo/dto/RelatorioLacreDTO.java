package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.Lacre;
import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusLacreEnum;
import br.com.sjc.modelo.enums.StatusLacreRomaneioEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class RelatorioLacreDTO {

    private Lacre lacre;

    private Romaneio romaneio;

    private StatusLacreRomaneioEnum statusLacreRomaneio;

    private RetornoNFe retornoNFe;

    private String statusLacreApresentado;

    private String responsavel;

    public RelatorioLacreDTO() {

    }

    public RelatorioLacreDTO(
            String codigoLacre,
            StatusLacreEnum statusLacre
    ) {
        this.setStatusLacreRomaneio(statusLacreRomaneio);
        this.setLacre(new Lacre());
        this.getLacre().setCodigo(codigoLacre);
        this.getLacre().setStatusLacre(statusLacre);
    }

    public RelatorioLacreDTO(
            String numeroRomaneio,
            StatusRomaneioEnum statusRomaneio,
            StatusIntegracaoSAPEnum statusIntegracaoSAP,
            String placaCavalo,
            String tipoVeiculo,
            String codigoLacre,
            StatusLacreEnum statusLacre,
            StatusLacreRomaneioEnum statusLacreRomaneio,
            Date dataUtilizacao,
            String nfe,
            Date nfeDataCadastro,
            String clienteCodigo,
            String clienteNome,
            String usuarioNomeQueUtilizou,
            String usuarioNomeQueInutilizou
    ) {
        this.setLacre(new Lacre());
        this.setRomaneio(new Romaneio());
        if (StringUtil.isNotNullEmpty(clienteCodigo) && StringUtil.isNotNullEmpty(clienteNome)) {
            this.getRomaneio().setCliente(new Cliente());
            this.getRomaneio().getCliente().setCodigo(clienteCodigo);
            this.getRomaneio().getCliente().setNome(clienteNome);
        }
        if (StringUtil.isNotNullEmpty(nfe)) {
            this.setRetornoNFe(new RetornoNFe());
            this.getRetornoNFe().setNfe(nfe);
            this.getRetornoNFe().setDataCadastro(nfeDataCadastro);
        }
        this.getRomaneio().setPlacaCavalo(new Veiculo());
        this.getRomaneio().getPlacaCavalo().setTipo(new TipoVeiculo());
        this.getRomaneio().setNumeroRomaneio(numeroRomaneio);
        this.getRomaneio().setStatusRomaneio(statusRomaneio);
        this.getRomaneio().setStatusIntegracaoSAP(statusIntegracaoSAP);
        this.getRomaneio().getPlacaCavalo().setPlaca1(StringUtil.isNotNullEmpty(placaCavalo) ? StringUtil.toPlaca(placaCavalo) : "-");
        this.getRomaneio().getPlacaCavalo().getTipo().setDescricao(StringUtil.isNotNullEmpty(tipoVeiculo) ? tipoVeiculo : "-");
        this.setStatusLacreRomaneio(statusLacreRomaneio);
        this.getLacre().setCodigo(codigoLacre);
        this.getLacre().setStatusLacre(statusLacre);
        this.getLacre().setDataUtilizacao(dataUtilizacao);
        if (StringUtil.isNotNullEmpty(usuarioNomeQueInutilizou)) {
            this.setResponsavel(usuarioNomeQueInutilizou);
        } else {
            this.setResponsavel(usuarioNomeQueUtilizou);
        }
    }

    public String getStatusLacreApresentado() {
        String status = "-";
        if (ObjetoUtil.isNotNull(statusLacreRomaneio) && StatusLacreRomaneioEnum.INUTILIZADO.equals(statusLacreRomaneio)) {
            status = statusLacreRomaneio.getDescricao();
        } else if (ObjetoUtil.isNotNull(romaneio) && StatusRomaneioEnum.ESTORNADO.equals(romaneio.getStatusRomaneio())) {
            status = StatusLacreEnum.DISPONIVEL.getDescricao();
        } else {
            status = this.getLacre().getStatusLacre().getDescricao();
        }
        return status;
    }
}
