package br.com.sjc.modelo.sap.request.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SAPParameter( function = "BAPISHIPMENTHEADERACTION" )
public class BapiShipmentHeaderActionHeader extends JCoItem {

    @SAPColumn( name = "SHIPMENT_NUM" )
    private String numeroDocTransporte;

    @SAPColumn( name = "STATUS_PLAN" )
    private String stPlan;

    @SAPColumn( name = "STATUS_CHECKIN" )
    private String stCheckin;

    @SAPColumn( name = "STATUS_LOAD_START" )
    private String stLoadStart;

    @SAPColumn( name = "STATUS_LOAD_END" )
    private String stLoadEnd;

    @SAPColumn( name = "STATUS_COMPL" )
    private String stCompl;

    @SAPColumn( name = "STATUS_SHPMNT_START" )
    private String stShpmntStart;

}
