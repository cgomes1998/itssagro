package br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Endereco {

    private String cep;

    private String rua;

    private int numero;

    private Object complemento;

    private String bairro;

    private String municipio;

    private String uf;

}
