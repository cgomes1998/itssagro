package br.com.sjc.modelo.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RelatorioLacreFiltroDTO {

    private String status;

    private Date dataUtilizacaoInicial;

    private Date dataUtilizacaoFinal;

    @JsonIgnore
    private List<RelatorioLacreDTO> lacres;
}
