package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.ObjectID;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TabelaClassificacaoHeader implements ObjectID<String> {

    private String itemClassificacao;

    private String codigoMaterial;

    private String descricao;

    public TabelaClassificacaoHeader() {

    }

    public TabelaClassificacaoHeader(String itemClassificacao, String codigoMaterial, String descricao) {

        this.itemClassificacao = itemClassificacao;
        this.codigoMaterial = codigoMaterial;
        this.descricao = descricao;
    }

    @Override
    public String getId() {

        return itemClassificacao;
    }

    @Override
    public void setId(String aLong) {

        itemClassificacao = aLong;
    }

}
