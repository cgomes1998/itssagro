package br.com.sjc.modelo.sap.request.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SAPParameter( function = "/BEV1/RPTDBES_I" )
public class Bev1RptdbesI extends JCoItem {

    @SAPColumn( name = "/BEV1/RPFAR1" )
    private String codigoMotorista;

    @SAPColumn( name = "/BEV1/RPMOWA" )
    private String placaCavalo;

    @SAPColumn( name = "/BEV1/RPANHAE" )
    private String placaCarretaUm;

}
