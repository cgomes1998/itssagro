package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.response.item.TableFlowOut;
import br.com.sjc.modelo.sap.response.item.TableLogReturn;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@SAPParameter(function = "ZITSSAGRO_FM_LOGS")
public class RfcEtapasRomaneioResponse extends JCoResponse {

    @SAPTable(name = "LOGRETURN", item = TableLogReturn.class)
    private List<TableLogReturn> logReturns;

    @SAPTable(name = "FLOWOUT", item = TableFlowOut.class)
    private List<TableFlowOut> flowOuts;

}
