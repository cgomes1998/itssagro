package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPStructure;
import br.com.sjc.modelo.sap.response.item.RfcDetalhesEstoqueItemResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.Collections;
import java.util.List;

@Getter
@Setter
@SAPParameter(function = "BAPI_MATERIAL_STOCK_REQ_LIST")
public class RfcSaldoEstoqueProdutoAcabadoResponse extends JCoResponse {

    @SAPStructure(name = "MRP_STOCK_DETAIL", item = RfcDetalhesEstoqueItemResponse.class)
    private RfcDetalhesEstoqueItemResponse detalhesDoEstoque;
}
