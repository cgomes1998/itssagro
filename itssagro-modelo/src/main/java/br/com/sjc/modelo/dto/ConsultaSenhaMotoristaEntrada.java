package br.com.sjc.modelo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConsultaSenhaMotoristaEntrada {

    private String filtro;

}
