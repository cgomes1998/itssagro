package br.com.sjc.modelo.util;

import br.com.sjc.modelo.Classificacao;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.sap.ConversaoLitragemItem;
import br.com.sjc.modelo.sap.ItemNFPedido;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.util.BigDecimalUtil;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class RomaneioUtil {

    public static boolean isOperacaoSemClassificacao(final DadosSincronizacao dadosSincronizacao, final Material material) {

        if (dadosSincronizacao.isGestaoPatio() && ObjetoUtil.isNotNull(material)) {
            return !material.isRealizarAnaliseQualidade();
        }
        return !dadosSincronizacao.isPossuiClassificacao();
    }

    public static boolean isOperacaoSemPesagemEClassificacao(final DadosSincronizacao dadosSincronizacao) {

        return !dadosSincronizacao.isPossuiPesagem() && !dadosSincronizacao.isPossuiClassificacao();
    }

    public static boolean isOperacaoSimplesPesagem(final DadosSincronizacao dadosSincronizacao) {

        return dadosSincronizacao.isSimplesPesagem();
    }

    public static boolean isOperacaoSemClassificacao(final DadosSincronizacao dadosSincronizacao) {

        return !dadosSincronizacao.isPossuiClassificacao();
    }

    public static boolean isPossuiPesagem(final Romaneio entidade) {

        return entidade.getOperacao().isPossuiPesagem();
    }

    public static boolean isPossuiClassificacao(final Romaneio entidade) {

        if (entidade.getOperacao().isGestaoPatio()) {
            final Material material = entidade.getItens().stream().map(ItemNFPedido::getMaterial).findFirst().orElse(entidade.getMaterial());
            return entidade.getOperacao().isPossuiClassificacao() && material.isRealizarAnaliseQualidade();
        }
        return entidade.getOperacao().isPossuiClassificacao();
    }

    public static boolean isConversaoRealizada(final Romaneio romaneio) {

        final Optional<Material> material = romaneio.getItens().stream().findFirst().map(ItemNFPedido::getMaterial);
        if (!material.isPresent()) {
            return false;
        }
        if (!material.get().isRealizarConversaoLitragem()) {
            return true;
        }

        return material.get().isRealizarConversaoLitragem() && ObjetoUtil.isNotNull(romaneio.getConversaoLitragem()) && ColecaoUtil.isNotEmpty(romaneio.getConversaoLitragem().getConversoes());
    }

    public static boolean isRomaneioFoiPesado(final Romaneio romaneio) {

        return ObjetoUtil.isNotNull(romaneio.getPesagem()) ? BigDecimalUtil.isMaiorZero(romaneio.getPesagem().getPesoInicial()) && BigDecimalUtil.isMaiorZero(romaneio.getPesagem().getPesoFinal()) : false;
    }

    public static boolean isRomaneioFoiClassificado(final Romaneio romaneio) {

        if (romaneio.getOperacao().isGestaoPatio()) {
            if (ColecaoUtil.isNotEmpty(romaneio.getAnalises())) {
                List<AtomicBoolean> todosResultadosInformados = new ArrayList<>();
                romaneio.getAnalises().stream().forEach(a -> {
                    todosResultadosInformados.add(new AtomicBoolean(a.getItens().stream().filter(i -> StringUtil.isEmpty(i.getResultado())).count() <= 0));
                });
                return todosResultadosInformados.stream().filter(t -> !t.get()).count() <= 0;
            }
            return false;
        }
        return ObjetoUtil.isNotNull(romaneio.getClassificacao()) ? ObjetoUtil.isNotNull(romaneio.getClassificacao().getId()) : false;
    }

    public static boolean isClassificacaoPossuiIndices(final Classificacao classificacao) {

        return ColecaoUtil.isNotEmpty(classificacao.getItens()) && classificacao.getItens().stream().filter(c -> ObjetoUtil.isNotNull(c.getIndice())).count() > 0;
    }

    public static HashMap<String, AtomicLong> converterPesoLiquidoParaUnidadeMedidaDaOrdem(Romaneio entidade) {

        final Material material = entidade.getItens().stream().map(ItemNFPedido::getMaterial).filter(Objects::nonNull).findFirst().get();

        final boolean temConversaoDeLitragem = material.isRealizarConversaoLitragem();

        AtomicLong valorASerComparado = new AtomicLong(0);

        HashMap<String, AtomicLong> stringAtomicLongHashMap = new HashMap<>();

        if (temConversaoDeLitragem) {

            if (RomaneioUtil.isConversaoRealizada(entidade)) {

                valorASerComparado.addAndGet(entidade.getConversaoLitragem().getConversoes().stream().sorted((i1, i2) -> Long.compare(i2.getDataCadastro().getTime(), i1.getDataCadastro().getTime())).map(ConversaoLitragemItem::getVolumeSetaVinteGraus).map(i -> i.longValue()).findFirst().orElse(Long.valueOf(0)));

                stringAtomicLongHashMap.put("L", valorASerComparado);
            }

        } else {

            BigDecimal pesoLiquido = entidade.getPesagem().getPesoInicial().subtract(entidade.getPesagem().getPesoFinal()).setScale(0, RoundingMode.HALF_UP);

            valorASerComparado.addAndGet(pesoLiquido.longValue());

            stringAtomicLongHashMap.put("KG", valorASerComparado);
        }

        return stringAtomicLongHashMap;
    }
}
