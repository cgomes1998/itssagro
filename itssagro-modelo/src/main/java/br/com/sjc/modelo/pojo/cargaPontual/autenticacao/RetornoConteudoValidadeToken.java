package br.com.sjc.modelo.pojo.cargaPontual.autenticacao;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RetornoConteudoValidadeToken {

    private int status;

    private String statusDescricao;

    private String mensagem;

    private TokenValidade conteudo;

}
