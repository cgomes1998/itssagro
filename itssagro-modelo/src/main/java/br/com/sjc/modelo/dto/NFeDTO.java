package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.sap.ItemNFPedido;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class NFeDTO {

    private String remessaNfe;

    private String chaveAcessoNfe;

    private String numeroNfe;

    private String serieNfe;

    private String numeroAleatorioChaveAcesso;

    private String numeroLog;

    private String digitoVerificadorNfe;

    private Date dataNfe;

    private BigDecimal valorUnitarioNfe;

    private BigDecimal valorTotalNfe;

    private BigDecimal pesoTotalNfe;

    public NFeDTO(final ItemNFPedido entidade) {

        this.remessaNfe = entidade.getRemessaNfe();

        this.chaveAcessoNfe = entidade.getChaveAcessoNfe();

        this.numeroNfe = entidade.getNumeroNfe();

        this.serieNfe = entidade.getSerieNfe();

        this.numeroAleatorioChaveAcesso = entidade.getNumeroAleatorioChaveAcesso();

        this.numeroLog = entidade.getNumeroLog();

        this.digitoVerificadorNfe = entidade.getDigitoVerificadorNfe();

        this.dataNfe = entidade.getDataNfe();

        this.valorUnitarioNfe = entidade.getValorUnitarioNfe();

        this.valorTotalNfe = entidade.getValorTotalNfe();

        this.pesoTotalNfe = entidade.getPesoTotalNfe();
    }
}
