package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusPedidoEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.anotation.ProjectionConfigurationDTO;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ProjectionConfigurationDTO
public class RomaneioListagemDTO extends DataDTO {

    @ProjectionProperty
    private Date dataCadastro;

    @ProjectionProperty
    private String numeroRomaneio;

    @ProjectionProperty
    private String numeroPedido;

    @ProjectionProperty
    private StatusPedidoEnum statusPedido;

    @ProjectionProperty(values = {"id", "operacao", "entidade"})
    private DadosSincronizacao operacao;

    @ProjectionProperty(values = {"id", "placa1"})
    private Veiculo placaCavalo;

    @ProjectionProperty(values = {"id", "codigo", "nome", "cpf", "cnpj"})
    private Produtor produtor;

    @ProjectionProperty(values = {"id", "codigo", "descricao", "cnpj", "cpf"})
    private Transportadora transportadora;

    @ProjectionProperty(values = {"id", "codigo", "nome", "cpf"})
    private Motorista motorista;

    @ProjectionProperty(values = {"id", "codigo", "nome", "cpfCnpj"})
    private Cliente cliente;

    @ProjectionProperty(values = {"id", "codigo", "descricao"})
    private Material material;

    @ProjectionProperty
    private StatusIntegracaoSAPEnum statusIntegracaoSAP;

    @ProjectionProperty
    private StatusRomaneioEnum statusRomaneio;

    private List<ItemNFPedidoListagemDTO> itens = new ArrayList<>();

    private List<RetornoNFEListagemDTO> retornoNfes = new ArrayList<>();

    public Boolean getPossuiItens() {

        return ColecaoUtil.isNotEmpty(itens);
    }

    public Boolean getMaisDeUmaNota() {

        if (ColecaoUtil.isNotEmpty(this.retornoNfes)) {

            return this.retornoNfes.size() > 1;
        }

        return Boolean.FALSE;
    }

    public Boolean getPossuiMaisDeUmProdutor() {

        if (getPossuiItens()) {

            Set<Produtor> produtores = itens.stream().map(ItemNFPedidoListagemDTO::getProdutor).collect(Collectors.toSet());

            return produtores.size() > 1;
        }

        return Boolean.FALSE;
    }

    public RetornoNFEListagemDTO getRetornoNFe() {

        if (ColecaoUtil.isEmpty(this.retornoNfes)) {

            return null;
        }

        return this.retornoNfes.stream().findFirst().orElse(null);
    }

}
