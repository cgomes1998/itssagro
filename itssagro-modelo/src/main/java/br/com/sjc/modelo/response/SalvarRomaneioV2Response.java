package br.com.sjc.modelo.response;

import br.com.sjc.modelo.sap.Romaneio;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class SalvarRomaneioV2Response implements Serializable {
    private static final long serialVersionUID = 1L;
    private Romaneio entidade;
    private boolean habilitarLacragem;
    private boolean habilitarConversao;
    private boolean habilitarPesagem;
    private boolean habilitarClassificacao;
    private boolean enviadoAoSAP;
    private byte[] pdfBytes;
    private String tipoFluxo;
    private String sucesso;
}
