package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.StatusChecklistEnum;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_senha_formulario", schema = "public")
public class SenhaFormulario extends EntidadeAutenticada {

    @Column(name = "status_checklist")
    @Enumerated(EnumType.STRING)
    private StatusChecklistEnum statusChecklist;

    @EntityProperties(value = {"id"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_senha", nullable = false)
    @JsonBackReference
    private Senha senha;

    @EntityProperties(value = {"id", "descricao", "formularioStatus", "cabecalhoDadosMotorista", "cabecalhoDadosVeiculo", "cabecalhoDadosCliente", "cabecalhoDadosTransportadora"}, collecions = {"itens"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_formulario", nullable = false)
    private Formulario formulario;

    @EntityProperties(value = {"id", "senhaFormulario.id", "item.id", "item.formulario.id", "item.titulo", "item.required", "item.ordem", "item.tamanho", "item.valorEsperado", "item.tipo"}, collecions = {"opcoes"}, targetEntity =
            SenhaFormularioResposta.class)
    @OrderBy("id desc")
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "senhaFormulario", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    private List<SenhaFormularioResposta> respostas = new ArrayList<>();

    @EntityProperties(value = {"id", "descricao", "tipo", "conferenciaPeso", "buscarDadosRomaneio"}, targetEntity = Motivo.class)
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "tb_senha_formulario_motivo", schema = "public", joinColumns = @JoinColumn(name = "id_senha_formulario"), inverseJoinColumns = @JoinColumn(name = "id_motivo"))
    private List<Motivo> motivos = new ArrayList<>();

}
