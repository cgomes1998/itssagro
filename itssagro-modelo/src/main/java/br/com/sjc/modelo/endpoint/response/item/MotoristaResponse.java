package br.com.sjc.modelo.endpoint.response.item;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MotoristaResponse {

    private String codigo;

    private String nome;
}