package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.dto.TabelaClassificacaoItemDTO;
import br.com.sjc.util.ColecaoUtil;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_tabela_classificacao", schema = "sap")
public class TabelaClassificacao extends EntidadeSAPSync {

    private static final long serialVersionUID = -3351987708785118658L;

    @Formula(value = "concat(cod_material,':',item_classificacao,':',desc_item_classificacao)")
    private String chave;

    @Column(name = "cod_leitor")
    private String codigoLeitor;

    @Column(name = "host_leitor")
    private String hostLeitor;

    @Column(name = "porta_leitor")
    private String portaLeitor;

    @Column(name = "cod_material")
    private String codigoMaterial;

    @Column(name = "cod_armazem_terceiros")
    private String codigoArmazemTerceiros;

    @Column(name = "item_classificacao")
    private String itemClassificacao;

    @Column(name = "indice")
    private String indice;

    @Column(name = "percentual_desconto")
    private String percentualDesconto;

    @Column(name = "desc_item_classificacao")
    private String descricaoItemClassificacao;

    @Column(name = "indice_numerico")
    private Double indiceNumerico;

    @Column(name = "indice_permitido_por_perfil", nullable = false, columnDefinition = "boolean default false")
    private boolean indicePermitidoPorPerfil;

    @ElementCollection
    @Column(name = "descricao_item")
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(name = "tb_itens_calculo_indice", schema = "sap", joinColumns = @JoinColumn(name = "id_tabela_classificacao"))
    private Set<String> itensParaCalculoIndice;

    @Transient
    private List<TabelaClassificacaoItemDTO> itens;

    public TabelaClassificacao() {

        super();
    }

    public TabelaClassificacao(String codigoMaterial, String codigoArmazemTerceiros, String itemClassificacao, String indice, String percentualDesconto, String descricaoItemClassificacao, Double indiceNumerico,
                               boolean indicePermitidoPorPerfil, Set<String> itensParaCalculoIndice) {

        this.codigoMaterial = codigoMaterial;
        this.codigoArmazemTerceiros = codigoArmazemTerceiros;
        this.itemClassificacao = itemClassificacao;
        this.indice = indice;
        this.percentualDesconto = percentualDesconto;
        this.descricaoItemClassificacao = descricaoItemClassificacao;
        this.indiceNumerico = indiceNumerico;
        this.indicePermitidoPorPerfil = indicePermitidoPorPerfil;
        this.itensParaCalculoIndice = itensParaCalculoIndice;
    }

    public TabelaClassificacao(Long id, String codigo, String codigoLeitor, String hostLeitor, String portaLeitor, String codigoMaterial, String codigoArmazemTerceiros, String itemClassificacao, String indice, String percentualDesconto,
                               String descricaoItemClassificacao, Double indiceNumerico, boolean indicePermitidoPorPerfil) {

        this.setId(id);
        this.setCodigo(codigo);
        this.codigoLeitor = codigoLeitor;
        this.hostLeitor = hostLeitor;
        this.portaLeitor = portaLeitor;
        this.codigoMaterial = codigoMaterial;
        this.codigoArmazemTerceiros = codigoArmazemTerceiros;
        this.itemClassificacao = itemClassificacao;
        this.indice = indice;
        this.percentualDesconto = percentualDesconto;
        this.descricaoItemClassificacao = descricaoItemClassificacao;
        this.indiceNumerico = indiceNumerico;
        this.indicePermitidoPorPerfil = indicePermitidoPorPerfil;
    }

    public ItensClassificacao toItensClassificacao() {

        ItensClassificacao entidade = new ItensClassificacao();

        entidade.setId(this.getId());

        entidade.setCodigoTabela(this.getCodigo());

        entidade.setItemClassificao(this.itemClassificacao);

        entidade.setDescricaoItem(this.descricaoItemClassificacao);

        entidade.setCodigoMaterial(this.codigoMaterial);

        entidade.setCodigoLeitor(this.codigoLeitor);

        entidade.setHostLeitor(this.hostLeitor);

        entidade.setPortaLeitor(this.portaLeitor);

        return entidade;
    }

    public void setItensParaCalculoList(List<String> itens) {

        if (ColecaoUtil.isNotEmpty(itens))
            this.setItensParaCalculoIndice(itens.stream().collect(Collectors.toSet()));
    }

}
