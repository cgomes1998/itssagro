package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LoteAnaliseQualidadeDTO extends DataDTO {

    @ProjectionProperty
    private String codigo;

    @ProjectionProperty
    private StatusEnum status;

    @ProjectionProperty
    private Date dataCadastro;

    @ProjectionProperty
    private Date dataFabricacao;

    @ProjectionProperty
    private Date dataVencimento;

    @ProjectionProperty
    private Material material;

    private List<Romaneio> romaneios = new ArrayList<>();

}
