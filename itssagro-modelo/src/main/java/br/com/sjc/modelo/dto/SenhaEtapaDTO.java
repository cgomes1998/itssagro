package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.SenhaEtapa;
import br.com.sjc.util.ObjetoUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class SenhaEtapaDTO {

    private FluxoEtapaDTO etapa;

    private Integer qtdRepeticao = 0;

    private Integer qtdChamada = 0;

    private Date horaEntrada;

    private Date horaSaida;

    private Long duracao;

    private String statusEtapa;

    public SenhaEtapaDTO(SenhaEtapa senhaEtapa) {
        setEtapa(new FluxoEtapaDTO(senhaEtapa.getEtapa()));
        setQtdRepeticao(senhaEtapa.getQtdRepeticao());
        setQtdChamada(senhaEtapa.getQtdChamada());
        setHoraEntrada(senhaEtapa.getHoraEntrada());
        setHoraSaida(senhaEtapa.getHoraSaida());
        setStatusEtapa(senhaEtapa.getStatusEtapa());
        setDuracao((ObjetoUtil.isNotNull(getHoraSaida()) ? getHoraSaida().getTime() : new Date().getTime()) - this.horaEntrada.getTime());
    }

    public String getDataEntradaFormatada(){
        return this.getHoraEntrada() != null ? new SimpleDateFormat("dd/MM/yyyy").format(this.getHoraEntrada()) : "";
    }

    public String getHoraEntradaFormatada(){
        return this.getHoraEntrada() != null ? new SimpleDateFormat("HH:mm:ss").format(this.getHoraEntrada()) : "";
    }

    public String getDataSaidaFormatada(){
        return this.getHoraSaida() != null ? new SimpleDateFormat("dd/MM/yyyy").format(this.getHoraSaida()) : "";
    }

    public String getHoraSaidaFormatada(){
        return this.getHoraSaida() != null ? new SimpleDateFormat("HH:mm:ss").format(this.getHoraSaida()) : "";
    }


}
