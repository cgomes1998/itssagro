package br.com.sjc.modelo.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class DivisaoCargaDTO {

    private Date dataNfe;

    private String numeroPedido;

    private String itemPedido;

    private String nfe;

    private String serieNfe;

    private String valorUnitarioNfe;

    private String saldoPedido;

    private String toleranciaPedido;
}