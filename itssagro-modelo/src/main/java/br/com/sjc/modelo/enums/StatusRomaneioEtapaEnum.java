package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusRomaneioEtapaEnum {

    AGUARDANDO("Aguardando"),
    EM_CRIACAO("Em Criação"),
    CRIADO("Criado"),
    CANCELADO("Cancelado"),
    PENDENTE("Pendente");

    private String descricao;

}
