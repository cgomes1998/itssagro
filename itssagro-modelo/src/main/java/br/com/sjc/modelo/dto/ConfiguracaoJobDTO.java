package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.cfg.ConfiguracaoJob;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ConfiguracaoJobDTO extends DataDTO {

    @ProjectionProperty
    private String nome;

    @ProjectionProperty
    private String descricao;

    @ProjectionProperty
    private boolean ativo;

    @ProjectionProperty
    private String schedule;

    @ProjectionProperty
    private DadosSincronizacaoEnum dadosSincronizacao;

    @ProjectionProperty
    private StatusEnum status;

    public ConfiguracaoJobDTO(final ConfiguracaoJob entidade) {

        setId(entidade.getId());

        setNome(entidade.getNome());

        setDescricao(entidade.getObservacao());

        setAtivo(entidade.isAtivo());

        setDadosSincronizacao(entidade.getDadosSincronizacao());

        setSchedule(entidade.getSchedule());

        setStatus(entidade.isAtivo() ? StatusEnum.ATIVO : StatusEnum.INATIVO);
    }

    public String getDadosSincronizacaoDescricao() {

        return this.dadosSincronizacao.getDescricao();
    }

    public String getOperacao() {

        return this.dadosSincronizacao.getOperacao();
    }

}
