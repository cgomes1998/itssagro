package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.enums.DirecaoOperacaoDepositoEnum;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.util.ObjetoUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RelatorioDepositoFiltroDTO {

    private DadosSincronizacao operacao;

    private Produtor depositante;

    private Produtor depositoEntrada;

    private Produtor depositoSaida;

    private Material material;

    private Centro centro;

    private Deposito deposito;

    private Safra safra;

    private DirecaoOperacaoDepositoEnum direcaoOperacaoDeposito;

    private Date dataInicioRomaneio;

    private Date dataFimRomaneio;

    private Date dataPedidoDe;

    private Date dataVencimentoDe;

    private Date dataNfeDe;

    private Date dataPedidoAte;

    private Date dataVencimentoAte;

    private Date dataNfeAte;

    private int paginacaoPageSize;

    private int paginacaoOffset;

    private int paginacaoPageNumber;

    public boolean isNotNull() {

        return ObjetoUtil.isNotNull(this.getOperacao())
                || ObjetoUtil.isNotNull(this.getDepositante())
                || ObjetoUtil.isNotNull(this.getDepositoEntrada())
                || ObjetoUtil.isNotNull(this.getDepositoSaida())
                || ObjetoUtil.isNotNull(this.getMaterial())
                || ObjetoUtil.isNotNull(this.getDirecaoOperacaoDeposito())
                || ObjetoUtil.isNotNull(this.getDataInicioRomaneio())
                || ObjetoUtil.isNotNull(this.getDataFimRomaneio());
    }

}
