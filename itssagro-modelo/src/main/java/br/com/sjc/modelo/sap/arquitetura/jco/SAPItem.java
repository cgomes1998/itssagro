package br.com.sjc.modelo.sap.arquitetura.jco;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * <b>Title:</b> SAPItem
 * </p>
 * 
 * <p>
 * <b>Description:</b> Annotation responsável por definir um parâmetro utilizado em determinada tabela SAP.
 * </p>
 * 
 * @author Bruno Zafalão
 * 
 * @version 1.0.0
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SAPItem {

	/**
	 * Propriedade que identifica o nome do item utilizado na tabela SAP.
	 * 
	 * @author Bruno Zafalão
	 */
	String name() default "";
}
