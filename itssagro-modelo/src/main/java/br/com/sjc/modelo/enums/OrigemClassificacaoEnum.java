package br.com.sjc.modelo.enums;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum OrigemClassificacaoEnum {

    ORIGEM("Origem", "01"),

    DESTINO("Destino", "02");

    private String descricao;

    private String codigo;

    OrigemClassificacaoEnum(final String descricao, final String codigo) {

        this.descricao = descricao;

        this.codigo = codigo;
    }

    public static OrigemClassificacaoEnum getLocalPesagem(final String codigo) {
        return Arrays.asList(OrigemClassificacaoEnum.values()).stream().filter(i -> new Long(i.getCodigo()).toString().equals(new Long(codigo).toString())).findFirst().orElse(null);
    }
}
