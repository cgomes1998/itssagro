package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusLacreEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(name = "tb_lacre")
public class Lacre extends EntidadeAutenticada {

    @EntityProperties(value = {"id"})
    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_lote", nullable = false)
    private Lote lote;

    @Column
    private String codigo;

    @Column(name = "observacao_status_lacre_alterar", columnDefinition = "text")
    private String observacaoStatusLacreAlterar;

    @Enumerated(EnumType.STRING)
    private StatusLacreEnum statusLacre;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_inutilizacao")
    private Date dataInutilizacao;

    @EntityProperties(value = {"id", "login", "nome", "cpf"})
    @ManyToOne
    @JoinColumn(name = "id_usuario_que_inutilizou")
    private Usuario usuarioQueInutilizou;

    @EntityProperties(value = {"id", "descricao", "tipo"})
    @ManyToOne
    @JoinColumn(name = "id_motivo_inutilizacao")
    private Motivo motivoInutilizacao;

    @Transient
    private Date dataUtilizacao;

    @Transient
    private String nfe;

    @Transient
    private String numeroRomaneio;

    @Transient
    private StatusRomaneioEnum statusRomaneio;

    @Transient
    private StatusIntegracaoSAPEnum statusIntegracaoSAP;

    public Lacre() {

    }

    public Lacre(String codigo, Date dataUtilizacao, String nfe, String numeroRomaneio, StatusLacreEnum statusLacre, StatusRomaneioEnum statusRomaneio, StatusIntegracaoSAPEnum statusSap) {

        this.codigo = codigo;
        this.dataUtilizacao = dataUtilizacao;
        this.nfe = StringUtil.isNotNullEmpty(nfe) ? nfe : "***";
        this.numeroRomaneio = numeroRomaneio;
        this.statusRomaneio = statusRomaneio;
        this.statusIntegracaoSAP = statusSap;
        this.statusLacre = statusLacre;
    }

    public Lacre preencherDadosPersistidos(Long id, Lote lote, StatusLacreEnum statusLacre, Date dataCadastro, Date dataInutilizacao, Usuario usuarioQueInutilizou, Motivo motivoInutilizacao) {

        this.setId(id);
        this.setLote(lote);
        this.setDataCadastro(dataCadastro);
        this.setDataInutilizacao(dataInutilizacao);
        this.setUsuarioQueInutilizou(usuarioQueInutilizou);
        this.setMotivoInutilizacao(motivoInutilizacao);
        this.setStatusLacre(statusLacre);
        return this;
    }

}
