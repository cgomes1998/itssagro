package br.com.sjc.modelo.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class SaldoPedidoReservadoRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    private String numeroRomaneioConsumidor;
    private String numeroPedido;
    private String itemPedido;
}
