package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.util.anotation.ProjectionConfigurationDTO;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ProjectionConfigurationDTO(groupProperties = true)
public class RomaneioBridgeDTO {

    @ProjectionProperty
    private String requestKey;

    @ProjectionProperty(values = {"id", "numeroRomaneio"})
    private Romaneio romaneio;

}
