package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.Analise;
import br.com.sjc.modelo.Motivo;
import br.com.sjc.modelo.cfg.CampoOperacaoMaterial;
import br.com.sjc.modelo.enums.TipoTransporteEnum;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_material", schema = "sap")
public class Material extends EntidadeSAPPre {

    @Column(name = "desc_material")
    private String descricao;

    @Column(name = "un_medida")
    private String unidadeMedida;

    @Column(name = "valor_unitario")
    private BigDecimal valorUnitario;

    @Column(name = "realizar_conversao_litragem", nullable = false, columnDefinition = "boolean default false")
    private boolean realizarConversaoLitragem;

    @Column(name = "necessita_treinamento_motorista", nullable = false, columnDefinition = "boolean default false")
    private boolean necessitaTreinamentoMotorista;

    @Column(name = "lacrar_carga", nullable = false, columnDefinition = "boolean default false")
    private boolean lacrarCarga;

    @Column(name = "realizar_analise_qualidade", nullable = false, columnDefinition = "boolean default false")
    private boolean realizarAnaliseQualidade;

    @Column(name = "buscar_certificado_qualidade", nullable = false, columnDefinition = "boolean default false")
    private boolean buscarCertificadoQualidade;

    @Column(name = "necessita_mais_de_uma_analise", nullable = false, columnDefinition = "boolean default false")
    private boolean necessitaMaisDeUmaAnalise;

    @Column(name = "possui_etapa_carregamento", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiEtapaCarregamento;

    @Column(name = "gerar_numero_certificado", nullable = false, columnDefinition = "boolean default false")
    private boolean gerarNumeroCertificado;

    @Column(name = "possui_resolucao_anp", nullable = false, columnDefinition = "boolean default false")
    private boolean possuiResolucaoAnp;

    @Column(name = "exibir_especificacao_impressao_laudo", nullable = false, columnDefinition = "boolean default false")
    private boolean exibirEspecificacaoNaImpressaoDoLaudo;

    @Column(name = "tolerancia_kg")
    private BigDecimal toleranciaEmKg;

    @Enumerated(EnumType.STRING)
    @Column(name = "tp_transporte")
    private TipoTransporteEnum tipoTransporte;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Analise> analises;

    @EntityProperties(value = {"id", "operacao", "obrigatorio", "somenteLeitura", "display", "campo.id", "campo.descricao", "material.id"}, targetEntity = CampoOperacaoMaterial.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, mappedBy = "material", fetch = FetchType.LAZY)
    private List<CampoOperacaoMaterial> campos;

    @EntityProperties(value = {"id", "quantidadeASerCalculada", "unidadeMedidaASerCalculada", "quantidadeEquivalente", "unidadeMedidaEquivalente", "utilizarCasasDecimais", "material.id"}, targetEntity = ConfiguracaoUnidadeMedidaMaterial.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, mappedBy = "material", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<ConfiguracaoUnidadeMedidaMaterial> conversoes;

    public Material() {

        super();
    }

    public Material(final String codigo, final String descricao, final String unidadeMedida) {

        this.setCodigo(codigo);

        this.descricao = descricao;

        this.unidadeMedida = unidadeMedida;
    }

    @Override
    public String toString() {

        return this.getCodigo() + " - " + this.descricao;
    }

    public String getDescricaoFormatada() {

        return toString();
    }
}
