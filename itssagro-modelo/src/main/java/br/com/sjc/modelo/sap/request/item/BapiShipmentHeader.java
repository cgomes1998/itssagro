package br.com.sjc.modelo.sap.request.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@SAPParameter( function = "BAPISHIPMENTHEADER" )
public class BapiShipmentHeader extends JCoItem {

    @SAPColumn( name = "SHIPMENT_NUM" )
    private String numeroDocTransporte;

    @SAPColumn( name = "SHIPMENT_TYPE" )
    private String tipoDeTransporte;

    @SAPColumn( name = "TRANS_PLAN_PT" )
    private String centro;

    @SAPColumn( name = "CONTAINER_ID" )
    private String placaCavalo;

    @SAPColumn( name = "STATUS_PLAN" )
    private String stOrganizacaoTransporte;

    @SAPColumn( name = "STATUS_CHECKIN" )
    private String stRegistro;

    @SAPColumn( name = "STATUS_LOAD_START" )
    private String stInicioCarregamento;

    @SAPColumn( name = "STATUS_LOAD_END" )
    private String stFimCarregamento;

    @SAPColumn( name = "STATUS_SHPMNT_START" )
    private String stInicioTransporte;

    @SAPColumn( name = "SERVICE_AGENT_ID" )
    private String transportadora;

    @SAPColumn( name = "TEXT_1" )
    private String placaUm;

    @SAPColumn( name = "TEXT_2" )
    private String placaDois;

    @SAPColumn( name = "TEXT_3" )
    private String nomeDoMotorista;

    @SAPColumn( name = "STATUS_SHPMNT_END" )
    private String stFimTransporte;

    @SAPColumn( name = "STATUS_COMPL" )
    private String stProcessamentoTransporte;

    public BapiShipmentHeader () {

    }

    public BapiShipmentHeader ( String numeroDocTransporte ) {

        this.numeroDocTransporte = numeroDocTransporte;
    }

    public BapiShipmentHeader (
            String tipoDeTransporte,
            String centro,
            String placaCavalo,
            String transportadora,
            String placaUm,
            String placaDois,
            String nomeDoMotorista
    ) {

        this.tipoDeTransporte = tipoDeTransporte;
        this.centro = centro;
        this.placaCavalo = placaCavalo;
        this.transportadora = transportadora;
        this.placaUm = placaUm;
        this.placaDois = placaDois;
        this.nomeDoMotorista = nomeDoMotorista;
    }
}
