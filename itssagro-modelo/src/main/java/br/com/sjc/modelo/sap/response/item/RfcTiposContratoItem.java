package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.sap.TipoContrato;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import br.com.sjc.util.DateUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class RfcTiposContratoItem extends JCoItem {

    @SAPColumn(name = "BSART")
    private String codigo;

    @SAPColumn(name = "BATXT")
    private String descricao;

    public TipoContrato toEntidade() {

        TipoContrato entidade = new TipoContrato();

        entidade.setDataCadastro(DateUtil.hoje());

        entidade.setCodigo(codigo);

        entidade.setDescricao(descricao);

        return entidade;
    }
}
