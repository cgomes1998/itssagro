package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SAPParameter(function = "/ITSSAGRO/XML_CTE_TCIB_T_HD")
public class RfcObterCteHeaderResponse extends JCoItem {

    @SAPColumn(name = "CHCTE")
    private String chave;

    @SAPColumn(name = "NCT")
    private String nct;

    @SAPColumn(name = "SERIE")
    private String serie;

    @SAPColumn(name = "DHEMI")
    private String dhemi;

    @SAPColumn(name = "CNPJ_EMIT")
    private String cnpjEmit;

    @SAPColumn(name = "UF_EMIT")
    private String ufEmit;

    @SAPColumn(name = "CNPJ_TOMA")
    private String cnpjToma;

    @SAPColumn(name = "UF_TOMA")
    private String ufToma;

    @SAPColumn(name = "CFOP")
    private String cfop;

    @SAPColumn(name = "VALCTE")
    private BigDecimal valorCte;

    @SAPColumn(name = "V_ICMS")
    private BigDecimal valorIcms;

    @SAPColumn(name = "V_TOT_TRIB")
    private BigDecimal valorTotalTrib;

    @SAPColumn(name = "NPROT")
    private String nprot;

    @SAPColumn(name = "CREDAT")
    private String credat;

    @SAPColumn(name = "CRETIM")
    private String cretim;

    @SAPColumn(name = "NFEREF")
    private String nferef;

    @SAPColumn(name = "LAST_STEPSTATUS")
    private String status;
}
