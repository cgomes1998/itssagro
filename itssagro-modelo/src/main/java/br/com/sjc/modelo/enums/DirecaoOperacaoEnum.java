package br.com.sjc.modelo.enums;

import lombok.Getter;

@Getter
public enum DirecaoOperacaoEnum {

    ENTRADA("E", "Entrada"),

    SAIDA("S", "Saída"),

    NAO_SE_APLICA("", "Não se Aplica");

    private String codigo;

    private String descricao;

    DirecaoOperacaoEnum(final String codigo, final String descricao) {

        this.codigo = codigo;

        this.descricao = descricao;
    }
}
