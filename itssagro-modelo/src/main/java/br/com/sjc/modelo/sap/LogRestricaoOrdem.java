package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.EntidadeGenerica;
import br.com.sjc.modelo.LogRomaneioAcoes;
import br.com.sjc.modelo.enums.TipoRestricaoEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_log_restricao_ordem", schema = "sap")
@SuppressWarnings("all")
public class LogRestricaoOrdem extends EntidadeGenerica {

    @Transient
    private String uuid = UUID.randomUUID().toString();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    @Type(type = "text")
    private String mensagem;

    @Column(name = "codigo_ordem_venda")
    private String codigoOrdemVenda;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_restricao")
    private TipoRestricaoEnum tipoRestricao;

    @ManyToOne
    @JoinColumn(name = "id_log_romaneio_acoes")
    @JsonIgnore
    private LogRomaneioAcoes logRomaneioAcoes;

    public LogRestricaoOrdem() {
        super();
    }

    public LogRestricaoOrdem(
            String mensagem,
            Long idLogRomaneioAcoes
    ) {
        this.setMensagem(mensagem);
        this.setLogRomaneioAcoes(new LogRomaneioAcoes());
        this.getLogRomaneioAcoes().setId(idLogRomaneioAcoes);
    }

    public LogRestricaoOrdem(String mensagem,
                             String codigoOrdemVenda,
                             TipoRestricaoEnum tipoRestricao,
                             LogRomaneioAcoes logRomaneioAcoes) {

        this.setMensagem(mensagem);

        this.setCodigoOrdemVenda(codigoOrdemVenda);

        this.setTipoRestricao(tipoRestricao);

        this.setLogRomaneioAcoes(logRomaneioAcoes);
    }
}
