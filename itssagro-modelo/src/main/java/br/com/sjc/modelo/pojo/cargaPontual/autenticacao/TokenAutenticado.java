package br.com.sjc.modelo.pojo.cargaPontual.autenticacao;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TokenAutenticado {

    private String token;

}
