package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.response.item.RfcClienteResponseItem;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@SAPParameter(function = "/ITSSAGRO/CDU008_CLIENTE")
public class RfcClienteResponse extends JCoResponse {

    @SAPTable(name = "T_CLIENTES", item = RfcClienteResponseItem.class)
    private List<RfcClienteResponseItem> clientes;

}
