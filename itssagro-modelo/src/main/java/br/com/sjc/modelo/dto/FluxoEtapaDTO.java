package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.FluxoEtapa;
import br.com.sjc.modelo.Formulario;
import br.com.sjc.modelo.TipoFluxoEnum;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.enums.FormularioStatus;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.util.ObjetoUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FluxoEtapaDTO extends DataDTO {

    private FluxoDTO fluxo;

    private Long sequencia;

    private EtapaEnum etapa;

    private boolean medirTempo = false;

    private boolean etapaFinal = false;

    private FormularioDTO formulario;

    public FluxoEtapaDTO(Long idFluxoEtapa, Long idFluxo, Long sequencia, EtapaEnum etapa, TipoFluxoEnum tipoFluxo) {

        this.fluxo = new FluxoDTO();

        this.setId(idFluxoEtapa);

        this.setSequencia(sequencia);

        this.getFluxo().setId(idFluxo);

        this.getFluxo().setTipoFluxo(tipoFluxo);

        this.etapa = etapa;
    }

    public FluxoEtapaDTO(
            Long id,
            StatusEnum status,
            Long sequencia,
            EtapaEnum etapa,
            boolean medirTempo,
            boolean etapaFinal,
            Long idFluxo,
            Long codigoFluxo,
            String descricaoFluxo,
            TipoFluxoEnum tipoFluxo,
            Long idFormulario,
            String descricaoFormulario,
            FormularioStatus formularioStatus,
            Boolean formularioCabecalhoDadosTransportadora,
            Boolean formularioCabecalhoDadosMotorista,
            Boolean formularioCabecalhoDadosCliente,
            Boolean formularioCabecalhoDadosVeiculo
                        ) {

        setId(id);

        setStatus(status);

        setSequencia(sequencia);

        setEtapa(etapa);

        setMedirTempo(medirTempo);

        setEtapaFinal(etapaFinal);

        setFluxo(new FluxoDTO(idFluxo, codigoFluxo, descricaoFluxo, tipoFluxo));

        if (ObjetoUtil.isNotNull(idFormulario)) {

            setFormulario(new FormularioDTO(getFormulario(idFormulario, descricaoFormulario, formularioStatus, formularioCabecalhoDadosTransportadora, formularioCabecalhoDadosMotorista, formularioCabecalhoDadosCliente, formularioCabecalhoDadosVeiculo)));
        }
    }

    public Formulario getFormulario(
            Long idFormulario,
            String descricaoFormulario,
            FormularioStatus formularioStatus,
            Boolean formularioCabecalhoDadosTransportadora,
            Boolean formularioCabecalhoDadosMotorista,
            Boolean formularioCabecalhoDadosCliente,
            Boolean formularioCabecalhoDadosVeiculo
                                   ) {

        Formulario formulario = new Formulario();

        formulario.setId(idFormulario);

        formulario.setDescricao(descricaoFormulario);

        formulario.setFormularioStatus(formularioStatus);

        formulario.setCabecalhoDadosTransportadora(formularioCabecalhoDadosTransportadora);

        formulario.setCabecalhoDadosCliente(formularioCabecalhoDadosCliente);

        formulario.setCabecalhoDadosMotorista(formularioCabecalhoDadosMotorista);

        formulario.setCabecalhoDadosVeiculo(formularioCabecalhoDadosVeiculo);

        return formulario;
    }

    public FluxoEtapaDTO(FluxoEtapa fluxoEtapa) {

        setId(fluxoEtapa.getId());

        setStatus(fluxoEtapa.getStatus());

        if (ObjetoUtil.isNotNull(fluxoEtapa.getFluxo())) {

            setFluxo(new FluxoDTO(fluxoEtapa.getFluxo()));
        }

        setSequencia(fluxoEtapa.getSequencia());

        setEtapa(fluxoEtapa.getEtapa());

        setMedirTempo(fluxoEtapa.isMedirTempo());

        setFormulario(ObjetoUtil.isNotNull(fluxoEtapa.getFormulario()) ? new FormularioDTO(fluxoEtapa.getFormulario()) : null);
    }

    public FluxoEtapaDTO(Long idFluxo, Long idFluxoEtapa, StatusEnum statusFluxoEtapa, EtapaEnum etapa) {

        setId(idFluxoEtapa);

        setFluxo(new FluxoDTO(idFluxo, null, null, null));

        setStatus(statusFluxoEtapa);

        setEtapa(etapa);
    }

}
