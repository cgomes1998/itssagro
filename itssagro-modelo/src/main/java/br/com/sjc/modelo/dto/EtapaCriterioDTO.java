package br.com.sjc.modelo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EtapaCriterioDTO {

    private Integer repetirEtapa;

    private Long idFluxoEtapa;
}
