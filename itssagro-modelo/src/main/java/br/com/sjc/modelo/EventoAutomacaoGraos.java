package br.com.sjc.modelo;

import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_evento_automacao_graos")
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventoAutomacaoGraos extends EntidadeAutenticada {

    @Column
    private String descricao;

    @EntityProperties(value = {"id", "entidade", "descricao", "modulo", "direcaoOperacao", "operacao", "eventoEntrada", "eventoSaida", "possuiDivisaoCarga", "possuiPesagem", "possuiClassificacao", "pesagemManual", "aplicaClassificacao",
            "simplesPesagem", "possuiConsultaSaldoDisponivel", "possuiBuscaNFeTransporte", "possuiClassificacaoOrigem", "possuiConsultaContrato", "possuiDadosNfe", "possuiRetornoNfeSAP", "possuiRetornoNfeTransporteSAP",
            "possuiValidacaoEntreVlrUniNotaEPedido", "toleranciaEntreVlrUniNotaEPedido", "possuiValidacaoParaEscriturarNota", "possuiImpressaoPorEtapa", "possuiValidacaoChaveAcesso", "possuiRateio", "possuiValidacaoDivergenciaProdutorNotaEPedido",
            "possuiValidacaoChaveEFornecedor", "utilizaObterDadosNfe", "utilizaObterDadosCte", "enviarEmailPesagemManual", "gestaoPatio", "criarRemessa", "criarDocumentoTransporte", "possuiValidacaoConversaoLitragem", "umidadeInformadaManualmente", "requestCount", "tempoPreCadastro",
            "ultimaSincronizacao"}, targetEntity = DadosSincronizacao.class)
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "tb_evento_automacao_graos_operacao", schema = "public", joinColumns = @JoinColumn(name = "id_evento_automacao_graos"), inverseJoinColumns = @JoinColumn(name = "id_operacao"))
    private List<DadosSincronizacao> operacoes;

    @EntityProperties(value = {"id", "eventoAutomacaoGraos.id", "sequencia", "statusEstornoAutomacao", "mensagemEstornoAutomacao", "etapa"}, collecions = {"eventos"}, targetEntity = EventoAutomacaoGraosEtapa.class)
    @OrderBy("id")
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "eventoAutomacaoGraos", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<EventoAutomacaoGraosEtapa> etapas;

}
