package br.com.sjc.modelo.endpoint.request.item;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MotoristaRequest {

    private String cpf;

    private String nome;

    private String cnh;
}
