package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.EntidadeAutenticada;
import br.com.sjc.modelo.enums.StatusConversaoLitragemEnum;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
@AllArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_conversao_litragem", schema = "sap")
public class ConversaoLitragem extends EntidadeAutenticada {

    @Column
    private Long tanque;

    @Column(name = "temp_tanque", precision = 19, scale = 2)
    private BigDecimal tempTanque;

    @Column(name = "grau_inpm", precision = 19, scale = 2)
    private BigDecimal grauInpm;

    @Column(precision = 19, scale = 2)
    private BigDecimal acidez;

    @Column(name = "status_conversao")
    @Enumerated(EnumType.STRING)
    private StatusConversaoLitragemEnum statusConversao;

    @EntityProperties(value = {"id", "dataCadastro", "conversaoLitragem.id",
            "tanque", "tempTanque", "tempCarreta", "grauInpm", "acidez",
            "volumeSetaVinteGraus", "volumeDensidadeLVinteGraus", "diferencaLitros"},
            targetEntity = ConversaoLitragemItem.class)
    @OrderBy("dataCadastro desc")
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE},
            mappedBy = "conversaoLitragem", fetch = FetchType.LAZY)
    private List<ConversaoLitragemItem> conversoes;

    @Transient
    private Material material;

    /*
    Utilizado para informar se é para validar a coversão,
    isso porque não deve ser validade quando utilizado no dashboard de filas,
    caso contrário, deve ser validado.
    */
    @Transient
    private boolean validarConversao;

    public ConversaoLitragem() {
        super();
    }

    public ConversaoLitragem(
            Long tanque,
            BigDecimal tempTanque,
            BigDecimal grauInpm,
            BigDecimal acidez,
            List<ConversaoLitragemItem> conversoes) {
        this.tanque = tanque;
        this.tempTanque = tempTanque;
        this.grauInpm = grauInpm;
        this.acidez = acidez;
        if (ColecaoUtil.isNotEmpty(conversoes)) {
            this.conversoes = conversoes
                    .stream()
                    .map(c -> new ConversaoLitragemItem(
                            c.getDataCadastro(),
                            c.getTanque(),
                            c.getTempTanque(),
                            c.getTempCarreta(),
                            c.getGrauInpm(),
                            c.getAcidez(),
                            c.getVolumeSetaVinteGraus(),
                            c.getVolumeDensidadeLVinteGraus(),
                            c.getDiferencaLitros()
                    ))
                    .collect(Collectors.toList());
        }
    }

    public boolean isNotNull() {
        return ObjetoUtil.isNotNull(this.tanque) &&
                ObjetoUtil.isNotNull(this.tempTanque) &&
                ObjetoUtil.isNotNull(this.grauInpm);
    }

}
