package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.EntidadeGenerica;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.text.MessageFormat;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_iva", schema = "sap")
public class Iva extends EntidadeGenerica {

    @Column(length = 2)
    private String codigo;

    @Column(length = 50)
    private String descricao;

    @Override
    public String toString() {
        return MessageFormat.format("{0} - {1}", this.codigo, this.descricao);
    }
}
