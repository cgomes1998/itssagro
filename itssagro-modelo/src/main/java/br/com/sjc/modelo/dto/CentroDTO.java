package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CentroDTO extends DataDTO {

    @ProjectionProperty
    private String codigo;

    @ProjectionProperty
    private String descricao;

    @ProjectionProperty
    private StatusCadastroSAPEnum statusCadastroSap;

    @ProjectionProperty
    private StatusRegistroEnum statusRegistro;

    public String getStatusSapDescricao() {

        return this.statusCadastroSap.getDescricao();
    }

    public String getStatusRegistroDescricao() {

        return this.statusRegistro.getDescricao();
    }

}
