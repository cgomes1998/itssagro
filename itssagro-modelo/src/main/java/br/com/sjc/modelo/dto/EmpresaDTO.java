package br.com.sjc.modelo.dto;

import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmpresaDTO extends DataDTO {

    @ProjectionProperty
    private String razaoSocial;

    @ProjectionProperty
    private String cnpj;

    @ProjectionProperty
    private String endereco;

    @ProjectionProperty
    private String cidade;

    @ProjectionProperty
    private String estado;

    @ProjectionProperty
    private String cep;

    @ProjectionProperty
    private String codigoAnp;

}
