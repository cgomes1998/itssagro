package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.DirecaoOperacaoDepositoEnum;
import br.com.sjc.modelo.enums.TipoAcessoPortariaEnum;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PortariaTicketDTO extends DataDTO {

    @ProjectionProperty(values = {"id", "descricao"})
    private PortariaDTO portaria;

    @ProjectionProperty
    private Date dataEntrada;

    @ProjectionProperty
    private Date dataMarcacao;

    @ProjectionProperty
    private Date dataSaida;

    @ProjectionProperty(values = {"id", "descricao"})
    private PortariaMotivoDTO motivo;

    @ProjectionProperty
    private String motivoComplemento;

    @ProjectionProperty
    private String cnpj;

    @ProjectionProperty
    private String empresa;

    @ProjectionProperty
    private String cpfMotorista;

    @ProjectionProperty
    private String motorista;

    @ProjectionProperty
    private String idSjcMotorista;

    @ProjectionProperty(values = {"id", "descricao"})
    private PortariaVeiculoDTO veiculo;

    @ProjectionProperty
    private String placaVeiculo;

    @ProjectionProperty
    private String placaCarreta1;

    @ProjectionProperty
    private String placaCarreta2;

    @ProjectionProperty
    private String placaCarreta3;

    @ProjectionProperty(values = {"id", "descricao"})
    private PortariaItemDTO produto;

    @ProjectionProperty
    private String observacao;

    @ProjectionProperty
    private String nomeColaborador;

    @ProjectionProperty
    private String idColaborador;

    @ProjectionProperty
    private String senha;

    @ProjectionProperty
    private TipoAcessoPortariaEnum tipoAcesso;

    @ProjectionProperty
    private String notaFiscal;

    @ProjectionProperty
    private boolean estorno;

    @ProjectionProperty
    private Date dataEstorno;

    @ProjectionProperty
    private String idColaboradorEstorno;

    @ProjectionProperty
    private String motivoEstorno;
}
