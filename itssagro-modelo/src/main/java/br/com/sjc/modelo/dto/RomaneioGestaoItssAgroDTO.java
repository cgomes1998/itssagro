package br.com.sjc.modelo.dto;

import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * Created by djeison.cassimiro on 05/04/2020
 */
@Getter
@Setter
public class RomaneioGestaoItssAgroDTO {

	private String motorista;

	private String placa;

	private String numRomaneio;

	private String pesoBruto;

	private String pesoTara;

	private Date dataPrimeiraPesagem;

	private Date dataSegundaPesagem;

	private String pesoLiquido;

	private Long idPesagem;

	private boolean gestaoPatio;
	
	private Date dataCriacaoNotaFiscal;
	
	private String requestKey;

	public RomaneioGestaoItssAgroDTO(
		   String numRomaneio,
		   String motorista,
		   String placa,
		   BigDecimal primeiraPesagem,
		   BigDecimal segundaPesagem,
		   Long idPesagem,
		   String requestKey,
		   boolean gestaoPatio
	) {

		setNumRomaneio(Objects.isNull(numRomaneio) ? StringUtil.empty() : numRomaneio);

		setMotorista(Objects.isNull(motorista) ? StringUtil.empty() : motorista);

		setPlaca(Objects.isNull(placa) ? StringUtil.empty() : placa);

		setIdPesagem(idPesagem);
		
		setRequestKey(requestKey);

		setGestaoPatio(gestaoPatio);

		if (gestaoPatio) {

			setPesoBruto(Objects.isNull(primeiraPesagem) ? BigDecimal.ZERO.toString() : primeiraPesagem.toString());

			setPesoTara(Objects.isNull(segundaPesagem) ? BigDecimal.ZERO.toString() : segundaPesagem.toString());

			setPesoLiquido(Objects.isNull(segundaPesagem) || Objects.isNull(primeiraPesagem) || segundaPesagem.compareTo(primeiraPesagem) == 1 ? " 0" : primeiraPesagem.toString());

		} else {

			setPesoBruto(Objects.isNull(segundaPesagem) ? BigDecimal.ZERO.toString() : segundaPesagem.toString());

			setPesoTara(Objects.isNull(primeiraPesagem) ? BigDecimal.ZERO.toString() : primeiraPesagem.toString());

			setPesoLiquido(Objects.isNull(primeiraPesagem) || Objects.isNull(segundaPesagem) || primeiraPesagem.compareTo(segundaPesagem) == 1 ? BigDecimal.ZERO.toString() : segundaPesagem.toString());
		}
	}

}
