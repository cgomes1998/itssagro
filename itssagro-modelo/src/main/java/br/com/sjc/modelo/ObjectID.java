package br.com.sjc.modelo;

import java.io.Serializable;

public interface ObjectID<ID extends Serializable> {

    ID getId();
    void setId(ID id);

}
