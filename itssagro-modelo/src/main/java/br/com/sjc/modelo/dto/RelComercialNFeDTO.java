package br.com.sjc.modelo.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class RelComercialNFeDTO {

    private String numeroNfe;

    private String numeroRomaneio;;

    private Date dataNfe;

    public RelComercialNFeDTO(String numeroNfe, String numeroRomaneio, Date dataNfe) {

        this.numeroNfe = numeroNfe;

        this.numeroRomaneio = numeroRomaneio;

        this.dataNfe = dataNfe;
    }


}