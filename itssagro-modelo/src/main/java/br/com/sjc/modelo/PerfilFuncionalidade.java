package br.com.sjc.modelo;

import br.com.sjc.modelo.dto.PerfilFuncionalidadeDTO;
import br.com.sjc.modelo.enums.FuncionalidadeEnum;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(name = "tb_perfil_funcionalidade")
public class PerfilFuncionalidade implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "ds_funcionalidade")
    private FuncionalidadeEnum funcionalidade;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private boolean visualizar;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private boolean alterar;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private boolean incluir;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private boolean excluir;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private boolean consultar;

    @Transient
    private Boolean permitirConsultar;

    @Transient
    private Boolean permitirVisualizar;

    @Transient
    private Boolean permitirIncluir;

    @Transient
    private Boolean permitirAlterar;

    @Transient
    private Boolean permitirExcluir;

    @EntityProperties(value = {"id"})
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_perfil")
    private Perfil perfil;

    public String getFuncionalidadeDescricao() {

        return this.funcionalidade.getDescricao();
    }

    public PerfilFuncionalidadeDTO toDTOApenasFuncionalidade() {

        PerfilFuncionalidadeDTO dto = new PerfilFuncionalidadeDTO();

        dto.setFuncionalidade(this.getFuncionalidade());

        return dto;
    }

}
