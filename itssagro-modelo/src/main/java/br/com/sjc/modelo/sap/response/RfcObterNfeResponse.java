package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPStructure;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPParameter(function = "/ITSSAGRO/XML_NFE_TCNH_T_HD")
public class RfcObterNfeResponse extends JCoResponse {

    @SAPStructure(name = "W_HEADER", item = RfcObterNfeHeaderResponse.class )
    private RfcObterNfeHeaderResponse header;

    @SAPStructure( name = "B_ITEM", item = RfcObterNfeItemResponse.class )
    private RfcObterNfeItemResponse b_item;

}
