package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class RfcOrdemVendaItemSaidaResponse extends JCoItem {

    @SAPColumn( name = "ITM_NUMBER" )
    private String numeroItem;

    @SAPColumn( name = "MATERIAL" )
    private String codigoMaterial;

    @SAPColumn( name = "ITEM_CATEG" )
    private String categoriaItem;

    @SAPColumn( name = "SALES_UNIT" )
    private String unidadeVenda;

    @SAPColumn( name = "REF_DOC" )
    private String numeroDocumentoReferencia;

    @SAPColumn( name = "PLANT" )
    private String centro;

    @SAPColumn( name = "ROUTE" )
    private String itinerario;

    @SAPColumn( name = "STGE_LOC" )
    private String codigoDeposito;

    @SAPColumn( name = "SHIP_POINT" )
    private String localExpedicao;

}
