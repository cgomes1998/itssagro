package br.com.sjc.modelo.cfg;

import br.com.sjc.modelo.EntidadeGenerica;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_campo_operacao", schema = "cfg")
public class CampoOperacao extends EntidadeGenerica {

  private static final long serialVersionUID = 1L;

  @Column(name = "operacao", updatable = false)
  @Enumerated(EnumType.STRING)
  private DadosSincronizacaoEnum operacao;

  @Column(name = "obrigatorio", nullable = false, columnDefinition = "boolean default false")
  private boolean obrigatorio;

  @Column(name = "somente_leitura", nullable = false, columnDefinition = "boolean default false")
  private boolean somenteLeitura;

  @Column(name = "divisao_carga", nullable = false, columnDefinition = "boolean default false")
  private boolean divisaoCarga;

  @Column(name = "permitir_valor_default", nullable = false, columnDefinition = "boolean default false")
  private boolean permitirValorDefault;

  @Column(name = "valor_default_e_enum", nullable = false, columnDefinition = "boolean default false")
  private boolean valorDefaultEEnum;

  @Column(name = "valor_default")
  private String valorDefault;

  @Column(name = "descricao_display")
  private String display;

  @EntityProperties(value = {"id", "descricao"})
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "id_campo", updatable = false)
  private Campo campo;

  public String getnomeVariavel() {

    return this.campo.getDescricao();
  }

}
