package br.com.sjc.modelo.enums;

import lombok.Getter;

@Getter
public enum DirecaoOperacaoDepositoEnum {

    ENTRADA("Entrada"),
    SAIDA("Saída"),
    TRANSFERENCIA("Transferência"),
    TRANSFERENCIA_TERCEIRO("Transferência Terceiro"),
    RETORNO("Retorno"),
    RETORNO_TERCEIRO("Retorno Terceiro"),
    ARMAZENAGEM("Armazenagem");

    private String descricao;

    DirecaoOperacaoDepositoEnum(String descricao) {

        this.descricao = descricao;
    }
}
