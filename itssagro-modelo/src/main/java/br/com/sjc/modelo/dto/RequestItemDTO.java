package br.com.sjc.modelo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestItemDTO {

    private String requestKey;

    private String operacao;

    private String numeroRomaneio;

}
