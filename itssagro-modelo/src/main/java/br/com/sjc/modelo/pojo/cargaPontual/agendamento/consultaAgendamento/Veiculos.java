package br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Veiculos {

    private String codigoTipoVeiculo;

    private String codsistemaexternotipoveiculo;

    private String tipoVeiculo;

    private PlacaCavalo placaCavalo;

    private Placa1semi placa1semi;

    private Placa2semi placa2semi;

    private Placa3semi placa3semi;

}
