package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.sap.RetornoNFe;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.util.ColecaoUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RomaneioArmazemTercDTO {

    private String numeroRomaneio;

    private String chaveNfRemessa;

    private StatusIntegracaoSAPEnum statusRomaneio;

    public RomaneioArmazemTercDTO(final Romaneio entidade) {

        setNumeroRomaneio(entidade.getNumeroRomaneio());

        setStatusRomaneio(entidade.getStatusIntegracaoSAP());

        if (ColecaoUtil.isNotEmpty(entidade.getRetornoNfes())) {

            final String nfe = entidade.getRetornoNfes().stream().map(RetornoNFe::getNfe).findFirst().orElse(null);

            setChaveNfRemessa(nfe);
        }

    }
}
