package br.com.sjc.modelo;

import br.com.sjc.modelo.sap.Material;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_fluxo_material")
public class FluxoMaterial extends EntidadeAutenticada {

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_fluxo", nullable = false)
    @EntityProperties(value = {"id"})
    private Fluxo fluxo;

    @EntityProperties(value = {"id", "codigo", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_material")
    private Material material;

}
