package br.com.sjc.modelo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TabelaClassificacaoItemDTO implements Serializable {

    private Long idItem;

    private String indice;

    private String percentualDesconto;

    private Double indiceNumerico;

    private boolean indicePermitidoPorPerfil;
}
