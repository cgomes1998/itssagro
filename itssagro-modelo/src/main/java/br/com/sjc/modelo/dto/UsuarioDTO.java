package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.Usuario;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.sap.Produtor;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioDTO extends DataDTO {
    
    @ProjectionProperty
    private String login;
    
    @ProjectionProperty
    private String senha;
    
    @ProjectionProperty
    private String nome;
    
    @ProjectionProperty
    private String cpf;
    
    @ProjectionProperty
    private StatusEnum status;
    
    public UsuarioDTO(final Usuario usuario) {
        
        setId(usuario.getId());
        setLogin(usuario.getLogin());
        setSenha(usuario.getSenha());
        setNome(usuario.getNome());
        setStatus(usuario.getStatus());
        setCpf(usuario.getCpf());
    }
    
    public UsuarioDTO(final Produtor produtor) {
        
        setId(produtor.getId());
        setLogin(StringUtil.apresentarValorIsNull("", produtor.getCpf(), produtor.getCnpj()));
        setSenha(produtor.getSenha());
        setNome(produtor.getNome());
    }
    
}
