package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.response.item.RfcProdutorResponseItem;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@SAPParameter(function = "ZITSSAGRO_FM_DM_PRODUTOR")
public class RfcProdutorResponse extends JCoResponse {

    @SAPTable(name = "PRODUTOR_RET", item = RfcProdutorResponseItem.class)
    private List<RfcProdutorResponseItem> retorno;
}
