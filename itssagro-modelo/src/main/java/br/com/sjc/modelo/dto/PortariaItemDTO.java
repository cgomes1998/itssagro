package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.cfg.PortariaItem;
import br.com.sjc.modelo.cfg.PortariaMotivo;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PortariaItemDTO extends DataDTO {
    @ProjectionProperty
    private String descricao;

    @ProjectionProperty
    private Boolean ativo;

    public PortariaItemDTO(PortariaItem item){
        this.setId(item.getId());
        this.setDescricao(item.getDescricao());
        this.setAtivo(item.isAtivo());
    }
}
