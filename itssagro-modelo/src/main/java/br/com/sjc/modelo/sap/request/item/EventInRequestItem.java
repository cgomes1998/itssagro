package br.com.sjc.modelo.sap.request.item;

import br.com.sjc.modelo.sap.Bridge;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class EventInRequestItem extends JCoItem {

    @SAPColumn(name = "CHAVE")
    private String requestKey;

    @SAPColumn(name = "FIELD")
    private String campo;

    @SAPColumn(name = "SEQNC")
    private String sequencia;

    @SAPColumn(name = "GRCOD")
    private String grupo;

    @SAPColumn(name = "GRITM")
    private String item;

    @SAPColumn(name = "VALUE")
    private String valor;

    public Integer getGrupoValid() {

        return StringUtil.isNotNullEmpty(this.grupo) ? Integer.valueOf(this.grupo) : Integer.valueOf(0);
    }

    public static Bridge toBridge(
            final EventInRequestItem item,
            final Romaneio romaneio
    ) {

        Bridge entidade = new Bridge();

        entidade.setDataCadastro(DateUtil.hoje());

        entidade.setCampo(item.getCampo());

        entidade.setGrupo(item.getItem());

        entidade.setItemDocumento(StringUtil.isNotNullEmpty(item.getGrupo()) ? item.getGrupo() : "0");

        entidade.setRequestKey(StringUtil.completarZeroAEsquerda(20, item.getRequestKey()));

        entidade.setValor(item.getValor());

        entidade.setSequencia(item.getSequencia());

        entidade.setRomaneio(romaneio);

        return entidade;
    }

    public static EventInRequestItem instance(
            final String requestKey,
            final String campo,
            final String sequencia,
            final String valor
    ) {

        final EventInRequestItem instance = new EventInRequestItem();

        instance.setRequestKey(requestKey);

        instance.setCampo(campo);

        instance.setSequencia(sequencia);

        instance.setItem("0");

        instance.setValor(valor);

        return instance;
    }

    public static EventInRequestItem instance(
            final String requestKey,
            final String campo,
            final String sequencia,
            final String item,
            final String valor
    ) {

        final EventInRequestItem instance = new EventInRequestItem();

        instance.setRequestKey(requestKey);

        instance.setCampo(campo);

        instance.setSequencia(sequencia);

        instance.setItem(item);

        instance.setValor(valor);

        return instance;
    }

    public static EventInRequestItem instance(
            final String requestKey,
            final String campo,
            final String sequencia,
            final String item,
            final String grupo,
            final String valor
    ) {

        final EventInRequestItem instance = new EventInRequestItem();

        instance.setRequestKey(requestKey);

        instance.setCampo(campo);

        instance.setSequencia(sequencia);

        instance.setItem(item);

        instance.setGrupo(grupo);

        instance.setValor(valor);

        return instance;
    }

    public static EventInRequestItem instance(final String requestKey) {

        final EventInRequestItem instance = new EventInRequestItem();

        instance.setRequestKey(requestKey);

        return instance;
    }

    public static String generateKey(
            final long requestCount,
            final String externo,
            final String modulo,
            final String anoAtual
    ) {

        final StringBuilder requestKey = new StringBuilder();

        requestKey.append(requestCount)

                .append(externo)

                .append(modulo)

                .append(anoAtual);

        String valor = requestKey.toString();

        return StringUtil.completarZeroAEsquerda(20, valor);
    }

    public static String generateKey(
            final long requestCount,
            final String externo,
            final String modulo,
            final String centro,
            final String anoAtual
    ) {

        final StringBuilder requestKey = new StringBuilder();

        requestKey.append(requestCount)

                .append(externo)

                .append(modulo)

                .append(centro)

                .append(anoAtual);

        String valor = requestKey.toString();

        return StringUtil.completarZeroAEsquerda(20, valor);
    }

}
