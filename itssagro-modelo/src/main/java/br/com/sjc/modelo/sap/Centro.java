package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.EntidadeGenerica;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.anotation.EntityProperties;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_centro", schema = "sap")
public class Centro extends EntidadeSAPSync {

    @Column(name = "desc_centro")
    private String descricao;

    @Column(name = "fone")
    private String fone;

    @Column(name = "email")
    private String email;

    @EntityProperties(value = {"id", "codigo", "descricao", "statusCadastroSap", "statusRegistro", "centro.id"}, targetEntity = Deposito.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "centro", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    private List<Deposito> depositos;

    @Override
    public String toString() {

        return this.getCodigo() + " - " + this.descricao;
    }

    @Override
    public boolean equals(final Object object) {

        if (object == this) {

            return Boolean.TRUE;
        }

        if (ObjetoUtil.isNull(object) || !(object instanceof EntidadeGenerica) || object.getClass() != this.getClass()) {

            return Boolean.FALSE;
        }

        final Centro centro = (Centro) object;

        if (ObjetoUtil.isNotNull(this.getCodigo()) && !this.getCodigo().equals(centro.getCodigo())) {

            return false;
        }

        return true;
    }

}
