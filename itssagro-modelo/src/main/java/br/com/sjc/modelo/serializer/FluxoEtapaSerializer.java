package br.com.sjc.modelo.serializer;

import br.com.sjc.modelo.FluxoEtapa;

import java.io.IOException;

public class FluxoEtapaSerializer extends CustomSerializer<FluxoEtapa> {

    @Override
    protected void serializeAttributes(FluxoEtapa entidade) throws IOException {

        this.writeObjectField("etapa", entidade.getEtapa());

        this.writeNumberField("sequencia", entidade.getSequencia());
    }
}
