package br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Retorno2 {

    private int status;

    private String statusDescricao;

    private String mensagem;

    private Conteudo conteudo;

}
