package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.response.item.RfcStatusPedidoResponseItem;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@SAPParameter(function = "ZITSSAGRO_FM_LIB_PEDIDO")
public class RfcStatusPedidoResponse extends JCoResponse {

    @SAPTable(name = "T_STATUS", item = RfcStatusPedidoResponseItem.class)
    private List<RfcStatusPedidoResponseItem> tStatus;
}
