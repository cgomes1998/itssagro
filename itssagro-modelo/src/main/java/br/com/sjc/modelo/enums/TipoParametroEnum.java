package br.com.sjc.modelo.enums;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.List;

@Getter
@RequiredArgsConstructor
public enum TipoParametroEnum {

    CODIGO_FILIAL("Código SAP da Filial"),

    CODIGO_CARAMURU("Código da Caramuru"),

    CENTRO_SAP("Centro SAP"),

    SENHA_PADRAO("Senha Padrão para Cadastro de Usuários"),

    MAIL_IMAP_HOST("(Host) Servidor de e-mail"),

    MAIL_IMAP_PORT("Porta E-mail"),

    MAIL_USUARIO("E-mail"),

    MAIL_USUARIO_DESTINATARIOS("Destinatários"),

    MAIL_PASSWORD("Senha"),

    TIPO_FLUXO("Fluxo para criação do Romaneio"),

    DIRETORIO_XML("Diretório onde os xmls obtidos no e-mail serão salvos"),

    QUANTIDADE_HORAS_VALIDADE_SENHA("Quantidade de Horas de Validade da Senha"),

    QUANTIDADE_DE_CHAMADAS_DE_SENHA("Quantidade de Chamadas da Senha"),

    EMAIL_PESO_INFORMADO_MANUALMENTE("E-mail Peso Informado Manualmente"),

    TOLERANCIA_NA_PESAGEM_BRUTO("Tolerância na Pesagem (Bruto)"),

    TOLERANCIA_NA_PESAGEM_TARA("Tolerância na Pesagem (Tara)"),

    TOLERANCIA_NA_PESAGEM_BRUTO_ETANOL("Tolerância na Pesagem (Bruto Etanol)"),

    EMAIL_RESPONSAVEL_PELA_LIBERACAO_SENHA("E-mail Responsável Pela Liberação de Senha"),

    CODIGO_COOPERATIVA("Código Cooperativa"),

    DEPOSITO_PADRAO_ROMANEIO("Código do Depósito Padrão"),

    CENTRO_CLIENTE("Código do cliente que representa o centro"),

    TIPO_ORDEM_INUTILIZADA("Tipos de ordens de vendas que não podem ser utilizadas (Separando por ; | Ex.: ZP01;ZP02 )"),

    IP_IMPRESSORA_AUTOATENDIMENTO("IP da Impressora do Autoatendimento"),

    PORTA_IMPRESSORA_AUTOATENDIMENTO("Porta da Impressora do Autoatendimento"),

    IP_IMPRESSORA_ENTRADA_MOTORISTA("IP da Impressora da Entrada do Motorista"),

    PORTA_IMPRESSORA_ENTRADA_MOTORISTA("Porta da Impressora da Entrada do Motorista"),

    TIPO_IMPRESSORA_ENTRADA_MOTORISTA("Tipo de impressora entrada motorista (bematech, normal)"),

    STATUS_AGENDAMENTO_ENTRADA_MOTORISTA("Status do Agendamento para Entrada do Motorista"),

    STATUS_AGENDAMENTO_RECEBIMENTO_NFE("Status do Agendamento para o Recebimento da NFe"),

    SERVIDOR_LDAP("AD - Servidor LDAP"),

    DOMINIO_LDAP("AD - Dominio LDAP"),

    BASE_DN_LDAP("AD - Diretório Base LDAP");

    @NonNull
    private String descricao;

    public static List<TipoParametroEnum> listarParametrosParaDonwloadNfe() {

        return Arrays.asList(new TipoParametroEnum[]{TipoParametroEnum.DIRETORIO_XML,
                TipoParametroEnum.MAIL_PASSWORD,
                TipoParametroEnum.MAIL_USUARIO,
                TipoParametroEnum.MAIL_IMAP_PORT,
                TipoParametroEnum.MAIL_IMAP_HOST});
    }
}
