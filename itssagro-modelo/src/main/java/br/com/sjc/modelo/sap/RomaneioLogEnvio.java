package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.EntidadeAutenticada;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_log_envio_romaneio", schema = "sap")
public class RomaneioLogEnvio extends EntidadeAutenticada {

    private static final long serialVersionUID = -3351987708785118658L;

    @ManyToOne
    @JoinColumn(name = "id_romaneio", referencedColumnName = "id")
    private Romaneio romaneio;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dt_envio")
    private Date dataEnvio;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dt_resposta")
    private Date dataResposta;

    @Column(name = "log_retorno", columnDefinition = "text")
    private String log;

    @Column(name = "log_exceptio", columnDefinition = "text")
    private String exception;

}
