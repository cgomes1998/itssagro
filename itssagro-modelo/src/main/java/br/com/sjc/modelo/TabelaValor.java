package br.com.sjc.modelo;

import br.com.sjc.modelo.sap.Material;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_tabela_valor")
public class TabelaValor extends EntidadeAutenticada {

    @Column
    private String descricao;

    @EntityProperties(value = {"id", "codigo", "descricao", "unidadeMedida", "valorUnitario", "realizarConversaoLitragem", "necessitaTreinamentoMotorista", "lacrarCarga", "realizarAnaliseQualidade", "buscarCertificadoQualidade",
            "necessitaMaisDeUmaAnalise", "possuiEtapaCarregamento", "gerarNumeroCertificado", "possuiResolucaoAnp", "exibirEspecificacaoNaImpressaoDoLaudo", "toleranciaEmKg", "tipoTransporte",})
    @ManyToOne
    @JoinColumn(name = "id_material")
    private Material material;

    @EntityProperties(value = {"id", "nome", "mimeType", "base64"})
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "id_arquivo")
    private Arquivo arquivo;

    @EntityProperties(value = {"id", "tabelaValor.id", "temperatura", "massaEspecifica", "massaEspecificaVinteGraus", "grauAlcoolico", "grauAlcoolicoVinteGraus", "fatorReducao"}, targetEntity = TabelaValorItem.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "tabelaValor", cascade = CascadeType.ALL, orphanRemoval = true)
    @Getter(value = AccessLevel.NONE)
    private List<TabelaValorItem> itens = new ArrayList<>();

    public List<TabelaValorItem> getItens() {

        return ColecaoUtil.isNotEmpty(itens) ? itens.stream().sorted(Comparator.comparing(TabelaValorItem::getId, Comparator.nullsLast(Comparator.naturalOrder()))).collect(Collectors.toList()) : new ArrayList<>();
    }

}
