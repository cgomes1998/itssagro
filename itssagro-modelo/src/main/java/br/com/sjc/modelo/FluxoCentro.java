package br.com.sjc.modelo;

import br.com.sjc.modelo.sap.Centro;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_fluxo_centro")
public class FluxoCentro extends EntidadeAutenticada {

    @EntityProperties(value = {"id"})
    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_fluxo", nullable = false)
    private Fluxo fluxo;

    @EntityProperties(value = {"id", "codigo", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_centro")
    private Centro centro;

}
