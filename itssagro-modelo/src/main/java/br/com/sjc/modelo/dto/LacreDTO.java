package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.Lacre;
import br.com.sjc.modelo.Lote;
import br.com.sjc.modelo.enums.StatusLacreEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class LacreDTO extends DataDTO {

    private String codigo;

    private String statusDescricao;

    private StatusLacreEnum statusLacre;

    private Lote lote;

    public LacreDTO(Lacre lacre) {
        this();
        setId(lacre.getId());
        setCodigo(lacre.getCodigo());
        setStatusDescricao(lacre.getStatusLacre().getDescricao());
        setStatusLacre(lacre.getStatusLacre());
        setLote(lacre.getLote());
    }

}
