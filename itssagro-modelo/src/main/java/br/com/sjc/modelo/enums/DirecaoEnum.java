package br.com.sjc.modelo.enums;

import lombok.Getter;

@Getter
public enum DirecaoEnum {

    ENTRADA("Entrada"),

    SAIDA("Saída"),

    BIDIRECIONAL("Bidirecional");

    private String descricao;

    DirecaoEnum(final String descricao) {

        this.descricao = descricao;
    }
}
