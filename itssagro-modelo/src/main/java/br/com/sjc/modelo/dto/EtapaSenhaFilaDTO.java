package br.com.sjc.modelo.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EtapaSenhaFilaDTO extends DataDTO {

    private SenhaDTO senha;

    private RomaneioDTO romaneio;

    private FluxoEtapaDTO fluxoEtapa;

    private Integer repetirEtapa = 0;

}
