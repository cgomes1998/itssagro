package br.com.sjc.modelo;

import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_pesagem_historico", schema = "sap")
public class PesagemHistorico extends EntidadeGenerica {

    @EntityProperties(value = {"id"})
    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_pesagem", nullable = false)
    private Pesagem pesagem;

    @Column(name = "peso_inicial", scale = 3)
    private BigDecimal pesoInicial;

    @Column(name = "peso_final", scale = 3)
    private BigDecimal pesoFinal;

    @EntityProperties(value = {"id", "nome", "login", "cpf"})
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;

}
