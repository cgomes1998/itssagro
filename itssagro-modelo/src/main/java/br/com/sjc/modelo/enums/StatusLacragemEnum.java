package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusLacragemEnum {

    AGUARDANDO("Aguardando"),
    EM_ANDAMENTO("Em Andamento"),
    LACRADO("Lacrado"),
    CANCELADO("Cancelado"),
    PENDENTE("Pendente");

    private String descricao;
}
