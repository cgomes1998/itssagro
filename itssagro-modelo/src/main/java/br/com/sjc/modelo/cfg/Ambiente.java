package br.com.sjc.modelo.cfg;

import br.com.sjc.modelo.EntidadeAutenticada;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_ambiente", schema = "cfg")
public class Ambiente extends EntidadeAutenticada {

    @Column(name = "flag_ambiente_padrao", nullable = false, columnDefinition = "boolean default false")
    private boolean padrao;

    @Column(name = "flag_load_bal", nullable = false, columnDefinition = "boolean default false")
    private boolean loadBalancing;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "rfc_mandante")
    private String mandante;

    @Column(name = "rfc_nr_instancia")
    private String numeroInstancia;

    @Column(name = "rfc_host")
    private String host;

    @Column(name = "rfc_sap_houter")
    private String sapRouter;

    @Column(name = "rfc_usuario")
    private String rfcUsuario;

    @Column(name = "rfc_senha")
    private String rfcSenha;

    @Column(name = "rfc_porta")
    private String rfcPorta;

    @Column(name = "rfc_program_id")
    private String rfcProgramID;

    @Column(name = "rfc_server_name")
    private String rfcServerName;

    @Column(name = "rfc_group", length = 10)
    private String group;

    @Column(name = "rfc_r3name", length = 10)
    private String r3name;

    @Override
    public String toString() {
        return this.mandante + " - " + this.descricao;
    }
}
