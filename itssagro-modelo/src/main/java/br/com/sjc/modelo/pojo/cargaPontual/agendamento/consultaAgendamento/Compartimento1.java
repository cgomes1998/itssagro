package br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Compartimento1 {

    private Compartimento1seta1 compartimento1seta1;

    private Compartimento1seta2 compartimento1seta2;

    private Compartimento1seta3 compartimento1seta3;

}
