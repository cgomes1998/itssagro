package br.com.sjc.modelo;

import br.com.sjc.modelo.sap.Material;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Setter
@Getter
@Entity
@Table(name = "tb_lote_analise_qualidade")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoteAnaliseQualidade extends EntidadeAutenticada {

    @Column
    private String codigo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dt_fabricacao")
    private Date dataFabricacao;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dt_vencimento")
    private Date dataVencimento;

    @EntityProperties(value = {"id", "codigo", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_material")
    private Material material;

    @Column(name = "motivo_inativacao", columnDefinition = "text")
    private String motivoInativacao;

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;
        LoteAnaliseQualidade that = (LoteAnaliseQualidade) o;
        return Objects.equals(codigo, that.codigo) && Objects.equals(dataFabricacao, that.dataFabricacao) && Objects.equals(dataVencimento, that.dataVencimento) && Objects.equals(material, that.material);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), codigo, dataFabricacao, dataVencimento, material);
    }

}
