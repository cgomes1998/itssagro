package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.sap.Motorista;
import br.com.sjc.modelo.sap.Veiculo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PreCadastroDTO {

    private Motorista motorista;

    private Veiculo veiculo;
}
