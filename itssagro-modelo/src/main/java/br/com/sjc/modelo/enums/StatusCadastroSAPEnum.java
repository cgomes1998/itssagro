package br.com.sjc.modelo.enums;

import lombok.Getter;

@Getter
public enum StatusCadastroSAPEnum {

    SINCRONIZADO_COM_SUCESSO("Sincronizado com sucesso"),

    NAO_ENVIADO("Não enviado"),

    RETORNO_COM_ERRO("Retorno com erro"),

    ENVIADO("Enviado");

    private String descricao;

    private StatusCadastroSAPEnum(final String descricao) {

        this.descricao = descricao;
    }
}
