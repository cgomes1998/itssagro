package br.com.sjc.modelo.comparator;

import br.com.sjc.modelo.sap.ItensClassificacao;

import java.util.Comparator;

public class ItensClassificacaoComparator implements Comparator<ItensClassificacao> {

    @Override
    public int compare(ItensClassificacao o1, ItensClassificacao o2) {

        return new Long(o1.getItemClassificao()).compareTo(new Long(o2.getItemClassificao()));
    }
}
