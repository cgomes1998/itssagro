package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.MateriaPrima;
import br.com.sjc.modelo.Usuario;
import br.com.sjc.modelo.enums.StatusLaudoEnum;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class LaudoListagemDTO extends DataDTO {

    @ProjectionProperty
    private String tanque;

    @ProjectionProperty
    private String numeroCertificado;

    @ProjectionProperty
    private Date dataCadastro;

    @ProjectionProperty
    private StatusLaudoEnum statusLaudo;

    @ProjectionProperty(values = {"id", "codigo", "descricao", "gerarNumeroCertificado"})
    private Material material;

    @ProjectionProperty(values = {"id", "nome"})
    private Usuario usuario;

    @ProjectionProperty
    private String volumeCertificado;

    @ProjectionProperty
    private String unidadeMedidaCertificado;

    @ProjectionProperty(values = {"id", "codigo", "descricao"})
    private MateriaPrima materiaPrima;

    @ProjectionProperty
    private String observacao;

}
