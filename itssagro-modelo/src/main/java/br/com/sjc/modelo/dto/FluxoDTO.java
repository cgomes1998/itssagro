package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.Fluxo;
import br.com.sjc.modelo.TipoFluxoEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FluxoDTO extends DataDTO {

    @ProjectionProperty
    private Long codigo;

    @ProjectionProperty
    private String descricao;

    @ProjectionProperty
    private TipoFluxoEnum tipoFluxo;

    @ProjectionProperty
    private StatusEnum status;

    public FluxoDTO(Fluxo fluxo) {

        setId(fluxo.getId());

        setStatus(fluxo.getStatus());

        setCodigo(fluxo.getCodigo());

        setDescricao(fluxo.getDescricao());

        setTipoFluxo(fluxo.getTipoFluxo());
    }

    public FluxoDTO(Long id, Long codigo, String descricao, TipoFluxoEnum tipoFluxo) {

        this.setId(id);

        this.codigo = codigo;

        this.descricao = descricao;

        this.tipoFluxo = tipoFluxo;
    }

}
