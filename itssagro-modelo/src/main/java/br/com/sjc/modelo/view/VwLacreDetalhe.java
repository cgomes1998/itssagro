package br.com.sjc.modelo.view;

import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusLacreEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

/*
-- sap.vw_lacre_detalhe source

CREATE OR REPLACE VIEW sap.vw_lacre_detalhe
AS SELECT row_number() OVER (ORDER BY v.codigo) AS id,
    v.codigo,
    v.dt_utilizacao,
    v.nota,
    v.nr_romaneio,
    v.usuario_que_utilizou,
    v.status_romaneio,
    v.status_sap,
    v.status_lacre
   FROM ( SELECT lacre_detalhe_com_romaneio.codigo,
            lacre_detalhe_com_romaneio.dt_utilizacao,
            lacre_detalhe_com_romaneio.nota,
            lacre_detalhe_com_romaneio.nr_romaneio::numeric AS nr_romaneio,
            lacre_detalhe_com_romaneio.usuario_que_utilizou,
            lacre_detalhe_com_romaneio.status_romaneio,
            lacre_detalhe_com_romaneio.status_sap,
            lacre_detalhe_com_romaneio.status_lacre
           FROM sap.vw_lacre_detalhe_com_romaneio lacre_detalhe_com_romaneio
        UNION
         SELECT lacre_detalhe_sem_romaneio.codigo,
            NULL::timestamp without time zone AS dt_utilizacao,
            NULL::text AS nota,
            NULL::numeric AS nr_romaneio,
            NULL::character varying AS usuario_que_utilizou,
            NULL::character varying AS status_romaneio,
            NULL::character varying AS status_sap,
            lacre_detalhe_sem_romaneio.status_lacre
           FROM sap.vw_lacre_detalhe_sem_romaneio lacre_detalhe_sem_romaneio) v;


-- sap.vw_lacre_detalhe_com_romaneio source

CREATE OR REPLACE VIEW sap.vw_lacre_detalhe_com_romaneio
AS SELECT lacre.codigo,
    usuario.nome AS usuario_que_utilizou,
    lacragem_lacre.dt_utilizacao,
        CASE
            WHEN max(retorno_nfe.nfe::text) IS NULL THEN max(retorno_nfe.nfe_transporte::text)
            ELSE max(retorno_nfe.nfe::text)
        END AS nota,
    romaneio.nr_romaneio,
    romaneio.status_romaneio,
    romaneio.status_sap,
        CASE
            WHEN romaneio.status_romaneio::text = 'EM_PROCESSAMENTO'::text THEN 'VINCULADO'::character varying
            WHEN romaneio.status_romaneio::text = 'CONCLUIDO'::text AND romaneio.status_sap IS NOT NULL AND romaneio.status_sap::text <> 'ESTORNADO'::text THEN 'UTILIZADO'::character varying
            WHEN romaneio.status_romaneio::text = 'ESTORNADO'::text THEN 'DISPONIVEL'::character varying
            WHEN romaneio.status_sap::text = 'ESTORNADO'::text THEN 'INUTILIZADO'::character varying
            ELSE lacre.statuslacre
        END AS status_lacre
   FROM tb_lacre lacre
     JOIN sap.tb_lacragem_lacre lacragem_lacre ON lacragem_lacre.id_lacre = lacre.id
     JOIN sap.tb_romaneio romaneio ON romaneio.id_lacragem = lacragem_lacre.id_lacragem
     LEFT JOIN sap.tb_retorno_nfe retorno_nfe ON retorno_nfe.id_romaneio = romaneio.id
     LEFT JOIN tb_usuario usuario ON usuario.id = lacragem_lacre.id_usuario_que_utilizou
  GROUP BY lacre.codigo, lacragem_lacre.dt_utilizacao, romaneio.nr_romaneio, romaneio.status_romaneio, romaneio.status_sap, lacre.statuslacre, usuario.nome, lacragem_lacre.status_lacre;


-- sap.vw_lacre_detalhe_sem_romaneio source

CREATE OR REPLACE VIEW sap.vw_lacre_detalhe_sem_romaneio
AS SELECT lacre.codigo,
    NULL::text AS dt_utilizacao,
    NULL::text AS nota,
    NULL::text AS nr_romaneio,
    NULL::text AS usuario_que_utilizou,
    NULL::text AS status_romaneio,
    NULL::text AS status_sap,
        CASE
            WHEN lacre.statuslacre::text = 'VINCULADO'::text OR lacre.statuslacre::text = 'UTILIZADO'::text THEN 'DISPONIVEL'::character varying
            ELSE lacre.statuslacre
        END AS status_lacre
   FROM tb_lacre lacre
     LEFT JOIN sap.tb_lacragem_lacre lacragem_lacre ON lacragem_lacre.id_lacre = lacre.id
  WHERE lacragem_lacre.id IS NULL;
 */
@Entity
@Table(name = "vw_lacre_detalhe", schema = "sap")
@Immutable
@Getter
@Setter
public class VwLacreDetalhe {
    
    @Id
    private Long id;
    
    @Column(name = "codigo")
    private String codigo;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dt_utilizacao")
    private Date dataUtilizacao;
    
    @Column(name = "nota")
    private String nfe;
    
    @Column(name = "nr_romaneio")
    private Long numeroRomaneio;
    
    @Column(name = "usuario_que_utilizou")
    private String usuarioQueUtilizou;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "status_romaneio")
    private StatusRomaneioEnum statusRomaneio;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "status_sap")
    private StatusIntegracaoSAPEnum statusIntegracaoSAP;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "status_lacre")
    private StatusLacreEnum statusLacre;
    
    public VwLacreDetalhe() {
    
    }
    
    @Builder
    public VwLacreDetalhe(String codigo, Date dataUtilizacao, String nfe, Long numeroRomaneio, StatusRomaneioEnum statusRomaneio, StatusIntegracaoSAPEnum statusIntegracaoSAP, StatusLacreEnum statusLacre, String usuarioQueUtilizou) {
        
        this.codigo = codigo;
        this.dataUtilizacao = dataUtilizacao;
        this.nfe = nfe;
        this.numeroRomaneio = numeroRomaneio;
        this.statusRomaneio = statusRomaneio;
        this.statusIntegracaoSAP = statusIntegracaoSAP;
        this.statusLacre = statusLacre;
        this.usuarioQueUtilizou = usuarioQueUtilizou;
    }
    
    @Override
    public String toString() {
        
        return "VwLacreDetalhe{" + "codigo='" + codigo + '\'' + ", numeroRomaneio=" + numeroRomaneio + '}';
    }
    
}
