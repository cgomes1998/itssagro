package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.sap.Material;
import br.com.sjc.util.ObjetoUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RelatorioLaudoFiltroDTO {

    private Date dataInicio;

    private Date dataFim;

    private Material material;

    public boolean isNotNull () {

        return  ObjetoUtil.isNotNull ( this.getDataInicio () ) || ObjetoUtil.isNotNull ( this.getDataFim () );
    }
}
