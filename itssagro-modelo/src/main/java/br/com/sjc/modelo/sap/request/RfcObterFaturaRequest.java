package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@SAPParameter(function = "ZITSSAGRO_GET_FATURA")
@NoArgsConstructor
@AllArgsConstructor
public class RfcObterFaturaRequest extends JCoRequest {

    @SAPColumn(name = "I_MODLO")
    private String modulo;

    @SAPColumn(name = "I_OPERA")
    private String operacao;

    @SAPColumn(name = "I_CHAVE")
    private String chave;

}
