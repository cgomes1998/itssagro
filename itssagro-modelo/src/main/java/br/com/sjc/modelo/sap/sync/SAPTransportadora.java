package br.com.sjc.modelo.sap.sync;

import br.com.sjc.modelo.sap.Transportadora;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;

@Getter
public class SAPTransportadora extends SAPEntidade {

    @SAPColumn(name = "COD_TRANSP")
    private String codigo;

    @SAPColumn(name = "NOME_TRANSP")
    private String descricao;

    @SAPColumn(name = "CNPJ_TRANSP")
    private String cnpj;

    @SAPColumn(name = "CPF_TRANSP")
    private String cpf;

    @SAPColumn(name = "BLOQUEADO")
    private String bloqueado;

    @SAPColumn(name = "MARC_ELIM")
    private String eliminado;

    public enum COLUMN_INPUT {

        DATA_ULT_INT,
    }

    public static SAPTransportadora newInstance() {

        return new SAPTransportadora();
    }

    public Transportadora copy(Transportadora entidadeSaved) {

        if (ObjetoUtil.isNull(entidadeSaved)) {

            entidadeSaved = new Transportadora();

        } else {

            entidadeSaved.setId(entidadeSaved.getId());
        }

        entidadeSaved.setCodigo(StringUtil.isNotNullEmpty(this.getCodigo()) ? this.getCodigo().trim() : null);

        entidadeSaved.setDescricao(StringUtil.isNotNullEmpty(this.getDescricao()) ? this.getDescricao().trim() : null);

        entidadeSaved.setCnpj(StringUtil.isNotNullEmpty(this.getCnpj()) ? this.getCnpj().trim() : null);

        entidadeSaved.setCpf(StringUtil.isNotNullEmpty(this.getCpf()) ? this.getCpf().trim() : null);

        StatusRegistroEnum status = StatusRegistroEnum.ATIVO;

        if (StringUtil.isNotNullEmpty(getBloqueado())) {

            status = StatusRegistroEnum.BLOQUEADO;
        }

        if (StringUtil.isNotNullEmpty(getEliminado())) {

            status = StatusRegistroEnum.ELIMINADO;
        }

        if (StringUtil.isNotNullEmpty(getBloqueado()) && StringUtil.isNotNullEmpty(getEliminado())) {

            status = StatusRegistroEnum.BLOQUEADO_E_ELIMINADO;
        }

        entidadeSaved.setStatusRegistro(status);

        entidadeSaved.setStatusCadastroSap(StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO);

        return entidadeSaved;
    }
}
