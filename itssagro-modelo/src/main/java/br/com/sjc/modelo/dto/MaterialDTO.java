package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.*;

import java.text.MessageFormat;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MaterialDTO extends DataDTO {

    @ProjectionProperty
    private String codigo;

    @ProjectionProperty
    private String descricao;

    @ProjectionProperty
    private boolean realizarConversaoLitragem;

    @ProjectionProperty
    private boolean necessitaTreinamentoMotorista;

    @ProjectionProperty
    private boolean lacrarCarga;

    @ProjectionProperty
    private boolean realizarAnaliseQualidade;

    @ProjectionProperty
    private boolean buscarCertificadoQualidade;

    @ProjectionProperty
    private boolean necessitaMaisDeUmaAnalise;

    @ProjectionProperty
    private boolean gerarNumeroCertificado;

    @ProjectionProperty
    private StatusCadastroSAPEnum statusCadastroSap;

    @ProjectionProperty
    private StatusRegistroEnum statusRegistro;

    @ProjectionProperty
    private boolean possuiResolucaoAnp;

    public MaterialDTO(Long id, String codigo, String descricao, boolean realizarConversaoLitragem, boolean necessitaTreinamentoMotorista, boolean lacrarCarga, boolean realizarAnaliseQualidade, boolean buscarCertificadoQualidade,
                       boolean necessitaMaisDeUmaAnalise, boolean gerarNumeroCertificado, boolean possuiResolucaoAnp) {

        this.setId(id);
        this.setCodigo(codigo);
        this.setDescricao(descricao);
        this.setRealizarConversaoLitragem(realizarConversaoLitragem);
        this.setNecessitaTreinamentoMotorista(necessitaTreinamentoMotorista);
        this.setLacrarCarga(lacrarCarga);
        this.setRealizarAnaliseQualidade(realizarAnaliseQualidade);
        this.setBuscarCertificadoQualidade(buscarCertificadoQualidade);
        this.setNecessitaMaisDeUmaAnalise(necessitaMaisDeUmaAnalise);
        this.setGerarNumeroCertificado(gerarNumeroCertificado);
        this.setPossuiResolucaoAnp(possuiResolucaoAnp);
    }

    public MaterialDTO(Long id, String codigo, String descricao) {

        this.setId(id);
        this.setCodigo(codigo);
        this.setDescricao(descricao);
    }

    public MaterialDTO(final Material entidade) {

        setId(entidade.getId());
        setCodigo(entidade.getCodigo());
        setDescricao(entidade.getDescricao());
        setRealizarConversaoLitragem(entidade.isRealizarConversaoLitragem());
        setNecessitaTreinamentoMotorista(entidade.isNecessitaTreinamentoMotorista());
        setLacrarCarga(entidade.isLacrarCarga());
        setRealizarAnaliseQualidade(entidade.isRealizarAnaliseQualidade());
        setBuscarCertificadoQualidade(entidade.isBuscarCertificadoQualidade());
        setNecessitaMaisDeUmaAnalise(entidade.isNecessitaMaisDeUmaAnalise());
        setStatusCadastroSap(entidade.getStatusCadastroSap());
        setStatusRegistro(entidade.getStatusRegistro());
        setPossuiResolucaoAnp(entidade.isPossuiResolucaoAnp());
    }

    @Override
    public String toString() {

        return MessageFormat.format("{0} - {1}", this.getCodigo(), this.getDescricao());
    }

    public String getStatusSapDescricao() {

        return ObjetoUtil.isNotNull(this.statusCadastroSap) ? this.statusCadastroSap.getDescricao() : null;
    }

    public String getStatusRegistroDescricao() {

        return ObjetoUtil.isNotNull(this.statusRegistro) ? this.statusRegistro.getDescricao() : null;
    }

}
