package br.com.sjc.modelo;

import br.com.sjc.util.anotation.EntityProperties;
import lombok.*;

import javax.persistence.*;

@Table(name = "tb_recebimento_nfe_sap_carga_pontual")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RecebimentoNfeSapCargaPontual extends EntidadeGenerica {

    @EntityProperties(value = {"id", "cargaPontualIdAgendamento"})
    @JoinColumn(name = "id_senha")
    @ManyToOne
    private Senha senha;

    @Column(columnDefinition = "text")
    private String exception;

    @Builder
    public RecebimentoNfeSapCargaPontual(Senha senha) {

        this.setSenha(senha);
    }

}
