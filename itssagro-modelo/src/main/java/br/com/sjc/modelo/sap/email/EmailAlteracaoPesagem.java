package br.com.sjc.modelo.sap.email;

import br.com.sjc.modelo.Pesagem;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.email.Email;
import br.com.sjc.util.email.EmailBodyBuilder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.text.NumberFormat;
import java.util.*;

/**
 * Created by julio.bueno on 24/07/2019.
 */
@RequiredArgsConstructor
public class EmailAlteracaoPesagem implements Email {

    @NonNull
    private Pesagem pesagem;

    @Override
    public String getAssunto() {
        return "Alteração de Pesagem Manualmente";
    }

    @Override
    public String getTextoEmail() {
        String textEmail = "";

        if(this.pesagem.isPesagemBrutaManual() && this.pesagem.getPesoInicial().compareTo(this.pesagem.getPesoInicialAntigo()) != 0){
            textEmail = "Realizado pesagem BRUTO (de ${pesagem.pesoInicialAntigo} para ${pesagem.pesoInicial}) digitada manualmente para o romaneio ${romaneio.numero} pelo seguinte motivo: ${motivoAlteracaoBruta}";
        }

        if(this.pesagem.isPesagemFinalManual() && this.pesagem.getPesoFinal().compareTo(this.pesagem.getPesoFinalAntigo()) != 0){
            if(StringUtil.isNotNullEmpty(textEmail)){
                textEmail += "----";
            }

            textEmail += "Realizado pesagem TARA (de ${pesagem.pesoFinalAntigo} para ${pesagem.pesoFinal}) digitada manualmente para o romaneio ${romaneio.numero} pelo seguinte motivo: ${motivo}";
        }

        return EmailBodyBuilder
                .build()
                .addText(textEmail)
                .toString();
    }

    @Override
    public List<String> getDestinatario() {
        return pesagem.getEmailsAlteracao();
    }

    @Override
    public Map<String, String> getParametros() {
        NumberFormat numberFormat = NumberFormat.getNumberInstance(new Locale.Builder().setLanguage("pt").setRegion("BR").build());

        Map<String, String> parametros = new HashMap<>();
        parametros.put("${romaneio.numero}", pesagem.getNumeroRomaneio());
        parametros.put("${motivo}", pesagem.getMotivoAlteracao());
        parametros.put("${motivoAlteracaoBruta}", pesagem.getMotivoAlteracaoBruta());
        parametros.put("${pesagem.pesoInicialAntigo}", Objects.isNull(pesagem.getPesoInicialAntigo()) ? "-" : numberFormat.format(pesagem.getPesoInicialAntigo().doubleValue()));
        parametros.put("${pesagem.pesoInicial}", Objects.isNull(pesagem.getPesoInicial()) ? "-" : numberFormat.format(pesagem.getPesoInicial().doubleValue()));
        parametros.put("${pesagem.pesoFinalAntigo}", Objects.isNull(pesagem.getPesoFinalAntigo()) ? "-" : numberFormat.format(pesagem.getPesoFinalAntigo().doubleValue()));
        parametros.put("${pesagem.pesoFinal}", Objects.isNull(pesagem.getPesoFinal()) ? "-" : numberFormat.format(pesagem.getPesoFinal().doubleValue()));

        return parametros;
    }

}
