package br.com.sjc.modelo.comparator;

import br.com.sjc.modelo.sap.ItemNFPedido;
import br.com.sjc.util.StringUtil;

import java.util.Comparator;

public class ItemNFPedidoComparator implements Comparator<ItemNFPedido> {

    @Override
    public int compare(ItemNFPedido o1, ItemNFPedido o2) {

        if (StringUtil.isNotNullEmpty(o1.getItemPedido()) && StringUtil.isNotNullEmpty(o2.getItemPedido())) {

            if (StringUtil.igual(new Long(o1.getItemPedido()).toString(), new Long(o2.getItemPedido()).toString())) {

                return 1;
            }

            return new Long(o1.getItemPedido()).compareTo(new Long(o2.getItemPedido()));
        }

        return o1.getId().compareTo(o2.getId());
    }
}
