package br.com.sjc.modelo.enums;

import lombok.Getter;

@Getter
public enum StatusContratoEnum {
    CONTRATO_ASSINADO(1),
    PAGAR_PENDENCIA_ASSINATURA(2),
    PENDENTE_ASSINATURA(3);

    private int codigoSap;

    StatusContratoEnum(int codigoSap) {
        this.codigoSap = codigoSap;
    }
}
