package br.com.sjc.modelo.mail;

public class MailAccount {
	
	private String usuario;
	
	private String senha;

	private String host;
	
	private Integer socketPort;
	
	private String socketClass;
	
	private Boolean auth;
	
	private Integer port;
	
	public MailAccount() {

	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getSocketPort() {
		return socketPort;
	}

	public void setSocketPort(Integer socketPort) {
		this.socketPort = socketPort;
	}

	public String getSocketClass() {
		return socketClass;
	}

	public void setSocketClass(String socketClass) {
		this.socketClass = socketClass;
	}

	public Boolean getAuth() {
		return auth;
	}

	public void setAuth(Boolean auth) {
		this.auth = auth;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((auth == null) ? 0 : auth.hashCode());
		result = prime * result + ((host == null) ? 0 : host.hashCode());
		result = prime * result + ((port == null) ? 0 : port.hashCode());
		result = prime * result + ((senha == null) ? 0 : senha.hashCode());
		result = prime * result + ((socketClass == null) ? 0 : socketClass.hashCode());
		result = prime * result + ((socketPort == null) ? 0 : socketPort.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MailAccount other = (MailAccount) obj;
		if (auth == null) {
			if (other.auth != null)
				return false;
		} else if (!auth.equals(other.auth))
			return false;
		if (host == null) {
			if (other.host != null)
				return false;
		} else if (!host.equals(other.host))
			return false;
		if (port == null) {
			if (other.port != null)
				return false;
		} else if (!port.equals(other.port))
			return false;
		if (senha == null) {
			if (other.senha != null)
				return false;
		} else if (!senha.equals(other.senha))
			return false;
		if (socketClass == null) {
			if (other.socketClass != null)
				return false;
		} else if (!socketClass.equals(other.socketClass))
			return false;
		if (socketPort == null) {
			if (other.socketPort != null)
				return false;
		} else if (!socketPort.equals(other.socketPort))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MailAccount [usuario=" + usuario + ", senha=" + senha + ", host=" + host + ", socketPort=" + socketPort
				+ ", socketClass=" + socketClass + ", auth=" + auth + ", port=" + port + "]";
	}
	
}
