package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.CondicaoEnum;
import br.com.sjc.util.ObjetoUtil;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_laudo_item", schema = "public")
public class LaudoItem extends EntidadeAutenticada {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_laudo", nullable = false)
    @JsonIgnore
    private Laudo laudo;

    @Column(name = "graus_inpm", nullable = false, columnDefinition = "boolean default false")
    private boolean grausInpm;

    @Column(name = "caracteristica")
    private String caracteristica;

    @Column(name = "metodologia")
    private String metodologia;

    @Column(name = "tipo_validacao")
    private String tipoValidacao;

    @Column(name = "especificacao")
    private String especificacao;

    @Column(name = "unidade")
    private String unidade;

    @Column(name = "sub_caracteristica")
    private String subCaracteristica;

    @Enumerated(EnumType.STRING)
    @Column(name = "condicao")
    private CondicaoEnum condicao;

    @Column(name = "valor_inicial")
    private String valorInicial;

    @Column(name = "valor_final")
    private String valorFinal;

    @Column(name = "laboratorio")
    private String laboratorio;

    @Column(name = "boletim")
    private String boletim;
    
    @Column(name = "resultado")
    private String resultado;

    @Column
    private Long index;

    @Column(name = "mostrar_especificacao", nullable = false, columnDefinition = "boolean default true")
    private boolean mostrarEspecificacao;

    @Transient
    private String condicaoStr;

    @PostLoad
    public void postLoad() {
        this.condicaoStr = ObjetoUtil.isNotNull(this.condicao) ? this.condicao.getDescricao() : null;
    }
}
