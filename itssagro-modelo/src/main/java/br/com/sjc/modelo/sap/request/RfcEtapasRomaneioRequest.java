package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPParameter(function = "ZITSSAGRO_FM_LOGS")
public class RfcEtapasRomaneioRequest extends JCoRequest {

    @SAPColumn(name = "INTPR")
    private String numeroRomaneio;
}
