package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusConfirmacaoPesoEnum {

    AGUARDANDO("Aguardando"),
    CONFIRMADO("Confirmado");

    private String descricao;
}
