package br.com.sjc.modelo.dto;

import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CartaoAcessoDTO extends DataDTO {

    @ProjectionProperty
    private String codigo;

    @ProjectionProperty
    private Integer numeroCartaoAcesso;

}
