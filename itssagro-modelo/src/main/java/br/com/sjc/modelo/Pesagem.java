package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.enums.StatusPesagemQualidadeEnum;
import br.com.sjc.modelo.sap.Safra;
import br.com.sjc.modelo.sap.Veiculo;
import br.com.sjc.util.BigDecimalUtil;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_pesagem", schema = "sap")
public class Pesagem extends EntidadeGenerica {

    @EntityProperties(value = {"id", "descricao", "codigo", "status"})
    @ManyToOne
    @JoinColumn(name = "id_safra")
    private Safra safra;

    @EntityProperties(value = {"id", "placa1", "codigo", "tipo.id", "tipo.peso"})
    @ManyToOne
    @JoinColumn(name = "id_veiculo")
    private Veiculo placaCavalo;

    @Column(name = "peso_inicial")
    @Setter(AccessLevel.NONE)
    private BigDecimal pesoInicial;

    @Column(name = "peso_final")
    private BigDecimal pesoFinal;

    @Column(name = "motivo_alteracao", columnDefinition = "TEXT")
    private String motivoAlteracao;

    @Column(name = "motivo_alteracao_bruta", columnDefinition = "TEXT")
    private String motivoAlteracaoBruta;

    @Column(name = "status_pesagem")
    @Enumerated(EnumType.STRING)
    private StatusPesagemQualidadeEnum statusPesagem = StatusPesagemQualidadeEnum.AGUARDANDO;

    @Column(name = "pesagem_bruta_manual", nullable = false, columnDefinition = "boolean default false")
    private boolean pesagemBrutaManual;

    @Column(name = "pesagem_tara_manual", nullable = false, columnDefinition = "boolean default false")
    private boolean pesagemFinalManual;

    @EntityProperties(value = {"id", "nome", "login", "cpf"})
    @ManyToOne
    @JoinColumn(name = "id_usuario_peso_inicial")
    private Usuario usuarioPesoInicial;

    @EntityProperties(value = {"id", "nome", "login", "cpf"})
    @ManyToOne
    @JoinColumn(name = "id_usuario_peso_final")
    private Usuario usuarioPesoFinal;

    @EntityProperties(value = {"descricao", "tipo", "conferenciaPeso", "buscarDadosRomaneio"})
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "tb_pesagem_motivo", schema = "public", joinColumns = @JoinColumn(name = "id_pesagem"), inverseJoinColumns = @JoinColumn(name = "id_motivo"))
    private List<Motivo> motivos = new ArrayList<>();

    @EntityProperties(value = {"pesagem.id", "dataCadastro", "pesoInicial", "pesoFinal", "usuario.id", "usuario.nome", "usuario.login", "usuario.cpf"}, targetEntity = PesagemHistorico.class)
    @OrderBy("dataCadastro desc")
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "pesagem", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PesagemHistorico> historicos = new ArrayList<>();

    @Transient
    private List<String> emailsAlteracao = new ArrayList<>();

    @Transient
    private String numeroRomaneio;

    @Transient
    private BigDecimal pesoInicialAntigo;

    @Transient
    private BigDecimal pesoFinalAntigo;

    @Transient
    private DadosSincronizacaoEnum dadosSincronizacaoEnum;

    public Pesagem() {

    }

    public Pesagem(Safra safra, Veiculo placaCavalo, BigDecimal pesoInicial, BigDecimal pesoFinal, String motivoAlteracao, String motivoAlteracaoBruta, StatusPesagemQualidadeEnum statusPesagem, List<Motivo> motivos) {

        this.safra = safra;
        this.placaCavalo = placaCavalo;
        this.pesoInicial = pesoInicial;
        this.pesoFinal = pesoFinal;
        this.motivoAlteracao = motivoAlteracao;
        this.motivoAlteracaoBruta = motivoAlteracaoBruta;
        this.statusPesagem = statusPesagem;
        this.motivos = motivos;
    }

    public void setPesoInicial(BigDecimal pesoInicial) {

        if (BigDecimalUtil.isMaiorQueZero(pesoInicial)) {
            this.pesoInicial = pesoInicial;
        }
    }

}
