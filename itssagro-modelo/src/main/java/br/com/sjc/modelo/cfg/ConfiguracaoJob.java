package br.com.sjc.modelo.cfg;

import br.com.sjc.modelo.EntidadeGenerica;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.enums.TipoJobEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_configuracao_job", schema = "cfg")
public class ConfiguracaoJob extends EntidadeGenerica {

    private static final long serialVersionUID = -3946564412651219292L;

    @Size(max = 255)
    @Column(name = "ds_nome", updatable = false, unique = true, length = 255)
    private String nome;

    @Column(name = "ds_observacao")
    private String observacao;

    @Column(name = "ds_schedule")
    private String schedule;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_job")
    private TipoJobEnum tipoJob;

    @Column(name = "is_ativo")
    private boolean ativo;

    @Enumerated(EnumType.STRING)
    @Column(name = "dados_sincronizacao")
    private DadosSincronizacaoEnum dadosSincronizacao;

    @Transient
    private Date dataUltimaSincronizacao;

}
