package br.com.sjc.modelo;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by julio.bueno on 10/07/2019.
 */
@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "tb_formulario_resposta")
public class FormularioResposta extends EntidadeGenerica {

    @ManyToOne
    @JoinColumn(name = "id_formulario_item",nullable = false)
    private FormularioItem item;

    @ManyToOne
    @JoinColumn(name = "id_valor_opcao")
    private FormularioItemOpcao valorOpcao;

    @Column(columnDefinition = "TEXT")
    private String valor;

    @ManyToMany(targetEntity = FormularioItemOpcao.class, fetch = FetchType.EAGER)
    @JoinTable(name = "tb_formulario_resposta_valor",joinColumns = @JoinColumn(name = "id_formulario_resposta"),inverseJoinColumns = @JoinColumn(name = "id_formulario_item_opcao"))
    private Set<FormularioItemOpcao> valores;

}
