package br.com.sjc.modelo;

import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by julio.bueno on 03/07/2019.
 */
@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_documento")
public class Documento extends EntidadeAutenticada {

    @Column
    private String codigo;

    @Column
    private String descricao;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_validade")
    private Date dataValidade;

    @Column
    private Boolean assinatura;

    @EntityProperties(value = {"id", "nome", "mimeType", "base64"})
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "id_arquivo")
    private Arquivo arquivo;

}
