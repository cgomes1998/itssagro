package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.sap.Romaneio;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestricaoOrdemDTO {

    private String mensagem;

    private String codigoOrdemVenda;

    private Romaneio romaneio;

    public RestricaoOrdemDTO() {
    }

    public RestricaoOrdemDTO(String mensagem, String codigoOrdemVenda, String numeroRomaneio) {
        this.mensagem = mensagem;
        this.codigoOrdemVenda = codigoOrdemVenda;
        this.romaneio = new Romaneio();
        this.romaneio.setNumeroRomaneio(numeroRomaneio);
    }
}
