package br.com.sjc.modelo.cfg;

import br.com.sjc.modelo.EntidadeGenerica;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_ambiente_sincronizacao", schema = "cfg")
public class AmbienteSincronizacao extends EntidadeGenerica {

    private static final long serialVersionUID = 8347066693634178861L;

    @EntityProperties(value = {"id", "padrao", "loadBalancing", "descricao", "mandante", "numeroInstancia", "host", "sapRouter", "rfcUsuario", "rfcSenha", "rfcPorta", "rfcProgramID", "rfcServerName", "group", "r3name"})
    @ManyToOne
    private Ambiente ambiente;

    @EntityProperties(value = {"id", "operacao", "entidade", "modulo"})
    @JsonIgnore
    @ManyToOne
    private DadosSincronizacao dadosSincronizacao;

    @Column(name = "count_requisicao")
    private long requestCount;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sincronizado_em")
    private Date ultimaSincronizacao;

    @Transient
    private Long idDadosSincronizacao;

}
