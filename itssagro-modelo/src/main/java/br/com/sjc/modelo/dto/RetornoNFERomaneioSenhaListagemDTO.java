package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.ObjectID;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.util.StringUtil;
import br.com.sjc.util.anotation.ProjectionConfigurationDTO;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ProjectionConfigurationDTO
public class RetornoNFERomaneioSenhaListagemDTO implements ObjectID<Long> {

    @ProjectionProperty
    private Long id;

    @ProjectionProperty(values = {"id", "numeroRomaneio", "requestKey", "senha.id", "senha.cargaPontualIdAgendamento", "senha.quantidadeCarregar"})
    private Romaneio romaneio;

}
