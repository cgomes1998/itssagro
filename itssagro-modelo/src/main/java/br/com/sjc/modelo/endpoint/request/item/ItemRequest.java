package br.com.sjc.modelo.endpoint.request.item;

import br.com.sjc.modelo.enums.TipoFreteEnum;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ItemRequest {

    private String nroNfTransporte;

    private String codigoFornecedor;

    private String codigoSafra;

    private String codigoMaterial;

    private String codigoDeposito;

    private String codigoTransportadora;

    private String codigoMotorista;

    private String codigoPlacaCavalo;

    private String placaCarretaUm;

    private String placaCarretadois;

    private String tipoFrete;

    private String chaveAcessoNfe;

    private String numeroNfe;

    private String serieNfe;

    private String dataNfe;

    private String numeroLog;

    private String digitoVerificadorNfe;

    private String numeroAleatorioChaveAcesso;

    private double valorUnitarioNfe;

    private double pesoTotalNfe;

    private double valorTotalNfe;

    public ItemNFPedido toEntidade(Produtor produtor, Safra safra, Material material, Deposito deposito, Transportadora transportadora, Motorista motorista, Veiculo placaCavalo, Veiculo placaCarretaUm, Veiculo placaCarretaDois) {

        ItemNFPedido item = new ItemNFPedido();

        item.setProdutor(produtor);

        item.setSafra(safra);

        item.setMaterial(material);

        item.setDeposito(deposito);

        item.setTransportadora(transportadora);

        item.setMotorista(motorista);

        item.setPlacaCavalo(placaCavalo);

        item.setPlacaCavaloUm(placaCarretaUm);

        item.setPlacaCavaloDois(placaCarretaDois);

        item.setNumeroNotaFiscalTransporte(this.nroNfTransporte);

        item.setTipoFrete(StringUtil.isNotNullEmpty(this.tipoFrete) ? TipoFreteEnum.valueOf(this.tipoFrete.toUpperCase().trim()) : null);

        item.setChaveAcessoNfe(this.chaveAcessoNfe);

        item.setNumeroNfe(this.numeroNfe);

        item.setSerieNfe(this.serieNfe);

        item.setDataNfe(StringUtil.isNotNullEmpty(this.dataNfe) ? DateUtil.format(this.dataNfe, "dd/MM/yyyy HH:mm:ss") : null);

        item.setNumeroLog(this.numeroLog);

        item.setDigitoVerificadorNfe(this.digitoVerificadorNfe);

        item.setNumeroAleatorioChaveAcesso(this.numeroAleatorioChaveAcesso);

        item.setValorTotalNfe(ObjetoUtil.isNotNull(this.valorTotalNfe) ? BigDecimal.valueOf(this.valorTotalNfe) : BigDecimal.ZERO);

        item.setPesoTotalNfe(ObjetoUtil.isNotNull(this.pesoTotalNfe) ? BigDecimal.valueOf(this.pesoTotalNfe) : BigDecimal.ZERO);

        item.setValorUnitarioNfe(ObjetoUtil.isNotNull(this.valorUnitarioNfe) ? BigDecimal.valueOf(this.valorUnitarioNfe) : BigDecimal.ZERO);

        return item;
    }
}
