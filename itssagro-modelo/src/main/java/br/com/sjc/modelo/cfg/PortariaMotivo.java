package br.com.sjc.modelo.cfg;

import br.com.sjc.modelo.EntidadeAutenticada;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_portaria_motivo", schema = "cfg")
public class PortariaMotivo extends EntidadeAutenticada {
    private static final long serialVersionUID = 1L;

    @Column(name = "descricao", length = 30, nullable = false)
    private String descricao;

    @Column(name = "ativo", nullable = false, columnDefinition = "boolean default false")
    private boolean ativo;
}
