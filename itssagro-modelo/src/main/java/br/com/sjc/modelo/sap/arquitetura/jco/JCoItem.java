package br.com.sjc.modelo.sap.arquitetura.jco;

/**
 * <p>
 * <b>Title:</b> JCoItem
 * </p>
 *
 * <p>
 * <b>Description:</b> Classe responsável por definir uma classe como item de uma tabela SAP.
 * </p>
 *
 * @author Bruno Zafalão
 * @version 1.0.0
 */
public class JCoItem {

    /**
     * Método responsável por obter o nome da classe utilizada como item da tabela SAP.
     *
     * @return <code>String</code>
     * @author Bruno Zafalão
     */
    public String name() {

        return this.getClass().getAnnotation(SAPItem.class).name();
    }
}
