package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.sap.CondicaoPagamento;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import br.com.sjc.util.DateUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class RfcCondicoesPagamentoItem extends JCoItem {

    @SAPColumn(name = "ZTERM")
    private String codigo;

    @SAPColumn(name = "TEXT1")
    private String descricao;

    public CondicaoPagamento toEntidade() {

        CondicaoPagamento entidade = new CondicaoPagamento();

        entidade.setDataCadastro(DateUtil.hoje());

        entidade.setCodigo(codigo);

        entidade.setDescricao(descricao);

        return entidade;
    }
}
