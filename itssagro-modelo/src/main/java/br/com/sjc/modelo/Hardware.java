package br.com.sjc.modelo;

import br.com.sjc.modelo.sap.Centro;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by julio.bueno on 02/07/2019.
 */
@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_hardware")
public class Hardware extends EntidadeAutenticada {

    public static String PREFIXO = "HD-";

    @Column
    private Long codigo;

    @Column(name = "codigo_formatado")
    private String codigoFormatado;

    @Column
    private String descricao;

    @Column
    private String endereco;

    @Column
    private String identificacao;

    @Column
    private Integer porta;

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "tb_hardware_centro", joinColumns = @JoinColumn(name = "id_hardware"), inverseJoinColumns = @JoinColumn(name = "id_centro"))
    @EntityProperties(value = {"id", "codigo", "descricao", "statusCadastroSap", "statusRegistro"}, targetEntity = Centro.class)
    private List<Centro> centros = new ArrayList<>();

    @Override
    public String toString() {

        return codigo + " - " + descricao;
    }

}
