package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.DirecaoEnum;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Collection;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_balanca", schema = "public")
public class Balanca extends EntidadeAutenticada {

    private static final long serialVersionUID = 1L;

    @Column(name = "nome")
    private String nome;

    @Column(name = "ip")
    private String ip;

    @Column(name = "porta")
    private Integer porta;

    @Enumerated(EnumType.STRING)
    @Column(name = "direcao")
    private DirecaoEnum direcao;

    @Column(name = "capacidade_minima")
    private Integer capacidadeMinima;

    @Column(name = "capacidade_maxima")
    private Integer capacidadeMaxima;

    @Column(name = "tempo_max_espera")
    private Integer tempoMaximoEspera;

    @Column(name = "tamanho_saltos")
    private Integer tamanhoSaltos;

    @Column(name = "tamanho_buffer")
    private Integer tamanhoBuffer;

    @Column(name = "observacao")
    private String observacao;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "balanca", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @EntityProperties(value = {"id", "tipoFluxo", "balanca.id"}, targetEntity = BalancaFluxoPesagem.class)
    private Collection<BalancaFluxoPesagem> fluxoPesagens;

}
