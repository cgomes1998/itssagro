package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class RfcOrdensVendaItemTxtResponse extends JCoItem {

    @SAPColumn(name = "VBELN")
    private String ordemVenda;

    @SAPColumn(name = "TDLINE")
    private String texto;
}
