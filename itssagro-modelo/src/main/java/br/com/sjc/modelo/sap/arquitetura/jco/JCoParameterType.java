package br.com.sjc.modelo.sap.arquitetura.jco;

/**
 * <p>
 * <b>Title:</b> JCoParameterType
 * </p>
 * 
 * <p>
 * <b>Description:</b> Tipo enumerado responsável por definir o ktokk de parâmetro enviado.
 * </p>
 * 
 * @author Bruno Zafalão
 * 
 * @version 1.0.0
 */
public enum JCoParameterType {

	/** Constante REQUEST. */
	REQUEST,

	/** Constante RESPONSE. */
	RESPONSE;
}
