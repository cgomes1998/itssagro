package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusPendenciaEnum {

    AGUARDANDO("Aguardando"),
    EM_ANALISE("Em Análise"),
    APROVADO("Aprovado"),
    CANCELADO("Cancelado"),
    LIBERADO("'Liberado'"),
    PENDENTE("Pendente");

    private String descricao;
}
