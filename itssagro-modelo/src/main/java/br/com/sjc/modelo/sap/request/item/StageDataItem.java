package br.com.sjc.modelo.sap.request.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SAPItem
public class StageDataItem extends JCoItem {

    @SAPColumn( name = "STAGE_CAT" )
    private String categoriaEtapa;

    @SAPColumn( name = "STAGE_SEQ" )
    private String sequenciaEtapaTransporte;

    @SAPColumn( name = "SHIPPING_TYPE" )
    private String tpExpedicaoEtapaTransporte;

    @SAPColumn( name = "LEG_INDICATOR" )
    private String codigoPercursoEtapaTransporte;

    @SAPColumn( name = "ORG_SHIPP_DPMNT", size = 4 )
    private String centro;

    @SAPColumn( name = "ORG_ADDR_INFO" )
    private String infoAdicional = "GESTAO-PATIO";

    @SAPColumn( name = "DEST_CUST", size = 10 )
    private String codigoCliente;

    @SAPColumn( name = "SERVICE_AGENT", size = 10 )
    private String codigoTransportadora;

    @SAPColumn( name = "SHPMNT_COST_REL" )
    private String relevanciaCustosFrete;

}
