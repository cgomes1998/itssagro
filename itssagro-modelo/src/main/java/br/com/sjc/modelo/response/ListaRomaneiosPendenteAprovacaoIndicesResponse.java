package br.com.sjc.modelo.response;

import br.com.sjc.modelo.dto.aprovacaoDocumento.RomaneioAprovacaoDocumento;
import br.com.sjc.modelo.dto.aprovacaoDocumento.UsuarioAprovacaoDocumento;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Collection;

@Data
@Builder
public class ListaRomaneiosPendenteAprovacaoIndicesResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private Collection<RomaneioAprovacaoDocumento> romaneios;
    private Collection<UsuarioAprovacaoDocumento> usuarios;
}
