package br.com.sjc.modelo;

import br.com.sjc.modelo.sap.Centro;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by julio.bueno on 03/07/2019.
 */
@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_local")
public class Local extends EntidadeAutenticada {

    public static String PREFIXO = "LO-";

    @Column
    private Long codigo;

    @Column(name = "codigo_formatado")
    private String codigoFormatado;

    @Column
    private String descricao;

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "tb_local_centro", joinColumns = @JoinColumn(name = "id_local"), inverseJoinColumns = @JoinColumn(name = "id_centro"))
    @EntityProperties(value = {"id", "codigo", "descricao", "statusCadastroSap", "statusRegistro"}, targetEntity = Centro.class)
    private List<Centro> centros = new ArrayList<>();

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "tb_local_hardware", joinColumns = @JoinColumn(name = "id_local"), inverseJoinColumns = @JoinColumn(name = "id_hardware"))
    @EntityProperties(value = {"id", "codigo", "codigoFormatado", "descricao", "endereco", "identificacao", "porta"}, targetEntity = Hardware.class)
    private List<Hardware> hardwares = new ArrayList<>();

    @Override
    public String toString() {

        return codigo + " - " + descricao;
    }

}
