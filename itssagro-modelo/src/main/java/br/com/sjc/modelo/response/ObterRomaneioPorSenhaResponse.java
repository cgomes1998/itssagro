package br.com.sjc.modelo.response;

import br.com.sjc.modelo.dto.CampoOperacaoDTO;
import br.com.sjc.modelo.sap.Romaneio;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
public class ObterRomaneioPorSenhaResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private Romaneio entidade;
    private List<CampoOperacaoDTO> campos;
}
