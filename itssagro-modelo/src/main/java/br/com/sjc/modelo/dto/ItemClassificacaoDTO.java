package br.com.sjc.modelo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ItemClassificacaoDTO implements Comparable<ItemClassificacaoDTO> {

    private String descricaoItemClassificacao;

    private String codigoItemClassificacao;

    @Override
    public int compareTo(ItemClassificacaoDTO o) {

        return new Long(this.getCodigoItemClassificacao()).compareTo(new Long(o.getCodigoItemClassificacao()));
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemClassificacaoDTO that = (ItemClassificacaoDTO) o;
        return descricaoItemClassificacao.equals(that.descricaoItemClassificacao) && codigoItemClassificacao.equals(that.codigoItemClassificacao);
    }

    @Override
    public int hashCode() {

        return Objects.hash(descricaoItemClassificacao, codigoItemClassificacao);
    }

}
