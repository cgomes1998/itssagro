package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.SenhaEtapa;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.util.ObjetoUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class SenhaEtapaRelatorioDTO  extends DataDTO {

    private String senha;

    private Long senhaId;

    private String ordemVenda;

    private String codigoMaterial;

    private String nomeMaterial;

    private String nomeCliente;

    private String numeroRomaneio;

    private String nomeMotorista;

    private String placaVeiculo;

    private Integer numeroCartaoAcesso;

    private EtapaEnum etapa;

    private Integer qtdRepeticao = 0;

    private Integer qtdChamada = 0;
    private Date horaEntrada;

    private Date horaSaida;

    private Long duracao;

    private String statusEtapa;

    public SenhaEtapaRelatorioDTO(
            Long id,
            String senha,
            Long senhaId,
            String ordemVenda,
            String codigoMaterial,
            String nomeMaterial,
            String nomeCliente,
            String nomeMotorista,
            String placaVeiculo,
            Integer numeroCartaoAcesso,
            EtapaEnum etapa,
            Integer qtdRepeticao,
            Integer qtdChamada,
            Date horaEntrada,
            Date horaSaida,
            String statusEtapa
    ) {
        setId(id);
        setSenha(senha);
        setSenhaId(senhaId);
        setOrdemVenda(ordemVenda);
        setCodigoMaterial(codigoMaterial);
        setNomeMaterial(nomeMaterial);
        setNomeCliente(nomeCliente);
        setNomeMotorista(nomeMotorista);
        setPlacaVeiculo(placaVeiculo);
        setNumeroCartaoAcesso(numeroCartaoAcesso);
        setEtapa(etapa);
        setQtdRepeticao(qtdRepeticao);
        setQtdChamada(qtdChamada);
        setHoraEntrada(horaEntrada);
        setHoraSaida(horaSaida);
        setStatusEtapa(statusEtapa);

        if(ObjetoUtil.isNotNull(this.getHoraSaida()) && this.getHoraSaida().getTime() > this.getHoraEntrada().getTime()){
            setDuracao(this.getHoraSaida().getTime() - this.getHoraEntrada().getTime());
        } else {
            setDuracao((new Date().getTime() - this.horaEntrada.getTime()));
        }
    }

    public String getDataEntradaFormatada(){
        return this.getHoraEntrada() != null ? new SimpleDateFormat("dd/MM/yyyy").format(this.getHoraEntrada()) : "";
    }

    public String getHoraEntradaFormatada(){
        return this.getHoraEntrada() != null ? new SimpleDateFormat("HH:mm").format(this.getHoraEntrada()) : "";
    }

    public String getDataSaidaFormatada(){
        return this.getHoraSaida() != null ? new SimpleDateFormat("dd/MM/yyyy").format(this.getHoraSaida()) : "";
    }

    public String getHoraSaidaFormatada(){
        return this.getHoraSaida() != null ? new SimpleDateFormat("HH:mm").format(this.getHoraSaida()) : "";
    }

    public String getTempoEsperaFormatado(){
        // formula for conversion for
        // milliseconds to minutes.
        long minutes = (this.getDuracao() / 1000) / 60;

        // formula for conversion for
        // milliseconds to seconds
        long seconds = (this.getDuracao() / 1000) % 60;

        return String.format("%02d", minutes) + ":" + String.format("%02d", seconds);
    }
}
