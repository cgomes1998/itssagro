package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.cfg.Portaria;
import br.com.sjc.modelo.cfg.PortariaMotivo;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PortariaMotivoDTO extends DataDTO {
    @ProjectionProperty
    private String descricao;

    @ProjectionProperty
    private Boolean ativo;

    public PortariaMotivoDTO(PortariaMotivo motivo){
        this.setId(motivo.getId());
        this.setDescricao(motivo.getDescricao());
        this.setAtivo(motivo.isAtivo());
    }
}
