package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.EntidadeAutenticada;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public class EntidadeSAPPre extends EntidadeAutenticada {

    @Column(name = "codigo", unique = true)
    private String codigo;

    @Column(name = "log_sap")
    private String logSAP;

    @Column(name = "request_key")
    private String requestKey;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_cadastro_sap")
    private StatusCadastroSAPEnum statusCadastroSap;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_registro_sap")
    private StatusRegistroEnum statusRegistro;
}
