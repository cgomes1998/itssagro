package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.enums.*;
import br.com.sjc.modelo.sap.Cliente;
import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

import java.text.MessageFormat;
import java.util.Date;

/**
 * Created by julio.bueno on 27/06/2019.
 */
@Getter
@Setter
@SAPItem
public class RfcClienteResponseItem extends JCoItem {

    @SAPColumn(name = "KUNNR")
    private String codigo;

    @SAPColumn(name = "NAME1")
    private String nome;

    @SAPColumn(name = "NAME2")
    private String nomeDois;

    @SAPColumn(name = "STATUS")
    private String status;

    @SAPColumn(name = "STKZN")
    private String pessoaFisica;

    @SAPColumn(name = "STCD1")
    private String cnpj;

    @SAPColumn(name = "STCD2")
    private String cpf;

    @SAPColumn(name = "STCD3")
    private String inscEstadualOuCnh;

    @SAPColumn(name = "ERDAT")
    private Date dataCriacao;

    @SAPColumn(name = "ERNAM")
    private String ernam;

    @SAPColumn(name = "ADRNR")
    private String endereco;

    @SAPColumn(name = "REGIO")
    private String regiao;

    @SAPColumn(name = "POST_CODE1")
    private String cep;

    @SAPColumn(name = "CITY1")
    private String local;

    @SAPColumn(name = "CITY2")
    private String bairro;

    @SAPColumn(name = "STREET")
    private String rua;

    @SAPColumn(name = "HOUSE_NUM1")
    private String numero;

    @SAPColumn(name = "TELF1")
    private String celular;

    @SAPColumn(name = "TELFX")
    private String fixo;

    public Cliente copy(Cliente entidadeSaved) {

        if (ObjetoUtil.isNull(entidadeSaved)) {

            entidadeSaved = new Cliente();

            entidadeSaved.setDataCadastro(DateUtil.hoje());

        } else {

            entidadeSaved.setDataAlteracao(DateUtil.hoje());
        }

        entidadeSaved.setNome(StringUtil.isNotNullEmpty(this.getNome()) ? this.getNome().trim() : entidadeSaved.getNome());

        entidadeSaved.setCpfCnpj(StringUtil.isNotNullEmpty(this.getCpf()) ? this.getCpf() : this.getCnpj());

        entidadeSaved.setTipoPessoa(StringUtil.isNotNullEmpty(this.getCpf()) ? TipoPessoaEnum.PF : TipoPessoaEnum.PJ);

        entidadeSaved.setInscricaoEstadual(StringUtil.isNotNullEmpty(this.getInscEstadualOuCnh()) ? this.getInscEstadualOuCnh().trim() : entidadeSaved.getInscricaoEstadual());

        entidadeSaved.setCep(StringUtil.isNotNullEmpty(this.getCep()) ? this.getCep().trim() : entidadeSaved.getCep());

        entidadeSaved.setEndereco(StringUtil.isNotNullEmpty(this.getRua()) ? this.getRua().trim() : entidadeSaved.getEndereco());

        entidadeSaved.setNumero(StringUtil.isNotNullEmpty(this.getNumero()) ? this.getNumero().trim() : entidadeSaved.getNumero());

        entidadeSaved.setMunicipio(StringUtil.isNotNullEmpty(this.getLocal()) ? this.getLocal().trim() : entidadeSaved.getMunicipio());

        entidadeSaved.setCodigo(StringUtil.isNotNullEmpty(this.getCodigo()) ? new Long(this.getCodigo().trim()).toString() : entidadeSaved.getCodigo());

        entidadeSaved.setBairro(StringUtil.isNotNullEmpty(this.getBairro()) ? this.getBairro().trim() : entidadeSaved.getBairro());

        entidadeSaved.setUf(StringUtil.isNotNullEmpty(this.getRegiao()) ? UFEnum.valueOf(this.getRegiao()) : entidadeSaved.getUf());

        entidadeSaved.setLogSAP(MessageFormat.format("CLIENTE SINCRONIZADO COM O SAP EM {0}", DateUtil.format("dd/MM/yyyy HH:mm:ss", DateUtil.hoje())));

        if (StringUtil.isNotNullEmpty(this.getCodigo())) {

            entidadeSaved.setLogSAP(entidadeSaved.getLogSAP() + "\nCÓDIGO: " + this.getCodigo());
        }

        entidadeSaved.setStatusCadastroSap(StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO);

        entidadeSaved.setStatus(StatusEnum.ATIVO);

        entidadeSaved.setStatusRegistro(StatusRegistroEnum.ATIVO);

        return entidadeSaved;
    }
}
