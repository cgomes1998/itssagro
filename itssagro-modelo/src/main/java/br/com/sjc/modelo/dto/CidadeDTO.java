package br.com.sjc.modelo.dto;
import br.com.sjc.modelo.Cidade;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CidadeDTO extends DataDTO {

    private String nome;

    public CidadeDTO(final Cidade cidade) {

        setId(cidade.getId());

        setNome(cidade.getNome());

    }
}