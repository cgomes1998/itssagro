package br.com.sjc.modelo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusLocalCarregamentoPesoEnum {

    AGUARDANDO("Aguardando"),
    CARREGADO("Carregado");

    private String descricao;
}
