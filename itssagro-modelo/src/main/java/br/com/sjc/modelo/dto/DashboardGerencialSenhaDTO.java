package br.com.sjc.modelo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class DashboardGerencialSenhaDTO {

    private SenhaDTO senha;

    private RomaneioDTO romaneio;

    private long duracao;

}
