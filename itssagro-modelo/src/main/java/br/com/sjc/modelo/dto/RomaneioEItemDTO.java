package br.com.sjc.modelo.dto;

import lombok.Data;

@Data
public class RomaneioEItemDTO {

    private Long idItem;

    private Long idRomaneio;

    private boolean requestSalva;

    public RomaneioEItemDTO() {
    }

    public RomaneioEItemDTO(Long idItem, Long idRomaneio) {
        this.idItem = idItem;
        this.idRomaneio = idRomaneio;
    }
}
