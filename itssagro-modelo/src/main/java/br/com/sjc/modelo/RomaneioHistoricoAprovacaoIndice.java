package br.com.sjc.modelo;

import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_romaneio_historico_aprovacao_indice", schema = "sap")
@NoArgsConstructor
@AllArgsConstructor
public class RomaneioHistoricoAprovacaoIndice extends EntidadeGenerica {

    @EntityProperties(value = {"id", "nome", "login", "cpf"})
    @ManyToOne
    @JoinColumn(name = "id_aprovador", referencedColumnName = "id")
    private Usuario aprovador;

    @EntityProperties(value = {"id"})
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_romaneio", referencedColumnName = "id")
    private Romaneio romaneio;

}
