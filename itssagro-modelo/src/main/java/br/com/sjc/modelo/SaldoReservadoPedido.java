package br.com.sjc.modelo;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@ToString
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "tb_saldo_reservado", schema = "sap", indexes = {
        @Index(name = "idx_tb_saldo_reservado", columnList = "nr_romaneio,numero_pedido,item_pedido")
})
public class SaldoReservadoPedido extends EntidadeGenerica {
    private static final long serialVersionUID = 1L;

    @Column(name = "nr_romaneio", length = 12)
    private String numeroRomaneioConsumidor;

    @Column(name = "numero_pedido", length = 12)
    private String numeroPedido;

    @Column(name = "item_pedido", length = 4)
    private String itemPedido;

    @Column(name = "saldo_reservado", columnDefinition = "numeric(19,6) default 0")
    private BigDecimal saldoReservado;
}
