package br.com.sjc.modelo.endpoint.request.item;

import br.com.sjc.modelo.Usuario;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RomaneioRequestItem {

    private String armazemCnpj;

    private String armazemNome;

    private String dtRecebimentoMercadoria;

    private String usuarioIntegracao;

    private String codigoCentro;

    private String codigoTicketOrigem;

    private String observacao;

    private String observacacaoNota;

    private PesagemRequest pesagem;

    private List<ItemRequest> itens;

    private List<ClassificacaoRequest> classificacaoDestino;

    private List<ClassificacaoRequest> classificacaoOrigem;

    public RomaneioArmazemTerceiros toRomaneioArmazemTerceiros() {

        final RomaneioArmazemTerceiros entidade = new RomaneioArmazemTerceiros();

        entidade.setTicketOrigem(this.codigoTicketOrigem);

        entidade.setCnpjArmazem(StringUtil.isNotNullEmpty(this.armazemCnpj) ? StringUtil.removerCaracteresEspeciais(this.armazemCnpj) : null);

        entidade.setNomeArmazem(this.armazemNome);

        entidade.setDataEntradaArmazem(StringUtil.isNotNullEmpty(this.dtRecebimentoMercadoria) ? DateUtil.format(this.dtRecebimentoMercadoria, "dd/MM/yyyy HH:mm:ss") : null);

        return entidade;
    }

    public Romaneio toEntidade(Usuario usuarioArmazem, Centro centro, DadosSincronizacao operacao, Veiculo placaCavalo, Safra safra) {

        final Romaneio entidade = new Romaneio();

        entidade.setIdUsuarioAutorCadastro(ObjetoUtil.isNotNull(usuarioArmazem) ? usuarioArmazem.getId() : null);

        entidade.setDataCadastro(DateUtil.hoje());

        entidade.setOperacao(operacao);

        entidade.setCentro(centro);

        entidade.setObservacao(this.observacao);

        entidade.setObservacaoNota(this.observacacaoNota);

        entidade.setRomaneioArmazemTerceiros(this.toRomaneioArmazemTerceiros());

        entidade.setPesagem(this.getPesagem().toEntidade(placaCavalo, safra, usuarioArmazem));

        entidade.setClassificacao(ClassificacaoRequest.toClassificacao(placaCavalo, safra, usuarioArmazem, this.getClassificacaoDestino()));

        entidade.setItensClassificacaoOrigem(new ArrayList(this.getClassificacaoOrigem().stream().map(ClassificacaoRequest::toItensClassificacao).collect(Collectors.toSet())));

        entidade.setPossuiDadosOrigem(ColecaoUtil.isNotEmpty(entidade.getItensClassificacaoOrigem()));

        return entidade;
    }
}
