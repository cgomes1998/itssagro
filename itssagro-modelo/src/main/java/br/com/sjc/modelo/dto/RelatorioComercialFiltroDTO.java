package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.sap.Cliente;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RelatorioComercialFiltroDTO {

    private String codigoOrdemVenda;

    private Material material;

    private List<Material> materiais;

    private Date dataInicio;

    private Date dataFim;

    private Cliente cliente;

    public boolean isNotNull() {

        return StringUtil.isNotNullEmpty(this.getCodigoOrdemVenda()) || ObjetoUtil.isNotNull(this.getMaterial()) || ColecaoUtil.isNotEmpty(this.getMateriais()) || ObjetoUtil.isNotNull(this.getDataInicio()) || ObjetoUtil.isNotNull(this.getDataFim()) || ObjetoUtil.isNotNull(this.getCliente());
    }

}
