package br.com.sjc.modelo;

import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(name = "tb_perfil_operacao")
public class PerfilOperacao implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @EntityProperties(value = {"id", "entidade", "descricao", "modulo", "direcaoOperacao", "operacao", "eventoEntrada", "eventoSaida", "possuiDivisaoCarga", "possuiPesagem", "possuiClassificacao", "pesagemManual", "aplicaClassificacao",
            "simplesPesagem", "possuiConsultaSaldoDisponivel", "possuiBuscaNFeTransporte", "possuiClassificacaoOrigem", "possuiConsultaContrato", "possuiDadosNfe", "possuiRetornoNfeSAP", "possuiRetornoNfeTransporteSAP",
            "possuiValidacaoEntreVlrUniNotaEPedido", "toleranciaEntreVlrUniNotaEPedido", "possuiValidacaoParaEscriturarNota", "possuiImpressaoPorEtapa", "possuiValidacaoChaveAcesso", "possuiRateio", "possuiValidacaoDivergenciaProdutorNotaEPedido",
            "possuiValidacaoChaveEFornecedor", "utilizaObterDadosNfe", "utilizaObterDadosCte", "enviarEmailPesagemManual", "gestaoPatio", "criarRemessa", "criarDocumentoTransporte", "possuiValidacaoConversaoLitragem", "umidadeInformadaManualmente", "requestCount", "tempoPreCadastro",
            "ultimaSincronizacao"})
    @ManyToOne
    @JoinColumn(name = "id_operacao", referencedColumnName = "id")
    private DadosSincronizacao operacao;

    @EntityProperties(value = {"id"})
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_perfil")
    private Perfil perfil;

    @Transient
    private boolean operacaoAtribuidaAoPerfil;

}
