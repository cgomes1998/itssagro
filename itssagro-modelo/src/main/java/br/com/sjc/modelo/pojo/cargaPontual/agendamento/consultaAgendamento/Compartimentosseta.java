package br.com.sjc.modelo.pojo.cargaPontual.agendamento.consultaAgendamento;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Compartimentosseta {

    private PlacaCavalo placaCavalo;

    private Placa1semi placa1semi;

    private Placa2semi placa2semi;

    private Placa3semi placa3semi;

}
