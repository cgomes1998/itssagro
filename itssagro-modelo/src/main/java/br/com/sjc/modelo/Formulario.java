package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.FormularioStatus;
import br.com.sjc.util.anotation.EntityProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by julio.bueno on 04/07/2019.
 */
@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_formulario")
@Proxy(lazy = false)
public class Formulario extends EntidadeAutenticada {

    @Column
    private String descricao;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private FormularioStatus formularioStatus;

    @Column
    private Boolean cabecalhoDadosMotorista = false;

    @Column
    private Boolean cabecalhoDadosVeiculo = false;

    @Column
    private Boolean cabecalhoDadosCliente = false;

    @Column
    private Boolean cabecalhoDadosTransportadora = false;

    @EntityProperties(value = {"id", "formulario.id", "titulo", "required", "ordem", "tamanho", "valorEsperado", "tipo"}, collecions = {"opcoes"}, targetEntity = FormularioItem.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "formulario", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    private List<FormularioItem> itens = new ArrayList<>();

    public void addItem(FormularioItem item) {

        itens.add(item);
    }

    public List<FormularioItem> getItens() {

        itens.sort((item_1, item_2) -> item_1.getOrdem().compareTo(item_2.getOrdem()));

        return itens;
    }

}
