package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusLacreEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LacreCountDTO {

    private StatusLacreEnum statusLacre;

    private Long total;
}
