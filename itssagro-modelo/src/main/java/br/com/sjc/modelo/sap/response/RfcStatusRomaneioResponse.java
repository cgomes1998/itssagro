package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.response.item.RfcStatusNfeItem;
import br.com.sjc.modelo.sap.response.item.RfcStatusPedidoResponseItem;
import br.com.sjc.modelo.sap.response.item.RfcStatusRomaneioItem;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@SAPParameter(function = "ZITSSAGRO_STATUSNF_RFC")
public class RfcStatusRomaneioResponse extends JCoResponse {

    @SAPTable(name = "T_ACTIVE", item = RfcStatusNfeItem.class)
    private List<RfcStatusNfeItem> tActive;

    @SAPTable(name = "T_STATUS", item = RfcStatusRomaneioItem.class)
    private List<RfcStatusRomaneioItem> tStatus;

    @SAPTable(name = "T_STATUS_PED", item = RfcStatusPedidoResponseItem.class)
    private List<RfcStatusPedidoResponseItem> tStatusPed;
}
