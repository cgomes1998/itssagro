package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.response.item.RfcOrdemVendaCabecalhoSaidaResponse;
import br.com.sjc.modelo.sap.response.item.RfcOrdemVendaDadosGeraisResponse;
import br.com.sjc.modelo.sap.response.item.RfcOrdemVendaItemSaidaResponse;
import br.com.sjc.modelo.sap.response.item.RfcOrdemVendaParceiroSaidaResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@SAPParameter( function = "BAPISDORDER_GETDETAILEDLIST" )
public class RfcOrdemVendaResponse extends JCoResponse {

    @SAPTable( name = "ORDER_HEADERS_OUT", item = RfcOrdemVendaCabecalhoSaidaResponse.class )
    private List<RfcOrdemVendaCabecalhoSaidaResponse> cabecalhos;

    @SAPTable( name = "ORDER_ITEMS_OUT", item = RfcOrdemVendaItemSaidaResponse.class )
    private List<RfcOrdemVendaItemSaidaResponse> itens;

    @SAPTable( name = "ORDER_PARTNERS_OUT", item = RfcOrdemVendaParceiroSaidaResponse.class )
    private List<RfcOrdemVendaParceiroSaidaResponse> parceiros;

    @SAPTable( name = "ORDER_BUSINESS_OUT", item = RfcOrdemVendaDadosGeraisResponse.class )
    private List<RfcOrdemVendaDadosGeraisResponse> dadosGerais;

}
