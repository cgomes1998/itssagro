package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.sap.Material;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Objects;

/**
 * Created by julio.bueno on 06/02/2020
 */
@Getter
@Setter
public class RelatorioAnaliseConversaoFiltroDTO {

    private Material material;

    private Date dataInicio;
    private Date dataFim;

    public boolean isNotNull () {
        return !Objects.isNull(material) || !Objects.isNull(dataInicio) || !Objects.isNull(dataFim);
    }

}
