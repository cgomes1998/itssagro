package br.com.sjc.modelo.enums;

import lombok.Getter;

@Getter
public enum FluxoPesagemOperacaoEnum {

    PESO_BRUTO("1ª Pesagem deve ser o bruto"),

    PESO_TARA("1ª Pesagem deve ser o tara"),

    SOMENTE_BRUTO("Somente pesagem bruto"),

    SEM_FLUXO("Sem Fluxo definido");

    private String descricao;

    private FluxoPesagemOperacaoEnum(final String descricao) {

        this.descricao = descricao;
    }


}
