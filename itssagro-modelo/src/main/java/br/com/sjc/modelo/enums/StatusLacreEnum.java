package br.com.sjc.modelo.enums;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum StatusLacreEnum {

    UTILIZADO("Utilizado"),

    DISPONIVEL("Disponível"),

    VINCULADO("Vinculado"),

    EXTRAVIADO("Extraviado"),

    DEVOLVIDO("Devolvido"),

    INATIVO("Inativo"),

    INUTILIZADO("Inutilizado");

    private String descricao;

    StatusLacreEnum(final String descricao) {
        this.descricao = descricao;
    }

    public static boolean valido(String status) {
        return Arrays.stream(StatusLacreEnum.values()).anyMatch(statusLacreEnum -> statusLacreEnum.name().equals(status));
    }
}
