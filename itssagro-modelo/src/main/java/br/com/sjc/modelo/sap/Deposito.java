package br.com.sjc.modelo.sap;

import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_deposito", schema = "sap")
public class Deposito extends EntidadeSAPSync {

    @Column
    private String descricao;

    @EntityProperties(value = {"id", "codigo", "descricao"})
    @JsonIgnore
    @ManyToOne
    private Centro centro;

    @Override
    public String toString() {

        return this.getCodigo() + " - " + this.descricao;
    }

    public String getStatusSapDescricao() {

        return this.getStatusCadastroSap().getDescricao();
    }

    public String getStatusRegistroDescricao() {

        return this.getStatusRegistro().getDescricao();
    }

}
