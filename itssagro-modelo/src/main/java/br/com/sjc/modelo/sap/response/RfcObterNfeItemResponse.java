package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SAPParameter(function = "/ITSSAGRO/XML_NFE_TCNH_T_HD")
public class RfcObterNfeItemResponse extends JCoItem {

    @SAPColumn(name = "NITEM")
    private String numeroItem;

    @SAPColumn(name = "VUNCOM")
    private BigDecimal vuncom;

    @SAPColumn(name = "QCOM")
    private BigDecimal qcom;

}
