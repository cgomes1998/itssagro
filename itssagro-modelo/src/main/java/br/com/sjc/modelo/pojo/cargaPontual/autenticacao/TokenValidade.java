package br.com.sjc.modelo.pojo.cargaPontual.autenticacao;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TokenValidade {

    private String token;

    private boolean valido;

}
