package br.com.sjc.modelo.sap.response;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoResponse;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.response.item.RfcCondicoesPagamentoItem;
import br.com.sjc.modelo.sap.response.item.RfcGruposCompradoresItem;
import br.com.sjc.modelo.sap.response.item.RfcOrganizacoesCompraItem;
import br.com.sjc.modelo.sap.response.item.RfcTiposContratoItem;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@SAPParameter(function = "/ITSSAGRO/DADOS_CONTRATO")
public class RfcDadosContratoResponse extends JCoResponse {

    @SAPTable(name = "T_TPCONTRATOS", item = RfcTiposContratoItem.class)
    private List<RfcTiposContratoItem> tiposContrato;

    @SAPTable(name = "T_COND_PAGTO", item = RfcCondicoesPagamentoItem.class)
    private List<RfcCondicoesPagamentoItem> condicoesPagamento;

    @SAPTable(name = "T_ORG_COMPRAS", item = RfcOrganizacoesCompraItem.class)
    private List<RfcOrganizacoesCompraItem> organizacoesCompra;

    @SAPTable(name = "T_GRP_COMPRAS", item = RfcGruposCompradoresItem.class)
    private List<RfcGruposCompradoresItem> gruposComrpadores;
}
