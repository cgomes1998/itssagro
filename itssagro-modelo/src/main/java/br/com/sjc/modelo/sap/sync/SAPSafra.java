package br.com.sjc.modelo.sap.sync;

import br.com.sjc.modelo.sap.Safra;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SAPSafra extends SAPEntidade {

    @SAPColumn(name = "C_COD_SAFRA")
    private String codigo;

    @SAPColumn(name = "C_DES_SAFRA")
    private String descricao;

    @SAPColumn(name = "C_STATUS_SAFRA")
    private String status;

    public enum COLUMN_INPUT {

        DT_ULT_INF;
    }

    public static SAPSafra newInstance() {

        return new SAPSafra();
    }

    public Safra copy(Safra entidadeSaved) {

        if (ObjetoUtil.isNull(entidadeSaved)) {

            entidadeSaved = new Safra();

        } else {

            entidadeSaved.setId(entidadeSaved.getId());
        }

        entidadeSaved.setCodigo(StringUtil.isNotNullEmpty(this.getCodigo()) ? this.getCodigo().trim() : entidadeSaved.getCodigo());

        entidadeSaved.setDescricao(StringUtil.isNotNullEmpty(this.getDescricao()) ? this.getDescricao().trim() : entidadeSaved.getDescricao());

        entidadeSaved.setStatus(StringUtil.isNotNullEmpty(this.getStatus()) ? StatusEnum.getCodigoSAP(this.getStatus()) : null);

        entidadeSaved.setStatusRegistro(StatusRegistroEnum.ATIVO);

        entidadeSaved.setStatusCadastroSap(StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO);

        return entidadeSaved;
    }
}
