package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.MotivoTipoEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by julio.bueno on 24/06/2019.
 */
@Getter
@Setter
@NoArgsConstructor
public class MotivoDTO extends DataDTO {

    @ProjectionProperty
    private String descricao;

    @ProjectionProperty
    private MotivoTipoEnum tipo;

    @ProjectionProperty
    private StatusEnum status;

}
