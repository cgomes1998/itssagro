package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_fluxo_etapa")
public class FluxoEtapa extends EntidadeAutenticada {

    @EntityProperties(value = {"id", "codigo", "descricao", "tipoFluxo"})
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_fluxo", nullable = false)
    @JsonIgnore
    private Fluxo fluxo;

    @Column
    private Long sequencia;

    @Column(name = "status_estorno_automacao")
    private Integer statusEstornoAutomacao;

    @Column(name = "status_carga_pontual_destino_sucesso")
    private Integer statusCargaPontualDestinoSucesso;

    @Column(name = "status_carga_pontual_destino_estorno")
    private Integer statusCargaPontualDestinoEstorno;

    @Column(name = "mensagem_estorno_automacao")
    private String mensagemEstornoAutomacao;

    @Enumerated(EnumType.STRING)
    @Column
    private EtapaEnum etapa;

    @Column(name = "medir_tempo")
    private boolean medirTempo = false;

    @Column(name = "etapa_final")
    private boolean etapaFinal = false;

    @Column(name = "avanco_automatico")
    private boolean avancoAutomatico = false;

    @EntityProperties(value = {"id", "descricao", "formularioStatus", "cabecalhoDadosMotorista", "cabecalhoDadosVeiculo", "cabecalhoDadosCliente"}, collecions = "itens")
    @ManyToOne
    @JoinColumn(name = "id_formulario")
    private Formulario formulario;

    @EntityProperties(value = {"id", "codigo", "codigoFormatado", "descricao"})
    @ManyToOne
    @JoinColumn(name = "id_local")
    private Local local;

    @EntityProperties(value = {"id", "codigo", "codigoFormatado", "descricao", "endereco", "identificacao", "porta"})
    @ManyToOne
    @JoinColumn(name = "id_hardware")
    private Hardware hardware;

    @EntityProperties(value = {"id", "email", "numeroCriterio", "repetirEtapa", "tipo", "fluxoEtapa.id", "proximaEtapa.id", "proximaEtapa.etapa"}, collecions = {"condicoes", "acoes", "eventos"}, targetEntity = EtapaCriterio.class)
    @OrderBy("id")
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "fluxoEtapa", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<EtapaCriterio> criterios = new ArrayList<>();

}
