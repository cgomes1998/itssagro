package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by julio.bueno on 03/07/2019.
 */
@Getter
@Setter
@NoArgsConstructor
public class DocumentoDTO extends DataDTO {

    @ProjectionProperty
    private String codigo;

    @ProjectionProperty
    private String descricao;

    @ProjectionProperty
    private StatusEnum status;

}
