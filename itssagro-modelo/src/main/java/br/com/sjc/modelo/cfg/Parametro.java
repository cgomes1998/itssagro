package br.com.sjc.modelo.cfg;

import br.com.sjc.modelo.EntidadeGenerica;
import br.com.sjc.modelo.enums.TipoParametroEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_parametro", schema = "cfg")
public class Parametro extends EntidadeGenerica {

    private static final long serialVersionUID = 1L;

    @Column(name = "parametro")
    @Enumerated(EnumType.STRING)
    private TipoParametroEnum tipoParametro;

    @Column(name = "valor")
    private String valor;

}
