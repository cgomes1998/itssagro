package br.com.sjc.modelo.sap.response.item;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoItem;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPItem;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SAPItem
public class TableLogReturn extends JCoItem {

    @SAPColumn(name = "FLOWD")
    private String etapa;

    @SAPColumn(name = "MESSAGE")
    private String mensagem;

    @SAPColumn(name = "LOGTY")
    private String tipoMensagem;

    @SAPColumn(name = "MODLO")
    private String modulo;

    @SAPColumn(name = "OPERA")
    private String operacao;

    @SAPColumn(name = "CHAVE")
    private String chave;
}
