package br.com.sjc.modelo.sap;

import br.com.sjc.modelo.EntidadeAutenticada;
import br.com.sjc.modelo.enums.StatusLocalCarregamentoPesoEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_local_carregamento", schema = "sap")
public class LocalCarregamento extends EntidadeAutenticada {

    @Column
    private Long identificador;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_local_carregamento")
    private StatusLocalCarregamentoPesoEnum statusLocalCarregamento = StatusLocalCarregamentoPesoEnum.AGUARDANDO;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_inicio_carregamento")
    private Date dataInicioDoCarregamento;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_fim_carregamento")
    private Date dataFimDoCarregamento;

}
