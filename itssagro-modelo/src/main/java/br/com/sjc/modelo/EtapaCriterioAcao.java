package br.com.sjc.modelo;

import br.com.sjc.modelo.enums.EtapaCampoEnum;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.util.anotation.EntityProperties;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tb_etapa_criterio_acao")
public class EtapaCriterioAcao extends EntidadeAutenticada {

    @EntityProperties(value = {"id", "email", "numeroCriterio", "repetirEtapa", "tipo", "fluxoEtapa.id", "fluxoEtapa.sequencia", "fluxoEtapa.etapa", "proximaEtapa.id", "proximaEtapa.sequencia", "proximaEtapa.etapa"}, collecions = {
            "condicoes", "acoes", "eventos"})
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_etapa_criterio", nullable = false)
    @JsonIgnore
    private EtapaCriterio etapaCriterio;

    private Integer numero;

    @Enumerated(EnumType.STRING)
    private EtapaEnum etapa;

    @Enumerated(EnumType.STRING)
    private EtapaCampoEnum campo;

    private String valor;

}
