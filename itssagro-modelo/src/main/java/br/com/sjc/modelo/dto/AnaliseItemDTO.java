package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.Analise;
import br.com.sjc.modelo.AnaliseItem;
import br.com.sjc.modelo.enums.StatusEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AnaliseItemDTO extends DataDTO {

    private String caracteristica;

    public AnaliseItemDTO(final AnaliseItem entidade) {
        setId(entidade.getId());
        setStatus(entidade.getStatus());
    }

}
