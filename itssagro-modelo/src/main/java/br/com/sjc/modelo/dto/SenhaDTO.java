package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.FluxoEtapa;
import br.com.sjc.modelo.Senha;
import br.com.sjc.modelo.SenhaEtapa;
import br.com.sjc.modelo.TipoFluxoEnum;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.enums.StatusSenhaEnum;
import br.com.sjc.util.ObjetoUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SenhaDTO extends DataDTO {

    private Date dataAlteracao;

    private String senha;

    private MaterialDTO material;

    private String veiculoPlaca;

    private String motoristaNome;

    private String clienteNome;

    private String clienteCpfCnpj;

    private String botaoAcionadoAguardandoChamadaCarregamento;

    private StatusSenhaEnum statusSenha;

    private String statusSenhaDescricao;

    private Long idUsuarioAutorCadastro;

    private SenhaEtapaDTO etapaAtual;

    private EtapaEnum etapa;

    private Long totalDuracao;

    private TipoFluxoEnum tipoFluxo;

    public SenhaDTO(Senha senha) {
        setId(senha.getId());
        setStatus(senha.getStatus());
        setDataAlteracao(senha.getDataAlteracao());
        setSenha(senha.getSenha());
        setMaterial(new MaterialDTO(senha.getMaterial()));
        setDataCadastro(senha.getDataCadastro());
        setVeiculoPlaca(ObjetoUtil.isNotNull(senha.getVeiculo()) ? senha.getVeiculo().getPlaca1() : "");
        setMotoristaNome(ObjetoUtil.isNotNull(senha.getMotorista()) ? senha.getMotorista().getNome() : "");
        setClienteNome(ObjetoUtil.isNotNull(senha.getCliente()) ? senha.getCliente().getNome() : "");
        setClienteCpfCnpj(ObjetoUtil.isNotNull(senha.getCliente()) ? senha.getCliente().getCpfCnpj() : "");
        setStatusSenha(senha.getStatusSenha());
        setStatusSenhaDescricao(senha.getStatusSenha().getDescricao());
        setIdUsuarioAutorCadastro(senha.getIdUsuarioAutorCadastro());
        setEtapaAtual(ObjetoUtil.isNotNull(senha.getEtapaAtual()) ? new SenhaEtapaDTO(senha.getEtapaAtual()) : null);
        setTipoFluxo(senha.getTipoFluxo());
    }

    public SenhaDTO(
            Long id,
            StatusEnum status,
            Date dataAlteracao,
            String senha,
            Long idMaterial,
            String codigoMaterial,
            String descricaoMaterial,
            Date dataCadastro,
            String veiculoPlaca,
            String motoristaNome,
            String clienteNome,
            String clienteCpfCnpj,
            StatusSenhaEnum statusSenha,
            Long idUsuarioAutorCadastro,
            Long idFluxoEtapa,
            EtapaEnum etapa,
            Long sequenciaFluxoEtapa,
            Integer etapaQtdRepeticao,
            Integer etapaQtdChamada,
            Date etapaHoraEntrada,
            Date etapaHoraSaida,
            String etapaStatus
    ) {

        setId(id);

        setStatus(status);

        setDataAlteracao(dataAlteracao);

        setSenha(senha);

        setMaterial(new MaterialDTO(
                idMaterial,
                codigoMaterial,
                descricaoMaterial
        ));

        setDataCadastro(dataCadastro);

        setVeiculoPlaca(veiculoPlaca);

        setMotoristaNome(motoristaNome);

        setClienteNome(clienteNome);

        setClienteCpfCnpj(clienteCpfCnpj);

        setStatusSenha(statusSenha);

        if (ObjetoUtil.isNotNull(statusSenha)) {

            setStatusSenhaDescricao(statusSenha.getDescricao());
        }

        setIdUsuarioAutorCadastro(idUsuarioAutorCadastro);

        if (ObjetoUtil.isNotNull(idFluxoEtapa)) {

            FluxoEtapa fluxoEtapa = new FluxoEtapa();

            fluxoEtapa.setId(idFluxoEtapa);

            fluxoEtapa.setEtapa(etapa);

            fluxoEtapa.setSequencia(sequenciaFluxoEtapa);

            setEtapaAtual(new SenhaEtapaDTO(getSenhaEtapa(fluxoEtapa, etapaQtdRepeticao, etapaQtdChamada, etapaHoraEntrada, etapaHoraSaida, etapaStatus)));
        }
    }

    public SenhaDTO(
            Long id,
            StatusEnum status,
            Date dataAlteracao,
            String senha,
            Long idMaterial,
            String codigoMaterial,
            String descricaoMaterial,
            Date dataCadastro,
            String veiculoPlaca,
            String motoristaNome,
            String clienteNome,
            String clienteCpfCnpj,
            StatusSenhaEnum statusSenha,
            Long idUsuarioAutorCadastro,
            Long idFluxoEtapa,
            EtapaEnum etapa,
            Long sequenciaFluxoEtapa,
            Integer etapaQtdRepeticao,
            Integer etapaQtdChamada,
            Date etapaHoraEntrada,
            Date etapaHoraSaida,
            String etapaStatus,
            Long totalDuracao
    ) {

        setId(id);

        setStatus(status);

        setDataAlteracao(dataAlteracao);

        setSenha(senha);

        setMaterial(new MaterialDTO(
                idMaterial,
                codigoMaterial,
                descricaoMaterial
        ));

        setDataCadastro(dataCadastro);

        setVeiculoPlaca(veiculoPlaca);

        setMotoristaNome(motoristaNome);

        setClienteNome(clienteNome);

        setClienteCpfCnpj(clienteCpfCnpj);

        setStatusSenha(statusSenha);

        if (ObjetoUtil.isNotNull(statusSenha)) {

            setStatusSenhaDescricao(statusSenha.getDescricao());
        }

        setIdUsuarioAutorCadastro(idUsuarioAutorCadastro);

        if (ObjetoUtil.isNotNull(idFluxoEtapa)) {

            FluxoEtapa fluxoEtapa = new FluxoEtapa();

            fluxoEtapa.setId(idFluxoEtapa);

            fluxoEtapa.setEtapa(etapa);

            fluxoEtapa.setSequencia(sequenciaFluxoEtapa);

            setEtapaAtual(new SenhaEtapaDTO(getSenhaEtapa(fluxoEtapa, etapaQtdRepeticao, etapaQtdChamada, etapaHoraEntrada, etapaHoraSaida, etapaStatus)));
        }

        setTotalDuracao(totalDuracao);
    }

    public SenhaEtapa getSenhaEtapa(FluxoEtapa senhaEtapa, Integer etapaQtdRepeticao, Integer etapaQtdChamada, Date etapaHoraEntrada, Date etapaHoraSaida, String etapaStatus) {

        SenhaEtapa entidadeSenhaEtapa = new SenhaEtapa();

        entidadeSenhaEtapa.setEtapa(senhaEtapa);

        entidadeSenhaEtapa.setQtdRepeticao(etapaQtdRepeticao);

        entidadeSenhaEtapa.setQtdChamada(etapaQtdChamada);

        entidadeSenhaEtapa.setHoraEntrada(etapaHoraEntrada);

        entidadeSenhaEtapa.setHoraSaida(etapaHoraSaida);

        entidadeSenhaEtapa.setStatusEtapa(etapaStatus);

        return entidadeSenhaEtapa;
    }
}
