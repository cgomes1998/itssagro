package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.enums.ModuloOperacaoEnum;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DadosSincronizacaoDTO extends DataDTO {

    @ProjectionProperty
    private String operacao;

    @ProjectionProperty
    private String descricao;

    @ProjectionProperty
    private ModuloOperacaoEnum modulo;

    public String getModuloDescricao() {

        return this.getModulo().getDescricao();
    }

}
