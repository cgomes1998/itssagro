package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPStructure;
import br.com.sjc.modelo.sap.request.item.RfcEstornarRemessaHeaderControlRequest;
import br.com.sjc.modelo.sap.request.item.RfcEstornarRemessaHeaderDataRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SAPParameter( function = "ZITSSAGRO_OUTB_DELIVERY_CHANGE" )
public class RfcEstornarRemessaRequest extends JCoRequest {

    @SAPStructure( name = "HEADER_DATA", item = RfcEstornarRemessaHeaderDataRequest.class )
    private RfcEstornarRemessaHeaderDataRequest headerData;

    @SAPStructure( name = "HEADER_CONTROL", item = RfcEstornarRemessaHeaderControlRequest.class )
    private RfcEstornarRemessaHeaderControlRequest headerControl;

    @Override
    public int getStructureRows ( String name ) {

        return 1;
    }

    @Override
    public List<?> getStructureItens (
            String name
    ) {

        final Field[] fields = this.getClass ().getDeclaredFields ();

        return Arrays.asList ( fields ).stream ().filter ( f -> f.getAnnotation ( SAPStructure.class ).name ().equals ( name ) ).map ( f -> {
            try {
                return f.get ( this );
            } catch ( IllegalAccessException e ) {
                e.printStackTrace ();
            }
            return null;
        } ).collect ( Collectors.toList () );
    }
}
