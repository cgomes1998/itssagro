package br.com.sjc.modelo.sap.sync;

import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import br.com.sjc.modelo.sap.TipoVeiculo;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPColumn;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class SAPTipoVeiculo extends SAPEntidade {

    @SAPColumn( name = "C_TPVEI" )
    private String codigo;

    @SAPColumn( name = "C_DESC" )
    private String descricao;

    @SAPColumn( name = "C_PESO" )
    private String peso;

    @SAPColumn( name = "C_TIPOIE" )
    private String tipo;

    @SAPColumn( name = "C_EARTX" )
    private String textoTipo;

    @SAPColumn( name = "C_STATUS" )
    private String status;

    public static SAPTipoVeiculo newInstance () {

        return new SAPTipoVeiculo ();
    }

    public TipoVeiculo copy ( TipoVeiculo entidadeSaved ) {

        if ( ObjetoUtil.isNull ( entidadeSaved ) ) {

            entidadeSaved = new TipoVeiculo ();

        } else {

            entidadeSaved.setId ( entidadeSaved.getId () );
        }

        entidadeSaved.setCodigo ( StringUtil.isNotNullEmpty ( this.getCodigo () ) ? this.getCodigo ().trim () : entidadeSaved.getCodigo () );

        entidadeSaved.setDescricao ( StringUtil.isNotNullEmpty ( this.getDescricao () ) ? this.getDescricao ().trim () : entidadeSaved.getDescricao () );

        if ( ObjetoUtil.isNull ( entidadeSaved.getPeso () ) )
            entidadeSaved.setPeso ( StringUtil.isNotNullEmpty ( this.getPeso () ) ? new BigDecimal ( this.getPeso () ) : entidadeSaved.getPeso () );

        entidadeSaved.setTipo ( this.getTipo () );

        entidadeSaved.setTextoTipo ( this.getTextoTipo () );

        entidadeSaved.setStatusCadastroSap ( StatusCadastroSAPEnum.SINCRONIZADO_COM_SUCESSO );

        entidadeSaved.setStatusRegistro ( StatusRegistroEnum.ATIVO );

        entidadeSaved.setStatus ( StringUtil.isNotNullEmpty ( this.getStatus () ) ? StatusEnum.getCodigoSAP ( this.getStatus () ) : null );

        entidadeSaved.setDataCadastro ( DateUtil.hoje () );

        return entidadeSaved;
    }
}
