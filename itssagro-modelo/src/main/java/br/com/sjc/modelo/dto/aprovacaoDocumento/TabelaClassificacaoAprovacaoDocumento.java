package br.com.sjc.modelo.dto.aprovacaoDocumento;

import br.com.sjc.modelo.sap.ItensClassificacao;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class TabelaClassificacaoAprovacaoDocumento {

    private String descricaoItem;

    private BigDecimal indice;

    private Double limite;

    public TabelaClassificacaoAprovacaoDocumento() {

    }

    @Builder
    public TabelaClassificacaoAprovacaoDocumento(ItensClassificacao item) {

        this.descricaoItem = item.getDescricaoItem();

        this.indice = item.getIndice();

        this.limite = item.getLimite();
    }

}
