package br.com.sjc.modelo.dto;

import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusPedidoEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.anotation.ProjectionConfigurationDTO;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ProjectionConfigurationDTO
public class RomaneioLacreListagemDTO extends DataDTO {

    @ProjectionProperty
    private Date dataCadastro;

    @ProjectionProperty
    private String numeroRomaneio;

    @ProjectionProperty(values = {"id", "operacao", "entidade"})
    private DadosSincronizacao operacao;

    @ProjectionProperty(values = {"id", "placa1"})
    private Veiculo placaCavalo;

    @ProjectionProperty(values = {"id", "codigo", "descricao"})
    private Material material;

    @ProjectionProperty
    private StatusIntegracaoSAPEnum statusIntegracaoSAP;

    @ProjectionProperty
    private StatusRomaneioEnum statusRomaneio;

    private List<ItemNFPedidoListagemDTO> itens = new ArrayList<>();

    public Boolean getPossuiItens() {

        return ColecaoUtil.isNotEmpty(itens);
    }
}
