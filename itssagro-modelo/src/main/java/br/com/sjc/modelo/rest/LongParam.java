package br.com.sjc.modelo.rest;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class LongParam {

    private Long number = null;

    public LongParam(String value) throws WebApplicationException {

        try {

            if (value.isEmpty() || value.equals("null") || value.equals("undefined")) {
                return;
            }

            this.number = new Long(value);

        } catch (Exception e) {

            throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST).entity("Não foi possivel converter o valor para Long: " + e.getMessage()).build());
        }
    }

    public Long getNumber() {
        return this.number;
    }
}