package br.com.sjc.modelo.sap.request;

import br.com.sjc.modelo.sap.arquitetura.jco.JCoRequest;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPParameter;
import br.com.sjc.modelo.sap.arquitetura.jco.SAPTable;
import br.com.sjc.modelo.sap.request.item.RfcStatusRomaneioRequestItem;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@SAPParameter(function = "ZITSSAGRO_STATUSNF_RFC")
public class RfcStatusRomaneioRequest extends JCoRequest {

    @SAPTable(name = "T_ROMANEIOS", item = RfcStatusRomaneioRequestItem.class)
    private List<RfcStatusRomaneioRequestItem> romaneios;

    @Override
    public int getTableRows() {

        return this.getRomaneios().size();
    }

    @Override
    public List<RfcStatusRomaneioRequestItem> getTableItens() {

        return this.getRomaneios();
    }
}
