package br.com.sjc.modelo.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Arquivo {

    private byte[] buffer;

    private String fileName;
}
