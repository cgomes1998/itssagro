package br.com.sjc.modelo.dto.aprovacaoDocumento;

import br.com.sjc.modelo.sap.Motorista;
import br.com.sjc.modelo.sap.Produtor;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.Veiculo;
import br.com.sjc.util.anotation.ProjectionConfigurationDTO;
import br.com.sjc.util.anotation.ProjectionProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ProjectionConfigurationDTO
public class ItemNFPedidoAprovacaoDocumento {

    @ProjectionProperty(values = {"id", "numeroRomaneio"})
    private Romaneio romaneio;

    @ProjectionProperty(values = {"id", "placa1"})
    private Veiculo placaCavalo;

    @ProjectionProperty(values = {"id", "codigo", "nome", "cpf"})
    private Motorista motorista;

    @ProjectionProperty(values = {"id", "nome"})
    private Produtor produtor;

}
