package br.com.sjc.jobs.sap;

import br.com.sjc.fachada.service.SAPService;
import br.com.sjc.jobs.arquitetura.QuartzScheduler;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.sap.sync.SAPSync;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.StringUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@DisallowConcurrentExecution
public class JOBClienteSync extends QuartzScheduler implements Job {

    private static final Logger LOG = Logger.getLogger(JOBClienteSync.class.getName());

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        try {

            if (!this.isEnable(JOBClienteSync.class, context) || !SAPSync.ONLINE) {

                return;
            }

            this.getSapService(context).sincronizarRFC(DadosSincronizacaoEnum.CLIENTE, DateUtil.hoje());

            JOBClienteSync.LOG.info(StringUtil.log("JOB PARA SINCRONIZAR CLIENTES EXECUTADO..."));

        } catch (Exception e) {

            JOBClienteSync.LOG.info("ERRO: " + e.getMessage());
        }
    }

    public SAPService getSapService(JobExecutionContext context) {

        return (SAPService) context.getJobDetail().getJobDataMap().get("sapService");
    }
}
