package br.com.sjc.jobs.sap;

import br.com.sjc.fachada.service.AgendamentoCargaPontualService;
import br.com.sjc.fachada.service.SenhaService;
import br.com.sjc.jobs.arquitetura.QuartzScheduler;
import br.com.sjc.util.StringUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@DisallowConcurrentExecution
public class JOBCargaPontualSync extends QuartzScheduler implements Job {

    private static final Logger LOG = Logger.getLogger(JOBCargaPontualSync.class.getName());

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        try {

            if (!this.isEnable(JOBCargaPontualSync.class, context)) {
                return;
            }

            this.getService(context).gerarSenhaCargaPontual();

            JOBCargaPontualSync.LOG.info(StringUtil.log("JOB PARA SINCRONIZAR AGENDAMENTOS DO CARGA PONTUAL EXECUTADO..."));

        } catch (Exception e) {

            JOBCargaPontualSync.LOG.info("ERRO: " + e.getMessage());
        }
    }

    public AgendamentoCargaPontualService getService(JobExecutionContext context) {
        return (AgendamentoCargaPontualService) context.getJobDetail().getJobDataMap().get("agendamentoCargaPontualService");
    }
}
