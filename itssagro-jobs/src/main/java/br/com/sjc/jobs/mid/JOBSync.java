package br.com.sjc.jobs.mid;

import br.com.sjc.jobs.arquitetura.QuartzScheduler;
import br.com.sjc.jobs.automacao.JOBObterPesoBalanca;
import br.com.sjc.jobs.sap.*;
import br.com.sjc.util.StringUtil;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class JOBSync extends QuartzScheduler {

    private static final Logger LOG = Logger.getLogger(JOBSync.class.getName());

    private static final String JOBS_SYNC_SAP = "JOBS_SYNC_SAP";

    public void iniciarJobs() {

        try {

            final Scheduler scheduler = new StdSchedulerFactory().getScheduler();

            scheduler.start();

            scheduler.scheduleJob(this.createJob(JOBCentroSync.class, JOBSync.JOBS_SYNC_SAP), this.createTrigger(JOBCentroSync.class, JOBSync.JOBS_SYNC_SAP));

            scheduler.scheduleJob(this.createJob(JOBMaterialSync.class, JOBSync.JOBS_SYNC_SAP), this.createTrigger(JOBMaterialSync.class, JOBSync.JOBS_SYNC_SAP));

            scheduler.scheduleJob(this.createJob(JOBSafraSync.class, JOBSync.JOBS_SYNC_SAP), this.createTrigger(JOBSafraSync.class, JOBSync.JOBS_SYNC_SAP));

            scheduler.scheduleJob(this.createJob(JOBTransportadoraSync.class, JOBSync.JOBS_SYNC_SAP), this.createTrigger(JOBTransportadoraSync.class, JOBSync.JOBS_SYNC_SAP));

            scheduler.scheduleJob(this.createJob(JOBProdutorSync.class, JOBSync.JOBS_SYNC_SAP), this.createTrigger(JOBProdutorSync.class, JOBSync.JOBS_SYNC_SAP));

            scheduler.scheduleJob(this.createJob(JOBTipoVeiculoSync.class, JOBSync.JOBS_SYNC_SAP), this.createTrigger(JOBTipoVeiculoSync.class, JOBSync.JOBS_SYNC_SAP));

            scheduler.scheduleJob(this.createJob(JOBMotoristaSync.class, JOBSync.JOBS_SYNC_SAP), this.createTrigger(JOBMotoristaSync.class, JOBSync.JOBS_SYNC_SAP));

            scheduler.scheduleJob(this.createJob(JOBVeiculoSync.class, JOBSync.JOBS_SYNC_SAP), this.createTrigger(JOBVeiculoSync.class, JOBSync.JOBS_SYNC_SAP));

            scheduler.scheduleJob(this.createJob(JOBTabelaClassificacaoSync.class, JOBSync.JOBS_SYNC_SAP), this.createTrigger(JOBTabelaClassificacaoSync.class, JOBSync.JOBS_SYNC_SAP));

            scheduler.scheduleJob(this.createJob(JOBNotaFiscalSync.class, JOBSync.JOBS_SYNC_SAP), this.createTrigger(JOBNotaFiscalSync.class, JOBSync.JOBS_SYNC_SAP));

            scheduler.scheduleJob(this.createJob(JOBIvaSync.class, JOBSync.JOBS_SYNC_SAP), this.createTrigger(JOBIvaSync.class, JOBSync.JOBS_SYNC_SAP));

            scheduler.scheduleJob(this.createJob(JOBNotasEPedidosEStatusRomaneioSync.class, JOBSync.JOBS_SYNC_SAP), this.createTrigger(JOBNotasEPedidosEStatusRomaneioSync.class, JOBSync.JOBS_SYNC_SAP));

            scheduler.scheduleJob(this.createJob(JOBRomaneioSync.class, JOBSync.JOBS_SYNC_SAP), this.createTrigger(JOBRomaneioSync.class, JOBSync.JOBS_SYNC_SAP));

            scheduler.scheduleJob(this.createJob(JOBClienteSync.class, JOBSync.JOBS_SYNC_SAP), this.createTrigger(JOBClienteSync.class, JOBSync.JOBS_SYNC_SAP));

            scheduler.scheduleJob(this.createJob(JOBSenhaSync.class, JOBSync.JOBS_SYNC_SAP), this.createTrigger(JOBSenhaSync.class, JOBSync.JOBS_SYNC_SAP));

            scheduler.scheduleJob(this.createJob(JOBObterPesoBalanca.class, JOBSync.JOBS_SYNC_SAP), this.createTrigger(JOBObterPesoBalanca.class, JOBSync.JOBS_SYNC_SAP));

            scheduler.scheduleJob(this.createJob(JOBCargaPontualSync.class, JOBSync.JOBS_SYNC_SAP), this.createTrigger(JOBCargaPontualSync.class, JOBSync.JOBS_SYNC_SAP));

            JOBSync.LOG.info(StringUtil.log("JOBS PARA SINCRONIZAÇÃO DE DADOS MESTRES REGISTRADOS..."));

        } catch (SchedulerException | NullPointerException e) {

            JOBSync.LOG.info("SCHEDULER JOB NÃO IMPLEMENTADO... " + e.getMessage());

        }
    }
}
