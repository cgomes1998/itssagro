package br.com.sjc.jobs.sap;

import br.com.sjc.fachada.service.SAPService;
import br.com.sjc.jobs.arquitetura.QuartzScheduler;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.sap.sync.SAPSync;
import br.com.sjc.util.StringUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@DisallowConcurrentExecution
public class JOBVeiculoPre extends QuartzScheduler implements Job {

    private static final Logger LOGGER = Logger.getLogger(JOBVeiculoPre.class.getName());

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        try {

            if (!this.isEnable(JOBVeiculoPre.class, context) || !SAPSync.ONLINE) {

                return;
            }

            this.getSapService(context).enviarCadastro(DadosSincronizacaoEnum.VEICULO, context.getJobDetail().getJobDataMap());

            JOBVeiculoPre.LOGGER.info(StringUtil.log("JOB RESPONSAVEL POR ENVIAR PRE-CADASTROS DE VEÍCULOS AO SAP EXECUTADO..."));

        } catch (Exception e) {

            JOBVeiculoPre.LOGGER.info("ERRO: " + e.getMessage());
        }
    }

    public SAPService getSapService(JobExecutionContext context) {

        return (SAPService) context.getJobDetail().getJobDataMap().get("sapService");
    }
}
