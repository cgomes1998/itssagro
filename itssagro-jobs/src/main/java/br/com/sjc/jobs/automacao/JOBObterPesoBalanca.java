package br.com.sjc.jobs.automacao;

import br.com.sjc.fachada.service.DashboardFilaService;
import br.com.sjc.jobs.arquitetura.QuartzScheduler;
import br.com.sjc.util.StringUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@DisallowConcurrentExecution
public class JOBObterPesoBalanca extends QuartzScheduler implements Job {

    private static final Logger LOG = Logger.getLogger(JOBObterPesoBalanca.class.getName());

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        try {

            if (!this.isEnable(JOBObterPesoBalanca.class, context)) {
                return;
            }

            this.getService(context).executarEventosAutomacao();

            JOBObterPesoBalanca.LOG.info(StringUtil.log("JOB PARA OBTER PESO DA BALANCA EXECUTADO..."));

        } catch (Exception e) {

            e.printStackTrace();

            JOBObterPesoBalanca.LOG.info("ERRO: " + e.getMessage());
        }
    }

    public DashboardFilaService getService(JobExecutionContext context) {

        return (DashboardFilaService) context.getJobDetail().getJobDataMap().get("dashboardFilaService");
    }

}
