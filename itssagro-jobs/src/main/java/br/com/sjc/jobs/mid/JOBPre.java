package br.com.sjc.jobs.mid;

import br.com.sjc.jobs.arquitetura.QuartzScheduler;
import br.com.sjc.jobs.sap.JOBMotoristaPre;
import br.com.sjc.jobs.sap.JOBVeiculoPre;
import br.com.sjc.util.StringUtil;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class JOBPre extends QuartzScheduler {

    private static final Logger LOG = Logger.getLogger(JOBPre.class.getName());

    private static final String JOBS_PRE_SAP = "JOBS_PRE_SAP";

    @Autowired
    private Environment environment;

    public void iniciarJobs() {

        try {

            final Scheduler scheduler = new StdSchedulerFactory().getScheduler();

            scheduler.start();

            scheduler.scheduleJob(this.createJob(JOBVeiculoPre.class, JOBPre.JOBS_PRE_SAP), this.createTrigger(JOBVeiculoPre.class, JOBPre.JOBS_PRE_SAP));

            scheduler.scheduleJob(this.createJob(JOBMotoristaPre.class, JOBPre.JOBS_PRE_SAP), this.createTrigger(JOBMotoristaPre.class, JOBPre.JOBS_PRE_SAP));

            JOBPre.LOG.info(StringUtil.log("JOBS RESPONSAVEIS POR ENVIAR PRE-CADASTROS AO SAP REGISTRADOS..."));

        } catch (SchedulerException | NullPointerException e) {

            JOBPre.LOG.info("SCHEDULER JOB NÃO IMPLEMENTADO... " + e.getMessage());
        }
    }
}
