package br.com.sjc.jobs.sap;

import br.com.sjc.fachada.service.SAPService;
import br.com.sjc.jobs.arquitetura.QuartzScheduler;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.sap.sync.SAPSync;
import br.com.sjc.util.StringUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@DisallowConcurrentExecution
public class JOBMotoristaPre extends QuartzScheduler implements Job {

    private static final Logger LOGGER = Logger.getLogger(JOBMotoristaPre.class.getName());

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        try {

            if (!this.isEnable(JOBMotoristaPre.class, context) || !SAPSync.ONLINE) {

                return;
            }

            this.getSapService(context).enviarCadastro(DadosSincronizacaoEnum.MOTORISTA, context.getJobDetail().getJobDataMap());

            JOBMotoristaPre.LOGGER.info(StringUtil.log("JOB RESPONSAVEL POR ENVIAR PRE-CADASTROS DE MOTORISTAS AO SAP EXECUTADO..."));

        } catch (Exception e) {

            JOBMotoristaPre.LOGGER.info("ERRO: " + e.getMessage());
        }
    }

    public SAPService getSapService(JobExecutionContext context) {

        return (SAPService) context.getJobDetail().getJobDataMap().get("sapService");
    }
}
