package br.com.sjc.jobs.sap;

import br.com.sjc.fachada.service.ParametroService;
import br.com.sjc.jobs.arquitetura.QuartzScheduler;
import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.enums.TipoParametroEnum;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.MailUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.logging.Logger;

@Component
@DisallowConcurrentExecution
public class JOBNotaFiscalSync extends QuartzScheduler implements Job {

    private static final Logger LOG = Logger.getLogger(JOBNotaFiscalSync.class.getName());
    private static final String IMAPS = "imaps";

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        try {

            if (!this.isEnable(JOBNotaFiscalSync.class, context)) {

                return;
            }

            JOBNotaFiscalSync.LOG.info(StringUtil.log("JOB PARA REALIZAR O DOWNLOAD DE NFES EXECUTADO..."));

            List<Parametro> parametros = this.getParametroService(context).obterPorTipoParametro(TipoParametroEnum.listarParametrosParaDonwloadNfe());

            final Parametro usuario = parametros.stream().filter(i -> TipoParametroEnum.MAIL_USUARIO.equals(i.getTipoParametro())).findFirst().orElse(null);

            final Parametro senha = parametros.stream().filter(i -> TipoParametroEnum.MAIL_PASSWORD.equals(i.getTipoParametro())).findFirst().orElse(null);

            final Parametro host = parametros.stream().filter(i -> TipoParametroEnum.MAIL_IMAP_HOST.equals(i.getTipoParametro())).findFirst().orElse(null);

            final Parametro porta = parametros.stream().filter(i -> TipoParametroEnum.MAIL_IMAP_PORT.equals(i.getTipoParametro())).findFirst().orElse(null);

            final Parametro diretorio = parametros.stream().filter(i -> TipoParametroEnum.DIRETORIO_XML.equals(i.getTipoParametro())).findFirst().orElse(null);

            if (ObjetoUtil.isNotNull(usuario) && ObjetoUtil.isNotNull(senha) && ObjetoUtil.isNotNull(host) && ObjetoUtil.isNotNull(porta) && ObjetoUtil.isNotNull(diretorio)) {

                MailUtil.downloadDeAnexos(DateUtil.hoje(), host.getValor(), porta.getValor(), IMAPS, usuario.getValor(), senha.getValor(), diretorio.getValor());
            }

        } catch (Exception e) {

            JOBNotaFiscalSync.LOG.info("ERRO: " + e.getMessage());
        }
    }

    public ParametroService getParametroService(JobExecutionContext context) {

        return (ParametroService) context.getJobDetail().getJobDataMap().get("parametroService");
    }
}
