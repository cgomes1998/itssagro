package br.com.sjc.jobs.sap;

import br.com.sjc.fachada.service.IvaService;
import br.com.sjc.jobs.arquitetura.QuartzScheduler;
import br.com.sjc.modelo.sap.sync.SAPSync;
import br.com.sjc.util.StringUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@DisallowConcurrentExecution
public class JOBIvaSync extends QuartzScheduler implements Job {

    private static final Logger LOG = Logger.getLogger(JOBIvaSync.class.getName());

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        try {

            if (!this.isEnable(JOBIvaSync.class, context) || !SAPSync.ONLINE) {

                return;
            }

            this.getIvaService(context).sincronizar();

            JOBIvaSync.LOG.info(StringUtil.log("JOB PARA SINCRONIZAR IVAS EXECUTADO..."));

        } catch (Exception e) {

            JOBIvaSync.LOG.info("ERRO: " + e.getMessage());
        }
    }

    public IvaService getIvaService(JobExecutionContext context) {

        return (IvaService) context.getJobDetail().getJobDataMap().get("ivaService");
    }
}
