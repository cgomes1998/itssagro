package br.com.sjc.jobs.arquitetura;

import br.com.sjc.fachada.service.*;
import br.com.sjc.modelo.cfg.ConfiguracaoJob;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.naming.NamingException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@Component
public class QuartzScheduler {

    private static final Logger LOG = Logger.getLogger(QuartzScheduler.class.getName());

    @Autowired
    private SAPService sapService;

    @Autowired
    private ConfiguracaoJobService configuracaoJobService;

    @Autowired
    private MotoristaService motoristaService;

    @Autowired
    private VeiculoService veiculoService;

    @Autowired
    private AmbienteSincronizacaoService ambienteSincronizacaoService;

    @Autowired
    private CentroService centroService;

    @Autowired
    private TipoVeiculoService tipoVeiculoService;

    @Autowired
    private ProdutorService produtorService;

    @Autowired
    private SafraService safraService;

    @Autowired
    private TabelaClassificacaoService tabelaClassificacaoService;

    @Autowired
    private TransportadoraService transportadoraService;

    @Autowired
    private MaterialService materialService;

    @Autowired
    private ParametroService parametroService;

    @Autowired
    private IvaService ivaService;

    @Autowired
    private SenhaService senhaService;

    @Autowired
    private DashboardFilaService dashboardFilaService;

    @Autowired
    private AgendamentoCargaPontualService agendamentoCargaPontualService;

    public boolean isEnable(Class<?> aClass, JobExecutionContext context) {

        boolean result = false;

        try {

            final ConfiguracaoJob parametro = this.getConfiguracaoJobService(context).obterPorNome(aClass.getName());

            if (ObjetoUtil.isNotNull(parametro)) {

                result = parametro.isAtivo();
            }

            QuartzScheduler.LOG.info(StringUtil.log(aClass.getName() + "=" + result));

            return result;

        } catch (NamingException e) {

            QuartzScheduler.LOG.info("ERRO AO REALIZAR LOOKUP: " + e.getMessage());
        }

        return true;
    }

    public JobDetail createJob(final Class<? extends Job> jobClass, final String group) {

        JobDetail jobDetail = JobBuilder.newJob(jobClass)

                .withIdentity(jobClass.getSimpleName(), group)

                .build();

        for (final String keySet : this.jobDataMap().keySet()) {

            jobDetail.getJobDataMap().put(keySet, this.jobDataMap().get(keySet));
        }

        return jobDetail;
    }

    public Trigger createTrigger(final Class<? extends Job> jobClass, final String group) {

        final ConfiguracaoJob configuracao;

        configuracao = this.configuracaoJobService.obterPorNome(jobClass.getName());

        if (configuracao == null) {

            QuartzScheduler.LOG.info(String.format("CONFIGURAÇÃO DE JOB NÃO ENCONTRADO PARA CLASSE [%s].", jobClass.getName()));

        } else if (StringUtil.isEmpty(configuracao.getSchedule())) {

            QuartzScheduler.LOG.info(String.format("CONFIGURAÇÃO DE JOB [%s] INVÁLIDA PARA CLASSE [%s].", configuracao.getSchedule(), jobClass.getName()));

        } else {

            QuartzScheduler.LOG.info(String.format("REGISTRANDO JOB [%s] COM O CRON [%s]...", jobClass.getName(), configuracao.getSchedule()));

            return TriggerBuilder.newTrigger()

                    .withIdentity("TRIGGER_" + jobClass.getName(), group)

                    .withSchedule(CronScheduleBuilder.cronSchedule(configuracao.getSchedule()))

                    .build();
        }

        return TriggerBuilder.newTrigger().build();
    }

    public Map<String, Object> jobDataMap() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("sapService", this.sapService);

        map.put("configuracaoJobService", this.configuracaoJobService);

        map.put("motoristaService", this.motoristaService);

        map.put("veiculoService", this.veiculoService);

        map.put("ambienteSincronizacaoService", this.ambienteSincronizacaoService);

        map.put("centroService", this.centroService);

        map.put("tipoVeiculoService", this.tipoVeiculoService);

        map.put("materialService", this.materialService);

        map.put("produtorService", this.produtorService);

        map.put("safraService", this.safraService);

        map.put("tabelaClassificacaoService", this.tabelaClassificacaoService);

        map.put("transportadoraService", this.transportadoraService);

        map.put("parametroService", this.parametroService);

        map.put("ivaService", this.ivaService);

        map.put("senhaService", this.senhaService);

        map.put("dashboardFilaService", this.dashboardFilaService);

        map.put("agendamentoCargaPontualService", this.agendamentoCargaPontualService);

        return map;
    }

    public ConfiguracaoJobService getConfiguracaoJobService(JobExecutionContext context) throws NamingException {

        return (ConfiguracaoJobService) context.getJobDetail().getJobDataMap().get("configuracaoJobService");
    }

}
