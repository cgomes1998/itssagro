package br.com.sjc.jobs.sap;

import br.com.sjc.fachada.service.*;
import br.com.sjc.fachada.servico.impl.ServiceInject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RFCServer {

	@Autowired
	private SAPService sapService;

	@Autowired
	private ParametroService parametroService;

	@Autowired
	private MotoristaService motoristaService;

	@Autowired
	private VeiculoService veiculoService;

	@Autowired
	private RomaneioRequestKeyService romaneioRequestKeyService;

	@Autowired
	private AmbienteSincronizacaoService ambienteSincronizacaoService;

	@Autowired
	private DadosSincronizacaoService dadosSincronizacaoService;

	@Autowired
	private AmbienteService ambienteService;

	@Autowired
	private RomaneioService romaneioService;

	@Autowired
	private PesagemService pesagemService;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private LaudoService laudoService;

	@Autowired
	private TabelaValorService tabelaValorService;

	public void iniciarServicosSAP() {

		this.sapService.initRFCServer();
	}

	public void injetarDependeciasEmMemoria() {

		ServiceInject.motoristaService = this.motoristaService;

		ServiceInject.veiculoService = this.veiculoService;

		ServiceInject.ambienteSincronizacaoService = this.ambienteSincronizacaoService;

		ServiceInject.romaneioRequestKeyService = this.romaneioRequestKeyService;

		ServiceInject.romaneioService = this.romaneioService;

		ServiceInject.pesagemService = this.pesagemService;

		ServiceInject.dadosSincronizacaoService = this.dadosSincronizacaoService;

		ServiceInject.ambienteService = this.ambienteService;

		ServiceInject.parametroService = this.parametroService;

		ServiceInject.usuarioService = this.usuarioService;

		ServiceInject.laudoService = this.laudoService;

		ServiceInject.tabelaValorService = this.tabelaValorService;

		ServiceInject.sapService = this.sapService;
	}

}
