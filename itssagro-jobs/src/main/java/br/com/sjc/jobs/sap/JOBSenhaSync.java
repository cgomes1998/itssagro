package br.com.sjc.jobs.sap;

import br.com.sjc.fachada.service.SenhaService;
import br.com.sjc.jobs.arquitetura.QuartzScheduler;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.sap.sync.SAPSync;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.StringUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@DisallowConcurrentExecution
public class JOBSenhaSync extends QuartzScheduler implements Job {

    private static final Logger LOG = Logger.getLogger(JOBSenhaSync.class.getName());

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        try {
            if (!this.isEnable(JOBSenhaSync.class, context)) {
                return;
            }
            this.getService(context).executarCancelamentoAutomatico();
            JOBSenhaSync.LOG.info(StringUtil.log("JOB PARA CANCELAR SENHAS EXECUTADO..."));
        } catch (Exception e) {
            JOBSenhaSync.LOG.info("ERRO: " + e.getMessage());
        }
    }

    public SenhaService getService(JobExecutionContext context) {
        return (SenhaService) context.getJobDetail().getJobDataMap().get("senhaService");
    }
}
