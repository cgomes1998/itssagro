package br.com.sjc.jobs.sap;

import br.com.sjc.fachada.service.SAPService;
import br.com.sjc.jobs.arquitetura.QuartzScheduler;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.sap.sync.SAPSync;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.StringUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@DisallowConcurrentExecution
public class JOBTipoVeiculoSync extends QuartzScheduler implements Job {

    private static final Logger LOG = Logger.getLogger(JOBTipoVeiculoSync.class.getName());

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        try {

            if (!this.isEnable(JOBTipoVeiculoSync.class, context) || !SAPSync.ONLINE) {

                return;
            }

            this.getSapService(context).sincronizar(DadosSincronizacaoEnum.TIPO_VEICULO, DateUtil.hoje());

            JOBTipoVeiculoSync.LOG.info(StringUtil.log("JOB PARA SINCRONIZAR TIPO DE VEÍCULOS EXECUTADO..."));

        } catch (Exception e) {

            JOBTipoVeiculoSync.LOG.info("ERRO: " + e.getMessage());
        }
    }

    public SAPService getSapService(JobExecutionContext context) {

        return (SAPService) context.getJobDetail().getJobDataMap().get("sapService");
    }
}
