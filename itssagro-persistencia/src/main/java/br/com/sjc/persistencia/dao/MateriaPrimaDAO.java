package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.MateriaPrima;
import br.com.sjc.modelo.cfg.Empresa;

public interface MateriaPrimaDAO extends DAO<Long, MateriaPrima> {
}
