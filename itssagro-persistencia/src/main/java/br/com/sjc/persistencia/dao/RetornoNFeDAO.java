package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.sap.RetornoNFe;

import java.util.List;
import java.util.Set;

public interface RetornoNFeDAO extends DAO<Long, RetornoNFe> {

    List<RetornoNFe> findByRomaneioId(final Long idRomaneio);

    List<RetornoNFe> findByNfeTransporteIn(final Set<String> nfes);

    RetornoNFe findFirstByNfeTransporteOrNfe(String nfeTransporte, String nfe);
}
