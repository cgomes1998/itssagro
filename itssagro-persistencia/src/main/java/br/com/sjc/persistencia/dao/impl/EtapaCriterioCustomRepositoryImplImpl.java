package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.modelo.EtapaCriterio;
import br.com.sjc.modelo.EtapaCriterio_;
import br.com.sjc.modelo.FluxoEtapa;
import br.com.sjc.modelo.FluxoEtapa_;
import br.com.sjc.modelo.dto.EtapaCriterioDTO;
import br.com.sjc.modelo.enums.CriterioTipoEnum;
import br.com.sjc.persistencia.dao.fachada.EtapaCriterioCustomRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Set;

@Service
public class EtapaCriterioCustomRepositoryImplImpl extends CustomRepositoryImpl implements EtapaCriterioCustomRepository {

    @Transactional
    @Override
    public List<EtapaCriterioDTO> obterQuantidadeRepeticoesDaEtapa(Set<Long> idsFluxosEtapa) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<EtapaCriterioDTO> criteriaQuery = builder.createQuery(EtapaCriterioDTO.class);

        Root<EtapaCriterio> root = criteriaQuery.from(EtapaCriterio.class);

        Join<EtapaCriterio, FluxoEtapa> fluxoEtapaJoin = root.join(EtapaCriterio_.fluxoEtapa);

        criteriaQuery.where(
                fluxoEtapaJoin.get(FluxoEtapa_.id).in(idsFluxosEtapa),
                builder.equal(root.get(EtapaCriterio_.tipo), CriterioTipoEnum.FALHA)
        );

        criteriaQuery.select(builder.construct(
                EtapaCriterioDTO.class,
                root.get(EtapaCriterio_.repetirEtapa),
                fluxoEtapaJoin.get(FluxoEtapa_.id)
        ));

        TypedQuery<EtapaCriterioDTO> query = this.getSession().createQuery(criteriaQuery);

        return query.getResultList();
    }
}
