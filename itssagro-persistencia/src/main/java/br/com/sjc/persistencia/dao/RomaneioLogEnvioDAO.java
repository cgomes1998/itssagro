package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.sap.RomaneioLogEnvio;

public interface RomaneioLogEnvioDAO extends DAO<Long, RomaneioLogEnvio> {

    RomaneioLogEnvio findByRomaneioId(Long id);
}
