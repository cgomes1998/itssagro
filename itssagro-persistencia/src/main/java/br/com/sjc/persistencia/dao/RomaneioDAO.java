package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Motivo;
import br.com.sjc.modelo.Pesagem;
import br.com.sjc.modelo.dto.RomaneioDTO;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusPedidoEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.sap.Romaneio;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface RomaneioDAO extends DAO<Long, Romaneio> {

    @Query("select r.motivos from Romaneio r where r.id = ?1")
    List<Motivo> findMotivosById(Long id);

    @Query("select r.id from Romaneio r where r.statusIntegracaoSAP in ?1 and r.dataCadastro >= ?2")
    List<Long> findIdByStatusIntegracaoSAPInAndDataCadastroGreaterThanEqual(List<StatusIntegracaoSAPEnum> statusIntegracaoSAP, Date dataCadastro);

    List<Romaneio> findByDataCadastroGreaterThanEqualAndStatusIntegracaoSAP(
            Date dataCadastro,
            StatusIntegracaoSAPEnum statusIntegracaoSAP
    );

    List<Romaneio> findByStatusIntegracaoSAP(StatusIntegracaoSAPEnum statusIntegracaoSAP);

    @Query("SELECT coalesce(max(r.id), 0) FROM Romaneio r")
    Long getMaxId();

    @Query("select r.motivoEstorno from Romaneio r where r.id = ?1")
    String findMotivoEstornoById(Long id);

    Romaneio findByRomaneioArmazemTerceirosTicketOrigem(final String ticket);

    Romaneio findByNumeroRomaneio(String numeroRomaneio);

    Romaneio findByNumeroRomaneioAndStatusRomaneioInAndOperacaoEntidade(
            String numeroRomaneio,
            List<StatusRomaneioEnum> statusRomaneio,
            DadosSincronizacaoEnum operacao
    );

    @Query("select coalesce(sum(i.pesoTotalNfe), 0) as saldoReservado from Romaneio r inner join ItemNFPedido i on i.romaneio.id = r.id where (r.statusRomaneio = 'EM_PROCESSAMENTO' or r.statusRomaneio = 'PENDENTE') and i.numeroPedido = ?1 and i.itemPedido = ?2 ")
    BigDecimal obterSaldoReservado(
            String pedido,
            String item
    );

    @Query("select coalesce(sum(i.quantidadeCarregar), 0) as saldoOrdemVendaBloqueado from Romaneio r inner join ItemNFPedido i on i.romaneio.id = r.id where (r.statusRomaneio = 'EM_PROCESSAMENTO' or r.statusRomaneio = 'PENDENTE') and i.codigoOrdemVenda = ?1 and r.numeroRomaneio != ?2 ")
    BigDecimal obterSaldoOrdemVendaBloqueado(
            String ordemVenda,
            String romaneio
    );

    Romaneio findByNumeroRomaneioOrRomaneioArmazemTerceirosTicketOrigemAndOperacaoEntidade(
            String numeroRomaneio,
            String ticket,
            DadosSincronizacaoEnum operacao
    );

    @Query("select count(r.id) > 0 from Romaneio r where r.romaneioArmazemTerceiros.ticketOrigem = ?1 and ( r.statusIntegracaoSAP <> ?2 or r.statusRomaneio <> ?3)")
    boolean existsByTicketOrigem(
            final String nota,
            StatusIntegracaoSAPEnum status,
            final StatusRomaneioEnum statusRomaneio
    );

    @Transactional
    @Modifying
    @Query("update Romaneio r set r.statusRomaneio = ?2, r.motivoEstorno = ?3 where r.id = ?1")
    void updateStatusRomaneio(
            Long id,
            StatusRomaneioEnum statusRomaneioEnum,
            String motivoEstorno
    );

    @Transactional
    @Modifying
    @Query("update Romaneio r set r.statusPedido = ?3, r.numeroPedido = ?2 where r.numeroRomaneio = ?1")
    void updateStatusPedido(
            final String romaneio,
            final String numeroPedido,
            final StatusPedidoEnum statusPedido
    );

    @Transactional
    @Modifying
    @Query("update Romaneio r set r.conversaoLitragem.id = ?1 where r.numeroRomaneio = ?2")
    void updateConversaoLitragem(final Long idConversao, final String numero);

    @Transactional
    @Modifying
    @Query("delete from RomaneioRequestKey k where k.romaneio.id in ?1")
    void deletarTodasAsRequests(final List<Long> ids);

    @Transactional
    @Modifying
    @Query("update Romaneio r set r.romaneioJaImprimidoAutoatendimento = ?1, r.dtImpressaoAutoatendimento = ?2 where r.numeroRomaneio = ?3 or r.numeroCartaoAcesso = ?4")
    void updateRomaneioAtendido(final Boolean imprimido, final Date DtImpressaoAutoatendimento, final String numeroRomaneio, Integer numeroCartaoAcesso);

    Romaneio findFirstBySenhaId(Long senhaId);

    @Query("select count(item.id) > 0 from ItemNFPedido item where item.tipoSeta.id = ?1")
    boolean existsItemNFPedidoByTipoVeiculoSeta(Long tipoVeiculoSetaId);

    Romaneio findByOperacaoEntidadeAndNumeroRomaneioAndStatusIntegracaoSAP(
            DadosSincronizacaoEnum operacaoEntidade,
            String numero,
            StatusIntegracaoSAPEnum statusRomaneio
    );

    @Query("select new br.com.sjc.modelo.dto.RomaneioDTO(t.id, t.numeroRomaneio, t.statusRomaneio) from #{#entityName} t where t.senha.id = :senhaId")
    RomaneioDTO getBySenhaId(@Param("senhaId") Long senhaId);

}
