package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.sap.Veiculo;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface VeiculoDAO extends DAO<Long, Veiculo> {

    Veiculo findByCodigo(final String codigo);

    Veiculo findByRequestKey(final String requestKey);

    Veiculo findByPlaca1(final String placa1);

    Veiculo findTop1ByPlaca1(final String placa1);

    Veiculo findByPlaca1OrCodigo(final String placa1, final String codigo);

    List<Veiculo> findTop10ByPlaca1IgnoreCaseContainingOrderByPlaca1Asc(String placa1);

    List<Veiculo> findTop10ByStatusAndPlaca1IgnoreCaseContainingOrderByPlaca1Asc(StatusEnum status, String placa1);

    List<Veiculo> findByStatusCadastroSapIn(final List<StatusCadastroSAPEnum> statusCadastroSAPEnums);

    List<Veiculo> findByStatusCadastroSapInAndDataCadastroGreaterThanEqual(final List<StatusCadastroSAPEnum> statusCadastroSAPEnums, final Date data);

    boolean existsByPlaca1(String placa1);

    @Query("select count(v.id)>0 from Veiculo v where v.id <> ?2 and v.placa1 = ?1")
    boolean existsByPlaca1AndNeId(final String placa1, final Long id);

}
