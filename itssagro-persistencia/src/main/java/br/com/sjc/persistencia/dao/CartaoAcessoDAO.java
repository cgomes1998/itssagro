package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.CartaoAcesso;

public interface CartaoAcessoDAO extends DAO<Long, CartaoAcesso> {

}
