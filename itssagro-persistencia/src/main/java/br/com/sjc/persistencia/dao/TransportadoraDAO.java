package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import br.com.sjc.modelo.sap.Transportadora;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface TransportadoraDAO extends DAO<Long, Transportadora> {

    Transportadora findByCodigo(final String codigo);

    Transportadora findFirstByCodigo(final String codigo);

    @Query("select t from Transportadora t where t.statusRegistro = ?1 and (t.codigo like concat('%', lower(?2), '%') or t.cnpj like concat('%', lower(?3), '%') or t.cpf like concat('%', lower(?4), '%') or lower(t.descricao) like concat('%', lower(?5), '%'))")
    List<Transportadora> findTop10ByStatusRegistroAndCodigoIgnoreCaseContainingOrCnpjIgnoreCaseContainingOrCpfIgnoreCaseContainingOrDescricaoIgnoreCaseContainingOrderByCodigoAsc(StatusRegistroEnum statusRegistro, String codigo, String cnpj, String cpf, String descricao);

    List<Transportadora> findByStatusCadastroSapIn(final List<StatusCadastroSAPEnum> statusCadastroSAPEnums);

    List<Transportadora> findByStatusCadastroSapInAndDataCadastroGreaterThanEqual(final List<StatusCadastroSAPEnum> statusCadastroSAPEnums, final Date data);

    List<Transportadora> findByCnpjInOrCpfIn(final List<String> cnpjs, final List<String> cpfs);
}
