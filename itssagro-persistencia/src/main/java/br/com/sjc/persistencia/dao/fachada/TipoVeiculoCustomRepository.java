package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.sap.TipoVeiculo;

import java.util.List;

public interface TipoVeiculoCustomRepository {

    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    List<TipoVeiculo> listarPorStatus(final StatusEnum status);
}
