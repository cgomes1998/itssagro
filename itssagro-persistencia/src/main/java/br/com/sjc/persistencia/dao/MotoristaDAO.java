package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.sap.Motorista;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

public interface MotoristaDAO extends DAO<Long, Motorista> {

    Motorista findByCodigo(final String codigo);

    Motorista findByRequestKey(final String requestKey);

    Motorista findByIdOrCpf(Long id, String cpf);

    Motorista findFirstByVeiculoIdAndStatus(Long veiculoId, StatusEnum status);

    Motorista findTop1ByCpfOrCnh(final String cpf, final String cnh);

    Motorista findByCpfOrCodigo(final String cpf, final String codigo);

    List<Motorista> findTop10ByNomeIgnoreCaseContainingOrCodigoIgnoreCaseContainingOrCpfIgnoreCaseContainingOrCnhIgnoreCaseContainingOrderByCodigoAsc(String nome, String codigo, String cpf, String cnh);

    List<Motorista> findByStatusCadastroSapIn(final List<StatusCadastroSAPEnum> statusCadastroSAPEnums);

    List<Motorista> findByStatusCadastroSapInAndDataCadastroGreaterThanEqual(final List<StatusCadastroSAPEnum> statusCadastroSAPEnums, final Date data);

    boolean existsByCpf(String placa1);

    @Query("select count(m.id)>0 from Motorista m where m.id <> ?2 and m.cpf = ?1")
    boolean existsByCpfAndNeId(final String cpf, final Long id);

    @Transactional
    @Modifying
    @Query("update Motorista motorista set motorista.veiculo = null where motorista.veiculo.id = ?1 and motorista.id != ?2")
    void removerVeiculoDeOutrosMotoristas(Long veiculoId, Long motoristaId);

}
