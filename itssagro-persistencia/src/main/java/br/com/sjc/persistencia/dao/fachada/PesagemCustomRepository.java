package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.Pesagem;
import br.com.sjc.modelo.PesagemHistorico;
import br.com.sjc.modelo.dto.PesagemHistoricoDTO;
import br.com.sjc.modelo.dto.PesagemManualDTO;
import br.com.sjc.modelo.sap.ItemNFPedido;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface PesagemCustomRepository {

	@Transactional
	List<PesagemHistoricoDTO> obterSegundaPesagemDeRomaneios(List<Long> idPesagens);

	@Transactional
	List<PesagemHistoricoDTO> obterPrimeiraPesagemDeRomaneios(List<Long> idPesagens);

	@Transactional(readOnly = true)
    List<PesagemManualDTO> obterPesagensManuais(final ItemNFPedido filtro);
}
