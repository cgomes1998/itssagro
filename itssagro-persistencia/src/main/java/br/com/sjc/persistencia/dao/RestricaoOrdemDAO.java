package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.sap.RestricaoOrdem;

public interface RestricaoOrdemDAO extends DAO<Long, RestricaoOrdem> {
}
