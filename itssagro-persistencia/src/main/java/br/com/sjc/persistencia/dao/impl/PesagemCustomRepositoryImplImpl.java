package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.modelo.Pesagem;
import br.com.sjc.modelo.PesagemHistorico;
import br.com.sjc.modelo.PesagemHistorico_;
import br.com.sjc.modelo.Pesagem_;
import br.com.sjc.modelo.dto.PesagemHistoricoDTO;
import br.com.sjc.modelo.dto.PesagemManualDTO;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.persistencia.dao.fachada.PesagemCustomRepository;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class PesagemCustomRepositoryImplImpl extends CustomRepositoryImpl implements PesagemCustomRepository {

    @Transactional
    @Override
    public List<PesagemHistoricoDTO> obterSegundaPesagemDeRomaneios(List<Long> idPesagens) {

        CriteriaBuilder criteriaBuilder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<PesagemHistoricoDTO> criteriaQuery = criteriaBuilder.createQuery(PesagemHistoricoDTO.class);

        Root<PesagemHistorico> root = criteriaQuery.from(PesagemHistorico.class);

        Join<PesagemHistorico, Pesagem> pesagemJoin = root.join(PesagemHistorico_.pesagem);

        List<Predicate> predicates = new ArrayList<>();

        predicates.add(pesagemJoin.get(Pesagem_.id).in(idPesagens));

        predicates.add(criteriaBuilder.and(
                criteriaBuilder.isNotNull(root.get(PesagemHistorico_.pesoInicial)),
                criteriaBuilder.isNotNull(root.get(PesagemHistorico_.pesoFinal))
        ));

        criteriaQuery.where(predicates.toArray(new Predicate[]{}));

        criteriaQuery.select(criteriaBuilder.construct(
                PesagemHistoricoDTO.class,
                pesagemJoin.get(Pesagem_.id),
                criteriaBuilder.max(root.get(PesagemHistorico_.dataCadastro.getName()))
        ));

        criteriaQuery.groupBy(pesagemJoin.get(Pesagem_.id));

        TypedQuery<PesagemHistoricoDTO> typedQuery = this.getSession().createQuery(criteriaQuery);

        return typedQuery.getResultList();
    }

    @Transactional
    @Override
    public List<PesagemHistoricoDTO> obterPrimeiraPesagemDeRomaneios(List<Long> idPesagens) {

        CriteriaBuilder criteriaBuilder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<PesagemHistoricoDTO> criteriaQuery = criteriaBuilder.createQuery(PesagemHistoricoDTO.class);

        Root<PesagemHistorico> root = criteriaQuery.from(PesagemHistorico.class);

        Join<PesagemHistorico, Pesagem> pesagemJoin = root.join(PesagemHistorico_.pesagem);

        List<Predicate> predicates = new ArrayList<>();

        predicates.add(pesagemJoin.get(Pesagem_.id).in(idPesagens));

        predicates.add(criteriaBuilder.or(
                criteriaBuilder.and(
                        criteriaBuilder.or(
                                criteriaBuilder.isNull(root.get(PesagemHistorico_.pesoInicial)),
                                criteriaBuilder.equal(root.get(PesagemHistorico_.pesoInicial), BigDecimal.ZERO)
                        ),
                        criteriaBuilder.isNotNull(root.get(PesagemHistorico_.pesoFinal))
                ),
                criteriaBuilder.and(
                        criteriaBuilder.or(
                                criteriaBuilder.isNull(root.get(PesagemHistorico_.pesoFinal)),
                                criteriaBuilder.equal(root.get(PesagemHistorico_.pesoFinal), BigDecimal.ZERO)
                        ),
                        criteriaBuilder.isNotNull(root.get(PesagemHistorico_.pesoInicial))
                )
        ));

        criteriaQuery.where(predicates.toArray(new Predicate[]{}));

        criteriaQuery.select(criteriaBuilder.construct(
                PesagemHistoricoDTO.class,
                pesagemJoin.get(Pesagem_.id),
                criteriaBuilder.max(root.get(PesagemHistorico_.dataCadastro.getName()))
        ));

        criteriaQuery.groupBy(pesagemJoin.get(Pesagem_.id));

        TypedQuery<PesagemHistoricoDTO> typedQuery = this.getSession().createQuery(criteriaQuery);

        return typedQuery.getResultList();
    }

    @Transactional(readOnly = true)
    @Override
    public List<PesagemManualDTO> obterPesagensManuais(final ItemNFPedido filtro) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<PesagemManualDTO> criteriaQuery = builder.createQuery(PesagemManualDTO.class);

        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);

        Join<Romaneio, Pesagem> joinPesagem = root.join(Romaneio_.pesagem);

        Join<Romaneio, ItemNFPedido> joinItemNfPedido = root.join(Romaneio_.itens);

        Join<ItemNFPedido, Material> joinMaterial = joinItemNfPedido.join(ItemNFPedido_.material);

        Join<ItemNFPedido, Veiculo> joinVeiculo = joinItemNfPedido.join(ItemNFPedido_.placaCavalo);

        Join<ItemNFPedido, Motorista> joinMotorista = joinItemNfPedido.join(ItemNFPedido_.motorista);

        criteriaQuery.where(predicates(
                builder,
                root,
                joinPesagem,
                joinMaterial,
                joinVeiculo,
                joinMotorista,
                filtro
        ).toArray(new Predicate[]{}));

        criteriaQuery.select(builder.construct(
                PesagemManualDTO.class,
                root.get(Romaneio_.numeroRomaneio),
                root.get(Romaneio_.dataCadastro),
                builder.concat(
                        builder.concat(
                                joinMaterial.get(Material_.codigo),
                                " - "
                        ),
                        joinMaterial.get(Material_.descricao)
                ),
                joinVeiculo.get(Veiculo_.placa1),
                builder.concat(
                        builder.concat(
                                joinMotorista.get(Motorista_.codigo),
                                " - "
                        ),
                        joinMotorista.get(Motorista_.nome)
                ),
                joinPesagem.get(Pesagem_.motivoAlteracao),
                joinPesagem.get(Pesagem_.motivoAlteracaoBruta)
        ));

        criteriaQuery.groupBy(
                root.get(Romaneio_.id),
                root.get(Romaneio_.numeroRomaneio),
                root.get(Romaneio_.dataCadastro),
                joinMaterial.get(Material_.codigo),
                joinMaterial.get(Material_.descricao),
                joinVeiculo.get(Veiculo_.placa1),
                joinMotorista.get(Motorista_.codigo),
                joinMotorista.get(Motorista_.nome),
                joinPesagem.get(Pesagem_.motivoAlteracao),
                joinPesagem.get(Pesagem_.motivoAlteracaoBruta)
        );

        criteriaQuery.orderBy(builder.desc(root.get(Romaneio_.id)));

        return this.getSession().createQuery(criteriaQuery).getResultList();
    }

    private List<Predicate> predicates(
            CriteriaBuilder builder,
            Root<Romaneio> root,
            Join<Romaneio, Pesagem> joinPesagem,
            Join<ItemNFPedido, Material> joinMaterial,
            Join<ItemNFPedido, Veiculo> joinVeiculo,
            Join<ItemNFPedido, Motorista> joinMotorista,
            ItemNFPedido filtro
    ) {

        List<Predicate> predicates = new ArrayList<>();

        predicates.add(builder.or(builder.isTrue(joinPesagem.get(Pesagem_.pesagemBrutaManual)), builder.isTrue(joinPesagem.get(Pesagem_.pesagemFinalManual)), builder.isNotNull(joinPesagem.get(Pesagem_.motivoAlteracao)), builder.isNotNull(joinPesagem.get(Pesagem_.motivoAlteracaoBruta))));

        if (ObjetoUtil.isNotNull(filtro)) {

            if (ObjetoUtil.isNotNull(filtro.getDataFiltroInicio()) && ObjetoUtil.isNotNull(filtro.getDataFiltroFim())) {
                predicates.add(builder.between(root.get(Romaneio_.dataCadastro), DateUtil.obterDataHoraZerada(filtro.getDataFiltroInicio()), DateUtil.converterDateFimDia(filtro.getDataFiltroFim())));

            } else if (ObjetoUtil.isNotNull(filtro.getDataFiltroInicio())) {
                predicates.add(builder.greaterThanOrEqualTo(root.get(Romaneio_.dataCadastro), DateUtil.obterDataHoraZerada(filtro.getDataFiltroInicio())));

            } else if (ObjetoUtil.isNotNull(filtro.getDataFiltroFim())) {
                predicates.add(builder.lessThanOrEqualTo(root.get(Romaneio_.dataCadastro), DateUtil.converterDateFimDia(filtro.getDataFiltroFim())));
            }

            if (ObjetoUtil.isNotNull(filtro.getMaterial())) {
                predicates.add(builder.and(builder.equal(joinMaterial.get(Material_.id), filtro.getMaterial().getId())));
            }

            if (ObjetoUtil.isNotNull(filtro.getPlacaCavalo())) {
                predicates.add(builder.and(builder.equal(joinVeiculo.get(Veiculo_.id), filtro.getPlacaCavalo().getId())));
            }

            if (ObjetoUtil.isNotNull(filtro.getMotorista())) {
                predicates.add(builder.and(builder.equal(joinMotorista.get(Motorista_.id), filtro.getMotorista().getId())));
            }
        }

        return predicates;
    }

}
