package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.modelo.dto.NfeDepositoDTO;
import br.com.sjc.modelo.dto.RetornoNfeDTO;
import br.com.sjc.modelo.dto.RomaneioDepositoDTO;
import br.com.sjc.modelo.sap.RetornoNFe;
import br.com.sjc.modelo.sap.RetornoNFe_;
import br.com.sjc.persistencia.dao.fachada.RetornoNfeCustomRepository;
import br.com.sjc.util.ColecaoUtil;
import org.springframework.stereotype.Service;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RetornoNfeCustomRepositoryImplImpl extends CustomRepositoryImpl implements RetornoNfeCustomRepository {

    @Transactional
    @Override
    public List<NfeDepositoDTO> obterDadosDeNotaParaRelatorioDeDeposito(List<RomaneioDepositoDTO> romaneioDepositoDTOS) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<NfeDepositoDTO> criteriaQuery = builder.createQuery(NfeDepositoDTO.class);

        Root<RetornoNFe> root = criteriaQuery.from(RetornoNFe.class);

        Set<String> requests = romaneioDepositoDTOS.stream().map(RomaneioDepositoDTO::getRequestKey).collect(Collectors.toSet());

        if (ColecaoUtil.isNotEmpty(requests)) {

            criteriaQuery.where(root.get(RetornoNFe_.requestKey).in(requests));

            criteriaQuery.select(builder.construct(NfeDepositoDTO.class, root.get(RetornoNFe_.nfe), root.get(RetornoNFe_.requestKey)));

            TypedQuery<NfeDepositoDTO> typedQuery = this.getSession().createQuery(criteriaQuery);

            return typedQuery.getResultList();
        }

        return null;
    }

    @Transactional
    @Override
    public List<RetornoNFe> obterIdRetornoNFePorNfe(final Set<String> notas) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<RetornoNFe> criteriaQuery = builder.createQuery(RetornoNFe.class);

        Root<RetornoNFe> root = criteriaQuery.from(RetornoNFe.class);

        criteriaQuery.where(builder.or(root.get(RetornoNFe_.nfe).in(notas), root.get(RetornoNFe_.nfeTransporte).in(notas)));

        criteriaQuery.select(builder.construct(RetornoNFe.class, root.get(RetornoNFe_.id), root.get(RetornoNFe_.nfe), root.get(RetornoNFe_.nfeTransporte)));

        TypedQuery<RetornoNFe> typedQuery = this.getSession().createQuery(criteriaQuery);

        return typedQuery.getResultList();
    }
    
    @Transactional
    @Override
    public List<RetornoNfeDTO> obterRetornoNFePorRomaneios(final List<String> numRomaneios) {
    	
        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<RetornoNfeDTO> criteriaQuery = builder.createQuery(RetornoNfeDTO.class);

        Root<RetornoNFe> root = criteriaQuery.from(RetornoNFe.class);

        if (ColecaoUtil.isNotEmpty(numRomaneios)) {

            criteriaQuery.where(root.join(RetornoNFe_.romaneio).get("numeroRomaneio").in(numRomaneios));

            criteriaQuery.select(builder.construct(RetornoNfeDTO.class, root.get(RetornoNFe_.nfe), root.get(RetornoNFe_.requestKey), root.get(RetornoNFe_.dataCriacaoNotaMastersaf)));

            TypedQuery<RetornoNfeDTO> typedQuery = this.getSession().createQuery(criteriaQuery);

            return typedQuery.getResultList();
        }

        return null;
    }

}
