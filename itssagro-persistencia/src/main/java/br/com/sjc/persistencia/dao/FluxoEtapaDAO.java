package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.FluxoEtapa;
import br.com.sjc.modelo.TipoFluxoEnum;
import br.com.sjc.modelo.enums.EtapaEnum;

public interface FluxoEtapaDAO extends DAO<Long, FluxoEtapa> {

    FluxoEtapa findTop1ByFluxoTipoFluxoAndFluxoMateriaisMaterialIdAndEtapa(TipoFluxoEnum tipoFluxo, Long idMaterial, EtapaEnum etapaEnum);

}
