package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.sap.Veiculo;

import java.util.Date;
import java.util.List;

public interface VeiculoCustomRepository {

    List<Veiculo> listarPreCadastro(Date data);
}
