package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Pesagem;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;

public interface PesagemDAO extends DAO<Long, Pesagem> {

	@Query("select coalesce(p.pesoInicial, 0) from Pesagem p where p.id = ?1")
	BigDecimal findPesoInicialById(Long id);

	@Query("select coalesce(p.pesoFinal, 0) from Pesagem p where p.id = ?1")
	BigDecimal findPesoFinalById(Long id);

}
