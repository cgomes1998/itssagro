package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.modelo.*;
import br.com.sjc.modelo.dto.FluxoDTO;
import br.com.sjc.modelo.dto.FluxoEtapaDTO;
import br.com.sjc.modelo.dto.SenhaDTO;
import br.com.sjc.modelo.dto.TempoPorEtapaDTO;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.enums.StatusSenhaEnum;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.persistencia.dao.fachada.SenhaCustomRepository;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SenhaCustomRepositoryImplImpl extends CustomRepositoryImpl implements SenhaCustomRepository {

    @Transactional(readOnly = true)
    @Override
    public List<TempoPorEtapaDTO> obterTempoMedioPorEtapaEnumESenhas(List<Long> ids, List<EtapaEnum> etapas) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<TempoPorEtapaDTO> criteriaQuery = builder.createQuery(TempoPorEtapaDTO.class);

        Root<Senha> root = criteriaQuery.from(Senha.class);

        Join<Senha, SenhaEtapa> senhaEtapaJoin = root.join(Senha_.etapas);

        Join<SenhaEtapa, FluxoEtapa> fluxoEtapaJoin = senhaEtapaJoin.join(SenhaEtapa_.etapa);

        criteriaQuery.where(builder.and(root.get(Senha_.id).in(ids), senhaEtapaJoin.join(SenhaEtapa_.etapa).get(FluxoEtapa_.etapa).in(etapas)));

        criteriaQuery.select(builder.construct(TempoPorEtapaDTO.class, root.get(Senha_.id), fluxoEtapaJoin.get(FluxoEtapa_.sequencia), fluxoEtapaJoin.get(FluxoEtapa_.etapa), senhaEtapaJoin.get(SenhaEtapa_.horaEntrada), senhaEtapaJoin.get(SenhaEtapa_.horaSaida)));

        return this.getSession().createQuery(criteriaQuery).getResultList();
    }

    @Transactional(readOnly = true)
    @Override
    public List<TempoPorEtapaDTO> obterTempoMedioPorEtapaESenhas(List<Long> ids, List<FluxoEtapa> etapas) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<TempoPorEtapaDTO> criteriaQuery = builder.createQuery(TempoPorEtapaDTO.class);

        Root<Senha> root = criteriaQuery.from(Senha.class);

        Join<Senha, SenhaEtapa> senhaEtapaJoin = root.join(Senha_.etapas);

        Join<SenhaEtapa, FluxoEtapa> fluxoEtapaJoin = senhaEtapaJoin.join(SenhaEtapa_.etapa);

        criteriaQuery.where(builder.and(root.get(Senha_.id).in(ids), senhaEtapaJoin.join(SenhaEtapa_.etapa).get(FluxoEtapa_.id).in(etapas.stream().map(FluxoEtapa::getId).collect(Collectors.toList()))));

        criteriaQuery.select(builder.construct(TempoPorEtapaDTO.class, root.get(Senha_.id), fluxoEtapaJoin.get(FluxoEtapa_.sequencia), fluxoEtapaJoin.get(FluxoEtapa_.etapa), senhaEtapaJoin.get(SenhaEtapa_.horaEntrada), senhaEtapaJoin.get(SenhaEtapa_.horaSaida)));

        return this.getSession().createQuery(criteriaQuery).getResultList();
    }

    @Transactional(readOnly = true)
    @Override
    public List<SenhaDTO> listarPorMaterialEPlacaENumeroRomaneioENumeroCartaoAcesso(
            List<Material> materiais,
            String placa,
            String numeroRomaneio,
            String numeroCartaoAcesso,
            Date data,
            List<FluxoEtapaDTO> fluxoEtapas,
            List<StatusSenhaEnum> statusSenha
    ) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<SenhaDTO> criteriaQuery = builder.createQuery(SenhaDTO.class);

        Root<Senha> root = criteriaQuery.from(Senha.class);

        criteriaQuery.distinct(true);

        Subquery<Romaneio> romaneioSubquery = criteriaQuery.subquery(Romaneio.class);

        Subquery<Long> senhaSubquery = criteriaQuery.subquery(Long.class);

        Subquery<Long> fluxoSubquery = criteriaQuery.subquery(Long.class);

        Root<Fluxo> fluxoRoot = fluxoSubquery.from(Fluxo.class);

        Join<Fluxo, FluxoMaterial> fluxoMaterialJoin = fluxoRoot.join(Fluxo_.materiais);

        Join<FluxoMaterial, Material> subMaterialJoin = fluxoMaterialJoin.join(FluxoMaterial_.material);

        Root<Romaneio> romaneioRoot = romaneioSubquery.from(Romaneio.class);

        Root<Romaneio> romaneioSenhaSubQueryRoot = senhaSubquery.from(Romaneio.class);

        Join<Romaneio, Senha> senhaJoinSubQuery = romaneioSenhaSubQueryRoot.join(Romaneio_.senha);

        Join<Senha, SenhaEtapa> senhaEtapaJoin = root.join(Senha_.etapaAtual);

        Join<SenhaEtapa, FluxoEtapa> fluxoEtapaJoin = senhaEtapaJoin.join(SenhaEtapa_.etapa);

        Join<Senha, Veiculo> veiculoJoin = root.join(Senha_.veiculo, JoinType.LEFT);

        Join<Senha, Material> materialJoin = root.join(Senha_.material, JoinType.LEFT);

        Join<Senha, Motorista> motoristaJoin = root.join(Senha_.motorista, JoinType.LEFT);

        Join<Senha, Cliente> clienteJoin = root.join(Senha_.cliente, JoinType.LEFT);

        List<Predicate> predicados = new ArrayList<>();

        if (ColecaoUtil.isNotEmpty(materiais)) {

            predicados.add(root.get(Senha_.material).in(materiais.stream().map(Material::getId).collect(Collectors.toList())));
        }

        if (StringUtil.isNotNullEmpty(placa)) {

            predicados.add(builder.equal(builder.upper(veiculoJoin.get(Veiculo_.placa1)), placa.toUpperCase().replace("-", "")));
        }

        if (StringUtil.isNotNullEmpty(numeroRomaneio)) {

            senhaSubquery.select(senhaJoinSubQuery.get(Senha_.id)).where(builder.equal(romaneioSenhaSubQueryRoot.get(Romaneio_.numeroRomaneio), numeroRomaneio));

            predicados.add(builder.equal(root.get(Senha_.id), senhaSubquery));
        }

        if (StringUtil.isNotNullEmpty(numeroCartaoAcesso)) {

            predicados.add(builder.equal(root.get(Senha_.numeroCartaoAcesso), numeroCartaoAcesso));
        }

        if (ObjetoUtil.isNotNull(data)) {

            predicados.add(builder.greaterThanOrEqualTo(root.get(Senha_.dataCadastro), data));
        }

        fluxoSubquery.where(fluxoRoot.get(Fluxo_.id).in(fluxoEtapas.stream().map(FluxoEtapaDTO::getFluxo).map(FluxoDTO::getId).collect(Collectors.toSet())));

        predicados.add(materialJoin.get(Material_.id).in(fluxoSubquery.select(subMaterialJoin.get(Material_.id))));

        predicados.add(root.get(Senha_.tipoFluxo).in(fluxoEtapas.stream().map(FluxoEtapaDTO::getFluxo).map(FluxoDTO::getTipoFluxo).collect(Collectors.toSet())));

        predicados.add(fluxoEtapaJoin.get(FluxoEtapa_.etapa).in(fluxoEtapas.stream().map(FluxoEtapaDTO::getEtapa).collect(Collectors.toSet())));

        if(ObjetoUtil.isNotNull(statusSenha) && !statusSenha.isEmpty()){
            predicados.add(builder.not(root.get(Senha_.statusSenha).in(statusSenha)));
        }

        predicados.add(builder.or(

                builder.and(
                        builder.equal(
                                fluxoEtapaJoin.get(FluxoEtapa_.etapa),
                                EtapaEnum.DISTRIBUICAO_SENHA
                        ),
                        builder.not(root.in(romaneioSubquery.select(romaneioRoot.get("senha"))))
                ),

                builder.notEqual(fluxoEtapaJoin.get(FluxoEtapa_.etapa), EtapaEnum.DISTRIBUICAO_SENHA)
        ));

        criteriaQuery.where(builder.and(predicados.toArray(new Predicate[]{})));

        criteriaQuery.select(getConstructDTO(builder, root, senhaEtapaJoin, fluxoEtapaJoin, veiculoJoin, materialJoin, motoristaJoin, clienteJoin));

        criteriaQuery.groupBy(getGroupBy(root, senhaEtapaJoin, fluxoEtapaJoin, veiculoJoin, materialJoin, motoristaJoin, clienteJoin));

        return this.getSession().createQuery(criteriaQuery).getResultList();
    }

    private Expression[] getGroupBy(
            Root<Senha> root,
            Join<Senha, SenhaEtapa> senhaEtapaJoin,
            Join<SenhaEtapa, FluxoEtapa> fluxoEtapaJoin,
            Join<Senha, Veiculo> veiculoJoin,
            Join<Senha, Material> materialJoin,
            Join<Senha, Motorista> motoristaJoin,
            Join<Senha, Cliente> clienteJoin
    ) {

        return new Expression[]{
                root.get(Senha_.id),
                root.get(Senha_.status),
                root.get(Senha_.dataAlteracao),
                root.get(Senha_.senha),
                materialJoin.get(Material_.id),
                materialJoin.get(Material_.codigo),
                materialJoin.get(Material_.descricao),
                root.get(Senha_.dataCadastro),
                veiculoJoin.get(Veiculo_.placa1),
                motoristaJoin.get(Motorista_.nome),
                clienteJoin.get(Cliente_.nome),
                clienteJoin.get(Cliente_.cpfCnpj),
                root.get(Senha_.statusSenha),
                root.get(Senha_.idUsuarioAutorCadastro),
                fluxoEtapaJoin.get(FluxoEtapa_.id),
                fluxoEtapaJoin.get(FluxoEtapa_.etapa),
                fluxoEtapaJoin.get(FluxoEtapa_.sequencia),
                senhaEtapaJoin.get(SenhaEtapa_.qtdRepeticao),
                senhaEtapaJoin.get(SenhaEtapa_.qtdChamada),
                senhaEtapaJoin.get(SenhaEtapa_.horaEntrada),
                senhaEtapaJoin.get(SenhaEtapa_.horaSaida),
                senhaEtapaJoin.get(SenhaEtapa_.statusEtapa)
        };
    }

    private CompoundSelection<SenhaDTO> getConstructDTO(
            CriteriaBuilder builder,
            Root<Senha> root,
            Join<Senha, SenhaEtapa> senhaEtapaJoin,
            Join<SenhaEtapa, FluxoEtapa> fluxoEtapaJoin,
            Join<Senha, Veiculo> veiculoJoin,
            Join<Senha, Material> materialJoin,
            Join<Senha, Motorista> motoristaJoin,
            Join<Senha, Cliente> clienteJoin
    ) {

        return builder.construct(SenhaDTO.class,
                root.get(Senha_.id),
                root.get(Senha_.status),
                root.get(Senha_.dataAlteracao),
                root.get(Senha_.senha),
                materialJoin.get(Material_.id),
                materialJoin.get(Material_.codigo),
                materialJoin.get(Material_.descricao),
                root.get(Senha_.dataCadastro),
                veiculoJoin.get(Veiculo_.placa1),
                motoristaJoin.get(Motorista_.nome),
                clienteJoin.get(Cliente_.nome),
                clienteJoin.get(Cliente_.cpfCnpj),
                root.get(Senha_.statusSenha),
                root.get(Senha_.idUsuarioAutorCadastro),
                fluxoEtapaJoin.get(FluxoEtapa_.id),
                fluxoEtapaJoin.get(FluxoEtapa_.etapa),
                fluxoEtapaJoin.get(FluxoEtapa_.sequencia),
                senhaEtapaJoin.get(SenhaEtapa_.qtdRepeticao),
                senhaEtapaJoin.get(SenhaEtapa_.qtdChamada),
                senhaEtapaJoin.get(SenhaEtapa_.horaEntrada),
                senhaEtapaJoin.get(SenhaEtapa_.horaSaida),
                senhaEtapaJoin.get(SenhaEtapa_.statusEtapa),
                builder.sum(senhaEtapaJoin.get(SenhaEtapa_.duracao))
        );
    }

    @Transactional(readOnly = true)
    @Override
    public List<Senha> obterPorDataAlteracaoBeforeEStatusSenhaIn(
            Date from, StatusSenhaEnum... statusSenha
    ) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();
        CriteriaQuery<Senha> criteriaQuery = builder.createQuery(Senha.class);
        Root<Senha> root = criteriaQuery.from(Senha.class);
        criteriaQuery.where(builder.and(predicates(builder, root, from, Arrays.asList(statusSenha)).toArray(new Predicate[]{})));
        criteriaQuery.select(construtorComVariaveisSimples(builder, root));
        TypedQuery<Senha> query = this.getSession().createQuery(criteriaQuery);
        return query.getResultList();
    }

    private CompoundSelection<Senha> construtorComVariaveisSimples(CriteriaBuilder builder, Root<Senha> root) {

        return builder.construct(Senha.class, root.get(Senha_.id));
    }

    private List<Predicate> predicates(
            CriteriaBuilder builder,
            Root<Senha> root,
            Date from,
            List<StatusSenhaEnum> statusSenha
    ) {

        List<Predicate> predicates = new ArrayList<>();
        if (ObjetoUtil.isNotNull(from)) {
            predicates.add(builder.lessThanOrEqualTo(root.get(Senha_.dataAlteracao), DateUtil.converterDateFimDia(from)));
        }
        if (ColecaoUtil.isNotEmpty(statusSenha)) {
            predicates.add(builder.and(root.get(Senha_.statusSenha).in(statusSenha)));
        }
        return predicates;
    }

}
