package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.PerfilFuncionalidade;

import java.util.List;

public interface PerfilFuncionalidadeDAO extends DAO<Long, PerfilFuncionalidade> {

    List<PerfilFuncionalidade> findByPerfilId(final Long id);
}
