package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.cfg.CampoOperacaoMaterial;

import java.util.List;

public interface CampoOperacaoMaterialDAO extends DAO<Long, CampoOperacaoMaterial> {

    List<CampoOperacaoMaterial> findByMaterialId(final Long idMaterial);
}
