package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Laudo;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LaudoDAO extends DAO<Long, Laudo> {

    Laudo findFirstByMaterialIdOrderByDataCadastroDesc(final Long idMaterial);

    Laudo findFirstByMaterialIdAndTanqueOrderByDataCadastroDesc(final Long idMaterial, final String tanque);

    @Query(value = "select new br.com.sjc.modelo.Laudo(l.id, l.numeroCertificado) from Laudo l where l.id in ?1")
    List<Laudo> findLaudoNumeroCertificadoById(List<Long> ids);
}
