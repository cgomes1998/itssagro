package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.sap.Safra;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface SafraDAO extends DAO<Long, Safra> {

    Safra findByCodigo(final String codigo);

    List<Safra> findByStatusCadastroSapIn(final List<StatusCadastroSAPEnum> statusCadastroSAPEnums);

    List<Safra> findByStatusCadastroSapInAndDataCadastroGreaterThanEqual(final List<StatusCadastroSAPEnum> statusCadastroSAPEnums, final Date data);

    List<Safra> findTop10ByStatusAndCodigoIgnoreCaseContainingOrderByCodigoAsc(StatusEnum status, String codigo);

    List<Safra> findTop10ByStatusAndDescricaoIgnoreCaseContainingOrderByCodigoAsc(StatusEnum status, String codigo);
}
