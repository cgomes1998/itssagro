package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.LogAtividade;

public interface LogAtividadeDAO extends DAO<Long, LogAtividade> {

}
