package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Perfil;
import br.com.sjc.modelo.enums.StatusEnum;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PerfilDAO extends DAO<Long, Perfil> {

    List<Perfil> findByStatus(final StatusEnum status);

    boolean existsByNome(String nome);

    @Query("select count(p.id)>0 from Perfil p where p.id <> ?2 and p.nome = ?1")
    boolean existsByNomeAndNeId(final String nome, final Long id);

}
