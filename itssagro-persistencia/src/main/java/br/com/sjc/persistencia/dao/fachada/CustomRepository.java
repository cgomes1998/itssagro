package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.ObjectID;
import org.springframework.data.jpa.domain.Specification;

import java.io.Serializable;

public interface CustomRepository<ID extends Serializable, E extends ObjectID<ID>> {
    E get(Serializable id);

    E get(String atributo, Object valor);

    E get(Specification<E> specification);
}
