package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.modelo.EntidadeAutenticada_;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.sap.TipoVeiculo;
import br.com.sjc.modelo.sap.TipoVeiculo_;
import br.com.sjc.persistencia.dao.fachada.TipoVeiculoCustomRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Service
public class TipoVeiculoCustomRepositoryImplImpl extends CustomRepositoryImpl implements TipoVeiculoCustomRepository {

    @Transactional(readOnly = true)
    @Override
    public List<TipoVeiculo> listarPorStatus(final StatusEnum status) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();
        CriteriaQuery<TipoVeiculo> criteriaQuery = builder.createQuery(TipoVeiculo.class);
        Root<TipoVeiculo> root = criteriaQuery.from(TipoVeiculo.class);
        criteriaQuery.where(builder.equal(root.get(EntidadeAutenticada_.status), status));
        criteriaQuery.orderBy(builder.asc(root.get(TipoVeiculo_.peso)));
        criteriaQuery.select(builder.construct(TipoVeiculo.class, root.get(TipoVeiculo_.id), root.get(TipoVeiculo_.codigo), root.get(TipoVeiculo_.descricao), root.get(TipoVeiculo_.tipo), root.get(TipoVeiculo_.textoTipo),
                root.get(TipoVeiculo_.peso), root.get(TipoVeiculo_.pesoTara), root.get(TipoVeiculo_.pesoLiquido)));
        return this.getSession().createQuery(criteriaQuery).getResultList();
    }

}
