package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.modelo.dto.TipoVeiculoSetaDTO;
import br.com.sjc.modelo.sap.TipoVeiculo;
import br.com.sjc.modelo.sap.TipoVeiculoSeta;
import br.com.sjc.modelo.sap.TipoVeiculoSeta_;
import br.com.sjc.modelo.sap.TipoVeiculo_;
import br.com.sjc.persistencia.dao.fachada.TipoVeiculoSetaCustomRepository;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.*;
import java.util.List;

@Service
public class TipoVeiculoSetaCustomRepositoryImplImpl extends CustomRepositoryImpl implements TipoVeiculoSetaCustomRepository {

    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    @Override
    public List<TipoVeiculoSetaDTO> listarDtoPorTipoVeiculo(final Long idTipoVeiculo) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();
        CriteriaQuery<TipoVeiculoSetaDTO> criteriaQuery = builder.createQuery(TipoVeiculoSetaDTO.class);
        Root<TipoVeiculoSeta> root = criteriaQuery.from(TipoVeiculoSeta.class);
        Join<TipoVeiculoSeta, TipoVeiculo> joinTipoVeiculo = root.join(TipoVeiculoSeta_.tipoVeiculo, JoinType.INNER);
        criteriaQuery.where(builder.equal(joinTipoVeiculo.get(TipoVeiculo_.id), idTipoVeiculo));
        criteriaQuery.orderBy(builder.asc(root.get(TipoVeiculoSeta_.qtd)));
        criteriaQuery.select(builder.construct(
                TipoVeiculoSetaDTO.class,
                root.get(TipoVeiculoSeta_.id),
                joinTipoVeiculo.get(TipoVeiculo_.id),
                root.get(TipoVeiculoSeta_.descricao),
                root.get(TipoVeiculoSeta_.qtd),
                root.get(TipoVeiculoSeta_.padrao)
        ));
        return this.getSession().createQuery(criteriaQuery).getResultList();
    }

    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    @Override
    public List<TipoVeiculoSeta> listarPorTipoVeiculo(final List<Long> idsTipoVeiculo) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();
        CriteriaQuery<TipoVeiculoSeta> criteriaQuery = builder.createQuery(TipoVeiculoSeta.class);
        Root<TipoVeiculoSeta> root = criteriaQuery.from(TipoVeiculoSeta.class);
        Join<TipoVeiculoSeta, TipoVeiculo> joinTipoVeiculo = root.join(TipoVeiculoSeta_.tipoVeiculo, JoinType.INNER);
        criteriaQuery.where(builder.in(joinTipoVeiculo.get("id")).value(idsTipoVeiculo));
        criteriaQuery.orderBy(builder.asc(root.get(TipoVeiculoSeta_.qtd)));
        criteriaQuery.select(builder.construct(
                TipoVeiculoSeta.class,
                root.get(TipoVeiculoSeta_.id),
                joinTipoVeiculo.get(TipoVeiculo_.id),
                root.get(TipoVeiculoSeta_.descricao),
                root.get(TipoVeiculoSeta_.qtd),
                root.get(TipoVeiculoSeta_.padrao)
        ));
        return this.getSession().createQuery(criteriaQuery).getResultList();
    }
}
