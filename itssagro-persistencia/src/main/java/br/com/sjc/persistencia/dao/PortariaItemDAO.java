package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.cfg.PortariaItem;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PortariaItemDAO extends DAO<Long, PortariaItem> {
    @Query("select p from PortariaItem p where p.ativo = true order by p.descricao ASC")
    List<PortariaItem> listarAtivos();
}
