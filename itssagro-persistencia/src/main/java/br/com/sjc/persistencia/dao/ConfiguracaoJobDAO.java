package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.cfg.ConfiguracaoJob;

import java.util.List;

public interface ConfiguracaoJobDAO extends DAO<Long, ConfiguracaoJob> {

    ConfiguracaoJob findByNome(final String nome);

    List<ConfiguracaoJob> findByNomeIgnoreCaseContaining(final String nome);
}
