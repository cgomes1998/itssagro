package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.TabelaValor;
import br.com.sjc.modelo.TabelaValorItem;
import br.com.sjc.modelo.sap.Material;

import java.math.BigDecimal;

public interface TabelaValorItemDAO extends DAO<Long, TabelaValorItem> {

    TabelaValorItem getFirstByTemperaturaAndGrauAlcoolicoAndTabelaValorMaterialId (
            final
            BigDecimal temp,
            final BigDecimal grauAlcoolico,
            final Long idMaterial
    );

}
