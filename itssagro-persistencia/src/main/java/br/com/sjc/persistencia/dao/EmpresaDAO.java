package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.cfg.Empresa;

public interface EmpresaDAO extends DAO<Long, Empresa> {
}
