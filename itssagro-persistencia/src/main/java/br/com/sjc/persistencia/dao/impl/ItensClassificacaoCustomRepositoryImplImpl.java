package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.modelo.Classificacao;
import br.com.sjc.modelo.Classificacao_;
import br.com.sjc.modelo.dto.ItemClassificacaoDepositoDTO;
import br.com.sjc.modelo.dto.RomaneioDepositoDTO;
import br.com.sjc.modelo.sap.ItensClassificacao;
import br.com.sjc.modelo.sap.ItensClassificacao_;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.Romaneio_;
import br.com.sjc.persistencia.dao.fachada.ItensClassificacaoCustomRepository;
import br.com.sjc.util.ColecaoUtil;
import org.springframework.stereotype.Service;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ItensClassificacaoCustomRepositoryImplImpl extends CustomRepositoryImpl implements ItensClassificacaoCustomRepository {

    @Transactional
    @Override
    public List<ItemClassificacaoDepositoDTO> obterDadosDeClassificaoParaRelatorioDeDeposito(List<RomaneioDepositoDTO> romaneioDepositoDTOS) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<ItemClassificacaoDepositoDTO> criteriaQuery = builder.createQuery(ItemClassificacaoDepositoDTO.class);

        Root<ItensClassificacao> root = criteriaQuery.from(ItensClassificacao.class);

        Join<ItensClassificacao, Classificacao> classificacaoJoin = root.join(ItensClassificacao_.classificacao);

        Set<Long> idsClassificao = romaneioDepositoDTOS.stream()
                .map(RomaneioDepositoDTO::getIdClassificacao)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());

        if (ColecaoUtil.isNotEmpty(idsClassificao)) {

            criteriaQuery.where(classificacaoJoin.get(Classificacao_.id).in(idsClassificao));

            criteriaQuery.select(builder.construct(
                    ItemClassificacaoDepositoDTO.class,
                    root.get(ItensClassificacao_.desconto),
                    root.get(ItensClassificacao_.indice),
                    root.get(ItensClassificacao_.percentualDesconto),
                    root.get(ItensClassificacao_.descricaoItem),
                    root.get(ItensClassificacao_.itemClassificao),
                    classificacaoJoin.get(Classificacao_.id)
            ));

            criteriaQuery.orderBy(
                    builder.asc(root.get(ItensClassificacao_.itemClassificao)),
                    builder.asc(classificacaoJoin.get(Classificacao_.id))
            );

            TypedQuery<ItemClassificacaoDepositoDTO> typedQuery = this.getSession().createQuery(criteriaQuery);

            return typedQuery.getResultList();
        }

        return null;
    }

    @Transactional
    @Override
    public List<ItemClassificacaoDepositoDTO> obterDadosDeClassificaoOrigemParaRelatorioDeDeposito(List<RomaneioDepositoDTO> romaneioDepositoDTOS) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<ItemClassificacaoDepositoDTO> criteriaQuery = builder.createQuery(ItemClassificacaoDepositoDTO.class);

        Root<ItensClassificacao> root = criteriaQuery.from(ItensClassificacao.class);

        Join<ItensClassificacao, Romaneio> romaneioJoin = root.join(ItensClassificacao_.romaneio);

        Set<String> numerosRomaneios = romaneioDepositoDTOS.stream()
                .map(RomaneioDepositoDTO::getNumeroRomaneio)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());

        if (ColecaoUtil.isNotEmpty(numerosRomaneios)) {

            criteriaQuery.where(romaneioJoin.get(Romaneio_.numeroRomaneio).in(numerosRomaneios));

            criteriaQuery.select(builder.construct(
                    ItemClassificacaoDepositoDTO.class,
                    root.get(ItensClassificacao_.desconto),
                    root.get(ItensClassificacao_.indice),
                    root.get(ItensClassificacao_.percentualDesconto),
                    root.get(ItensClassificacao_.descricaoItem),
                    root.get(ItensClassificacao_.itemClassificao),
                    romaneioJoin.get(Romaneio_.numeroRomaneio)
            ));

            criteriaQuery.orderBy(
                    builder.asc(root.get(ItensClassificacao_.itemClassificao)),
                    builder.asc(romaneioJoin.get(Romaneio_.numeroRomaneio))
            );

            TypedQuery<ItemClassificacaoDepositoDTO> typedQuery = this.getSession().createQuery(criteriaQuery);

            return typedQuery.getResultList();
        }

        return null;
    }

}
