package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.sap.CondicaoPagamento;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CondicaoPagamentoDAO extends DAO<Long, CondicaoPagamento> {

    @Query("select c.id from CondicaoPagamento c where c.codigo = ?1")
    Long findIdByCodigo(final String codigo);

    List<CondicaoPagamento> findByOrderByCodigoAsc();
}
