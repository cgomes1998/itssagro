package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.LogSenha;

public interface LogSenhaDAO extends DAO<Long, LogSenha> {

}
