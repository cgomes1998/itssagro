package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.modelo.*;
import br.com.sjc.modelo.dto.LogRomaneioDTO;
import br.com.sjc.modelo.sap.LogRestricaoOrdem;
import br.com.sjc.modelo.sap.LogRestricaoOrdem_;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.Romaneio_;
import br.com.sjc.persistencia.dao.fachada.LogRomaneioCustomRepository;
import br.com.sjc.util.ColecaoUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LogRomaneioCustomRepositoryImplImpl extends CustomRepositoryImpl implements LogRomaneioCustomRepository {

    @Transactional(readOnly = true)
    @Override
    public List<LogRomaneioDTO> obterLogsDeLiberacaoPorRomaneio(final Long idRomaneio) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<LogRomaneioDTO> criteriaQuery = builder.createQuery(LogRomaneioDTO.class);

        Root<LogRomaneio> root = criteriaQuery.from(LogRomaneio.class);

        Join<LogRomaneio, Romaneio> romaneioJoin = root.join(LogRomaneio_.romaneio);

        Join<LogRomaneio, LogRomaneioAcoes> logRomaneioAcoesJoin = root.join(LogRomaneio_.logs);

        Join<LogRomaneioAcoes, Usuario> usuarioJoin = logRomaneioAcoesJoin.join(LogRomaneioAcoes_.responsavel);

        criteriaQuery.orderBy(builder.desc(logRomaneioAcoesJoin.get(LogRomaneioAcoes_.dataLiberacao)));

        criteriaQuery.where(
                builder.equal(romaneioJoin.get(Romaneio_.id), idRomaneio)
        );

        criteriaQuery.select(
                builder.construct(
                        LogRomaneioDTO.class,
                        usuarioJoin.get(Usuario_.login),
                        usuarioJoin.get(Usuario_.nome),
                        logRomaneioAcoesJoin.get(LogRomaneioAcoes_.motivo),
                        logRomaneioAcoesJoin.get(LogRomaneioAcoes_.dataLiberacao),
                        logRomaneioAcoesJoin.get(LogRomaneioAcoes_.id)
                )
        );

        List<LogRomaneioDTO> logs = this.getSession().createQuery(criteriaQuery).getResultList();

        if (ColecaoUtil.isEmpty(logs)) {
            return null;
        }

        List<LogRestricaoOrdem> restricoes = obterRestricoesPorLog(logs
                .stream()
                .map(LogRomaneioDTO::getIdLogRomaneioAcoes)
                .collect(Collectors.toList()));

        if (ColecaoUtil.isNotEmpty(restricoes)) {

            logs
                    .stream()
                    .forEach(log -> log.setRestricoes(
                            restricoes
                                    .stream()
                                    .filter(restricao -> restricao.getLogRomaneioAcoes().getId().equals(log.getIdLogRomaneioAcoes()))
                                    .map(LogRestricaoOrdem::getMensagem)
                                    .collect(Collectors.toList())
                    ));
        }

        return logs;
    }

    private List<LogRestricaoOrdem> obterRestricoesPorLog(final List<Long> idsLogRomaneioAcoes) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<LogRestricaoOrdem> criteriaQuery = builder.createQuery(LogRestricaoOrdem.class);

        Root<LogRestricaoOrdem> root = criteriaQuery.from(LogRestricaoOrdem.class);

        Join<LogRestricaoOrdem, LogRomaneioAcoes> logRomaneioAcoesJoin = root.join(LogRestricaoOrdem_.logRomaneioAcoes);

        criteriaQuery.where(
                builder.in(logRomaneioAcoesJoin.get(LogRomaneioAcoes_.id.getName())).value(idsLogRomaneioAcoes)
        );

        criteriaQuery.select(
                builder.construct(
                        LogRestricaoOrdem.class,
                        root.get(LogRestricaoOrdem_.mensagem),
                        logRomaneioAcoesJoin.get(LogRomaneioAcoes_.id)
                )
        );

        return this.getSession().createQuery(criteriaQuery).getResultList();
    }
}
