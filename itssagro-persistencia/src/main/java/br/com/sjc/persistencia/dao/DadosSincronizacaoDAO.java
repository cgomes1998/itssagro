package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.enums.ModuloOperacaoEnum;

import java.util.List;

public interface DadosSincronizacaoDAO extends DAO<Long, DadosSincronizacao> {

    DadosSincronizacao findByEntidade(final DadosSincronizacaoEnum dadosSincronizacaoEnum);

    DadosSincronizacao findByModuloAndOperacao(final ModuloOperacaoEnum modulo, final String operacao);

    DadosSincronizacao findByEntidadeAndModulo(final DadosSincronizacaoEnum entidade, final ModuloOperacaoEnum modulo);

    List<DadosSincronizacao> findByModuloInOrderByOperacaoAsc(final List<ModuloOperacaoEnum> modulos);

    DadosSincronizacao findByModuloInAndOperacao(final List<ModuloOperacaoEnum> modulos, final String operacao);
}
