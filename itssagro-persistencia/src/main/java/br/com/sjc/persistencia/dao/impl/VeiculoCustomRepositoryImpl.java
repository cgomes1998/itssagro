package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.modelo.sap.Veiculo;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.persistencia.dao.fachada.VeiculoCustomRepository;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class VeiculoCustomRepositoryImpl implements VeiculoCustomRepository {

    private static final int CINCO_MINUTOS = 300000;

    @PersistenceContext
    private EntityManager em;

    @Transactional
    @Override
    public List<Veiculo> listarPreCadastro(final Date data) {

        final Criteria criteria = this.novoCriteria();

        criteria.add(Restrictions.eq("status", StatusEnum.ATIVO));

        List<StatusCadastroSAPEnum> status = new ArrayList<StatusCadastroSAPEnum>();

        status.add(StatusCadastroSAPEnum.NAO_ENVIADO);

        status.add(StatusCadastroSAPEnum.RETORNO_COM_ERRO);

        Conjunction conjunctionAnd = Restrictions.conjunction();

        conjunctionAnd.add(Restrictions.le("dataCadastro", data));

        conjunctionAnd.add(Restrictions.in("statusCadastroSap", status));

        Conjunction conjunctionAnd1 = Restrictions.conjunction();

        conjunctionAnd1.add(Restrictions.eq("statusCadastroSap", StatusCadastroSAPEnum.ENVIADO));

        conjunctionAnd1.add(Restrictions.isNull("codigo"));

        conjunctionAnd1.add(Restrictions.le("dataCadastro", new Date(new Date().getTime() - CINCO_MINUTOS)));

        criteria.add(Restrictions.disjunction().add(conjunctionAnd).add(conjunctionAnd1));

        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

        return criteria.list();
    }

    public Session getSession() {

        return (Session) this.em.getDelegate();
    }

    private Criteria novoCriteria() {

        return this.getSession().createCriteria(Veiculo.class);
    }
}
