package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.cfg.Parametro;
import br.com.sjc.modelo.enums.TipoParametroEnum;

import java.util.List;

public interface ParametroDAO extends DAO<Long, Parametro> {

    Parametro findByTipoParametro(final TipoParametroEnum tipoParametro);

    List<Parametro> findByTipoParametroIn(final List<TipoParametroEnum> tipoParametros);
}
