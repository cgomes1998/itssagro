package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.sap.ItemNFPedido;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.Date;

public interface ItemNFPedidoDAO extends DAO<Long, ItemNFPedido> {

    boolean existsByPlacaCavaloIdAndRomaneioStatusRomaneio(final Long idPlaca, final StatusRomaneioEnum statusRomaneio);

    boolean existsByPlacaCavaloIdAndRomaneioStatusRomaneioAndRomaneioIdNot(final Long idPlaca, final StatusRomaneioEnum statusRomaneio, final Long idRomaneio);

    @Query("select count(i.id) > 0 from ItemNFPedido i where i.numeroNfe = ?1 " + "and i.serieNfe = ?2 " + "and (i.dataNfe >= ?3 or i.dataNfe <= ?4)" + "and i.produtor.id = ?5 " + "and i.safra.id = ?6 " + "and i.material.id = ?7 " + "and" +
            " ( i.romaneio.statusIntegracaoSAP <> ?8 and i.romaneio.statusRomaneio <> ?9)" + "and (?10 is null or i.id <> ?10)")
    boolean existsByNumeroNfeAndSerieNfeAndDataNfeAndProdutorIdAndSafraIdAndMaterialIdAndRomaneioStatusIntegracaoSAPOrRomaneioStatusRomaneioAndIdNe(final String nota, final String serie, final Date dataInicio, final Date dataFim,
                                                                                                                                                    final Long idProdutor, final Long idSafra, final Long idMaterial,
                                                                                                                                                    StatusIntegracaoSAPEnum status, final StatusRomaneioEnum statusRomaneio, final Long idItem);

    @Query("select count(i.romaneio.id) > 0 from ItemNFPedido i where i.numeroNotaFiscalTransporte = ?1 " + "and ( i.romaneio.statusIntegracaoSAP <> ?2 and i.romaneio.statusRomaneio <> ?3)" + "and (?4 is null or i.romaneio.id <> ?4)")
    boolean existsByNumeroNotaFiscalTransporteAndRomaneioStatusIntegracaoSAPInOrRomaneioStatusRomaneioInAndRomaneioIdNe(final String nota, final StatusIntegracaoSAPEnum status, final StatusRomaneioEnum statusRomaneio,
                                                                                                                        final Long idRomaneio);

    @Transactional
    @Modifying
    @Query("update ItemNFPedido i set i.documentoFaturamento = ?1 where i.requestKey = ?2 and i.romaneio = (select r from Romaneio r where r.numeroRomaneio = ?3)")
    void updateDocumentoFaturamento(Long documentoFaturamento, String requestKey, String numeroRomaneio);

    @Transactional
    @Modifying
    @Query("update ItemNFPedido i set i.requestKey = ?1 where i.id = ?2")
    void updateRequestKey(String requestKey, Long id);

}
