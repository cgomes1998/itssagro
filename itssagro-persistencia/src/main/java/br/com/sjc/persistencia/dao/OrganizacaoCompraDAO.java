package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.sap.OrganizacaoCompra;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrganizacaoCompraDAO extends DAO<Long, OrganizacaoCompra> {

    @Query("select o.id from OrganizacaoCompra o where o.codigo = ?1")
    Long findIdByCodigo(final String codigo);

    List<OrganizacaoCompra> findByOrderByCodigoAsc();
}
