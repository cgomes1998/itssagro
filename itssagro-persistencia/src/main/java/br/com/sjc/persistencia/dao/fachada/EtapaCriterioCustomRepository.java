package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.dto.EtapaCriterioDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

public interface EtapaCriterioCustomRepository {

    @Transactional
    List<EtapaCriterioDTO> obterQuantidadeRepeticoesDaEtapa(Set<Long> idsFluxosEtapa);
}
