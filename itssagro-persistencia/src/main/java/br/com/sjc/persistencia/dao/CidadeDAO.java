package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Cidade;
import br.com.sjc.modelo.enums.UFEnum;

import java.io.Serializable;
import java.util.List;

public interface CidadeDAO extends DAO<Long, Cidade>, Serializable {

    List<Cidade> findByEstado(UFEnum uf);

}