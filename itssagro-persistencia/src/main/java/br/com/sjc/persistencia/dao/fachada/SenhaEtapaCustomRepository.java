package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.FluxoEtapa;
import br.com.sjc.modelo.Senha;
import br.com.sjc.modelo.SenhaEtapa;
import br.com.sjc.modelo.dto.*;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.enums.StatusSenhaEnum;
import br.com.sjc.modelo.sap.Material;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface SenhaEtapaCustomRepository {

    @Transactional
    List<SenhaEtapaRelatorioDTO> obterSenhasEtapasPorFluxoEtapas(
            Date data,
            Date dataFim,
            List<FluxoEtapaDTO> fluxoEtapas
    );
}
