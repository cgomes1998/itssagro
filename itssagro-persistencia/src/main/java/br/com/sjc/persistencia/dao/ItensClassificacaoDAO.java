package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.sap.ItensClassificacao;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.math.BigDecimal;

public interface ItensClassificacaoDAO extends DAO<Long, ItensClassificacao> {

	@Transactional
	@Modifying
	@Query("update ItensClassificacao t set t.desconto= ?2, t.descontoClassificacao=?3 where t.id=?1")
	void updateItensClassificacao(final Long id, final BigDecimal desconto, final BigDecimal descontoClassificacao);

}