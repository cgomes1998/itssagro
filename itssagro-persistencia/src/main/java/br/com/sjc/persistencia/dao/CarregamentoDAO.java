package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Carregamento;

public interface CarregamentoDAO extends DAO<Long, Carregamento> {

}
