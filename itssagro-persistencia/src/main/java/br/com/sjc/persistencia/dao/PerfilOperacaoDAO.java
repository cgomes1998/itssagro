package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.PerfilOperacao;

import java.util.List;

public interface PerfilOperacaoDAO extends DAO<Long, PerfilOperacao> {

    List<PerfilOperacao> findByPerfilId(final Long id);
}
