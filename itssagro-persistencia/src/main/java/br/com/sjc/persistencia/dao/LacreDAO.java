package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Lacre;
import br.com.sjc.modelo.Motivo;
import br.com.sjc.modelo.enums.StatusLacreEnum;
import br.com.sjc.modelo.enums.StatusLacreRomaneioEnum;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface LacreDAO extends DAO<Long, Lacre> {

    boolean existsByCodigo(String codigo);

    List<Lacre> findTop10ByCodigoIgnoreCaseContainingAndStatusLacreOrderByCodigoAsc(String value, StatusLacreEnum statusLacre);

    @Transactional
    @Modifying
    @Query("update Lacre l set l.statusLacre = ?1 where l.id=?2")
    void alterarStatusDoLacre(final StatusLacreEnum statusLacre, final Long id);

    @Transactional
    @Modifying
    @Query("update Lacre l set l.statusLacre = ?1, l.observacaoStatusLacreAlterar=?2 where l.id=?3")
    void alterarStatusLacreEObservacaoStatusLacreAlteracao(final StatusLacreEnum statusLacre, final String observacao, final Long id);

    @Transactional
    @Modifying
    @Query("update Lacre l set l.statusLacre = ?2, l.dataInutilizacao = ?3, l.usuarioQueInutilizou.id=?4, l.motivoInutilizacao.id=?5 where l.id in (?1)")
    void alterarStatusDoLacres(final List<Long> lacresId, final StatusLacreEnum statusLacre, final Date date, final Long usuarioId, final Long motivoId);

    @Transactional
    @Modifying
    @Query("update LacreRomaneio l set l.statusLacreRomaneio = ?1 where l.lacre.id in (?2)")
    void alterarStatusDoLacreRomaneios(final StatusLacreRomaneioEnum statusLacreRomaneio, final List<Long> lacresIds);
}
