package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Lote;
import br.com.sjc.modelo.dto.LoteDTO;
import br.com.sjc.modelo.enums.StatusLacreEnum;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LoteDAO extends DAO<Long, Lote> {

    @Query(value = "SELECT new br.com.sjc.modelo.dto.LoteDTO(l.id) FROM Lote l LEFT JOIN l.lacres lacre WHERE lacre.statusLacre = :statusLacre and l.id IN :lotesId "
            + "GROUP BY l.id "
            + "HAVING COUNT( lacre.id) = (select count(lacre2.id) FROM  Lacre lacre2 WHERE lacre2.lote.id = l.id)")
    List<LoteDTO> getLotesComTodosLacresStatusIgualA(@Param("statusLacre") StatusLacreEnum statusLacre, @Param("lotesId") List<Long> lotesId);

}
