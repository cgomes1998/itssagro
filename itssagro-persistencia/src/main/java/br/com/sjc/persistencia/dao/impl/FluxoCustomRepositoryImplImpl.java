package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.modelo.Fluxo;
import br.com.sjc.modelo.FluxoMaterial;
import br.com.sjc.modelo.FluxoMaterial_;
import br.com.sjc.modelo.Fluxo_;
import br.com.sjc.modelo.dto.FluxoDTO;
import br.com.sjc.modelo.dto.MaterialDTO;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.modelo.sap.Material_;
import br.com.sjc.persistencia.dao.fachada.FluxoCustomRepository;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class FluxoCustomRepositoryImplImpl extends CustomRepositoryImpl implements FluxoCustomRepository {

    @Transactional
    @Override
    public List<MaterialDTO> listarMateriaisDoFluxo(List<Long> idFluxos) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<MaterialDTO> criteriaQuery = builder.createQuery(MaterialDTO.class);

        Root<Fluxo> root = criteriaQuery.from(Fluxo.class);

        Join<Fluxo, FluxoMaterial> fluxoFluxoMaterialJoin = root.join(Fluxo_.materiais);

        Join<FluxoMaterial, Material> materialJoin = fluxoFluxoMaterialJoin.join(FluxoMaterial_.material);

        criteriaQuery.where(root.get(Fluxo_.id).in(idFluxos));

        criteriaQuery.select(builder.construct(MaterialDTO.class, materialJoin.get(Material_.id), materialJoin.get(Material_.codigo), materialJoin.get(Material_.descricao)));

        criteriaQuery.orderBy(builder.asc(materialJoin.get(Material_.descricao)));

        return this.getSession().createQuery(criteriaQuery).getResultList();
    }

    @Transactional
    @Override
    public List<FluxoDTO> listarOrdernandoDeFormaCrescentePorCodigo() {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<FluxoDTO> criteriaQuery = builder.createQuery(FluxoDTO.class);

        Root<Fluxo> root = criteriaQuery.from(Fluxo.class);

        criteriaQuery.select(builder.construct(FluxoDTO.class, root.get(Fluxo_.id), root.get(Fluxo_.codigo), root.get(Fluxo_.descricao), root.get(Fluxo_.tipoFluxo)));

        criteriaQuery.orderBy(builder.asc(root.get(Fluxo_.codigo)));

        return this.getSession().createQuery(criteriaQuery).getResultList();
    }

}
