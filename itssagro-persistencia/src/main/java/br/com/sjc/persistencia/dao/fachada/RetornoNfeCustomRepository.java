package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.dto.NfeDepositoDTO;
import br.com.sjc.modelo.dto.RetornoNfeDTO;
import br.com.sjc.modelo.dto.RomaneioDepositoDTO;
import br.com.sjc.modelo.sap.RetornoNFe;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

public interface RetornoNfeCustomRepository {

    @Transactional
    List<NfeDepositoDTO> obterDadosDeNotaParaRelatorioDeDeposito(List<RomaneioDepositoDTO> romaneioDepositoDTOS);

    @Transactional
    List<RetornoNFe> obterIdRetornoNFePorNfe(Set<String> notas);

    @Transactional
	List<RetornoNfeDTO> obterRetornoNFePorRomaneios(List<String> numRomaneios);
}
