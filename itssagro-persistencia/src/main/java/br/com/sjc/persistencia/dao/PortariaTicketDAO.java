package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.PortariaTicket;
import br.com.sjc.modelo.cfg.PortariaMotivo;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PortariaTicketDAO extends DAO<Long, PortariaTicket> {
}
