package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.dto.ItemClassificacaoDepositoDTO;
import br.com.sjc.modelo.dto.RomaneioDepositoDTO;

import javax.transaction.Transactional;
import java.util.List;

public interface ItensClassificacaoCustomRepository {

    @Transactional
    List<ItemClassificacaoDepositoDTO> obterDadosDeClassificaoParaRelatorioDeDeposito(List<RomaneioDepositoDTO> romaneioDepositoDTOS);

    @Transactional
    List<ItemClassificacaoDepositoDTO> obterDadosDeClassificaoOrigemParaRelatorioDeDeposito(List<RomaneioDepositoDTO> romaneioDepositoDTOS);
}
