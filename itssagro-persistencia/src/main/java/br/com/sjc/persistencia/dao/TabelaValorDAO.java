package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.TabelaValor;
import br.com.sjc.modelo.sap.Material;

public interface TabelaValorDAO extends DAO<Long, TabelaValor> {

    boolean existsByMaterial(Material material);

}
