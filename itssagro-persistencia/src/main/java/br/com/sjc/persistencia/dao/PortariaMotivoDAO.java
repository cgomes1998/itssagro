package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.cfg.Portaria;
import br.com.sjc.modelo.cfg.PortariaMotivo;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PortariaMotivoDAO extends DAO<Long, PortariaMotivo> {
    @Query("select p from PortariaMotivo p where p.ativo = true order by p.descricao ASC")
    List<PortariaMotivo> listarAtivos();
}
