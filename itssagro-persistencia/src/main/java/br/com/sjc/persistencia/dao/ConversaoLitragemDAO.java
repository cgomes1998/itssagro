package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.sap.ConversaoLitragem;

public interface ConversaoLitragemDAO extends DAO<Long, ConversaoLitragem> {

}
