package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Classificacao;

public interface ClassificacaoDAO extends DAO<Long, Classificacao> {

}
