package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.RecebimentoNfeSapCargaPontual;

public interface RecebimentoNfeSapCargaPontualDAO extends DAO<Long, RecebimentoNfeSapCargaPontual> {

}
