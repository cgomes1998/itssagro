package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Hardware;
import br.com.sjc.modelo.enums.StatusEnum;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by julio.bueno on 02/07/2019.
 */
public interface HardwareDAO extends DAO<Long, Hardware> {

    @Query("select max(codigo) from Hardware")
    Long getMaxCodigo();

    @Query("select count(id) > 0 from Hardware where lower(identificacao) = lower(?2) and id != ?1")
    boolean existsOutroComMesmaIdentificacao(Long id, String identificacao);

    List<Hardware> findByStatus(StatusEnum status);
}
