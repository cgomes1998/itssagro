package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.Laudo;
import br.com.sjc.modelo.dto.AnaliseFiltroDTO;
import br.com.sjc.modelo.sap.Romaneio;

import javax.transaction.Transactional;

public interface AnaliseQualidadeCustomRepository {

    @Transactional
    Romaneio obterAnaliseQualidadeParaCarregamento(AnaliseFiltroDTO filtroDTO, Laudo laudo);

}
