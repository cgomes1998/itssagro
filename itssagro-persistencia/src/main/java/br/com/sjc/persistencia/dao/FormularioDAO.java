package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Formulario;
import br.com.sjc.modelo.enums.FormularioStatus;

import java.util.List;

/**
 * Created by julio.bueno on 04/07/2019.
 */
public interface FormularioDAO extends DAO<Long, Formulario> {
    List<Formulario> findByFormularioStatus(FormularioStatus publicado);
}
