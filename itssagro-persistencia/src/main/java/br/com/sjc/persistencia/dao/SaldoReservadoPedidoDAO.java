package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.SaldoReservadoPedido;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;

public interface SaldoReservadoPedidoDAO extends DAO<Long, SaldoReservadoPedido> {

    SaldoReservadoPedido findFirstByNumeroRomaneioConsumidorAndNumeroPedidoAndItemPedido(String numeroRomaneioConsumidor, String numeroPedido, String itemPedido);

    @Query("select coalesce(sum(s.saldoReservado), 0) as saldoReservado from SaldoReservadoPedido s where numeroPedido = ?1 and itemPedido = ?2 ")
    BigDecimal obterSaldoReservado(String numeroPedido, String itemPedido);

    @Query("select coalesce(sum(s.saldoReservado), 0) as saldoReservado from SaldoReservadoPedido s where s.numeroRomaneioConsumidor != ?1 and numeroPedido = ?2 and itemPedido = ?3 ")
    BigDecimal obterSaldoReservado(String numeroRomaneioConsumidor, String numeroPedido, String itemPedido);
}
