package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.dto.MaterialDTO;
import br.com.sjc.modelo.enums.StatusRegistroEnum;

import java.util.List;

public interface MaterialCustomRepository {

    @org.springframework.transaction.annotation.Transactional( readOnly = true )
    List<MaterialDTO> listarPorStatus (
            StatusRegistroEnum statusRegistro
    );
}
