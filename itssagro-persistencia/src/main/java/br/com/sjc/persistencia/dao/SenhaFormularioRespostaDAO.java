package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.SenhaFormularioResposta;

import java.util.List;

public interface SenhaFormularioRespostaDAO extends DAO<Long, SenhaFormularioResposta> {

    List<SenhaFormularioResposta> findBySenhaFormularioId(final Long idSenha);
}
