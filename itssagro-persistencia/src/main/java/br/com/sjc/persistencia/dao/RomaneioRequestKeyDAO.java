package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.dto.RomaneioRequestKeyDTO;
import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.sap.RomaneioRequestKey;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface RomaneioRequestKeyDAO extends DAO<Long, RomaneioRequestKey> {

    RomaneioRequestKey findByRequestKeyAndRomaneioOperacaoOperacao(
            final String request,
            final String operacao
    );

    @Query("select new br.com.sjc.modelo.dto.RomaneioRequestKeyDTO(rk.romaneio.operacao.modulo, rk.romaneio.operacao.operacao, rk.requestKey, rk.romaneio.numeroRomaneio) from RomaneioRequestKey rk where rk.romaneio.id in ?1")
    List<RomaneioRequestKeyDTO> findByRomaneioIdIn(final List<Long> ids);

    @Transactional
    @Modifying
    @Query("update Romaneio r set r.statusIntegracaoSAP = ?3 where r.id in (select rk.romaneio.id from RomaneioRequestKey rk where rk.requestKey = ?1 and rk.romaneio.operacao.operacao = ?2)")
    void updateStatusSAPRomaneioPorChaveEOperacao(
            final String chave,
            final String operacao,
            final StatusIntegracaoSAPEnum statusIntegracaoSAP
    );
}
