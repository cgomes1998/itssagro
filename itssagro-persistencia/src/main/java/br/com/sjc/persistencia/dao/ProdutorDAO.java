package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.sap.Produtor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

public interface ProdutorDAO extends DAO<Long, Produtor> {

    @Query("select p.codigo from Produtor p where p.id = ?1")
    String findCodigoById(Long id);

    Produtor findByCodigo(final String codigo);

    Produtor findByCpfOrCnpjAndSenha(final String cpf, final String cnpj, final String senha);

    List<Produtor> findTop10ByGrpContaProdutorInAndStatusCadastroSapAndNomeIgnoreCaseContainingOrCodigoIgnoreCaseContainingOrCnpjIgnoreCaseContainingOrCpfIgnoreCaseContainingOrderByCodigoAsc(List<String> tipos, StatusCadastroSAPEnum statusCadastroSAP, String nome, String codigo, String cnpj, String cpf);

    List<Produtor> findByStatusCadastroSapIn(final List<StatusCadastroSAPEnum> statusCadastroSAPEnums);

    List<Produtor> findByStatusCadastroSapInAndDataCadastroGreaterThanEqual(final List<StatusCadastroSAPEnum> statusCadastroSAPEnums, final Date data);

    List<Produtor> findByCnpjInOrCpfIn(final List<String> cnpjs, final List<String> cpfs);

    @Transactional
    @Modifying
    @Query("update Produtor p set p.senha = ?1 where p.id = ?2")
    void updateSenha(final String senha, final Long id);
}
