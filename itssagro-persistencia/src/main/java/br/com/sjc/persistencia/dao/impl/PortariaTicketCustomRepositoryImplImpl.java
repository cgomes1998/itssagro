package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.modelo.PortariaTicket;
import br.com.sjc.persistencia.dao.fachada.PortariaTicketCustomRepository;
import br.com.sjc.util.ObjetoUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PortariaTicketCustomRepositoryImplImpl extends CustomRepositoryImpl implements PortariaTicketCustomRepository {

    @Transactional
    @Override
    public List<PortariaTicket> obterDadosRelatorioPortariaTicket(PortariaTicket filtro) {
        CriteriaBuilder criteriaBuilder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<PortariaTicket> criteriaQuery = criteriaBuilder.createQuery(PortariaTicket.class);

        Root<PortariaTicket> root = criteriaQuery.from(PortariaTicket.class);

        List<Predicate> predicates = new ArrayList<>();

        if (ObjetoUtil.isNotNull(filtro.getPortaria())) {
            predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("portaria"), filtro.getPortaria())));
        }

        if (ObjetoUtil.isNotNull(filtro.getDataEntrada())) {
            Expression<Date> dataEntradaFiltro = criteriaBuilder.function("date", Date.class, root.get("dataEntrada"));
            Expression<Date> dataMarcacaoFiltro = criteriaBuilder.function("date", Date.class, root.get("dataMarcacao"));

            predicates.add(
                criteriaBuilder.and(
                    criteriaBuilder.or(
                        criteriaBuilder.greaterThanOrEqualTo(dataEntradaFiltro, filtro.getDataEntrada()),
                        criteriaBuilder.greaterThanOrEqualTo(dataMarcacaoFiltro, filtro.getDataEntrada())
                    )
                )
            );
        }

        if (ObjetoUtil.isNotNull(filtro.getDataSaida())) {
            Expression<Date> dataSaidaFiltro = criteriaBuilder.function("date", Date.class, root.get("dataSaida"));

            List<Predicate> predicateDataSaida = new ArrayList<>();

            if(filtro.isEstorno()){
                Expression<Date> dataEstornoFiltro = criteriaBuilder.function("date", Date.class, root.get("dataEstorno"));

                predicateDataSaida.add(
                    criteriaBuilder.and(
                        criteriaBuilder.or(
                            criteriaBuilder.lessThanOrEqualTo(dataSaidaFiltro, filtro.getDataSaida()),
                            criteriaBuilder.isNull(root.get("dataSaida")),
                            criteriaBuilder.lessThanOrEqualTo(dataEstornoFiltro, filtro.getDataSaida()),
                            criteriaBuilder.isNull(root.get("dataEstorno"))
                        )
                    )
                );
            } else {
                predicateDataSaida.add(
                    criteriaBuilder.and(
                        criteriaBuilder.lessThanOrEqualTo(dataSaidaFiltro, filtro.getDataSaida())
                    )
                );
            }

            predicates.add(criteriaBuilder.or(predicateDataSaida.toArray(new Predicate[]{})));
        }

        if (ObjetoUtil.isNotNull(filtro.getProduto())) {
            predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("produto"), filtro.getProduto())));
        }

        if (ObjetoUtil.isNotNull(filtro.getPlacaVeiculo())) {
            predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("placaVeiculo"), filtro.getPlacaVeiculo())));
        }

        if(!filtro.isEstorno()){
            predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("estorno"), false)));
        }

        criteriaQuery.where(predicates.toArray(new Predicate[]{}));

        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("dataEntrada")));

        TypedQuery<PortariaTicket> typedQuery = this.getSession().createQuery(criteriaQuery);

        return typedQuery.getResultList();
    }
}
