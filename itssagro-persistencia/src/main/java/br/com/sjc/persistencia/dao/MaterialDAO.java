package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.sap.Material;

import java.util.Date;
import java.util.List;

public interface MaterialDAO extends DAO<Long, Material> {
    
    Material findFirstByCodigo(final String codigo);
    
    List<Material> findByStatusCadastroSapIn(final List<StatusCadastroSAPEnum> statusCadastroSAPEnums);
    
    List<Material> findByStatusCadastroSapInAndDataCadastroGreaterThanEqual(final List<StatusCadastroSAPEnum> statusCadastroSAPEnums, final Date data);
    
}
