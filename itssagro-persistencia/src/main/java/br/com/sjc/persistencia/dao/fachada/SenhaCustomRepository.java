package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.FluxoEtapa;
import br.com.sjc.modelo.Senha;
import br.com.sjc.modelo.dto.FluxoEtapaDTO;
import br.com.sjc.modelo.dto.SenhaDTO;
import br.com.sjc.modelo.dto.TempoPorEtapaDTO;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.enums.StatusSenhaEnum;
import br.com.sjc.modelo.sap.Material;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface SenhaCustomRepository {

    @Transactional(readOnly = true)
    List<TempoPorEtapaDTO> obterTempoMedioPorEtapaEnumESenhas(List<Long> ids, List<EtapaEnum> etapas);

    @Transactional(readOnly = true)
    List<TempoPorEtapaDTO> obterTempoMedioPorEtapaESenhas(List<Long> ids, List<FluxoEtapa> etapas);

    @Transactional
    List<SenhaDTO> listarPorMaterialEPlacaENumeroRomaneioENumeroCartaoAcesso(
            List<Material> materiais,
            String placa,
            String numeroRomaneio,
            String numeroCartaoAcesso,
            Date data,
            List<FluxoEtapaDTO> fluxoEtapas,
            List<StatusSenhaEnum> statusSenha
    );

    @Transactional(readOnly = true)
    List<Senha> obterPorDataAlteracaoBeforeEStatusSenhaIn(
            Date from,
            StatusSenhaEnum... statusSenha
    );
}
