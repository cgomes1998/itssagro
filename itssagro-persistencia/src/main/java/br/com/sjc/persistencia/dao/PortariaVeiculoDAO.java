package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.cfg.PortariaMotivo;
import br.com.sjc.modelo.cfg.PortariaVeiculo;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PortariaVeiculoDAO extends DAO<Long, PortariaVeiculo> {
    @Query("select p from PortariaVeiculo p where p.ativo = true order by p.descricao ASC")
    List<PortariaVeiculo> listarAtivos();
}
