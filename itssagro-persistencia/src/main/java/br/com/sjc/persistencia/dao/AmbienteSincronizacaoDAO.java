package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.cfg.AmbienteSincronizacao;

public interface AmbienteSincronizacaoDAO extends DAO<Long, AmbienteSincronizacao> {

}
