package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.modelo.*;
import br.com.sjc.modelo.dto.*;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.persistencia.dao.fachada.SenhaEtapaCustomRepository;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SenhaEtapaCustomRepositoryImplImpl extends CustomRepositoryImpl implements SenhaEtapaCustomRepository {

    @Transactional(readOnly = true)
    @Override
    public List<SenhaEtapaRelatorioDTO> obterSenhasEtapasPorFluxoEtapas(
            Date data,
            Date dataFim,
            List<FluxoEtapaDTO> fluxoEtapas
    ) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<SenhaEtapaRelatorioDTO> criteriaQuery = builder.createQuery(SenhaEtapaRelatorioDTO.class);

        Root<SenhaEtapa> root = criteriaQuery.from(SenhaEtapa.class);

        Join<SenhaEtapa, Senha> senhaJoin = root.join(SenhaEtapa_.senha);

        Join<SenhaEtapa, FluxoEtapa> fluxoEtapaJoin = root.join(SenhaEtapa_.etapa);

        Join<Senha, Material> materialJoin = senhaJoin.join(Senha_.material, JoinType.LEFT);

        Join<Senha, Cliente> clienteJoin = senhaJoin.join(Senha_.cliente, JoinType.LEFT);

        Join<Senha, Veiculo> veiculoJoin = senhaJoin.join(Senha_.veiculo, JoinType.LEFT);

        Join<Senha, Motorista> motoristaJoin = senhaJoin.join(Senha_.motorista, JoinType.LEFT);

        criteriaQuery.distinct(true);

        List<Predicate> predicados = new ArrayList<>();

        predicados.add(root.get(SenhaEtapa_.ETAPA).in(fluxoEtapas.stream().map(FluxoEtapaDTO::getId).collect(Collectors.toSet())));

        if (ObjetoUtil.isNotNull(data)) {
            predicados.add(builder.greaterThanOrEqualTo(senhaJoin.get(Senha_.dataCadastro), data));
        }

        if(ObjetoUtil.isNotNull(dataFim)){
            predicados.add(builder.lessThanOrEqualTo(senhaJoin.get(Senha_.dataCadastro), DateUtil.converterDateFimDia(dataFim)));
        }

        criteriaQuery.where(builder.and(predicados.toArray(new Predicate[]{})));

        criteriaQuery.select(getConstructDTO(builder, root, senhaJoin, materialJoin, clienteJoin, veiculoJoin, fluxoEtapaJoin, motoristaJoin));

        return this.getSession().createQuery(criteriaQuery).getResultList();
    }

    private CompoundSelection<SenhaEtapaRelatorioDTO> getConstructDTO(
            CriteriaBuilder builder,
            Root<SenhaEtapa> root,
            Join<SenhaEtapa, Senha> senhaJoin,
            Join<Senha, Material> materialJoin,
            Join<Senha, Cliente> clienteJoin,
            Join<Senha, Veiculo> veiculoJoin,
            Join<SenhaEtapa, FluxoEtapa> fluxoEtapaJoin,
            Join<Senha, Motorista> motoristaJoin
    ) {

        return builder.construct(SenhaEtapaRelatorioDTO.class,
                root.get(SenhaEtapa_.ID),
                senhaJoin.get(Senha_.SENHA),
                senhaJoin.get(Senha_.ID),
                senhaJoin.get(Senha_.CODIGO_ORDEM_VENDA),
                materialJoin.get(Material_.CODIGO),
                materialJoin.get(Material_.DESCRICAO),
                clienteJoin.get(Cliente_.NOME),
                motoristaJoin.get(Motorista_.NOME),
                veiculoJoin.get(Veiculo_.PLACA1),
                senhaJoin.get(Senha_.NUMERO_CARTAO_ACESSO),
                fluxoEtapaJoin.get(FluxoEtapa_.ETAPA),
                root.get(SenhaEtapa_.QTD_REPETICAO),
                root.get(SenhaEtapa_.QTD_CHAMADA),
                root.get(SenhaEtapa_.HORA_ENTRADA),
                root.get(SenhaEtapa_.HORA_SAIDA),
                root.get(SenhaEtapa_.STATUS_ETAPA)
        );
    }
}
