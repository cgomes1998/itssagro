package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.sap.Bridge;

import java.util.List;

public interface BridgeDAO extends DAO<Long, Bridge> {

    List<Bridge> findByRomaneioId(final Long id);

    boolean existsByRomaneioId(final Long id);
}
