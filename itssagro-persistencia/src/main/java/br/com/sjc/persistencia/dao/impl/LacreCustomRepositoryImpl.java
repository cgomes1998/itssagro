package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.modelo.*;
import br.com.sjc.modelo.dto.LacreCountDTO;
import br.com.sjc.modelo.dto.RelatorioLacreDTO;
import br.com.sjc.modelo.dto.RelatorioLacreFiltroDTO;
import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusLacreEnum;
import br.com.sjc.modelo.enums.StatusLacreRomaneioEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.modelo.view.VwLacreDetalhe;
import br.com.sjc.modelo.view.VwLacreDetalhe_;
import br.com.sjc.persistencia.dao.fachada.LacreCustomRepository;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.DateUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.hibernate.Session;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class LacreCustomRepositoryImpl implements LacreCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    @Override
    public List<RelatorioLacreDTO> obterRelatorioDeLacresSemVinculoARomaneio(RelatorioLacreFiltroDTO relatorioLacreFiltro) {
        CriteriaBuilder criteriaBuilder = getSession().getCriteriaBuilder();
        CriteriaQuery<RelatorioLacreDTO> criteriaQuery = criteriaBuilder.createQuery(RelatorioLacreDTO.class);
        Root<Lacre> root = criteriaQuery.from(Lacre.class);
        if (ObjetoUtil.isNotNull(relatorioLacreFiltro)) {
            List<Predicate> predicates = new ArrayList<>();
            if (ObjetoUtil.isNotNull(relatorioLacreFiltro.getStatus())) {
                StatusLacreEnum statusLacre = StatusLacreEnum.valueOf(relatorioLacreFiltro.getStatus());
                if (ObjetoUtil.isNull(statusLacre)) {
                    return null;
                }
                predicates.add(criteriaBuilder.equal(root.get(Lacre_.statusLacre), statusLacre));
            }
            if (ObjetoUtil.isNotNull(relatorioLacreFiltro.getDataUtilizacaoInicial())) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(Lacre_.dataCadastro), DateUtil.obterDataHoraZerada(relatorioLacreFiltro.getDataUtilizacaoInicial())));
            }
            if (ObjetoUtil.isNotNull(relatorioLacreFiltro.getDataUtilizacaoFinal())) {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(Lacre_.dataCadastro), DateUtil.converterDateFimDia(relatorioLacreFiltro.getDataUtilizacaoFinal())));
            }
            criteriaQuery.where(predicates.toArray(new Predicate[0]));
        }
        criteriaQuery.select(
                criteriaBuilder.construct(
                        RelatorioLacreDTO.class,
                        root.get(Lacre_.codigo),
                        root.get(Lacre_.statusLacre)
                )
        );
        criteriaQuery.groupBy(
                root.get(Lacre_.codigo),
                root.get(Lacre_.statusLacre)
        );
        TypedQuery<RelatorioLacreDTO> typedQuery = getSession().createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    @Transactional
    @Override
    public List<RelatorioLacreDTO> obterRelatorioDeLacresVinculadosARomaneio(RelatorioLacreFiltroDTO relatorioLacreFiltro) {
        CriteriaBuilder criteriaBuilder = getSession().getCriteriaBuilder();
        CriteriaQuery<RelatorioLacreDTO> criteriaQuery = criteriaBuilder.createQuery(RelatorioLacreDTO.class);
        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);
        Join<Romaneio, Lacragem> lacragemJoin = root.join(Romaneio_.lacragem);
        Join<Romaneio, RetornoNFe> retornoNFeJoin = root.join(Romaneio_.retornoNfes, JoinType.LEFT);
        Join<Lacragem, LacreRomaneio> lacreRomaneioJoin = lacragemJoin.join(Lacragem_.lacres);
        Join<LacreRomaneio, Usuario> lacreRomaneioUsuarioJoin = lacreRomaneioJoin.join("usuarioQueUtilizou", JoinType.LEFT);
        Join<LacreRomaneio, Lacre> lacreJoin = lacreRomaneioJoin.join(LacreRomaneio_.lacre);
        Join<Lacre, Usuario> lacreUsuarioJoin = lacreJoin.join(Lacre_.usuarioQueInutilizou, JoinType.LEFT);
        Join<Romaneio, ItemNFPedido> itemNFPedidoJoin = root.join(Romaneio_.itens);
        Join<ItemNFPedido, Cliente> clienteJoin = itemNFPedidoJoin.join(ItemNFPedido_.cliente, JoinType.LEFT);
        Join<ItemNFPedido, Veiculo> veiculoJoin = itemNFPedidoJoin.join(ItemNFPedido_.placaCavalo, JoinType.LEFT);
        Join<Veiculo, TipoVeiculo> tipoVeiculoJoin = veiculoJoin.join(Veiculo_.tipo, JoinType.LEFT);
        List<Predicate> predicates = new ArrayList<>();
        if (ObjetoUtil.isNotNull(relatorioLacreFiltro)) {
            if (ObjetoUtil.isNotNull(relatorioLacreFiltro.getStatus())) {
                StatusLacreRomaneioEnum statusLacreRomaneio = StatusLacreRomaneioEnum.valueOf(relatorioLacreFiltro.getStatus());
                if (ObjetoUtil.isNotNull(statusLacreRomaneio)) {
                    predicates.add(criteriaBuilder.equal(lacreRomaneioJoin.get(LacreRomaneio_.statusLacreRomaneio), statusLacreRomaneio));
                }
            }
            if (ObjetoUtil.isNotNull(relatorioLacreFiltro.getDataUtilizacaoInicial())) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(lacreRomaneioJoin.get(LacreRomaneio_.dataUtilizacao), DateUtil.obterDataHoraZerada(relatorioLacreFiltro.getDataUtilizacaoInicial())));
            }
            if (ObjetoUtil.isNotNull(relatorioLacreFiltro.getDataUtilizacaoFinal())) {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(lacreRomaneioJoin.get(LacreRomaneio_.dataUtilizacao), DateUtil.converterDateFimDia(relatorioLacreFiltro.getDataUtilizacaoFinal())));
            }
            criteriaQuery.where(predicates.toArray(new Predicate[0]));
        }
        criteriaQuery.select(
                criteriaBuilder.construct(
                        RelatorioLacreDTO.class,
                        root.get(Romaneio_.numeroRomaneio),
                        root.get(Romaneio_.statusRomaneio),
                        root.get(Romaneio_.statusIntegracaoSAP),
                        veiculoJoin.get(Veiculo_.placa1),
                        tipoVeiculoJoin.get(TipoVeiculo_.descricao),
                        lacreJoin.get(Lacre_.codigo),
                        lacreJoin.get(Lacre_.statusLacre),
                        lacreRomaneioJoin.get(LacreRomaneio_.statusLacreRomaneio),
                        lacreRomaneioJoin.get(LacreRomaneio_.dataUtilizacao),
                        retornoNFeJoin.get(RetornoNFe_.nfe),
                        retornoNFeJoin.get(RetornoNFe_.dataCadastro),
                        clienteJoin.get(Cliente_.codigo),
                        clienteJoin.get(Cliente_.nome),
                        lacreRomaneioUsuarioJoin.get(Usuario_.nome),
                        lacreUsuarioJoin.get(Usuario_.nome)
                )
        );
        criteriaQuery.groupBy(
                root.get(Romaneio_.numeroRomaneio),
                root.get(Romaneio_.statusRomaneio),
                root.get(Romaneio_.statusIntegracaoSAP),
                veiculoJoin.get(Veiculo_.placa1),
                tipoVeiculoJoin.get(TipoVeiculo_.descricao),
                lacreJoin.get(Lacre_.codigo),
                lacreJoin.get(Lacre_.statusLacre),
                lacreRomaneioJoin.get(LacreRomaneio_.statusLacreRomaneio),
                lacreRomaneioJoin.get(LacreRomaneio_.dataUtilizacao),
                retornoNFeJoin.get(RetornoNFe_.nfe),
                retornoNFeJoin.get(RetornoNFe_.dataCadastro),
                clienteJoin.get(Cliente_.codigo),
                clienteJoin.get(Cliente_.nome),
                lacreRomaneioUsuarioJoin.get(Usuario_.nome),
                lacreUsuarioJoin.get(Usuario_.nome)
        );
        criteriaQuery.orderBy(
                criteriaBuilder.desc(root.get(Romaneio_.numeroRomaneio)),
                criteriaBuilder.asc(lacreJoin.get(Lacre_.codigo))
        );
        TypedQuery<RelatorioLacreDTO> typedQuery = getSession().createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    @Transactional
    @Override
    public List<VwLacreDetalhe> obterLacreVinculadoARomaneio(final List<Lacre> lacres) {

        if (ColecaoUtil.isNotEmpty(lacres)) {
    
            CriteriaBuilder criteriaBuilder = getSession().getCriteriaBuilder();
    
            CriteriaQuery<VwLacreDetalhe> criteriaQuery = criteriaBuilder.createQuery(VwLacreDetalhe.class);
    
            Root<VwLacreDetalhe> root = criteriaQuery.from(VwLacreDetalhe.class);
    
            criteriaQuery.where(root.get(VwLacreDetalhe_.codigo).in(lacres.stream().map(Lacre::getCodigo).collect(Collectors.toList())));
            
            criteriaQuery.select(
                    criteriaBuilder.construct(
                            VwLacreDetalhe.class,
                            root.get(VwLacreDetalhe_.codigo),
                            root.get(VwLacreDetalhe_.dataUtilizacao),
                            root.get(VwLacreDetalhe_.nfe),
                            root.get(VwLacreDetalhe_.numeroRomaneio),
                            root.get(VwLacreDetalhe_.statusRomaneio),
                            root.get(VwLacreDetalhe_.statusIntegracaoSAP),
                            root.get(VwLacreDetalhe_.statusLacre),
                            root.get(VwLacreDetalhe_.usuarioQueUtilizou)
                    )
            );
            
            criteriaQuery.orderBy(criteriaBuilder.asc(root.get(VwLacreDetalhe_.codigo)));
         
            TypedQuery<VwLacreDetalhe> typedQuery = getSession().createQuery(criteriaQuery);
            
            return typedQuery.getResultList();
        }

        return null;
    }

    @Transactional
    @Override
    public List<LacreCountDTO> obterTotalDeLacresPorStatus(StatusLacreEnum statusLacre) {
        
        CriteriaBuilder criteriaBuilder = getSession().getCriteriaBuilder();
        
        CriteriaQuery<LacreCountDTO> criteriaQuery = criteriaBuilder.createQuery(LacreCountDTO.class);
        
        Root<VwLacreDetalhe> root = criteriaQuery.from(VwLacreDetalhe.class);
        
        if (Objects.nonNull(statusLacre)) {
            criteriaQuery.where(criteriaBuilder.equal(root.get(VwLacreDetalhe_.statusLacre), statusLacre));
        }
        
        criteriaQuery.select(criteriaBuilder.construct(
                LacreCountDTO.class,
                root.get(VwLacreDetalhe_.statusLacre),
                criteriaBuilder.count(root.get(VwLacreDetalhe_.id))
        ));
        
        criteriaQuery.groupBy(root.get(VwLacreDetalhe_.statusLacre));
        
        TypedQuery<LacreCountDTO> typedQuery = getSession().createQuery(criteriaQuery);
        
        return typedQuery.getResultList();
    }

    public Session getSession() {

        return (Session) this.em.getDelegate();
    }
}
