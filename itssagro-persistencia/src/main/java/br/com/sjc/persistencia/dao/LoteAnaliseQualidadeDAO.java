package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.LoteAnaliseQualidade;

public interface LoteAnaliseQualidadeDAO extends DAO<Long, LoteAnaliseQualidade> {

}
