package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.sap.TipoContrato;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TipoContratoDAO extends DAO<Long, TipoContrato> {

    @Query("select t.id from TipoContrato t where t.codigo = ?1")
    Long findIdByCodigo(final String codigo);

    List<TipoContrato> findByOrderByCodigoAsc();
}
