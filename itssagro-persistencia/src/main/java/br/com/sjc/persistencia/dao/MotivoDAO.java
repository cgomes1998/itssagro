package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Motivo;

/**
 * Created by julio.bueno on 24/06/2019.
 */
public interface MotivoDAO extends DAO<Long, Motivo> {
}
