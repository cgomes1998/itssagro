package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.dto.TipoVeiculoSetaDTO;
import br.com.sjc.modelo.sap.TipoVeiculoSeta;

import java.util.List;

public interface TipoVeiculoSetaCustomRepository {

    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    List<TipoVeiculoSetaDTO> listarDtoPorTipoVeiculo(Long idTipoVeiculo);

    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    List<TipoVeiculoSeta> listarPorTipoVeiculo(List<Long> idsTipoVeiculo);
}
