package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Balanca;
import br.com.sjc.modelo.enums.DirecaoEnum;
import br.com.sjc.modelo.enums.TipoFluxoEnum;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BalancaDAO extends DAO<Long, Balanca> {

    List<Balanca> findByDirecaoInAndFluxoPesagensTipoFluxo(List<DirecaoEnum> direcoes, TipoFluxoEnum tipoFluxo);

    boolean existsByNome(String nome);

    @Query("select count(b.id)>0 from Balanca b where b.id <> ?2 and b.nome = ?1")
    boolean existsByNomeAndNeId(final String nome, final Long id);
}