package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.SenhaEtapa;

public interface SenhaEtapaDAO extends DAO<Long, SenhaEtapa> {
}
