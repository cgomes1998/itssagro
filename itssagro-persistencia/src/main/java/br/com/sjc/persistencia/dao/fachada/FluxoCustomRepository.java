package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.dto.FluxoDTO;
import br.com.sjc.modelo.dto.MaterialDTO;

import javax.transaction.Transactional;
import java.util.List;

public interface FluxoCustomRepository {

    @Transactional
    List<MaterialDTO> listarMateriaisDoFluxo(List<Long> idFluxos);

    @Transactional
    List<FluxoDTO> listarOrdernandoDeFormaCrescentePorCodigo();
}
