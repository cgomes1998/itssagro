package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.dto.LogRomaneioDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface LogRomaneioCustomRepository {

    @Transactional(readOnly = true)
    List<LogRomaneioDTO> obterLogsDeLiberacaoPorRomaneio(Long idRomaneio);
}
