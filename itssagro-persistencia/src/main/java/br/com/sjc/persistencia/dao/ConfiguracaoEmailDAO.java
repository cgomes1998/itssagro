package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.ConfiguracaoEmail;

/**
 * Created by julio.bueno on 23/07/2019.
 */
public interface ConfiguracaoEmailDAO extends DAO<Long, ConfiguracaoEmail> {
}
