package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.sap.Deposito;

import java.util.List;

public interface DepositoDAO extends DAO<Long, Deposito> {

    Deposito findByCodigoAndCentroId(final String codigo, final Long id);

    Deposito findByCodigo(final String codigo);

    Deposito findFirstByCodigo(String codigo);

    List<Deposito> findByCentroCodigoOrderByCodigoAsc(final String codigo);

    List<Deposito> findByCentroIdOrderByCodigoAsc(final Long id);
}
