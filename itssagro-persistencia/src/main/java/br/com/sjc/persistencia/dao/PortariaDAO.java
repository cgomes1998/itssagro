package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.cfg.Portaria;
import br.com.sjc.modelo.sap.Motorista;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PortariaDAO extends DAO<Long, Portaria> {
    @Query("select p from Portaria p where p.ativo = true order by p.descricao ASC")
    List<Portaria> listarAtivas();
}
