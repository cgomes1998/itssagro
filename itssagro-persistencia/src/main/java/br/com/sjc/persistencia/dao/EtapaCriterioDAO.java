package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.EtapaCriterio;

public interface EtapaCriterioDAO extends DAO<Long, EtapaCriterio> {
}
