package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.cfg.Ambiente;

public interface AmbienteDAO extends DAO<Long, Ambiente> {

    Ambiente findByHostAndMandante(final String host, final String mandante);
}
