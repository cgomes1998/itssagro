package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.dto.*;
import br.com.sjc.modelo.sap.Cliente;
import br.com.sjc.modelo.sap.Romaneio;
import br.com.sjc.modelo.sap.request.item.RfcStatusRomaneioRequestItem;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Set;

public interface RomaneioCustomRepository extends CustomRepository<Long, Romaneio> {

    @Transactional
    List<PesagemHistoricoRomaneioDTO> obterQuantidadePesagensBrutasPorRomaneios(List<String> numerosRomaneios);

    @Transactional
    List<RestricaoOrdemDTO> getRestricoesDasOrdens(List<String> numeroRomaneios, List<String> codigoOrdens);

    @Transactional
    RomaneioConferenciaPeso getRomaneioConferenciaPeso(String numeroRomaneio);

    @Transactional
    List<ConversaoLitragemRomaneioDTO> countConversoesPorRomaneio(List<String> romaneios);

    String obterRomaneioQueEstaUtilizandoOCartaoDeAcesso(Integer numeroCartaoAcesso);

    @Transactional
    List<RfcStatusRomaneioRequestItem> listarRequestKeysDeRomaneiosPorId(List<Long> idRomaneios);

    @Transactional
    Long obterCountDadosDoRomaneioParaRelatorioDeDeposito(RelatorioDepositoFiltroDTO filtro);

    @Transactional
    List<RomaneioDepositoDTO> obterDadosDoRomaneioParaRelatorioDeDeposito(RelatorioDepositoFiltroDTO filtro);

    List<RomaneioConversaoLitragemDTO> obterRomaneiosComConversaoLitragem(List<String> romaneios);

    @Transactional
    List<RfcStatusRomaneioRequestItem> obterRequestsDeRomaneiosSemNota(List<Long> idRomaneios);

    @Transactional
    List<RomaneioEItemDTO> obterIdItens(List<Long> ids);

    @Transactional
    List<RequestItemDTO> obterRequestKeys(List<Long> ids);

    @Transactional
    boolean existeNumeroRomaneio(String numeroRomaneio);

    @Transactional
    boolean existeRomaneioVinculadoAsenha(Long idRomaneio, Long idSenha);

    @Transactional
    List<Cliente> obterClientesPorRomaneio(Long idRomaneio);

    @Transactional
    List<Romaneio> obterIdEOperacaoPorNumeroRomaneio(
            final Set<String> numeroRomaneios
    );

    @Transactional
    List<RelatorioComercial_V2_DTO> findByDataCadastroAndMaterialIdOrdOrderByIdDesc_V2(RelatorioComercialFiltroDTO filtro);

    @Transactional
    List<RelatorioComercialDTO> findByDataCadastroAndMaterialIdOrdOrderByIdDesc(RelatorioComercialFiltroDTO filtro);

    @Transactional
    List<RelatorioFluxoDeCarregamentoDTO> findByDataCadastroAndMaterialIdOrdOrderByIdDescCarregamento(RelatorioFluxoDeCarregamentoFiltroDTO filtro);

    List<RelatorioAnaliseConversaoDTO> obterRelatorioAnaliseConversao(RelatorioAnaliseConversaoFiltroDTO filtro);

    @Transactional
    List<RelComercialNFeDTO> getRelatorioComercialNFeDTOS(RelatorioComercialFiltroDTO filtro);

    @Transactional
    List<RelatorioLaudoAnalitico> obterRelatorioLaudoAnalitico(RelatorioLaudoFiltroDTO relatorioLaudoFiltro);

    @Transactional
    List<RomaneioGestaoItssAgroDTO> listarRomaneioGestaoItssAgroPorData(Date dataInicial, Date dataFinal);

    @Transactional
    List<RelatorioLaudoSintetico> obterRelatorioLaudoSintetico(RelatorioLaudoFiltroDTO relatorioLaudoFiltro);

    @Transactional
    Long obterIdDaPesagemPorRomaneio(Long idRomaneio);

    @org.springframework.transaction.annotation.Transactional
    void alterarAtributos(Romaneio entidade, String... atributos);
}
