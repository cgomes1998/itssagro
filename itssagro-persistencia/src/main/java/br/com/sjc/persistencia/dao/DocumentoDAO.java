package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Documento;

/**
 * Created by julio.bueno on 03/07/2019.
 */
public interface DocumentoDAO extends DAO<Long, Documento> {

    Documento findByAssinaturaTrue();

}
