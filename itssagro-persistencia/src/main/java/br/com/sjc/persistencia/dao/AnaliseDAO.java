package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Analise;
import br.com.sjc.modelo.enums.StatusEnum;

public interface AnaliseDAO extends DAO<Long, Analise> {

    boolean existsByMaterial_Id ( Long materialId );

    Analise findByMaterialId (
            final Long materialId
    );
}
