package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.modelo.dto.ItensCalculoIndiceDTO;
import br.com.sjc.modelo.sap.ItensClassificacao;
import br.com.sjc.modelo.sap.TabelaClassificacao;
import br.com.sjc.modelo.sap.TabelaClassificacaoHeader;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TabelaClassificacaoCustomRepository {

	Page<TabelaClassificacaoHeader> listarPaginado(Paginacao<TabelaClassificacaoHeader> paginacao);

	@Transactional(readOnly = true)
	TabelaClassificacao obterPorCodigoEItemClassificacaoEDescricaoItemClassificacaoEIndice(
		   String codigoMaterial,
		   String codigo,
		   String itemClassificacao,
		   String descricaoItemClassificacao,
		   String indice
	);

	@Transactional(readOnly = true)
	List<TabelaClassificacao> obterPorMaterial(String codigoMaterial);

	@Transactional(readOnly = true)
	List<ItensCalculoIndiceDTO> obterItensParaCalculoDeIndicesPorTabela(List<Long> ids);

    @Transactional
    boolean indiceInformadoExisteNaTabelaDeClassificacao(ItensClassificacao itensClassificacao);
}
