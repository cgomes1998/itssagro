package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.cfg.CampoOperacao;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;

import java.util.List;

public interface CampoOperacaoDAO extends DAO<Long, CampoOperacao> {

    List<CampoOperacao> findByOperacao( final DadosSincronizacaoEnum operacao);
}
