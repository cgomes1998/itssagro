package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.modelo.AnaliseQualidade;
import br.com.sjc.modelo.AnaliseQualidade_;
import br.com.sjc.modelo.Laudo;
import br.com.sjc.modelo.dto.AnaliseFiltroDTO;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.persistencia.dao.fachada.AnaliseQualidadeCustomRepository;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

@Service
public class AnaliseQualidadeCustomRepositoryImplImpl extends CustomRepositoryImpl implements AnaliseQualidadeCustomRepository {

    @Override
    @Transactional
    public Romaneio obterAnaliseQualidadeParaCarregamento(AnaliseFiltroDTO filtroDTO, Laudo laudo) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<Romaneio> criteriaQuery = builder.createQuery(Romaneio.class);

        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);

        Join<Romaneio, AnaliseQualidade> analiseQualidadeJoin = root.join(Romaneio_.analises);

        Join<Romaneio, ItemNFPedido> itemNFPedidoJoin = root.join(Romaneio_.itens);

        Join<ItemNFPedido, Material> materialJoin = itemNFPedidoJoin.join(ItemNFPedido_.material);

        criteriaQuery.where(builder.and(builder.equal(materialJoin.get(Material_.id), filtroDTO.getMaterial().getId()), builder.equal(analiseQualidadeJoin.get(AnaliseQualidade_.idLaudo), laudo.getId()), builder.notEqual(root.get(Romaneio_.id), filtroDTO.getIdRomaneio())));

        criteriaQuery.orderBy(builder.desc(root.get(Romaneio_.id)));

        TypedQuery<Romaneio> typedQuery = this.getSession().createQuery(criteriaQuery);

        typedQuery.setMaxResults(1);

        try {

            return typedQuery.getSingleResult();

        } catch (NoResultException exception) {

            return null;
        }
    }

}
