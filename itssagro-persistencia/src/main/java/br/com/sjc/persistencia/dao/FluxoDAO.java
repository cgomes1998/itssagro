package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Fluxo;

import java.util.List;

public interface FluxoDAO extends DAO<Long, Fluxo> {

    List<Fluxo> findAllByOrderByCodigoAsc();

}
