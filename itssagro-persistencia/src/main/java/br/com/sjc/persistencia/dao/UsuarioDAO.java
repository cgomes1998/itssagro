package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Usuario;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.Date;

public interface UsuarioDAO extends DAO<Long, Usuario> {

    @Query( "select u.nome from Usuario u where u.id=?1" )
    String findNomeById ( final Long id );

    Usuario findByLoginAndSenha (
            String login,
            String senha
    );

    boolean existsByCpf ( String cpf );

    Usuario findTop1ByLogin ( String login );

    @Query( "select u.login from Usuario u where u.id=?1" )
    String findLoginById ( final Long id );

    boolean existsByLogin ( String login );

    @Query( "select count(u.id)>0 from Usuario u where u.id <> ?2 and u.login = ?1" )
    boolean existsByLoginAndNeId (
            final String login,
            final Long id
    );

    boolean existsByLoginAndSenha (
            final String login,
            final String senha
    );

    @Transactional
    @Modifying
    @Query( "update Usuario u set u.ultimoAcesso = ?2 where u.id = ?1" )
    void atualizarDataUltimoAcesso (
            final Long id,
            final Date data
    );
}
