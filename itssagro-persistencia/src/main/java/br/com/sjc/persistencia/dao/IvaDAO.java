package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.sap.Iva;

import java.util.List;

public interface IvaDAO extends DAO<Long, Iva> {

    Iva findByCodigo(final String codigo);

    List<Iva> findByOrderByCodigoAsc();
}
