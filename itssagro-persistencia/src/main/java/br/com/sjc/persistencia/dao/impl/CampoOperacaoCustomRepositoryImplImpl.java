package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.modelo.cfg.Campo;
import br.com.sjc.modelo.cfg.CampoOperacao;
import br.com.sjc.modelo.cfg.CampoOperacao_;
import br.com.sjc.modelo.cfg.Campo_;
import br.com.sjc.modelo.dto.CampoOperacaoDTO;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.persistencia.dao.fachada.CampoOperacaoCustomRepository;
import org.springframework.stereotype.Service;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;

@Service
public class CampoOperacaoCustomRepositoryImplImpl extends CustomRepositoryImpl implements CampoOperacaoCustomRepository {

  @org.springframework.transaction.annotation.Transactional(readOnly = true)
  @Override
  public List<CampoOperacaoDTO> listar(
    final DadosSincronizacaoEnum operacao
  ) {

    CriteriaBuilder builder = this.getSession().getCriteriaBuilder();
    CriteriaQuery<CampoOperacaoDTO> criteriaQuery = builder.createQuery(CampoOperacaoDTO.class);
    Root<CampoOperacao> root = criteriaQuery.from(CampoOperacao.class);
    Join<CampoOperacao, Campo> joinCampo = root.join(
      CampoOperacao_.campo,
      JoinType.INNER
    );
    criteriaQuery.where(builder.equal(
      root.get(CampoOperacao_.operacao),
      operacao
    ));
    criteriaQuery.select(builder.construct(
      CampoOperacaoDTO.class,
      root.get(CampoOperacao_.obrigatorio),
      root.get(CampoOperacao_.somenteLeitura),
      root.get(CampoOperacao_.divisaoCarga),
      root.get(CampoOperacao_.permitirValorDefault),
      root.get(CampoOperacao_.valorDefaultEEnum),
      root.get(CampoOperacao_.display),
      root.get(CampoOperacao_.valorDefault),
      joinCampo.get(Campo_.descricao)
    ));
    TypedQuery<CampoOperacaoDTO> query = this.getSession().createQuery(criteriaQuery);
    return query.getResultList();
  }

}
