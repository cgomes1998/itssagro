package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.sap.ConfiguracaoUnidadeMedidaMaterial;

import java.util.List;

public interface ConfiguracaoUnidadeMedidaMaterialDAO extends DAO<Long, ConfiguracaoUnidadeMedidaMaterial> {

    List<ConfiguracaoUnidadeMedidaMaterial> findByMaterialCodigo(
            String codigoMaterial
    );
}
