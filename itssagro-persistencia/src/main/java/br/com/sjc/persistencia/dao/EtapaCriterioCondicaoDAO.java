package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.EtapaCriterioCondicao;

public interface EtapaCriterioCondicaoDAO extends DAO<Long, EtapaCriterioCondicao> {
}
