package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.Lacre;
import br.com.sjc.modelo.dto.LacreCountDTO;
import br.com.sjc.modelo.dto.RelatorioLacreDTO;
import br.com.sjc.modelo.dto.RelatorioLacreFiltroDTO;
import br.com.sjc.modelo.enums.StatusLacreEnum;
import br.com.sjc.modelo.view.VwLacreDetalhe;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface LacreCustomRepository {
    @Transactional
    List<RelatorioLacreDTO> obterRelatorioDeLacresSemVinculoARomaneio(RelatorioLacreFiltroDTO relatorioLacreFiltroDTO);

    @Transactional
    List<RelatorioLacreDTO> obterRelatorioDeLacresVinculadosARomaneio(RelatorioLacreFiltroDTO relatorioLacreFiltro);

    @Transactional
    List<VwLacreDetalhe> obterLacreVinculadoARomaneio(List<Lacre> lacres);

    @Transactional
    List<LacreCountDTO> obterTotalDeLacresPorStatus(StatusLacreEnum statusLacre);
}
