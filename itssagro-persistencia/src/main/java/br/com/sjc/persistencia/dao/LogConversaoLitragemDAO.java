package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.LogConversaoLitragem;

public interface LogConversaoLitragemDAO extends DAO<Long, LogConversaoLitragem> {

}
