package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.cfg.Campo;

public interface CampoDAO extends DAO<Long, Campo> {

    Campo findByDescricao(String descricao);
}
