package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.PortariaTicket;

import javax.transaction.Transactional;
import java.util.List;

public interface PortariaTicketCustomRepository {
    @Transactional
    List<PortariaTicket> obterDadosRelatorioPortariaTicket(PortariaTicket filtro);
}
