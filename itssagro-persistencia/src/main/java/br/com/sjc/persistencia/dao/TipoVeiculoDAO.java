package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.sap.TipoVeiculo;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.enums.StatusEnum;

import java.util.Date;
import java.util.List;

public interface TipoVeiculoDAO extends DAO<Long, TipoVeiculo> {

    TipoVeiculo findByCodigo(final String codigo);

    TipoVeiculo findFirstByCodigoOrTipo(final String codigo, final String tipo);

    List<TipoVeiculo> findByStatusCadastroSapIn(final List<StatusCadastroSAPEnum> statusCadastroSAPEnums);

    List<TipoVeiculo> findByStatusCadastroSapInAndDataCadastroGreaterThanEqual(final List<StatusCadastroSAPEnum> statusCadastroSAPEnums, final Date data);
}
