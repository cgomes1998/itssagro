package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.sap.Centro;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;

import java.util.Date;
import java.util.List;

public interface CentroDAO extends DAO<Long, Centro> {

    Centro findByCodigo(final String codigo);

    List<Centro> findByOrderByCodigoAsc();

    List<Centro> findByStatusCadastroSapIn(final List<StatusCadastroSAPEnum> statusCadastroSAPEnums);

    List<Centro> findByStatusCadastroSapInAndDataCadastroGreaterThanEqual(final List<StatusCadastroSAPEnum> statusCadastroSAPEnums, final Date data);
}
