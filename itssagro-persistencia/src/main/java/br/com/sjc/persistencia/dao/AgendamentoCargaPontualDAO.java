package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.AgendamentoCargaPontual;

public interface AgendamentoCargaPontualDAO extends DAO<Long, AgendamentoCargaPontual> {

}
