package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.modelo.dto.ItemNFPedidoDTO;
import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.persistencia.dao.fachada.ItemNFPedidoCustomRepository;
import org.springframework.stereotype.Service;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class ItemNFPedidoCustomRepositoryImplImpl extends CustomRepositoryImpl implements ItemNFPedidoCustomRepository {

    @Transactional
    @Override
    public List<ItemNFPedidoDTO> obterItensDeRomaneioParaEntradaLiberada() {
        CriteriaBuilder criteriaBuilder = getSession().getCriteriaBuilder();
        CriteriaQuery<ItemNFPedidoDTO> criteriaQuery = criteriaBuilder.createQuery(ItemNFPedidoDTO.class);

        Root<ItemNFPedido> root = criteriaQuery.from(ItemNFPedido.class);
        Join<ItemNFPedido, Romaneio> romaneioJoin = root.join(ItemNFPedido_.romaneio);
        Join<ItemNFPedido, Motorista> motoristaJoin = root.join(ItemNFPedido_.motorista);

        criteriaQuery.select(
                criteriaBuilder.construct(
                        ItemNFPedidoDTO.class,
                        romaneioJoin.get(Romaneio_.numeroRomaneio),
                        romaneioJoin.get(Romaneio_.numeroCartaoAcesso),
                        romaneioJoin.get("dataSaidaEtapaAguardandoChamada"),
                        root.join(ItemNFPedido_.placaCavalo).get(Veiculo_.placa1),
                        motoristaJoin.get(Motorista_.codigo),
                        motoristaJoin.get(Motorista_.nome)
                )
        );

        criteriaQuery.where(
                criteriaBuilder.and(
                        criteriaBuilder.equal(romaneioJoin.get(Romaneio_.statusRomaneio), StatusRomaneioEnum.EM_PROCESSAMENTO),
                        criteriaBuilder.equal(romaneioJoin.get("passouEtapaAguardandoChamada"), Boolean.TRUE)
                )
        );

        criteriaQuery.orderBy(criteriaBuilder.desc(romaneioJoin.get("dataSaidaEtapaAguardandoChamada")));

        TypedQuery<ItemNFPedidoDTO> typedQuery = getSession().createQuery(criteriaQuery);

        return typedQuery.getResultList();
    }

    @Transactional
    @Override
    public List<ItemNFPedidoDTO> obterItensDeRomaneioParaAutoatendimento() {
        CriteriaBuilder criteriaBuilder = getSession().getCriteriaBuilder();
        CriteriaQuery<ItemNFPedidoDTO> criteriaQuery = criteriaBuilder.createQuery(ItemNFPedidoDTO.class);

        Root<ItemNFPedido> root = criteriaQuery.from(ItemNFPedido.class);
        Join<ItemNFPedido, Romaneio> romaneioJoin = root.join(ItemNFPedido_.romaneio);
        Join<ItemNFPedido, Motorista> motoristaJoin = root.join(ItemNFPedido_.motorista);

        criteriaQuery.select(
                criteriaBuilder.construct(
                        ItemNFPedidoDTO.class,
                        romaneioJoin.get(Romaneio_.numeroRomaneio),
                        romaneioJoin.get(Romaneio_.numeroCartaoAcesso),
                        romaneioJoin.get(Romaneio_.dataAlteracao),
                        root.join(ItemNFPedido_.placaCavalo).get(Veiculo_.placa1),
                        motoristaJoin.get(Motorista_.codigo),
                        motoristaJoin.get(Motorista_.nome)
                )
        );

        criteriaQuery.where(
                criteriaBuilder.and(
                        criteriaBuilder.equal(romaneioJoin.get(Romaneio_.statusRomaneio), StatusRomaneioEnum.CONCLUIDO),
                        criteriaBuilder.equal(romaneioJoin.get(Romaneio_.statusIntegracaoSAP), StatusIntegracaoSAPEnum.COMPLETO),
                        criteriaBuilder.equal(romaneioJoin.get(Romaneio_.romaneioLiberadoParaImpressaoAutoAtendimento), Boolean.TRUE),
                        criteriaBuilder.equal(romaneioJoin.get(Romaneio_.romaneioJaImprimidoAutoatendimento), Boolean.FALSE)
                )
        );

        criteriaQuery.orderBy(criteriaBuilder.desc(romaneioJoin.get(Romaneio_.dataAlteracao)));

        TypedQuery<ItemNFPedidoDTO> typedQuery = getSession().createQuery(criteriaQuery);

        return typedQuery.getResultList();
    }
}
