package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.Fluxo;
import br.com.sjc.modelo.TipoFluxoEnum;
import br.com.sjc.modelo.dto.FluxoEtapaDTO;
import br.com.sjc.modelo.dto.ProximoFluxoEtapaDTO;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.sap.Centro;

import javax.transaction.Transactional;
import java.util.List;

public interface FluxoEtapaCustomRepository {

    List<ProximoFluxoEtapaDTO> listarProximasEtapasDoFluxo(Fluxo fluxo);

    @Transactional
    List<FluxoEtapaDTO> listarPorFluxo(List<Long> idFluxos);

    List<FluxoEtapaDTO> obterPorCentroEtapaMaterial(Centro centro, EtapaEnum etapa, String codigoMaterial, String descricaoMaterial, TipoFluxoEnum tipoFluxo);

}
