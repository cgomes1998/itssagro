package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.sap.Cliente;

import java.util.List;

/**
 * Created by julio.bueno on 27/06/2019.
 */
public interface ClienteDAO extends DAO<Long, Cliente> {

    List<Cliente> findTop10ByNomeIgnoreCaseContainingOrCodigoIgnoreCaseContainingOrCpfCnpjIgnoreCaseContainingOrderByCodigoAsc(String nome, String codigo, String cpfCnpj);

    Cliente findByCodigo(String codigoCliente);
}
