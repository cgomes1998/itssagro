package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.EventoAutomacaoGraos;

public interface EventoAutomacaoGraosDAO extends DAO<Long, EventoAutomacaoGraos> {

}
