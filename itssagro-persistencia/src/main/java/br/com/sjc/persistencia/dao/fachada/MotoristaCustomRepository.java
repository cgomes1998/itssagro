package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.sap.Motorista;

import java.util.Date;
import java.util.List;

public interface MotoristaCustomRepository {

    List<Motorista> listarPreCadastro ( Date data );
}
