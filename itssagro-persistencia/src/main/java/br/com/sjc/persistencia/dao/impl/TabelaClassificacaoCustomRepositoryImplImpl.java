package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.fachada.rest.paginacao.Paginacao;
import br.com.sjc.modelo.dto.ItensCalculoIndiceDTO;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.persistencia.dao.fachada.TabelaClassificacaoCustomRepository;
import br.com.sjc.util.ColecaoUtil;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class TabelaClassificacaoCustomRepositoryImplImpl extends CustomRepositoryImpl implements TabelaClassificacaoCustomRepository {

	@javax.transaction.Transactional
	@Override
	public Page<TabelaClassificacaoHeader> listarPaginado(Paginacao<TabelaClassificacaoHeader> paginacao) {

		CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

		CriteriaQuery<TabelaClassificacaoHeader> criteriaQuery = builder.createQuery(TabelaClassificacaoHeader.class);

		Root<TabelaClassificacao> root = criteriaQuery.from(TabelaClassificacao.class);

		TabelaClassificacaoHeader entidade = paginacao.getEntidade();

		final List<Predicate> predicates = new ArrayList<>();

		if (StringUtil.isNotNullEmpty(entidade.getItemClassificacao())) {

			predicates.add(builder.and(builder.like(builder.upper(root.get(TabelaClassificacao_.itemClassificacao)), "%" + entidade.getItemClassificacao().toUpperCase() + "%")));
		}

		if (StringUtil.isNotNullEmpty(entidade.getDescricao())) {

			predicates.add(builder.and(builder.like(builder.upper(root.get(TabelaClassificacao_.descricaoItemClassificacao)), "%" + entidade.getDescricao().toUpperCase() + "%")));
		}

		if (ObjetoUtil.isNotNull(entidade.getCodigoMaterial())) {

			predicates.add(builder.equal(root.get(TabelaClassificacao_.codigoMaterial), entidade.getCodigoMaterial()));
		}

		criteriaQuery.where(predicates.toArray(new Predicate[]{}));

		criteriaQuery.orderBy(builder.asc(root.get(TabelaClassificacao_.itemClassificacao)));

		criteriaQuery.select(builder.construct(
			   TabelaClassificacaoHeader.class,
			   root.get(TabelaClassificacao_.itemClassificacao),
			   root.get(TabelaClassificacao_.codigoMaterial),
			   root.get(TabelaClassificacao_.descricaoItemClassificacao)
		));

		criteriaQuery.groupBy(
			   root.get(TabelaClassificacao_.itemClassificacao),
			   root.get(TabelaClassificacao_.codigoMaterial),
			   root.get(TabelaClassificacao_.descricaoItemClassificacao)
		);

		TypedQuery<TabelaClassificacaoHeader> queryApenasItemClassificacao = this.getSession().createQuery(criteriaQuery);

		int totalRows = queryApenasItemClassificacao.getResultList().size();

		criteriaQuery.where(predicates.toArray(new Predicate[]{}));

		TypedQuery<TabelaClassificacaoHeader> query = this.getSession().createQuery(criteriaQuery);

		query.setFirstResult(paginacao.getPageNumber() * paginacao.getPageSize());

		query.setMaxResults(paginacao.getPageSize());

		return new PageImpl<TabelaClassificacaoHeader>(
			   query.getResultList(),
			   paginacao,
			   totalRows
		);
	}

	@Transactional(readOnly = true)
	@Override
	public TabelaClassificacao obterPorCodigoEItemClassificacaoEDescricaoItemClassificacaoEIndice(
		   final String codigoMaterial,
		   final String codigo,
		   String itemClassificacao,
		   String descricaoItemClassificacao,
		   String indice
	) {

		CriteriaBuilder builder = this.getSession().getCriteriaBuilder();
		CriteriaQuery<TabelaClassificacao> criteriaQuery = builder.createQuery(TabelaClassificacao.class);
		Root<TabelaClassificacao> root = criteriaQuery.from(TabelaClassificacao.class);
		criteriaQuery.where(builder.and(predicates(
			   builder,
			   root,
			   codigo,
			   itemClassificacao,
			   descricaoItemClassificacao,
			   indice,
                     codigoMaterial
		).toArray(new Predicate[]{})));
		criteriaQuery.select(construtorComVariaveisSimples(
			   builder,
			   root
		));
		TypedQuery<TabelaClassificacao> query = this.getSession().createQuery(criteriaQuery);
		query.setMaxResults(1);
		List<TabelaClassificacao> tabelaClassificacaoList = query.getResultList();
		return tabelaClassificacaoList.stream().filter(Objects::nonNull).findFirst().orElse(null);
	}

	@Transactional(readOnly = true)
	@Override
	public List<TabelaClassificacao> obterPorMaterial(final String codigoMaterial) {

		CriteriaBuilder builder = this.getSession().getCriteriaBuilder();
		CriteriaQuery<TabelaClassificacao> criteriaQuery = builder.createQuery(TabelaClassificacao.class);
		Root<TabelaClassificacao> root = criteriaQuery.from(TabelaClassificacao.class);
		criteriaQuery.where(builder.and(predicates(
			   builder,
			   root,
			   null,
			   null,
			   null,
			   null,
			   codigoMaterial
		).toArray(new Predicate[]{})));
		criteriaQuery.select(construtorComVariaveisSimples(
			   builder,
			   root
		));
		criteriaQuery.orderBy(builder.asc(root.get(TabelaClassificacao_.itemClassificacao)));
		return this.getSession().createQuery(criteriaQuery).getResultList();
	}

	@Transactional(readOnly = true)
	@Override
	public List<ItensCalculoIndiceDTO> obterItensParaCalculoDeIndicesPorTabela(List<Long> ids) {

		StringBuilder sb = new StringBuilder();
		sb.append("select ");
		sb.append("	* ");
		sb.append("from ");
		sb.append("	sap.tb_itens_calculo_indice ");
		sb.append("where ");
		sb.append(MessageFormat.format(
			   "	id_tabela_classificacao in ( {0} ) ; ",
			   ids.stream().map(Objects::toString).collect(Collectors.joining(","))
		));
		List<Object[]> retorno = this.getSession().createNativeQuery(sb.toString()).getResultList();
		if (ColecaoUtil.isNotEmpty(retorno)) {
			return retorno.stream()
			              .map(r -> new ItensCalculoIndiceDTO(
				                 (BigInteger) r[0],
				                 (String) r[1]
			              ))
			              .collect(Collectors.toList());
		}
		return null;
	}

	private List<Predicate> predicates(
		   CriteriaBuilder builder,
		   Root<TabelaClassificacao> root,
		   final String codigo,
		   final String itemClassificacao,
		   final String descricaoItemClassificacao,
		   final String indice,
		   final String codigoMaterial
	) {

		List<Predicate> predicates = new ArrayList<>();
		if (StringUtil.isNotNullEmpty(codigo)) {
			predicates.add(builder.and(builder.equal(
				   root.get(TabelaClassificacao_.codigo),
				   codigo
			)));
		}
		if (StringUtil.isNotNullEmpty(itemClassificacao)) {
			predicates.add(builder.and(builder.equal(
				   root.get(TabelaClassificacao_.itemClassificacao),
				   itemClassificacao
			)));
		}
		if (StringUtil.isNotNullEmpty(descricaoItemClassificacao)) {
			predicates.add(builder.and(builder.equal(
				   root.get(TabelaClassificacao_.descricaoItemClassificacao),
				   descricaoItemClassificacao
			)));
		}
		if (StringUtil.isNotNullEmpty(indice)) {
			predicates.add(builder.and(builder.equal(
				   root.get(TabelaClassificacao_.indice),
				   indice
			)));
		}
		if (StringUtil.isNotNullEmpty(codigoMaterial)) {
			predicates.add(builder.and(builder.equal(
				   root.get(TabelaClassificacao_.codigoMaterial),
				   codigoMaterial
			)));
		}
		return predicates;
	}

	private CompoundSelection<TabelaClassificacao> construtorComVariaveisSimples(
		   CriteriaBuilder builder,
		   Root<TabelaClassificacao> root
	) {

		return builder.construct(
			   TabelaClassificacao.class,
			   root.get(TabelaClassificacao_.id),
			   root.get(TabelaClassificacao_.codigo),
			   root.get(TabelaClassificacao_.codigoLeitor),
			   root.get(TabelaClassificacao_.hostLeitor),
			   root.get(TabelaClassificacao_.portaLeitor),
			   root.get(TabelaClassificacao_.codigoMaterial),
			   root.get(TabelaClassificacao_.codigoArmazemTerceiros),
			   root.get(TabelaClassificacao_.itemClassificacao),
			   root.get(TabelaClassificacao_.indice),
			   root.get(TabelaClassificacao_.percentualDesconto),
			   root.get(TabelaClassificacao_.descricaoItemClassificacao),
			   root.get(TabelaClassificacao_.indiceNumerico),
			   root.get(TabelaClassificacao_.indicePermitidoPorPerfil)
		);
	}

	@Transactional
	@Override
	public boolean indiceInformadoExisteNaTabelaDeClassificacao(ItensClassificacao itensClassificacao) {
		CriteriaBuilder criteriaBuilder = this.getSession().getCriteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
		Root<TabelaClassificacao> root = criteriaQuery.from(TabelaClassificacao.class);
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(criteriaBuilder.equal(root.get(TabelaClassificacao_.itemClassificacao), itensClassificacao.getItemClassificao()));
		predicates.add(criteriaBuilder.equal(root.get(TabelaClassificacao_.codigoMaterial), itensClassificacao.getCodigoMaterial()));
		Double _indice = 0.0;
		if (ObjetoUtil.isNotNull(itensClassificacao.getIndice())) {
			_indice = itensClassificacao.getIndice().doubleValue();
		}
		predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(TabelaClassificacao_.indiceNumerico), _indice));
		criteriaQuery.where(predicates.toArray(new Predicate[0]));
		criteriaQuery.select(criteriaBuilder.count(root.get(TabelaClassificacao_.id)));
		TypedQuery<Long> typedQuery = this.getSession().createQuery(criteriaQuery);
		Long totalIndices = typedQuery.getSingleResult();
		return ObjetoUtil.isNotNull(totalIndices) && totalIndices > 0l;
	}
}
