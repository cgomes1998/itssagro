package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.sap.RomaneioArmazemTerceiros;

public interface RomaneioArmazemTerceirosDAO extends DAO<Long, RomaneioArmazemTerceiros> {

}
