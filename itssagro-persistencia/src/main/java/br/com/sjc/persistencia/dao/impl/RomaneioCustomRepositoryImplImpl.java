package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.modelo.*;
import br.com.sjc.modelo.cfg.DadosSincronizacao;
import br.com.sjc.modelo.cfg.DadosSincronizacao_;
import br.com.sjc.modelo.dto.*;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;
import br.com.sjc.modelo.enums.StatusIntegracaoSAPEnum;
import br.com.sjc.modelo.enums.StatusRomaneioEnum;
import br.com.sjc.modelo.enums.StatusSenhaEnum;
import br.com.sjc.modelo.sap.*;
import br.com.sjc.modelo.sap.request.item.RfcStatusRomaneioRequestItem;
import br.com.sjc.persistencia.dao.fachada.RomaneioCustomRepository;
import br.com.sjc.util.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RomaneioCustomRepositoryImplImpl extends CustomRepositoryImpl<Long, Romaneio> implements RomaneioCustomRepository {

    @Transactional
    @Override
    public List<PesagemHistoricoRomaneioDTO> obterQuantidadePesagensBrutasPorRomaneios(List<String> numerosRomaneios) {
        CriteriaBuilder criteriaBuilder = this.getSession().getCriteriaBuilder();
        CriteriaQuery<PesagemHistoricoRomaneioDTO> criteriaQuery = criteriaBuilder.createQuery(PesagemHistoricoRomaneioDTO.class);
        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);
        Join<Romaneio, Pesagem> pesagemJoin = root.join(Romaneio_.pesagem);
        Join<Pesagem, PesagemHistorico> pesagemHistoricoJoin = pesagemJoin.join(Pesagem_.historicos);
        criteriaQuery.where(root.get(Romaneio_.numeroRomaneio).in(numerosRomaneios));
        criteriaQuery.groupBy(root.get(Romaneio_.numeroRomaneio));
        criteriaQuery.select(
                criteriaBuilder.construct(
                        PesagemHistoricoRomaneioDTO.class,
                        root.get(Romaneio_.numeroRomaneio),
                        criteriaBuilder.countDistinct(pesagemHistoricoJoin.get(PesagemHistorico_.pesoInicial))
                )
        );
        TypedQuery<PesagemHistoricoRomaneioDTO> typedQuery = this.getSession().createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    @Transactional
    @Override
    public List<RestricaoOrdemDTO> getRestricoesDasOrdens(List<String> numeroRomaneios, List<String> codigoOrdens) {

        CriteriaBuilder criteriaBuilder = this.getSession().getCriteriaBuilder();
        CriteriaQuery<RestricaoOrdemDTO> criteriaQuery = criteriaBuilder.createQuery(RestricaoOrdemDTO.class);
        Root<RestricaoOrdem> root = criteriaQuery.from(RestricaoOrdem.class);
        List<Predicate> predicates = new ArrayList<>();
        Join<RestricaoOrdem, Romaneio> romaneioJoin = root.join(RestricaoOrdem_.romaneio);
        if (ColecaoUtil.isNotEmpty(codigoOrdens)) {
            predicates.add(criteriaBuilder.or(
                    romaneioJoin.get(Romaneio_.numeroRomaneio).in(numeroRomaneios),
                    root.get(RestricaoOrdem_.codigoOrdemVenda).in(codigoOrdens)
            ));
        } else {
            predicates.add(romaneioJoin.get(Romaneio_.numeroRomaneio).in(numeroRomaneios));
        }
        criteriaQuery.where(predicates.toArray(new Predicate[0]));
        criteriaQuery.select(criteriaBuilder.construct(
                RestricaoOrdemDTO.class,
                root.get(RestricaoOrdem_.mensagem),
                root.get(RestricaoOrdem_.codigoOrdemVenda),
                romaneioJoin.get(Romaneio_.numeroRomaneio)
        ));
        TypedQuery<RestricaoOrdemDTO> typedQuery = this.getSession().createQuery(criteriaQuery);

        return typedQuery.getResultList();
    }

    @Transactional
    @Override
    public RomaneioConferenciaPeso getRomaneioConferenciaPeso(String numeroRomaneio) {
        CriteriaBuilder criteriaBuilder = this.getSession().getCriteriaBuilder();
        CriteriaQuery<RomaneioConferenciaPeso> criteriaQuery = criteriaBuilder.createQuery(RomaneioConferenciaPeso.class);
        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);
        Join<Romaneio, Pesagem> pesagemJoin = root.join(Romaneio_.pesagem);
        criteriaQuery.where(criteriaBuilder.equal(root.get(Romaneio_.numeroRomaneio), numeroRomaneio));
        criteriaQuery.select(
                criteriaBuilder.construct(
                        RomaneioConferenciaPeso.class,
                        pesagemJoin.get(Pesagem_.pesoInicial),
                        pesagemJoin.get(Pesagem_.pesoFinal)
                )
        );
        TypedQuery<RomaneioConferenciaPeso> typedQuery = this.getSession().createQuery(criteriaQuery);
        try {
            return typedQuery.getSingleResult();
        } catch (NoResultException | NonUniqueResultException exceptio) {
            return null;
        }
    }

    @Transactional
    @Override
    public List<ConversaoLitragemRomaneioDTO> countConversoesPorRomaneio(List<String> romaneios) {
        CriteriaBuilder criteriaBuilder = this.getSession().getCriteriaBuilder();
        CriteriaQuery<ConversaoLitragemRomaneioDTO> criteriaQuery = criteriaBuilder.createQuery(ConversaoLitragemRomaneioDTO.class);
        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);
        Join<ConversaoLitragem, ConversaoLitragemItem> conversaoLitragemItemJoin = root.join(Romaneio_.conversaoLitragem).join(ConversaoLitragem_.conversoes);
        criteriaQuery.where(root.get(Romaneio_.numeroRomaneio).in(romaneios));
        criteriaQuery.select(criteriaBuilder.construct(
                ConversaoLitragemRomaneioDTO.class,
                root.get(Romaneio_.numeroRomaneio),
                criteriaBuilder.count(conversaoLitragemItemJoin.get(ConversaoLitragemItem_.id))
        ));
        criteriaQuery.groupBy(
                root.get(Romaneio_.numeroRomaneio)
        );
        TypedQuery<ConversaoLitragemRomaneioDTO> typedQuery = getSession().createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    @Transactional
    @Override
    public String obterRomaneioQueEstaUtilizandoOCartaoDeAcesso(Integer numeroCartaoAcesso) {

        CriteriaBuilder criteriaBuilder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<String> criteriaQuery = criteriaBuilder.createQuery(String.class);

        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);

        List<Predicate> predicates = new ArrayList<>();

        predicates.add(criteriaBuilder.equal(root.get(Romaneio_.numeroCartaoAcesso), numeroCartaoAcesso));

        predicates.add(criteriaBuilder.equal(root.get(Romaneio_.statusRomaneio), StatusRomaneioEnum.EM_PROCESSAMENTO));

        criteriaQuery.where(predicates.toArray(new Predicate[]{}));

        criteriaQuery.select(root.get(Romaneio_.numeroRomaneio));

        TypedQuery<String> typedQuery = this.getSession().createQuery(criteriaQuery);

        try {

            typedQuery.setMaxResults(1);

            return typedQuery.getSingleResult();

        } catch (NoResultException e) {

            return null;
        }
    }

    @Transactional
    @Override
    public List<RfcStatusRomaneioRequestItem> listarRequestKeysDeRomaneiosPorId(final List<Long> idRomaneios) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<RfcStatusRomaneioRequestItem> criteriaQuery =
                builder.createQuery(RfcStatusRomaneioRequestItem.class);

        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);

        Join<Romaneio, DadosSincronizacao> dadosSincronizacaoJoin = root.join(Romaneio_.operacao);

        Join<Romaneio, ItemNFPedido> itemNFPedidoJoin = root.join(
                Romaneio_.itens,
                JoinType.LEFT
        );

        criteriaQuery.where(builder.and(
                root.get(Romaneio_.id).in(idRomaneios),
                builder.or(
                        builder.isNotNull(root.get(Romaneio_.requestKey)),
                        builder.isNotNull(itemNFPedidoJoin.get(ItemNFPedido_.requestKey))
                )
        ));

        criteriaQuery.select(builder.construct(
                RfcStatusRomaneioRequestItem.class,
                dadosSincronizacaoJoin.get(DadosSincronizacao_.modulo),
                dadosSincronizacaoJoin.get(DadosSincronizacao_.operacao),
                builder.selectCase().when(
                        builder.isNull(root.get(Romaneio_.requestKey)),
                        itemNFPedidoJoin.get(ItemNFPedido_.requestKey)
                ).otherwise(root.get(Romaneio_.requestKey)),
                root.get(Romaneio_.numeroRomaneio)
        ));

        TypedQuery<RfcStatusRomaneioRequestItem> typedQuery = this.getSession().createQuery(criteriaQuery);

        return typedQuery.getResultList();
    }

    @Transactional
    @Override
    public Long obterCountDadosDoRomaneioParaRelatorioDeDeposito(RelatorioDepositoFiltroDTO filtro) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);

        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);

        Join<Romaneio, DadosSincronizacao> dadosSincronizacaoJoin = root.join(Romaneio_.operacao);

        Join<Romaneio, Pesagem> pesagemJoin = root.join(Romaneio_.pesagem);

        Join<Romaneio, Classificacao> classificacaoJoin = root.join(Romaneio_.classificacao);

        Join<Romaneio, ItemNFPedido> itemNFPedidoJoin = root.join(Romaneio_.itens, JoinType.LEFT);

        Join<Romaneio, Produtor> depositoEntradaCabecalhoJoin = root.join(Romaneio_.depositoEntrada, JoinType.LEFT);

        Join<ItemNFPedido, Produtor> depositoEntradaItemJoin = itemNFPedidoJoin.join(ItemNFPedido_.depositoEntrada, JoinType.LEFT);

        Join<Romaneio, Produtor> depositoSaidaCabecalhoJoin = root.join(Romaneio_.depositoSaida, JoinType.LEFT);

        Join<ItemNFPedido, Produtor> depositoSaidaItemJoin = itemNFPedidoJoin.join(ItemNFPedido_.depositoSaida, JoinType.LEFT);

        Join<Romaneio, Material> materialCabelhoJoin = root.join(Romaneio_.material, JoinType.LEFT);

        Join<ItemNFPedido, Material> materialItemJoin = itemNFPedidoJoin.join(ItemNFPedido_.material, JoinType.LEFT);

        Join<Romaneio, Veiculo> veiculoCabelhoJoin = root.join(Romaneio_.placaCavalo, JoinType.LEFT);

        Join<ItemNFPedido, Veiculo> veiculoItemJoin = itemNFPedidoJoin.join(ItemNFPedido_.placaCavalo, JoinType.LEFT);

        Join<Romaneio, Produtor> produtorCabelhoJoin = root.join(Romaneio_.produtor, JoinType.LEFT);

        Join<ItemNFPedido, Produtor> produtorItemJoin = itemNFPedidoJoin.join(ItemNFPedido_.produtor, JoinType.LEFT);

        List<Predicate> predicates = new ArrayList<>();

        predicates.add(builder.equal(dadosSincronizacaoJoin.get(DadosSincronizacao_.gestaoPatio), false));

        if (ObjetoUtil.isNotNull(filtro) && filtro.isNotNull()) {

            if (ObjetoUtil.isNotNull(filtro.getDataInicioRomaneio()) && ObjetoUtil.isNotNull(filtro.getDataFimRomaneio())) {

                predicates.add(builder.between(root.get(Romaneio_.dataCadastro), DateUtil.obterDataHoraZerada(filtro.getDataInicioRomaneio()), DateUtil.converterDateFimDia(filtro.getDataFimRomaneio())));

            } else if (ObjetoUtil.isNotNull(filtro.getDataInicioRomaneio())) {

                predicates.add(builder.greaterThanOrEqualTo(root.get(Romaneio_.dataCadastro), DateUtil.obterDataHoraZerada(filtro.getDataInicioRomaneio())));

            } else if (ObjetoUtil.isNotNull(filtro.getDataFimRomaneio())) {

                predicates.add(builder.lessThanOrEqualTo(root.get(Romaneio_.dataCadastro), DateUtil.converterDateFimDia(filtro.getDataFimRomaneio())));
            }

            if (ObjetoUtil.isNotNull(filtro.getOperacao())) {

                predicates.add(builder.equal(dadosSincronizacaoJoin.get(DadosSincronizacao_.id), filtro.getOperacao().getId()));
            }

            if (ObjetoUtil.isNotNull(filtro.getDepositante())) {

                predicates.add(builder.or(
                        builder.equal(produtorCabelhoJoin.get(Produtor_.cnpj), filtro.getDepositante().getCnpj()),
                        builder.equal(produtorCabelhoJoin.get(Produtor_.cpf), filtro.getDepositante().getCpf()),
                        builder.equal(produtorItemJoin.get(Produtor_.cnpj), filtro.getDepositante().getCnpj()),
                        builder.equal(produtorItemJoin.get(Produtor_.cpf), filtro.getDepositante().getCpf())
                ));
            }

            if (ObjetoUtil.isNotNull(filtro.getDepositoEntrada())) {

                predicates.add(builder.or(
                        builder.equal(depositoEntradaCabecalhoJoin.get(Produtor_.id), filtro.getDepositoEntrada().getId()),
                        builder.equal(depositoEntradaItemJoin.get(Produtor_.id), filtro.getDepositoEntrada().getId())
                ));
            }

            if (ObjetoUtil.isNotNull(filtro.getDepositoSaida())) {

                predicates.add(builder.or(
                        builder.equal(depositoSaidaCabecalhoJoin.get(Produtor_.id), filtro.getDepositoSaida().getId()),
                        builder.equal(depositoSaidaItemJoin.get(Produtor_.id), filtro.getDepositoSaida().getId())
                ));
            }

            if (ObjetoUtil.isNotNull(filtro.getMaterial())) {

                predicates.add(builder.or(
                        builder.equal(materialCabelhoJoin.get(Material_.id), filtro.getMaterial().getId()),
                        builder.equal(materialItemJoin.get(Material_.id), filtro.getMaterial().getId())
                ));
            }
        }

        criteriaQuery.where(predicates.toArray(new Predicate[]{}));

        criteriaQuery.select(builder.count(root.get(Romaneio_.numeroRomaneio)));

        TypedQuery<Long> typedQuery = this.getSession().createQuery(criteriaQuery);

        return typedQuery.getSingleResult();
    }

    @Transactional
    @Override
    public List<RomaneioDepositoDTO> obterDadosDoRomaneioParaRelatorioDeDeposito(RelatorioDepositoFiltroDTO filtro) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<RomaneioDepositoDTO> criteriaQuery = builder.createQuery(RomaneioDepositoDTO.class);

        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);

        Join<Romaneio, DadosSincronizacao> dadosSincronizacaoJoin = root.join(Romaneio_.operacao);

        Join<Romaneio, Pesagem> pesagemJoin = root.join(Romaneio_.pesagem);

        Join<Romaneio, Classificacao> classificacaoJoin = root.join(Romaneio_.classificacao);

        Join<Romaneio, ItemNFPedido> itemNFPedidoJoin = root.join(Romaneio_.itens, JoinType.LEFT);

        Join<Romaneio, Produtor> depositoEntradaCabecalhoJoin = root.join(Romaneio_.depositoEntrada, JoinType.LEFT);

        Join<ItemNFPedido, Produtor> depositoEntradaItemJoin = itemNFPedidoJoin.join(ItemNFPedido_.depositoEntrada, JoinType.LEFT);

        Join<Romaneio, Produtor> depositoSaidaCabecalhoJoin = root.join(Romaneio_.depositoSaida, JoinType.LEFT);

        Join<ItemNFPedido, Produtor> depositoSaidaItemJoin = itemNFPedidoJoin.join(ItemNFPedido_.depositoSaida, JoinType.LEFT);

        Join<Romaneio, Material> materialCabelhoJoin = root.join(Romaneio_.material, JoinType.LEFT);

        Join<ItemNFPedido, Material> materialItemJoin = itemNFPedidoJoin.join(ItemNFPedido_.material, JoinType.LEFT);

        Join<Romaneio, Veiculo> veiculoCabelhoJoin = root.join(Romaneio_.placaCavalo, JoinType.LEFT);

        Join<ItemNFPedido, Veiculo> veiculoItemJoin = itemNFPedidoJoin.join(ItemNFPedido_.placaCavalo, JoinType.LEFT);

        Join<Romaneio, Produtor> produtorCabelhoJoin = root.join(Romaneio_.produtor, JoinType.LEFT);

        Join<ItemNFPedido, Produtor> produtorItemJoin = itemNFPedidoJoin.join(ItemNFPedido_.produtor, JoinType.LEFT);

        List<Predicate> predicates = new ArrayList<>();

        predicates.add(builder.equal(dadosSincronizacaoJoin.get(DadosSincronizacao_.gestaoPatio), false));

        predicates.add(builder.notEqual(root.get(Romaneio_.statusRomaneio), StatusRomaneioEnum.ESTORNADO));

        predicates.add(
                builder.or(
                        builder.notEqual(root.get(Romaneio_.statusIntegracaoSAP), StatusIntegracaoSAPEnum.ESTORNADO),
                        builder.isNull(root.get(Romaneio_.statusIntegracaoSAP))
                )
        );

        if (ObjetoUtil.isNotNull(filtro) && filtro.isNotNull()) {

            if (ObjetoUtil.isNotNull(filtro.getDataInicioRomaneio()) && ObjetoUtil.isNotNull(filtro.getDataFimRomaneio())) {

                predicates.add(builder.between(root.get(Romaneio_.dataCadastro), DateUtil.obterDataHoraZerada(filtro.getDataInicioRomaneio()), DateUtil.converterDateFimDia(filtro.getDataFimRomaneio())));

            } else if (ObjetoUtil.isNotNull(filtro.getDataInicioRomaneio())) {

                predicates.add(builder.greaterThanOrEqualTo(root.get(Romaneio_.dataCadastro), DateUtil.obterDataHoraZerada(filtro.getDataInicioRomaneio())));

            } else if (ObjetoUtil.isNotNull(filtro.getDataFimRomaneio())) {

                predicates.add(builder.lessThanOrEqualTo(root.get(Romaneio_.dataCadastro), DateUtil.converterDateFimDia(filtro.getDataFimRomaneio())));
            }

            if (ObjetoUtil.isNotNull(filtro.getOperacao())) {

                predicates.add(builder.equal(dadosSincronizacaoJoin.get(DadosSincronizacao_.id), filtro.getOperacao().getId()));
            }

            if (ObjetoUtil.isNotNull(filtro.getDepositante())) {

                predicates.add(builder.or(
                        builder.equal(produtorCabelhoJoin.get(Produtor_.cnpj), filtro.getDepositante().getCnpj()),
                        builder.equal(produtorCabelhoJoin.get(Produtor_.cpf), filtro.getDepositante().getCpf()),
                        builder.equal(produtorItemJoin.get(Produtor_.cnpj), filtro.getDepositante().getCnpj()),
                        builder.equal(produtorItemJoin.get(Produtor_.cpf), filtro.getDepositante().getCpf())
                ));
            }

            if (ObjetoUtil.isNotNull(filtro.getDepositoEntrada())) {

                predicates.add(builder.or(
                        builder.equal(depositoEntradaCabecalhoJoin.get(Produtor_.id), filtro.getDepositoEntrada().getId()),
                        builder.equal(depositoEntradaItemJoin.get(Produtor_.id), filtro.getDepositoEntrada().getId())
                ));
            }

            if (ObjetoUtil.isNotNull(filtro.getDepositoSaida())) {

                predicates.add(builder.or(
                        builder.equal(depositoSaidaCabecalhoJoin.get(Produtor_.id), filtro.getDepositoSaida().getId()),
                        builder.equal(depositoSaidaItemJoin.get(Produtor_.id), filtro.getDepositoSaida().getId())
                ));
            }

            if (ObjetoUtil.isNotNull(filtro.getMaterial())) {

                predicates.add(builder.or(
                        builder.equal(materialCabelhoJoin.get(Material_.id), filtro.getMaterial().getId()),
                        builder.equal(materialItemJoin.get(Material_.id), filtro.getMaterial().getId())
                ));
            }
        }

        criteriaQuery.where(predicates.toArray(new Predicate[]{}));

        criteriaQuery.select(builder.construct(
                RomaneioDepositoDTO.class,
                builder.concat(
                        dadosSincronizacaoJoin.get(DadosSincronizacao_.operacao),
                        builder.concat(
                                " - ",
                                dadosSincronizacaoJoin.get(DadosSincronizacao_.descricao)
                        )
                ),
                depositoEntradaCabecalhoJoin.get(Produtor_.codigo),
                depositoEntradaCabecalhoJoin.get(Produtor_.nome),
                depositoEntradaItemJoin.get(Produtor_.codigo),
                depositoEntradaItemJoin.get(Produtor_.nome),
                depositoSaidaCabecalhoJoin.get(Produtor_.codigo),
                depositoSaidaCabecalhoJoin.get(Produtor_.nome),
                depositoSaidaItemJoin.get(Produtor_.codigo),
                depositoSaidaItemJoin.get(Produtor_.nome),
                root.get(Romaneio_.direcaoOperacaoDeposito),
                root.get(Romaneio_.numeroRomaneio),
                root.get(Romaneio_.statusIntegracaoSAP),
                root.get(Romaneio_.dataCadastro),
                veiculoCabelhoJoin.get(Veiculo_.placa1),
                veiculoItemJoin.get(Veiculo_.placa1),
                produtorCabelhoJoin.get(Produtor_.codigo),
                produtorCabelhoJoin.get(Produtor_.nome),
                produtorItemJoin.get(Produtor_.codigo),
                produtorItemJoin.get(Produtor_.nome),
                materialCabelhoJoin.get(Material_.codigo),
                materialCabelhoJoin.get(Material_.descricao),
                materialItemJoin.get(Material_.codigo),
                materialItemJoin.get(Material_.descricao),
                root.get(Romaneio_.dtSimplesEntradaReferencia),
                classificacaoJoin.get(Classificacao_.id),
                classificacaoJoin.get(Classificacao_.dataCadastro),
                pesagemJoin.get(Pesagem_.id),
                pesagemJoin.get(Pesagem_.pesoInicial),
                pesagemJoin.get(Pesagem_.pesoFinal),
                root.get(Romaneio_.pesoBrutoOrigem),
                root.get(Romaneio_.pesoTaraOrigem),
                root.get(Romaneio_.pesoLiquidoUmidoOrigem),
                root.get(Romaneio_.pesoLiquidoSecoOrigem),
                itemNFPedidoJoin.get(ItemNFPedido_.rateio),
                root.get(Romaneio_.requestKey),
                itemNFPedidoJoin.get(ItemNFPedido_.requestKey),
                itemNFPedidoJoin.get(ItemNFPedido_.pesoTotalNfe),
                itemNFPedidoJoin.get(ItemNFPedido_.quebra)
        ));

        criteriaQuery.groupBy(
                dadosSincronizacaoJoin.get(DadosSincronizacao_.operacao),
                dadosSincronizacaoJoin.get(DadosSincronizacao_.descricao),
                depositoEntradaCabecalhoJoin.get(Produtor_.codigo),
                depositoEntradaCabecalhoJoin.get(Produtor_.nome),
                depositoEntradaItemJoin.get(Produtor_.codigo),
                depositoEntradaItemJoin.get(Produtor_.nome),
                depositoSaidaCabecalhoJoin.get(Produtor_.codigo),
                depositoSaidaCabecalhoJoin.get(Produtor_.nome),
                depositoSaidaItemJoin.get(Produtor_.codigo),
                depositoSaidaItemJoin.get(Produtor_.nome),
                root.get(Romaneio_.direcaoOperacaoDeposito),
                root.get(Romaneio_.numeroRomaneio),
                root.get(Romaneio_.dataCadastro),
                veiculoCabelhoJoin.get(Veiculo_.placa1),
                veiculoItemJoin.get(Veiculo_.placa1),
                produtorCabelhoJoin.get(Produtor_.codigo),
                produtorCabelhoJoin.get(Produtor_.nome),
                produtorItemJoin.get(Produtor_.codigo),
                produtorItemJoin.get(Produtor_.nome),
                materialCabelhoJoin.get(Material_.codigo),
                materialCabelhoJoin.get(Material_.descricao),
                materialItemJoin.get(Material_.codigo),
                materialItemJoin.get(Material_.descricao),
                classificacaoJoin.get(Classificacao_.id),
                classificacaoJoin.get(Classificacao_.dataCadastro),
                pesagemJoin.get(Pesagem_.id),
                pesagemJoin.get(Pesagem_.pesoInicial),
                pesagemJoin.get(Pesagem_.pesoFinal),
                root.get(Romaneio_.pesoBrutoOrigem),
                root.get(Romaneio_.pesoTaraOrigem),
                root.get(Romaneio_.pesoLiquidoUmidoOrigem),
                root.get(Romaneio_.pesoLiquidoSecoOrigem),
                itemNFPedidoJoin.get(ItemNFPedido_.rateio),
                root.get(Romaneio_.requestKey),
                itemNFPedidoJoin.get(ItemNFPedido_.requestKey),
                root.get(Romaneio_.statusIntegracaoSAP),
                root.get(Romaneio_.dtSimplesEntradaReferencia),
                itemNFPedidoJoin.get(ItemNFPedido_.pesoTotalNfe),
                itemNFPedidoJoin.get(ItemNFPedido_.quebra)
        );

        criteriaQuery.orderBy(builder.desc(root.get(Romaneio_.dataCadastro)));

        TypedQuery<RomaneioDepositoDTO> typedQuery = this.getSession().createQuery(criteriaQuery);

        if (filtro.getPaginacaoPageNumber() > 0 && filtro.getPaginacaoPageSize() > 0) {
            typedQuery.setFirstResult(filtro.getPaginacaoPageNumber() * filtro.getPaginacaoPageSize());
            typedQuery.setMaxResults(filtro.getPaginacaoPageSize());
        }

        return typedQuery.getResultList();
    }

    @Transactional
    @Override
    public List<RomaneioConversaoLitragemDTO> obterRomaneiosComConversaoLitragem(List<String> romaneios) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<RomaneioConversaoLitragemDTO> criteriaQuery = builder.createQuery(RomaneioConversaoLitragemDTO.class);

        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);

        Join<Romaneio, ConversaoLitragem> conversaoLitragemJoin = root.join(Romaneio_.conversaoLitragem);

        Join<ConversaoLitragem, ConversaoLitragemItem> conversaoLitragemItemJoin = conversaoLitragemJoin.join(ConversaoLitragem_.conversoes);

        criteriaQuery.where(root.get(Romaneio_.numeroRomaneio).in(romaneios));

        criteriaQuery.select(builder.construct(
                RomaneioConversaoLitragemDTO.class,
                root.get(Romaneio_.numeroRomaneio),
                conversaoLitragemItemJoin.get(ConversaoLitragemItem_.volumeSetaVinteGraus),
                conversaoLitragemItemJoin.get(ConversaoLitragemItem_.dataCadastro)
        ));

        criteriaQuery.groupBy(
                root.get(Romaneio_.numeroRomaneio),
                conversaoLitragemItemJoin.get(ConversaoLitragemItem_.volumeSetaVinteGraus),
                conversaoLitragemItemJoin.get(ConversaoLitragemItem_.dataCadastro)
        );

        criteriaQuery.orderBy(builder.desc(conversaoLitragemItemJoin.get(ConversaoLitragemItem_.dataCadastro)));

        TypedQuery<RomaneioConversaoLitragemDTO> typedQuery = this.getSession().createQuery(criteriaQuery);

        return typedQuery.getResultList();
    }

    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    @Override
    public List<RfcStatusRomaneioRequestItem> obterRequestsDeRomaneiosSemNota(final List<Long> idRomaneios) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<RfcStatusRomaneioRequestItem> criteriaQuery = builder.createQuery(RfcStatusRomaneioRequestItem.class);

        Root<RomaneioRequestKey> root = criteriaQuery.from(RomaneioRequestKey.class);

        Join<RomaneioRequestKey, Romaneio> joinRomaneio = root.join(RomaneioRequestKey_.romaneio);

        Join<Romaneio, RetornoNFe> retornoNFeJoin = joinRomaneio.join(Romaneio_.retornoNfes, JoinType.LEFT);

        Join<Romaneio, DadosSincronizacao> dadosSincronizacaoJoin = joinRomaneio.join(Romaneio_.operacao);

        criteriaQuery.where(builder.and(
                joinRomaneio.get(Romaneio_.id).in(idRomaneios),
                builder.or(
                        builder.or(
                                builder.isNull(retornoNFeJoin.get(RetornoNFe_.status)),
                                builder.equal(retornoNFeJoin.get(RetornoNFe_.status), StringUtil.empty())
                        ),
                        builder.isNull(retornoNFeJoin.get(RetornoNFe_.nfe)),
                        builder.isNull(retornoNFeJoin.get(RetornoNFe_.requestKey))
                )
        ));

        criteriaQuery.select(builder.construct(
                RfcStatusRomaneioRequestItem.class,
                dadosSincronizacaoJoin.get(DadosSincronizacao_.modulo),
                dadosSincronizacaoJoin.get(DadosSincronizacao_.operacao),
                root.get(RomaneioRequestKey_.requestKey),
                joinRomaneio.get(Romaneio_.numeroRomaneio)
        ));

        return this.getSession().createQuery(criteriaQuery).getResultList();
    }

    @Override
    @Transactional
    public List<RomaneioEItemDTO> obterIdItens(List<Long> ids) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<RomaneioEItemDTO> criteriaQuery = builder.createQuery(RomaneioEItemDTO.class);

        Root<ItemNFPedido> root = criteriaQuery.from(ItemNFPedido.class);

        Join<ItemNFPedido, Romaneio> romaneioJoin = root.join(ItemNFPedido_.romaneio);

        criteriaQuery.where(romaneioJoin.get(Romaneio_.id).in(ids));

        criteriaQuery.select(builder.construct(
                RomaneioEItemDTO.class,
                root.get(ItemNFPedido_.id),
                romaneioJoin.get(Romaneio_.id)
        ));

        criteriaQuery.orderBy(builder.desc(root.get(ItemNFPedido_.id)));

        TypedQuery<RomaneioEItemDTO> query = this.getSession().createQuery(criteriaQuery);

        return query.getResultList();
    }

    @Transactional
    @Override
    public List<RequestItemDTO> obterRequestKeys(List<Long> ids) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<RequestItemDTO> criteriaQuery = builder.createQuery(RequestItemDTO.class);

        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);

        Join<Romaneio, ItemNFPedido> itemNFPedidoJoin = root.join(Romaneio_.itens);

        Join<Romaneio, DadosSincronizacao> dadosSincronizacaoJoin = root.join(Romaneio_.operacao);

        criteriaQuery.where(
                builder.and(
                        builder.isNotNull(itemNFPedidoJoin.get(ItemNFPedido_.requestKey)),
                        builder.isNull(itemNFPedidoJoin.get(ItemNFPedido_.documentoFaturamento))
                ),
                root.get(Romaneio_.id).in(ids)
        );

        criteriaQuery.orderBy(builder.desc(root.get(Romaneio_.id)));

        criteriaQuery.select(builder.construct(
                RequestItemDTO.class,
                itemNFPedidoJoin.get(ItemNFPedido_.requestKey),
                dadosSincronizacaoJoin.get(DadosSincronizacao_.operacao),
                root.get(Romaneio_.numeroRomaneio)
        ));

        return this.getSession().createQuery(criteriaQuery).getResultList();
    }

    @Transactional
    @Override
    public boolean existeNumeroRomaneio(final String numeroRomaneio) {
        CriteriaBuilder criteriaBuilder = this.getSession().getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);
        criteriaQuery.where(criteriaBuilder.equal(root.get(Romaneio_.numeroRomaneio), numeroRomaneio));
        criteriaQuery.select(criteriaBuilder.count(root.get(Romaneio_.id)));
        TypedQuery<Long> query = this.getSession().createQuery(criteriaQuery);
        Long result = query.getSingleResult();
        return ObjetoUtil.isNotNull(result) && result.doubleValue() > 0;
    }

    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    @Override
    public boolean existeRomaneioVinculadoAsenha(Long idRomaneio, Long idSenha) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);

        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);

        Join<Romaneio, Senha> senhaJoin = root.join(Romaneio_.senha);

        List<Predicate> predicates = new ArrayList<>();

        if (ObjetoUtil.isNotNull(idRomaneio)) {

            predicates.add(builder.not(builder.equal(
                    root.get(Romaneio_.id),
                    idRomaneio
            )));
        }

        predicates.add(root.get(Romaneio_.statusRomaneio).in(Arrays.asList(
                StatusRomaneioEnum.EM_PROCESSAMENTO,
                StatusRomaneioEnum.PENDENTE,
                StatusRomaneioEnum.CONCLUIDO
        )));

        predicates.add(builder.equal(
                senhaJoin.get(Senha_.id),
                idSenha
        ));

        criteriaQuery.where(predicates.toArray(new Predicate[]{}));

        criteriaQuery.select(builder.count(root.get(Romaneio_.id)));

        TypedQuery<Long> query = this.getSession().createQuery(criteriaQuery);

        Long result = query.getSingleResult();

        return ObjetoUtil.isNotNull(result) && result.intValue() > 0;
    }

    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    @Override
    public List<Cliente> obterClientesPorRomaneio(final Long idRomaneio) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<Cliente> criteriaQuery = builder.createQuery(Cliente.class);

        Root<ItemNFPedido> root = criteriaQuery.from(ItemNFPedido.class);

        Join<ItemNFPedido, Romaneio> joinRomaneio = root.join(ItemNFPedido_.romaneio);

        Join<ItemNFPedido, Cliente> joinCliente = root.join(ItemNFPedido_.cliente);

        criteriaQuery.where(builder.equal(
                joinRomaneio.get(Romaneio_.id),
                idRomaneio
        ));

        criteriaQuery.select(builder.construct(
                Cliente.class,
                joinCliente.get(Cliente_.codigo),
                joinCliente.get(Cliente_.cpfCnpj),
                joinCliente.get(Cliente_.nome)
        ));

        return this.getSession().createQuery(criteriaQuery).getResultList();
    }

    @Transactional
    @Override
    public List<RelatorioLaudoSintetico> obterRelatorioLaudoSintetico(RelatorioLaudoFiltroDTO relatorioLaudoFiltro) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<RelatorioLaudoSintetico> criteriaQuery = builder.createQuery(RelatorioLaudoSintetico.class);

        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);

        Join<Romaneio, ItemNFPedido> joinPedido = root.join(Romaneio_.itens, JoinType.INNER);

        Join<Romaneio, RetornoNFe> retornoNFeJoin = root.join(Romaneio_.retornoNfes, JoinType.LEFT);

        Join<ItemNFPedido, Material> joinMaterial = joinPedido.join(ItemNFPedido_.material, JoinType.INNER);

        Join<ItemNFPedido, Veiculo> joinVeiculo = joinPedido.join(ItemNFPedido_.placaCavalo, JoinType.INNER);

        Join<ItemNFPedido, Cliente> joinCliente = joinPedido.join(ItemNFPedido_.cliente, JoinType.INNER);

        Join<Romaneio, Pesagem> joinPesagem = root.join(Romaneio_.pesagem, JoinType.LEFT);

        Join<Romaneio, AnaliseQualidade> joinAnalise = root.join(Romaneio_.analises, JoinType.LEFT);

        Join<AnaliseQualidade, LoteAnaliseQualidade> joinLoteAnaliseQualidade = joinAnalise.join(AnaliseQualidade_.loteAnaliseQualidade, JoinType.LEFT);

        List<Predicate> predicates = new ArrayList<>();

        if (ObjetoUtil.isNotNull(relatorioLaudoFiltro.getDataInicio()) && ObjetoUtil.isNotNull(relatorioLaudoFiltro.getDataFim())) {
            predicates.add(builder.between(
                    root.get(Romaneio_.dataCadastro),
                    DateUtil.obterDataHoraZerada(relatorioLaudoFiltro.getDataInicio()),
                    DateUtil.converterDateFimDia(relatorioLaudoFiltro.getDataFim())
            ));
        } else if (ObjetoUtil.isNotNull(relatorioLaudoFiltro.getDataInicio())) {
            predicates.add(builder.greaterThanOrEqualTo(root.get(Romaneio_.dataCadastro), DateUtil.obterDataHoraZerada(relatorioLaudoFiltro.getDataInicio())));
        } else if (ObjetoUtil.isNotNull(relatorioLaudoFiltro.getDataFim())) {
            predicates.add(builder.lessThanOrEqualTo(root.get(Romaneio_.dataCadastro), DateUtil.converterDateFimDia(relatorioLaudoFiltro.getDataFim())));
        }

        if (ObjetoUtil.isNotNull(relatorioLaudoFiltro.getMaterial())) {
            predicates.add(builder.equal(joinMaterial.get(Material_.id), relatorioLaudoFiltro.getMaterial().getId()));
        }

        predicates.add(
                builder.or(
                        builder.not(builder.equal(
                                root.get(Romaneio_.statusIntegracaoSAP),
                                StatusIntegracaoSAPEnum.ESTORNADO
                        )),
                        builder.not(builder.equal(
                                root.get(Romaneio_.statusRomaneio),
                                StatusRomaneioEnum.ESTORNADO
                        ))
                )
        );

        criteriaQuery.where(predicates.toArray(new Predicate[0]));

        criteriaQuery.select(builder.construct(
                RelatorioLaudoSintetico.class,
                root.get(Romaneio_.id),
                joinPedido.get(ItemNFPedido_.codigoOrdemVenda),
                joinMaterial.get(Material_.id),
                joinMaterial.get(Material_.codigo),
                joinMaterial.get(Material_.descricao),
                joinMaterial.get(Material_.buscarCertificadoQualidade),
                joinCliente.get(Cliente_.codigo),
                joinCliente.get(Cliente_.nome),
                root.get(Romaneio_.numeroRomaneio),
                joinVeiculo.get(Veiculo_.placa1),
                joinPesagem.get(Pesagem_.pesoFinal),
                joinPesagem.get(Pesagem_.pesoInicial),
                retornoNFeJoin.get(RetornoNFe_.nfe),
                joinPedido.get(ItemNFPedido_.unidadeMedidaOrdem),
                builder.max(retornoNFeJoin.get(RetornoNFe_.dataCadastro.getName())),
                joinAnalise.get(AnaliseQualidade_.id),
                joinLoteAnaliseQualidade.get(LoteAnaliseQualidade_.codigo),
                joinAnalise.get(AnaliseQualidade_.lacreAmostra),
                joinAnalise.get(AnaliseQualidade_.idLaudo)
        ));

        criteriaQuery.groupBy(
                root.get(Romaneio_.id),
                joinPedido.get(ItemNFPedido_.codigoOrdemVenda),
                joinMaterial.get(Material_.id),
                joinMaterial.get(Material_.codigo),
                joinMaterial.get(Material_.descricao),
                joinMaterial.get(Material_.buscarCertificadoQualidade),
                joinCliente.get(Cliente_.codigo),
                joinCliente.get(Cliente_.nome),
                root.get(Romaneio_.numeroRomaneio),
                joinVeiculo.get(Veiculo_.placa1),
                joinPesagem.get(Pesagem_.pesoFinal),
                joinPesagem.get(Pesagem_.pesoInicial),
                retornoNFeJoin.get(RetornoNFe_.nfe),
                joinPedido.get(ItemNFPedido_.unidadeMedidaOrdem),
                joinAnalise.get(AnaliseQualidade_.id),
                joinLoteAnaliseQualidade.get(LoteAnaliseQualidade_.codigo),
                joinAnalise.get(AnaliseQualidade_.lacreAmostra),
                joinAnalise.get(AnaliseQualidade_.idLaudo)
        );

        TypedQuery<RelatorioLaudoSintetico> typedQuery = this.getSession().createQuery(criteriaQuery);

        return typedQuery.getResultList();
    }

    @Transactional
    @Override
    public List<RelatorioLaudoAnalitico> obterRelatorioLaudoAnalitico(RelatorioLaudoFiltroDTO relatorioLaudoFiltro) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<RelatorioLaudoAnalitico> criteriaQuery = builder.createQuery(RelatorioLaudoAnalitico.class);

        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);

        Join<Romaneio, ItemNFPedido> joinPedido = root.join(Romaneio_.itens, JoinType.INNER);

        Join<Romaneio, RetornoNFe> retornoNFeJoin = root.join(Romaneio_.retornoNfes, JoinType.LEFT);

        Join<ItemNFPedido, Material> joinMaterial = joinPedido.join(ItemNFPedido_.material, JoinType.INNER);

        Join<ItemNFPedido, Veiculo> joinVeiculo = joinPedido.join(ItemNFPedido_.placaCavalo, JoinType.INNER);

        Join<ItemNFPedido, Cliente> joinCliente = joinPedido.join(ItemNFPedido_.cliente, JoinType.INNER);

        Join<Romaneio, Pesagem> joinPesagem = root.join(Romaneio_.pesagem, JoinType.LEFT);

        Join<Romaneio, AnaliseQualidade> joinAnalise = root.join(Romaneio_.analises);

        Join<AnaliseQualidade, LoteAnaliseQualidade> joinLoteAnaliseQualidade = joinAnalise.join(AnaliseQualidade_.loteAnaliseQualidade);

        Join<AnaliseQualidade, AnaliseQualidadeItem> analiseQualidadeItemJoin = joinAnalise.join(AnaliseQualidade_.itens);

        Join<AnaliseQualidadeItem, AnaliseItem> analiseItemJoin = analiseQualidadeItemJoin.join(AnaliseQualidadeItem_.analiseItem);

        List<Predicate> predicates = new ArrayList<>();

        if (ObjetoUtil.isNotNull(relatorioLaudoFiltro.getDataInicio()) && ObjetoUtil.isNotNull(relatorioLaudoFiltro.getDataFim())) {
            predicates.add(builder.between(
                    root.get(Romaneio_.dataCadastro),
                    DateUtil.obterDataHoraZerada(relatorioLaudoFiltro.getDataInicio()),
                    DateUtil.converterDateFimDia(relatorioLaudoFiltro.getDataFim())
            ));
        } else if (ObjetoUtil.isNotNull(relatorioLaudoFiltro.getDataInicio())) {
            predicates.add(builder.greaterThanOrEqualTo(root.get(Romaneio_.dataCadastro), DateUtil.obterDataHoraZerada(relatorioLaudoFiltro.getDataInicio())));
        } else if (ObjetoUtil.isNotNull(relatorioLaudoFiltro.getDataFim())) {
            predicates.add(builder.lessThanOrEqualTo(root.get(Romaneio_.dataCadastro), DateUtil.converterDateFimDia(relatorioLaudoFiltro.getDataFim())));
        }

        if (ObjetoUtil.isNotNull(relatorioLaudoFiltro.getMaterial())) {
            predicates.add(builder.equal(joinMaterial.get(Material_.id), relatorioLaudoFiltro.getMaterial().getId()));
        }

        predicates.add(
                builder.or(
                        builder.not(builder.equal(
                                root.get(Romaneio_.statusIntegracaoSAP),
                                StatusIntegracaoSAPEnum.ESTORNADO
                        )),
                        builder.not(builder.equal(
                                root.get(Romaneio_.statusRomaneio),
                                StatusRomaneioEnum.ESTORNADO
                        ))
                )
        );

        criteriaQuery.where(predicates.toArray(new Predicate[0]));

        criteriaQuery.select(builder.construct(
                RelatorioLaudoAnalitico.class,
                root.get(Romaneio_.id),
                joinPedido.get(ItemNFPedido_.codigoOrdemVenda),
                joinMaterial.get(Material_.id),
                joinMaterial.get(Material_.codigo),
                joinMaterial.get(Material_.descricao),
                joinMaterial.get(Material_.buscarCertificadoQualidade),
                joinMaterial.get(Material_.necessitaMaisDeUmaAnalise),
                joinCliente.get(Cliente_.codigo),
                joinCliente.get(Cliente_.nome),
                root.get(Romaneio_.numeroRomaneio),
                joinVeiculo.get(Veiculo_.placa1),
                joinPesagem.get(Pesagem_.pesoFinal),
                joinPesagem.get(Pesagem_.pesoInicial),
                retornoNFeJoin.get(RetornoNFe_.nfe),
                joinPedido.get(ItemNFPedido_.unidadeMedidaOrdem),
                builder.max(retornoNFeJoin.get(RetornoNFe_.dataCadastro.getName())),
                joinAnalise.get(AnaliseQualidade_.id),
                joinLoteAnaliseQualidade.get(LoteAnaliseQualidade_.codigo),
                joinAnalise.get(AnaliseQualidade_.lacreAmostra),
                joinAnalise.get(AnaliseQualidade_.idLaudo),
                analiseItemJoin.get(AnaliseItem_.caracteristica),
                analiseItemJoin.get(AnaliseItem_.especificacao),
                analiseItemJoin.get(AnaliseItem_.unidade),
                analiseItemJoin.get(AnaliseItem_.subCaracteristica),
                analiseQualidadeItemJoin.get(AnaliseQualidadeItem_.resultado)
        ));

        criteriaQuery.groupBy(
                root.get(Romaneio_.id),
                joinPedido.get(ItemNFPedido_.codigoOrdemVenda),
                joinMaterial.get(Material_.id),
                joinMaterial.get(Material_.codigo),
                joinMaterial.get(Material_.descricao),
                joinMaterial.get(Material_.buscarCertificadoQualidade),
                joinCliente.get(Cliente_.codigo),
                joinCliente.get(Cliente_.nome),
                root.get(Romaneio_.numeroRomaneio),
                joinVeiculo.get(Veiculo_.placa1),
                joinPesagem.get(Pesagem_.pesoFinal),
                joinPesagem.get(Pesagem_.pesoInicial),
                retornoNFeJoin.get(RetornoNFe_.nfe),
                joinPedido.get(ItemNFPedido_.unidadeMedidaOrdem),
                joinAnalise.get(AnaliseQualidade_.id),
                joinLoteAnaliseQualidade.get(LoteAnaliseQualidade_.codigo),
                joinAnalise.get(AnaliseQualidade_.lacreAmostra),
                joinAnalise.get(AnaliseQualidade_.idLaudo),
                analiseItemJoin.get(AnaliseItem_.caracteristica),
                analiseItemJoin.get(AnaliseItem_.especificacao),
                analiseItemJoin.get(AnaliseItem_.unidade),
                analiseItemJoin.get(AnaliseItem_.subCaracteristica),
                analiseQualidadeItemJoin.get(AnaliseQualidadeItem_.resultado)
        );

        TypedQuery<RelatorioLaudoAnalitico> typedQuery = this.getSession().createQuery(criteriaQuery);

        return typedQuery.getResultList();
    }

    @Transactional
    @Override
    public List<RomaneioGestaoItssAgroDTO> listarRomaneioGestaoItssAgroPorData(Date dataInicial, Date dataFinal) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<RomaneioGestaoItssAgroDTO> criteriaQuery = builder.createQuery(RomaneioGestaoItssAgroDTO.class);

        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);

        Join<Romaneio, ItemNFPedido> joinPedido = root.join(
                Romaneio_.itens,
                JoinType.INNER
        );

        Join<Romaneio, Motorista> joinMotorista = root.join(
                Romaneio_.motorista,
                JoinType.INNER
        );

        Join<Romaneio, DadosSincronizacao> joinDadosSinc = root.join(
                Romaneio_.OPERACAO,
                JoinType.INNER
        );

        Join<ItemNFPedido, Veiculo> joinVeiculo = joinPedido.join(
                ItemNFPedido_.placaCavalo,
                JoinType.INNER
        );

        Join<Romaneio, Pesagem> joinPesagem = root.join(
                Romaneio_.pesagem,
                JoinType.LEFT
        );

        criteriaQuery.where(builder.between(
                root.get(Romaneio_.dataCadastro),
                dataInicial,
                dataFinal
        ));

        criteriaQuery.select(builder.construct(
                RomaneioGestaoItssAgroDTO.class,
                root.get(Romaneio_.numeroRomaneio),
                joinMotorista.get(Motorista_.nome),
                joinVeiculo.get(Veiculo_.placa1),
                joinPesagem.get(Pesagem_.pesoInicial),
                joinPesagem.get(Pesagem_.pesoFinal),
                joinPesagem.get(Pesagem_.id),
                joinPedido.get(ItemNFPedido_.requestKey),
                joinDadosSinc.get(DadosSincronizacao_.gestaoPatio)
        ));

        return this.getSession().createQuery(criteriaQuery).getResultList();
    }

    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    @Override
    public List<Romaneio> obterIdEOperacaoPorNumeroRomaneio(
            final Set<String> numeroRomaneios
    ) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();
        CriteriaQuery<Romaneio> criteriaQuery = builder.createQuery(Romaneio.class);
        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);
        Join<Romaneio, DadosSincronizacao> joinOperacao = root.join(
                Romaneio_.operacao,
                JoinType.INNER
        );
        criteriaQuery.where(root.get(Romaneio_.numeroRomaneio).in(numeroRomaneios));
        criteriaQuery.select(builder.construct(
                Romaneio.class,
                root.get(Romaneio_.id),
                joinOperacao.get(DadosSincronizacao_.id),
                root.get(Romaneio_.numeroRomaneio),
                joinOperacao.get(DadosSincronizacao_.entidade)
        ));
        TypedQuery<Romaneio> typedQuery = this.getSession().createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    @Transactional
    public List<RetornoNFe> obterNotasParaOperacaoSimplesPesagem(List<String> numeroRomaneiosSimplesPesagem) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<RetornoNFe> criteriaQuery = builder.createQuery(RetornoNFe.class);

        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);

        Join<Romaneio, DadosSincronizacao> dadosSincronizacaoJoin = root.join(Romaneio_.operacao);

        Join<Romaneio, RetornoNFe> retornoNFeJoin = root.join(Romaneio_.retornoNfes);

        criteriaQuery.where(
                builder.equal(dadosSincronizacaoJoin.get(DadosSincronizacao_.entidade), DadosSincronizacaoEnum.OPERACAO_SIMPLES_PESAGEM),
                root.get(Romaneio_.numeroRomaneio).in(numeroRomaneiosSimplesPesagem)
        );

        criteriaQuery.select(
                builder.construct(
                        RetornoNFe.class,
                        retornoNFeJoin.get(RetornoNFe_.nfe),
                        retornoNFeJoin.get(RetornoNFe_.dataCriacaoNotaMastersaf),
                        root.get(Romaneio_.numeroRomaneio)
                )
        );

        TypedQuery<RetornoNFe> typedQuery = this.getSession().createQuery(criteriaQuery);

        return typedQuery.getResultList();
    }

    @Transactional
    @Override
    public List<RelatorioFluxoDeCarregamentoDTO> findByDataCadastroAndMaterialIdOrdOrderByIdDescCarregamento(RelatorioFluxoDeCarregamentoFiltroDTO filtro) {

        List<RelatorioFluxoDeCarregamentoDTO> romaneiosParaFluxoCarregamento = obterRomaneiosParaFluxoCarregamento(filtro);

        if (ColecaoUtil.isNotEmpty(romaneiosParaFluxoCarregamento)) {

            List<String> numerosRomaneios = new ArrayList<>();

            numerosRomaneios.addAll(romaneiosParaFluxoCarregamento.stream().map(RelatorioFluxoDeCarregamentoDTO::getNumeroRomaneio).collect(Collectors.toSet()));

            List<RetornoNFe> nfesOperacaoSimplesPesagem = obterNotasParaOperacaoSimplesPesagem(numerosRomaneios);

            if (ColecaoUtil.isNotEmpty(nfesOperacaoSimplesPesagem)) {

                romaneiosParaFluxoCarregamento.parallelStream().filter(item -> numerosRomaneios.stream().filter(n -> n.equals(item.getNumeroRomaneio())).count() > 0).forEach(item -> {
                    nfesOperacaoSimplesPesagem.stream().filter(nfe -> nfe.getRomaneio().getNumeroRomaneio().equals(item.getNumeroRomaneio())).findFirst().ifPresent(nfe -> {
                        item.setNota(nfe.getNfe());
                        item.setDataEmissaoNfe(nfe.getDataCriacaoNotaMastersaf());
                    });
                });
            }
        }

        if (filtro.isNotNull() && (ObjetoUtil.isNotNull(filtro.getDataSenhaInicio()) || ObjetoUtil.isNotNull(filtro.getDataSenhaFim()))) {

            List<Long> senhas = romaneiosParaFluxoCarregamento.stream()
                    .filter(r -> ObjetoUtil.isNotNull(r.getIdSenha()))
                    .map(RelatorioFluxoDeCarregamentoDTO::getIdSenha)
                    .collect(Collectors.toList());

            List<RelatorioFluxoDeCarregamentoDTO> senhasParaFluxoCarregamento = obterSenhasParaFluxoCarregamento(filtro, senhas);

            romaneiosParaFluxoCarregamento.addAll(senhasParaFluxoCarregamento);
        }

        return romaneiosParaFluxoCarregamento;
    }

    private List<RelatorioFluxoDeCarregamentoDTO> obterSenhasParaFluxoCarregamento(RelatorioFluxoDeCarregamentoFiltroDTO filtro, List<Long> senhas) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<RelatorioFluxoDeCarregamentoDTO> criteriaQuery = builder.createQuery(RelatorioFluxoDeCarregamentoDTO.class);

        Root<Senha> root = criteriaQuery.from(Senha.class);

        Join<Senha, Cliente> senhaClienteJoin = root.join(Senha_.cliente, JoinType.LEFT);

        Join<Senha, Cliente> operadorLogisticoJoin = root.join(Senha_.operadorLogistico, JoinType.LEFT);

        Join<Senha, Motorista> senhaMotoristaJoin = root.join(Senha_.motorista, JoinType.LEFT);

        Join<Senha, Veiculo> senhaVeiculoJoin = root.join(Senha_.veiculo, JoinType.LEFT);

        Join<Senha, Transportadora> senhaTransportadoraJoin = root.join(Senha_.transportadora, JoinType.LEFT);

        Join<Senha, Material> materialSenhaJoin = root.join(Senha_.material);

        List<Predicate> predicates = new ArrayList<>();

        if (ColecaoUtil.isNotEmpty(senhas)) {

            predicates.add(builder.not(root.get(Senha_.id).in(senhas)));
        }

        predicates.add(builder.notEqual(root.get(Senha_.statusSenha), StatusSenhaEnum.CANCELADA));

        if (filtro.isNotNull()) {

            final Date dataSenhaInicio = ObjetoUtil.isNotNull(filtro.getDataSenhaInicio()) ? DateUtil.obterDataHoraZerada(filtro.getDataSenhaInicio()) : null;

            final Date dataFimSenha = ObjetoUtil.isNotNull(filtro.getDataSenhaFim()) ? DateUtil.converterDateFimDia(filtro.getDataSenhaFim()) : null;

            if (ObjetoUtil.isNotNull(dataSenhaInicio, dataFimSenha)) {

                predicates.add(builder.between(root.get(Senha_.dataCadastro), dataSenhaInicio, dataFimSenha));

            } else if (ObjetoUtil.isNotNull(dataSenhaInicio)) {

                predicates.add(builder.greaterThanOrEqualTo(root.get(Senha_.dataCadastro), dataSenhaInicio));

            } else if (ObjetoUtil.isNotNull(dataFimSenha)) {

                predicates.add(builder.lessThanOrEqualTo(root.get(Senha_.dataCadastro), dataFimSenha));
            }

            if (ObjetoUtil.isNotNull(filtro.getMaterial())) {

                predicates.add(builder.equal(materialSenhaJoin.get(Material_.id), filtro.getMaterial().getId()));
            }
        }

        criteriaQuery.where(predicates.toArray(new Predicate[]{}));

        criteriaQuery.select(builder.construct(
                RelatorioFluxoDeCarregamentoDTO.class,
                root.get(Senha_.codigoOrdemVenda),
                materialSenhaJoin.get(Material_.codigo),
                materialSenhaJoin.get(Material_.descricao),
                senhaClienteJoin.get(Cliente_.codigo),
                senhaClienteJoin.get(Cliente_.nome),
                operadorLogisticoJoin.get(Cliente_.codigo),
                operadorLogisticoJoin.get(Cliente_.nome),
                operadorLogisticoJoin.get(Cliente_.uf),
                operadorLogisticoJoin.get(Cliente_.municipio),
                senhaTransportadoraJoin.get(Transportadora_.codigo),
                senhaTransportadoraJoin.get(Transportadora_.descricao),
                senhaVeiculoJoin.get(Veiculo_.placa1),
                senhaMotoristaJoin.get(Motorista_.codigo),
                senhaMotoristaJoin.get(Motorista_.nome),
                root.get(Senha_.dataCadastro),
                root.get(Senha_.dataChegadaMotorista),
                root.get(Senha_.cargaPontualDataAgendamentoInicial),
                root.get(Senha_.cargaPontualDataAgendamentoFinal)
        ));

        criteriaQuery.groupBy(
                root.get(Senha_.codigoOrdemVenda),
                materialSenhaJoin.get(Material_.codigo),
                materialSenhaJoin.get(Material_.descricao),
                senhaClienteJoin.get(Cliente_.codigo),
                senhaClienteJoin.get(Cliente_.nome),
                operadorLogisticoJoin.get(Cliente_.codigo),
                operadorLogisticoJoin.get(Cliente_.nome),
                operadorLogisticoJoin.get(Cliente_.uf),
                operadorLogisticoJoin.get(Cliente_.municipio),
                senhaTransportadoraJoin.get(Transportadora_.codigo),
                senhaTransportadoraJoin.get(Transportadora_.descricao),
                senhaVeiculoJoin.get(Veiculo_.placa1),
                senhaMotoristaJoin.get(Motorista_.codigo),
                senhaMotoristaJoin.get(Motorista_.nome),
                root.get(Senha_.dataCadastro),
                root.get(Senha_.dataChegadaMotorista),
                root.get(Senha_.cargaPontualDataAgendamentoInicial),
                root.get(Senha_.cargaPontualDataAgendamentoFinal)
        );

        TypedQuery<RelatorioFluxoDeCarregamentoDTO> typedQuery = this.getSession().createQuery(criteriaQuery);

        return typedQuery.getResultList();
    }

    private List<RelatorioFluxoDeCarregamentoDTO> obterRomaneiosParaFluxoCarregamento(RelatorioFluxoDeCarregamentoFiltroDTO filtro) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<RelatorioFluxoDeCarregamentoDTO> criteriaQuery = builder.createQuery(RelatorioFluxoDeCarregamentoDTO.class);

        Root<Romaneio> root = criteriaQuery.from(Romaneio.class);

        Join<Romaneio, DadosSincronizacao> dadosSincronizacaoJoin = root.join(Romaneio_.operacao);

        Join<Romaneio, AnaliseQualidade> analiseQualidadeJoin = root.join(Romaneio_.analises, JoinType.LEFT);

        Join<Romaneio, Senha> senhaJoin = root.join(Romaneio_.senha, JoinType.LEFT);

        Join<Romaneio, ItemNFPedido> itemNFPedidoJoin = root.join(Romaneio_.itens, JoinType.LEFT);

        Join<Romaneio, Pesagem> pesagemJoin = root.join(Romaneio_.pesagem, JoinType.LEFT);

        Join<ItemNFPedido, Cliente> itemNFPedidoClienteJoin = itemNFPedidoJoin.join(ItemNFPedido_.cliente, JoinType.LEFT);

        Join<ItemNFPedido, Cliente> itemNFPedidoOperadorLogisticoJoin = itemNFPedidoJoin.join(ItemNFPedido_.operadorLogistico, JoinType.LEFT);

        Join<Senha, Cliente> senhaClienteJoin = senhaJoin.join(Senha_.cliente, JoinType.LEFT);

        Join<ItemNFPedido, Motorista> itemNFPedidoMotoristaJoin = itemNFPedidoJoin.join(ItemNFPedido_.motorista, JoinType.LEFT);

        Join<Senha, Motorista> senhaMotoristaJoin = senhaJoin.join(Senha_.motorista, JoinType.LEFT);

        Join<ItemNFPedido, Veiculo> itemNFPedidoVeiculoJoin = itemNFPedidoJoin.join(ItemNFPedido_.placaCavalo, JoinType.LEFT);

        Join<Senha, Veiculo> senhaVeiculoJoin = senhaJoin.join(Senha_.veiculo, JoinType.LEFT);

        Join<ItemNFPedido, Transportadora> itemNFPedidoSubTransportadoraJoin = itemNFPedidoJoin.join(ItemNFPedido_.transportadoraSubcontratada, JoinType.LEFT);

        Join<ItemNFPedido, Transportadora> itemNFPedidoTransportadoraJoin = itemNFPedidoJoin.join(ItemNFPedido_.transportadora, JoinType.LEFT);

        Join<Senha, Transportadora> senhaTransportadoraJoin = senhaJoin.join(Senha_.transportadora, JoinType.LEFT);

        Join<ItemNFPedido, Material> materialItemJoin = itemNFPedidoJoin.join(ItemNFPedido_.material, JoinType.LEFT);

        Join<Senha, Material> materialSenhaJoin = senhaJoin.join(Senha_.material, JoinType.LEFT);

        Join<Romaneio, RetornoNFe> retornoNFeJoin = root.join(Romaneio_.retornoNfes, JoinType.LEFT);

        retornoNFeJoin.on(builder.equal(retornoNFeJoin.get(RetornoNFe_.requestKey), itemNFPedidoJoin.get(ItemNFPedido_.requestKey)));

        List<Predicate> predicates = new ArrayList<>();

        predicates.add(builder.isTrue(dadosSincronizacaoJoin.get(DadosSincronizacao_.gestaoPatio)));

        predicates.add(builder.notEqual(senhaJoin.get(Senha_.statusSenha), StatusSenhaEnum.CANCELADA));

        if (filtro.isNotNull()) {

            final Date dataInicio = ObjetoUtil.isNotNull(filtro.getDataInicio()) ? DateUtil.obterDataHoraZerada(filtro.getDataInicio()) : null;

            final Date dataSenhaInicio = ObjetoUtil.isNotNull(filtro.getDataSenhaInicio()) ? DateUtil.obterDataHoraZerada(filtro.getDataSenhaInicio()) : null;

            final Date dataFim = ObjetoUtil.isNotNull(filtro.getDataFim()) ? DateUtil.converterDateFimDia(filtro.getDataFim()) : null;

            final Date dataFimSenha = ObjetoUtil.isNotNull(filtro.getDataSenhaFim()) ? DateUtil.converterDateFimDia(filtro.getDataSenhaFim()) : null;

            predicates.addAll(predicatesParaFluxoCarregamento(
                    builder,
                    dataInicio,
                    dataSenhaInicio,
                    dataFim,
                    dataFimSenha,
                    filtro.getMaterial(),
                    root,
                    dadosSincronizacaoJoin,
                    senhaJoin,
                    materialItemJoin,
                    materialSenhaJoin
            ));
        }

        criteriaQuery.select(builder.construct(
                RelatorioFluxoDeCarregamentoDTO.class,
                root.get(Romaneio_.statusRomaneio),
                root.get(Romaneio_.statusIntegracaoSAP),
                itemNFPedidoJoin.get(ItemNFPedido_.codigoOrdemVenda),
                root.get(Romaneio_.numeroRomaneio),
                builder.selectCase().when(
                        builder.isNotNull(materialItemJoin.get(Material_.codigo)),
                        materialItemJoin.get(Material_.realizarConversaoLitragem)
                ).otherwise(materialSenhaJoin.get(Material_.realizarConversaoLitragem)),
                builder.selectCase().when(
                        builder.isNotNull(materialItemJoin.get(Material_.codigo)),
                        materialItemJoin.get(Material_.codigo)
                ).otherwise(materialSenhaJoin.get(Material_.codigo)),
                builder.selectCase().when(
                        builder.isNotNull(materialItemJoin.get(Material_.descricao)),
                        materialItemJoin.get(Material_.descricao)
                ).otherwise(materialSenhaJoin.get(Material_.descricao)),
                builder.selectCase().when(
                        builder.isNotNull(itemNFPedidoClienteJoin.get(Cliente_.codigo)),
                        itemNFPedidoClienteJoin.get(Cliente_.codigo)
                ).otherwise(senhaClienteJoin.get(Cliente_.codigo)),
                builder.selectCase().when(
                        builder.isNotNull(itemNFPedidoClienteJoin.get(Cliente_.nome)),
                        itemNFPedidoClienteJoin.get(Cliente_.nome)
                ).otherwise(senhaClienteJoin.get(Cliente_.nome)),
                builder.selectCase().when(
                        builder.isNotNull(itemNFPedidoTransportadoraJoin.get(Transportadora_.codigo)),
                        itemNFPedidoTransportadoraJoin.get(Transportadora_.codigo)
                ).otherwise(senhaTransportadoraJoin.get(Transportadora_.codigo)),
                builder.selectCase().when(
                        builder.isNotNull(itemNFPedidoTransportadoraJoin.get(Transportadora_.descricao)),
                        itemNFPedidoTransportadoraJoin.get(Transportadora_.descricao)
                ).otherwise(senhaTransportadoraJoin.get(Transportadora_.descricao)),
                itemNFPedidoSubTransportadoraJoin.get(Transportadora_.codigo),
                itemNFPedidoSubTransportadoraJoin.get(Transportadora_.descricao),
                builder.selectCase().when(
                        builder.isNotNull(itemNFPedidoVeiculoJoin.get(Veiculo_.placa1)),
                        itemNFPedidoVeiculoJoin.get(Veiculo_.placa1)
                ).otherwise(senhaVeiculoJoin.get(Veiculo_.placa1)),
                builder.selectCase().when(
                        builder.isNotNull(itemNFPedidoMotoristaJoin.get(Motorista_.codigo)),
                        itemNFPedidoMotoristaJoin.get(Motorista_.codigo)
                ).otherwise(senhaMotoristaJoin.get(Motorista_.codigo)),
                builder.selectCase().when(
                        builder.isNotNull(itemNFPedidoMotoristaJoin.get(Motorista_.nome)),
                        itemNFPedidoMotoristaJoin.get(Motorista_.nome)
                ).otherwise(senhaMotoristaJoin.get(Motorista_.nome)),
                itemNFPedidoOperadorLogisticoJoin.get(Cliente_.codigo),
                itemNFPedidoOperadorLogisticoJoin.get(Cliente_.nome),
                itemNFPedidoOperadorLogisticoJoin.get(Cliente_.uf),
                itemNFPedidoOperadorLogisticoJoin.get(Cliente_.municipio),
                senhaJoin.get(Senha_.dataCadastro),
                senhaJoin.get(Senha_.dataChegadaMotorista),
                senhaJoin.get(Senha_.cargaPontualDataAgendamentoInicial),
                senhaJoin.get(Senha_.cargaPontualDataAgendamentoFinal),
                root.get(Romaneio_.dataCadastro),
                builder.max(analiseQualidadeJoin.get(AnaliseQualidade_.dataCadastro.getName())),
                retornoNFeJoin.get(RetornoNFe_.nfe),
                retornoNFeJoin.get(RetornoNFe_.dataCriacaoNotaMastersaf),
                senhaJoin.get(Senha_.id),
                pesagemJoin.get(Pesagem_.id),
                pesagemJoin.get(Pesagem_.pesoInicial),
                pesagemJoin.get(Pesagem_.pesoFinal),
                itemNFPedidoJoin.get(ItemNFPedido_.rateio)
        ));

        criteriaQuery.groupBy(
                root.get(Romaneio_.statusRomaneio),
                root.get(Romaneio_.statusIntegracaoSAP),
                itemNFPedidoJoin.get(ItemNFPedido_.codigoOrdemVenda),
                root.get(Romaneio_.numeroRomaneio),
                itemNFPedidoSubTransportadoraJoin.get(Transportadora_.codigo),
                itemNFPedidoSubTransportadoraJoin.get(Transportadora_.descricao),
                root.get(Romaneio_.dataCadastro),
                senhaJoin.get(Senha_.dataCadastro),
                senhaJoin.get(Senha_.dataChegadaMotorista),
                senhaJoin.get(Senha_.cargaPontualDataAgendamentoInicial),
                senhaJoin.get(Senha_.cargaPontualDataAgendamentoFinal),
                retornoNFeJoin.get(RetornoNFe_.nfe),
                retornoNFeJoin.get(RetornoNFe_.dataCriacaoNotaMastersaf),
                materialItemJoin.get(Material_.codigo),
                materialSenhaJoin.get(Material_.codigo),
                materialItemJoin.get(Material_.descricao),
                materialSenhaJoin.get(Material_.descricao),
                itemNFPedidoClienteJoin.get(Cliente_.codigo),
                senhaClienteJoin.get(Cliente_.codigo),
                itemNFPedidoClienteJoin.get(Cliente_.nome),
                senhaClienteJoin.get(Cliente_.nome),
                itemNFPedidoTransportadoraJoin.get(Transportadora_.codigo),
                senhaTransportadoraJoin.get(Transportadora_.codigo),
                itemNFPedidoTransportadoraJoin.get(Transportadora_.descricao),
                senhaTransportadoraJoin.get(Transportadora_.descricao),
                itemNFPedidoVeiculoJoin.get(Veiculo_.placa1),
                senhaVeiculoJoin.get(Veiculo_.placa1),
                itemNFPedidoMotoristaJoin.get(Motorista_.codigo),
                senhaMotoristaJoin.get(Motorista_.codigo),
                itemNFPedidoMotoristaJoin.get(Motorista_.nome),
                senhaMotoristaJoin.get(Motorista_.nome),
                itemNFPedidoOperadorLogisticoJoin.get(Cliente_.codigo),
                itemNFPedidoOperadorLogisticoJoin.get(Cliente_.nome),
                itemNFPedidoOperadorLogisticoJoin.get(Cliente_.uf),
                itemNFPedidoOperadorLogisticoJoin.get(Cliente_.municipio),
                materialItemJoin.get(Material_.realizarConversaoLitragem),
                materialSenhaJoin.get(Material_.realizarConversaoLitragem),
                senhaJoin.get(Senha_.id),
                pesagemJoin.get(Pesagem_.id),
                pesagemJoin.get(Pesagem_.pesoInicial),
                pesagemJoin.get(Pesagem_.pesoFinal),
                itemNFPedidoJoin.get(ItemNFPedido_.rateio)
        );

        criteriaQuery.where(builder.and(predicates.toArray(new Predicate[]{})));

        TypedQuery<RelatorioFluxoDeCarregamentoDTO> typedQuery = this.getSession().createQuery(criteriaQuery);

        return typedQuery.getResultList();
    }

    private List<Predicate> predicatesParaFluxoCarregamento(
            CriteriaBuilder builder,
            Date dataInicioFiltro,
            Date dataSenhaInicioFiltro,
            Date dataFimFiltro,
            Date dataFimSenhaFiltro,
            Material materialFiltro,
            Root<Romaneio> root,
            Join<Romaneio, DadosSincronizacao> dadosSincronizacaoJoin,
            Join<Romaneio, Senha> senhaJoin,
            Join<ItemNFPedido, Material> materialItemJoin,
            Join<Senha, Material> materialSenhaJoin
    ) {

        List<Predicate> predicates = new ArrayList<>();

        if (ObjetoUtil.isNotNull(
                dataInicioFiltro,
                dataFimFiltro
        )) {

            predicates.add(builder.between(
                    root.get(Romaneio_.dataCadastro),
                    dataInicioFiltro,
                    dataFimFiltro
            ));

        } else if (ObjetoUtil.isNotNull(dataInicioFiltro)) {

            predicates.add(builder.greaterThanOrEqualTo(
                    root.get(Romaneio_.dataCadastro),
                    dataInicioFiltro
            ));

        } else if (ObjetoUtil.isNotNull(dataFimFiltro)) {

            predicates.add(builder.lessThanOrEqualTo(
                    root.get(Romaneio_.dataCadastro),
                    dataFimFiltro
            ));
        }

        if (ObjetoUtil.isNotNull(
                dataSenhaInicioFiltro,
                dataFimSenhaFiltro
        )) {

            predicates.add(builder.between(
                    senhaJoin.get(Senha_.dataCadastro),
                    dataSenhaInicioFiltro,
                    dataFimSenhaFiltro
            ));

        } else if (ObjetoUtil.isNotNull(dataSenhaInicioFiltro)) {

            predicates.add(builder.greaterThanOrEqualTo(
                    senhaJoin.get(Senha_.dataCadastro),
                    dataSenhaInicioFiltro
            ));

        } else if (ObjetoUtil.isNotNull(dataFimSenhaFiltro)) {

            predicates.add(builder.lessThanOrEqualTo(
                    senhaJoin.get(Senha_.dataCadastro),
                    dataFimSenhaFiltro
            ));
        }

        if (ObjetoUtil.isNotNull(materialFiltro)) {

            predicates.add(builder.or(
                    builder.equal(
                            materialItemJoin.get(Material_.id),
                            materialFiltro.getId()
                    ),
                    builder.equal(
                            materialSenhaJoin.get(Material_.id),
                            materialFiltro.getId()
                    )
            ));
        }

        return predicates;
    }

    @Transactional
    @Override
    public List<RelatorioComercial_V2_DTO> findByDataCadastroAndMaterialIdOrdOrderByIdDesc_V2(
            RelatorioComercialFiltroDTO filtro
    ) {

        StringBuilder sb = new StringBuilder();

        sb.append("select ");
        sb.append("	r.status_romaneio, ");
        sb.append("	r.status_sap, ");
        sb.append("	cast(cast(i.codigo_ordem_venda as integer) as varchar), ");
        sb.append("	i.numero_pedido, ");
        sb.append("	m.codigo as codigoMaterial, ");
        sb.append("	m.desc_material, ");
        sb.append("	c.codigo as codigoCliente, ");
        sb.append("	c.nome as nomeCliente, ");
        sb.append("	t.codigo as codigoTransportadora, ");
        sb.append("	t.desc_transportadora, ");
        sb.append("	r.nr_romaneio, ");
        sb.append("	v.placa_1, ");
        sb.append("	mo.codigo as codigoMotorista, ");
        sb.append("	mo.nome as nomeMotorista, ");
        sb.append("	rn.data_criacao_nota_mastersaf, ");
        sb.append("	rn.nfe, ");
        sb.append("	i.nr_remessa, ");
        sb.append("	i.nr_ordem_transporte, ");
        sb.append("	i.documento_faturamento, ");
        sb.append("	case ");
        sb.append("		when m.realizar_conversao_litragem then ( ");
        sb.append("		select ");
        sb.append("			volume_seta_vinte_graus ");
        sb.append("		from ");
        sb.append("			sap.tb_conversao_litragem_item ");
        sb.append("		where ");
        sb.append("			id_conversao_litragem = r.id_conversao_litragem ");
        sb.append("		order by ");
        sb.append("			dt_cadastro desc ");
        sb.append("		limit 1 offset 0 ) ");
        sb.append("		else (p.peso_inicial - p.peso_final) ");
        sb.append("	end, ");
        sb.append("	i.vlr_rateio, ");
        sb.append(" i.id, ");
        sb.append(" m.un_medida ");
        sb.append("from ");
        sb.append("	sap.tb_romaneio as r ");
        sb.append("inner join sap.tb_item_nf_pedido as i on ");
        sb.append("	i.id_romaneio = r.id ");
        sb.append("inner join sap.tb_material as m on ");
        sb.append("	m.id = i.id_material ");
        sb.append("inner join sap.tb_cliente as c on ");
        sb.append("	c.id = i.id_cliente ");
        sb.append("inner join sap.tb_motorista as mo on ");
        sb.append("	mo.id = i.id_motorista ");
        sb.append("inner join sap.tb_veiculo as v on ");
        sb.append("	v.id = i.id_placa_cavalo ");
        sb.append("inner join sap.tb_transportadora as t on ");
        sb.append("	t.id = i.id_transportadora ");
        sb.append("inner join cfg.tb_dados_sincronizacao as o on ");
        sb.append("	o.id = r.id_operacao ");
        sb.append("left join sap.tb_conversao_litragem as cl on ");
        sb.append("	cl.id = r.id_conversao_litragem ");
        sb.append("inner join sap.tb_pesagem as p on ");
        sb.append("	p.id = r.id_pesagem ");
        sb.append("left join sap.tb_retorno_nfe as rn on ");
        sb.append("	rn.request_key = i.request_key ");

        sb.append(" where o.gestao_patio = true  ");

        sb.append(" and i.codigo_ordem_venda is not null ");

        sb.append(" and r.status_romaneio <> 'ESTORNADO' ");

        sb.append(" and r.status_sap <> 'ESTORNADO' ");

        if (filtro.isNotNull()) {

            String dataInicio = ObjetoUtil.isNotNull(filtro.getDataInicio()) ? DateUtil.formatTo(
                    DateUtil.obterDataHoraZerada(filtro.getDataInicio()),
                    "yyyy-MM-dd HH:mm:ss"
            ) : null;

            String dataFinal = ObjetoUtil.isNotNull(filtro.getDataFim()) ? DateUtil.formatTo(
                    DateUtil.converterDateFimDia(filtro.getDataFim()),
                    "yyyy-MM-dd HH:mm:ss"
            ) : null;

            if (ObjetoUtil.isNotNull(filtro.getDataInicio()) && ObjetoUtil.isNotNull(filtro.getDataFim())) {

                sb.append(" and	rn.data_criacao_nota_mastersaf between '" + dataInicio + "' and '" + dataFinal + "' ");

            } else if (ObjetoUtil.isNotNull(filtro.getDataInicio())) {

                sb.append(" and	rn.data_criacao_nota_mastersaf >= '" + dataInicio + "'");

            } else if (ObjetoUtil.isNotNull(filtro.getDataFim())) {

                sb.append(" and	rn.data_criacao_nota_mastersaf <= '" + dataFinal + "'");
            }

            if (StringUtil.isNotNullEmpty(filtro.getCodigoOrdemVenda())) {

                sb.append(" and lpad(i.codigo_ordem_venda, 10, '0') = '" + StringUtils.leftPad(filtro.getCodigoOrdemVenda(), 10, "0") + "' ");
            }

            if (ColecaoUtil.isNotEmpty(filtro.getMateriais())) {

                sb.append(" and m.id in " + filtro.getMateriais()
                        .stream()
                        .map(Material::getId)
                        .map(i -> i.toString())
                        .collect(Collectors.joining(
                                ",",
                                "(",
                                ")"
                        )));
            }
        }

        sb.append("order by ");
        sb.append("	r.dt_cadastro desc ");

        List<Object[]> retorno = this.getSession().createNativeQuery(sb.toString()).getResultList();

        if (ColecaoUtil.isNotEmpty(retorno)) {

            return retorno.stream().map(r -> new RelatorioComercial_V2_DTO(
                    StatusRomaneioEnum.valueOf((String) r[0]),
                    (String) r[1] == null ? null : StatusIntegracaoSAPEnum.valueOf((String) r[1]),
                    (String) r[2],
                    (String) r[3],
                    (String) r[4],
                    (String) r[5],
                    (String) r[6],
                    (String) r[7],
                    (String) r[8],
                    (String) r[9],
                    (String) r[10],
                    (String) r[11],
                    (String) r[12],
                    (String) r[13],
                    (Date) r[14],
                    (String) r[15],
                    (String) r[16],
                    (String) r[17],
                    (BigInteger) r[18],
                    (BigDecimal) r[19],
                    (BigDecimal) r[20],
                    (BigInteger) r[21],
                    (String) r[22]
            )).collect(Collectors.toList());
        }

        return null;
    }

    @Transactional
    @Override
    public List<RelatorioComercialDTO> findByDataCadastroAndMaterialIdOrdOrderByIdDesc(RelatorioComercialFiltroDTO filtro) {

        StringBuilder sb = new StringBuilder();

        sb.append("select \n"
                + " \tdistinct on (i.id) i.id ,\n"
                + "\ti.codigo_ordem_venda as codigoOrdemVenda, "
                + "\n" + "\ti.numero_pedido as pedido, \n"
                + "    concat(ma.codigo, ' - ', ma" + ".desc_material) as material, \n"
                + "\tconcat(c.codigo, ' - ', c.nome) as cliente, \n"
                + "\tconcat(t.codigo, ' - ', t.desc_transportadora) as transportadora, \n"
                + "\tconcat(st.codigo, ' - ', st.desc_transportadora) as transportadoraSubcontratada, \n"
                + "\tr.nr_romaneio as numeroRomaneio, \n"
                + "\tconcat(substring(v.placa_1 from 1 for 3), '-', substring(v.placa_1 from 4)) as placa, \n"
                + "\tconcat(m.codigo, ' - ', m.nome) as motorista, \n"
                + "\tr.status_romaneio as statusRomaneio, \n"
                + "\tr.status_sap as statusSap, \n"
                + "\tp.peso_final as pesoTara, \n"
                + "\tp.peso_inicial as pesoBruto, \n"
                + "\t(p.peso_inicial - p.peso_final) as pesoLiquido, \n"
                + "\tconcat(rnfe.nfe, ' - ', rnfe.serie) as nota, \n"
                + "   i.nr_remessa as remessa, \n"
                + "   i.nr_ordem_transporte as numeroOrdemTransporte, \n"
                + "\tcase \n" + "\t\twhen i.vlr_rateio > 0 then i.vlr_rateio \n"
                + "\t\twhen ma.realizar_conversao_litragem then ( \n" + "\t\tselect \n"
                + "\t\t\tvolume_seta_vinte_graus \n" + "\t\tfrom \n" + "\t\t\tsap.tb_conversao_litragem_item \n"
                + "       where id_conversao_litragem = r.id_conversao_litragem \n" + "\t\torder by \n"
                + "\t\t\tdt_cadastro desc \n" + "\t\tlimit 1 offset 0 ) \n" + "\t\telse (p.peso_inicial - p.peso_final) \n"
                + "\tend as qtdKgL, \n"
                + "\tmo.descricao as descricaoMotivo, \n"
                + "\tr.romaneio_vinculado_por_motivo as romaneioVinculadoPorMotivo, \n"
                + "\tr.dt_cadastro as dt_cadastro, \n"
                + "\tr.id_pesagem as id_pesagem \n"
                + " from \n" + "\tsap.tb_romaneio as r \n"
                + " inner join sap.tb_item_nf_pedido as i on \n"
                + "\ti.id_romaneio = r.id \n"
                + "inner join sap.tb_cliente as c on \n" + "\tc.id = i.id_cliente \n"
                + "left join sap.tb_material as ma on \n" + "\tma.id = i.id_material \n"
                + "left join public.tb_motivo as mo on \n" + "\tmo.id = r.id_motivo \n"
                + "left join sap.tb_transportadora as t on \n" + "\tt.id = i.id_transportadora \n"
                + "left join sap.tb_transportadora as st on \n" + "\tst.id = i.id_transportadora_subcontratada \n"
                + "left join sap.tb_veiculo as v on \n" + "\tv.id = i.id_placa_cavalo \n"
                + "left join sap.tb_motorista as m on \n" + "\tm.id = i.id_motorista \n"
                + "left join sap.tb_pesagem as p on \n" + "\tp.id = r.id_pesagem \n"
                + "left join sap.tb_retorno_nfe as rnfe on \n" + "\trnfe.id_romaneio = r.id \n"
                + "left join (\n" + "SELECT\n" + "\tDISTINCT ON (id_romaneio) id_romaneio as idar,\n" + "\t*\n" + "FROM\n" + "\tsap.tb_analise_qualidade \n" + "ORDER BY id_romaneio, dt_cadastro\t\n" + ") as aq on \n" + "\taq.id_romaneio = r.id \n" + "left join public.tb_documento as doc on \n" + "\tdoc.id = aq.id_assinatura \n" + "left join public.tb_senha as se on \n" + " se.id = r.id_senha\n");
        if (filtro.isNotNull()) {
            sb.append("where ");
            List<String> condicoes = new ArrayList<>();
            if (ObjetoUtil.isNotNull(filtro.getDataInicio()) && ObjetoUtil.isNotNull(filtro.getDataFim())) {
                condicoes.add("( r.dt_cadastro >= '" + DateUtil.formatTo(
                        DateUtil.obterDataHoraZerada(filtro.getDataInicio()),
                        "yyyy-MM-dd HH:mm:ss"
                ) + "' and r.dt_cadastro <= '" + DateUtil.formatTo(
                        DateUtil.converterDateFimDia(filtro.getDataFim()),
                        "yyyy-MM-dd HH:mm:ss"
                ) + "') ");
            } else if (ObjetoUtil.isNotNull(filtro.getDataInicio())) {
                condicoes.add("r.dt_cadastro >= '" + DateUtil.formatTo(
                        DateUtil.obterDataHoraZerada(filtro.getDataInicio()),
                        "yyyy-MM-dd HH:mm:ss"
                ) + "' ");
            } else if (ObjetoUtil.isNotNull(filtro.getDataFim())) {
                condicoes.add("r.dt_cadastro <= '" + DateUtil.formatTo(
                        DateUtil.converterDateFimDia(filtro.getDataFim()),
                        "yyyy-MM-dd HH:mm:ss"
                ) + "' ");
            }
            if (ObjetoUtil.isNotNull(filtro.getMaterial())) {
                condicoes.add(" ma.id = " + filtro.getMaterial().getId());
            }
            if (ObjetoUtil.isNotNull(filtro.getCliente())) {
                condicoes.add(" c.id = " + filtro.getCliente().getId());
            }
            sb.append(condicoes.stream().collect(Collectors.joining(" and ")));
        }
        sb.append("\norder by ");
        sb.append("	i.id desc, r.id desc ");

        List<Object[]> retorno = this.getSession().createNativeQuery(sb.toString()).getResultList();

        if (ColecaoUtil.isNotEmpty(retorno)) {

            return retorno.stream().map(r -> new RelatorioComercialDTO(
                    (String) r[1],
                    (String) r[2],
                    (String) r[3],
                    (String) r[4],
                    (String) r[5],
                    (String) r[6],
                    (String) r[7],
                    (String) r[8],
                    (String) r[9],
                    StatusRomaneioEnum.valueOf((String) r[10]),
                    (String) r[11] == null ? null : StatusIntegracaoSAPEnum.valueOf((String) r[11]),
                    (BigDecimal) r[12],
                    (BigDecimal) r[13],
                    (BigDecimal) r[14],
                    (String) r[15],
                    (String) r[16],
                    (String) r[17],
                    (BigDecimal) r[18],
                    (String) r[19],
                    (String) r[20],
                    ((BigInteger) r[0]).longValue(),
                    (Date) r[21],
                    r[22] == null ? null : Long.parseLong(String.valueOf(r[22]))
            )).collect(Collectors.toList());
        }

        return null;
    }

    @Transactional
    @Override
    public List<RelComercialNFeDTO> getRelatorioComercialNFeDTOS(RelatorioComercialFiltroDTO filtro) {

        StringBuilder sb1 = new StringBuilder();

        sb1.append("select\n");
        sb1.append("\tdistinct on (nf.nfe) nfe,\n");
        sb1.append("\tnr_romaneio,\n");
        sb1.append("\tconcat(nf.nfe, ' - ', nf.serie),\n");
        sb1.append("\tnf.dt_cadastro\n");
        sb1.append("from\n");
        sb1.append("\tsap.tb_romaneio as r \n");
        sb1.append("\tleft join sap.tb_retorno_nfe as nf on\n");
        sb1.append("\t\tr.id = nf.id_romaneio\n");
        sb1.append("\tinner join sap.tb_item_nf_pedido as i on\n");
        sb1.append("\t\ti.id_romaneio = r.id\n");
        sb1.append("\tleft join sap.tb_material as ma on\n");
        sb1.append("\t\tma.id = i.id_material\n");
        if (filtro.isNotNull()) {
            sb1.append("where ");
            if (ObjetoUtil.isNotNull(filtro.getDataInicio()) && ObjetoUtil.isNotNull(filtro.getDataFim())) {
                sb1.append("( r.dt_cadastro >= '" + DateUtil.formatTo(
                        DateUtil.obterDataHoraZerada(filtro.getDataInicio()),
                        "yyyy-MM-dd HH:mm:ss"
                ) + "' and r.dt_cadastro <= '" + DateUtil.formatTo(
                        DateUtil.converterDateFimDia(filtro.getDataFim()),
                        "yyyy-MM-dd HH:mm:ss"
                ) + "') ");
                if (ObjetoUtil.isNotNull(filtro.getMaterial())) {
                    sb1.append(" and ");
                }
            } else if (ObjetoUtil.isNotNull(filtro.getDataInicio())) {
                sb1.append("r.dt_cadastro >= '" + DateUtil.formatTo(
                        DateUtil.obterDataHoraZerada(filtro.getDataInicio()),
                        "yyyy-MM-dd HH:mm:ss"
                ) + "' ");
                if (ObjetoUtil.isNotNull(filtro.getMaterial())) {
                    sb1.append(" and ");
                }
            } else if (ObjetoUtil.isNotNull(filtro.getDataFim())) {
                sb1.append("r.dt_cadastro <= '" + DateUtil.formatTo(
                        DateUtil.converterDateFimDia(filtro.getDataFim()),
                        "yyyy-MM-dd HH:mm:ss"
                ) + "' ");
                if (ObjetoUtil.isNotNull(filtro.getMaterial())) {
                    sb1.append(" and ");
                }
            }
            if (ObjetoUtil.isNotNull(filtro.getMaterial())) {
                sb1.append(" ma.id = " + filtro.getMaterial().getId());
            }
        }
        sb1.append("\n\torder by  nf.nfe desc, r.id desc, nr_romaneio asc, nf.id desc");

        List<Object[]> retorno = this.getSession().createNativeQuery(sb1.toString()).getResultList();

        if (ColecaoUtil.isNotEmpty(retorno)) {
            return retorno.stream().map(r -> new RelComercialNFeDTO(
                    (String) r[2],
                    (String) r[1],
                    (Date) r[3]
            )).collect(Collectors.toList());
        }

        return null;
    }

    @Transactional
    @Override
    public List<RelatorioAnaliseConversaoDTO> obterRelatorioAnaliseConversao(RelatorioAnaliseConversaoFiltroDTO filtro) {

        StringBuilder sb = new StringBuilder();

        sb.append("SELECT");
        sb.append("\tDISTINCT ON (c.nr_romaneio) c.nr_romaneio,\n");
        sb.append("\tc.tanque,\n");
        sb.append("\tc.id,\n");
        sb.append("\tc.dt_cadastro,\n");
        sb.append("\tc.temp_tanque, \n");
        sb.append("\tc.grau_inpm, \n");
        sb.append("\tc.acidez, \n");
        sb.append("\tc.volume_seta_vinte_graus,\n");
        sb.append("\tc.volume_densidade_l_vinte_graus,\n");
        sb.append("\tc.diferenca_litros \n");
        sb.append("FROM (\n");
        sb.append("\tSELECT\n");
        sb.append("\t\tr.nr_romaneio,\n");
        sb.append("\t\tcl.tanque,\n");
        sb.append("\t\tcl.id,\n");
        sb.append("\t\tcase when i.dt_cadastro is null then cl.dt_cadastro else i.dt_cadastro end as dt_cadastro," +
                "\n");
        sb.append("\t\tcase when i.temp_tanque is null then cl.temp_tanque else i.temp_tanque end as temp_tanque, " +
                "\n");
        sb.append("\t\tcase when i.grau_inpm is null then cl.grau_inpm else i.grau_inpm end as grau_inpm, \n");
        sb.append("\t\tcase when i.acidez is null then cl.acidez else i.acidez end as acidez, \n");
        sb.append("\t\ti.volume_seta_vinte_graus,\n");
        sb.append("\t\ti.volume_densidade_l_vinte_graus,\n");
        sb.append("\t\ti.diferenca_litros, \n");
        sb.append("\t\tr.id_material, \n");
        sb.append("\t\tr.id AS id_romaneio \n");
        sb.append("\tFROM sap.tb_conversao_litragem_item i \n");
        sb.append("\tJOIN sap.tb_conversao_litragem cl\n");
        sb.append("\t\tON i.id_conversao_litragem = cl.id\n");
        sb.append("\tJOIN sap.tb_romaneio r\n");
        sb.append("\t\tON cl.id = r.id_conversao_litragem\n");
        sb.append("\tORDER BY r.id ASC, dt_cadastro DESC\n");
        sb.append(") c\n");
        if (filtro.isNotNull()) {
            sb.append("WHERE\n");
            if (ObjetoUtil.isNotNull(filtro.getDataInicio()) && ObjetoUtil.isNotNull(filtro.getDataFim())) {
                sb.append("(c.dt_cadastro >= '" + DateUtil.formatTo(
                        DateUtil.obterDataHoraZerada(filtro.getDataInicio()),
                        "yyyy-MM-dd HH:mm:ss"
                ) + "' and c.dt_cadastro <= '" + DateUtil.formatTo(
                        DateUtil.converterDateFimDia(filtro.getDataFim()),
                        "yyyy-MM-dd HH:mm:ss"
                ) + "') ");
                if (ObjetoUtil.isNotNull(filtro.getMaterial())) {
                    sb.append(" and ");
                }
            } else if (ObjetoUtil.isNotNull(filtro.getDataInicio())) {
                sb.append(" c.dt_cadastro >= '" + DateUtil.formatTo(
                        DateUtil.obterDataHoraZerada(filtro.getDataInicio()),
                        "yyyy-MM-dd HH:mm:ss"
                ) + "' ");
                if (ObjetoUtil.isNotNull(filtro.getMaterial())) {
                    sb.append(" and ");
                }
            } else if (ObjetoUtil.isNotNull(filtro.getDataFim())) {
                sb.append(" c.dt_cadastro <= '" + DateUtil.formatTo(
                        DateUtil.converterDateFimDia(filtro.getDataFim()),
                        "yyyy-MM-dd HH:mm:ss"
                ) + "' ");
                if (ObjetoUtil.isNotNull(filtro.getMaterial())) {
                    sb.append(" and ");
                }
            }
            if (ObjetoUtil.isNotNull(filtro.getMaterial())) {
                sb.append(" c.id_material = " + filtro.getMaterial().getId());
            }
        }

        List<Object[]> retorno = this.getSession().createNativeQuery(sb.toString()).getResultList();

        if (ColecaoUtil.isNotEmpty(retorno)) {

            return retorno.stream().map(r -> new RelatorioAnaliseConversaoDTO(
                    ((BigInteger) r[1]).longValue(),
                    ((BigInteger) r[2]).longValue(),
                    (String) r[0],
                    (Date) r[3],
                    (BigDecimal) r[4],
                    (BigDecimal) r[5],
                    (BigDecimal) r[6],
                    (BigDecimal) r[7],
                    (BigDecimal) r[8],
                    (BigDecimal) r[9]
            )).collect(Collectors.toList());
        }

        return null;
    }

    @Transactional
    @Override
    public Long obterIdDaPesagemPorRomaneio(Long idRomaneio) {

        try {

            CriteriaBuilder criteriaBuilder = this.getSession().getCriteriaBuilder();

            CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

            Root<Romaneio> root = criteriaQuery.from(Romaneio.class);

            Join<Romaneio, Pesagem> pesagemJoin = root.join(Romaneio_.pesagem);

            criteriaQuery.where(criteriaBuilder.equal(root.get(Romaneio_.id), idRomaneio));

            criteriaQuery.select(pesagemJoin.get(Pesagem_.id));

            TypedQuery<Long> typedQuery = this.getSession().createQuery(criteriaQuery);

            return typedQuery.getSingleResult();

        } catch (NoResultException e) {

            return null;
        }
    }

    @org.springframework.transaction.annotation.Transactional
    @Override
    public void alterarAtributos(Romaneio entidade, String... atributos) {

        StringBuilder sqlUpdate = new StringBuilder();

        Table table = entidade.getClass().getAnnotation(Table.class);

        sqlUpdate.append("update ");
        if (!org.springframework.util.StringUtils.isEmpty(table.schema())) {
            sqlUpdate.append(table.schema()).append(".");
        }
        sqlUpdate.append(table.name()).append(" set ");

        Map<String, Object> valoresPorParametro = new HashMap<>();

        Arrays.stream(atributos).forEach(atributo -> {
            Field field = FieldUtils.getField(entidade.getClass(), atributo, true);

            String nomeColune;
            if (field.isAnnotationPresent(JoinColumn.class)) {
                JoinColumn joinColumn = field.getAnnotation(JoinColumn.class);
                nomeColune = joinColumn.name();

            } else if (field.isAnnotationPresent(Column.class)) {
                Column column = field.getAnnotation(Column.class);
                nomeColune = column.name().length() > 0 ? column.name() : field.getName();

            } else {
                nomeColune = field.getName();

            }

            Object valor = Util.getValorFromField(entidade, field);

            sqlUpdate.append(nomeColune).append(" = ").append(Objects.isNull(valor) ? "null" : ":" + atributo).append(", ");

            if (!Objects.isNull(valor)) {
                if (valor.getClass().isEnum()) {
                    Enumerated enumerated = field.getAnnotation(Enumerated.class);

                    Enum valorEnum = (Enum) valor;
                    valor = enumerated.value() == EnumType.ORDINAL ? valorEnum.ordinal() : valorEnum.name();

                } else if (valor instanceof ObjectID) {
                    valor = ((ObjectID) valor).getId();

                }

                valoresPorParametro.put(atributo, valor);
            }
        });

        sqlUpdate.delete(sqlUpdate.length() - 2, sqlUpdate.length());
        sqlUpdate.append(" where ").append(getNameIdProperty()).append(" = :id");
        valoresPorParametro.put("id", entidade.getId());

        Query query = em.createNativeQuery(sqlUpdate.toString());
        valoresPorParametro.forEach(query::setParameter);
        query.executeUpdate();
    }
}
