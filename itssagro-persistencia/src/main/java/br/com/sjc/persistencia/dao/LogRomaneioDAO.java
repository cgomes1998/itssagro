package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.LogRomaneio;

public interface LogRomaneioDAO extends DAO<Long, LogRomaneio> {

    LogRomaneio findByRomaneioId(final Long idRomaneio);
}
