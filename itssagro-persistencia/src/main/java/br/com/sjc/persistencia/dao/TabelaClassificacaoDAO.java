package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.enums.StatusCadastroSAPEnum;
import br.com.sjc.modelo.sap.TabelaClassificacao;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Set;

public interface TabelaClassificacaoDAO extends DAO<Long, TabelaClassificacao> {

    TabelaClassificacao findByCodigo(final String codigo);

    List<TabelaClassificacao> findByCodigoArmazemTerceirosIn(final List<String> codigosArmazemTerceiros);

    List<TabelaClassificacao> findByItemClassificacaoAndCodigoMaterialOrderByIndiceNumericoAsc(final String itemClassificacao, final String codigoMaterial);

    List<TabelaClassificacao> findByStatusCadastroSapIn(final List<StatusCadastroSAPEnum> statusCadastroSAPEnums);

    List<TabelaClassificacao> findByStatusCadastroSapInAndDataCadastroGreaterThanEqual(final List<StatusCadastroSAPEnum> statusCadastroSAPEnums, final Date data);

    @Query(value = "select desc_item_classificacao from sap.tb_tabela_classificacao group by desc_item_classificacao, item_classificacao order by item_classificacao asc", nativeQuery = true)
    List<String> findDistinctDescricaoItemClassificacaoByOrderByItemClassificacaoAsc();

    @Transactional
    @Modifying
    @Query("update TabelaClassificacao t set t.indicePermitidoPorPerfil = ?2 where t.id = ?1")
    void updatePermitirIndicePorPerfil(Long id, boolean indicePermitidoPorPerfil);

    @Transactional
    @Modifying
    @Query("update TabelaClassificacao t set t.codigoArmazemTerceiros = ?2 where t.id in ?1")
    void updateCodigoOrigem(List<Long> id, String codigo);

    @Transactional
    @Modifying
    @Query("update TabelaClassificacao t set t.codigoLeitor = ?2, t.hostLeitor = ?3, t.portaLeitor = ?4 where t.id in ?1")
    void updateCodigoLeitor(List<Long> id, String codigoLeitor, String hostLeitor, String portaLeitor);

    @Transactional
    @Modifying
    @Query("update TabelaClassificacao t set t.codigo = ?2, t.codigoMaterial = ?3, t.descricaoItemClassificacao = ?4, t.indice = ?5, t.itemClassificacao = ?6, t.percentualDesconto = ?7, t.indiceNumerico = ?8, t.indicePermitidoPorPerfil = ?9 where t.id in ?1")
    void updateTabelaClassificacao(final Long id, String codigo, String codigoMaterial, String descricaoItemClassificacao, String indice, String itemClassificacao, String percentualDesconto, Double indiceNumerico, boolean indicePermitidoPorPerfil);

}
