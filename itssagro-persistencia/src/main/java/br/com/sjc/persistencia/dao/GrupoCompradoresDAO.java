package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.sap.GrupoCompradores;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface GrupoCompradoresDAO extends DAO<Long, GrupoCompradores> {

    @Query("select g.id from GrupoCompradores g where g.codigo = ?1")
    Long findIdByCodigo(final String codigo);

    List<GrupoCompradores> findByOrderByCodigoAsc();
}
