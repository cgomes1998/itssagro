package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.FluxoMaterial;

public interface FluxoMaterialDAO extends DAO<Long, FluxoMaterial> {
}
