package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Motivo;
import br.com.sjc.modelo.Senha;
import br.com.sjc.modelo.enums.StatusSenhaEnum;
import br.com.sjc.modelo.sap.Material;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

public interface SenhaDAO extends DAO<Long, Senha> {

    boolean existsByVeiculoIdAndStatusSenhaNotIn(
            final Long idPlaca,
            final List<StatusSenhaEnum> statusSenhaEnums
    );

    boolean existsByVeiculoIdAndStatusSenhaNotInAndIdNot(
            final Long idPlaca,
            final List<StatusSenhaEnum> statusSenhaEnums,
            final Long idSenha
    );

    Senha findOneBySenha(String senha);

    Senha findTop1ByStatusSenhaOrderByDataAlteracaoDesc(StatusSenhaEnum statusSenha);

    @Query("select s.motivos from Senha s where s.id = ?1")
    List<Motivo> findMotivosById(Long id);

    List<Senha> findTop3ByStatusSenhaAndIdNotOrderByDataAlteracaoDesc(StatusSenhaEnum statusSenha, Long id);

    @Query("select count(senha.id) > 0 from Senha senha where senha.seta.id = ?1")
    boolean existsSenhaByTipoVeiculoSeta(Long tipoVeiculoSetaId);

    List<Senha> findTop10ByStatusSenhaAndSenhaContainingIgnoreCaseOrMaterialDescricaoContainingIgnoreCaseOrMotoristaNomeContainingIgnoreCaseOrderBySenhaAsc(
            StatusSenhaEnum statusSenha,
            String senha,
            String materialDescricao,
            String motoristaNome
    );

    @Transactional
    @Modifying
    @Query("update Senha s set s.statusSenha = ?1 where s.id in ?2")
    void atualizarStatusDeSenhas(
            final StatusSenhaEnum statusSenhaEnum,
            final List<Long> ids
    );
}
