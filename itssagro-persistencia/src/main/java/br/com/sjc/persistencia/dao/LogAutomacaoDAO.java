package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.LogAutomacao;

/**
 * Created by Claudio Gomes on 19/08/2020
 */
public interface LogAutomacaoDAO extends DAO<Long, LogAutomacao> {

}
