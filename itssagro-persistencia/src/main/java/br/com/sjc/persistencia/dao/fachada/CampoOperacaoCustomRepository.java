package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.dto.CampoOperacaoDTO;
import br.com.sjc.modelo.enums.DadosSincronizacaoEnum;

import java.util.List;

public interface CampoOperacaoCustomRepository {

    @org.springframework.transaction.annotation.Transactional( readOnly = true )
    List<CampoOperacaoDTO> listar (
            DadosSincronizacaoEnum operacao
    );
}
