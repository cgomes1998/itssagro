package br.com.sjc.persistencia.dao.fachada;

import br.com.sjc.modelo.dto.ItemNFPedidoDTO;

import javax.persistence.Transient;
import javax.transaction.Transactional;
import java.util.List;

public interface ItemNFPedidoCustomRepository {

    @Transactional
    List<ItemNFPedidoDTO> obterItensDeRomaneioParaEntradaLiberada();

    @Transient
    List<ItemNFPedidoDTO> obterItensDeRomaneioParaAutoatendimento();
}
