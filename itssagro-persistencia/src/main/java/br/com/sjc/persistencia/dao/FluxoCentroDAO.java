package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.FluxoCentro;

public interface FluxoCentroDAO extends DAO<Long, FluxoCentro> {
}
