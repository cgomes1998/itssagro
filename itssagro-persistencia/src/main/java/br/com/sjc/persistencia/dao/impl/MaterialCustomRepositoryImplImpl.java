package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.modelo.dto.MaterialDTO;
import br.com.sjc.modelo.enums.StatusRegistroEnum;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.modelo.sap.Material_;
import br.com.sjc.persistencia.dao.fachada.MaterialCustomRepository;
import org.springframework.stereotype.Service;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Service
public class MaterialCustomRepositoryImplImpl extends CustomRepositoryImpl implements MaterialCustomRepository {

    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    @Override
    public List<MaterialDTO> listarPorStatus(
            final
            StatusRegistroEnum statusRegistro
    ) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();
        CriteriaQuery<MaterialDTO> criteriaQuery = builder.createQuery(MaterialDTO.class);
        Root<Material> root = criteriaQuery.from(Material.class);
        criteriaQuery.orderBy(builder.asc(root.get(Material_.descricao)));
        criteriaQuery.where(builder.equal(root.get(Material_.statusRegistro), statusRegistro));
        criteriaQuery.select(builder.construct(
                MaterialDTO.class,
                root.get(Material_.id),
                root.get(Material_.codigo),
                root.get(Material_.descricao),
                root.get(Material_.realizarConversaoLitragem),
                root.get(Material_.necessitaTreinamentoMotorista),
                root.get(Material_.lacrarCarga),
                root.get(Material_.realizarAnaliseQualidade),
                root.get(Material_.buscarCertificadoQualidade),
                root.get(Material_.necessitaMaisDeUmaAnalise),
                root.get(Material_.gerarNumeroCertificado),
                root.get(Material_.possuiResolucaoAnp)
        ));
        TypedQuery<MaterialDTO> query = this.getSession().createQuery(criteriaQuery);
        return query.getResultList();
    }
}
