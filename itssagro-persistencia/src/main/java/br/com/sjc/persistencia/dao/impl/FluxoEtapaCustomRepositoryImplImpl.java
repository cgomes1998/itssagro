package br.com.sjc.persistencia.dao.impl;

import br.com.sjc.modelo.*;
import br.com.sjc.modelo.dto.FluxoEtapaDTO;
import br.com.sjc.modelo.dto.ProximoFluxoEtapaDTO;
import br.com.sjc.modelo.enums.EtapaEnum;
import br.com.sjc.modelo.enums.StatusEnum;
import br.com.sjc.modelo.sap.Centro;
import br.com.sjc.modelo.sap.Material;
import br.com.sjc.persistencia.dao.fachada.FluxoEtapaCustomRepository;
import br.com.sjc.util.ObjetoUtil;
import br.com.sjc.util.StringUtil;
import org.springframework.stereotype.Service;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class FluxoEtapaCustomRepositoryImplImpl extends CustomRepositoryImpl implements FluxoEtapaCustomRepository {

    @Transactional
    @Override
    public List<ProximoFluxoEtapaDTO> listarProximasEtapasDoFluxo(Fluxo fluxo) {

        CriteriaBuilder criteriaBuilder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<ProximoFluxoEtapaDTO> criteriaQuery = criteriaBuilder.createQuery(ProximoFluxoEtapaDTO.class);

        Root<Fluxo> root = criteriaQuery.from(Fluxo.class);

        Join<Fluxo, FluxoEtapa> fluxoEtapaJoin = root.join(Fluxo_.etapas);

        List<Predicate> predicates = new ArrayList<>();

        predicates.add(criteriaBuilder.equal(root.get(Fluxo_.id), fluxo.getId()));

        criteriaQuery.where(predicates.toArray(new Predicate[0]));

        criteriaQuery.select(criteriaBuilder.construct(ProximoFluxoEtapaDTO.class, fluxoEtapaJoin.get(FluxoEtapa_.id), fluxoEtapaJoin.get(FluxoEtapa_.sequencia), fluxoEtapaJoin.get(FluxoEtapa_.etapa)));

        criteriaQuery.orderBy(criteriaBuilder.asc(fluxoEtapaJoin.get(FluxoEtapa_.sequencia)));

        TypedQuery<ProximoFluxoEtapaDTO> typedQuery = this.getSession().createQuery(criteriaQuery);

        return typedQuery.getResultList();
    }

    @Transactional
    @Override
    public List<FluxoEtapaDTO> listarPorFluxo(List<Long> idFluxos) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<FluxoEtapaDTO> criteriaQuery = builder.createQuery(FluxoEtapaDTO.class);

        Root<FluxoEtapa> root = criteriaQuery.from(FluxoEtapa.class);

        Join<FluxoEtapa, Fluxo> fluxoJoin = root.join(FluxoEtapa_.fluxo);

        Join<FluxoEtapa, Formulario> formularioJoin = root.join(FluxoEtapa_.formulario, JoinType.LEFT);

        criteriaQuery.where(fluxoJoin.get(Fluxo_.id).in(idFluxos));

        criteriaQuery.select(builder.construct(FluxoEtapaDTO.class, root.get(FluxoEtapa_.id), root.get(FluxoEtapa_.status), root.get(FluxoEtapa_.sequencia), root.get(FluxoEtapa_.etapa), root.get(FluxoEtapa_.medirTempo), root.get(FluxoEtapa_.etapaFinal), fluxoJoin.get(Fluxo_.id), fluxoJoin.get(Fluxo_.codigo), fluxoJoin.get(Fluxo_.descricao), fluxoJoin.get(Fluxo_.tipoFluxo), formularioJoin.get(Formulario_.id), formularioJoin.get(Formulario_.descricao), formularioJoin.get(Formulario_.formularioStatus), formularioJoin.get(Formulario_.cabecalhoDadosTransportadora), formularioJoin.get(Formulario_.cabecalhoDadosMotorista), formularioJoin.get(Formulario_.cabecalhoDadosCliente), formularioJoin.get(Formulario_.cabecalhoDadosVeiculo)));

        criteriaQuery.orderBy(builder.asc(root.get(FluxoEtapa_.sequencia)));

        return this.getSession().createQuery(criteriaQuery).getResultList();
    }

    @Transactional
    @Override
    public List<FluxoEtapaDTO> obterPorCentroEtapaMaterial(Centro centro, EtapaEnum etapa, String codigoMaterial, String descricaoMaterial, TipoFluxoEnum tipoFluxo) {

        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();

        CriteriaQuery<FluxoEtapaDTO> criteriaQuery = builder.createQuery(FluxoEtapaDTO.class);

        Root<FluxoEtapa> root = criteriaQuery.from(FluxoEtapa.class);

        Join<FluxoEtapa, Fluxo> fluxoJoin = root.join(FluxoEtapa_.fluxo, JoinType.LEFT);

        List<Predicate> predicados = new ArrayList<>();

        if (ObjetoUtil.isNotNull(centro)) {

            Join<FluxoEtapa, FluxoCentro> fluxoCentroJoin = fluxoJoin.join("centros", JoinType.LEFT);

            Join<FluxoCentro, Centro> centroJoin = fluxoCentroJoin.join("centro", JoinType.LEFT);

            predicados.add(builder.equal(centroJoin.get("id"), centro.getId()));
        }

        if (StringUtil.isNotNullEmpty(codigoMaterial)) {

            Join<FluxoEtapa, FluxoMaterial> fluxoMaterialJoin = fluxoJoin.join("materiais", JoinType.LEFT);

            Join<FluxoMaterial, Material> materialJoin = fluxoMaterialJoin.join("material", JoinType.LEFT);

            predicados.add(builder.equal(materialJoin.get("codigo"), codigoMaterial));
        }

        if (ObjetoUtil.isNotNull(etapa)) {

            predicados.add(builder.equal(root.get("etapa"), etapa));
        }

        if (ObjetoUtil.isNotNull(tipoFluxo)) {

            predicados.add(builder.equal(fluxoJoin.get(Fluxo_.tipoFluxo), tipoFluxo));
        }

        predicados.add(builder.equal(fluxoJoin.get("status"), StatusEnum.ATIVO.ordinal()));

        criteriaQuery.where(predicados.toArray(new Predicate[]{}));

        criteriaQuery.select(builder.construct(
                FluxoEtapaDTO.class,
                fluxoJoin.get(Fluxo_.id),
                root.get(FluxoEtapa_.id),
                root.get(FluxoEtapa_.status),
                root.get(FluxoEtapa_.etapa)
        ));

        criteriaQuery.groupBy(fluxoJoin.get(Fluxo_.id), root.get(FluxoEtapa_.id), root.get(FluxoEtapa_.status), root.get(FluxoEtapa_.etapa));

        TypedQuery<FluxoEtapaDTO> typedQuery = this.getSession().createQuery(criteriaQuery);

        return typedQuery.getResultList();
    }

}
