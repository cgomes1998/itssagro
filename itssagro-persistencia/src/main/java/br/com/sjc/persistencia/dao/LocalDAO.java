package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.Local;
import br.com.sjc.modelo.enums.StatusEnum;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by julio.bueno on 03/07/2019.
 */
public interface LocalDAO extends DAO<Long, Local> {

    @Query("select max(codigo) from Local")
    Long getMaxCodigo();

    List<Local> findByStatus(StatusEnum status);
}
