package br.com.sjc.persistencia.dao;

import br.com.sjc.fachada.dao.DAO;
import br.com.sjc.modelo.AnaliseQualidade;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AnaliseQualidadeDAO extends DAO<Long, AnaliseQualidade> {


    boolean existsByRomaneioId ( final Long id );

    List<AnaliseQualidade> findByRomaneioId(Long idRomaneio);

    @Query("select count(a.id)>0 from AnaliseQualidade a where a.idLaudo = ?1 and a.romaneio is not null")
    boolean laudoEmUso(
            Long idLaudo
    );
}
